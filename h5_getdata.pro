; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/h5_getdata.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; NAME:
;    H5_GETDATA
;
; PURPOSE:
;    An easy one step way to read an entire data set from an HDF5 file
;
; MODIFICATION HISTORY:
;     Written by:   AGEH, 7/2013
;-
function h5_getdata, file, idIn
  compile_opt idl2, hidden
  on_error, 2

  if (N_PARAMS() ne 2) then begin
    message, 'Both FILE and ID must be supplied'
    return, -1
  endif
  if (N_ELEMENTS(file) ne 1) then begin
    message, 'FILE must be a single element'
    return, -1
  endif
  if (SIZE(idIn, /TNAME) ne 'STRING') then begin
    message, 'ID must be a string'
    return, -1
  endif
  
  currentlyOpen = 0b
  case SIZE(file, /TNAME) of
    'LONG' : begin
      catch, err
      if (err ne 0) then begin
        catch, /cancel
        message, 'Invalid HDF5 file/group ID: '+STRTRIM(file,2)
        return, -1
      endif
      !NULL = H5G_GET_OBJINFO(file, '/')
      fid = file
      currentlyOpen = 1b
      catch, /cancel
    end
    'STRING' : begin
      ; Does file exist?
      str = FILE_INFO(file)
      if (str.exists) then begin
        if (~H5F_IS_HDF5(file)) then begin
          message, 'File is not a valid HDF5 file: '+file
          return, -1
        endif
        fid = H5F_OPEN(file)
      endif else begin
        catch, err
        if (err ne 0) then begin
          catch, /cancel
          message, 'Unable to open file: '+file
          return, -1
      endif
        fid = H5F_OPEN(file)
      endelse
    end
    else : begin
      message, 'Invalid file specification: '+STRING(file)
      return, -1
    end
  endcase
  
  if (N_ELEMENTS(idIn) eq 6) then begin
    if ( (STRUPCASE(idIn[0,0]) eq 'FILE') && $
         (STRUPCASE(idIn[0,1]) eq 'DATASET') ) then begin
      id = idIn[1,1]
    endif else begin
      message, 'Invalid ID'
      return, -1
    endelse
  endif else begin
    id = idIn
  endelse
  if (N_ELEMENTS(id) ne 1) then begin
    message, 'Incorrect number of elements in ID'
    return, -1
  endif
  
  catch, err
  if (err ne 0) then begin
    catch, /cancel
    message, 'Unable to read dataset: '+id
    return, -1
  endif
  
  did = H5D_OPEN(fid, id)
  data = H5D_READ(did)
  H5D_CLOSE, did
  if (~currentlyOpen) then begin
    H5F_CLOSE, fid
  endif
  
  return, data
  
end