; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/file_gunzip.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+


;---------------------------------------------------------------------------
pro FILE_GUNZIP, filesIn, filesOut, $
  DEBUG=debug, _REF_EXTRA=extra

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then begin
    ON_ERROR, 2
    CATCH, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      msg = !ERROR_STATE.msg
      pos = STRPOS(msg, 'ZIP')
      if (pos gt 0) then begin
        msg = STRMID(msg, 0, pos) + 'UN' + STRMID(msg, pos)
      endif
      ; Use NONAME since we will already have a prefix from FILE_GZIP.
      MESSAGE, msg, /NONAME
    endif
  endif
  
  if (N_PARAMS() eq 1) then begin
    FILE_GZIP, filesIn, /UNCOMPRESS, $
      DEBUG=debug, _STRICT_EXTRA=extra
  endif else begin
    FILE_GZIP, filesIn, filesOut, /UNCOMPRESS, $
      DEBUG=debug, _STRICT_EXTRA=extra
  endelse

end

