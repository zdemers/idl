; $Id: //depot/InDevelopment/scrums/IDL_Kraken/idl/idldir/lib/tic.pro#7 $
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;-------------------------------------------------------------------------------
;+
; :Description:
;    Record the current system clock time, for use in time profiling.
;
; :Keywords:
;    PROFILER
;-
function tic, name, DEFAULT=default, PROFILER=profiler

  compile_opt idl2, hidden
  on_error, 2
  common tictoc, tictoc_time, tictoc_profiler

  if ~ISA(tictoc_time) then begin
    tictoc_time = 0d
  endif

  if (~ISA(name)) then name = ''

  ; Make sure TOC is ready to go, to avoid including the compile time.
  RESOLVE_ROUTINE, 'TOC', /NO_RECOMPILE

  if (ISA(profiler)) then begin
    tictoc_profiler = KEYWORD_SET(profiler)
    PROFILER, /RESET
    if (tictoc_profiler) then begin
      PROFILER
      PROFILER, /SYSTEM
    endif else begin
      PROFILER, /SYSTEM, /CLEAR
      PROFILER, /CLEAR
    endelse
  endif
  
  tt = SYSTIME(/SECONDS)
  result = {NAME: name, TIME: tt}

  if (KEYWORD_SET(default)) then $
    tictoc_time = tt

  return, result

end


;-------------------------------------------------------------------------------
;+
; :Description:
;    Record the current system clock time, for use in time profiling.
;
; :Keywords:
;    PROFILER
;
;-
pro tic, name, PROFILER=profiler

  compile_opt idl2, hidden
  on_error, 2
  common tictoc, tictoc_time, tictoc_profiler
  
  !NULL = TIC(name, /DEFAULT, PROFILER=profiler)
end
