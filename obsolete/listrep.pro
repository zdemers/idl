; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/obsolete/listrep.pro#1 $
;
;  Copyright (c) 1991-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.


 pro ListRep,X,RowNum,Rows,here
; ListRep repairs a matrix that has had data thrown away in Listwise

  SX = size(X)
 SX1 = SX(1)

 
 count = N_Elements(RowNum)
 SX2 = SX(2)+count

 X1 = Fltarr( SX1,SX2)
 X1(*,here) =X
 X1(*,RowNum) = Rows
 X =X1

  return
 end
   