; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/isave.pro#1 $
;
; Copyright (c) 2008-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;   iSave
;
; PURPOSE:
;   Saves the selected visualization to an image file
;
; PARAMETERS:
;   FILENAME - The name of the file to open
;
; KEYWORDS:
;   SAVE_AS - If set, and filename is not supplied, then always prompt for a
;             new filename. The default is to mimic the behaviour of 
;             File->Save; if the tool has already been saved to a file, then
;             save to the file again, overwriting the old file, without
;             prompting for a new filename.
;
;   RESOLUTION - The output resolution in dots-per-inch (dpi).
;             The default is to use the iTools resolution preference.
;
;   TARGET_IDENTIFIER - The identifier of the window, or iTool to save. If set
;                       to an item that is not a tool or window, the window
;                       that encompasses the defined object will be used. If
;                       not supplied the current iTool will be used.
;
;-

;-------------------------------------------------------------------------
pro iSave, strFileIn, $
           BITMAP=bitmapIn, $
           SAVE_AS=saveAs, $
           RESOLUTION=resolutionIn, $
           TARGET_IDENTIFIER=ID, $
           TOOL=toolIn, $
           _EXTRA=_extra
  compile_opt hidden, idl2

@idlit_itoolerror.pro

  strFile = (N_ELEMENTS(strFileIn) gt 0 ? strFileIn[0] : '')

  ;; Set up parameters
  fullId = (N_ELEMENTS(ID) eq 0) ? iGetCurrent() : iGetID(ID[0], TOOL=toolIn)

  ;; Error checking
  if (fullID[0] eq '') then begin
    message, 'Target not found: '+ID
    return
  endif

  ;; Get the system object
  oSystem = _IDLitSys_GetSystem(/NO_CREATE)
  if (~OBJ_VALID(oSystem)) then return

  ;; Get the object from ID
  oObj = oSystem->GetByIdentifier(fullID)
  if (~OBJ_VALID(oObj)) then return
  
  ;; Get the tool
  oTool = oObj->GetTool()
  if (~OBJ_VALID(oTool)) then return
  
  ;; Get the save operation
  oDesc = oTool->GetByIdentifier('OPERATIONS/FILE/SAVE')
  oSave = oDesc->GetObjectInstance() 

  oSave->SetProperty, FILENAME=strFile
  
  ;; If save as, then always show ui (CT: leave this as an &&)
  if ((strFile eq '') && KEYWORD_SET(saveAs)) then begin
    oSave->GetProperty, SHOW_EXECUTION_UI=oldShowUI
    oSave->SetProperty, SHOW_EXECUTION_UI=1
  endif
  
  ;; Output resolution
  resolution = ISA(resolutionIn) ?  DOUBLE(resolutionIn[0]) : 600d
  
  if (ISA(bitmapIn)) then begin
    bitmap = KEYWORD_SET(bitmapIn)
  endif else begin
    ; The default is BITMAP=0 (Vector) unless we are 3D.
    bitmap = 0
    ; If any of the contained dataspaces are 3D, then switch from
    ; vector rendering to bitmap rendering.
    oWin = OBJ_VALID(oTool) ? oTool->GetCurrentWindow() : OBJ_NEW()
    oView = OBJ_VALID(oWin) ? oWin->GetCurrentView() : OBJ_NEW()
    oLayer = OBJ_VALID(oView) ? oView->GetCurrentLayer() : OBJ_NEW()
    oWorld = OBJ_VALID(oLayer) ? oLayer->GetWorld() : OBJ_NEW()
    oDataSpaces = OBJ_VALID(oWorld) ? oWorld->GetDataSpaces() : OBJ_NEW()
    foreach oDS, oDataSpaces do begin
      if (obj_Valid(oDS) && oDS->Is3D()) then begin
        bitmap = 1
        break
      endif
    endforeach
  endelse
    
  
  ;; Do it
  void = oSave->DoAction(oTool, $
    BIT_DEPTH=0, $   ; this is the default
    SUCCESS=success, $
    BITMAP=bitmap, $
    RESOLUTION=resolution, $
    _EXTRA=_extra)

  ;; Reset show execution flag
  if (success) then begin
    oSave->SetProperty, SHOW_EXECUTION_UI=0
  endif else begin
    oSave->SetProperty, SHOW_EXECUTION_UI=oldShowUI
  endelse
    
end
