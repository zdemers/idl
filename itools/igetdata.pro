; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/igetdata.pro#1 $
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;   iGetData
;
; PURPOSE:
;   Returns data from a given iTools object, if any exists, or the 
;   specified object if requested
;
; CALLING SEQUENCE:
;   iGetData, ID, arg1, arg2, ...
;
; INPUTS:
;   ID - An identifier to an iTools object
;
;   ARGs - Named variables in which to return the data
;
; KEYWORD PARAMETERS:
;   OBJECT - If set to a named variable, return the object specified by the ID.
;
; MODIFICATION HISTORY:
;   Written by: AGEH, RSI, Jun 2008
;
;-

;-------------------------------------------------------------------------
PRO iGetData, ID, arg1, arg2, arg3, arg4, arg5, arg6, arg7, $
              OBJECT=oObj, TOOL=tool, _REF_EXTRA=_extra 
  compile_opt hidden, idl2

on_error, 2

  fullID = (iGetID(ID, TOOL=tool))[0]
  if (fullID[0] eq '') then begin
    message, 'Identifier not found: '+ID
    return
  endif

  catch, iErr
  if(iErr ne 0)then begin
    catch, /cancel
    message, 'Unable to get data: '+ID 
    return
  endif

  ;; Get the system object
  oSystem = _IDLitSys_GetSystem(/NO_CREATE)
  if (~OBJ_VALID(oSystem)) then return

  oObj = oSystem->GetByIdentifier(fullID)
  if (~OBJ_VALID(oObj)) then return
  
  case N_PARAMS() of
    2 : oObj->GetData, arg1, _EXTRA=_extra
    3 : oObj->GetData, arg1, arg2, _EXTRA=_extra
    4 : oObj->GetData, arg1, arg2, arg3, _EXTRA=_extra
    5 : oObj->GetData, arg1, arg2, arg3, arg4, _EXTRA=_extra
    6 : oObj->GetData, arg1, arg2, arg3, arg4, arg5, _EXTRA=_extra
    7 : oObj->GetData, arg1, arg2, arg3, arg4, arg5, arg6, _EXTRA=_extra
    8 : oObj->GetData, arg1, arg2, arg3, arg4, arg5, arg6, arg7, _EXTRA=_extra
    else :
  endcase
  ; For iTools we want polygons and polylines in the annotation layer
  ; to return DEVICE coords, but for Graphics we want them to return
  ; NORMAL coords.
  if (ISA(oObj, "IDLITVISPOLYGON") || ISA(oObj, "IDLITVISPOLYLINE")) then begin
    oObj->GetProperty, _PARENT=oParent
    ;; Ensure data is in proper format
    if (OBJ_ISA(oParent, 'IDLitgrAnnotateLayer')) then begin
      ; don't forget there is the ID parameter
      switch (N_PARAMS()) of
        4 : begin
          ; x, y, z
          arg3 = iConvertCoord(arg3, /NORMAL, /TO_DEVICE)
          ; fall through
        end
        3 : begin
          ; x, y
          arg1 = iConvertCoord(arg1, /NORMAL, /TO_DEVICE)
          arg2 = iConvertCoord(arg2, /NORMAL, /TO_DEVICE)
          break
        end
        ; data (for backwards compatability)
        2 : arg1 = iConvertCoord(arg1, /NORMAL, /TO_DEVICE)
        else :
      endswitch
    endif
  endif
  
end
