; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvisarrow__define.pro#2 $
;
; Copyright (c) 2005-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
; Purpose:
;   The IDLitVisArrow class is the component for vector visualization.
;
;----------------------------------------------------------------------------
; Purpose:
;    Initialize this component
;
; Syntax:
;
;    Obj = OBJ_NEW('IDLitVisArrow')
;
; Result:
;   This function method returns 1 on success, or 0 on failure.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
function IDLitVisArrow::Init, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Initialize superclasses
    if (~self->IDLitVisualization::Init(TYPE="IDLVISARROW", $
        ICON='fitwindow', $
        DESCRIPTION="An arrow Visualization", $
        NAME="Arrow", $
        IMPACTS_RANGE=0, $
        /MANIPULATOR_TARGET, $
        _EXTRA=_extra)) then $
        RETURN, 0

    ; Register all properties.
    self->IDLitVisArrow::_RegisterProperties
    
    ; This will also register our X parameter.
    dummy = self->_IDLitVisVertex::Init(POINTS_NEEDED=2)

    ; Add in our special manipulator visual.
    if (~KEYWORD_SET(noVertexVisual)) then begin
        self->SetDefaultSelectionVisual, OBJ_NEW('IDLitManipVisVertex', $
            /HIDE, PREFIX='LINE')
    endif
    
    self._oPoly = OBJ_NEW('IDLgrPolygon', /ANTIALIAS, /PRIVATE)
    self->Add, self._oPoly
    self._oLine = OBJ_NEW('IDLgrPolyline', /ANTIALIAS, /PRIVATE)
    self->Add, self._oLine


    self._pXdata = PTR_NEW(/ALLOCATE)
    self._pYdata = PTR_NEW(/ALLOCATE)
    self._pZdata = PTR_NEW(/ALLOCATE)

    self._fillBackground = 1b
    self._arrowStyle = 1b
    self._headAngle = 30
    self._headIndent = 0.4d
    self._lengthX = 1
    self._lengthY = 1
    self._thick = 1d
    self._useColor=1d
    self._headSize = 1d

    ; Set any properties
    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisArrow::SetProperty, _EXTRA=_extra

    return, 1                     ; Success
end

;----------------------------------------------------------------------------
; Purpose:
;    Cleanup this component
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
pro IDLitVisArrow::Cleanup

    compile_opt idl2, hidden

    ; Cleanup our palette
    PTR_FREE, self._pXdata, self._pYdata, self._pZdata
    PTR_FREE, self._pColor, self._pFillColor, self._pHeadSize, self._pThick

    ; Cleanup superclass
    self->IDLitVisualization::Cleanup
end

;----------------------------------------------------------------------------
; IDLitVisArrow::_RegisterProperties
;
; Purpose:
;   Internal routine that will register all properties supported by
;   this object.
;
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLitVisArrow::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

    compile_opt idl2, hidden

    registerAll = ~KEYWORD_SET(updateFromVersion)

    if (registerAll) then begin

        self->RegisterProperty, 'ARROW_STYLE', $
            DESCRIPTION='Arrow style', $
            ENUMLIST=[' --------', ' ------->', ' <-------', ' <------>', $
                ' >------>', ' <------<'], $
            NAME='Arrow style'
            
        self->RegisterProperty, 'THICK', /THICK, $
            NAME='Arrow thickness', $
            DESCRIPTION='Thickness of arrow shaft'

        self->RegisterProperty, 'LINE_THICK', /THICK, $
            NAME='Line thickness', $
            DESCRIPTION='Line thickness'

        self->RegisterProperty, 'HEAD_ANGLE', /FLOAT, $
            NAME='Arrowhead angle', $
            DESCRIPTION='Angle in degrees of arrowhead from shaft', $
            VALID_RANGE=[0,90,1], /ADVANCED_ONLY

        self->RegisterProperty, 'HEAD_INDENT', /FLOAT, $
            NAME='Arrowhead indentation', $
            DESCRIPTION='Indentation of arrowhead along shaft', $
            VALID_RANGE=[-1,1, 0.1d], /ADVANCED_ONLY
            
        self->RegisterProperty, 'HEAD_SIZE', /FLOAT, $
            NAME='Head size', $
            DESCRIPTION='Arrowhead size', $
            VALID_RANGE=[0.1d,4d, 0.1d], /ADVANCED_ONLY

        self->RegisterProperty, 'FILL_BACKGROUND', /BOOLEAN, $
            NAME='Fill Background', $
            DESCRIPTION='Fill Background'

        self->RegisterProperty, 'COLOR', /COLOR, $
            NAME='Color', $
            DESCRIPTION='Arrow color'
            
        self->RegisterProperty, 'FILL_COLOR', /COLOR, $
            NAME='Fill Color', $
            DESCRIPTION='Arrow Fill color'

        self->RegisterProperty, 'TRANSPARENCY', /INTEGER, $
            NAME='Transparency', $
            DESCRIPTION='Transparency of vector visualization', $
            VALID_RANGE=[0,100,5]
            
        self->RegisterProperty, 'FILL_TRANSPARENCY', /INTEGER, $
            NAME='Fill Transparency', $
            DESCRIPTION='Transparency of vector visualization', $
            VALID_RANGE=[0,100,5]

        self->RegisterProperty, 'ZVALUE', /FLOAT, $
            NAME='Z value', $
            DESCRIPTION='Z value for vectors', /ADVANCED_ONLY

    endif
end

;----------------------------------------------------------------------------
; IDLitVisArrow::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisArrow::Restore
    compile_opt idl2, hidden

    ; Call superclass restore.
    self->_IDLitVisualization::Restore

    ; Register new properties.
    self->IDLitVisArrow::_RegisterProperties, $
        UPDATE_FROM_VERSION=self.idlitcomponentversion

    self->_UpdateData
end

;----------------------------------------------------------------------------
; IIDLProperty Interface
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
; Purpose:
;   This procedure method retrieves the
;   value of a property or group of properties.
;
; Arguments:
;   None.
;
; Keywords:
;   Any keyword to IDLitVisArrow::Init followed by the word "Get"
;   can be retrieved using this method.
;
pro IDLitVisArrow::GetProperty, $
    ANTIALIAS=antialias, $
    FILL_BACKGROUND=fillBackground, $
    ARROW_STYLE=arrowStyle, $
    THICK=thick, $
    COLOR=color, $
    DATA=data, $
    FILL_COLOR = fillColor, $
    FILL_TRANSPARENCY=fillTransparency, $
    GRID_UNITS=gridUnits, $
    HEAD_ANGLE=headAngle, $
    HEAD_INDENT=headIndent, $
    HEAD_SIZE=headSize, $
    LINE_THICK=lineThick, $
    TRANSPARENCY=transparency, $
    ZVALUE=zValue, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if (ARG_PRESENT(antialias)) then $
        self._oLine->GetProperty, ANTIALIAS=antialias

    if (ARG_PRESENT(arrowStyle)) then $
        arrowStyle = self._arrowStyle
        
    if (ARG_PRESENT(fillBackground)) then $
        fillBackground = self._fillBackground

    if (ARG_PRESENT(thick)) then begin
      thick = PTR_VALID(self._pThick) ? *self._pThick : self._thick
    endif

    if (ARG_PRESENT(color)) then begin
      color = PTR_VALID(self._pColor) ? *self._pColor : self._color
    endif
    
    if (ARG_PRESENT(data)) then $
        self._oPoly->GetProperty, DATA=data
        
    if (arg_present(fillColor)) then begin
      fillColor = PTR_VALID(self._pFillColor) ? $
        *self._pFillColor : self._fillColor
    endif

    if (ARG_PRESENT(gridUnits) ne 0) then $
        gridUnits = self._gridUnits

    if (ARG_PRESENT(headAngle)) then $
        headAngle = self._headAngle

    if (ARG_PRESENT(headIndent)) then $
        headIndent = self._headIndent
        
    if (ARG_PRESENT(headSize)) then begin
      headSize = PTR_VALID(self._pHeadSize) ? $
        *self._pHeadSize : self._headSize
    endif

    if (ARG_PRESENT(lineThick)) then $
        self._oLine->GetProperty, THICK=lineThick

    if ARG_PRESENT(transparency) then begin
        self._oLine->GetProperty, ALPHA_CHANNEL=alpha
        transparency = 0 > ROUND(100 - alpha*100) < 100
    endif
    
    if ARG_PRESENT(fillTransparency) then begin
        self._oPoly->GetProperty, ALPHA_CHANNEL=alpha
        fillTransparency = 0 > ROUND(100 - alpha*100) < 100
    endif

    if (ARG_PRESENT(zValue)) then $
        zValue = self._zValue

    ; get superclass properties
    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisualization::GetProperty, _EXTRA=_extra

end

;----------------------------------------------------------------------------
; Purpose:
;   This procedure method sets the
;   value of a property or group of properties.
;
; Arguments:
;   None.
;
; Keywords:
;   Any keyword to IDLitVisArrow::Init followed by the word "Set"
;   can be set using this method.
;
pro IDLitVisArrow::SetProperty, $
    ANTIALIAS=antialias, $
    FILL_BACKGROUND=fillBackground, $
    FILL_TRANSPARENCY=fillTransparency, $
    ARROW_STYLE=arrowStyle, $
    THICK=thick, $
    COLOR=colorIn, $
    FILL_COLOR=fillColorIn, $
    GRID_UNITS=gridUnits, $
    HEAD_ANGLE=headAngle, $
    HEAD_INDENT=headIndent, $
    HEAD_SIZE=headSize, $
    LINE_THICK=lineThick, $
    TRANSPARENCY=transparency, $
    WITHIN_DRAW=withinDraw, $   ; not a property! just an internal keyword
    ZVALUE=zValue, $
    DATA = data, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if (N_ELEMENTS(gridUnits) ne 0) then begin
        if (ISA(gridUnits, 'STRING')) then begin
          case (STRLOWCASE(gridUnits)) of
          'm': self._gridUnits = 1
          'meters': self._gridUnits = 1
          'deg': self._gridUnits = 2
          'degrees': self._gridUnits = 2
          else: self._gridUnits = 0
          endcase
        endif else begin
          self._gridUnits = gridUnits
        endelse
        ; Change to isotropic for units = meters or degrees.
        wasIsotropic = self->IsIsotropic()
        isIsotropic = self._gridUnits eq 1 || self._gridUnits eq 2
        if (wasIsotropic ne isIsotropic) then begin
            self->IDLitVisualization::SetProperty, ISOTROPIC=isIsotropic
        endif
        ; If units changed we may need to recalculate our vectors.
        self->_UpdateData, WITHIN_DRAW=withinDraw
        ; If isotropy changed then update our dataspace as well.
        if (wasIsotropic ne isIsotropic) then begin
            self->OnDataChange, self
            self->OnDataComplete, self
        endif
    endif

    if (N_ELEMENTS(antialias) gt 0) then begin
        self._oLine->SetProperty, ANTIALIAS=antialias
        self._oPoly->SetProperty, ANTIALIAS=antialias
    endif

    if (N_ELEMENTS(fillBackground) gt 0) then begin
        if ((fillBackground lt 0) || (fillBackground gt 1)) then fillBackground = 1 
        self._fillBackground = fillBackground
        self._oPoly->SetProperty, HIDE=(self._fillBackground eq 0)
        if (self._fillBackground eq 1) then $
          self._oPoly->SetProperty, COLOR=self._fillColor
        self->_UpdateData, WITHIN_DRAW=withinDraw
    endif
        
    if (N_ELEMENTS(arrowStyle) gt 0) then begin
        if ((arrowStyle gt 5) || (arrowStyle lt 0)) then arrowStyle = 1
        self._arrowStyle = arrowStyle
        self->_UpdateData, WITHIN_DRAW=withinDraw
    endif
  
    nthick = N_ELEMENTS(thick)
    if (nthick gt 0) then begin
      PTR_FREE, self._pThick
      if (nthick gt 1) then begin
        self._pThick = PTR_NEW(thick)
      endif else begin
        self._thick = 1 > DOUBLE(thick) < 10
      endelse
      self->_UpdateData, WITHIN_DRAW=withinDraw
    endif

    if (N_ELEMENTS(colorIn) gt 0) then begin
      style_convert, colorIn, COLOR=color
      if (~ISA(color, /ARRAY) || MAX(color lt 0) eq 1) then $
        color = [0b,0b,0b]
      nd = SIZE(color, /N_DIM)
      dim = SIZE(color, /DIM)
      if (nd eq 1 && dim[0] ne 3) then $
        MESSAGE, 'COLOR must be a color name or a 3-element array.'
      if (nd eq 2 && dim[0] ne 3) then $
        MESSAGE, 'COLOR must be a string array of colors names or a 3xN array.'

      if (nd eq 2) then begin
        PTR_FREE, self._pColor
        self._pColor = PTR_NEW(color)
        self->_UpdateArrow
      endif else begin
        ; If we used to have an array of colors, clear out our pointer
        ; and reset the IDLgrPolyline property.
        if PTR_VALID(self._pColor) then begin
          PTR_FREE, self._pColor
          self._oLine->SetProperty, VERT_COLORS=0
        endif
        self._color = color[0:2]
        self._oLine->SetProperty, COLOR=color[0:2]
        if (self._useColor) then begin
          self._oPoly->SetProperty, COLOR=color[0:2]
          self._fillColor = color[0:2]
        endif
      endelse

    endif

    
    if (N_ELEMENTS(fillcolorIn) gt 0) then begin
      isScalar = ISA(fillColorIn, /SCALAR, /NUMBER)
      style_convert, fillColorIn, COLOR=fillColor
      if (~ISA(fillColor, /ARRAY) || MAX(fillColor lt 0) eq 1) then $
        fillColor = self._color
      nd = SIZE(fillColor, /N_DIM)
      dim = SIZE(fillColor, /DIM)
      if (nd eq 1 && dim[0] ne 3) then $
        MESSAGE, 'FILL_COLOR must be a color name or a 3-element array.'
      if (nd eq 2 && dim[0] ne 3) then $
        MESSAGE, 'FILL_COLOR must be a string array of colors names or a 3xN array.'

      if (nd eq 2) then begin
        PTR_FREE, self._pFillColor
        self._pFillColor = PTR_NEW(fillColor)
        self->_UpdateArrow
      endif else begin
        ; If we used to have an array of colors, clear out our pointer
        ; and reset the IDLgrPolygon property.
        if PTR_VALID(self._pFillColor) then begin
          PTR_FREE, self._pFillColor
        endif
        self._fillColor = fillColor[0:2]
        ; If the fill color is the same as the line color,
        ; assume the fill color is not actually being set differently.
        self._useColor = ARRAY_EQUAL(self._fillcolor, self._color)
        self._oPoly->SetProperty, COLOR=fillColor[0:2]
        ; If we got a scalar, assume our fill colors should match our line color.
        if (self._useColor && isScalar) then begin
          self._oLine->GetProperty, VERT_COLORS=vertColors
          self._oPoly->SetProperty, VERT_COLORS=vertColors
        endif else begin
          ; Otherwise, if we got an RGB triplet, turn off vert colors
          ; and make all of the fill colors the same.
          self._oPoly->SetProperty, VERT_COLORS=0
        endelse
      endelse

    endif


    if (N_ELEMENTS(headAngle) gt 0 && headAngle ge 0) then begin
        if(headAngle gt 90) then headAngle = 30
        self._headAngle = ABS(headAngle)
        self->_UpdateData, WITHIN_DRAW=withinDraw
    endif

    if (N_ELEMENTS(headIndent) gt 0) then begin
        self._headIndent = -1 > DOUBLE(headIndent) < 1
        self->_UpdateData, WITHIN_DRAW=withinDraw
    endif
    
    nhead = N_ELEMENTS(headSize)
    if (nhead gt 0) then begin
      PTR_FREE, self._pHeadSize
      if (nhead gt 1) then begin
        self._pHeadSize = PTR_NEW(headSize)
      endif else begin
        if DOUBLE(headSize) lt 0 then headSize = 1
        self._headSize = headSize
      endelse
      self->_UpdateData, WITHIN_DRAW=withinDraw
    endif

    if (N_ELEMENTS(transparency)) then begin
        alpha = 0 > ((100.-transparency)/100) < 1
        self._oPoly->GetProperty, ALPHA=oldFillAlpha
        self._oLine->GetProperty, ALPHA=oldLineAlpha
        self._oLine->SetProperty, ALPHA_CHANNEL=alpha
        if (oldFillAlpha eq oldLineAlpha) then $
          self._oPoly->SetProperty, ALPHA_CHANNEL=alpha
    endif

    if (N_ELEMENTS(fillTransparency)) then begin
        alpha = 0 > ((100.-fillTransparency)/100) < 1
        self._oPoly->SetProperty, ALPHA_CHANNEL=alpha
    endif

    if (N_ELEMENTS(lineThick) gt 0) then begin
        self._oLine->SetProperty, THICK=1 > DOUBLE(lineThick) < 10
        self._oPoly->SetProperty, THICK=1 > DOUBLE(lineThick) < 10
    endif


    if (N_ELEMENTS(zValue) gt 0) then begin
        self._zValue = zValue
        self->_UpdateData, WITHIN_DRAW=withinDraw
        ;self->Set3D, (self._zValue ne 0), /ALWAYS
        self->OnDataChange, self
        self->OnDataComplete, self
    endif

    if (N_ELEMENTS(data) gt 0) then begin
       *self._pXdata = data[0,*]
       *self._pYdata = data[1,*]
       *self._pZdata = data[2,*]
        oDataObj = self->GetParameter('VERTICES')
        if (~OBJ_VALID(oDataObj)) then begin
            oDataObj = OBJ_NEW("IDLitData", data, /NO_COPY, $
                NAME='Vertices', $
                TYPE='IDLVERTEX', ICON='segpoly', /PRIVATE)
;            void = self->SetData(oDataObj, $
;                PARAMETER_NAME= 'VERTICES', /BY_VALUE)
            status = self->IDLitParameter::SetData(oDataObj, PARAMETER_NAME= 'VERTICES', /BY_VALUE)

        endif else begin
            void = oDataObj->SetData(data, /NO_COPY)
        endelse
      self->_UpdateData
    endif

    ; Set superclass properties
    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisualization::SetProperty,_EXTRA=_extra
end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to retrieve the data
;
; Arguments:
;   X, Y
;
; Keywords:
;   NONE
;
pro IDLitVisArrow::GetData, x, y, _REF_EXTRA=_extra
  compile_opt idl2, hidden

  if (n_params() ne 2) then $
    MESSAGE, 'Incorrect number of arguments.'
    
  ; Get the endpoints in data coordinates
  x = reform(*self._pXdata)
  y = reform(*self._pYdata)

  self->GetProperty, _PARENT=oParent, DATA=dataIn
  if (OBJ_ISA(oParent, 'IDLitgrAnnotateLayer')) then begin
    ; Return normalized coordinates if in the annotation layer
    data = iConvertCoord(x, y, /ANNOTATION_DATA, /TO_NORMAL)
    x = data[0,*]
    y = data[1,*]
  end

  narrows = N_ELEMENTS(x)/2
  if (narrows gt 1) then begin
    x = REFORM(x, 2, narrows, /OVERWRITE)
    y = REFORM(y, 2, narrows, /OVERWRITE)
  endif
  ; Get additional keyword property
;  self->GetProperty, _EXTRA=_extra

end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to directly set the data
;
; Arguments:
;   Varies
;
; Keywords:
;   NONE
;
pro IDLitVisArrow::_SetData, x, y, z, _EXTRA=_extra
  compile_opt idl2, hidden

  nx = N_ELEMENTS(x)
  nparams = n_params()
  if (nparams lt 2) then $
    MESSAGE, 'Incorrect number of arguments.'

  sizeX = SIZE(x, /DIM)
  sizeY = SIZE(y, /DIM)
  sizeZ = SIZE(z, /DIM)
  if (sizeX[0] ne 2) then $
    MESSAGE, 'X must be a two-element vector or a [2,n] array.'
  if (sizeY[0] ne 2) then $
    MESSAGE, 'Y must be a two-element vector or a [2,n] array.'
  if (nparams eq 3 && sizeZ[0] ne 2) then $
    MESSAGE, 'Z must be a two-element vector or a [2,n] array.'

  ;; Ensure data is in proper format
  self->GetProperty, _PARENT=oParent
  if (OBJ_ISA(oParent, 'IDLitgrAnnotateLayer')) then begin
    data = iConvertCoord(x, y, /NORMAL, /TO_ANNOTATION_DATA)
    data[2,*] = 0.99d
  endif else begin
    ;; Reset the transform so that incoming data is properly reflected
    self->SetProperty, TRANSFORM=Identity(4)
    data = DBLARR(3, N_ELEMENTS(x))
    data[0,*] = x
    data[1,*] = y
    if (nparams eq 3) then data[2,*] = z
  endelse
  
  ; Change the Data
  *self._pXdata = data[0,*]
  *self._pYdata = data[1,*]
  *self._pZdata = data[2,*]

  self->_UpdateData, _EXTRA=_extra

  oTool = self->GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->RefreshCurrentWindow

end


;----------------------------------------------------------------------------
; Purpose:
;   Internal method to update the visualization.
;
pro IDLitVisArrow::_UpdateArrow, xRange, yRange

    compile_opt idl2, hidden

    if (*self._pXdata eq !null) then return
   
    sMap = self->GetProjection()
    if (N_TAGS(sMap) ne 0) then begin
      points = MAP_PROJ_FORWARD(*self._pXdata, *self._pYdata, $
        MAP_STRUCTURE=sMap)
    endif else begin
      points = [*self._pXdata, *self._pYdata]
    endelse
    points = [points, *self._pZdata]
 

    ; First determine the tip and end points.
    ; Handle multiple arrows with the stride 0:*:2
    switch self._arrowStyle of
    0: ; fall through
    1: ; fall through
    3: ; fall through
    4: begin
      xEnd = REFORM(points[0,0:*:2])
      yEnd = REFORM(points[1,0:*:2])
      xTip = REFORM(points[0,1:*:2])
      yTip = REFORM(points[1,1:*:2])
      break
      end
    2: ; fall through
    5: begin
      ; arrowhead is at the first point instead of the second
      xEnd = REFORM(points[0,1:*:2])
      yEnd = REFORM(points[1,1:*:2])
      xTip = REFORM(points[0,0:*:2])
      yTip = REFORM(points[1,0:*:2])
      break
      end
    else: MESSAGE, 'Invalid arrow_style'
    endswitch

    narrows = N_ELEMENTS(xTip)

    hasZvalue = self._zValue ne 0
 
    headSizeOrig = PTR_VALID(self._pHeadSize) ? *self._pHeadSize : self._headSize
    nhead = N_ELEMENTS(headSizeOrig)
    if (nhead gt 1) then begin
      if (nhead lt narrows) then begin
        headSizeOrig = headSizeOrig[LINDGEN(narrows) mod nhead]
      endif else if (nhead gt narrows) then begin
        headSizeOrig = headSizeOrig[0:narrows-1]
      endif
    endif

    headSize = 0.06d*headSizeOrig
    xyScale = self._lengthX/self._lengthY
    rCosTheta = (self._headAngle eq 90) ? $
      0 : headSize*COS(self._headAngle*!DPI/180)
    rSinTheta = (self._headAngle eq 0) ? $
      0 : headSize*SIN(self._headAngle*!DPI/180)
 
    oDataspace = self->GetDataspace(/UNNORMALIZED)
    isWithinDataspace = oDataspace->GetXYZRange(xDataRange, yDataRange, zDataRange)

    ; Compute some generic locations, regardless of arrow style.
    xdelta = xTip - xEnd
    ydelta = yTip - yEnd  

    if (isWithinDataspace) then begin
      oDataspace->GetProperty, XLOG=xlog, YLOG=ylog
      if (xlog) then begin
        xTip = ALOG10(xTip)
        xEnd = ALOG10(xEnd)
        xdelta = xTip - xEnd
      endif
      if (ylog)  then begin
        yTip = ALOG10(yTip)
        yEnd = ALOG10(yEnd)
        ydelta = yTip - yEnd
      endif
      ; If we are within the dataspace, convert our arrow length back to
      ; screen (pixel) coordinates, so we can normalize the arrow head size.
      self->VisToWindow, [xEnd, xTip], [yEnd, yTip], xPixels, yPixels
      pixelMag = SQRT((xPixels[narrows:*]-xPixels[0:narrows-1])^2 + $
        (yPixels[narrows:*]-yPixels[0:narrows-1])^2)
      magnitude = pixelMag/280d
    endif else begin
      ; If we are in the annotation layer, just normalize the head size
      ; by our arrow length.
      magnitude = sqrt(xdelta * xdelta + ydelta * ydelta)
    endelse

    xdelta = xdelta/magnitude
    ydelta = ydelta/magnitude  

    ; Note that we need to apply the aspect ratio to the
    ; tips of the arrowhead, since they are off the shaft.
    dHeadX = (xyScale*rSinTheta)*ydelta
    dHeadY = ((1/xyScale)*rSinTheta)*xdelta
    
    
    thick = PTR_VALID(self._pThick) ? *self._pThick : self._thick
    nthick = N_ELEMENTS(headSizeOrig)
    if (nthick gt 1) then begin
      if (nthick lt narrows) then begin
        thick = thick[LINDGEN(narrows) mod nthick]
      endif else if (nthick gt narrows) then begin
        thick = thick[0:narrows-1]
      endif
    endif

    ; These will already have the aspect ratio factor applied.
    fracThick = (thick-1)/9d
    xShaftThick = fracThick*dHeadX
    yShaftThick = fracThick*dHeadY

    ; Intersection of the back of the arrowhead with the middle of the shaft.
    xTipMid = xTip - (1 - self._headIndent)*rCosTheta*xdelta
    yTipMid = yTip - (1 - self._headIndent)*rCosTheta*ydelta
    
    ; The end of the barbs
    xTipBarb1 = xTip - dHeadX - rCosTheta*xdelta
    yTipBarb1 = yTip + dHeadY - rCosTheta*ydelta
    xTipBarb2 = xTip + dHeadX - rCosTheta*xdelta
    yTipBarb2 = yTip - dHeadY - rCosTheta*ydelta


    ; Now construct the actual polygon points.
    switch self._arrowStyle of
    0: begin
      p = 5
      polyvert = DBLARR(2 + hasZvalue, p*narrows, /NOZERO)
      if (hasZvalue) then polyvert[2, *] = self._zValue
      
      polyvert[0, 0:*:p] = xTip - xShaftThick
      polyvert[1, 0:*:p] = yTip + yShaftThick
      polyvert[0, 1:*:p] = xTip + xShaftThick
      polyvert[1, 1:*:p] = yTip - yShaftThick
      polyvert[0, 2:*:p] = xEnd + xShaftThick
      polyvert[1, 2:*:p] = yEnd - yShaftThick
      polyvert[0, 3:*:p] = xEnd - xShaftThick
      polyvert[1, 3:*:p] = yEnd + yShaftThick
      polyvert[0, 4:*:p] = xTip - xShaftThick
      polyvert[1, 4:*:p] = yTip + yShaftThick
      
      ; Construct array of nElts polygons, each with p vertices.
      lindgn = p*LINDGEN(narrows)
      polygons = LONARR((p+1)*narrows)
      polygons[0:*:(p+1)] = p
      for i=0,p-1 do polygons[i+1:*:(p+1)] = i + lindgn
      polylines = polygons
      break
    end

    1: ; opposite of arrow_style=2, with x/yTip and x/yEnd reversed, so fall through
    
    2: begin
      p = 8
      polyvert = DBLARR(2 + hasZvalue, p*narrows, /NOZERO)
      if (hasZvalue) then polyvert[2, *] = self._zValue
    
      polyvert[0, 0:*:p] = xTip
      polyvert[1, 0:*:p] = yTip
      polyvert[0, 1:*:p] = xTipBarb1
      polyvert[1, 1:*:p] = yTipBarb1
      polyvert[0, 2:*:p] = xTipMid*(1-fracThick) + fracThick*xTipBarb1
      polyvert[1, 2:*:p] = yTipMid*(1-fracThick) + fracThick*yTipBarb1
      polyvert[0, 3:*:p] = xEnd - xShaftThick
      polyvert[1, 3:*:p] = yEnd + yShaftThick
      polyvert[0, 4:*:p] = xEnd + xShaftThick
      polyvert[1, 4:*:p] = yEnd - yShaftThick
      polyvert[0, 5:*:p] = xTipMid*(1-fracThick) + fracThick*xTipBarb2
      polyvert[1, 5:*:p] = yTipMid*(1-fracThick) + fracThick*yTipBarb2
      polyvert[0, 6:*:p] = xTipBarb2
      polyvert[1, 6:*:p] = yTipBarb2
      polyvert[0, 7:*:p] = xTip
      polyvert[1, 7:*:p] = yTip
      
      ; Construct array of nElts polygons, each with p vertices.
      lindgn = p*LINDGEN(narrows)
      polygons = LONARR((p+1)*narrows)
      polygons[0:*:(p+1)] = p
      for i=0,p-1 do polygons[i+1:*:(p+1)] = i + lindgn
      plines = [2,3,4,5,6,0,1,2]
      polylines = LONARR((p+1)*narrows)
      polylines[0:*:(p+1)] = p
      for i=0,p-1 do polylines[i+1:*:(p+1)] = plines[i] + lindgn
      break
    end

    3: begin
      ; Compute the other arrowhead.
      xEndMid = xEnd + (1 - self._headIndent)*rCosTheta*xdelta
      yEndMid = yEnd + (1 - self._headIndent)*rCosTheta*ydelta
      xEndBarb1 = xEnd - dHeadX + rCosTheta*xdelta
      yEndBarb1 = yEnd + dHeadY + rCosTheta*ydelta
      xEndBarb2 = xEnd + dHeadX + rCosTheta*xdelta
      yEndBarb2 = yEnd - dHeadY + rCosTheta*ydelta
  
      p = 11
      polyvert = DBLARR(2 + hasZvalue, p*narrows, /NOZERO)
      if (hasZvalue) then polyvert[2, *] = self._zValue

      polyvert[0, 0:*:p] = xTip
      polyvert[1, 0:*:p] = yTip
      polyvert[0, 1:*:p] = xTipBarb1
      polyvert[1, 1:*:p] = yTipBarb1
      polyvert[0, 2:*:p] = xTipMid*(1-fracThick) + fracThick*xTipBarb1
      polyvert[1, 2:*:p] = yTipMid*(1-fracThick) + fracThick*yTipBarb1
      polyvert[0, 3:*:p] = xEndMid*(1-fracThick) + fracThick*xEndBarb1
      polyvert[1, 3:*:p] = yEndMid*(1-fracThick) + fracThick*yEndBarb1
      polyvert[0, 4:*:p] = xEndBarb1
      polyvert[1, 4:*:p] = yEndBarb1
      polyvert[0, 5:*:p] = xEnd
      polyvert[1, 5:*:p] = yEnd
      polyvert[0, 6:*:p] = xEndBarb2
      polyvert[1, 6:*:p] = yEndBarb2
      polyvert[0, 7:*:p] = xEndMid*(1-fracThick) + fracThick*xEndBarb2
      polyvert[1, 7:*:p] = yEndMid*(1-fracThick) + fracThick*yEndBarb2
      polyvert[0, 8:*:p] = xTipMid*(1-fracThick) + fracThick*xTipBarb2
      polyvert[1, 8:*:p] = yTipMid*(1-fracThick) + fracThick*yTipBarb2
      polyvert[0, 9:*:p] = xTipBarb2
      polyvert[1, 9:*:p] = yTipBarb2
      polyvert[0, 10:*:p] = xTip
      polyvert[1, 10:*:p] = yTip
      
      lindgn = p*LINDGEN(narrows)
      plines = [2,3,4,5,6,7,8,9,0,1,2]
      polylines = LONARR((p+1)*narrows)
      polylines[0:*:(p+1)] = p
      for i=0,p-1 do polylines[i+1:*:(p+1)] = plines[i] + lindgn

      if(self._fillBackground eq 0) then begin
        ; Construct array of nElts polygons, each with p vertices.
        polygons = LONARR((p+1)*narrows)
        polygons[0:*:(p+1)] = p
        for i=0,p-1 do polygons[i+1:*:(p+1)] = i + lindgn
      endif else begin
        np = 19
        polygons = LONARR(np*narrows)

        pgons = [0, 1, 2, 8, 9, 10]
        polygons[0:*:np] = 6
        for i=0,5 do polygons[i+1:*:np] = pgons[i] + lindgn

        pgons = [2, 3, 7, 8]
        polygons[7:*:np] = 4
        for i=0,3 do polygons[i+8:*:np] = pgons[i] + lindgn

        pgons = [5, 4, 3, 7, 6, 5]
        polygons[12:*:np] = 6
        for i=0,5 do polygons[i+13:*:np] = pgons[i] + lindgn
      endelse
      break
    end

    4: ; opposite of arrow_style=5, with x/yTip and x/yEnd reversed, so fall through

    5: begin
      ; Compute the tail (the fletching) of the arrow
      xEndMid = xEnd + (1 - self._headIndent)*rCosTheta*xdelta
      yEndMid = yEnd + (1 - self._headIndent)*rCosTheta*ydelta
      xEndBarb1 = xEnd - dHeadX - self._headIndent*rCosTheta*xdelta
      yEndBarb1 = yEnd + dHeadY - self._headIndent*rCosTheta*ydelta
      xEndBarb2 = xEnd + dHeadX - self._headIndent*rCosTheta*xdelta
      yEndBarb2 = yEnd - dHeadY - self._headIndent*rCosTheta*ydelta

      p = 11
      polyvert = DBLARR(2 + hasZvalue, p*narrows, /NOZERO)
      if (hasZvalue) then polyvert[2, *] = self._zValue

      polyvert[0, 0:*:p] = xTip
      polyvert[1, 0:*:p] = yTip
      polyvert[0, 1:*:p] = xTipBarb1
      polyvert[1, 1:*:p] = yTipBarb1
      polyvert[0, 2:*:p] = xTipMid*(1-fracThick) + fracThick*xTipBarb1
      polyvert[1, 2:*:p] = yTipMid*(1-fracThick) + fracThick*yTipBarb1

      ; Compute the intersection of the shaft lines with the tail feather
      polyvert[0, 3:*:p] = xEndMid*(1 - fracThick) + fracThick*xEndBarb1
      polyvert[1, 3:*:p] = yEndMid*(1 - fracThick) + fracThick*yEndBarb1

      polyvert[0, 4:*:p] = xEndBarb1
      polyvert[1, 4:*:p] = yEndBarb1
      polyvert[0, 5:*:p] = xEnd
      polyvert[1, 5:*:p] = yEnd
      polyvert[0, 6:*:p] = xEndBarb2
      polyvert[1, 6:*:p] = yEndBarb2

      polyvert[0, 7:*:p] = xEndMid*(1 - fracThick) + fracThick*xEndBarb2
      polyvert[1, 7:*:p] = yEndMid*(1 - fracThick) + fracThick*yEndBarb2

      polyvert[0, 8:*:p] = xTipMid*(1-fracThick) + fracThick*xTipBarb2
      polyvert[1, 8:*:p] = yTipMid*(1-fracThick) + fracThick*yTipBarb2
      polyvert[0, 9:*:p] = xTipBarb2
      polyvert[1, 9:*:p] = yTipBarb2
      polyvert[0, 10:*:p] = xTip
      polyvert[1, 10:*:p] = yTip
      
      lindgn = p*LINDGEN(narrows)
      plines = [2,3,4,5,6,7,8,9,0,1,2]
      polylines = LONARR((p+1)*narrows)
      polylines[0:*:(p+1)] = p
      for i=0,p-1 do polylines[i+1:*:(p+1)] = plines[i] + lindgn

      if(self._fillBackground eq 0) then begin
        ; Construct array of nElts polygons, each with p vertices.
        polygons = LONARR((p+1)*narrows)
        polygons[0:*:(p+1)] = p
        for i=0,p-1 do polygons[i+1:*:(p+1)] = i + lindgn
      endif else begin
        polygons = [6, 0, 1, 2, 8, 9, 10, 6, 2, 3, 5, 7, 8, 2, 4, 3, 4, 5, 3, 4, 5, 6, 7, 5]
        np = 24
        polygons = LONARR(np*narrows)

        pgons = [0, 1, 2, 8, 9, 10]
        polygons[0:*:np] = 6
        for i=0,5 do polygons[i+1:*:np] = pgons[i] + lindgn

        pgons = [2, 3, 5, 7, 8, 2]
        polygons[7:*:np] = 6
        for i=0,5 do polygons[i+8:*:np] = pgons[i] + lindgn

        pgons = [3, 4, 5, 3]
        polygons[14:*:np] = 4
        for i=0,3 do polygons[i+15:*:np] = pgons[i] + lindgn

        pgons = [5, 6, 7, 5]
        polygons[19:*:np] = 4
        for i=0,3 do polygons[i+20:*:np] = pgons[i] + lindgn
      endelse
      break
    end

    else:MESSAGE, 'Invalid arrow_style'
    endswitch


    if PTR_VALID(self._pColor) then begin
      dim = SIZE(*self._pColor, /DIM)
      if (N_ELEMENTS(dim) eq 2 && dim[0] eq 3) then begin
        ; Construct a 3xM RGB array, for "n" arrows each with "p" vertices.
        ;  For example for 4 arrows with p=5:
        ;    0,0,0,0,0, 1,1,1,1,1, 2,2,2,2,2, 3,3,3,3,3
        ; If there are less colors than arrows, then the "mod"
        ; makes it cyclically repeat the colors.
        index = (LINDGEN(p*narrows)/p) mod dim[1]
        lineColors = (*self._pColor)[*,index]
      endif
      if (self._useColor && ~PTR_VALID(self._pFillColor)) then begin
        polyColors = lineColors
      endif
    endif

    if PTR_VALID(self._pFillColor) then begin
      dim = SIZE(*self._pFillColor, /DIM)
      if (N_ELEMENTS(dim) eq 2 && dim[0] eq 3) then begin
        ; We may already have the index from above.
        ; Also see comment above regarding this index array.
        if (~ISA(index)) then $
          index = (LINDGEN(p*narrows)/p) mod dim[1]
        polyColors = (*self._pFillColor)[*,index]
        polyColors = REBIN(polyColors, 3, p*narrows)
      endif
    endif

    self._oPoly->SetProperty, DATA=polyvert, $
        POLYGONS=polygons, $
        HIDE=(self._fillBackground eq 0), $
        STYLE=(self._fillBackground eq 0) ? 1 : 2, $
        VERT_COLORS=polyColors

    self._oLine->SetProperty, DATA=polyvert, $
        LABEL_OBJECTS=OBJ_NEW(), $
        LABEL_POLYLINES=0, $
        POLYLINES=polylines, $
        VERT_COLORS=lineColors

    ; If we have multiple arrows, just use a generic selection box
    ; for our manip vis. For a single arrow use the vertex select vis.
    oSelVis = self->GetDefaultSelectionVisual()
    if (narrows gt 1) then begin
      if ISA(oSelVis, 'IDLitManipVisVertex') then begin
        self->SetDefaultSelectionVisual, $
          OBJ_NEW('IDLitManipVisSelectBox', /HIDE)
      endif
    endif else begin
      if ~ISA(oSelVis, 'IDLitManipVisVertex') then begin
        self->SetDefaultSelectionVisual, $
          OBJ_NEW('IDLitManipVisVertex', /HIDE, PREFIX='LINE')
      endif
    endelse


end


;----------------------------------------------------------------------------
; Purpose:
;   Internal method to update the visualization.
;
; Keywords:
;   MAP_PROJECTION: An optional input giving the map projection structure.
;       This is provided for calling convenience. If MAP_PROJECTION is not
;       provided it will be retrieved.
;   SUBSAMPLE: If set then this method is being called because
;       the View Zoom has changed. In this case much of the internals
;       can be skipped.
;   WITHIN_DRAW: If set then this method is being called from ::Draw,
;       and we don't want to do any special data notification.
;
pro IDLitVisArrow::_UpdateData, $
    MAP_PROJECTION=sMap, $
    WITHIN_DRAW=withinDraw

    compile_opt idl2, hidden

    withinDraw = KEYWORD_SET(withinDraw)
 
    self->_UpdateArrow

    ; If we are in the annotation layer, hide our ZVALUE property.
    self->GetProperty, _PARENT=oParent
    if (OBJ_ISA(oParent, 'IDLitgrAnnotateLayer')) then begin
      self->SetPropertyAttribute, 'ZVALUE', /HIDE
    endif

    if (~withinDraw) then begin
        self->OnDataChange
        self->OnDataComplete
    endif
end


;----------------------------------------------------------------------------
pro IDLitVisArrow::OnProjectionChange, sMap

    compile_opt idl2, hidden

    self->_UpdateData, MAP_PROJECTION=sMap

end


;----------------------------------------------------------------------------
pro IDLitVisArrow::OnDataRangeChange, oSubject, XRange, YRange, ZRange

  compile_opt idl2, hidden

  self->_IDLitVisualization::ClipToDataRange, XRange, YRange, ZRange
end


;----------------------------------------------------------------------------
; PURPOSE:
;   This procedure method handles notification that the dimensionality
;   of the parent world has changed.
;
pro IDLitVisArrow::OnWorldDimensionChange, oSubject, is3D

    compile_opt idl2, hidden

    ; If the world changes to 3D, the isotropic setting is
    ; not as relevant.  Turn it off in this case so that if
    ; it gets added to a 3D world (that is not isotropic), the
    ; scaling remains consistent.
    ; In a 2D world, isotropic scaling is used for map data.
    wantIso = self._gridUnits eq 1 || self._gridUnits eq 2
    self->IDLitVisualization::SetProperty, ISOTROPIC=~is3D && wantIso

    ; Call superclass.
    self->IDLitVisualization::OnWorldDimensionChange, oSubject, is3D
end

;-----------------------------------------------------------------------------
; Calculate the X/Y aspect ratio.
; Returns 1 if the aspect ratio has changed, 0 otherwise.
;
function IDLitVisArrow::_ComputeAspect

    compile_opt idl2, hidden

    ; Find out if aspect ratio is still correct
    self->WindowToVis, [[0,0], [1d,0]], onePixel
    lengthX = ABS(onePixel[0,1] - onePixel[0,0])
    if (lengthX lt 1d-9) then $
        return, 0
    self->WindowToVis, [[0,0], [0,1d]], onePixel
    lengthY = ABS(onePixel[1,1] - onePixel[1,0])
    if (lengthY lt 1d-9) then $
        return, 0
    xyScale = lengthX/lengthY
    prevScale = self._lengthX/self._lengthY
    newAspect = ABS(xyScale - prevScale) gt 1d-4*ABS(xyScale)
    self._lengthX = lengthX
    self._lengthY = lengthY
    
    return, newAspect
end

;-----------------------------------------------------------------------------
; Override IDLgrModel::Draw so we can
; automatically adjust for changes in aspect ratio.
;
pro IDLitVisArrow::Draw, oDest, oView

    compile_opt idl2, hidden

    ; Don't do extra work if we are in the lighting or selection pass.
    oDest->GetProperty, IS_BANDING=isBanding, $
        IS_LIGHTING=isLighting, IS_SELECTING=isSelecting

    if (~isLighting && ~isSelecting && ~isBanding) then begin
        if (self->_ComputeAspect()) then begin
            self->_UpdateData, /WITHIN_DRAW
        endif
    endif

    self->IDLitVisualization::Draw, oDest, oView
end


;-----------------------------------------------------------------------------
pro IDLitVisArrow::MoveVertex, xyz, INDEX=index, WINDOW=WINDOW

    compile_opt hidden, idl2

    ; Retrieve the data pointer and check the indices.
    if (~self->_IDLitVisVertex::_CheckVertex(oDataObj, pData, index)) then $
        return

    ; Number of vertices.
    ptsStored = (SIZE(*pData, /N_DIM) eq 1) ? 1 : $
        (SIZE(*pData, /DIMENSIONS))[1]

    nDim = (SIZE(xyz, /DIMENSIONS))[0]

    if(keyword_set(WINDOW))then $
        self->_IDLitVisualization::WindowToVis, xyz, visXYZ $
    else $
        visXYZ = xyz

    ; Note that we are directly modifying the data pointer.
    (*pData)[0:nDim-1, index] = visXYZ

    ; If only 2D vertices, then zero out the Z values.
    if (nDim eq 2) then $
        (*pData)[2, index] = 0

    ; Notify our observers if we have enough points.
    if (ptsStored ge self._ptsNeeded) then begin
        oDataObj->NotifyDataChange
        oDataObj->NotifyDataComplete
    endif
    *self._pXdata = (*pData)[0,*]
    *self._pYdata = (*pData)[1,*]
    
    self->_UpdateData

end

;----------------------------------------------------------------------------
; IDLitVisArrow__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisArrow object.
;
pro IDLitVisArrow__Define

    compile_opt idl2, hidden

    struct = { IDLitVisArrow,              $
        inherits IDLitVisualization,  $ ; Superclass: _IDLitVisualization
        inherits _IDLitVisVertex, $
        _oLine: OBJ_NEW(), $
        _oPoly: OBJ_NEW(), $
        _pXdata: PTR_NEW(), $
        _pYdata: PTR_NEW(), $
        _pZdata: PTR_NEW(), $
        _pColor: PTR_NEW(), $
        _pFillColor: PTR_NEW(), $
        _pHeadSize: PTR_NEW(), $
        _pThick: PTR_NEW(), $
        _color: [0b,0b,0b], $
        _fillColor: [0b,0b,0b], $
        _gridUnits: 0b, $
        _arrowStyle: 0b, $
        _fillBackground: 0b, $
        _useColor:0b, $
        _thick: 0d, $
        _lengthX: 0d, $
        _lengthY: 0d, $
        _headAngle: 0d, $
        _headIndent: 0d, $
        _headSize:0d, $
        _zValue: 0d $
    }
end
