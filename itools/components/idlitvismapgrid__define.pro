; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvismapgrid__define.pro#2 $
;
; Copyright (c) 2004-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLitVisMapGrid
;
; PURPOSE:
;    The IDLitVisMapGrid class implements a a polyline visualization
;    object for the iTools system.
;
; CATEGORY:
;    Components
;
; SUPERCLASSES:
;   IDLitVisualization
;
;-


;----------------------------------------------------------------------------
function IDLitVisMapGrid::Init, TOOL=oTool, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Initialize superclass
    if (~self->IDLitVisualization::Init(NAME="Map Grid", $
        TYPE="IDLMAPGRID", $
        TOOL=oTool, $
        /ISOTROPIC, $
        ICON='axis', $
        DESCRIPTION="Map grid",$
        _EXTRA=_EXTRA))then $
        return, 0

    ; Request no axes.
    self->SetAxesRequest, 0, /ALWAYS

    self->_RegisterProperties

    self._gridLongitude = 30
    self._gridLatitude = 15

    self._autoGrid = 1b

    self->_AddLineContainers
      
    self._oBox = OBJ_NEW( 'IDLitvisMapBoxAxes', self, NAME='Box Axes', HIDE=1, $
      IMPACTS_RANGE=0, MANIPULATOR_TARGET=0 )
      
;    self._oBox->SetPropertyAttribute, $
;      ['ARROW_STYLE', 'ARROW_SIZE', 'ZVALUE'], /HIDE
    self->Add, self._oBox, /AGGREGATE, /NO_UPDATE, /NO_NOTIFY

    if (N_ELEMENTS(_extra) gt 0) then $
      self->IDLitVisMapGrid::SetProperty, _EXTRA=_extra

    return, 1
end


;----------------------------------------------------------------------------
;pro IDLitVisMapGrid::Cleanup
;    compile_opt idl2, hidden
;    ; Cleanup superclass
;    self->IDLitVisualization::Cleanup
;end


;----------------------------------------------------------------------------
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLitVisMapGrid::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

    compile_opt idl2, hidden

    registerAll = ~KEYWORD_SET(updateFromVersion)

    ; Property added in IDL62.
    if (registerAll || (updateFromVersion lt 620)) then begin
        self->RegisterProperty, 'AUTO_GRID', /BOOLEAN, /ADVANCED_ONLY, $
            NAME='Automatic grid', $
            DESCRIPTION='Automatically compute the grid range and spacing'
    endif

    ; Property added in IDL 8.2.2
    if (registerAll || (updateFromVersion lt 822)) then begin
      if (~registerAll && updateFromVersion lt 822) then begin
        self->SetPropertyAttribute, 'HORIZON', /HIDE
      endif
      self->RegisterProperty, 'HORIZON_LINESTYLE', /LINESTYLE, $
        NAME='Horizon linestyle', $
        DESCRIPTION='Draw the current map horizon'
      self->RegisterProperty, 'HORIZON_COLOR', /COLOR, $
        NAME='Horizon color', $
        DESCRIPTION='Horizon color'
      self->RegisterProperty, 'HORIZON_THICK', /THICK, $
        NAME='Horizon thickness', $
        DESCRIPTION='Horizon thickness'
    endif
    
    if (registerAll) then begin

        self->RegisterProperty, 'BOX_AXES', /BOOLEAN, $
            NAME='Box Axes', $
            DESCRIPTION='Display a box around the map area'

        self->RegisterProperty, 'LONGITUDE_MIN', /FLOAT, $
            NAME='Longitude minimum (deg)', $
            VALID_RANGE=[-360,360], $
            DESCRIPTION='Minimum longitude to include in projection (degrees)'

        self->RegisterProperty, 'LONGITUDE_MAX', /FLOAT, $
            NAME='Longitude maximum (deg)', $
            VALID_RANGE=[-360,360], $
            DESCRIPTION='Maximum longitude to include in projection (degrees)'

        self->RegisterProperty, 'LATITUDE_MIN', /FLOAT, $
            NAME='Latitude minimum (deg)', $
            VALID_RANGE=[-90,90], $
            DESCRIPTION='Minimum latitude to include in projection (degrees)'

        self->RegisterProperty, 'LATITUDE_MAX', /FLOAT, $
            NAME='Latitude maximum (deg)', $
            VALID_RANGE=[-90,90], $
            DESCRIPTION='Maximum latitude to include in projection (degrees)'

        self->RegisterProperty, 'GRID_LONGITUDE', /FLOAT, $
            NAME='Longitude spacing', $
            DESCRIPTION='Longitude grid spacing in degrees', $
            VALID_RANGE=[0,360], /ADVANCED_ONLY

        self->RegisterProperty, 'GRID_LATITUDE', /FLOAT, $
            NAME='Latitude spacing', $
            DESCRIPTION='Latitude grid spacing in degrees', $
            VALID_RANGE=[0,360], /ADVANCED_ONLY

        self->RegisterProperty, 'EDIT_LONGITUDES', USERDEF='Click to edit', $
            NAME='Longitude lines', $
            DESCRIPTION='Edit individual longitude lines', /ADVANCED_ONLY

        self->RegisterProperty, 'EDIT_LATITUDES', USERDEF='Click to edit', $
            NAME='Latitude lines', $
            DESCRIPTION='Edit individual latitude lines', /ADVANCED_ONLY

    endif

end


;----------------------------------------------------------------------------
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisMapGrid::Restore

    compile_opt idl2, hidden

    ; Call superclass restore.
    self->IDLitVisualization::Restore

    ; Register new properties.
    self->IDLitVisMapGrid::_RegisterProperties, $
        UPDATE_FROM_VERSION=self.idlitcomponentversion

    ; In IDL64 we switched to always impact the range.
    if (self.idlitcomponentversion lt 640) then $
        self->SetProperty, /IMPACTS_RANGE
end


;----------------------------------------------------------------------------
pro IDLitVisMapGrid::_AddLineContainers

    compile_opt idl2, hidden


    for i=0,1 do begin
        self._oLineContainer[i] = OBJ_NEW('IDLitVisMapGridContainer', $
            CLIP=0, $
            NAME=(['Longitudes','Latitudes'])[i], $
            /PROPERTY_INTERSECTION)
        self._oLineContainer[i]->SetPropertyAttribute, $
            ['NAME', 'DESCRIPTION', 'HIDE'], /HIDE
        self->Add, self._oLineContainer[i], /AGGREGATE, $
            /NO_UPDATE, /NO_NOTIFY
    endfor

    ; Add one grid line so we pick up all the aggregated properties.
    self._limit = [0,0,0,0]  ; temporarily reset
    self->_UpdateGridlines, 0, sMap
    self->_UpdateGridlines, 1, sMap
    self._limit = [-90,-180,90,540]

end


;----------------------------------------------------------------------------
; This method is only needed to keep the filled background polygon in the
; correct position within the dataspace container.
;
pro IDLitVisMapGrid::OnNotify, id, message, data
  compile_opt hidden, idl2
  
  if (message eq 'MOVEITEMS' && ISA(self._oFill) && $
    ISA(data) && data eq self->GetFullIdentifier()) then begin
    oDS = self->GetDataspace()
    if (oDS->IsContained(self, POSITION=indexSelf) && $
      oDS->IsContained(self._oFill, POSITION=indexFill)) then begin
      ; Move the filled background so it is just before our grid.
      newIndex = (indexFill gt indexSelf) ? indexSelf : (indexSelf-1)
      oDS->Move, indexFill, newIndex, /NO_NOTIFY
    endif
  endif
  self->IDLitVisualization::OnNotify, id, message,data
end


;----------------------------------------------------------------------------
; Construct the lon/lat coordinates for the filled background.
;
function IDLitVisMapGrid::_GetFillHorizonData, sMap, $
  CONNECTIVITY=connectivity, $
  HORIZON_CONNECTIVITY=horizonConn, $
  HORIZON_DATA=horizonData, $
  TESSELLATE=tessellate

  compile_opt idl2, hidden

  origLimit = self._limit

  ; Nudge the corner points to avoid clipping issues.
  ; CT, VIS, March 2013: Be careful: If you change EPS, be sure to
  ; change the EPS within IDLitVisMapGridline::OnProjectionChange.
  EPS = 1d-5
  limit = origLimit
  dx = 0.5*EPS*ABS(limit[3] - limit[1])
  dy = 0.5*EPS*ABS(limit[2] - limit[0])
  limit += [dy, dx, -dy, -dx]
  dlon = limit[3] - limit[1]
  dlat = limit[2] - limit[0]
  tessellate = 1b

  switch sMap.up_name of
    'Interrupted Goode': begin
      if (dlon lt 359) then break
      n = 91
      ; yNd = latitudes, Northern hemisphere, descending from 90N to 0N
      ; yNu = latitudes, Northern hemisphere, ascending from 0N to 90N
      ; ySd = latitudes, Southern hemisphere, descending from 0N to 90S
      ; ySu = latitudes, Southern hemisphere, ascending from 90S to 0N
      yNu = DINDGEN(n)/(n-1)*limit[2]
      yNd = REVERSE(yNu)
      ySd = DINDGEN(n)/(n-1)*limit[0]
      ySu = REVERSE(ySd)
      yBot = limit[0] + DBLARR(n)
      yTop = limit[2] + DBLARR(n)
      ; Start at the top left corner, 90N, 180W, go counterclockwise.
      lats = [yNd, ySd, yBot, ySu, ySd, yBot, ySu, ySd, yBot, ySu, ySd, yBot, $
        ySu, yNu, yTop, yNd, yNu, yTop]

      ; xr = longitude points slightly to the right of a "gap"
      ; xl = longitude points slightly to the left of a "gap"
      ; The tiny offset prevents the points from getting clipped by the
      ; map split/clip algorithm.
      xr = DBLARR(n) + 1d-5
      xl = DBLARR(n) - 1d-5
      dx = 1d-5
      x = DINDGEN(n)/(n-1)
      w180 = limit[1]
      e180 = limit[3]
      lons = [xr+w180, xr+w180, w180+80*x, xl-100, xr-100, $
        -100+80*x, xl-20, xr-20, -20+100*x, $
        xl+80, xr+80, 80+100*x, xl+e180, xl+e180, $
        e180-220*x, xr-40, xl-40, -40-140*x]

      ; One big polygon.
      n = N_ELEMENTS(lons)
      connectivity = [n, LINDGEN(n)]

      ; Luckily, the Interrupted Goode can be defined by a single polygon,
      ; and so the horizon line can just use the same points & connectivity.
      ; This isn't true for the Interrupted Mollweide... See below...
      break
      end

    'Interrupted Mollweide': begin
      if (dlon lt 359) then break
      n = 91
      ; See comments in Interrupted Goode above...
      yNu = DINDGEN(n)/(n-1)*limit[2]
      yNd = REVERSE(yNu)
      ySd = DINDGEN(n)/(n-1)*limit[0]
      ySu = REVERSE(ySd)
      ; Starting at 90N,180E and working counterclockwise
      lats = [yNd, yNu, yNd, ySd, ySu, ySd, ySu, yNu, $
        ySu, ySd, ySu, yNu, yNd, yNu, yNd, ySd]
      xr = DBLARR(n) + 1d-4
      xl = DBLARR(n) - 1d-4
      dx = 1d-4
      lons = $
        [xr+110, xl+110, xr+20, xr+20, xl+140, xr+140, xl+180, xl+180, $
        xl-70, xr-70, xl+20, xl+20, xr-100, xl-100, xr-180, xr-180]
      ; The Interrupted Mollweide needs to be split up into 2 polygons,
      ; because the projection crosses the dateline near the middle.
      ; The seam between the 2 polygons will run down the dateline.
      connectivity = [8*n, LINDGEN(8*n), 8*n, LINDGEN(8*n)+8*n]

      ; Since we used 2 polygons, we can't use the same data for the horizon,
      ; because we don't want to see the line down the dateline. 
      ; Also, if the user sets the LIMIT to a smaller latitude range,
      ; we need to run lines along the top & bottom edges of the map.
      x = DINDGEN(n)/(n-1)
      zero = DBLARR(n)
      hlat = [limit[2]+zero, yNd, yNu, limit[2]+zero, yNd, $  ; top left edge
        ySd, limit[0]+zero, ySu, ySd, limit[0]+zero, $  ; bottom left edge
        limit[0]+zero, ySu, ySd, limit[0]+zero, ySu, $  ; bottom right edge
        yNu, limit[2]+zero, yNd, yNu, limit[2]+zero]  ; top right edge
      hlon = [180-70*x, xr+110, xl+110, 110-90*x, xr+20, $  ; top left edge
        xr+20, 20+120*x, xl+140, xr+140, 140+40*x, $  ; bottom left edge
        -180+110*x, xl-70, xr-70, -70+90*x, xl+20, $  ; bottom right edge
        xl+20, -100+120*x, xr-100, xl-100, -180+80*x]  ; top right edge
      horizonData = TRANSPOSE([[hlon], [hlat]])
      ; Two different polylines, one for each half. Split is at 180E.
      horizonConn = [10*n, LINDGEN(10*n), 10*n, LINDGEN(10*n)+10*n]

      break
      end

    'Gnomonic': ; fall through
    'Orthographic': ; fall through
    'Near Side Perspective': begin

      ; Tricky: we need to draw a circle around the outside of the globe,
      ; which might be zoomed in (using the map HEIGHT), or not centered
      ; on the equator (CENTER_LATITUDE). Also, if the user has set the LIMIT,
      ; then the "circle" might really be a rectangle with rounded corners.
      ; To solve this, "shoot" rays of u/v coordinates out from the center,
      ; convert to lat/lon, then convert back to u/v but as a "polyline",
      ; so that the line gets clipped to the edge. Then, find the last good
      ; point that didn't get clipped and save it. These will produce a "ring"
      ; that should go out to the edge of the map but not beyond.

      radius = ((sMap.uv_box[2] - sMap.uv_box[0])/2) < 2d7
      ; Take the sqrt so we end up with less points near the center of the map,
      ; and more points near the edge where we really need good resolution.
      ; Use the SQRT(2) in case we have a "square" map (user has set LIMIT).
      line = SQRT(2)*radius*SQRT((DINDGEN(90)+1)/90)
      uv = DBLARR(2,361)
      for i=0,359 do begin
        u = line*COS(2*!DPI*i/360)
        v = line*SIN(2*!DPI*i/360)
        latlon1 = MAP_PROJ_INVERSE(u, v, MAP_STRUCTURE=sMap)
        good = WHERE(TOTAL(FINITE(latlon1), 1) eq 2, /NULL)
        if (~ISA(good)) then break
        uv[*,i] = (MAP_PROJ_FORWARD(latlon1[*,good], $
          MAP_STRUCTURE=sMap, POLYLINES=void))[*,-1]
      endfor

      ; Didn't make it to the end?
      if (i le 359) then break

      ; Connect the end to the beginning.
      uv[*,-1] = uv[*,0]
      ; Nudge the edge to avoid clipping issues.
      uv -= 1d-5*uv
      lonlat = MAP_PROJ_INVERSE(uv, MAP_STRUCTURE=sMap)
      lons = REFORM(lonlat[0,*])
      lats = REFORM(lonlat[1,*])
      break
      end

    'Polar Stereographic': begin
      if (ABS(dlon) lt 359) then break
      ; Include the end points so we don't have a gap.
      lons = [origLimit[1],(DINDGEN(361)/360)*dlon + limit[1], origLimit[3]]
      nlon = N_ELEMENTS(lons)
      lats = REPLICATE(sMap.p0lat ge 0 ? limit[0] : limit[2], nlon)
      if (MAX(ABS(origLimit[[0,2]])) lt 89.9d) then begin
        tessellate = 1b
        lons = [lons, lons]
        lats = [lats, REPLICATE(sMap.p0lat ge 0 ? limit[2] : limit[0], nlon)]
        horizonConn = [nlon, LINDGEN(nlon), nlon, LINDGEN(nlon)+nlon]
      endif else begin
        tessellate = 0b
      endelse
      break
      end
  endswitch
  
  ; Default case: Just define a rectangle that spans the map limits.
  if (~ISA(lons)) then begin
    dlon = limit[3] - limit[1]
    dlat = limit[2] - limit[0]
    lon1 = (DINDGEN(361)/360)*dlon + limit[1]
    lat1 = (DINDGEN(181)/180)*dlat + limit[0]
    nx = N_ELEMENTS(lon1)
    ny = N_ELEMENTS(lat1)
    lons = [lon1, DBLARR(ny)+limit[3], REVERSE(lon1), DBLARR(ny)+limit[1]]
    lats = [DBLARR(nx)+limit[0], lat1, DBLARR(nx)+limit[2], REVERSE(lat1)]
    n = N_ELEMENTS(lons)
    connectivity = [n, LINDGEN(n)]
  endif

  lonlat = TRANSPOSE([[lons], [lats]])

  if (~ISA(connectivity)) then begin
    n = N_ELEMENTS(lons)
    connectivity = [n, LINDGEN(n)]
  endif
  
  if (~ISA(horizonData)) then begin
    horizonData = lonlat
  endif
  
  if (~ISA(horizonConn)) then begin
    n = N_ELEMENTS(horizonData)/2
    horizonConn = [n, LINDGEN(n)]
  endif
  
  ; Include a point in the middle of each side of the fill polygon that
  ; extends out to the original map limits. Since our fill polygon has
  ; impacts_range set to 1, this will force the dataspace to set the x/yrange
  ; to the full map limit, instead of our "nudged" limit.
  ; Don't include this point in the connectivity array!
  m1 = MEAN(limit[[1,3]])
  m2 = MEAN(limit[[0,2]])
  lonlat = [[lonlat], $
    [origLimit[1], m2], $
    [origLimit[3], m2], $
    [m1, origLimit[0]], $
    [m1, origLimit[2]]]

  return, lonlat
end


;----------------------------------------------------------------------------
; Construct the horizon polyline.
;
pro IDLitVisMapGrid::_ConstructHorizon

  compile_opt idl2, hidden
  
  sMap = self->GetProjection()
    
  self._oBox->GetProperty, TRANSPARENCY=transparency
  self->IDLitVisualization::GetProperty, CLIP=myClip, HIDE=myHide
  
  self._oHorizon = Obj_New('IDLitVisPolyline', $
    IMPACTS_RANGE=1, $
    CLIP=0, $
    HIDE=myHide, $
    LINESTYLE='none', $
    MANIPULATOR_TARGET=0, $
    MAP_INTERPOLATE=0, $
    NAME='Map Horizon', $
    TRANSPARENCY=transparency)
    
  self->Add, self._oHorizon, /NO_NOTIFY, /NO_UPDATE
  
  ; Attach the data after we've added the object to ourself.
  ; That way the horizon object can retrieve things like the map projection.
;  self._oHorizon->SetProperty, CONNECTIVITY=horizonConn, $
;    DATA=horizonData
end


;----------------------------------------------------------------------------
; Construct the lon/lat coordinates for the filled background.
;
pro IDLitVisMapGrid::_ConstructFillPolygon

  compile_opt idl2, hidden
  
  sMap = self->GetProjection()

  self._oBox->GetProperty, TRANSPARENCY=transparency
  self->IDLitVisualization::GetProperty, CLIP=myClip, HIDE=myHide
  n = N_ELEMENTS(lonlat)/2

  self._oFill = Obj_New('IDLitVisPolygon', $
;    _DATA=lonlat, $
    /NO_CLOSE, $
    CLIP=myClip, $
    HIDE=myHide, $
    IMPACTS_RANGE=1, $
    LINESTYLE='none', $
    MANIPULATOR_TARGET=0, $
    MAP_INTERPOLATE=0, $
    NAME='Map Background', $ ;*** If you change this, also change WriteKML!!!
    TRANSPARENCY=transparency)

  ; Add the grid background polygon to our Dataspace, so that it
  ; appears underneath any continents or other map items.
  oDS = self->GetDataspace()
  oDS->Add, self._oFill, /NO_UPDATE, /NO_NOTIFY
  
  ; Make ourself an observer of the Dataspace, so we get notifications if the
  ; grid has been moved up/down, and we can then move our background polygon.
  oTool = self->GetTool()
  oTool->AddOnNotifyObserver, self->GetFullIdentifier(), oDS->GetFullIdentifier()
end


;----------------------------------------------------------------------------
function IDLitVisMapGrid::_GetGridlines

    compile_opt idl2, hidden

    ; Return either the longitude or the latitude container & contents.
    oContainer = self._oLineContainer[self._container]
    oLines = oContainer->Get(/ALL, $
        ISA='IDLitVisMapGridline', COUNT=nlines)
    count = 0L
    for i=0,nlines-1 do begin
        oLines[i]->IDLgrModel::GetProperty, HIDE=hide
        if (hide) then $
            continue
        oLines[count] = oLines[i]  ; move to front
        count++  ; found a non-hidden line
    endfor

    ; Sanity check.
    oContainer->_CheckIntersectAttributes

    return, (count gt 0) ? [oContainer, oLines[0:count-1]] : oContainer

end


;----------------------------------------------------------------------------
pro IDLitVisMapGrid::_UpdateGridlines, isLatitude, sMap

    compile_opt idl2, hidden

    hasMap = (N_TAGS(sMap) gt 0)

    oLineContainer = self._oLineContainer[isLatitude]

    oLines = oLineContainer->Get(ISA='IDLitVisMapGridline', $
        /ALL, COUNT=ncurrent)

    gridSpacing = isLatitude ? self._gridLatitude : self._gridLongitude

    if (gridSpacing eq 0) then begin
        ; Zero grid spacing: Turn off all grid lines and return.
        for i=0,ncurrent-1 do $
            oLines[i]->SetProperty, /HIDE, IMPACTS_RANGE=0
        return
    endif

    ; Create/modify longitude/latitude lines
    mylimits = isLatitude ? self._limit[[0,2]] : self._limit[[1,3]]

    clip0 = 0b
    clip180 = 0b
    if (hasMap) then begin

        maplimits = isLatitude ? sMap.ll_box[[0,2]] : sMap.ll_box[[1,3]]

        if (~isLatitude && (sMap.ll_box[3] - sMap.ll_box[1]) ge 360) then begin
            ; Find the center latitude of the projection.
            latMiddle = 0.5*(sMap.ll_box[0] + sMap.ll_box[2])
            uv180 = MAP_PROJ_FORWARD([-180, 180], [latMiddle, latMiddle], $
                MAP=sMap)
            if (MIN(FINITE(uv180)) eq 1) then begin
                dist = TOTAL((uv180[*,1] - uv180[*,0])^2)
                clip180 = (dist lt 1d-3)
            endif
            ; See if 0E and 360E lie on top of each other.
            ; Use a small epsilon so the map doesn't wrap around by mistake.
            uv0 = MAP_PROJ_FORWARD([0, 360-1d-6], [latMiddle, latMiddle], $
              MAP=sMap)
            if (MIN(FINITE(uv0)) eq 1) then begin
              dist = TOTAL((uv0[*,1] - uv0[*,0])^2)
              clip0 = (dist lt 1d-3)
            endif
        endif

    endif


    nlines = LONG((mylimits[1] - mylimits[0])/gridSpacing) + 1
    nlines = 1 > nlines < 360   ; arbitrary cutoff at 360 lines
    start = gridSpacing*CEIL(mylimits[0]/gridSpacing)
    locations = DINDGEN(nlines)*gridSpacing + start

    ; Remove the last gridline if it is past the edge.
    if (nlines ge 2 && locations[-1] gt mylimits[1]) then $
      locations = locations[0:-2]
    nlines = N_ELEMENTS(locations)

    found180 = 0b
    found0 = 0b
    
    oDead = []

    if (ncurrent gt 0) then begin

        props = oLines[0]->QueryProperty()

        for i=ncurrent-1,0,-1 do begin
            oLines[i]->GetProperty, LOCATION=currLocation
            imatch = (WHERE(locations eq currLocation))[0]

            ; We found a match, so don't duplicate it below.
            if (imatch ge 0) then $
                locations[imatch] = -999

            ; If we already found 180E, and 180W would lie on top,
            ; then hide it.
            if (found180 && clip180 && ABS(currLocation) eq 180) then begin
              imatch = -1
            endif
            if (found0 && clip0 && (currLocation mod 360) eq 0) then begin
              imatch = -1
            endif

            ; If we didn't have a match, or the old line is out of bounds,
            ; then hide it. Otherwise turn it on.
            if (imatch lt 0) || $
                (hasMap && (currLocation lt maplimits[0] || $
                currLocation gt maplimits[1])) then begin
                oDead = [oDead, oLines[i]]
            endif else begin
                oLines[i]->SetProperty, HIDE=0, /IMPACTS_RANGE
                oLines[i]->OnProjectionChange, sMap
                ; Keep track if we just did 180W or 180E
                if (clip180 && ABS(currLocation) eq 180) then $
                  found180 = 1b
                if (clip0 && (currLocation mod 360) eq 0) then $
                  found0 = 1b
            endelse

        endfor

    endif

    ndead = N_ELEMENTS(oDead)

    ; Create new lines if necessary.
    for i=0,nlines-1 do begin

        if (locations[i] eq -999 || $
            (hasMap && (locations[i] lt maplimits[0] || $
            locations[i] gt maplimits[1]))) then $
            continue

        ; If we already found 180E, and 180W would lie on top,
        ; then hide it.
        if (clip180 && ABS(locations[i]) eq 180) then begin
            if (found180) then $
                continue
            found180 = 1b
        endif
        if (clip0 && (locations[i] mod 360) eq 0) then begin
          if (found0) then $
            continue
          found0 = 1b
        endif

        strloc = STRING(ABS(locations[i]), FORMAT='(g0)')
        if (isLatitude) then begin
            name = (locations[i] eq 0) ? 'Equator (0N)' : $
                'Lat ' + strloc + ((locations[i] ge 0) ? 'N' : 'S')
        endif else begin
            name = (locations[i] eq 0) ? 'Prime Meridian (0E)' : $
                'Lon ' + strloc + ((locations[i] ge 0) ? 'E' : 'W')
        endelse

        if (ndead gt 0) then begin
          oLine = oDead[-1]
          ndead--
          oDead = (ndead ge 1) ? oDead[0:ndead-1] : !NULL
          isNew = 0
        endif else begin
          oLine = OBJ_NEW('IDLitVisMapGridline', NAME=name, TOOL=self->GetTool())
          isNew = 1
        endelse

        ; Copy properties from my first gridline over to the new one.
        for p=0,N_ELEMENTS(props)-1 do begin
          if (oLines[0]->GetPropertyByIdentifier(props[p], value)) then begin
            void = oLine->GetPropertyByIdentifier(props[p], oldvalue)
            if (~ARRAY_EQUAL(value, oldvalue)) then $
              oLine->SetPropertyByIdentifier, props[p], value
          endif
        endfor

        if (isNew) then begin
          oLineContainer->Add, oLine, /AGGREGATE, $
            /NO_NOTIFY, /NO_UPDATE
        endif

        ; This will automatically call OnProjectionChange.
        ; Set HIDE=0 in case it got stomped by the copy props above.
        oLine->SetProperty, NAME=name, HIDE=0, $
          /IMPACTS_RANGE, ORIENTATION=isLatitude, LOCATION=locations[i]

    endfor


    if (ISA(oDead)) then begin
      oLineContainer->_IDLitVisualization::Remove, oDead
      OBJ_DESTROY, oDead
    endif

    oLines = oLineContainer->Get(ISA='IDLitVisMapGridline', $
        /ALL, COUNT=ncurrent)
    if (ncurrent gt 0) then begin
      loc = []
      oGood = []
      for i=0,ncurrent-1 do begin
        if (~ISA(oLines[i])) then continue
        oLines[i]->GetProperty, LOCATION=location
        oGood = [oGood, oLines[i]]
        loc = [loc, location]
      endfor
      oLineContainer->IDLgrModel::Remove, /ALL
      if (ISA(oGood)) then begin
        oGood = oGood[SORT(loc)]
        oLineContainer->IDLgrModel::Add, oGood
      endif
    endif

end


;---------------------------------------------------------------------------
; For the current dataspace compute a default lon/lat range.
; Returns result as  [Latmin, Lonmin, Latmax, Lonmax]
;
pro IDLitVisMapGrid::_UpdateLimit, sMap

  compile_opt idl2, hidden

  ; Default grid range. [Latmin, Lonmin, Latmax, Lonmax]
  self._limit = [-90,-180,90,540]
  limit = [-90d, -180d, 90d, 180d]
  userLimit = -999
  
  oDataspace = self->GetDataspace(/UNNORMALIZED)
  if (~OBJ_VALID(oDataspace)) then $
    goto, done
    
  ; Do we currently have a valid map projection?
  ; If so retrieve the map range and use it instead.
  haveMapProjection = N_TAGS(sMap) gt 0
  if (haveMapProjection) then begin
    oMapProj = oDataspace->_GetMapProjection()
    oMapProj->GetProperty, LIMIT=limit, USER_LIMIT=userLimit
    limit[0] = limit[0] > sMap.ll_box[0]
    limit[1] = limit[1] > sMap.ll_box[1]
    limit[2] = limit[2] < sMap.ll_box[2]
    limit[3] = limit[3] < sMap.ll_box[3]
  endif
  
  ; Flip range if necessary.
  if (limit[0] gt limit[2]) then $
    limit[[0,2]] = limit[[2,0]]
  if (limit[1] gt limit[3]) then $
    limit[[1,3]] = limit[[3,1]]
    
    
  ; If the user has explicitely set the limit, just use it.
  ; Otherwise, we'll get the limit from the contained visualizations.
  if MAX(userLimit) gt -999 || limit[3] gt 180 then $
    goto, done
    

  ; Try to directly retrieve the lonlat range.
  success = oDataspace->GetLonLatRange(xrange, yrange, MAP_STRUCTURE=sMap)

  ; If that fails, use a brute force approach, given the dataspace range
  ; and the current map projection.
  if (~success || (xrange[0] eq xrange[1]) || (yrange[0] eq yrange[1])) then begin
    success = oDataspace->GetXYZRange(xrange, yrange, zrange, $
      /NO_TRANSFORM)
      
    if (~success) then $
      goto, done
    if (xrange[0] eq xrange[1] || yrange[0] eq yrange[1]) then $
      goto, done
      
    if (haveMapProjection) then begin

      ; If the dataspace has a map projection,
      ; then convert a grid of points back to degrees.
      ; Note that we don't care what our image map projection is, just
      ; the dataspace, since that determines the U/V extent.
      
      ; Pick a reasonable grid spacing.
      ; Note: This might cause gaps (see *** below)
      n = 91
      xr = DINDGEN(n)/(n-1)*(xrange[1]-xrange[0]) + xrange[0]
      ; Be sure to use a column vector for Y, so we get a full grid!
      yr = DINDGEN(1,n)/(n-1)*(yrange[1]-yrange[0]) + yrange[0]
      mathError = CHECK_MATH(/NOCLEAR)
      lonlat = MAP_PROJ_INVERSE(REBIN(xr, n, n), REBIN(yr, n, n), $
        MAP_STRUCTURE=sMap)
      ; If no errors are pending, then clear all exceptions.
      if (mathError eq 0) then dummy = CHECK_MATH()
      
      minn = MIN(lonlat, DIMENSION=2, MAX=maxx, /NAN)
      xrange = [minn[0], maxx[0]]
      yrange = [minn[1], maxx[1]]
      
      ; *** Do a quick sanity check - run the original lon/lat limit
      ; through the map transform. If the points survive, then
      ; bump the ranges out to these limits. This avoids problems with
      ; projections like the Polar Stereographic, where you can have
      ; "gaps" between 180W and 180E, because of the finite grid
      ; spacing above.
      lons = limit[[1,3,3,1]]
      lats = limit[[0,0,2,2]]
      xytmp = MAP_PROJ_FORWARD(lons, lats, MAP_STRUCTURE=sMap)
      xytmpMin = MIN(xytmp, DIMENSION=2, MAX=xytmpMax, /NAN)
      if (FINITE(xytmpMin[0])) then xrange[0] <= limit[1]
      if (FINITE(xytmpMin[1])) then yrange[0] <= limit[0]
      if (FINITE(xytmpMax[0])) then xrange[1] >= limit[3]
      if (FINITE(xytmpMax[1])) then yrange[1] >= limit[2]
      
    endif
    
  endif
  
  ; Flip range if necessary.
  if (xrange[0] gt xrange[1]) then $
    xrange = xrange[[1,0]]
  if (yrange[0] gt yrange[1]) then $
    yrange = yrange[[1,0]]

  ; Restrict our grid range to match contained visualizations.
  if (ABS(limit[1] - xrange[0]) gt 1e-3) then limit[1] >= xrange[0]
  if (ABS(limit[3] - xrange[1]) gt 1e-3) then limit[3] <= xrange[1]
  if (ABS(limit[0] - yrange[0]) gt 1e-3) then limit[0] >= yrange[0]
  if (ABS(limit[2] - yrange[1]) gt 1e-3) then limit[2] <= yrange[1]
  
  done:
  self._limit[0] = self._limit[0] > limit[0]
  self._limit[1] = self._limit[1] > limit[1]
  self._limit[2] = self._limit[2] < limit[2]
  self._limit[3] = self._limit[3] < limit[3]
end


;---------------------------------------------------------------------------
; For min/max range compute a default grid spacing in degrees.
;
pro IDLitVisMapGrid::_UpdateGridSpacing, NO_NOTIFY=noNotify

  compile_opt idl2, hidden

  dlon = abs(self._limit[3] - self._limit[1])
  dlat = abs(self._limit[2] - self._limit[0])

  ; Spacing in seconds, minutes, or degrees.
  deltaLon = [1d/3600,5d/3600,10d/3600,15d/3600,30d/3600, $
    1d/60,5d/60,10d/60,15d/60,30d/60,1,5,10,15,30,45,90]
  deltaLat = [1d/3600,5d/3600,10d/3600,15d/3600,30d/3600, $
    1d/60,5d/60,10d/60,15d/60,30d/60,1,5,10,15,30,30,30]

  nlon = dlon/deltaLon
  nlat = dlat/deltaLat

  ; First try to pick between 3 and 10 lines for the spacing,
  ; since that will look nicest. If that fails, try between 2 and 12 lines.
  goodLon = nlon ge 3 and nlon le 10
  if (TOTAL(goodLon) eq 0) then $
    goodLon = nlon ge 2 and nlon le 12

  goodLat = nlat ge 3 and nlat le 10
  if (TOTAL(goodLat) eq 0) then $
    goodLat = nlat ge 2 and nlat le 12

  ; If possible, try to pick the same grid spacing for lon & lat.
  bothMatch = (WHERE(goodLon and goodLat))[0]

  if (bothMatch ge 0) then begin
    lonMatch = bothMatch
    latMatch = bothMatch
  endif else begin
    ; If the grid spacing is too different, then pick
    ; grid spacing for lon & lat separately.
    lonMatch = (WHERE(goodLon))[0]
    latMatch = (WHERE(goodLat))[0]
    if (lonMatch gt latMatch) then begin
      latMatch = (WHERE(goodLat))[-1]
    endif else begin
      lonMatch = (WHERE(goodLon))[-1]
    endelse
  endelse

  gridLon = (lonMatch ge 0) ? deltaLon[lonMatch] : dlon/6
  gridLat = (latMatch ge 0) ? deltaLat[latMatch] : dlat/6

  if (gridLon ne self._gridLongitude || $
    gridLat ne self._gridLatitude) then begin
    self._gridLongitude = gridLon
    self._gridLatitude = gridLat
    ; Notify observers (like property sheet) that my grid spacing changed.
    if (~KEYWORD_SET(noNotify)) then begin
      self->DoOnNotify, self->GetFullIdentifier(), 'SETPROPERTY', ''
    endif
  endif

end


;----------------------------------------------------------------------------
pro IDLitVisMapGrid::OnProjectionChange, sMap, NO_NOTIFY=noNotify

    compile_opt idl2, hidden

    ; Lock ourself down, so we don't call back into here
    ; from the OnDataRangeChange.
    self._withinProjChange = 1b

    if (~N_ELEMENTS(sMap)) then $
        sMap = self->GetProjection()

    if (N_TAGS(sMap) eq 0) then $
      return

    if (self._autoGrid) then begin
      ; Compute grid range. [Latmin, Lonmin, Latmax, Lonmax]
      self->_UpdateLimit, sMap
      ; Compute grid spacing.
      self->_UpdateGridSpacing, NO_NOTIFY=noNotify
    endif

    self->_UpdateGridlines, 0, sMap
    self->_UpdateGridlines, 1, sMap

    self->UpdateSelectionVisual
    
    ; Box axes
    !NULL = self->GetCenterRotation(/NO_TRANSFORM, $
                                    /INCLUDE_AXES, $
                                    XRANGE=xRange, $
                                    YRANGE=yRange, $
                                    ZRANGE=zRange)

    self._oBox->SetProperty, HIDE=~self._boxAxes, IMPACTS_RANGE=self._boxAxes
    ; Only calculate the box axes data if it is visible
    if (self._boxAxes) then self._oBox->_UpdateBoxGrid

    ; Fill
    if (ISA(self._oFill) || ISA(self._oHorizon)) then begin
      lonlat = self->_GetFillHorizonData(sMap, CONNECTIVITY=conn, $
        TESSELLATE=tessellate, $
        HORIZON_DATA=horizonData, HORIZON_CONNECTIVITY=horizonConn)
      if (ISA(self._oFill)) then begin
        self._oFill->SetProperty, _DATA=lonlat, $
          CONNECTIVITY=conn, TESSELLATE=tessellate
        self._oFill->OnProjectionChange, self->GetProjection()
      endif
      if (ISA(self._oHorizon)) then begin
        self._oHorizon->SetProperty, _DATA=horizonData, CONNECTIVITY=horizonConn
        self._oHorizon->OnProjectionChange, self->GetProjection()
      endif
    endif
    
    if (~KEYWORD_SET(noNotify)) then begin
        self->IDLgrModel::GetProperty, PARENT=oParent
        if (OBJ_VALID(oParent)) then begin
            self->OnDataChange, oParent
            self->OnDataComplete, oParent
        endif
    endif

  self._withinProjChange = 0b
end


;----------------------------------------------------------------------------
pro IDLitVisMapGrid::GetProperty, $
    AUTO_GRID=autoGrid, $
    BOX_AXES=boxAxes, $
    FILL_COLOR=fillColor, $
    GRID_LATITUDE=gridLatitude, $
    GRID_LONGITUDE=gridLongitude, $
    EDIT_LONGITUDES=editLongitudes, $
    EDIT_LATITUDES=editLatitudes, $
    HORIZON_COLOR=horizonColor, $
    HORIZON_LINESTYLE=horizonLinestyle, $
    HORIZON_THICK=horizonThick, $
    LONGITUDE_MIN=longitudeMin, $
    LONGITUDE_MAX=longitudeMax, $
    LATITUDE_MIN=latitudeMin, $
    LATITUDE_MAX=latitudeMax, $
    FILL_OBJECT=fillObject, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if ARG_PRESENT(autoGrid) then $
        autoGrid = self._autoGrid

    if ARG_PRESENT(boxAxes) then $
        boxAxes = self._boxAxes

    if (ARG_PRESENT(fillColor) && ISA(self._oFill)) then begin
      if (ISA(self._oFill)) then begin
        self._oFill->GetProperty, FILL_COLOR=fillColor
      endif else fillColor = -1
    endif

    if ARG_PRESENT(fillObject) then $
      fillObject = self._oFill
      
    if ARG_PRESENT(horizonColor) then begin
      if (ISA(self._oHorizon)) then begin
        self._oHorizon->GetProperty, COLOR=horizonColor
      endif else horizonColor = [0b,0b,0b]
    endif

    if ARG_PRESENT(horizonLinestyle) then begin
      if (ISA(self._oHorizon)) then begin
        self._oHorizon->GetProperty, LINESTYLE=horizonLinestyle
      endif else horizonLinestyle = 6
    endif

    if ARG_PRESENT(horizonThick) then begin
      if (ISA(self._oHorizon)) then begin
        self._oHorizon->GetProperty, THICK=horizonThick
      endif else horizonThick = 1
    endif

    if ARG_PRESENT(editLatitudes) then $
        editLatitudes = 0

    if ARG_PRESENT(editLongitudes) then $
        editLongitudes = 0

    if ARG_PRESENT(gridLatitude) then $
        gridLatitude = self._gridLatitude

    if ARG_PRESENT(gridLongitude) then $
        gridLongitude = self._gridLongitude

    if ARG_PRESENT(latitudeMin) then $
        latitudeMin = self._limit[0]

    if ARG_PRESENT(latitudeMax) then $
        latitudeMax = self._limit[2]

    if ARG_PRESENT(longitudeMin) then $
        longitudeMin = self._limit[1]

    if ARG_PRESENT(longitudeMax) then $
        longitudeMax = self._limit[3]

    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisualization::GetProperty, _EXTRA=_extra
end


;----------------------------------------------------------------------------
pro IDLitVisMapGrid::SetProperty, $
    AUTO_GRID=autoGrid, $
    BOX_AXES=boxAxes, $
    CLIP=doClip, $
    COLOR=color, $
    THICK=thick, $
    LINESTYLE=linestyle, $
    FILL_COLOR=fillColor, $
    HIDE=hide, $
    HORIZON_COLOR=horizonColor, $
    HORIZON_LINESTYLE=horizonLinestyle, $
    HORIZON_THICK=horizonThick, $
    TRANSPARENCY=transparency, $
    GRID_LATITUDE=gridLatitude, $
    GRID_LONGITUDE=gridLongitude, $
    LONGITUDE_MIN=longitudeMin, $
    LONGITUDE_MAX=longitudeMax, $
    LATITUDE_MIN=latitudeMin, $
    LATITUDE_MAX=latitudeMax, $
    EDIT_LONGITUDES=editLongitudes, $ ; swallow (just userdef placeholder)
    EDIT_LATITUDES=editLatitudes, $ ; swallow (just userdef placeholder)
    LOCATION=location, $       ; swallow (don't aggregate)
    ORIENTATION=orientation, $ ; swallow (don't aggregate)
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    updateGrid = 0b

    self->IDLitComponent::GetProperty, INITIALIZING=isInit

    if (N_ELEMENTS(autoGrid)) then begin
        self._autoGrid = autoGrid
        self->SetPropertyAttribute, ['LONGITUDE_MIN', 'LONGITUDE_MAX', $
            'LATITUDE_MIN', 'LATITUDE_MAX', $
            'GRID_LONGITUDE', 'GRID_LATITUDE'], $
            SENSITIVE=~self._autoGrid
        if (self._autoGrid) then updateGrid = 1b
    endif

    if (N_ELEMENTS(boxAxes)) then begin
        self._boxAxes = boxAxes
        updateGrid = 1b
    endif


    if (N_ELEMENTS(horizonColor) && ~isInit) then begin
      if (~ISA(self._oHorizon)) then begin
        self->_ConstructHorizon
        updateGrid = 1b
      endif
      if (ISA(horizonColor, 'STRING')) then $
        STYLE_CONVERT, horizonColor, COLOR=horizonColor
      self._oHorizon->SetProperty, COLOR=horizonColor
      ; If our horizon line wasn't currently turned "on", then
      ; make it visible.
      if (~ISA(horizonLinestyle)) then begin
        self._oHorizon->GetProperty, LINESTYLE=oldstyle
        if (oldstyle eq 6) then horizonLinestyle = 0
      endif
    endif


    if (N_ELEMENTS(horizonThick) && ~isInit) then begin
      if (~ISA(self._oHorizon)) then begin
        self->_ConstructHorizon
        updateGrid = 1b
      endif
      self._oHorizon->SetProperty, THICK=horizonThick
      ; If our horizon line wasn't currently turned "on", then
      ; make it visible.
      if (~ISA(horizonLinestyle)) then begin
        self._oHorizon->GetProperty, LINESTYLE=oldstyle
        if (oldstyle eq 6) then horizonLinestyle = 0
      endif
    endif
    

    if (ISA(horizonLinestyle) && ~isInit) then begin
      if (~ISA(self._oHorizon)) then begin
        self->_ConstructHorizon
        updateGrid = 1b
      endif
      self._oHorizon->SetProperty, LINESTYLE=LINESTYLE_CONVERT(horizonLinestyle)
    endif

    
    wasAutoGrid = self._autoGrid

    if (N_ELEMENTS(latitudeMin) && $
        latitudeMin ne self._limit[0]) then begin
        self._limit[0] = latitudeMin
        self._autoGrid = 0b
        updateGrid = 1b
    endif

    if (N_ELEMENTS(latitudeMax) && $
        latitudeMax ne self._limit[2]) then begin
        self._limit[2] = latitudeMax
        self._autoGrid = 0b
        updateGrid = 1b
    endif

    if (N_ELEMENTS(longitudeMin) && $
        longitudeMin ne self._limit[1]) then begin
        self._limit[1] = longitudeMin
        self._autoGrid = 0b
        updateGrid = 1b
    endif

    if (N_ELEMENTS(longitudeMax) && $
        longitudeMax ne self._limit[3]) then begin
        self._limit[3] = longitudeMax
        self._autoGrid = 0b
        updateGrid = 1b
    endif
    
    if (wasAutoGrid ne self._autoGrid) then begin
      self->_UpdateGridSpacing, /NO_NOTIFY
    endif

    if (N_ELEMENTS(gridLatitude) && $
      gridLatitude ne self._gridLatitude) then begin
      self._gridLatitude = gridLatitude
      self._autoGrid = 0b
      updateGrid = 1b
    endif
    
    if (N_ELEMENTS(gridLongitude) && $
      gridLongitude ne self._gridLongitude) then begin
      self._gridLongitude = gridLongitude
      self._autoGrid = 0b
      updateGrid = 1b
    endif
    
    if (ISA(fillColor) && ~isInit) then begin
      if (ISA(fillColor, 'STRING')) then $
        STYLE_CONVERT, fillColor, COLOR=fillColor
      if (ARRAY_EQUAL(fillColor, [255b,255b,255b])) then $
        fillColor = -1
      if (N_ELEMENTS(fillColor) gt 1 && ~ISA(self._oFill)) then begin
        self->_ConstructFillPolygon
        updateGrid = 1b
      endif
      if (ISA(self._oFill)) then begin
        self._oFill->SetProperty, FILL_COLOR=fillColor, $
          FILL_BACKGROUND=(N_ELEMENTS(fillColor) eq 3)
      endif
    endif
    
    if (ISA(doClip) && ISA(self._oFill)) then $
      self._oFill->SetProperty, CLIP=doClip

    if (ISA(hide) && ISA(self._oFill)) then $
      self._oFill->SetProperty, HIDE=hide

    if (ISA(transparency)) then begin
      if (ISA(self._oFill)) then $
        self._oFill->SetProperty, TRANSPARENCY=transparency
      if (ISA(self._oHorizon)) then $
        self._oHorizon->SetProperty, TRANSPARENCY=transparency
    endif

    self._oBox->SetProperty, COLOR=color, HIDE=hide, THICK=thick, LINESTYLE=linestyle, $
      TRANSPARENCY=transparency, _EXTRA=_extra

    self->IDLitVisualization::SetProperty, CLIP=doClip, $
      COLOR=color, HIDE=hide, THICK=thick, $
      LINESTYLE=linestyle, TRANSPARENCY=transparency, _EXTRA=_extra

    ; Is this object being initialized during creation?
    self->IDLitComponent::GetProperty, Initializing=isInit

    if (~isInit && updateGrid) then begin
        self->OnProjectionChange
    endif

    if (~isInit) then begin
        ; If more gridlines were created then our properties (like color)
        ; might no longer be equal for all intersected objects. Or if the
        ; labels were changed, some properties might have been (de)sensitized.
        ; In either case, check intersected properties for our line containers.
        self._oLineContainer[0]->_CheckIntersectAttributes
        self._oLineContainer[1]->_CheckIntersectAttributes
    endif

end


;----------------------------------------------------------------------------
function IDLitVisMapGrid::EditUserDefProperty, oTool, identifier

    compile_opt idl2, hidden

    switch identifier of

        'EDIT_LONGITUDES': ; fall thru
        'EDIT_LATITUDES': begin
            self._container = (identifier eq 'EDIT_LATITUDES')
            success = oTool->DoUIService('MapGridlines', self)
            ; We want to return "failure" to avoid committing the Userdef
            ; property changes. But we need to commit our individual
            ; SetProperty actions. Note that hitting the "Close" (X) button
            ; still commits the actions.
            oTool->CommitActions
            return, 0   ; "failure", so we don't commit our userdef property
            break
            end

        else: break

    endswitch

    ; Call our superclass.
    return, self->IDLitVisualization::EditUserDefProperty(oTool, identifier)

end


;---------------------------------------------------------------------------
; Convert a location from decimal degrees to DDDdMM'SS", where "d" is
; the degrees symbol.
;
; Note that this is only used to report the cursor location, not the
; actual labels for the gridlines. See IDLitVisMapGridline for the labels.
;
function IDLitVisMapGrid::_DegToDMS, x, isLat, spacing

    compile_opt idl2, hidden

    if (~FINITE(x)) then $
        return, '---'

    eps = 1d-9
    x = (x ge 0) ? x + eps : x - eps
    degrees = ABS(FIX(x))
    minutes = FIX((ABS(x) - degrees)*60)
    seconds = (ABS(x) - degrees - minutes/60d)*3600

    dms = STRING(degrees, FORMAT='(I4)') + STRING(176b) + $
        STRING(minutes, FORMAT='(I2.2)') + "'" + $
        STRING(seconds, FORMAT='(I2.2)')

    ; If grid spacing is less than 10 arcseconds (~280 meters),
    ; then also output the fractional arcseconds.
    if (MIN(spacing) lt 0.0028) then $
      dms += STRMID(STRING(seconds mod 1, FORMAT='(f4.2)'),1)

    dms += '"' + (isLat ? (x lt 0 ? 'S' : 'N') : (x lt 0 ? 'W' : 'E'))

    return, dms

end


;---------------------------------------------------------------------------
; Convert XYZ dataspace coordinates into actual data values.
;
function IDLitVisMapGrid::GetDataString, xyz
    compile_opt idl2, hidden

    x = xyz[0]
    y = xyz[1]

    sMap = self->GetProjection()
    if (N_TAGS(sMap) gt 0) then begin
        mathError = CHECK_MATH(/NOCLEAR)
        lonlat = MAP_PROJ_INVERSE(x, y, MAP_STRUCTURE=sMap)
        ; If no errors are pending, then clear all exceptions.
        if (mathError eq 0) then dummy = CHECK_MATH()
        x = lonlat[0]
        y = lonlat[1]
    endif

    value = self->_DegToDMS(x, 0, self._gridLongitude) + $
        '  ' + self->_DegToDMS(y, 1, self._gridLatitude)

    return, value

end


;----------------------------------------------------------------------------
; IDL-68612: If our dataspace has any visualizations other than ourself,
; and we are using an "automatic" grid, then don't include ourself in the
; dataspace range. Otherwise, if you add a new visualization, the dataspace
; will pick up our current grid range as the "true" range, even though the
; grid range may be about to change to match the vis.
;
; Special case: if we have box axes, then *do* include them, because they
; always impact the range.
;
function IDLitVisMapGrid::GetXYZRange, xr, yr, zr, _REF_EXTRA=ex

  compile_opt idl2, hidden

  oDS = self->GetDataspace()
  if (ISA(oDS) && (self._autoGrid || self._boxAxes)) then begin
    oVis = oDS->GetVisualizations(COUNT=c)
    foundGoodVis = 0b
    for i=0,c-1 do begin
      if (ISA(oVis[i], 'IDLitVisMapGrid') || $
        ISA(oVis[i], 'IDLitVisText')) then continue
      oVis[i]->GetProperty, IMPACTS_RANGE=impactsRange
      if (~impactsRange) then continue
      if (self._boxAxes) then begin
        return, self._oBox->GetXYZRange(xr, yr, zr,_EXTRA=ex)
      endif else begin
        return, 0
      endelse
    endfor
  endif

  return, self->_IDLitVisualization::GetXYZRange(xr, yr, zr,_EXTRA=ex)
end


;----------------------------------------------------------------------------
; PURPOSE:
;      This procedure method handles notification that the data range
;      has changed.
;
; CALLING SEQUENCE:
;    Obj->OnDataRangeChange, oSubject, XRange, YRange, ZRange
;
; INPUTS:
;      oSubject:  A reference to the object sending notification
;                 of the data range change.
;      XRange:    The new xrange, [xmin, xmax].
;      YRange:    The new yrange, [ymin, ymax].
;      ZRange:    The new zrange, [zmin, zmax].
;
; OUTPUTS:
;      There are no outputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;
pro IDLitVisMapGrid::OnDataRangeChange, oSubject, XRange, YRange, ZRange

    compile_opt idl2, hidden

    ; We only care about dataspace range changes if Automatic grid is on.
    ; Use NO_NOTIFY to avoid notifying the dataspace again, since
    ; our range shouldn't affect the dataspace anyway.
    if (self._autoGrid && ~self._withinProjChange) then $
        self->OnProjectionChange, /NO_NOTIFY

  self->_IDLitVisualization::ClipToDataRange, XRange, YRange, ZRange, /STRICT
end


;----------------------------------------------------------------------------
;+
; :Description:
;    Override the <i>_IDLitVisualization::Remove</i> method, so we can turn off
;    our AUTO_GRID property if one of the grid lines is deleted.
;
; :Params:
;    oVis
;
; :Keywords:
;    _REF_EXTRA
;
; :Author: chris
;-
pro IDLitVisMapGridContainer::Remove, oVis, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if ((Obj_Valid(oVis))[0] && Obj_Isa(oVis[0], 'IDLitVisMapGridline')) then begin
        self->GetProperty, PARENT=oParent

        if (Obj_Valid(oParent) && Obj_Isa(oParent, 'IDLitVisMapGrid')) then begin
            oParent->SetProperty, AUTO_GRID=0
        endif
    endif
    
    self->_IDLitVisualization::Remove, oVis, _REF_EXTRA=_extra
end


;----------------------------------------------------------------------------
pro IDLitVisMapGridContainer__Define

    compile_opt idl2, hidden

    struct = { IDLitVisMapGridContainer,           $
        inherits _IDLitVisualization }

end


;----------------------------------------------------------------------------
;+
; IDLitVisMapGrid__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisMapGrid object.
;
;-
pro IDLitVisMapGrid__Define

    compile_opt idl2, hidden

    struct = { IDLitVisMapGrid,           $
        inherits IDLitVisualization,       $
        _oLineContainer: OBJARR(2), $
        _oLatLines: OBJ_NEW(), $
        _oBox: OBJ_NEW(), $
        _oFill: OBJ_NEW(), $
        _oHorizon: OBJ_NEW(), $
        _boxAxes: 0b, $
        _horizon: 0b, $
        _gridLongitude: 0d, $
        _gridLatitude: 0d, $
        _limit: DBLARR(4), $  ; [latmin,lonmin,latmax,lonmax]
        _autoGrid: 0b, $
        _container: 0b, $  ; current container (either 0 or 1)
        _withinProjChange: 0b $
        }
end
