; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvismapgridline__define.pro#2 $
;
; Copyright (c) 2004-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLitVisMapGridline
;
; PURPOSE:
;    The IDLitVisMapGridline class implements a gridline visualization
;    object for the iTools system.
;
; CATEGORY:
;    Components
;
; SUPERCLASSES:
;   IDLitVisualization
;
;-


;----------------------------------------------------------------------------
function IDLitVisMapGridline::Init, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Initialize superclass
    if (~self->IDLitVisualization::Init(NAME="Gridline", $
        CLIP=0, $
        TYPE="IDLGRIDLINE", $
        IMPACTS_RANGE=1, $
        ICON='line', $
        DESCRIPTION="Grid line",$
        _EXTRA=_EXTRA))then $
        return, 0

    self._oLine = OBJ_NEW("IDLgrPolyline", /PRIVATE, $
        /ANTIALIAS, /REGISTER_PROPERTIES, $
        /USE_TEXT_ALIGNMENTS)
    self->Add, self._oLine, /AGGREGATE
    self->SetPropertyAttribute,['NAME', 'DESCRIPTION', 'SHADING'], /HIDE

    self._labelPosition = 0.5
    self._labelShow = 1
    self._labelAngle = -1

    self->IDLitVisMapGridline::_RegisterProperties

    ; Create the Font object. Use the current zoom factor of the tool window
    ; as the initial font zoom factor.  Likewise for the view zoom, and
    ; normalization factor.
    oTool = self->GetTool()
    if (OBJ_VALID(oTool) && OBJ_ISA(oTool, 'IDLitTool')) then begin
        oWin = oTool->GetCurrentWindow()
        if (OBJ_VALID(oWin)) then begin
            oWin->GetProperty, CURRENT_ZOOM=fontZoom
            oView = oWin->GetCurrentView()
            if (OBJ_VALID(oView)) then begin
                oView->GetProperty, CURRENT_ZOOM=viewZoom
                normViewDims = oView->GetViewport(UNITS=3,/VIRTUAL)
                fontNorm = MIN(normViewDims)
            endif
        endif
    endif
    self._oFont = OBJ_NEW('IDLitFont', FONT_SIZE=8, FONT_ZOOM=fontZoom, $
        VIEW_ZOOM=viewZoom, FONT_NORM=fontNorm)
    self->Aggregate, self._oFont
    self._oText = OBJ_NEW('IDLgrText', $
        /ENABLE_FORMATTING, $
        ALIGNMENT=0.5, $
        VERTICAL_ALIGNMENT=0.5, $
        FONT=self._oFont->GetFont(), $
        RECOMPUTE_DIMENSIONS=2)

    self._oLine->SetProperty, LABEL_OBJECTS=self._oText

    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisMapGridline::SetProperty, _EXTRA=_extra

    return, 1
end


;----------------------------------------------------------------------------
pro IDLitVisMapGridline::Cleanup

    compile_opt idl2, hidden

    OBJ_DESTROY, self._oText
    OBJ_DESTROY, self._oFont

    ; Cleanup superclass
    self->IDLitVisualization::Cleanup

end

;----------------------------------------------------------------------------
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLitVisMapGridline::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

    compile_opt idl2, hidden

    registerAll = ~KEYWORD_SET(updateFromVersion)

    if (registerAll) then begin
        self._oLine->RegisterProperty, 'TRANSPARENCY', /INTEGER, $
            NAME='Transparency', $
            DESCRIPTION='Transparency of grid', $
            VALID_RANGE=[0,100,5]

        ; Use TRANSPARENCY property instead.
        self->SetPropertyAttribute, 'ALPHA_CHANNEL', /HIDE, /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_SHOW', /BOOLEAN, $
            NAME='Label', $
            DESCRIPTION='Label gridlines', /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_POSITION', /FLOAT, $
            NAME='Label position', $
            DESCRIPTION='Normalized label position', $
            VALID_RANGE=[0d,1d,0.05d], /ADVANCED_ONLY

    ;    result = IDLitGetResource(1, formatNames, /DEGREESFORMAT, /NAMES)
    ;    result = IDLitGetResource(1, formatExamples, /DEGREESFORMAT, /EXAMPLES)
    ;
        self._oLine->RegisterProperty, 'LABEL_FORMAT', /STRING, $
            NAME='Label format', $
            DESCRIPTION='Predefined label format'

        self._oLine->RegisterProperty, 'LABEL_USE_COLOR', /BOOLEAN, $
            NAME='Use label color', $
            DESCRIPTION='Use provided label color instead of default', $
            /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_COLOR', /COLOR, $
            NAME='Label color', $
            DESCRIPTION='Color of labels', /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_FILL_BACKGROUND', $
            ENUMLIST=['False', 'True'], $
            NAME='Label fill background', $
            DESCRIPTION='Mode for label fill background', /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_FILL_COLOR', /COLOR, $
            NAME='Label fill color', $
            DESCRIPTION='Fill color for label background', /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_ANGLE', /FLOAT, $
            NAME='Label angle', $
            DESCRIPTION='Label angle', /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_ALIGN', /FLOAT, $
            NAME='Label alignment', $
            DESCRIPTION='Label alignment', /ADVANCED_ONLY

        self._oLine->RegisterProperty, 'LABEL_VALIGN', /FLOAT, $
            NAME='Label vertical align', $
            DESCRIPTION='Label vertical align', /ADVANCED_ONLY


    endif

    ; Property added in IDL64.
    if (registerAll || (updateFromVersion lt 640)) then begin
        self._oLine->RegisterProperty, 'ZVALUE', /FLOAT, $
            NAME='Z value', $
            DESCRIPTION='Z value for grid lines', /ADVANCED_ONLY
    endif

end

;----------------------------------------------------------------------------
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisMapGridline::Restore

    compile_opt idl2, hidden

    ; Call superclass restore.
    self->IDLitVisualization::Restore

    ; Register new properties.
    self->IDLitVisMapGridline::_RegisterProperties, $
        UPDATE_FROM_VERSION=self.idlitcomponentversion

end


;----------------------------------------------------------------------------
function MapGrid_LabelFunction, orientation, location, hasFraction, label

  compile_opt idl2, hidden

  cardinal = orientation ? $
    ((location lt 0) ? 'S' : 'N') : ((location lt 0) ? 'W' : 'E')

  ; Use Unicode for the degrees symbol, to avoid 8-bit ASCII problems.
  degsym = '!M' + STRING(176b)

  absloc = ABS(location)

  if (hasFraction) then begin
    degrees = FIX(absloc)
    minutes = FIX((absloc - degrees)*60)
    seconds = ROUND((absloc - degrees - minutes/60d)*3600)
    ; Watch out for round-off to exactly 60 seconds or 60 minutes
    if (seconds eq 60) then begin
      seconds = 0
      minutes++
    endif
    if (minutes eq 60) then begin
      minutes = 0
      degrees++
    endif
    minsym = '!M' + STRING(162b)
    secsym = '!M' + STRING(178b)
    ; Note that the degrees, minutes, & seconds symbols are 3 chars long.
    label = STRING(degrees, degsym, minutes, minsym, $
      seconds, secsym, cardinal, $
      FORMAT='(G0, A3, I2.2, A3, I2.2, A3, A1)')
  endif else begin
    loc = ROUND(absloc*10000)/10000d
    label = STRING(loc, degsym, cardinal, FORMAT='(g0,A3,A1)')
  endelse

  return, label
end


;----------------------------------------------------------------------------
pro IDLitVisMapGridline::_UpdateLabel

    compile_opt idl2, hidden

    ; This assumes that the VisMapGrid is the grandparent of Gridline.
    self->IDLitComponent::GetProperty, _PARENT=oParent
    if (~ISA(oParent)) then return
    oParent->IDLitComponent::GetProperty, _PARENT=oParent
    if (~ISA(oParent)) then return
    oParent->GetProperty, $
        GRID_LONGITUDE=gridLon, GRID_LATITUDE=gridLat, $
        LONGITUDE_MIN=lonMin, LATITUDE_MIN=latMin, $
        LONGITUDE_MAX=lonMax, LATITUDE_MAX=latMax
    ; If any of our gridlines have a fractional part, use DMS format.
    hasFraction = gridLon ne FIX(gridLon) || gridLat ne FIX(gridLat)

    loc = self._location

    if (self._orientation) then begin
      onBoundary = loc eq latMin || loc eq latMax
    endif else begin
      while (loc lt -180) do loc += 360
      while (loc gt 180) do loc -= 360
      onBoundary = loc eq lonMin || loc eq lonMax
      if (loc eq 0 && onBoundary) then begin
        sMap = self->GetProjection()
        if (N_TAGS(sMap) gt 0 && (sMap.ll_box[3] - sMap.ll_box[1]) ge 360) then begin
          ; Find the center latitude of the projection.
          latMiddle = 0.5*(sMap.ll_box[0] + sMap.ll_box[2])
          uv0 = MAP_PROJ_FORWARD([0, 360], [latMiddle, latMiddle], MAP=sMap)
          if (MIN(FINITE(uv0)) eq 1) then begin
            dist = TOTAL((uv0[*,1] - uv0[*,0])^2)
            if (dist lt 1d-3) then onBoundary = 0
          endif
        endif
      endif
    endelse

    if (STRMID(self._labelFormat,0,1) eq '(') then begin

      ; CT Note, Nov 2011: Using a format string is currently undocumented.
      ; In the future, we might want to support a real latlon format string,
      ; such as:
      ;    %D%d%M%m%S%s%C
      ; where %D is the degrees, %d is the degrees symbol
      ; %M is the minutes, %m is the minutes symbol
      ; %S is the seconds, %s is the seconds symbol
      ; %C is the Cardinal direction N/S/E/W
      ;
      ; Use Unicode for the degrees symbol, to avoid 8-bit ASCII problems.
      degsym = '!M' + STRING(176b)
      cardinal = self._orientation ? $
        ((loc lt 0) ? 'S' : 'N') : ((loc lt 0) ? 'W' : 'E')
      label = STRING(ABS(loc), degsym, cardinal, FORMAT=self._labelFormat)

    endif else begin
      
      label = MapGrid_LabelFunction(self._orientation, loc, hasFraction)

      if (self._labelFormat ne '') then begin
        label = CALL_FUNCTION(self._labelFormat, $
          self._orientation, loc, hasFraction, label[0])
      endif

    endelse

    self._oText->SetProperty, STRINGS=label
    
    ; IDL-68461: Remove labels that lie on the exact edge of the map,
    ; unless the label position is on the side (instead of the middle).
    if (self._labelShow) then begin
      labelShow = ~onBoundary || $
        self._labelPosition eq 0 || self._labelPosition eq 1
      self._oLine->SetProperty, $
        LABEL_OBJECTS=labelShow ? self._oText: OBJ_NEW()
    endif
end


;----------------------------------------------------------------------------
pro IDLitVisMapGridline::OnProjectionChange, sMap

    compile_opt idl2, hidden

    if (~N_ELEMENTS(sMap)) then $
        sMap = self->GetProjection()
    hasMap = N_TAGS(sMap) gt 0

    ; This assumes that the VisMapGrid is the grandparent of Gridline.
    self->IDLitComponent::GetProperty, _PARENT=oParent
    if (~ISA(oParent)) then return
    oParent->IDLitComponent::GetProperty, _PARENT=oParent
    if (~ISA(oParent)) then return
    oParent->GetProperty, $
        LONGITUDE_MIN=lonMin, $
        LONGITUDE_MAX=lonMax, $
        LATITUDE_MIN=latMin, $
        LATITUDE_MAX=latMax

    npts = (self._orientation) ? 180 : 90

    ; We must use the range of the opposite coordinate
    ; (latitude if lonlines, and vice versa) so that the gridlines
    ; don't extend past the ends of the other lines.
    range = (self._orientation) ? $
        [lonMin, lonMax] : [latMin, latMax]
    if (hasMap) then begin
      range[0] >= sMap.ll_box[self._orientation]
      range[1] <= sMap.ll_box[self._orientation+2]
    endif

    ; CT, VIS, March 2013: Be careful: If you change EPS, be sure to
    ; change the EPS within IDLitVisMapGrid::_GetFillHorizonData.
    EPS = 1d-5

    if (self._orientation) then begin
      if (range[0] eq -180) then range[0] += 180*EPS
      if (range[1] eq 180) then range[1] -= 180*EPS
    endif else begin
      if (range[0] eq -90) then range[0] += 90*EPS
      if (range[1] eq 90) then range[1] -= 90*EPS
    endelse

    points = DINDGEN(npts)*((range[1]-range[0])/(npts-1)) + $
        range[0]
    data = DBLARR(2, npts)
    location = self._location


    ; If our lat/lon line is on a map boundary,
    ; bump it slightly so it doesn't get clipped.
    if (self._orientation) then begin  ; latitude
        if ((hasMap && (location eq sMap.ll_box[0])) || location eq -90) then begin
            location += ABS(location)*EPS
        endif
        if ((hasMap && (location eq sMap.ll_box[2])) || location eq 90) then begin
            location -= ABS(location)*EPS
        endif
        data[0,*] = points
        data[1,*] = location
    endif else begin   ; longitude
        if (hasMap && (location eq sMap.ll_box[1])) then begin
            location += ABS(location)*EPS
        endif
        if (hasMap && (location eq sMap.ll_box[3])) then begin
            location -= ABS(location)*EPS
        endif
        data[0,*] = location
        data[1,*] = points
    endelse

    polylines = 0 ; clear out our polylines if no map proj

    doHide = 0b

    if (hasMap) then begin
      data = MAP_PROJ_FORWARD(TEMPORARY(data), $
        MAP=sMap, $
        POLYLINES=polylines)
      if (N_ELEMENTS(data) gt 1) then begin
        ; If all of our line points collapse down to a tiny point,
        ; then remove the gridline. This happens for +/-90 for
        ; Polar Stereographic for example.
        mn = MIN(data, MAX=mx, DIM=2)
        ; Ratio of the gridline extent to the whole map extent.
        delta = (mx - mn)/(sMap.uv_box[[2,3]] - sMap.uv_box[[0,1]])
        ; If gridline covers only a tiny fraction of the map extent, hide it.
        if (MAX(delta) lt 1d-4) then begin
          doHide = 1b
        endif
      endif

      if (N_ELEMENTS(data) lt 4) then begin
        self._oLine->SetProperty, /HIDE
        self->IDLitVisualization::SetProperty, IMPACTS_RANGE=0
        return
      endif
    endif

    self->_UpdateLabel

    self._oLine->SetProperty, HIDE=doHide, DATA=data, POLYLINES=polylines
    self->IDLitVisualization::SetProperty, IMPACTS_RANGE=~doHide

end


;---------------------------------------------------------------------------
; IDLitVisMapGridline::OnViewportChange
;
; Purpose:
;   This procedure method handles notification that the viewport
;   has changed.
;
; Arguments:
;   oSubject: A reference to the object sending notification of the
;     viewport change.
;
;   oDestination: A reference to the destination in which the view
;     appears.
;
;   viewportDims: A 2-element vector, [w,h], representing the new
;     width and height of the viewport (in pixels).
;
;   normViewDims: A 2-element vector, [w,h], representing the new
;     width and height of the visibile view (normalized relative to
;     the virtual canvas).
;
pro IDLitVisMapGridline::OnViewportChange, oSubject, oDestination, $
    viewportDims, normViewDims

    compile_opt idl2, hidden

    ; Check if destination zoom factor or normalized viewport has changed.
    ; If so, update the corresponding font properties.
    self._oFont->GetProperty, FONT_ZOOM=fontZoom, FONT_NORM=fontNorm
    if (OBJ_VALID(oDestination)) then $
        oDestination->GetProperty, CURRENT_ZOOM=zoomFactor $
    else $
        zoomFactor = 1.0

    normFactor = MIN(normViewDims)

    if ((fontZoom ne zoomFactor) || $
        (fontNorm ne normFactor)) then $
        self._oFont->SetProperty, FONT_ZOOM=zoomFactor, FONT_NORM=normFactor

    ; Allow superclass to notify all children.
    self->_IDLitVisualization::OnViewportChange, oSubject, oDestination, $
        viewportDims, normViewDims
end

;---------------------------------------------------------------------------
; IDLitVisMapGridline::OnViewZoom
;
; Purpose:
;   This procedure method handles notification that the view zoom factor
;   has changed
;
; Arguments:
;   oSubject: A reference to the object sending notification of the
;     view zoom factor change.
;
;   oDestination: A reference to the destination in which the view
;     appears.
;
;   viewZoom: The new zoom factor for the view.
;
pro IDLitVisMapGridline::OnViewZoom, oSubject, oDestination, viewZoom

    compile_opt idl2, hidden

    ; Check if view zoom factor has changed.  If so, update the font.
    self._oFont->GetProperty, VIEW_ZOOM=fontViewZoom

    if (fontViewZoom ne viewZoom) then $
        self._oFont->SetProperty, VIEW_ZOOM=viewZoom

    ; Allow superclass to notify all children.
    self->_IDLitVisualization::OnViewZoom, oSubject, oDestination, $
        viewZoom
end


;----------------------------------------------------------------------------
pro IDLitVisMapGridline::_GetAlignment, align, valign

  compile_opt idl2, hidden

  angle = self._labelAngle
  lpos = self._labelPosition

  ; If angle < 0 then we are ignoring the angle.
  if (angle lt 0) then begin
    valign = 0.5
    align = (lpos le 0) ? 1.25 : (lpos ge 1 ? -0.25 : 0.5)
    return
  endif

  if (lpos gt 0 && lpos lt 1) then begin  ; 0 > label position < 1
    align = 0.5
    valign = 0.5
    return
  endif
  
  ; Label position is exactly 0 or 1.

  if (self._orientation) then begin  ; latitude
    if (angle lt 20 || angle gt 340) then begin
      align = 1.25
      valign = 0.5
    endif else if (angle le 70 || angle ge 290) then begin
      align = 1.15
      valign = angle le 70 ? -0.2 : 1.2
    endif else if (angle lt 110 || angle ge 250) then begin
      align = 0.5
      valign = angle lt 110 ? -0.8 : 1.7
    endif else if (angle lt 160 || angle gt 200) then begin
      align = -0.1
      valign = angle lt 160 ? -0.3 : 1.3
    endif else begin   ; around 180 degrees
      align = -0.3
      valign = 0.5
    endelse
  endif else begin
    if (angle lt 20 || angle gt 340) then begin
      align = 0.5
      valign = 1.65
    endif else if (angle le 70 || angle ge 290) then begin
      align = angle le 70 ? 1.1 : -0.1
      valign = 1.25
    endif else if (angle lt 110 || angle ge 250) then begin
      align = angle lt 110 ? 1.25 : -0.2
      valign = 0.5
    endif else if (angle lt 160 || angle gt 200) then begin
      align = angle lt 160 ? 1.2 : -0.1
      valign = -0.3
    endif else begin   ; around 180 degrees
      align = 0.5
      valign = -0.6
    endelse
  endelse

  ; If labels are on the right then flip the alignments.
  if (lpos eq 1) then begin
    align = 1 - align
    valign = 0.9 - valign
  endif

end


;----------------------------------------------------------------------------
pro IDLitVisMapGridline::GetProperty, $
    LABEL_ALIGN=labelAlign, $
    LABEL_VALIGN=labelValign, $
    LABEL_ANGLE=labelAngle, $
    LABEL_COLOR=labelColor, $
    LABEL_FILL_BACKGROUND=labelFillBackground, $
    LABEL_FILL_COLOR=labelFillColor, $
    LABEL_FORMAT=labelFormat, $
    LABEL_POSITION=labelPosition, $
    LABEL_SHOW=labelShow, $
    LABEL_USE_COLOR=labelUseColor, $
    LOCATION=location, $
    ORIENTATION=orientation, $
    TRANSPARENCY=transparency, $
    ZVALUE=zvalue, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if (ARG_PRESENT(labelAlign)) then $
        self._oText->GetProperty, ALIGNMENT=labelAlign

    if (ARG_PRESENT(labelValign)) then $
        self._oText->GetProperty, VERTICAL_ALIGNMENT=labelValign

    if (ARG_PRESENT(labelAngle)) then $
        labelAngle = self._labelAngle

    if (ARG_PRESENT(labelColor)) then $
        self._oText->GetProperty, COLOR=labelColor

    if (ARG_PRESENT(labelFillBackground)) then $
        self._oText->GetProperty, FILL_BACKGROUND=labelFillBackground

    ; Note: -1 will automatically be converted by a PropSheet into
    if (ARG_PRESENT(labelFillColor)) then $
        self._oText->GetProperty, FILL_COLOR=labelFillColor

    if (ARG_PRESENT(labelFormat)) then $
        labelFormat = self._labelFormat

    if (ARG_PRESENT(labelPosition)) then $
        labelPosition = self._labelPosition

    if (ARG_PRESENT(labelShow)) then $
        labelShow = self._labelShow

    if (ARG_PRESENT(labelUseColor)) then begin
        self._oLine->GetProperty, USE_LABEL_COLOR=labelUseColor
        labelUseColor = labelUseColor[0]
    endif

    if ARG_PRESENT(location) then $
        location = self._location

    if ARG_PRESENT(orientation) then $
        orientation = self._orientation

    if ARG_PRESENT(transparency) then begin
        self._oLine->GetProperty, ALPHA_CHANNEL=alpha
        transparency = 100.0 - alpha * 100.0
    endif

    if (ARG_PRESENT(zvalue)) then $
        zvalue = self._zvalue

    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisualization::GetProperty, _EXTRA=_extra
end


;----------------------------------------------------------------------------
pro IDLitVisMapGridline::SetProperty, $
    CLIP=swallow, $
    COLOR=colorIn, $
    DATA=data, $
    LABEL_ALIGN=labelAlign, $
    LABEL_VALIGN=labelValign, $
    LABEL_ANGLE=labelAngle, $
    LABEL_COLOR=labelColorIn, $
    LABEL_FILL_BACKGROUND=labelFillBackground, $
    LABEL_FILL_COLOR=labelFillColorIn, $
    LABEL_FORMAT=labelFormat, $
    LABEL_POSITION=labelPosition, $
    LABEL_SHOW=labelShow, $
    LABEL_USE_COLOR=labelUseColor, $
    LINESTYLE=linestyleIn, $
    LOCATION=location, $
    ORIENTATION=orientation, $
    POLYLINES=polylines, $
    RANGE=range, $
    TRANSPARENCY=transparency, $
    ZVALUE=zvalue, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    self->IDLitComponent::GetProperty, INITIALIZING=isInit

    updateGrid = 0b

    if (N_ELEMENTS(location)) then begin
        self._location = location
        updateGrid = 1b
    endif

    if (N_ELEMENTS(orientation)) then begin
        self._orientation = KEYWORD_SET(orientation)
        updateGrid = 1b
    endif

    if (N_ELEMENTS(transparency)) then begin
        transparency = 0 > transparency < 100
        self._oLine->SetProperty, ALPHA_CHANNEL=(100.-transparency)/100
    endif


    if (N_ELEMENTS(labelFormat)) then begin
      if (labelFormat ne '' && $
        (STRMID(labelFormat,0,1) ne '(') && $
        ~IDL_VALIDNAME(labelFormat)) then begin
        MESSAGE, 'LABEL_FORMAT must be a valid IDL function name.'
      endif
      self._labelFormat = labelFormat
      if (~isInit) then self->_UpdateLabel
    endif

    if (N_ELEMENTS(labelPosition)) then begin
        self._labelPosition = labelPosition
        self._oLine->SetProperty, LABEL_OFFSETS=0 > labelPosition < 1
        ; Use the code in LABEL_ANGLE to fix the text alignment.
        if (~ISA(labelAngle)) then $
          labelAngle = self._labelAngle
        if (~isInit) then self->_UpdateLabel
    endif

    if (N_ELEMENTS(labelUseColor)) then begin
        self->SetPropertyAttribute, 'LABEL_COLOR', $
            SENSITIVE=self._labelShow && KEYWORD_SET(labelUseColor)
        self._oLine->SetProperty, $
            USE_LABEL_COLOR=KEYWORD_SET(labelUseColor)
    endif

    if (N_ELEMENTS(labelFillColorIn)) then begin
        labelFillColor = labelFillColorIn
        if (ISA(labelFillColorIn, 'STRING') || N_ELEMENTS(labelFillColorIn) eq 1) then $
          style_convert, labelFillColorIn, COLOR=labelFillColor
        self._oText->SetProperty, FILL_COLOR=labelFillColor
    endif

    if (N_ELEMENTS(labelFillBackground)) then begin
        self->SetPropertyAttribute, 'LABEL_FILL_COLOR', $
            SENSITIVE=self._labelShow && (labelFillBackground ge 1)
        self._oText->SetProperty, $
            FILL_BACKGROUND=(labelFillBackground gt 0)
    endif


    if (N_ELEMENTS(labelShow)) then begin

        self._labelShow = KEYWORD_SET(labelShow)

        self._oLine->SetProperty, $
            LABEL_OBJECTS=self._labelShow ? self._oText: OBJ_NEW()

        ; Turn on/off labelling properties.
        self->SetPropertyAttribute, $
            ['LABEL_USE_COLOR', 'LABEL_POSITION', $
            'LABEL_FILL_BACKGROUND', $
            'FONT_INDEX', 'FONT_STYLE', 'FONT_SIZE'], $
            SENSITIVE=self._labelShow

        ; Only turn on label color prop if LABEL_USE_COLOR is also set.
        self._oLine->GetProperty, USE_LABEL_COLOR=labelUseColor
        self->SetPropertyAttribute, 'LABEL_COLOR', $
            SENSITIVE=self._labelShow && labelUseColor

        self->IDLitVisMapGridline::GetProperty, $
            LABEL_FILL_BACKGROUND=labelFillBackground
        self->SetPropertyAttribute, 'LABEL_FILL_COLOR', $
            SENSITIVE=self._labelShow && (labelFillBackground ne 0)
    endif


    if (ISA(labelAngle)) then begin
      ; If a negative value is specified, just wrap around to positive.
      ; Note that -1 is a flag indicating "turn off label angle".
      if (labelAngle lt -1) then labelAngle += 360

      self._labelAngle = labelAngle

      self->_GetAlignment, align, valign

      if (labelAngle ge 0) then begin
        a = labelAngle*!DtoR
        baseline = [cos(a),sin(a),0]
        updir = [-sin(a),cos(a),0]
      endif

      self._oText->SetProperty, ALIGNMENT=align, VERTICAL_ALIGNMENT=valign, $
        BASELINE=baseline, UPDIR=updir
      self._oLine->SetProperty, USE_LABEL_ORIENTATION=labelAngle ge 0
    endif

    if (ISA(labelAlign)) then $
      self._oText->SetProperty, ALIGNMENT=labelAlign

    if (ISA(labelValign)) then $
      self._oText->SetProperty, VERTICAL_ALIGNMENT=labelValign

    if (ISA(linestyleIn)) then begin
      self._oLine->SetProperty, LINESTYLE=LINESTYLE_CONVERT(linestyleIn)
    endif

    if (ISA(colorIn)) then begin
        color = colorIn
        if (ISA(colorIn, 'STRING') || N_ELEMENTS(colorIn) eq 1) then $
          style_convert, colorIn, COLOR=color
        self._oLine->SetProperty, COLOR=color
        self._oLine->GetProperty, USE_LABEL_COLOR=labelUseColor
        if (~labelUseColor) then $
            self._oText->SetProperty, COLOR=color
    endif

    if (ISA(labelColorIn)) then begin
      labelColor = labelColorIn
      if (isa(labelColorIn, 'STRING') || N_ELEMENTS(labelColorIn) eq 1) then $
        style_convert, labelColorIn, COLOR=labelColor
      self._oLine->GetProperty, COLOR=lineColor, USE_LABEL_COLOR=labelUseColor
      if (~ARRAY_EQUAL(labelColor, lineColor)) then begin
        labelUseColor = 1
        self._oLine->SetProperty, /USE_LABEL_COLOR
      endif
      if (labelUseColor) then $
        self._oText->SetProperty, COLOR=labelColor
    endif

    if (N_ELEMENTS(zvalue) ne 0) then begin
        self._zvalue = zvalue
        self->IDLgrModel::GetProperty, TRANSFORM=transform
        transform[2,3] = zvalue
        self->IDLgrModel::SetProperty, TRANSFORM=transform
        ; put the visualization into 3D mode if necessary
        self->Set3D, (zvalue ne 0), /ALWAYS
    endif

    if (N_ELEMENTS(data) || N_ELEMENTS(polylines)) then $
        self._oLine->SetProperty, DATA=data, POLYLINES=polylines

    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisualization::SetProperty, _EXTRA=_extra


    if (updateGrid && ~isInit) then $
        self->OnProjectionChange

end


;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; IDLitVisMapGridline__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisMapGridline object.
;
;-
pro IDLitVisMapGridline__Define

    compile_opt idl2, hidden

    struct = { IDLitVisMapGridline,           $
        inherits IDLitVisualization,       $
        _oLine: OBJ_NEW(), $
        _oText: OBJ_NEW(), $
        _oFont: OBJ_NEW(), $
        _location: 0d, $
        _labelFormat: '', $
        _labelPosition: 0d, $
        _labelShow: 0b, $
        _orientation: 0b, $
        _labelAngle: 0d, $
        _zvalue: 0d $
        }
end
