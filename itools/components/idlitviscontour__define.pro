; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitviscontour__define.pro#1 $
;
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
; Purpose:
;   The IDLitVisContour class is the component wrapper for IDLgrContour
;

;----------------------------------------------------------------------------
; Purpose:
;    Initialize this component
;
; Syntax:
;
;    Obj = OBJ_NEW('IDLitVisContour'[, Z[, X, Y]])
;
; Result:
;   This function method returns 1 on success, or 0 on failure.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
function IDLitVisContour::Init, NAME=NAME,$
                        DESCRIPTION=desc,_REF_EXTRA=_extra

   compile_opt idl2, hidden

    ;; Initialize superclass
    if (self->IDLitVisualization::Init( $
        NAME=KEYWORD_SET(name) ? name : 'Contour', $
        DESCRIPTION=KEYWORD_SET(desc) ? desc :'Contour level visualization', $
        TYPE="IDLCONTOUR", $
        ICON='contour', $
        _EXTRA=_extra) ne 1) then $
     RETURN, 0

    self._oPalette = OBJ_NEW('IDLgrPalette')

   ; We assume that we are PLANAR initially.
    self._oContour = OBJ_NEW('IDLgrContour', /ANTIALIAS, /PLANAR, $
                        ; start with palette disconnected for default
                        ;PALETTE=self._oPalette, $
                        COLOR=[0,0,0], $
                        /HIDE, $
                        LABEL_THRESHOLD=0.2d, $
                        C_LABEL_INTERVAL=0.6d, $
                        C_USE_LABEL_ORIENTATION=2, $
                        /REGISTER_PROPERTIES, /PRIVATE)

    ;; Expose the properties for the standard IDL gr surface
    self->Add, self._oContour, /AGGREGATE

    self._oLevels = OBJ_NEW('IDLitVisContourContainer', $
        NAME='Contours', /PROPERTY_INTERSECTION)
    self._oLevels->SetPropertyAttribute, $
        ['NAME', 'DESCRIPTION', 'HIDE'], /HIDE

    ; CT: We temporarily aggregate our Contour Level container,
    ; so styles will pick up all the CL properties. Once we have
    ; data, we will remove the CL container from the aggregate,
    ; so that these properties won't show up in the property sheet
    ; for the contour. That way, in the Style Editor you can access
    ; all the level properties directly from contour.
    ; In the Vis Browser you can only change the level props via
    ; the multicolumn prop sheet.
    self->Add, self._oLevels, /AGGREGATE, /NO_UPDATE

    ; Add a generic "first" level, to pick up all the properties.
    oLevel = OBJ_NEW('IDLitVisContourLevel', NAME='Level 0', ID='Level_0', $
      FONT_SIZE=10)

    ; Retrieve the registered props so we can filter them in Get/SetProperty.
    registered = oLevel->QueryProperty()
    keep = Bytarr(N_ELEMENTS(registered))
    for i=0,N_ELEMENTS(registered)-1 do begin
      oLevel->GetPropertyAttribute, registered[i], HIDE=hidden
      ;; Filter out hidden properties
      keep[i] = ~hidden
    endfor
    registered = registered[Where(keep)]
    
    self._pProps = PTR_NEW(registered)

    self._oLevels->Add, oLevel, /AGGREGATE, /NO_UPDATE, /INTERNAL
    
    self->IDLitVisContour::_RegisterParameters
    self->IDLitVisContour::_RegisterProperties

    ; set defaults
    self._palColor = 0
    self._defaultIndices = 1
    self._tickinterval = 0.2d
    self._ticklen = 0.1d


    if (N_ELEMENTS(_extra) gt 0) then $
      self->IDLitVisContour::SetProperty, _EXTRA=_extra

    return, 1
end


;----------------------------------------------------------------------------
pro IDLitVisContour::_RegisterParameters

    compile_opt idl2, hidden

    self->RegisterParameter, 'Z', DESCRIPTION='Z Data', $
                            /INPUT, TYPES='IDLARRAY2D', /OPTARGET

    self->RegisterParameter, 'X', DESCRIPTION='X Data', $
                            /INPUT, TYPES=['IDLVECTOR','IDLARRAY2D'], $
                            /OPTIONAL

    self->RegisterParameter, 'Y', DESCRIPTION='Y Data', $
                            /INPUT, TYPES=['IDLVECTOR','IDLARRAY2D'], $
                            /OPTIONAL

    self->RegisterParameter, 'PALETTE', DESCRIPTION='RGB Color Table', $
        /INPUT, TYPES=['IDLPALETTE','IDLARRAY2D'], /OPTIONAL, /OPTARGET

    self->RegisterParameter, 'RGB_INDICES', DESCRIPTION='Color Table Indices', $
        /INPUT, TYPES='IDLVECTOR', /OPTIONAL

    self->RegisterParameter, 'VERTICES', DESCRIPTION='Contour Level Vertices', $
        INPUT=0, /OUTPUT, TYPES='IDLARRAY2D', /OPTIONAL

    self->RegisterParameter, 'CONNECTIVITY', DESCRIPTION='Contour Level Connectivity', $
        INPUT=0, /OUTPUT, TYPES='IDLVECTOR', /OPTIONAL

end


;----------------------------------------------------------------------------
; IDLitVisContour::_RegisterProperties
;
; Purpose:
;   This procedure method registers properties associated with this class.
;
; Calling sequence:
;   oObj->[IDLitVisContour::]_RegisterProperties
;
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLitVisContour::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

   compile_opt idl2, hidden

    registerAll = ~KEYWORD_SET(updateFromVersion)

    if (registerAll || (updateFromVersion lt 610)) then begin

        self->RegisterProperty, 'GRID_UNITS', $
            DESCRIPTION='Grid units', $
            NAME='Grid units', $
            ENUMLIST=['Not applicable','Meters','Degrees'], $
            SENSITIVE=0, /ADVANCED_ONLY

    endif


    if (registerAll) then begin

        self->RegisterProperty, 'CONTOUR_LEVELS', $
            USERDEF='Click to edit', $
            NAME='Contour level properties', $
            DESCRIPTION='Edit properties of individual contour levels'

        self->RegisterProperty, 'PAL_COLOR', /BOOLEAN, $
            DESCRIPTION='Use palette for level colors', $
            NAME='Use palette color', /ADVANCED_ONLY

        self->RegisterProperty, 'VISUALIZATION_PALETTE', /HIDE, $
            NAME='Levels color table', $
            USERDEF='Edit color table', $
            DESCRIPTION='Edit Levels Color Table', /ADVANCED_ONLY

        self->RegisterProperty, 'ZVALUE', /FLOAT, $
            NAME='Planar Z value', $
            DESCRIPTION='Z value on which to project contours.', /ADVANCED_ONLY

        ;; Override the PropertyDescriptor attributes.
        self->SetPropertyAttribute, /HIDE, $
            ['COLOR', 'DEPTH_OFFSET', 'PALETTE']

        self->SetPropertyAttribute, ['TICKLEN', 'TICKINTERVAL'], $
            VALID_RANGE=[0.01d,1,0.01d]

            ; For styles, hide these properties until we have data.
        self->SetPropertyAttribute, ['MIN_VALUE', 'MAX_VALUE'], /HIDE

        ; LABEL_COLOR, and FONT_* are needed for the new Graphics property buttons.
        self->RegisterProperty, 'LABEL_COLOR', /COLOR, /HIDE, $
            NAME='Label color', $
            DESCRIPTION='Color of labels'

        oFont = OBJ_NEW('IDLitFont')
        oFont->GetPropertyAttribute, 'FONT_INDEX', ENUMLIST=enumlist
        OBJ_DESTROY, oFont

        self->RegisterProperty, 'FONT_INDEX', /HIDE, $
            ENUMLIST=enumlist, $
            NAME='Text font', $
            DESCRIPTION='Font name'

        self->RegisterProperty, 'FONT_STYLE', /HIDE, $
            ENUMLIST=['Normal', 'Bold', 'Italic', 'Bold Italic'], $
            NAME='Text style', $
            DESCRIPTION='Font style'

        self->RegisterProperty, 'FONT_SIZE', /HIDE, /FLOAT, $
            NAME='Text font size', $
            DESCRIPTION='Font size in points'
    endif


    if (registerAll || (updateFromVersion lt 610)) then begin

        self->RegisterProperty, 'TRANSPARENCY', /INTEGER, $
            NAME='Transparency', $
            DESCRIPTION='Transparency of contour', $
            VALID_RANGE=[0,100,5]
        ; Use TRANSPARENCY property instead.
        self->SetPropertyAttribute, 'ALPHA_CHANNEL', /HIDE, /ADVANCED_ONLY

    endif


end


;----------------------------------------------------------------------------
; Purpose:
;    Cleanup this component
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
pro IDLitVisContour::Cleanup

    compile_opt idl2, hidden

    OBJ_DESTROY, self._oPalette

    ; Destroy any contained text/symbol objects.
    self._oContour->GetProperty, C_LABEL_OBJECTS=c_label_objects
    OBJ_DESTROY, c_label_objects

    ; Destroy our level objects.
    OBJ_DESTROY, self._oLevels

    PTR_FREE, self._pProps
    PTR_FREE, self._pShow
    PTR_FREE, self._pLinestyle
    PTR_FREE, self._pThick

    ; Cleanup superclass
    self->IDLitVisualization::Cleanup
end


;----------------------------------------------------------------------------
; IDLitVisContour::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisContour::Restore
    compile_opt idl2, hidden

    ; Call superclass restore.
    self->_IDLitVisualization::Restore

    ; Call ::GetProperty on each aggregated graphic object
    ; to force its internal restore process to be called, thereby
    ; ensuring any new properties are registered.
    if (OBJ_VALID(self._oContour)) then $
        self._oContour->GetProperty

    ; Register new properties.
    self->IDLitVisContour::_RegisterProperties, $
        UPDATE_FROM_VERSION=self.idlitcomponentversion

    oContour = self._oContour
    if (Ptr_Valid(oContour.data)) then begin
      self._maxValue = MAX(*oContour.data, /NAN, MIN=minn)
      self._minValue = minn
    endif
end


;;----------------------------------------------------------------------------
;; Purpose:
;;   This function method is used to cyclically fill in parameters if needed
;;
;; Arguments:
;;   PARAMETERNAME - String name of registered parameter
;;
;;   PARAMETER - Old value of parameter.  Values will be cyclically
;;               repeated as needed.
;;
;; Keywords:
;;   FORCE2D - set if the parameter is supposed to be a 2D array.  This
;;          allows for handling the case of one colour which seems to
;;          be a 3 element vector instead of a 3x1 array.
;;
function IDLitVisContour::_FillParameter, parameterName, parameter, nLevels, $
  FORCE2D=force2D
  compile_opt idl2, hidden

  ; ensure that all parameters are arrays
  if N_ELEMENTS(parameter) eq 1 then $
    parameter = REFORM(parameter,1)
    
  ; ensure that what should be 2D is 2D
  twoD = KEYWORD_SET(force2D) || SIZE(parameter,/N_DIM) eq 2
  if (twod && SIZE(parameter,/N_DIMENSIONS) lt 2) then begin
    parameter = REFORM(parameter, N_ELEMENTS(parameter),1)
  endif
  
  ; if needed, make new array and fill in cyclically
  sz = SIZE(parameter)
  oldSize = sz[sz[0]]
  if oldSize lt nLevels then begin
    sz[sz[0]] = nLevels
    newParameter = MAKE_ARRAY(SIZE=sz)
    if (twod) then begin
      for i=0,nLevels-1 do $
        newParameter[*,i] = parameter[*,i mod oldSize]
    endif else begin
      for i=0,nLevels-1 do $
        newParameter[i] = parameter[i mod oldsize]
    endelse
    return, newParameter
  endif
  
  return, !NULL
END


;----------------------------------------------------------------------------
pro IDLitVisContour::_FillAllParameters, nLevels

  compile_opt idl2, hidden

  if (nLevels le 1) then return

  self._oContour->GetProperty, $
    C_COLOR=cColorOld, $
    C_LABEL_INTERVAL=cLabelIntOld, $
    C_LABEL_NOGAPS=cLabelGapsOld, $
    C_LABEL_SHOW=cLabelShowOld, $
    C_LINESTYLE=cLinestyleOld, $
    C_THICK=cThickOld, $
    C_USE_LABEL_COLOR=cLabelColorOld, $
    C_USE_LABEL_ORIENTATION=cLabelOriOld

  if ~PTR_VALID(self._pShow) then $
    self._pShow = PTR_NEW(cLabelShowOld)
  cLabelShowOld = *self._pShow

  if ~PTR_VALID(self._pLinestyle) then $
    self._pLinestyle = PTR_NEW(cLinestyleOld)
  cLinestyleOld = *self._pLinestyle

  if ~PTR_VALID(self._pThick) then $
    self._pThick = PTR_NEW(cThickOld)
  cThickOld = *self._pThick

  hasVal = 0
  if (N_ELEMENTS(cColorOld) gt 1) then begin
    newVal = self->_FillParameter('C_COLOR',cColorOld,nLevels, $
      FORCE2D=~self._palColor && N_ELEMENTS(cColorOld) eq 3)
    hasVal += ISA(newVal)
    if (ISA(newVal)) then cColor = newVal
  endif

  newVal = self->_FillParameter('C_LABEL_INTERVAL',cLabelIntOld,nLevels)
  hasVal += ISA(newVal)
  if (ISA(newVal)) then cLabelInt = newVal

  newVal = self->_FillParameter('C_LABEL_NOGAPS',cLabelGapsOld,nLevels)
  hasVal += ISA(newVal)
  if (ISA(newVal)) then cLabelGaps = newVal

  newVal = self->_FillParameter('C_LABEL_SHOW',cLabelShowOld,nLevels)
  hasVal += ISA(newVal)
  if (ISA(newVal)) then cLabelShow = newVal

  if (N_ELEMENTS(cLinestyleOld) ne 1 || cLinestyleOld ne -1) then begin
    newVal = self->_FillParameter('C_LINESTYLE',cLinestyleOld,nLevels)
    hasVal += ISA(newVal)
    if (ISA(newVal)) then cLinestyle = newVal
  endif

  if (N_ELEMENTS(cThickOld) ne 1 || cThickOld ne -1) then begin
    newVal = self->_FillParameter('C_THICK',cThickOld,nLevels)
    hasVal += ISA(newVal)
    if (ISA(newVal)) then cThick = newVal
  endif

  newVal = self->_FillParameter('C_USE_LABEL_COLOR',cLabelColorOld,nLevels)
  hasVal += ISA(newVal)
  if (ISA(newVal)) then cLabelColor = newVal

  newVal = self->_FillParameter('C_USE_LABEL_ORIENTATION',cLabelOriOld,nLevels)
  hasVal += ISA(newVal)
  if (ISA(newVal)) then cLabelOri = newVal

  if (hasVal gt 0) then begin
    self._oContour->SetProperty, $
      C_COLOR=cColor, $
      C_LABEL_INTERVAL=cLabelInt, $
      C_LABEL_NOGAPS=cLabelGaps, $
      C_LABEL_SHOW=cLabelShow, $
      C_LINESTYLE=cLinestyle, $
      C_THICK=cThick, $
      C_USE_LABEL_COLOR=cLabelColor, $
      C_USE_LABEL_ORIENTATION=cLabelOri
  endif
end


;----------------------------------------------------------------------------
; Purpose:
;   This function method is used to retrieve the contourLevel objects.
;
; Arguments:
;   None
;
; Keywords:
;   None.
;
function IDLitVisContour::_GetLevels, N_LEVELS=nlevels

    compile_opt idl2, hidden

    if (~N_ELEMENTS(nlevels) || nlevels le 0) then begin
      self->GetProperty, N_LEVELS=nlevels, C_VALUE=c_value
      if ((nlevels le 1) && (c_value[0] eq -1)) then $
        return, OBJ_NEW()
    endif

    oLevels = self._oLevels->Get(/ALL, ISA='IDLitVisContourLevel', $
        COUNT=ncontained)

    oTool = self->GetTool()

    ; Since we didn't set the tool in init.
    self._oLevels->_SetTool, oTool

    ; Retrieve only the first nlevels items from the container.
    if (ncontained gt 0) then begin
        n = (nlevels < ncontained)
        oLevels = oLevels[0:n-1]
        props = oLevels[0]->QueryProperty()
        for i=0,N_ELEMENTS(props)-1 do begin
            oLevels[0]->GetPropertyAttribute, props[i], HIDE=hide
            if (hide) then $
                props[i] = ''
        endfor
        for i=0,n-1 do begin
            ; We didn't set the tool in init on the first level.
            oLevels[i]->_SetTool, oTool
            oLevels[i]->GetProperty, _CONTOUR=oContour
            oLevels[i]->SetProperty, _CONTOUR=self._oContour, $
                INDEX=i, $
                NAME='Level ' + STRTRIM(i, 2)
            self->DoOnNotify, oLevels[i]->GetFullIdentifier(), 'SETPROPERTY', 'NAME'
        endfor
    endif

    if (ncontained lt nlevels) then begin
        ; Need to create some objects.
        for i=ncontained, nlevels-1 do begin
            oLevel = OBJ_NEW('IDLitVisContourLevel', $
                NAME='Level ' + STRTRIM(i,2), $
                ID='Level_' + STRTRIM(i,2), $
                INDEX=i, TOOL=oTool, $
                _CONTOUR=self._oContour)

            ; Concat onto list of levels to return.
            oLevels = (i gt 0) ? [oLevels, oLevel] : oLevel

            self._oLevels->Add, oLevel, /AGGREGATE, /NO_UPDATE, /INTERNAL

            ; Copy properties from my first level over to the new one.
            for p=0,N_ELEMENTS(props)-1 do begin
                ; Do not copy the value, causes vertex problems.
                if (~props[p] || props[p] eq 'VALUE') then $
                    continue
                if (oLevels[0]->GetPropertyByIdentifier(props[p], value)) then $
                    oLevel->SetPropertyByIdentifier, props[p], value
            endfor

        endfor

    endif

    return, oLevels

end


;----------------------------------------------------------------------------
; Purpose:
;   This procedure method is used to create default indices into the
;   color table. Indices correspond to the location of the contour levels
;   within the data range scaled to the range of the color table.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
;----------------------------------------------------------------------------
pro IDLitVisContour::_UpdateDefaultIndices

  compile_opt idl2, hidden

  if (~self._palColor || ~self._defaultIndices) then $
    return

  ; Don't call directly on the IDLgrContour. We may have an extra fill level.
  self->GetProperty, C_VALUE=cValue

  ; scale indices from location of contour levels into byte range.
  ; Use a slighly shorter byte range to avoid colors at the ends of the table.
  indices = BYTSCL(cValue, TOP=235) + 10b

  self->IDLitVisContour::SetProperty, C_COLOR=indices
  ; Turn on this flag after our ::SetProperty call, so it doesn't
  ; get overwritten.
  self._defaultIndices = 1b

end


;----------------------------------------------------------------------------
; Purpose:
;   This procedure method is used to load color values into the
;   color table. RGB values may come from a parameter defined
;   by the user or default values from a grayscale palette.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
;----------------------------------------------------------------------------
pro IDLitVisContour::_LoadPalette

    compile_opt idl2, hidden

    oDataRGB = self->GetParameter('PALETTE')
    if ~OBJ_VALID(oDataRGB) then begin
      ; define default palette, allows editing color table
        ramp = BINDGEN(256)
        colors = transpose([[ramp],[ramp],[ramp]])
        oDataRGB = OBJ_NEW('IDLitDataIDLPalette', colors, NAME='RGB Table')
    endif else begin
        ; reset even if parameter already existed in order to reconnect palette
        ; and update parameter for export
        success = oDataRGB->GetData(colors)
        oDataRGB = OBJ_NEW('IDLitDataIDLPalette', colors, NAME='RGB Table')
    endelse
    ; connect palette
    result = self->SetData(oDataRGB, PARAMETER_NAME='PALETTE',/by_value)

end


;----------------------------------------------------------------------------
; Param = 'X' or 'Y'
;
function IDLitVisContour::_GetXYdata, param

    compile_opt idl2, hidden

    oParamZ = self->GetParameter('Z')
    if (~OBJ_VALID(oParamZ) || ~oParamZ->GetData(pData, /POINTER)) then $
        return, 0

    dims = SIZE(*pData, /DIMENSIONS)
    dimWant = (param eq 'X') ? dims[0] : dims[1]

    oParamXY = self->GetParameter(param)
    if (~OBJ_VALID(oParamXY) || ~oParamXY->GetData(xyData)) then begin
        xyData = FINDGEN(dimWant)
    endif

    minn = MIN(xyData, MAX=maxx)
    ndimXY = SIZE(xyData, /N_DIMENSIONS)
    if ((ndimXY eq 1 && N_ELEMENTS(xyData) ne dimWant) || $
        (ndimXY eq 2 && ~ARRAY_EQUAL(SIZE(xyData, /DIM), dims)) || $
        (minn eq maxx)) then begin
        xyData = FINDGEN(dimWant)
    endif

    ; We have our X or Y data. Now check for logarithmic.

    isLog = (param eq 'X') ? self._xVisLog : self._yVisLog

    ; Turn off log if values are zero or negative.
    if (minn le 0) then $
        isLog = 0

    ; Take log if necessary.
    if (isLog) then $
        xyData = ALOG10(TEMPORARY(xyData))

    ; May need to disable log.
    if (param eq 'X') then begin
        self._xVisLog = isLog
    endif else begin
        self._yVisLog = isLog
    endelse

    return, xyData

end


;----------------------------------------------------------------------------
; Force a recalculation of the label values.
;
pro IDLitVisContour::_RecalculateLabelValues

  compile_opt idl2, hidden

  self->IDLitVisContour::GetProperty, HAS_LABEL_OBJECTS=hasLabelObjects
  if (~hasLabelObjects) then begin
    oLevels = self._oLevels->Get(/ALL, ISA='IDLitVisContourLevel', COUNT=nLevels)
    show = PTR_VALID(self._pShow) ? *self._pShow : 0
    nshow = N_ELEMENTS(show)
    for i=0,nLevels-1 do begin
      ; Set LABEL_TYPE=1 (value labels) if c_label_show is true
      oLevels[i]->GetProperty, LABEL_TYPE=labelType
      ; If SHOW=1, then make sure our label is turned on (either 1,2, or 3)
      ; If SHOW=0, then turn off label.
      labelType = show[i mod nshow] ? (labelType > 1) : 0
      if (labelType eq 0 || labelType eq 1) then $
        oLevels[i]->SetProperty, LABEL_TYPE=labelType
    endfor
  endif
end


;----------------------------------------------------------------------------
function IDLitVisContour::_ComputeContourValues, n_levels

  compile_opt idl2, hidden

  ; For N_LEVELS we need to reset the current C_VALUE.
  self._oContour->GetProperty, FILL=filled, MIN_VALUE=minV, MAX_VALUE=maxV

  if (minV lt -1d300 || maxV gt 1d300) then begin
    minV = 0
    maxV = 1
  endif

  if (minV eq maxV) then return, REPLICATE(minV, n_levels ? n_levels : 10)

  oAxis = OBJ_NEW('IDLgrAxis', RANGE=[minV, maxV])

  if (n_levels le 0) then begin    
    maxlen = 999
    for nmajor=15,7,-1 do begin
      oAxis->SetProperty, MAJOR=nmajor
      oAxis->GetProperty, TICKV=tickvtmp
      d1 = STRING(tickvtmp[-1] - tickvtmp[[-2]], FORMAT='(G0)')
      if (STRLEN(d1) lt maxlen) then begin
        maxlen = STRLEN(d1)
        tickv = tickvtmp
      endif
    endfor
    keep = WHERE(tickv gt minV and tickv lt maxV)
    c_value = tickv[keep]
    n_levels = N_ELEMENTS(c_value)
  endif else begin
    nmajor = n_levels
    while (nmajor le 2*n_levels) do begin
      oAxis->SetProperty, MAJOR=nmajor
      oAxis->GetProperty, TICKV=tickv
      keep = WHERE(tickv gt minV and tickv lt maxV, count)
      if (count ge (n_levels-filled)) then break
      nmajor++
    endwhile
    c_value = tickv[keep[0:((n_levels-filled)<count)-1]]
  endelse

  if (c_value[0] ne minV && (filled || N_ELEMENTS(c_value) lt n_levels)) then $
    c_value = [minV, c_value]
  OBJ_DESTROY, oAxis
  
  return, c_value

end


;----------------------------------------------------------------------------
; IIDLProperty Interface
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
; Purpose:
;   This procedure method retrieves the
;   value of a property or group of properties.
;
; Arguments:
;   None.
;
; Keywords:
;   Any keyword to IDLitVisContour::Init followed by the word "Get"
;   can be retrieved using this method.
;
pro IDLitVisContour::GetProperty, $
    CONTOUR_LEVELS=oLevels, $
    C_COLOR=cColor, $
    C_USE_LABEL_ORIENTATION=cUseOrient, $
    C_VALUE=cValue, $
    EQUATION=equation, $
    EQN_SAMPLES=eqnSamples, $
    EQN_USERDATA=eqnUserdata, $
    GRID_UNITS=gridUnits, $
    HIDE=hide, $
    FONT_INDEX=fontIndex, $
    FONT_NAME=fontName, $
    FONT_SIZE=fontSize, $
    FONT_STYLE=fontStyle, $
    HAS_LABEL_OBJECTS=hasLabelObjects, $
    LABEL_COLOR=labelColor, $
    LABEL_FORMAT=labelFormat, $
    MIN_VALUE=minValue, $
    MAX_VALUE=maxValue, $
    N_LEVELS=n_levels, $
    PAL_COLOR=palColor, $
    RGB_TABLE=rgbTable, $
    RGB_INDICES=rgbIndices, $
    TICKINTERVAL=tickinterval, $
    TICKLEN=ticklen, $
    TRANSPARENCY=transparency, $
    VISUALIZATION_PALETTE=visPalette, $
    X_VIS_LOG=xVisLog, $
    Y_VIS_LOG=yVisLog, $
    Z_VIS_LOG=zVisLog, $
    ZVALUE=zvalue, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; For C_COLOR, always return a 3xN array of RGB values.
    if (ARG_PRESENT(cColor)) then begin
      self._oContour->GetProperty, COLOR=color, C_COLOR=cColor
      if (N_ELEMENTS(cColor) eq 1) then begin
        ; If a single element, just fill in from the COLOR property.
        self._oContour->GetProperty, N_LEVELS=nLevels, C_VALUE=cValue
        hasExtraFillLevel = (N_ELEMENTS(cValue) gt 1) && cValue[-1] eq self._maxValue
        if (hasExtraFillLevel) then nLevels--
        cColor = REBIN(color, N_ELEMENTS(color), nLevels)
      endif else if (SIZE(cColor, /N_DIMENSIONS) eq 1) then begin
        ; For indices, just fill in from the palette.
        self._oPalette->GetProperty, BLUE_VALUES=blue, $
            GREEN_VALUES=green, RED_VALUES=red
        cColor = TRANSPOSE([[red[cColor]], [green[cColor]], [blue[cColor]]])
      endif
    endif

    if (ARG_PRESENT(cUseOrient)) then begin
      self._oContour->GetProperty, C_USE_LABEL_ORIENTATION=cUseOrient
      ; Switch from internal undocumented value 2 to 0.
      cUseOrient[WHERE(cUseOrient eq 2, /NULL)] = 0
    endif
    
    if ARG_PRESENT(equation) then begin
      equation = self._equation
    endif

    if ARG_PRESENT(eqnSamples) then begin
      eqnSamples = self._eqnSamples
    endif
    
    if ARG_PRESENT(eqnUserdata) then begin
      eqnUserdata = PTR_VALID(self._eqnUserdata) ? *self._eqnUserdata : !NULL
    endif
    
    if (ARG_PRESENT(gridUnits)) then $
        gridUnits = self._gridUnits

    if (ARG_PRESENT(hasLabelObjects)) then $
        hasLabelObjects = PTR_VALID(self._pLabelObjects) && OBJ_VALID((*self._pLabelObjects)[0])

    if (ARG_PRESENT(hide)) then self->IDLgrModel::GetProperty, HIDE=hide

    ; LABEL_COLOR, and FONT_* are needed for the new Graphics property buttons.
    if (ARG_PRESENT(fontIndex) || ARG_PRESENT(fontName) || $
      ARG_PRESENT(fontSize) || ARG_PRESENT(fontStyle)) then begin
      self._oLevels->GetProperty, FONT_INDEX=fontIndex, FONT_NAME=fontName, $
        FONT_SIZE=fontSize, FONT_STYLE=fontStyle
    endif

    if (ARG_PRESENT(labelColor)) then begin
      self._oLevels->GetProperty, LABEL_COLOR=labelColor
    endif

    if (ARG_PRESENT(labelFormat)) then begin
      self._oLevels->GetProperty, TICKFORMAT=labelFormat
    endif

    if ARG_PRESENT(n_levels) then begin
      n_levels = self._nLevels
    endif

    if (Arg_Present(minValue) ne 0) then begin
        self._oContour->GetProperty, MIN_VALUE=minValue
        if (self._zVisLog) then minValue = 10^minValue
    endif

    if (Arg_Present(maxValue) ne 0) then begin
        self._oContour->GetProperty, MAX_VALUE=maxValue
        if (self._zVisLog) then maxValue = 10^maxValue
    endif

    if ARG_PRESENT(palColor) then begin
        palColor = self._palColor
    endif

    if ARG_PRESENT(transparency) then begin
        self._oContour->GetProperty, ALPHA_CHANNEL=alpha
        transparency = 0 > ROUND(100 - alpha*100) < 100
    endif

    if ARG_PRESENT(tickinterval) then $
        tickinterval = self._tickinterval

    if ARG_PRESENT(ticklen) then $
        ticklen = self._ticklen

    if ARG_PRESENT(zvalue) then $
        zvalue = self._zvalue

    if ARG_PRESENT(oLevels) then $
        oLevels = self->_GetLevels()

    IF ARG_PRESENT(cValue) THEN BEGIN
      self._oContour->GetProperty, N_LEVELS=nlevelsTmp, C_VALUE=cValue
      hasExtraFillLevel = (N_ELEMENTS(cValue) gt 1) && cValue[-1] eq self._maxValue
      if (hasExtraFillLevel) then cValue = cValue[0:-2]
    ENDIF

    if (Arg_Present(xVisLog)) then $
        xVisLog = self._xVisLog

    if (Arg_Present(yVisLog)) then $
        yVisLog = self._yVisLog

    if (Arg_Present(zVisLog)) then $
        zVisLog = self._zVisLog
        
    if ARG_PRESENT(rgbTable) || ARG_PRESENT(visPalette) then begin
        self._oPalette->GetProperty, BLUE_VALUES=blue, $
            GREEN_VALUES=green, RED_VALUES=red
        visPalette = TRANSPOSE([[red], [green], [blue]])
        rgbTable = visPalette
    endif
    
    if ARG_PRESENT(rgbIndices) then begin
      indices = self._oParameterSet->GetByName("RGB_INDICES")
      if (ISA(indices)) then begin
        !null = indices.GetData(rgbIndices)
      endif else begin
        self._oContour->GetProperty, C_COLOR=rgbIndices, $
          N_LEVELS=nlevelsTmp, C_VALUE=cValue
        if (SIZE(rgbIndices, /N_DIMENSIONS) eq 1) then begin
          hasExtraFillLevel = (N_ELEMENTS(cValue) gt 1) && cValue[-1] eq self._maxValue
          if (hasExtraFillLevel) then rgbIndices = rgbIndices[0:-2]
        endif else begin
          rgbIndices = BYTSCL(cValue, TOP=235) + 10b
        endelse
      endelse
    endif

    if (N_ELEMENTS(_extra) gt 0) then begin

        ; Pass properties directly to Contour Level container.
        ; We want to treat the CL container as if it is aggregated, but
        ; we don't actually aggregate it because we don't want the props
        ; to show up in the Contour property sheet.
        ; See Note in ::Init.
        levelProps = WHERE(_extra eq *self._pProps, nProps, $
            COMPLEMENT=otherProps, NCOMP=nOther)
        if (nOther gt 0) then begin
            ; get IDLgrContour properties
            self._oContour->GetProperty, _EXTRA=_extra[otherProps]
            ; get superclass properties
            self->IDLitVisualization::GetProperty, _EXTRA=_extra[otherProps]
        endif
        if (nProps gt 0) then begin
            self._oLevels->GetProperty, _EXTRA=_extra[levelProps]
        endif

    endif

end


;----------------------------------------------------------------------------
; Purpose:
;   This procedure method sets the
;   value of a property or group of properties.
;
; Arguments:
;   None.
;
; Keywords:
;   Any keyword to IDLitVisSurface::Init followed by the word "Set"
;   can be set using this method.
;
pro IDLitVisContour::SetProperty, $
    EQUATION=equation, $
    EQN_SAMPLES=eqnSamples, $
    EQN_USERDATA=eqnUserdata, $
    GRID_UNITS=gridUnitsIn, $
    TRANSPARENCY=transparency, $
    PAL_COLOR=palColor, $
    FILL=filled, $
    FONT_COLOR=fontColor, $  ; needed for command-line graphics
    FONT_INDEX=fontIndex, $
    FONT_NAME=fontName, $
    FONT_SIZE=fontSize, $
    FONT_STYLE=fontStyle, $
    LABEL_COLOR=labelColor, $
    LABEL_FORMAT=labelFormat, $
    MIN_VALUE=minValue, $
    MAX_VALUE=maxValue, $
    N_LEVELS=n_levels, $
    PLANAR=planar, $
    RGB_INDICES=rgbIndicesIn, $
    RGB_TABLE=rgbTableIn, $
    TICKINTERVAL=tickinterval, $
    TICKLEN=ticklen, $
    ZVALUE=zvalue, $
    C_COLOR=c_color, $
    C_FILL_PATTERN=c_fill_pattern, $
    C_LABEL_INTERVAL=c_label_interval, $
    C_LABEL_NOGAPS=c_label_nogaps, $
    C_LABEL_OBJECTS=c_label_objects, $
    C_LABEL_SHOW=c_label_show, $
    C_LINESTYLE=c_linestyle, $
    C_THICK=c_thick, $
    C_USE_LABEL_COLOR=c_use_label_color, $
    C_USE_LABEL_ORIENTATION=c_use_label_orientation, $
    C_VALUE=C_VALUE, $
    COLOR=singleColor, $
    DOWNHILL=downhill, $
    VISUALIZATION_PALETTE=visPalette, $
    X_VIS_LOG=xVisLog, $
    Y_VIS_LOG=yVisLog, $
    Z_VIS_LOG=zVisLog, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    self->IDLitComponent::GetProperty, INITIALIZING=isInit

    if (ISA(equation) || ISA(eqnSamples) || $
      ISA(eqnUserdata) || ISA(eqnUserdata, /NULL)) then begin
      if (ISA(equation)) then begin
        eqn = STRLOWCASE(equation)
        if (~ISA(equation, 'STRING') || $
          STRPOS(eqn, '&') ge 0 || STRPOS(eqn, 'execute') ge 0) then begin
          MESSAGE, 'Illegal value for EQUATION property.'
        endif
        self._equation = equation
      endif
      if (ISA(eqnSamples)) then begin
        self._eqnSamples = eqnSamples > 0
      endif
      if (ISA(eqnUserdata) || ISA(eqnUserdata, /NULL)) then begin
        self._eqnUserdata = ISA(eqnUserdata) ? PTR_NEW(eqnUserdata) : PTR_NEW()
      endif
      self._xrange = [0,0]
      self._yrange = [0,0]
      self->_UpdateData
    endif
    
    if (N_ELEMENTS(gridUnitsIn) ne 0) then begin
        if (ISA(gridUnitsIn, 'STRING')) then begin
          case (STRLOWCASE(gridUnitsIn)) of
          '': gridUnits = 0
          'm': gridUnits = 1
          'meters': gridUnits = 1
          'deg': gridUnits = 2
          'degrees': gridUnits = 2
          else: MESSAGE, 'Illegal value for GRID_UNITS.'
          endcase
        endif else begin
          gridUnits = gridUnitsIn
        endelse

        self._gridUnits = gridUnits

        ; Change to isotropic for units = meters or degrees.
        ; Do this before the OnProjectionChange.
        wasIsotropic = self->IsIsotropic()
        isIsotropic = gridUnits eq 1 || gridUnits eq 2
        if (wasIsotropic ne isIsotropic) then begin
            self->IDLitVisualization::SetProperty, ISOTROPIC=isIsotropic
        endif

        ; If units changed we may need to recalculate our contours.
        self->OnProjectionChange

        ; If isotropy changed then update our dataspace as well.
        if (wasIsotropic ne isIsotropic) then begin
            self->OnDataChange, self
            self->OnDataComplete, self
        endif

    endif


    if (N_ELEMENTS(transparency)) then begin
        self._oContour->SetProperty, $
            ALPHA_CHANNEL=0 > ((100.-transparency)/100) < 1
        ; Notify observers like the colorbar.
        self->DoOnNotify, self->GetFullIdentifier(), 'IMAGECHANGED', 0
    endif

    if (N_ELEMENTS(palColor) gt 0) then begin
        self._palColor = palColor
        self->SetPropertyAttribute, HIDE=~palColor, 'VISUALIZATION_PALETTE'
        if keyword_set(palColor) then begin
            ; switch to palette colors, using current palette and indices
            ; create the default palette and indices if necessary
            self->_LoadPalette
            self->_UpdateDefaultIndices
        endif else begin
            ; extract current colors and load these triplets directly
            ; to allow individual modifications to individual levels
            self._oContour->GetProperty, C_COLOR=cColorOld, $
                N_LEVELS=nLevels, $
                PALETTE=oldPalette
            if (MIN(cColorOld) ge 0 && OBJ_VALID(oldPalette)) then begin
                nColors = N_ELEMENTS(cColorOld)
                newColors = BYTARR(3,nLevels)
                self->GetProperty, $
                    VISUALIZATION_PALETTE=visPaletteTmp
                for i=0, nLevels-1 do begin
                    newColors[*,i] = reform(visPaletteTmp[*,cColorOld[i mod nColors]])
                endfor
                ; disconnect self._oPalette from the contour
                ; to allow rgb values to be set through c_color
                ; it can be reconnected if pal_color is set
                self._oContour->SetProperty, PALETTE=OBJ_NEW()
                self._oContour->SetProperty, C_COLOR=newColors
            endif
        endelse
    endif


    if (ISA(downhill)) then begin
      self._oContour->SetProperty, DOWNHILL=KEYWORD_SET(downhill)
      ; Force a recomputation (below)
      if (KEYWORD_SET(downhill)) then begin
        if (~ISA(tickinterval)) then $
          tickinterval = self._tickinterval
        if (~ISA(ticklen)) then $
          ticklen = self._ticklen
      endif
    endif


    ; TICKINVERVAL or TICKLEN
    if N_ELEMENTS(tickinterval) || N_ELEMENTS(ticklen) then begin
        ; Scale from normalized range to x/yrange.
        self._oContour->GetProperty, XRANGE=xr, YRANGE=yr
        maxrange = ABS(xr[1] - xr[0]) > ABS(yr[1] - yr[0])

        if (N_ELEMENTS(tickinterval)) then begin
            self._tickinterval = tickinterval
            scaledTickinterval = 0.5d*maxrange*tickinterval
        endif
        if (N_ELEMENTS(ticklen)) then begin
            self._ticklen = ticklen
            scaledTicklen = 0.5d*maxrange*ticklen
        endif
        self._oContour->SetProperty, $
            TICKINTERVAL=scaledTickinterval, $
            TICKLEN=scaledTicklen
    endif


    if (N_ELEMENTS(filled)) then begin
      self._oContour->SetProperty, FILL=filled
      self._oContour->SetPropertyAttribute, 'SHADING', $
        SENSITIVE=KEYWORD_SET(filled)
      self._oContour->GetProperty, C_VALUE=cValueCurrent
      if (filled && (N_ELEMENTS(cValueCurrent) gt 1) && $
        (cValueCurrent[-1] lt self._maxValue)) then begin
        cValues = [cValueCurrent, self._maxValue]
        self._oContour->SetProperty, C_VALUE=cValues
      endif
    endif

    ; LABEL_COLOR, and FONT_* are needed for the new Graphics property buttons.
    if (ISA(fontIndex) || ISA(fontName) || ISA(fontSize) || ISA(fontStyle)) then begin
      self._oLevels->SetProperty, FONT_INDEX=fontIndex, FONT_NAME=fontName, $
        FONT_SIZE=fontSize, FONT_STYLE=fontStyle
    endif

    if (ISA(fontColor)) then $
      labelColor = fontColor

    if (ISA(labelColor)) then begin
      self._oLevels->SetProperty, LABEL_COLOR=labelColor
      c_use_label_color = ISA(labelColor, /ARRAY)
    endif

    if (ISA(labelFormat)) then begin
      self._oLevels->SetProperty, TICKFORMAT=labelFormat
    endif

    if (N_ELEMENTS(minValue) || N_ELEMENTS(maxValue)) then begin
        if (self._zVisLog) then begin
            oContour = self._oContour
            minn = self._minValue
            maxx = self._maxValue
            if (N_Elements(minValue)) then $
                minV = (minValue gt 0) ? Alog10(minValue) : minn
            if (N_Elements(maxValue)) then $
                maxV = (maxValue gt 0) ? Alog10(maxValue) : maxx
        endif else begin
            if (N_Elements(minValue)) then minV = minValue
            if (N_Elements(maxValue)) then maxV = maxValue
        endelse
        self._oContour->SetProperty, MIN_VALUE=minV, MAX_VALUE=maxV
        self->_UpdateDefaultIndices
        self->OnDataChange, self
        self->OnDataComplete, self
    endif


    if (ISA(n_levels) && N_ELEMENTS(c_value) eq 0) then begin
      ; Create our new C_VALUE array. It will actually be set down
      ; further in the code.
      if (~isInit) then begin
        self._nLevels = n_levels
        c_value = self->_ComputeContourValues(n_levels)
        hasNlevels = 1b
      endif
    endif


    if (N_ELEMENTS(zvalue) gt 0) then begin
        oContour = self._oContour
        self._zvalue = zvalue
        ; IDLgrContour must have data to set GEOMZ.
        IF (Ptr_Valid(oContour.data)) THEN BEGIN
            zvalueSet = self._zvalue
            if (self._zVisLog) then $
                zvalueSet = (self._zvalue gt 0) ? Alog10(self._zvalue) : 0
            self._oContour->GetProperty, PLANAR=planar
            self._oContour->SetProperty, $
                GEOMZ=Keyword_Set(planar) ? zvalueSet : *oContour.data
            ; put the visualization into 3D mode if necessary
            self->Set3D, ~planar || (self._zvalue ne 0), /ALWAYS
            self->OnDataChange, self
            self->OnDataComplete, self
        ENDIF
    endif

    if (N_ELEMENTS(planar) gt 0) then begin
        self->SetPropertyAttribute, 'ZVALUE', SENSITIVE=Keyword_Set(planar)
        oContour = self._oContour
        ; IDLgrContour must have data to set GEOMZ.
        if (Ptr_Valid(oContour.data)) then begin
            zvalueSet = self._zvalue
            if (self._zVisLog) then $
                zvalueSet = (self._zvalue gt 0) ? Alog10(self._zvalue) : 0
            self._oContour->SetProperty, PLANAR=planar, $
                GEOMZ=Keyword_Set(planar) ? zvalueSet : *oContour.data
            ; put the visualization into 3D mode if necessary
            self->Set3D, ~planar || (self._zvalue ne 0), /ALWAYS
            self->OnDataChange, self
            self->OnDataComplete, self
        endif else begin
            self._oContour->SetProperty, PLANAR=planar
        endelse
;        ; update clipping if necessary
;        oDataSpace = self->GetDataSpace(/UNNORMALIZED)
;        if (OBJ_VALID(oDataSpace)) then begin
;            success = oDataSpace->GetXYZRange(xRange, yRange, zRange)
;            if success then self->OnDataRangeChange, self, XRange, YRange, ZRange
;        endif
    endif

    if (N_ELEMENTS(visPalette) gt 3) then begin
        oPal = self->GetParameter('PALETTE')
        if OBJ_VALID(oPal) then begin
            success = oPal->SetData(visPalette)
        endif else begin
            ; Set manually if we don't have a parameter.
            self._oPalette->SetProperty, BLUE_VALUES=visPalette[2,*], $
                GREEN_VALUES=visPalette[1,*], RED_VALUES=visPalette[0,*]
        endelse
    endif



    if (N_ELEMENTS(c_color) gt 0) then begin
        twoD = size(c_color, /n_dimensions) gt 1
        if (twoD) then begin
            ; supplied c_color is array of RGBs.
            self._palColor = 0
            self->SetPropertyAttribute, /HIDE, 'VISUALIZATION_PALETTE'
            ; disconnect self._oPalette from the contour
            ; to allow rgb values to be set through c_color
            ; it can be reconnected if pal_color is set
            self._oContour->SetProperty, PALETTE=OBJ_NEW()
        endif
        ; Make sure we create enough levels to hold our values.
        nvalues = (SIZE(c_color,/DIM))[twoD]
        if (nvalues gt 1) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        if (ARRAY_EQUAL(c_color, -1)) then c_color = -1
        if (~isInit) then $
          self._defaultIndices = ARRAY_EQUAL(c_color, -1)
        self._oContour->SetProperty,C_COLOR=c_color
    endif

    if (N_ELEMENTS(c_fill_pattern) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        isFill = ISA(c_fill_pattern, 'OBJREF')
        if (isFill) then $
            void = self->_GetLevels(N_LEVELS=N_ELEMENTS(c_fill_pattern))
        self._oContour->SetProperty, $
          C_FILL_PATTERN=isFill ? c_fill_pattern : 0
    endif

    if (N_ELEMENTS(c_label_interval) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_label_interval)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        self._oContour->SetProperty, C_LABEL_INTERVAL=c_label_interval
    endif

    if (N_ELEMENTS(c_label_nogaps) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_label_nogaps)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        self._oContour->SetProperty, C_LABEL_NOGAPS=c_label_nogaps
    endif

    if (N_ELEMENTS(c_label_objects) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_label_objects)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        self._oContour->SetProperty, C_LABEL_OBJECTS=c_label_objects
        if ~PTR_VALID(self._pLabelObjects) then self._pLabelObjects = PTR_NEW(/ALLOC)
        *self._pLabelObjects = c_label_objects
    endif

    if (N_ELEMENTS(c_label_show) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_label_show)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        self._oContour->SetProperty, C_LABEL_SHOW=c_label_show
        if ~PTR_VALID(self._pShow) then self._pShow = PTR_NEW(/ALLOC)
        *self._pShow = c_label_show

        ; Force a recalculation of the label values.
        self->IDLitVisContour::_RecalculateLabelValues
    endif

    if (N_ELEMENTS(c_linestyle) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_linestyle)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        if ~PTR_VALID(self._pLinestyle) then self._pLinestyle = PTR_NEW(/ALLOC)
        *self._pLinestyle = c_linestyle
        ; Be sure to pass scalars in as 1-element arrays, so IDLgrContour
        ; knows to cyclically repeat them (unless it's the special -1 flag).
        self._oContour->SetProperty, C_LINESTYLE=(c_linestyle[0] ne -1) ? [c_linestyle] : -1
    endif

    if (N_ELEMENTS(c_thick) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_thick)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        if ~PTR_VALID(self._pThick) then self._pThick = PTR_NEW(/ALLOC)
        *self._pThick = c_thick
        ; Be sure to pass scalars in as 1-element arrays, so IDLgrContour
        ; knows to cyclically repeat them (unless it's the special -1 flag).
        self._oContour->SetProperty, C_THICK=(c_thick[0] ne -1) ? [c_thick] : -1
    endif

    if (N_ELEMENTS(c_use_label_color) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_use_label_color)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        self._oContour->SetProperty, C_USE_LABEL_COLOR=c_use_label_color
    endif

    if (N_ELEMENTS(c_use_label_orientation) gt 0) then begin
        ; Make sure we create enough levels to hold our values.
        nvalues = N_ELEMENTS(c_use_label_orientation)
        if (nvalues gt 0) then $
            void = self->_GetLevels(N_LEVELS=nvalues)
        ; Switch from value 0 to our undocumented value 2, which indicates
        ; that the labels should be flipped if upside down.
        cUseOrient = c_use_label_orientation
        cUseOrient[WHERE(cUseOrient eq 0, /NULL)] = 2
        self._oContour->SetProperty, C_USE_LABEL_ORIENTATION=cUseOrient
    endif

    if (N_ELEMENTS(c_value) gt 0) then begin
      ;; trap c_value and don't allow -1 values to be set. This is
      ;; needed b/c of the design of the contour object (n_levels
      ;; and c_value) and how the property bag works.
      if ((SIZE(c_value, /N_DIM) gt 0) || c_value ne -1) then begin
        self._oContour->GetProperty, FILL=isFilled
        self._cValSet = ~ISA(hasNlevels) && $
          ((SIZE(c_value, /N_DIMENSIONS) GT 0) || c_value NE 0)
        cValues = c_value
        if (isFilled && (N_ELEMENTS(cValues) gt 1) && $
          cValues[-1] lt self._maxValue) then begin
          cValues = [cValues, self._maxValue]
        endif
        self._oContour->SetProperty, C_VALUE=cValues

        self->_UpdateDefaultIndices

        self->IDLitVisContour::_FillAllParameters, N_ELEMENTS(cValues)

        ; Force a recalculation of the label values.
        self->IDLitVisContour::_RecalculateLabelValues
      endif
    endif

    IF n_elements(singleColor) EQ 3 THEN BEGIN
      self._oContour->SetProperty, COLOR=singleColor, C_COLOR=-1
      if (~isInit) then $
        self._defaultIndices = 0b
    ENDIF

    IF (n_elements(xVisLog) GT 0) && (xVisLog NE self._xVisLog) THEN BEGIN
        self._xVisLog = xVisLog
        xData = self->_GetXYdata('X')
        if (N_ELEMENTS(xData) gt 1) then begin
            self._oContour->SetProperty, GEOMX=xData
            self._oContour->GetProperty, DOWNHILL=downhill
            if (downhill) then begin
              ; Force a recalculation of the tick interval/length.
              self->IDLitVisContour::SetProperty, $
                TICKINTERVAL=self._tickinterval, $
                TICKLEN=self._ticklen
            endif
            self->UpdateSelectionVisual
        endif
    endif

    IF (n_elements(yVisLog) GT 0) && (yVisLog NE self._yVisLog) THEN BEGIN
        self._yVisLog = yVisLog
        yData = self->_GetXYdata('Y')
        if (N_ELEMENTS(yData) gt 1) then begin
            self._oContour->SetProperty, GEOMY=yData
            self._oContour->GetProperty, DOWNHILL=downhill
            if (downhill) then begin
              ; Force a recalculation of the tick interval/length.
              self->IDLitVisContour::SetProperty, $
                TICKINTERVAL=self._tickinterval, $
                TICKLEN=self._ticklen
            endif
            self->UpdateSelectionVisual
        endif
    endif

    if (N_Elements(zVisLog) gt 0) && (zVisLog ne self._zVisLog) then begin
        self._zVisLog = zVisLog
        oParamZ = self->GetParameter('Z')
        if (Obj_Valid(oParamZ) && oParamZ->GetData(pZ, /POINTER) && $
            N_Elements(*pZ) gt 0) then begin
            newZ = (zVisLog gt 0) ? Alog10(*pZ) : *pZ
            minn = Min(newZ, Max=maxx, /NAN)
            self._oContour->GetProperty, PLANAR=planar
            zvalueSet = self._zvalue
            if (self._zVisLog) then $
                zvalueSet = (self._zvalue gt 0) ? Alog10(self._zvalue) : 0
            self._oContour->SetProperty, DATA=newZ, $
                GEOMZ=KEYWORD_SET(planar) ? zvalueSet : newZ, $
                MIN_VALUE=minn, MAX_VALUE=maxx
            ; Notify our observers in case the prop sheet is visible.
            ; Use N_LEVELS as the property since the levels may have changed.
            self->DoOnNotify, self->GetFullIdentifier(), 'SETPROPERTY', 'N_LEVELS'
        endif
    endif
    
    if (ISA(rgbTableIn) || ISA(visPalette)) then begin
      rgbTable = ISA(rgbTableIn) ? rgbTableIn : visPalette
      if (N_ELEMENTS(rgbTable) eq 1) then begin
        rgbTable = Colortable(rgbTable[0])
      endif
      dim = SIZE(rgbTable, /DIMENSIONS)
      if (N_ELEMENTS(dim) eq 2) then begin
        if (dim[0] gt dim[1]) then $
          rgbTable = TRANSPOSE(rgbTable)
        self._oPalette->SetProperty, BLUE_VALUES=rgbTable[2,*], $
          GREEN_VALUES=rgbTable[1,*], RED_VALUES=rgbTable[0,*]
        oDataRGB = self->GetParameter('PALETTE')
        if OBJ_VALID(oDataRGB) then $
          success = oDataRGB->SetData(rgbTable)
      endif
    endif
    
    if ISA(rgbIndicesIn) then begin  
      newindices = OBJ_NEW('IDLitDataIDLVector', rgbIndicesIn, $
                      NAME='RGB Indices', TYPE='IDLVECTOR', icon='layer')
      self._oParameterSet->add,newindices, PARAMETER_NAME="RGB_INDICES"
      self->OnDataChangeUpdate,newindices, "RGB_INDICES"
    endif

    ; Set superclass properties
    if (N_ELEMENTS(_extra) gt 0) then begin

        ; Pass properties directly to Contour Level container.
        ; We want to treat the CL container as if it is aggregated, but
        ; we don't actually aggregate it because we don't want the props
        ; to show up in the Contour property sheet.
        ; See Note in ::Init.
        isLevelProp = BYTARR(N_ELEMENTS(_extra))
        foreach prop, _extra, i do begin
          isLevelProp[i] = MAX(*self._pProps eq prop)
        endforeach
        levelProps = WHERE(isLevelProp, nProps, $
            COMPLEMENT=otherProps, NCOMP=nOther)
        if (nOther gt 0) then begin
            self._oContour->SetProperty, _EXTRA=_extra[otherProps]
            self->IDLitVisualization::SetProperty, _EXTRA=_extra[otherProps]
        endif
        if (nProps gt 0) then begin
            self._oLevels->SetProperty, _EXTRA=_extra[levelProps]
        endif

    endif

end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to retrieve the data from the grContour
;
; Arguments:
;   Z, X, Y
;
; Keywords:
;   NONE
;
pro IDLitVisContour::GetData, zData, xData, yData, _EXTRA=_extra
  compile_opt idl2, hidden
  
  oDataZ = self->GetParameter('Z')
  if (OBJ_VALID(oDataZ)) then $
    void = oDataZ->GetData(zData)
  xData = self->_GetXYdata('X')
  yData = self->_GetXYdata('Y')

end

;----------------------------------------------------------------------------
; Purpose:
;   This method is used to z value and a given x and y location
;
; Arguments:
;   Z, X, Y
;
; Keywords:
;   NONE
;
function IDLitVisContour::GetValueAtLocation, arg1, arg2, $
  DATA=data, DEVICE=device, NORMAL=normal, $
  INTERPOLATE=interpolate
  
  compile_opt idl2, hidden
  if (N_PARAMS() ne 2) then MESSAGE, 'Incorrect number of arguments'
    
  xloc = arg1
  yloc = arg2  
   
  if((keyword_set(device)) || (keyword_set(normal))) then begin
    converted = iconvertcoord(arg1, arg2, DEVICE=device, NORMAL=normal, /TO_DATA)
    xloc = converted[0]
    yloc = converted[1]
  endif

  oDataZ = self->GetParameter('Z')
  if (OBJ_VALID(oDataZ)) then void = oDataZ->GetData(zData, /POINTER)
  dims = SIZE(*zData, /DIMENSIONS)
  
  oDataX = self->GetParameter('X')
  if (OBJ_VALID(oDataX)) then void = oDataX->GetData(xData, /POINTER) else xData = ptr_new(FINDGEN(dims[0]))
  oDataY = self->GetParameter('Y')
  if (OBJ_VALID(oDataY)) then void = oDataY->GetData(yData, /POINTER) else yData = ptr_new(FINDGEN(dims[1]))

  if (~KEYWORD_SET(interpolate)) then begin
    minn = MIN(ABS(*xdata - xloc), mxnloc)
    xloc = (*xdata)[mxnloc]
    minn = MIN(ABS(*ydata - yloc), mynloc)
    yloc = (*ydata)[mynloc]
    zloc = (*zData)[mxnloc, mynloc]
  endif else begin ; interpolate
    ; find nearest points
    minn = MIN(ABS(*xdata - xloc), xloc1)
    datalength = n_elements(*xdata)
  
    if(xloc1 eq datalength-1) then begin
      xloc2 = xloc1-1
    endif else if (xloc1 eq 0) then begin
      xloc2 = xloc1+1
    endif else begin
      if (xloc lt (*xdata)[xloc1]) then xloc2 = xloc1-1 else xloc2 = xloc1+1
    endelse
    
    x1 = (*xdata)[xloc1]
    x2 = (*xdata)[xloc2]
    
    minn = MIN(ABS(*ydata - yloc), yloc1)
    datalength = n_elements(*ydata)
  
    if(yloc1 eq datalength-1) then begin
      yloc2 = yloc1-1
    endif else if (yloc1 eq 0) then begin
      yloc2 = yloc1+1
    endif else begin
      if (yloc lt (*ydata)[yloc1]) then yloc2 = yloc1-1 else yloc2 = yloc1+1
    endelse
    
    y1 = (*ydata)[yloc1]
    y2 = (*ydata)[yloc2]
    
    z11 = (*zData)[xloc1, yloc1]
    z12 = (*zData)[xloc1, yloc2]
    z21 = (*zData)[xloc2, yloc1]
    z22 = (*zData)[xloc2, yloc2]
    
    zloc = (1/ ((x2-x1) * (y2-y1))) * ((z11 * (x2-xloc) * (y2-yloc)) + $
           (z21 * (xloc-x1) * (y2-yloc)) + $
           (z12 * (x2-xloc) * (yloc-y1)) + $
           (z22 * (xloc-x1) * (yloc-y1)))
  endelse
  
  value = [xloc, yloc, zloc] 
  
  return, value

end


;----------------------------------------------------------------------------
pro IDLitVisContour::_UpdateData, xrange, yrange

  compile_opt idl2, hidden
  
  if (self._equation eq '') then return
  if (self._withinUpdateData) then return
  self._withinUpdateData = 1b
  
  ; If we don't have the XRANGE then this is a "new" equation,
  ; so we should try to pick a new range.
  newXrange = 0b
  if (~ISA(xrange)) then begin
    newXrange = 1b
    xrange = [-10,10]
    yrange = [-10,10]
    oDS = self.GetDataSpace()
    if (ISA(oDS)) then begin
      oDS.GetProperty, X_MINIMUM=xMin, X_MAXIMUM=xMax, $
        Y_MINIMUM=yMin, Y_MAXIMUM=yMax, $
        X_AUTO_UPDATE=xAutoUpdate, Y_AUTO_UPDATE=yAutoUpdate
      if (~xAutoUpdate) then xrange = [xMin, xMax]
      if (~yAutoUpdate) then yrange = [yMin, yMax]
    endif
  endif

  nx = (self._eqnSamples[0] gt 0) ? self._eqnSamples[0] : 200
  ny = (self._eqnSamples[1] gt 0) ? self._eqnSamples[1] : 200
  dx = DOUBLE(xrange[1] - xrange[0])/(nx-1)
  dy = DOUBLE(yrange[1] - yrange[0])/(ny-1)

  if (dx eq 0 || dy eq 0) then $
    goto, endEarly

  ; If range is unchanged, then bail early.
  if (MAX(ABS(self._xrange - xrange))/dx lt 1d-6 && $
    MAX(ABS(self._yrange - yrange))/dy lt 1d-6) then begin
    goto, endEarly
  endif

  x = REBIN(DINDGEN(nx)*dx + xrange[0], nx, ny)
  y = REBIN(DINDGEN(1,ny)*dy + yrange[0], nx, ny)
  
  if (IDL_VALIDNAME(self._equation)) then begin
    if (PTR_VALID(self._eqnUserdata)) then begin
      z = CALL_FUNCTION(self._equation, x, y, *self._eqnUserdata)
    endif else begin
      z = CALL_FUNCTION(self._equation, x, y)
    endelse
    success = N_ELEMENTS(z) eq N_ELEMENTS(x)
  endif else begin
    success = EXECUTE('z = ' + self._equation)
  endelse

  if (success) then begin
    good = FINITE(z)
    x = x[*,0]
    y = REFORM(y[0,*])
    if (~ARRAY_EQUAL(good, 1b)) then begin
      xtotal = TOTAL(good, 2, /INTEGER)
      ytotal = TOTAL(good, 1, /INTEGER)
      xkeep = WHERE(xtotal gt 0, xcount, /NULL)
      ykeep = WHERE(ytotal gt 0, ycount, /NULL)
      if (xcount gt 0 && ycount gt 0) then begin
        x = x[xkeep[0]:xkeep[-1]]
        y = y[ykeep[0]:ykeep[-1]]
        z = z[xkeep[0]:xkeep[-1], ykeep[0]:ykeep[-1]]
      endif
    endif
    
    self._xrange = xrange
    self._yrange = yrange
    self->PutData, z, x, y
    if (~self._cValSet) then begin
      self->SetProperty, N_LEVELS=self._nLevels
    endif
  endif

endEarly:
  self._withinUpdateData = 0b
  
end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to directly set the data
;
; Arguments:
;   Z, X, Y
;
; Keywords:
;   NONE
;
pro IDLitVisContour::_SetData, Z, X, Y, _EXTRA=_extra
  compile_opt idl2, hidden
  
  case n_params() of
  1: self->PutData, Z, _EXTRA=_extra
  3: begin
    irregular = Graphic_IsIrregular(z, x, y, $
      GRID_UNITS=self._gridUnits, SPHERE=sphere)
    if (irregular) then begin
      Graphic_GridData, z, x, y, zOut, xOut, yOut, SPHERE=sphere
      self->PutData, zOut, xOut, yOut, _EXTRA=_extra
    endif else begin
      self->PutData, Z, X, Y, _EXTRA=_extra
    endelse
    end
  else: message, 'Incorrect number of arguments.'
  endcase

end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to directly set the data
;
; Arguments:
;   Z, X, Y
;
; Keywords:
;   NONE
;
pro IDLitVisContour::PutData, Z, X, Y, _EXTRA=_extra
  compile_opt idl2, hidden
 
  switch (N_PARAMS()) of 
    3 : begin
          oData = OBJ_NEW('IDLitData', X, /AUTO_DELETE)
          self->SetParameter, 'X', oData, /NO_UPDATE
          oData = OBJ_NEW('IDLitData', Y, /AUTO_DELETE)
          self->SetParameter, 'Y', oData, /NO_UPDATE
        end
    1 : begin
          oData = OBJ_NEW('IDLitData', Z, /AUTO_DELETE)
          self->SetParameter, 'Z', oData
        end 
    else :
  endswitch
  
  ; Send a notification message to update UI
  self->DoOnNotify, self->GetFullIdentifier(),"ADDITEMS", ''
  self->OnDataComplete, self
  
  oTool = self->GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->RefreshCurrentWindow

end


;---------------------------------------------------------------------------
pro IDLitVisContour::OnNotify, strID, message, userdata

  compile_opt idl2, hidden

  ; CT, May 2013: See DoOnNotify within GraphicsManip::OnMouseDown/OnMouseUp
  case STRUPCASE(message) of
    'PAN_START': begin
      self._withinUpdateData = 1b
    end
    'PAN_FINISH': begin
      self._withinUpdateData = 0b
      self->_UpdateData
      end
    else:
  endcase
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLitVisContour::EnsureXYParameters
;
; PURPOSE:
;   Ensure that X and Y parameters exist, based on the contour data
;   dimensions.
;
; CALLING SEQUENCE:
;   Obj->[IDLitVisContour::]EnsureXYParameters
;
; KEYWORD PARAMETERS:
;   None.
;
; USAGE:
;   This is used by operations such as IDLitOpInsertImage, that need
;   the contour parameter in order to create an image based on the
;   contour and to be notified of changes to the contour.
;-
pro IDLitVisContour::EnsureXYParameters

  compile_opt idl2, hidden

    ; Retrieve Z parameter first to get data dimensions.
    oZParam = self->GetParameter('Z')
    if (~OBJ_VALID(oZParam)) then $
        return
    if (~(oZParam->GetData(pData, /POINTER))) then $
        return
    dims = SIZE(*pData, /DIMENSIONS)

    paramNames = ['X', 'Y']
    for i=0,1 do begin
        ; Check if parameter already exists.
        oParam = self->GetParameter(paramNames[i])
        if ~obj_valid(oParam) then begin
            ; Create and set the parameter.
            data = DINDGEN(dims[i])
            oData = OBJ_NEW('IDLitDataIDLVector', data, NAME=paramNames[i])
            oData->SetProperty, /AUTO_DELETE
            self->SetParameter, paramNames[i], oData, /NO_UPDATE

            ; Add to data manager.
            oData = self->GetParameter(paramNames[i])

            ; Add to the same container in which the Z data is contained.
            oZParam->GetProperty,_PARENT=oParent

            ; Note: the X,Y data should not be added to an
            ; IDLitDataIDLImagePixels object.  Keep walking up the tree.
            if (OBJ_ISA(oParent, 'IDLitDataIDLImagePixels')) then begin
                oImagePixels = oParent
                oImagePixels->GetProperty,_PARENT=oParent
            endif

            if obj_valid(oParent) then begin
                oParent->Add,oData
                ;; check to see if we need to mangle the name
                ;; Get our base name and append the id number.
                oData->IDLitComponent::GetProperty, IDENTIFIER=id, NAME=name
                ;; See if we have an id number at the end of our identifier.
                idnum = (STRSPLIT(id, '_', /EXTRACT, COUNT=count))[count>1 - 1]
                ;; Append the id number.
                if (STRMATCH(idnum, '[0-9]*')) then begin
                  name += ' ' + idnum
                  ;; set new name
                  oData->IDLitComponent::SetProperty, NAME=name
                  oTool = self->GetTool()
                  oTool->DoOnNotify,oData->GetFullIdentifier(), $
                                    'SETPROPERTY','NAME'
                endif
            endif else $
                self->AddByIdentifier,'/DATA MANAGER',oData
        endif
    endfor

    ; Send a notification message to update UI
    self->DoOnNotify, self->GetFullIdentifier(),"ADDITEMS", ''

end

;----------------------------------------------------------------------------
; Purpose:
;   This function method is used to edit a user-defined property.
;
; Arguments:
;   Tool: Object reference to the tool.
;
;   PropertyIdentifier: String giving the name of the userdef property.
;
; Keywords:
;   None.
;
function IDLitVisContour::EditUserDefProperty, oTool, identifier

    compile_opt idl2, hidden

    case identifier of

        'CONTOUR_LEVELS': begin
            oTargets = oTool->GetSelectedItems(count=nTargets)
            self->GetProperty, PARENT=parent
            if (nTargets gt 1) || (OBJ_ISA(parent, 'IDLitVisGroup')) then begin
                self->ErrorMessage, $
                    [IDLitLangCatQuery('Message:EditContourLevels:Text')], $
                    title=IDLitLangCatQuery('Message:EditContourLevels:Title'), severity=1
                return, 0
            endif

            ; Make sure label color is in sync with PAL_COLOR.
            self->IDLitVisContour::GetProperty, $
                CONTOUR_LEVELS=oLevels, PAL_COLOR=palColor
            for i=0,N_ELEMENTS(oLevels)-1 do begin
                oLevels[i]->SetPropertyAttribute, 'COLOR', $
                    SENSITIVE=~palColor
            endfor

            ; Sanity check.
            self._oLevels->_CheckIntersectAttributes

            success = oTool->DoUIService('ContourLevels', self)

            ; We want to return "failure" to avoid committing the Userdef
            ; property changes. But we need to commit our individual
            ; SetProperty actions. Note that hitting the "Close" (X) button
            ; still commits the actions.
            oTool->CommitActions

            return, 0
            end

        'VISUALIZATION_PALETTE': begin
            success = oTool->DoUIService('PaletteEditor', self)
            if success then begin
                return, 1
            endif
        end

        else:

    endcase

    ; Call our superclass.
    return, self->IDLitVisualization::EditUserDefProperty(oTool, identifier)

end


;----------------------------------------------------------------------------
; IIDLDataObserver Interface
;----------------------------------------------------------------------------



;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisContour::OnDataRangeChange
;
; PURPOSE:
;      This procedure method handles notification that the data range
;      has changed.
;
;      NOTE: This implementation currently assumes that no transformation
;      matrix is being applied between this Contour and the Subject sending
;      the notification.
;
; CALLING SEQUENCE:
;    Obj->[IDLitVisContour::]OnDataRangeChange, oSubject, $
;          XRange, YRange, ZRange
;
; INPUTS:
;      oSubject:  A reference to the object sending notification
;                 of the data range change.
;      XRange:    The new xrange, [xmin, xmax].
;      YRange:    The new yrange, [ymin, ymax].
;      ZRange:    The new zrange, [zmin, zmax].
;
;-
pro IDLitVisContour::OnDataRangeChange, oSubject, XRange, YRange, ZRange

  compile_opt idl2, hidden

  if (self._equation ne '') then begin
    self->_UpdateData, XRange, YRange
  endif

  self->_IDLitVisualization::ClipToDataRange, XRange, YRange, ZRange

  ; CR60943: If our dataspace axes ranges are reversed,
  ; be sure to flip our text labels so that the orientation is correct.
  oDS = self->GetDataspace(/UNNORM)
  if (OBJ_VALID(oDS)) then begin
    if (oDS->_GetXYZAxisRange(xRange, yRange, zRange, $
              XREVERSE=xReverse, YREVERSE=yReverse, ZREVERSE=zReverse)) then begin
      if (xReverse || yReverse) then begin
        self->IDLitVisContour::GetProperty, HAS_LABEL_OBJECTS=hasLabelObjects
        if (~hasLabelObjects) then begin
          self._oContour->GetProperty, C_LABEL_OBJECTS=cLabelObj
          foreach obj, cLabelObj do begin
            if (~OBJ_ISA(obj, 'IDLgrText')) then continue
            obj->GetProperty, UPDIR=updir, BASELINE=baseline
            if (xReverse) then baseline[0] = -1
            if (yReverse) then updir[1] = -1
            obj->SetProperty, UPDIR=updir, BASELINE=baseline
            self._oContour->SetProperty, C_USE_LABEL_ORIENTATION=1
          endforeach
        endif
      endif
    endif
  endif

end


;----------------------------------------------------------------------------
PRO IDLitVisContour::OnDataDisconnect, ParmName
   compile_opt hidden, idl2

   ;; Just check the name and perform the desired action
   case ParmName of
       'Z': begin
           ;; You can't unset data, so we hide the contour.
           self._oContour->SetProperty, DATA_VALUES=[[0,0],[0,0]], $
             GEOMZ=[[0,0],[0,0]]
           self._oContour->SetProperty, /HIDE
       end

       'X': begin
           self._oContour->GetProperty, DATA_VALUES=data
           szDims = size(data,/dimensions)
           data=0b
           self._oContour->SetProperty, GEOMX=indgen(szDims[0])
          ; Force a recalculation of the tick interval/length.
          self->IDLitVisContour::SetProperty, $
            TICKINTERVAL=self._tickinterval, TICKLEN=self._ticklen
       end
       'Y': begin
           self._oContour->GetProperty, DATA_VALUES=data
           szDims = size(data,/dimensions)
           data=0b
           self._oContour->SetProperty, GEOMY=indgen(szDims[1])
          ; Force a recalculation of the tick interval/length.
          self->IDLitVisContour::SetProperty, $
            TICKINTERVAL=self._tickinterval, TICKLEN=self._ticklen
       end
       else:
       endcase
end


;----------------------------------------------------------------------------
; Purpose:
;    This procedure method is called by a Subject via a Notifier when
;    its data has changed.  This method obtains the data from the subject
;    and updates the IDLgrContour object.
;
; Arguments:
;    oSubject: The Subject object in the Subject-Observer relationship.
;    This object (the surface) is the observer, so it uses the
;    IIDLDataSource interface to get the data from the subject.
;    Then, it puts the data in the IDLgrContour object.
;
;   ParmName: The name of the parameter that was changed.
;
; Keywords:
;   None.
;
pro IDLitVisContour::OnDataChangeUpdate, oSubject, parmName

    compile_opt idl2, hidden


    case STRUPCASE(parmName) of

        '<PARAMETER SET>':begin
            ;; Get our data
            position = oSubject->Get(/ALL, count=nCount, NAME=names)
            for i=0, nCount-1 do begin
                oData = oSubject->GetByName(names[i],count=nCount)
                IF nCount NE 0 THEN self->OnDataChangeUpdate,oData,names[i]
            endfor
            ; Might have palette but no indices, so load default if necessary
            self->_UpdateDefaultIndices
            self->OnProjectionChange

            oTool = self->GetTool()
            id = self->GetFullIdentifier()
            if (ISA(oTool) && STRLEN(id) gt 1) then begin
              oTool->AddOnNotifyObserver, id, $
                oTool->GetFullIdentifier() + '/MANIPULATORS/SELECT'
            endif
        END
        'Z': BEGIN
          success = oSubject->GetData(zData, NAN=nan)

          zDims = SIZE(zData, /DIMENSIONS)

          oDataSpace = self->GetDataSpace(/UNNORMALIZED)
          if (OBJ_VALID(oDataSpace)) then begin
            oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, ZLOG=zLog
            self._xVisLog = xLog
            self._yVisLog = yLog
            self._zVisLog = zLog
          endif

          ; Reset the X and Y along with the Z so that all are up-to-date
          ; simultaneously.
          xData = self->_GetXYdata('X')
          yData = self->_GetXYdata('Y')

          if (self._zVisLog gt 0) then zData =  Alog10(Temporary(zData))
          mn = MIN(zData, MAX=mx, NAN=nan)
          self._minValue = mn
          self._maxValue = mx

          self._oContour->GetProperty, C_VALUE=cValueCurrent, $
            FILL=isFilled, PLANAR=planar, DOWNHILL=downhill
          self->Set3D, ~planar || (self._zvalue ne 0), /ALWAYS
          zvalueSet = self._zvalue
          if (self._zVisLog) then $
            zvalueSet = (self._zvalue gt 0) ? Alog10(self._zvalue) : 0
          if (isFilled && (N_ELEMENTS(cValueCurrent) gt 1) && $
            (cValueCurrent[-1] lt self._maxValue)) then begin
            cValues = [cValueCurrent, self._maxValue]
          endif

          self._oContour->SetProperty, DATA_VALUES=zData, $
              HIDE=0, $
              C_VALUE=cValues, $
              MIN_VALUE=mn, MAX_VALUE=mx, $
              GEOMX=xData, $
              GEOMY=yData, $
              GEOMZ=KEYWORD_SET(planar) ? zvalueSet : zData
          


          ; These properties were disabled for styles. Reenable them.
          self->SetPropertyAttribute, ['MIN_VALUE', 'MAX_VALUE'], HIDE=0

          if (downhill) then begin
            self->IDLitVisContour::SetProperty, $
              TICKINTERVAL=self._tickinterval, TICKLEN=self._ticklen
          endif

          ; If we are an iTool (not a new graphic),
          ; load up the vertices and connectivity for export
          if (~ISA(self._proxy)) then begin
            self->IDLitVisContour::UpdateOutputParameters
          endif
        END

        'X': BEGIN
            oDataSpace = self->GetDataSpace(/UNNORMALIZED)
            if (OBJ_VALID(oDataSpace)) then begin
                oDataSpace->GetProperty, XLOG=xLog
                self._xVisLog = xLog
            endif
            xData = self->_GetXYdata('X')
            if (N_ELEMENTS(xData) gt 1) then begin
                self._oContour->SetProperty, GEOMX=temporary(xData)
                ; Force a recalculation of the tick interval/length.
                self->IDLitVisContour::SetProperty, $
                    TICKINTERVAL=self._tickinterval, $
                    TICKLEN=self._ticklen
                self->UpdateSelectionVisual
            endif
            END

        'Y': BEGIN
            oDataSpace = self->GetDataSpace(/UNNORMALIZED)
            if (OBJ_VALID(oDataSpace)) then begin
                oDataSpace->GetProperty, YLOG=yLog
                self._yVisLog = yLog
            endif
            yData = self->_GetXYdata('Y')
            if (N_ELEMENTS(yData) gt 1) then begin
                self._oContour->SetProperty, GEOMY=temporary(yData)
                ; Force a recalculation of the tick interval/length.
                self->IDLitVisContour::SetProperty, $
                    TICKINTERVAL=self._tickinterval, $
                    TICKLEN=self._ticklen
                self->UpdateSelectionVisual
            endif
            END

         'PALETTE': begin
            if(oSubject->GetData(data)) then begin
                if (size(data, /TYPE) ne 1) then $
                    data=bytscl(data)
                self._oPalette->SetProperty, $
                    RED_VALUES=data[0,*], $
                    GREEN_VALUES=data[1,*], $
                    BLUE_VALUES=data[2,*]
                self._oContour->SetProperty, PALETTE=self._oPalette
                self._palColor = 1
                self->GetProperty, CONTOUR_LEVELS=oLevels
                nLevels = N_ELEMENTS(oLevels)
                self->SetPropertyAttribute, HIDE=0, 'VISUALIZATION_PALETTE'
                if OBJ_VALID(oLevels[0]) then begin
                    for i=0, nLevels-1 do begin
                        oLevels[i]->SetPropertyAttribute, SENSITIVE=0, 'COLOR'
                    endfor
                endif
            end
         end

        'RGB_INDICES': begin
            if(oSubject->GetData(data)) then begin
                oSubject->GetProperty, NAME=name
                self->IDLitVisContour::SetProperty, C_COLOR=data
            end
        end
        'VERTICES': begin ; this is a read only parameter - for export only
            dummy=5
        end
        'CONNECTIVITY': begin ; this is a read only parameter - for export only
            dummy=5
        end

       ELSE: ; ignore unknown parameters

    endcase

    ; Remove the Contour Level container from the aggregate, so that
    ; all of these properties won't show up in the Contour prop sheet.
    ; See Note in ::Init.
    if (self->IsContainedAggregate(self._oLevels)) then $
        self->RemoveAggregate, self._oLevels

end


;----------------------------------------------------------------------------
; IDLitVisContour::UpdateOutputParameters
;
; Purpose:
;   This procedure method updates the values of the output parameters
;   for this contour.  The output parameters (used for export) include:
;
;     VERTICES
;     CONNECTIVITY
;
pro IDLitVisContour::UpdateOutputParameters
    compile_opt idl2, hidden

    ; Retrieve new vertex and connectivity.
    result = self._oContour->GetVertexData(Outverts, Outconn)
    if (~result) then $
        return

    if (SIZE(Outverts, /N_DIMENSIONS) ne 2) then $
        Outverts = DBLARR(3,2)

    ; Update VERTICES.
    oVertObj = self->GetParameter('VERTICES')
    if (OBJ_VALID(oVertObj)) then begin
        ; We don't need to receive notification, so use NO_NOTIFY.
        result = oVertObj->SetData(Outverts, /NO_COPY, /NO_NOTIFY)
    endif else begin
        oVertices = OBJ_NEW('IDLitDataIDLArray2D', Outverts, NAME='VERTICES')
        result = self->SetData(oVertices, PARAMETER_NAME='VERTICES', $
            /BY_VALUE)
    endelse

    ; Update CONNECTIVITY.
    oConnObj = self->GetParameter('CONNECTIVITY')
    if (OBJ_VALID(oConnObj)) then begin
        ; We don't need to receive notification, so use NO_NOTIFY.
        result = oConnObj->SetData(Outconn, /NO_COPY, /NO_NOTIFY)
    endif else begin
        oConnectivity = OBJ_NEW('IDLitDataIDLVector', Outconn, $
            NAME='CONNECTIVITY')
        result = self->SetData(oConnectivity, PARAMETER_NAME='CONNECTIVITY',$
            /BY_VALUE)
    endelse

end

;----------------------------------------------------------------------------
pro IDLitVisContour::OnProjectionChange, sMap

    compile_opt idl2, hidden

    if (~N_ELEMENTS(sMap)) then $
        sMap = self->GetProjection()

    hasMapProjection = N_TAGS(sMap) gt 0
    if (hasMapProjection) then $
        self->SetPropertyAttribute, 'GRID_UNITS', /SENSITIVE

    if (self._gridUnits ne 2) then begin
        ; Remove the map projection from the contour.
        if (self._hasMapProjection) then begin
            self._oContour->SetProperty, MAP_STRUCTURE=0
            self._hasMapProjection = 0b
            ; Update output parameters.
            self->IDLitVisContour::UpdateOutputParameters
        endif
        return
    endif

    self._hasMapProjection = hasMapProjection

    self._oContour->SetProperty, MAP_STRUCTURE=sMap

    ; Update tick intervals and lengths.
    ; Scale from normalized range to x/yrange.
    self._oContour->GetProperty, XRANGE=xr, YRANGE=yr, DOWNHILL=downhill
    if (downhill) then begin
      maxrange = ABS(xr[1] - xr[0]) > ABS(yr[1] - yr[0])
      scaledTickinterval = 0.5d*maxrange*self._tickinterval
      scaledTicklen = 0.5d*maxrange*self._ticklen
      self._oContour->SetProperty, $
        TICKINTERVAL=scaledTickinterval, $
        TICKLEN=scaledTicklen
    endif

    ; Update output parameters.
    self->IDLitVisContour::UpdateOutputParameters
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLitVisContour::SetParameter
;
; PURPOSE;
;   This procedure method associates a data object with the given
;   parameter.
;
; CALLING SEQUENCE:
;   Obj->[IDLitVisContour::]SetParameter, ParamName, oItem
;
; INPUTS:
;   ParamName:  The name of the parameter to set.
;   oItem:  The IDLitData object to be associated with the
;     given parameter.
;-
pro IDLitVisContour::SetParameter, ParamName, oItem, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if (STRUPCASE(ParamName) eq 'Z') then begin
        oItem->GetProperty, NAME=name
        if (STRLEN(name) gt 0) then $
            self->StatusMessage, 'Contour target: '+name
    endif

    ; Pass along to the superclass.
    self->IDLitParameter::SetParameter, ParamName, oItem, _EXTRA=_extra
end


;---------------------------------------------------------------------------
; Convert XYZ dataspace coordinates into actual data values.
;
function IDLitVisContour::_GetDataLocation, x, y

    compile_opt idl2, hidden

    oDataObj = self->GetParameter('Z')

    if (~oDataObj->GetData(pData, /POINTER)) || $
        (~N_ELEMENTS(pData)) then $
        return, ''

    dims = SIZE(*pdata, /DIMENSIONS)
    if (N_ELEMENTS(dims) ne 2) then $
        return, ''

    ; If we have X and Y parameters, convert from X and Y data coordinates
    ; back to indices.
    oX = self->GetParameter('X')
    if (obj_valid(oX) && $
        oX->GetData(xdata) && N_ELEMENTS(xdata) eq dims[0]) then begin
        ; Find the closest X index to our X data value.
        minn = MIN(ABS(xdata - x), loc)
        x = loc
    endif

    oY = self->GetParameter('Y')
    if (obj_valid(oY) && $
        oY->GetData(ydata) && N_ELEMENTS(ydata) eq dims[1]) then begin
        ; Find the closest Y index to our Y data value.
        minn = MIN(ABS(ydata - y), loc)
        y = loc
    endif

    ; Out of bounds. Failure.
    if ((x lt 0) || (x ge dims[0]) || (y lt 0) || (y ge dims[1])) then $
        return, ''

    return, STRING(DOUBLE((*pdata)[x, y]), FORMAT='(g0.4)')

end


;---------------------------------------------------------------------------
; Report X, Y, and possibly Z location.
;
function IDLitVisContour::GetDataString, xyz

    compile_opt idl2, hidden

    if (self._xVisLog) then $
        xyz[0] = 10d^xyz[0]
    if (self._yVisLog) then $
        xyz[1] = 10d^xyz[1]

    ; Notify any observers that someone is probing us.
    ; This is usually the image panel, and will probably
    ; call right back into our GetExtendedDataStrings method below.
    self->DoOnNotify, self->GetFullIdentifier(), $
        'CONTOURPROBE', xyz[0:1]

    value = STRING(xyz[0:1], FORMAT='("X: ",g0.4,"  Y: ",g0.4)')

    if (self._hasMapProjection) then $
        return, value

    zStr = self->_GetDataLocation(xyz[0], xyz[1])
    if (zStr ne '') then $
        value += '  Z: ' + zStr

    return, value

end

;---------------------------------------------------------------------------
; Convert a location from decimal degrees to DDDdMM'SS", where "d" is
; the degrees symbol.
;
function IDLitVisContour::_DegToDMS, x

    compile_opt idl2, hidden

    eps = 0.5d/3600
    x = (x ge 0) ? x + eps : x - eps
    degrees = FIX(x)
    minutes = FIX((ABS(x) - ABS(degrees))*60)

    ; Arcseconds are trickier. We need to determine whether we should
    ; output integers or floats.
    seconds = (ABS(x) - ABS(degrees) - minutes/60d)*3600
    format = '(I2)'

    ; If grid spacing is less than 10 arcseconds (~280 meters).
;    eps = (self._gridUnits eq 2) ? 0.0028 : 280
;    if (MIN(self._userStep) lt eps) then $
;        format = '(g0.4)'

    dms = STRING(degrees, FORMAT='(I4)') + STRING(176b) + $
        STRING(minutes, FORMAT='(I2)') + "'" + $
        STRING(seconds, FORMAT=format) + '"'

    return, dms

end


;---------------------------------------------------------------------------
; Retrieve extended data information strings (computed in the
;   most recent IDLitVisContour::GetDataString call).
;
; xyLoc should contain the [x,y] as computed by GetDataString.
;
pro IDLitVisContour::GetExtendedDataStrings, xyLoc, $
    MAP_LOCATION=mapLocation, $
    PROBE_LOCATION=probeLocation, $
    PIXEL_VALUES=pixelValues

    compile_opt idl2, hidden

    probeLocation = ''
    pixelValues = ''

    ; Map location:
    if (ARG_PRESENT(mapLocation)) then begin

        if (self._hasMapProjection) then begin
            ; If we have a map projection, then xyLoc is already in meters, and
            ; needs to be converted back to degrees.
            mapStruct = self->GetProjection()
            lonlat = MAP_PROJ_INVERSE(xyLoc[0], xyLoc[1], $
                MAP_STRUCTURE=mapStruct)
            ; Longitude & latitude.
            loc0 = 'Lon: ' + self->_DegToDMS(lonlat[0])
            loc1 = 'Lat: ' + self->_DegToDMS(lonlat[1])
            mapLocation = [loc0, loc1]
            ; X and Y location.
            loc0 = STRTRIM(STRING(xyLoc[0],FORMAT='(g0.8)'),2)
            loc0 = '     (' + loc0 + ' m' + ')'
            loc1 = STRTRIM(STRING(xyLoc[1],FORMAT='(g0.8)'),2)
            loc1 = '     (' + loc1 + ' m' + ')'
            mapLocation = [mapLocation, loc0, loc1]

            pixelValues = self->_GetDataLocation(lonlat[0], lonlat[1])

        endif else begin

            ; If we don't have a map projection, then xyLoc is in either
            ; degrees or meters.
            ; geomUnits=1 is meters, 2 is degrees
            if (self._gridUnits eq 2) then begin
                ; Longitude & latitude.
                loc0 = 'Lon: ' + self->_DegToDMS(xyLoc[0])
                loc1 = 'Lat: ' + self->_DegToDMS(xyLoc[1])
            endif else begin
                ; X and Y location.
                loc0 = STRTRIM(STRING(xyLoc[0],FORMAT='(g0.8)'),2)
                loc0 = 'X: ' + loc0
                loc1 = STRTRIM(STRING(xyLoc[1],FORMAT='(g0.8)'),2)
                loc1 = 'Y: ' + loc1
                if (self._gridUnits eq 1) then begin
                    loc0 += ' m'
                    loc1 += ' m'
                endif
            endelse

            ; Since we don't have a map projection, append null strings.
            mapLocation = [loc0, loc1, '', '']

            pixelValues = self->_GetDataLocation(xyLoc[0], xyLoc[1])

        endelse
    endif

    ; Probe location:
    if (ARG_PRESENT(probeLocation)) then begin
        probeLocation = '['+   STRTRIM(STRING(xyLoc[0]),2) + $
                 ', ' + STRTRIM(STRING(xyLoc[1]),2) + $
                 ']'
    endif

end


;----------------------------------------------------------------------------
; PURPOSE:
;   This function method retrieves the LonLat range of
;   contained visualizations. Override the _Visualization method
;   so we can retrieve the correct range.
;
function IDLitVisContour::GetLonLatRange, lonRange, latRange, $
    MAP_STRUCTURE=sMap

    compile_opt idl2, hidden

    ; No units, failure.
    if (self._gridUnits ne 1 && self._gridUnits ne 2) then $
        return, 0

    xData = self->_GetXYdata('X')
    yData = self->_GetXYdata('Y')

    xmin = MIN(xData, MAX=xmax)
    ymin = MIN(yData, MAX=ymax)

    ; Units in degrees. Just return the range.
    if (self._gridUnits eq 2) then begin

        lonRange = [xmin, xmax]
        latRange = [ymin, ymax]

        return, 1
    endif

    ; Units must be in meters.

    if (N_TAGS(sMap) eq 0) then begin
        sMap = self->GetProjection()
    endif

    ; If our dataspace is actually in meters, failure.
    if (N_TAGS(sMap) eq 0) then $
        return, 0

    ; If the dataspace has a map projection,
    ; then convert the four corners back to degrees.
    lonlat = MAP_PROJ_INVERSE([xmin, xmax, xmax, xmin], $
        [ymin, ymin, ymax, ymax], $
        MAP_STRUCTURE=sMap)

    minn = MIN(lonlat, DIMENSION=2, MAX=maxx)
    lonRange = [minn[0], maxx[0]]
    latRange = [minn[1], maxx[1]]

    return, 1

end


;----------------------------------------------------------------------------
;+
; IDLitVisContour__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisContour object.
;
;-
pro IDLitVisContour__Define

    compile_opt idl2, hidden

    struct = { IDLitVisContour,         $
        inherits IDLitVisualization, $   ; Superclass: _IDLitVisualization
        _oContour: OBJ_NEW(),        $   ; IDLgrContour object
        _oPalette: OBJ_NEW(),        $
        _oLevels: OBJ_NEW(),         $   ; CONTOUR_LEVELS container
        _pProps: PTR_NEW(),          $   ; contour level registered properties
        _pLabelObjects: PTR_NEW(),   $   ; C_LABEL_OBJECTS cache
        _pShow: PTR_NEW(),           $   ; C_LABEL_SHOW cache
        _pLinestyle: PTR_NEW(),      $   ; C_LINESTYLE cache
        _pThick: PTR_NEW(),          $   ; C_THICK cache
        _equation: '', $
        _eqnSamples: [0L, 0L], $
        _eqnUserdata: PTR_NEW(), $
        _withinUpdateData: 0b,       $
        _xrange: DBLARR(2),          $
        _yrange: DBLARR(2),          $
        _gridUnits: 0b,              $ ; List of possible units
        _nLevels: 0s, $
        _xVisLog : 0b, $
        _yVisLog : 0b, $
        _zVisLog : 0b, $
        _zvalue: 0d,                 $
        _cValSet: 0b,                $
        _bClipped: 0b,               $ ; Flag: does contour lie entirely
                                          ;   outside of current data range?
        _preClipHide: 0b,            $ ; HIDE setting prior to clip.
        _palColor: 0b,               $
        _defaultIndices: 0b,         $
        _hasMapProjection: 0b,       $   ; map projection in effect?
        _tickinterval: 0d,           $
        _ticklen: 0d,                $
        _minValue: 0d, $
        _maxValue: 0d $
    }
end
