; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitviscolorbar__define.pro#1 $
;
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
; Purpose:
;    The IDLitVisColorbar class is the component wrapper for the colorbar.
;
; MODIFICATION HISTORY:
;     Written by:   CT, July 2002.
;


;----------------------------------------------------------------------------
; Purpose:
;    Initialize this component
;
; Arguments:
;   None.
;
; Keywords:
;   All keywords that can be used for IDLitVisualization
;
; Result:
;    This function method returns 1 on success, or 0 on failure.
;
function IDLitVisColorbar::Init, TOOL=tool, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Initialize superclass
    success = self->IDLitVisualization::Init( TYPE="IDLCOLORBAR", $
        /MANIPULATOR_TARGET, $
        NAME="Colorbar", $
        ICON='colorbar', $
        IMPACTS_RANGE=0, $   ; should not affect DataSpace range
        DESCRIPTION="A Colorbar Visualization", $
        TOOL=tool, $
        _EXTRA=_extra)

    if (~success) then $
        return, 0

    ; Create Parameter
    self->RegisterParameter, 'PALETTE', $
        DESCRIPTION='Image Data', $
        /INPUT, TYPES=['IDLPALETTE','IDLARRAY2D']

    self->RegisterParameter, 'OPACITY TABLE', $
        DESCRIPTION='Opacity Data', $
        /INPUT, TYPES=['IDLOPACITY_TABLE','IDLVECTOR']

    ; handle data of visContour, visVolume, etc.
    self->RegisterParameter, 'VISUALIZATION DATA', $
        DESCRIPTION='Visualization Data', $
        /INPUT, TYPES=['IDLVECTOR', 'IDLARRAY2D', 'IDLARRAY3D']

    self._oPalette = OBJ_NEW('IDLgrPalette', $
        RED=BINDGEN(256), $
        GREEN=BINDGEN(256), $
        BLUE=BINDGEN(256))

    ; Create Image object and add it to this Visualization.
    self._oPolygon = OBJ_NEW('IDLgrPolygon', PALETTE=self._oPalette, /PRIVATE)
    self->Add, self._oPolygon

    self._oAxis = OBJ_NEW('IDLitVisAxis', $
        TICKLEN=0.25d, $
        /EXACT, $
        FONT_SIZE=9, $
        TOOL=tool, $
        MANIPULATOR_TARGET=0, $
        /private)

    self->Add, self._oAxis, /AGGREGATE

    self._oBorder = OBJ_NEW('IDLgrPolyline', /HIDE, /PRIVATE)
    self->Add, self._oBorder

    self->_RegisterProperties

    ; Ensure that our colorbar is above our dataspace in Z.
    self->IDLgrModel::Translate, 0, 0, 0.99d, /PREMULTIPLY

    ; Set any properties
    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisColorbar::SetProperty, _EXTRA=_extra

    RETURN, 1 ; Success
end


;----------------------------------------------------------------------------
; Purpose:
;    Cleanup this component
;
pro IDLitVisColorbar::Cleanup

    compile_opt idl2, hidden

    OBJ_DESTROY, self._oPolygon
    OBJ_DESTROY, self._oPalette
    PTR_FREE, self._rgbTable
    PTR_FREE, self._tickname

    ; Cleanup superclass
    self->IDLitVisualization::Cleanup
end


;----------------------------------------------------------------------------
pro IDLitVisColorbar::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

    compile_opt idl2, hidden

    registerAll = ~KEYWORD_SET(updateFromVersion)

    if (registerAll) then begin

        ; Add general properties
        self->RegisterProperty, 'BORDER_ON', /BOOLEAN, $
            DESCRIPTION='Turn on border', $
            NAME='Border', /ADVANCED_ONLY

        self->RegisterProperty, 'Orientation', $
            ENUMLIST=['Horizontal', 'Vertical'], $
            DESCRIPTION='Orientation'

        self->RegisterProperty, 'Location',$
            USERDEF="Location", $
            DESCRIPTION="Location", /HIDE, /ADVANCED_ONLY

    endif

    if (registerAll || (updateFromVersion lt 620)) then begin
        ; Previous releases did not hide the DATA_POSITION
        ; (a.k.a., 'Lock to Data Position')property.
        ; Reset back to appropriate value, and hide.
        self._oAxis->SetProperty, DATA_POSITION=0
        self._oAxis->SetPropertyAttribute, 'DATA_POSITION', /HIDE, $
          /ADVANCED_ONLY
        self._oAxis->SetPropertyAttribute, 'TRANSPARENCY', NAME='Axis transparency'

        self->RegisterProperty, 'Taper', $
            ENUMLIST=['None', 'Both ends', 'Low end', 'High end'], $
            DESCRIPTION='Taper the ends'

    endif

end


;----------------------------------------------------------------------------
; IDLitVisColorbar::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save files to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisColorbar::Restore
    compile_opt idl2, hidden

    ; Call superclass restore.
    self->_IDLitVisualization::Restore

    ; Call ::GetProperty on each aggregated graphic object
    ; to force its internal restore process to be called, thereby
    ; ensuring any new properties are registered.
    self._oAxis->Restore
    self._oAxis->UpdateComponentVersion

    ; Register new properties.
    self->IDLitVisColorbar::_RegisterProperties, $
        UPDATE_FROM_VERSION=self.idlitcomponentversion

    ; ---- Required for SAVE files transitioning ----------------------------
    ;      from IDL versions less than 8.2 to 8.2 or above:
    if (self.idlitcomponentversion lt 820) then begin
      ; We no longer use the texture-mapped polygon.
      oOld = (self->Get(/ALL, ISA='IDLgrPolygon'))[0]
      if (OBJ_VALID(oOld) && oOld ne self._oPolygon) then begin
          self->Remove, oOld
          OBJ_DESTROY, oOld
      endif
      ; We switched from an image to a polygon
      oOld = (self->Get(/ALL, ISA='IDLgrImage'))[0]
      if OBJ_VALID(oOld) then begin
        self->Remove, oOld
        OBJ_DESTROY, oOld
      endif

      self._oPolygon = OBJ_NEW('IDLgrPolygon', PALETTE=self._oPalette, /PRIVATE)
      self->Add, self._oPolygon

      self._oBorder = OBJ_NEW('IDLgrPolyline', /HIDE, /PRIVATE)
      self->Add, self._oBorder

      ; Force the bar to be updated.
      self._oAxis->GetProperty, RANGE=dataRange
      self->SetProperty, RANGE=dataRange
    endif
      
end


;----------------------------------------------------------------------------
pro IDLitVisColorbar::_UpdateBar

  compile_opt idl2, hidden

  if (~ISA(self._rgbTable)) then $
    self._rgbTable = PTR_NEW(TRANSPOSE(BINDGEN(256,3)))

  ncolor = N_ELEMENTS((*self._rgbTable)[0,*])

  range = self._dataRange
  rspan = self._dataRange[1] - self._dataRange[0]
  delta = rspan/ncolor
  if (rspan eq 0) then $
    return

  hasLabels = ISA(self._tickname) && N_ELEMENTS(*self._tickname) gt 1

  isDiscreteBar = 0b

  if (hasLabels) then begin

    labels = *self._tickname
    ; Clip the # of labels to at most 1 more than the # of colors
    nlabel = N_ELEMENTS(labels) < (ncolor + 1)
    labels = labels[0:nlabel-1]

    ; If we have labels, and we have at least as many labels as colors,
    ; then assume we have a "discrete" colorbar.
    isDiscreteBar = nlabel ge ncolor

    if (isDiscreteBar) then begin

      tickv = DINDGEN(nlabel)
      ; If we have an equal # of labels and colors, move the labels so they
      ; are in the middle of the color boxes.
      if (nlabel eq ncolor) then tickv += 0.5
      tickv = tickv/ncolor*rspan + self._dataRange[0]

      if (self._taper eq 1 || self._taper eq 2) then begin
        range[0] += delta
        if (hasLabels) then begin
          nlabel--
          tickv = tickv[1:*]
          labels = labels[1:*]
        endif
      endif
      if (self._taper eq 1 || self._taper eq 3) then begin
        range[1] -= delta
        nlabel--
        tickv = tickv[0:-2]
        labels = labels[0:-2]
      endif

    endif

  endif


  self._oPalette->SetProperty, $
    RED=(*self._rgbTable)[0,*], $
    GREEN=(*self._rgbTable)[1,*], $
    BLUE=(*self._rgbTable)[2,*]


  self._oAxis->SetProperty, RANGE=range

  if (isDiscreteBar) then $
    self._oAxis->SetProperty, TICKLEN=0, MINOR=0

  if (hasLabels) then begin
    self._oAxis->SetProperty, MAJOR=nlabel
    self._oAxis->SetProperty, TICKVALUES=tickv, TICKNAME=labels
  endif

  self._coord_conv = [-self._dataRange[0]/rspan, 1.0/rspan]


  self._oAxis->GetProperty, $
    DIRECTION=currentorientation, COLOR=axisColor, THICK=axisThick


  if ARRAY_EQUAL(self._coord_conv, [0d,0]) then $
    self._coord_conv=[0d,1]

  case currentorientation of
    0 : self._oAxis->SetProperty, $
      XCOORD_CONV=self._coord_conv,YCOORD_CONV=[0,0.05d]
    1 : self._oAxis->SetProperty, $
      XCOORD_CONV=[0,0.05d],YCOORD_CONV=self._coord_conv
  endcase


  n = ncolor
  polydata = FLTARR(2, 2*(n+1))
  polydata[currentorientation,*] = (LINDGEN(2*(n+1))/2)/FLOAT(n)
  polydata[1-currentorientation,0:*:2] = 0 
  polydata[1-currentorientation,1:*:2] = 0.1
  index = 2*LINDGEN(n)
  polygons = LONARR(6*n)
  polygons[0:*:6] = 5
  polygons[1:*:6] = index
  polygons[2:*:6] = index+2
  polygons[3:*:6] = index+3
  polygons[4:*:6] = index+1
  polygons[5:*:6] = index
  vertcolors = (LINDGEN(2*(n+1))/2) < (n-1)

  ; TAPER
  if (isDiscreteBar) then begin
    if (self._taper eq 1 || self._taper eq 2) then begin
      polydata[1-currentorientation,0:1] = 0.05
    endif
    if (self._taper eq 1 || self._taper eq 3) then begin
      polydata[1-currentorientation,-2:-1] = 0.05
    endif
  endif

  self._oPolygon->SetProperty, DATA=polydata, POLYGON=polygons, $
    VERT_COLORS=vertcolors
  
  
  ; BORDER
  if (isDiscreteBar) then begin
    borderdata = polydata
    polylines = polygons
  endif else begin
    borderdata = [[0,0],[1,0],[1,0.1],[0,0.1]]
    if (currentorientation) then borderdata = borderdata[[1,0],*]
    polylines = [5,0,1,2,3,0]
  endelse

  self._oBorder->SetProperty, DATA=borderdata, POLYLINES=polylines, $
    COLOR=axisColor, THICK=axisThick

  self->UpdateSelectionVisual

end


;----------------------------------------------------------------------------
; Compute the new position and cache it.
;
pro IDLitVisColorbar::_ComputePosition
  compile_opt idl2, hidden
  self->IDLgrModel::GetProperty, TRANSFORM=trCurr
  ; IDL-68696: Only do this computation if the colorbar has not been rotated.
  ; We will have to fix this when we can generate code from graphics.
  if (ABS(trCurr[1,0]) lt 1d-5 && ABS(trCurr[0,1]) lt 1d-5) then begin
    !NULL = self->GetXYZRange(xr,yr,zr)
    conv = iConvertCoord(xr,yr, /ANNOTATION, /TO_NORMAL, TARGET_IDENTIFIER=self)
    self._position = [conv[0:1,0],conv[0:1,1]]
  endif
end


;----------------------------------------------------------------------------
; IIDLProperty Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisColorbar::GetProperty
;
; PURPOSE:
;      This procedure method retrieves the
;      value of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisColorbar::]GetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLitVisColorbar::Init followed by the word "Get"
;      can be retrieved using IDLitVisColorbar::GetProperty.  In addition
;      the following keywords are available:
;
;      ALL: Set this keyword to a named variable that will contain
;              an anonymous structure containing the values of all the
;              retrievable properties associated with this object.
;              NOTE: UVALUE is not returned in this struct.
;-
pro IDLitVisColorbar::GetProperty, $
    BORDER_ON=border, $
    LOCATION=location, $
    ORIENTATION=orientation, $
    POSITION=position, $
    PARENT=parent, $
    RANGE=dataRange, $
    RGB_TABLE=rgbTable, $
    TAPER=taper, $
    TICKLEN=ticklen, $
    TRANSPARENCY=transparency, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if (ARG_PRESENT(rgbTable)) then begin
      self._oPalette->GetProperty, RED=red, GREEN=green, BLUE=blue
      rgbTable = TRANSPOSE([[red], [green], [blue]])
    endif

    ; Handle our properties.
    if ARG_PRESENT(border) then begin
      self._oBorder->GetProperty, HIDE=hide
      border = 1-hide
    endif

    if ARG_PRESENT(location) then $
        location = self._location

    if ARG_PRESENT(position) then begin
      position = self._position
    endif    

    if ARG_PRESENT(dataRange) then begin
      self._oAxis->GetProperty, RANGE=dataRange
    endif

    if ARG_PRESENT(taper) then $
      taper = self._taper

    ; Convert from grAxis tick length to our normalized tick length.
    if ARG_PRESENT(ticklen) then begin
        self._oAxis->GetProperty, TICKLEN=ticklen
    endif

    if (ARG_PRESENT(transparency)) then begin
      self._oAxis->GetProperty, TRANSPARENCY=transparency
    endif

    ; The ORIENTATION property has the same value as the axis DIRECTION.
    ; All other axis properties are handled by aggregation.
    if (ARG_PRESENT(orientation)) then $
      self._oAxis->GetProperty, DIRECTION=orientation

    ; get superclass properties
    if ((N_ELEMENTS(_extra) gt 0) || ARG_PRESENT(parent)) then $
        self->IDLitVisualization::GetProperty, PARENT=parent, _EXTRA=_extra

end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisColorbar::SetProperty
;
; PURPOSE:
;      This procedure method sets the value
;      of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisColorbar::]SetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLitVisColorbar::Init followed by the word "Set"
;      can be set using IDLitVisColorbar::SetProperty.
;-

pro IDLitVisColorbar::SetProperty,  $
    BORDER_ON=border, $
    COLOR=color, $
    DIRECTION=swallow, $  ; interferes with orientation
    LOCATION=location, $
    POSITION=position, $
    ORIENTATION=orientation, $
    RANGE=dataRange, $
    RGB_TABLE=rgbTableIn, $
    TAPER=taper, $
    TEXTPOS=textpos, $
    THICK=thick, $
    TICKLEN=ticklen, $
    TICKNAME=tickname, $ ; not registered, handle manually
    TICKVALUES=tickvalues, $ ; not registered, handle manually
    TRANSPARENCY=transparency, $
    TRANSFORM=transform, $
    _EXTRA=_extra

    compile_opt idl2, hidden


    modifiedprops = ''

    self->IDLitComponent::GetProperty, Initializing=isInit


    if (ISA(rgbTableIn)) then begin
      rgbTable = rgbTableIn

      if (~ISA(self._rgbTable)) then begin
        self._rgbTable = PTR_NEW(/ALLOC)
      endif

      ; Handle a color table number
      if (N_Elements(rgbTable) eq 1) then begin
        rgbTable = Colortable(rgbTableIn)
      endif

      if (SIZE(rgbTable, /N_DIMENSIONS) EQ 2) then begin
        ; Handle either 3xM or Mx3, but convert to 3xM to store.
        dim = SIZE(rgbTable, /DIMENSIONS)
        if (dim[1] eq 3) then rgbTable = TRANSPOSE(rgbTable)
      endif else if ISA(rgbTable, "STRING") then begin
        Style_Convert, rgbTable[*], COLOR=rgbTable
      endif

      ; If we had an old color table, and the data range matched the old
      ; number of colors, then reset the range. This will get set again below.
      if ISA(*self._rgbTable) then begin
        nOldColor = N_ELEMENTS((*self._rgbTable)[0,*])
        if (self._dataRange[0] eq 0 && self._dataRange[1] eq nOldColor) then $
          self._dataRange = [0,0]
      endif

      *self._rgbTable = rgbTable
      ncolor = N_ELEMENTS((*self._rgbTable)[0,*])
      ; If the data range has never been set, then set it equal
      ; to the size of the new RGB table.
      if (ARRAY_EQUAL(self._dataRange, [0,0])) then $
        self._dataRange = [0, ncolor]
      updateBar = 1b
    endif

    ; Set my properties.
    if (N_ELEMENTS(border) eq 1) then begin
      self._oBorder->SetProperty, HIDE=1-KEYWORD_SET(border)
    endif

    if ((N_ELEMENTS(color) gt 0) || $
        (N_ELEMENTS(thick) gt 0)) then begin
        if (ISA(color, 'STRING') || N_ELEMENTS(color) eq 1) then $
          style_convert, color[0], COLOR=color
        self._oBorder->SetProperty, COLOR=color, THICK=thick
        self._oAxis->SetProperty, COLOR=color, THICK=thick
    endif

    if (N_ELEMENTS(location) eq 3) then begin
      if (~isInit) then begin
        self->Reset
        self->Translate, location[0], location[1], location[2], /PREMULTIPLY
      endif
      self._location = location
    endif

    ; Convert from our normalized tick length to grAxis tick length.
    if (N_ELEMENTS(ticklen) gt 0) then begin
      self._oAxis->SetProperty, TICKLEN=ticklen
    endif


    self._oPalette->SetProperty, _EXTRA=_extra

    ; Note: All axis properties are handled by aggregation.

    ; If the text position was flipped, then we also need to shift
    ; the axis.
    if (N_ELEMENTS(textpos) eq 1) then begin
        self._oAxis->SetProperty, LOCATION=textpos ? [2d, 2d, 0] : [0, 0, 0]
    endif


    ; Flip the orientation.
    if (N_ELEMENTS(orientation) eq 1) then begin
      
      if (~isInit) then begin
        ; Save current location
        self->GetProperty, TRANSFORM=tr
        self._location = tr[3,0:2]
        ; Lose rotation and scaling
        self->Reset
        ; Restore current location
        self->Translate, self._location[0], self._location[1], $
          self._location[2], /PREMULTIPLY
      endif

      self._oAxis->SetProperty, DIRECTION=orientation
      updateBar = 1b
    endif


    IF (n_elements(dataRange) EQ 2 && ~isInit) THEN BEGIN
      self._dataRange = dataRange
      updateBar = 1b
    ENDIF

    if (N_ELEMENTS(position) eq 4) then begin
      self._position = position
      ; Reset our _dims and transform matrix so we recompute
      ; the position in the ::Draw.
      self._dims = [0,0]
      self->Reset
    endif

    if (ISA(taper)) then begin
      self._taper = 0 > taper < 3
      updateBar = 1b
    endif


    if (N_ELEMENTS(transparency) eq 1) then begin
      alphaValue = 0.0 > ((100 - transparency) * 0.01d) < 1.0
      self._oAxis->SetProperty, TRANSPARENCY=transparency
      self._oPolygon->SetProperty, ALPHA=alphaValue
      self._oBorder->SetProperty, ALPHA=alphaValue
    endif

    ; These properties are not registered on the Axis, so we need
    ; to handle them manually.
    if (ISA(tickvalues)) then begin
      ; We need to set the tickname at the same time, otherwise the names
      ; won't get used by the new tick values.
      if (ISA(self._tickname)) then tname = *self._tickname
      self._oAxis->SetProperty, TICKNAME=tname, TICKVALUES=tickvalues
    endif

    if (ISA(tickname)) then begin
      if (~ISA(self._tickname)) then begin
        self._tickname = PTR_NEW(/ALLOC)
      endif
      *self._tickname = tickname
      updateBar = 1b
    endif


    if (~isInit && KEYWORD_SET(updateBar)) then $
      self->_UpdateBar

    ; Set superclass properties
    self->IDLitVisualization::SetProperty, $
        TEXTPOS=textpos, $
        _EXTRA=_extra


    ; We must send this directly to the grModel without going thru the
    ; property aggregation system.
    if (N_ELEMENTS(transform) gt 0) then begin
      self->IDLgrModel::SetProperty, TRANSFORM=transform
      self->_ComputePosition
    endif

end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to retrieve the data from the grPalette
;
; Arguments:
;   RED (or 3xM array), GREEN, BLUE
;
; Keywords:
;   NONE
;
pro IDLitVisColorbar::GetData, arg1, arg2, arg3, _EXTRA=_extra
  compile_opt idl2, hidden
  
  self._oPalette->GetProperty, RED=arg1, GREEN=arg2, BLUE=arg3

  if (N_PARAMS() eq 1) then $
    arg1 = TRANSPOSE([[arg1], [arg2], [arg3]])
  
end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to directly set the data
;
; Arguments:
;   RED (or 3xM array), GREEN, BLUE
;
; Keywords:
;   NONE
;
pro IDLitVisColorbar::PutData, red, green, blue, _EXTRA=_extra
  compile_opt idl2, hidden

  ;; Do not allow setting of data on the color bar
  message, ''
  
end


;----------------------------------------------------------------------------
; IIDLDataObserver Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; IDLitVisColorbar::OnDataDisconnect
;
; Purpose:
;   This is called by the framework when a data item has disconnected
;   from a parameter on the surface.
;
; Parameters:
;   ParmName   - The name of the parameter that was disconnected.
;
;
;-
PRO IDLitVisColorbar::OnDataDisconnect, ParmName
   compile_opt hidden, idl2

   ; Just check the name and perform the desired action
   switch ParmName of
       'PALETTE':
       'OPACITY TABLE':
;       'VISUALIZATION DATA': self->SetProperty, HIDE=1
       else:
   endswitch
end


;----------------------------------------------------------------------------
; METHODNAME:
;    IDLitVisColorbar::OnDataChangeUpdate
;
; PURPOSE:
;    This procedure method is called by a Subject via a Notifier when
;    its data has changed.  This method obtains the data from the subject
;    and updates the IDLgrImage object.
;
; CALLING SEQUENCE:
;
;    Obj->[IDLitVisColorbar::]OnDataChangeUpdate, oSubject, ParmName
;
; INPUTS:
;    oSubject: The Subject object in the Subject-Observer relationship.
;    This object (the image) is the observer, so it uses the
;    IIDLDataSource interface to get the data from the subject.
;    Then, it puts the data in the IDLgrImage object.
;
pro IDLitVisColorbar::OnDataChangeUpdate, oSubject, parmName
  compile_opt idl2, hidden

  case STRUPCASE(parmName) OF
    '<PARAMETER SET>': begin
      parmNames = ['PALETTE', 'OPACITY TABLE', 'VISUALIZATION DATA']
      for i=0, N_ELEMENTS(parmNames)-1 do begin
        oData = oSubject->GetByName(parmNames[i], count=nCount)
        if ncount ne 0 then begin
                                ; vector to code below
          self->OnDataChangeUpdate,oData,parmNames[i]
        endif
      endfor
    END
    'PALETTE': begin
      success = oSubject->GetData(palette)
      if (N_ELEMENTS(palette) gt 0) then begin
        self->SetProperty, RGB_TABLE=palette
      endif
    END
    'OPACITY TABLE': begin
      success = oSubject->GetData(palette)
      if (N_ELEMENTS(palette) gt 0) then begin
        self->SetProperty, RGB_TABLE=palette
      endif
    END
    'VISUALIZATION DATA': begin
      success = oSubject->GetData(visData)
      if (N_ELEMENTS(visData) gt 0) then begin
        ; For byte data, to match iImage behavior, set range to 0-255.
        if (SIZE(visData[0], /TYPE) eq 1) then begin
            dataMin = 0b
            dataMax = 255b
        endif else begin
            dataMin = MIN(visData, MAX=dataMax, /NAN)
        endelse
        self->SetProperty, RANGE=[dataMin, dataMax]
      ENDIF
    END

    else:                       ; ignore unknown parameters
  ENDCASE

END


;-----------------------------------------------------------------------------
pro IDLitVisColorbar::_UpdateContour, oVis

  compile_opt idl2, hidden

  c = oVis._oContour
  if (~ISA(c)) then return

  oVis->GetProperty, LABEL_FORMAT=labelFormat, $
    MAX_VALUE=maxValue, MIN_VALUE=minValue, $
    Z_VIS_LOG=zVisLog

  c->GetProperty, C_VALUE=cvalue, C_COLOR=ccolor, $
    FILL=isFilled, N_LEVELS=nLevels, $
    PALETTE=oPalette, C_LABEL_OBJ=labelObj

  if (zVisLog) then cvalue = 10^cvalue

  if (labelFormat eq '') then begin
    ; If our tick values can be rounded to integers, without causing
    ; any overlap, then use integer format. Otherwise, use floating-pt format.
    intValues = ROUND(cvalue)
    nuniq = N_ELEMENTS(UNIQ(intValues, SORT(intValues)))
    allowInt = nuniq eq N_ELEMENTS(intValues)
    tickname = allowInt ? STRTRIM(intValues, 2) : $
      STRTRIM(STRING(cvalue, FORMAT='(G0)'), 2)
  endif else begin
    if (STRPOS(labelFormat, '(') ge 0) then begin
      tickname = STRING(cvalue, FORMAT=labelFormat)
    endif else begin
      tickname = STRARR(N_ELEMENTS(cvalue))
      for i=0,N_ELEMENTS(cvalue)-1 do begin
        tickname[i] = CALL_FUNCTION(labelFormat, 2, i, cvalue[i])
      endfor
    endelse
  endelse


  ; If we have label text objects, use those string values instead.
  if (ISA(labelObj)) then begin

    ; Note that we might have extra (useless) label objects, so discard them.
    n = N_ELEMENTS(labelObj) < N_ELEMENTS(cvalue)

    if (n gt 1) then begin

      for i=0,n-1 do begin
        if ISA(labelObj[i], 'IDLgrText') then begin
          ; Only fill in the user-specified labels, but keep the rest of the
          ; tickname array unchanged.
          labelObj[i]->GetProperty, STRINGS=label
          tickname[i] = label[0]
        endif
      endfor

    endif else begin

      if ISA(labelObj, 'IDLgrText') then begin
        labelObj->GetProperty, STRINGS=tickname
      endif

    endelse

  endif

  ; IDLgrContour never fills above the last contour line, so delete it
  ; from the colorbar.
  if (isFilled) then begin
    nLevels--
  endif

  if (ISA(oPalette) && SIZE(ccolor, /N_DIM) eq 1) then begin
    nLevels = nLevels < N_ELEMENTS(ccolor)
    ccolor = ccolor[0:nLevels-1]
    tickname = tickname[0:((nLevels+1) < N_ELEMENTS(tickname))-1]
    if (ISA(oPalette)) then begin
      oPalette->GetProperty, BLUE=blue, GREEN=green, RED=red
      colortable = TRANSPOSE([[red], [green], [blue]])
    endif else begin
      colortable = BINDGEN(3,256)
    endelse
    rgbTable = colortable[*, ccolor]
  endif else if (SIZE(ccolor, /N_DIM) eq 2) then begin
    nLevels = nLevels < N_ELEMENTS(ccolor[0,*])
    tickname = tickname[0:((nLevels+1) < N_ELEMENTS(tickname))-1]
    rgbTable = ccolor[*,0:nLevels-1]
  endif

  if (isFilled) then begin

    ; If our contour is filled, then add an extra blank label at the end
    ; of the colorbar, so that the labels are underneath the divisions
    ; between the colors, instead of underneath the color swatches.
    if (N_ELEMENTS(rgbTable)/3 eq N_ELEMENTS(tickname)) then begin
      tickname = [tickname, '']
    endif

    self._taper = 0

    if (cvalue[0] le minValue) then $
      self._taper = 2

    if (cvalue[-1] ge maxValue) then $
      self._taper = 3 - self._taper

  endif

  self->SetProperty, RGB_TABLE=rgbTable, TICKNAME=tickname

end


;;-----------------------------------------------------------------------------
;; IDLitVisColorbar::OnNotify
;;
;; Purpose:
;;    Updates the colourbar if the data or bytescaling has changed
;;
;; Parameters:
;;   STRITEM - The id of the target vis
;;
;;   STRMSG - The notification message
;;
;;   STRUSER - Not used
;;
;; Keywords:
;;   NONE
;;
PRO IDLitVisColorbar::OnNotify, strItem, StrMsg, strUser
  compile_opt idl2, hidden

  switch StrMsg of

    "IMAGECHANGED" : ; fall thru

    "SETPROPERTY": begin
      oTool = self->GetTool()
      if (~ISA(oTool)) then break
      oVis = oTool->GetByIdentifier(strItem)
      
      if (ISA(oVis,'IDLitVisContour')) then begin

        self->_UpdateContour, oVis

      endif else if (ISA(oVis,'IDLitVisImage')) then begin

        if (strUser eq '' || $
          strUser eq 'MIN_VALUE' || strUser eq 'MAX_VALUE') then begin
          oVis->GetProperty, MIN_VALUE=bMin, MAX_VALUE=bMax
          self->SetProperty, RANGE=[bMin,bMax]
        endif

      endif

      break
    end

  endswitch

end


;-----------------------------------------------------------------------------
pro IDLitVisColorbar::Translate, Tx, Ty, Tz, PREMULTIPLY=premult

  compile_opt idl2, hidden

  self->IDLgrModel::Translate, Tx, Ty, Tz, PREMULTIPLY=premult
  self->_ComputePosition
end


;-----------------------------------------------------------------------------
; Override IDLgrModel::Draw so we can
; automatically adjust for changes in aspect ratio.
;
pro IDLitVisColorbar::Draw, oDest, oLayer

  compile_opt idl2, hidden

  catch, iErr
  if (iErr ne 0) then begin
      ; Quietly return from errors so we don't crash IDL in the draw loop.
      catch, /cancel
      return
  endif

  ; Don't do extra work if we are in the lighting or selection pass.
  oDest->GetProperty, DIMENSIONS=dims, IS_BANDING=isBanding, $
      IS_LIGHTING=isLighting, IS_SELECTING=isSelecting

  if (~isLighting && ~isSelecting && ~isBanding) then begin
    position = self._position
    if (~ARRAY_EQUAL(self._dims, dims)) then begin
      self._dims = dims
      self->IDLgrModel::GetProperty, TRANSFORM=trCurr
      ; IDL-68696: Only do this computation if the colorbar has not been rotated.
      ; We will have to fix this when we can generate code from graphics.
      if (ABS(trCurr[1,0]) lt 1d-5 && ABS(trCurr[0,1]) lt 1d-5) then begin
        self._oAxis->GetProperty, DIRECTION=orientation
        scale = [0.0,0.0]
        loc = iConvertCoord(position[0:1], /TO_ANNOTATION, TARGET_IDENTIFIER=self)
        scale[0] = (position[2]-position[0])*2
        if (dims[0] gt dims[1]) then $
          scale[0] *= dims[0]/dims[1]
        scale[1] = (position[3]-position[1])*2
        if (dims[1] gt dims[0]) then $
          scale[1] *= dims[1]/dims[0]
        scale[~orientation] *= 10
        tr = IDENTITY(4)
        tr[0,0] = scale[0]
        tr[1,1] = scale[1]
        tr[3,0:2] = [loc[0], loc[1], 0.99]
        self->IDLgrModel::SetProperty, TRANSFORM=tr
      endif
    endif
  endif

  self->IDLgrModel::Draw, oDest, oLayer

  ; Mac sometimes throws floating underflows...
  void = CHECK_MATH()
end


;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; IDLitVisColorbar__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisColorbar object.
;
;-
pro IDLitVisColorbar__Define

    compile_opt idl2, hidden

    struct = { IDLitVisColorbar,           $
        inherits IDLitVisualization, $
        _oPolygon: OBJ_NEW(), $
        _oPalette: OBJ_NEW(),        $
        _oAxis: OBJ_NEW(),           $
        _oBorder: OBJ_NEW(),         $
        _location: DBLARR(3),        $
        _coord_conv: DBLARR(2),      $
        _position: DBLARR(4),        $
        _dataRange: DBLARR(2),    $
        _dims: LONARR(2),            $
        _rgbTable: PTR_NEW(), $
        _tickname: PTR_NEW(), $
        _taper: 0s $
    }
end
