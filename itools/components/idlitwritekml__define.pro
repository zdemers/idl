; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitwritekml__define.pro#2 $
;
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+


;-----------------------------------------------------------------------------
; helper function
function idlkml_isPolygonClockWise, points, npoints
  compile_opt idl2, hidden
  area = 0
  for i=1, npoints-2 do begin
    cross = ((points[0,i]-points[0,i-1]) * (points[1,i+1]-points[1,i])) - $
      ((points[1,i]-points[1,i-1]) * (points[0,i+1]-points[0,i]))
    area = area + cross
  endfor
  
  ; if area is negative, then it is clockwise
  return, (area lt 0)
  
end


;----------------------------------------------------------------------------
; Purpose:
;   This file implements the IDLitWriteKML class.
;

;---------------------------------------------------------------------------
; Lifecycle Routines
;---------------------------------------------------------------------------
; Purpose:
;   The constructor of the object.
;
; Arguments:
;   None.
;
; Keywords:
;   All superclass keywords.
;
function IDLitWriteKML::Init, $
    _EXTRA=_extra
    
    
  compile_opt idl2, hidden
  
  ; Init superclass
  ; The only properties that can be set at INIT time can be set
  ; in the superclass Init method.
  if (~self->IDLitWriter::Init(['kmz','kml'], $
    TYPES="IDLDEST", $
    NAME='Keyhole markup Language', $
    DESCRIPTION="Keyhole Markup Language", $
    ICON='demo', $
    _EXTRA=_extra)) then $
    return, 0
    
  ; Initialize ourself
    
    
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitWriteKML::SetProperty, _EXTRA=_extra
    
  return, 1
end


;---------------------------------------------------------------------------
; Purpose:
; The destructor for the class.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
;pro IDLitWriteKML::Cleanup
;    compile_opt idl2, hidden
;    ; Cleanup superclass
;    self->IDLitWriter::Cleanup
;end


;---------------------------------------------------------------------------
; Property Management
;---------------------------------------------------------------------------
; Purpose:
;   Used to get the value of the properties associated with this class.
;
; Arguments:
;   None.
;
; Keywords:
;   All ::Init keywords.
;
pro IDLitWriteKML::GetProperty, $
    _REF_EXTRA=_super
    
  compile_opt idl2, hidden
  
  
  if(n_elements(_super) gt 0) then $
    self->IDLitWriter::GetProperty, _EXTRA=_super
    
end


;---------------------------------------------------------------------------
; Purpose:
;   Used to set the value of the properties associated with this class.
;
; Arguments:
;   None.
;
; Keywords:
;   All ::Init keywords.
;
pro IDLitWriteKML::SetProperty, $
    _REF_EXTRA=_super
    
  compile_opt idl2, hidden
  
  
  if(n_elements(_super) gt 0)then $
    self->IDLitWriter::SetProperty, BIT_DEPTH=bitDepth, _EXTRA=_super
end


;---------------------------------------------------------------------------
; Implementation
;---------------------------------------------------------------------------

;---------------------------------------------------------------------------
; Purpose:
;   Procedure for displaying image in kml.
;
; Arguments:
;   ImageData: An object reference to the data to be written.
;
; Keywords:
;   None.
;
function IDLitWriteKML::SetImageOverlay, oVis, l_id, FILE_NAME=file_name
  compile_opt idl2, hidden
  strFilename = self->GetFilename()
  if (strFilename eq '') then $
    return, 0 ; failure
    
  odataSpace = ovis->GetDataSpace(/unnormalized)
  oproj=odataspace->_GetMapProjection()
  smap = oproj->_GetMapStructure()
  
  oVis->GetProperty, GRID_UNITS=gridUnits
  
  sep = PATH_SEP()
  
  isKMZ = strcmp(strmid(file_basename(strFilename), $
    strlen(file_basename(strFilename))-3, 3), 'kmz', /FOLD_CASE)
  ; if it is .kmz, still write .kml file
  if (isKMZ) then strFilename = file_dirname(strFilename) + sep + $
    file_basename(strFilename, 'kmz')+'kml'
    
  if(~(oVis->_GetImageDimensions(initDataSize, IMAGE_DATA=pImgData, $
    N_PLANES=nPlanes, N_DIMENSIONS=nDims))) then $
    return, 0
    
  ;  initImageData = bytarr(initDataSize[0], initDataSize[1], nPlanes)
  initImageData = MAKE_ARRAY(initDataSize[0], initDataSize[1], nPlanes, $
    TYPE=SIZE(*pImgData[0], /TYPE), /NOZERO)
  for i=0,nPlanes-1 do initImageData[0,0,i] = *pImgData[i]
  
  initImageData=oVis->ByteScaleData(pImgData, initImageData, nPlanes)
  
  
  if (nPlanes eq 1) then oVis->EnsurePalette, red, green, blue
  if (nPlanes eq 1) then begin
    ; We are guaranteed to have the palette from earlier.
    ; See if we have a simple grayscale ramp. If so we don't
    ; need to convert to RGB yet.
    gray = BINDGEN(256)
    isGray = ARRAY_EQUAL(red, gray) && $
      ARRAY_EQUAL(green, gray) && $
      ARRAY_EQUAL(blue, gray)
    if (~isGray) then begin
      initImageData = [ $
        [[red[initImageData]]], $
        [[green[initImagedata]]], $
        [[blue[initImageData]]]]
      nPlanes = 3
    endif
  endif
  initImageData = REFORM(initImageData, $
    initDataSize[0]*initDataSize[1], nPlanes, /OVERWRITE)
  initImageData = REFORM(TRANSPOSE(initImageData), $
    nPlanes, initDataSize[0], initDataSize[1])

  
  if (gridUnits ne 2) then begin

    ; If our image is in a particular map projection (units are "meters"),
    ; then warp the image back to Equirectangular, and save the warped image.
    ; Here we ignore any map limits and just output the entire image.

    umin = oVis._userorigin[0]
    vmin = oVis._userorigin[1]
    umax = umin + initDataSize[0]*oVis._userstep[0]
    vmax = vmin + initDataSize[1]*oVis._userstep[1]

    mapStruct = MAP_PROJ_INIT('Equirectangular', /GCTP)

    ; If we don't actually have an image map projection, just throw the image
    ; into the KML at location 0,0 on the Earth.
    imageStruct = ISA(oVis._oMapProj) ? oVis._oMapProj->_GetMapStructure() : 0
    if (N_TAGS(imageStruct) eq 0) then imageStruct = mapStruct

    imageData = BYTARR(4, initDataSize[0], initDataSize[1], /NOZERO)
    xindex = !null
    yindex = !null
    for i=0,nPlanes-1 do begin
      imageData[i,*,*] = MAP_PROJ_IMAGE(REFORM(initimagedata[i,*,*]), $
        [umin, vmin, umax, vmax], $
        IMAGE_STRUCTURE=imageStruct, MAP_STRUCTURE=mapStruct, $
        MASK=imagemask, UVRANGE=uvRange, XINDEX=xindex, YINDEX=yindex)
    endfor
    
    ; If we only have a single-band image, duplicate the bands.
    if (nPlanes eq 1) then begin
      imageData[1,*,*] = imageData[0,*,*]
      imageData[2,*,*] = imageData[0,*,*]
    endif
    
    ; Add an alpha channel containing the masked off regions. These will
    ; show up as transparent in Google Earth.
    imagedata[3,*,*] = 255b*imagemask

    ; Find the lat/lon extents of our now Equirectangular image.
    lat_lon = DBLARR(4)
    ll = MAP_PROJ_INVERSE(uvRange[0], uvRange[1], MAP=mapStruct)
    lat_lon[0] = ll[1]
    lat_lon[1] = ll[0]
    ll = MAP_PROJ_INVERSE(uvRange[2], uvRange[3], MAP=mapStruct)
    lat_lon[2] = ll[1]
    lat_lon[3] = ll[0]

  endif else begin

    ; Our image is in "degrees". No need to warp.
    ; But if the map only covers part of the image, then only output the
    ; clipped portion of the image.
    
    userOrigin = ovis._userorigin
    userStep = ovis._userStep
    ; find the intersection between limit from smap and limit from image data
    lat_lon=dblarr(4)
    lat_lon[0] = smap.ll_box[0] > userOrigin[1]
    lat_lon[1] = smap.ll_box[1] > userOrigin[0]
    ymax = userOrigin[1] + initDataSize[1]*userStep[1]
    lat_lon[2] = smap.ll_box[2] < ymax
    xmax = userOrigin[0] + initDataSize[0]*userStep[0]
    lat_lon[3] = smap.ll_box[3] < xmax
  
    pix_per_deg=1/userstep
    xindexes=floor([(lat_lon[1]-userorigin[0])*pix_per_deg[0], $
      (lat_lon[3]-userorigin[0])*pix_per_deg[0]])
    xindexes = 0 > xindexes < (initDataSize[0]-1)
    yindexes=floor( [(lat_lon[0]-userorigin[1]) * pix_per_deg[1], $
      (lat_lon[2]-userorigin[1]) * pix_per_deg[1]])
    yindexes = 0 > yindexes < (initDataSize[1]-1)
    imageData = initimagedata[*,xindexes[0]:xindexes[1], yindexes[0]:yindexes[1]]
  
  endelse
  
  
;  file_mkdir, file_dirname(strFilename) + sep + 'files'
  out_image_name = file_dirname(strFilename) + sep + '_' + $
    file_basename(strFilename, '.kml') + '_image' + $
    strtrim(string(l_id),2) + '.PNG'
  file_name = out_image_name
  
  WRITE_PNG, out_image_name, imageData


  file_north = max([lat_lon[0], lat_lon[2]], min=file_south)
  file_east = max([lat_lon[1], lat_lon[3]], min=file_west)
  file_north_string = strtrim(string(file_north, format='(F16.7)'),2)
  file_south_string = strtrim(string(file_south, format='(F16.7)'),2)
  file_east_string = strtrim(string(file_east, format='(F16.7)'),2)
  file_west_string = strtrim(string(file_west, format='(F16.7)'),2)
  
  on_ioerror, ioFailed
  OPENW, lun, strFilename, /GET_LUN, /APPEND
  
  printf, lun, '<Folder>'
  printf, lun, '<name>'+ oVis.name+'</name>'
  printf, lun, '<description>'+oVis.description+'</description>'
  printf, lun, '<GroundOverlay>'
  printf, lun, '<visibility>'+strtrim(string(1),2)+'</visibility>'
  printf, lun, '<Icon>'
  printf, lun, '<href>'+file_basename(out_image_name)+'</href>'
  printf, lun, '</Icon>'
  printf, lun, '<altitude>'+strtrim(string(oVis._zvalue),2)+'</altitude>'
  if(oVis._zvalue ne 0) then $
    printf, lun, '<altitudeMode>absolute</altitudeMode>'
  printf, lun, '<LatLonBox>'
  printf, lun, '<north>'+file_north_string+'</north>'
  printf, lun, '<south>'+file_south_string+'</south>'
  printf, lun, '<east>'+file_east_string+'</east>'
  printf, lun, '<west>'+file_west_string+'</west>'
  printf, lun, '</LatLonBox>'
  printf, lun, '</GroundOverlay>'
  printf, lun, '</Folder>'
  
  FREE_LUN, lun
  on_ioerror, null
  l_id=l_id+1
  return, 1
  
  ioFailed:
  self->ErrorMessage, $
    [IDLitLangCatQuery('Error:Framework:FileWriteError'), !ERROR_STATE.msg], $
    title=IDLitLangCatQuery('Error:Error:Title'), severity=2
  return, 0 ; failure
  
end

;---------------------------------------------------------------------------
; Purpose:
;   Procedure for displaying line in kml
;
; Arguments:
;   ImageData: An object reference to the data to be written.
;
; Keywords:
;   None.
;
function IDLitWriteKML::SetLine, oVis, l_id

  compile_opt idl2, hidden
  strFilename = self->GetFilename()
  if (strFilename eq '') then $
    return, 0 ; failure
    
  sep = PATH_SEP()
  isKMZ = strcmp(strmid(file_basename(strFilename), $
    strlen(file_basename(strFilename))-3, 3), 'kmz', /FOLD_CASE)
  ; if it is .kmz, still write .kml file
  if (isKMZ) then begin
    strFilename = file_dirname(strFilename) + sep + $
      file_basename(strFilename, 'kmz')+'kml'
  endif
  
  on_ioerror, ioFailed
  OPENW, lun, strFilename, /GET_LUN, /APPEND
  
  if (ISA(oVis, 'IDLitVisPlot')) then begin
    oVis->GetData, x, y, z
    num_points = N_ELEMENTS(x)
    points = DBLARR(3, num_points)
    points[0,*] = x
    points[1,*] = y
    if (ISA(z)) then points[2,*] = z
  endif else begin
    oVis->GetData, points
    num_points = N_ELEMENTS(points)/3
  endelse
  
  ; Retrieve connectivity array
  oDataObj = ovis->GetParameter('CONNECTIVITY')
  if (OBJ_VALID(oDataObj)) then begin
    success = oDataObj->GetData(conn)
  endif else begin
    conn = reform([lonarr(1,1)+num_points, lindgen(num_points,1)], num_points+1)
  endelse
  conn_dim = size(conn, /dimensions)
  
  ;if any of the z components is non zero, then use "absolute" altitude mode
  isZ = MAX(points[2,*] ne 0)
    
  oVis->GetProperty, THICK=line_thick, COLOR=line_color, $
    TRANSPARENCY=transparency
  alpha=0 > ((100.-transparency)/100) < 1
  
  if(isa(line_thick) ||isa(line_color)) then begin
    linestyle_id = '_Linestyle' + strtrim(string(oVis.name), 2) + $
      strtrim(string(l_id),2)
    printf, lun, '<Style id="'+linestyle_id+'">'
    printf, lun, '<LineStyle>'
    ; color is agbr
    if(isa(line_color)) then begin
      printf, lun, '<color>'+strtrim(string(alpha*255, format='(z2.2)'),2)+ $ ; alpha
        strtrim(string(line_color[2], format='(z2.2)'),2)+ $ ; blue
        strtrim(string(line_color[1], format='(z2.2)'),2)+ $ ; green
        strtrim(string(line_color[0], format='(z2.2)'),2)+ $ ; red
        '</color>'
    endif
    if(isa(line_thick))then $
      printf, lun, '<width>'+strtrim(string(line_thick),2)+'</width>'
    printf, lun, '</LineStyle>'
    printf, lun, '</Style>'
    l_id = l_id+1
  endif

  done = 0
  npoints=0
  while (done eq 0) do begin
    polypoints =conn[npoints]
    printf, lun, '<Placemark>'
    printf, lun, '<name>'+ strtrim(string(oVis.name)) +'</name>'
    printf, lun, '<description>'+strtrim(string(oVis.description))+'</description>'
    printf, lun, '<styleUrl>#'+linestyle_id+'</styleUrl>'
    printf, lun, '<LineString>'
    printf, lun, isZ ? '<altitudeMode>absolute</altitudeMode>' : $
      '<tessellate>1</tessellate>'
    printf, lun, '<coordinates>'
    
    ; polyline follows the line of great circle if it is clamped to the ground,
    ; hence interpolate point to get something similar.
    ; It looks like Google Earth has a limit of 65536 vertices, so don't let
    ; our interpolation generate too many points.
    factor = 1 > LONG(65500d/polypoints) < 20
    conn1 = conn[npoints+1:npoints+polypoints]

    if (~isZ) then begin
      points1 = CONGRID(points[0:1, conn1], 2, factor*polypoints, $
        /MINUS_ONE, /INTERP)
      s = STRCOMPRESS(STRING(points1, $
        FORMAT='(F16.7,",",F16.7)'), /REMOVE_ALL)
    endif else begin
      points1 = CONGRID(points[*, conn1], 3, factor*polypoints, $
        /MINUS_ONE, /INTERP)
      s = STRCOMPRESS(STRING(points1, $
        FORMAT='(F16.7,",",F16.7,",",F16.7)'), /REMOVE_ALL)
    endelse
    
    printf, lun, s

    npoints += polypoints + 1
    if (npoints eq conn_dim) then done = 1
    printf, lun, '</coordinates>'
    printf, lun, '</LineString>'
    printf, lun, '</Placemark>'
  endwhile

  FREE_LUN, lun
  on_ioerror, null
  return, 1
  
  ioFailed:
  self->ErrorMessage, $
    [IDLitLangCatQuery('Error:Framework:FileWriteError'), !ERROR_STATE.msg], $
    title=IDLitLangCatQuery('Error:Error:Title'), severity=2
  return, 0 ; failure
end


;---------------------------------------------------------------------------
; Purpose:
;   Procedure for displaying contour in kml
;
; Arguments:
;   ImageData: An object reference to the data to be written.
;
; Keywords:
;   None.
;

function IDLitWriteKML::SetContour, oVis, l_id
  compile_opt idl2, hidden
  
  strFilename = self->GetFilename()
  if (strFilename eq '') then $
    return, 0 ; failure
    
  sep = PATH_SEP()
  isKMZ = strcmp(strmid(file_basename(strFilename), $
    strlen(file_basename(strFilename))-3, 3), 'kmz', /FOLD_CASE)
  ; if it is .kmz, still write .kml file
  if (isKMZ) then strFilename = file_dirname(strFilename) + sep + $
    file_basename(strFilename, 'kmz')+'kml'
    
  on_ioerror, ioFailed
  OPENW, lun, strFilename, /GET_LUN, /APPEND
  
  oVis->getData, contour_data
  oDataObj = ovis->GetParameter('X')
  if(obj_valid(oDataObj)) then success = oDataObj->GetData(xparm)
  oDataObj = ovis->GetParameter('Y')
  if(obj_valid(oDataObj)) then success = oDataObj->GetData(yparm)
  oDataObj = ovis->GetParameter('PALETTE')
  if(obj_valid(oDataObj)) then success = oDataObj->GetData(palette)
  
  ; Retrieve the properties from our internal object so we are sure
  ; to get the actual levels and values, without IDLitVisContour trying
  ; to just give us what the user set.
  oVis._oContour->GetProperty, FILL=fill, $
    C_VALUE=c_valueIn, N_LEVELS=n_levelsIn, $
    PLANAR=planar, $
    ALPHA=alpha
  
  if (ISA(c_valueIn, /ARRAY)) then begin
    n_levels = N_ELEMENTS(c_valueIn)
    c_value = c_valueIn
  endif else begin
    n_levels = n_levelsIn
  endelse

  
  c_color = ptr_valid((ovis._ocontour).c_color) ? $
    *((ovis._ocontour).c_color) : *((ovis._ocontour).color)

  ; if c_color is a vector, then it is index to color palette.
  isIndex = 0
  if (size(c_color, /n_dimension) eq 1) then begin
    isIndex = 1
  endif

  if (ptr_valid((ovis._ocontour).c_thick)) then $
    line_thick = *((ovis._ocontour).c_thick)
    
  geomx = temporary(xparm)
  geomy = temporary(yparm)

  if (ovis._gridunits eq 1) then begin
    odataspace=ovis->GetDataSpace(/unnormalize)
    oProj=odataspace->_Getmapprojection(/NO_CREATE)
    smap = ISA(oProj) ? oProj->_GetMapStructure() : 0
    if (N_Tags(sMap) gt 0) then begin
      xy = map_proj_inverse(geomx, geomy, map_structure=smap)
      geomx = reform(xy[0,*])
      geomy = reform(xy[1,*])
    endif
  endif else if (ovis._gridunits eq 2) then begin
    ; Google Earth cannot handle filled contours if the longitudes are > 180.
    ; So if our longitudes go past 180, shift the array and subtract 360.
    if (SIZE(contour_data, /N_DIM) eq 2 && SIZE(geomx, /N_DIM) eq 1) then begin
      minn = MIN(geomx)
      i180 = (WHERE(geomx gt 180))[0]
      if (i180 ne -1 && minn ge 0 && minn lt 180) then begin
        geomx = [geomx[i180:*] - 360, geomx[0:i180-1]]
        contour_data = SHIFT(contour_data, -i180, 0)
      endif
    endif
  endif
  
  ; get the points for the contour lines and connectivity array.
  isocontour, contour_data, points, conn, $
    C_VALUE=c_value, N_LEVELS=n_levels, $
    GEOMX=geomx, GEOMY=geomy, $
    LEVEL_VALUES=level_values, $
    OUTCONN_INDICES=outconn_indices, FILL=fill

  points_dim = size(points, /dimensions)
  num_points = points_dim[1]
  conn_dim = size(conn, /dimensions)
  
  ; If we are in planar mode, set the Z values to 0.
  if (planar) then points[2,*] = 0

  printf, lun, '<Folder>'
  printf, lun, '<name>'+strtrim(oVis.name,2)+'</name>'


  ;set linestyle for different contour levels.
  style_id = strarr(n_elements(outconn_indices)/2)
  for i=0, n_elements(style_id)-1 do begin
    if ((outconn_indices[i*2] eq 0) && (outconn_indices[(i*2)+1] eq 0)) then $
      continue
    if (isIndex eq 1) then begin
      line_color = palette[*, c_color[i mod n_elements(c_color)]]
    endif else begin
      line_color = c_color[*,[i]]
    endelse
    style_id[i] = '_style' + strtrim(oVis.name, 2) + $
      strtrim(string(l_id),2)
    printf, lun, '<Style id="'+style_id[i]+'">'


    printf, lun, fill ? '<PolyStyle>' : '<LineStyle>'

    ; color is agbr
    printf, lun, '<color>'+strtrim(string(alpha*255, format='(z2.2)'),2)+ $ ; alpha
      strtrim(string(line_color[2], format='(z2.2)'),2)+ $ ; blue
      strtrim(string(line_color[1], format='(z2.2)'),2)+ $ ; green
      strtrim(string(line_color[0], format='(z2.2)'),2)+ $ ; red
      '</color>'
    
    if (fill) then begin
      printf, lun, '<fill>1</fill>'
      ; do not want to show outline because it looks bad visually,
      ; having outlines from multiple teselated polygons.
      printf, lun, '<outline>0</outline>'
      printf, lun, '</PolyStyle>'
    endif else begin
      printf, lun, '<width>'+strtrim(string(line_thick[i]),2)+'</width>'
      printf, lun, '</LineStyle>'
    endelse
    
    printf, lun, '</Style>'
    l_id = l_id+1
  endfor


  ; draw lines
  done = 0
  npoints=0
  contour_index=0
  pi=0 ; ith polyline in outconn
  (ovis._ocontour)->GetProperty, C_LABEL_OBJECTS=clabel_obj, $
    C_LABEL_SHOW=c_label_show
  n_label_objects = n_elements(clabel_obj)
  
  folder_label=1
  while (~done) do begin
    
    if ((outconn_indices[contour_index*2] eq 0) && $
      (outconn_indices[(contour_index*2)+1] eq 0)) then begin
      contour_index++
      continue
    endif
    
    description = STRTRIM(STRING(level_values[contour_index], FORMAT='(G15.6)'),2)
    
    ; if there are label objects, overwrite the label
    if (clabel_obj ne !null) then begin
      ; a single label object is provided, each of its strings will
      ; correspond to a contour level
      if(n_label_objects eq 1) then begin
        ; label is of idlgrtext
        if (obj_isa(clabel_obj, 'IDLGRTEXT')) then begin
          if  (ptr_valid(clabel_obj.strings)) then begin
            if (contour_index lt (n_elements(*(clabel_obj.strings)))) then begin
              description = strtrim(string((*(clabel_obj.strings))[contour_index]), 2)
            endif
          endif
        endif
      endif else begin ; vector of label objects
        if (contour_index lt n_label_objects) then begin
          if (obj_isa(clabel_obj[contour_index], 'IDLGRTEXT')) then begin
            if (obj_valid(clabel_obj[contour_index]) && $
              (ptr_valid(clabel_obj[contour_index].strings))) then begin
              description = strtrim(string(*(clabel_obj[contour_index].strings)), 2)
            endif
          endif
        endif
      endelse
    endif
    
    
    if (folder_label eq 1) then begin
      printf, lun, '<Placemark>'
      printf, lun, '<name>' + description + '</name>'
      
      if ((npoints ge outconn_indices[contour_index*2]) && $
        (npoints le outconn_indices[(contour_index*2)+1])) then begin
        printf, lun, '<styleUrl>#'+style_id[contour_index]+'</styleUrl>'
      endif
      printf, lun, '<MultiGeometry>'
      folder_label = 0
    endif
    
    
    polypoints =conn[npoints]
    
    if (fill) then begin  ; filled contours

      ; check if the polygon is clockwise or counterclockwise.
      ppoints = make_array(3, polypoints)
      for i=0, polypoints-1 do $
        ppoints[*,i] = points[*,conn[npoints+i+1]]
      isclockwise = idlkml_isPolygonClockWise(ppoints, polypoints)
      if (isclockwise) then begin
        conn1 = conn[npoints+polypoints:npoints+1:-1]
      endif else begin
        conn1 = conn[npoints+1:npoints+polypoints]
      endelse

      ; if for some reason, the connectivity array does not close, close it.
      if (conn1[0] ne conn1[-1]) then conn1 = [conn1, conn1[0]]
    
      printf, lun, '<Polygon>'
      printf, lun, '<tessellate>1</tessellate>'
      printf, lun, '<outerBoundaryIs>'
      printf, lun, '<LinearRing>'
      printf, lun, '<coordinates>'

      if (planar) then begin
        s = STRCOMPRESS(STRING(points[0:1,conn1], $
          FORMAT='(F16.7,",",F16.7)'), /REMOVE_ALL)
      endif else begin
        s = STRCOMPRESS(STRING(points[*,conn1], $
          FORMAT='(F16.7,",",F16.7,",",F16.7)'), /REMOVE_ALL)
      endelse

      printf, lun, s
      
      printf, lun, '</coordinates>'
      printf, lun, '</LinearRing>'
      printf, lun, '</outerBoundaryIs>'
      printf, lun, '</Polygon>'
    
    endif else begin  ; else line contours

      printf, lun, '<LineString>'
      printf, lun, '<tessellate>1</tessellate>'
      printf, lun, '<coordinates>'

      ; polyline follows the line of great circle if it is clamped to the ground,
      ; hence interpolate point to get something similar
      conn1 = conn[npoints+1:npoints+polypoints]

      if (planar) then begin
        points1 = CONGRID(points[0:1, conn1], 2, 10*polypoints, $
          /MINUS_ONE, /INTERP)
        s = STRCOMPRESS(STRING(points1, $
          FORMAT='(F16.7,",",F16.7)'), /REMOVE_ALL)
      endif else begin
        points1 = CONGRID(points[*, conn1], 3, 10*polypoints, $
          /MINUS_ONE, /INTERP)
        s = STRCOMPRESS(STRING(points1, $
          FORMAT='(F16.7,",",F16.7,",",F16.7)'), /REMOVE_ALL)
      endelse

      printf, lun, s
      
      printf, lun, '</coordinates>'
      printf, lun, '</LineString>'

    endelse  ; line contours

    pi++
    npoints += polypoints+1
    
    if(npoints gt outconn_indices[(contour_index*2)+1]) then begin
      contour_index=contour_index+1
      folder_label = 1
      printf, lun, '</MultiGeometry>'
      printf, lun, '</Placemark>'
    endif
    
    if (npoints eq conn_dim) then done = 1
  endwhile
  
  printf, lun, '</Folder>'

  FREE_LUN, lun
  on_ioerror, null
  return, 1
  
  ioFailed:
  self->ErrorMessage, $
    [IDLitLangCatQuery('Error:Framework:FileWriteError'), !ERROR_STATE.msg], $
    title=IDLitLangCatQuery('Error:Error:Title'), severity=2
  return, 0 ; failure
end


;---------------------------------------------------------------------------
; Purpose:
;   Procedure for writing line data out to the file.
;
; Arguments:
;   ImageData: An object reference to the data to be written.
;
; Keywords:
;   None.
;
function IDLitWriteKML::SetPolygon, oVis, l_id, sMap

  compile_opt idl2, hidden
  strFilename = self->GetFilename()
  if (strFilename eq '') then $
    return, 0 ; failure
    
  sep = PATH_SEP()
  isKMZ = strcmp(strmid(file_basename(strFilename), $
    strlen(file_basename(strFilename))-3, 3), 'kmz', /FOLD_CASE)
  ; if it is .kmz, still write .kml file
  if (isKMZ) then strFilename = file_dirname(strFilename) + sep + $
    file_basename(strFilename, 'kmz')+'kml'
  
  on_ioerror, ioFailed
  OPENW, lun, strFilename, /GET_LUN, /APPEND
  
  ; get the outline points and connectivity information
  oVis->getData, outline_points
  outline_points_dim = size(outline_points, /dimensions)
  num_outline_points = outline_points_dim[1]
  oDataObj = ovis->GetParameter('CONNECTIVITY')
  if (OBJ_VALID(oDataObj)) then begin
    success = oDataObj->GetData(outline_conn)
  endif else begin
    outline_conn = reform([lonarr(1,1)+num_outline_points+1, $
      lindgen(num_outline_points+1,1)], num_outline_points+2)
    outline_conn[outline_conn[0]] = 0
  endelse
  outline_conn_dim = size(outline_conn, /dimensions)
  
  ; get the tesselated polygon points and the connectivity array
  ovis._opolygon->GetProperty, DATA=points, POLYGONS=conn, $
    COLOR=fill_color, ALPHA = fill_alpha, HIDE=hide
  oVis._oLine->GetProperty, THICK=line_thick, COLOR=line_color, ALPHA = alpha
  
  ; if hide = 1, no polygon points, hence use the points from the polygon outline
  if (hide eq 1) then begin
    points = outline_points
    conn = outline_conn
  endif
  
  points_dim = size(points, /dimensions)
  num_points = points_dim[1]
  conn_dim = size(conn, /dimensions)
  
  isZ=0
  if(points_dim[0] eq 3) then begin
    for i=0, points_dim[1]-1 do isZ = (isZ or (points[2,i] ne 0))
  endif
  
  ; if it is not 3 D polygon, the points returned from
  ; GetProperty is not in degree coordinate
  ; and hence need to be transform into degreens
  ; coordinate value via map_proj_inverse
  if(~isZ) then begin
    points_inverse = map_proj_inverse(points, map_structure=sMap)
    points = temporary(points_inverse)
  endif
  
  ;specifiy line thick and color
  if isa(fill_color) then begin
    polystyle_id = 'Polystyle' + strtrim(string(oVis.name), 2) + $
      strtrim(string(l_id),2)
    printf, lun, '<Style id="'+polystyle_id+'">'
    
    
    if(isa(line_thick) ||isa(line_color)) then begin
      printf, lun, '<LineStyle>'
      ; color is agbr
      if(isa(line_color)) then begin
        printf, lun, '<color>'+strtrim(string(alpha*255, format='(z2.2)'),2)+ $ ; alpha
          strtrim(string(line_color[2], format='(z2.2)'),2)+ $ ; blue
          strtrim(string(line_color[1], format='(z2.2)'),2)+ $ ; green
          strtrim(string(line_color[0], format='(z2.2)'),2)+ $ ; red
          '</color>'
      endif
      if(isa(line_thick))then $
        printf, lun, '<width>'+strtrim(string(line_thick),2)+'</width>'
      printf, lun, '</LineStyle>'
    endif
    
    printf, lun, '<PolyStyle>'
    ; color is agbr
    printf, lun, '<color>'+strtrim(string(fill_alpha*255, format='(z2.2)'),2)+ $ ; alpha
      strtrim(string(fill_color[2], format='(z2.2)'),2)+ $ ; blue
      strtrim(string(fill_color[1], format='(z2.2)'),2)+ $ ; green
      strtrim(string(fill_color[0], format='(z2.2)'),2)+ $ ; red
      '</color>'
      
    printf, lun, oVis._fillbackground ? '<fill>1</fill>' : '<fill>0</fill>'
    printf, lun, '<outline>0</outline>'
    printf, lun, '</PolyStyle>'
    printf, lun, '</Style>'
    l_id = l_id+1
  endif
  
  printf, lun, '<Placemark>'
  printf, lun, '<name>'+ strtrim(string(oVis.name), 2)+'</name>'
  printf, lun, '<description>'+strtrim(string(oVis.description))+'</description>'
  
  printf, lun, '<styleUrl>#'+polystyle_id+'</styleUrl>'
  printf, lun, '<MultiGeometry>'
  
  ; draw the tesselated polygons.
  done = 0
  npoints=0
  while (done eq 0) do begin
    polypoints =conn[npoints]
    
    ; check if the polygon is clockwise or counterclockwise.
    ppoints = make_array(isZ ? 3 : 2, polypoints)
    for i=0, polypoints-1 do $
      ppoints[*,i] = points[*,conn[npoints+i+1]]
    isclockwise = idlkml_isPolygonClockWise(ppoints, polypoints)
    if (isclockwise) then begin
      start_index = polypoints-1
      end_index = 0
      step  = -1
    endif else begin
      start_index = 0
      end_index = polypoints-1
      step = 1
    endelse
    
    printf, lun, '<Polygon>'
    printf, lun, isZ ? '<altitudeMode>absolute</altitudeMode>' : $
      '<tessellate>1</tessellate>'
    printf, lun, '<outerBoundaryIs>'
    printf, lun, '<LinearRing>'
    printf, lun, '<coordinates>'
    for i=start_index, end_index, step do begin
      pm_lon = strtrim(string(points[0,conn[npoints+i+1]], format='(F16.7)'), 2)
      pm_lat = strtrim(string(points[1,conn[npoints+i+1]], format='(F16.7)'), 2)
      pm_alt = isZ ? $
        strtrim(string(points[2,conn[npoints+i+1]], format='(F16.7)'), 2) : $
        strtrim(string(0),2)
      printf, lun, pm_lon+','+pm_lat+','+pm_alt
    endfor
    ; if for some reason, the connectivity array does not close, close it.
    if (conn[npoints+1] ne conn[npoints+polypoints]) then begin
      pm_lon = strtrim(string(points[0,conn[npoints+start_index+1]], format='(F16.7)'), 2)
      pm_lat = strtrim(string(points[1,conn[npoints+start_index+1]], format='(F16.7)'), 2)
      pm_alt = isZ ? $
        strtrim(string(points[2,conn[npoints+start_index+1]], format='(F16.7)'), 2) : $
        strtrim(string(0),2)
      printf, lun, pm_lon+','+pm_lat+','+pm_alt
    endif
    npoints = npoints + polypoints + 1
    if(npoints eq conn_dim) then done = 1
    printf, lun, '</coordinates>'
    printf, lun, '</LinearRing>'
    printf, lun, '</outerBoundaryIs>'
    printf, lun, '</Polygon>'
    
  endwhile
  
  ; draw the outline of polygons
  done = 0
  npoints=0
  while (done eq 0) do begin
    polypoints =outline_conn[npoints]
    printf, lun, '<LineString>'
    if(isZ) then begin
      printf, lun, '<altitudeMode>absolute</altitudeMode>'
      n_points=2
    endif else begin
      printf, lun, '<tessellate>1</tessellate>'
      ; polyline folows the line of great circle if it is clamp to the ground, hence interpolate point to get something similar
      n_points=20
    endelse
    printf, lun, '<coordinates>'
    pIndgen = findgen(n_points)/(n_points-1)
    for i=0, polypoints-2 do begin
      conn2 = outline_conn[npoints+i+2]
      conn1 = outline_conn[npoints+i+1]
      pm_lons = pIndgen * (outline_points[0,conn2]-outline_points[0,conn1]) + outline_points[0,conn1]
      pm_lats = pIndgen * (outline_points[1,conn2]-outline_points[1,conn1]) + outline_points[1,conn1]
      pm_alts = isZ ? pIndgen * (outline_points[2,conn2]-outline_points[2,conn1]) + outline_points[2,conn1] : $
        fltarr(n_points)
      for j=0, n_points-1 do begin
        printf, lun, strtrim(string(pm_lons[j], format='(F16.7)'), 2) + $
          ',' + strtrim(string(pm_lats[j], format='(F16.7)'), 2) + $
          ',' + strtrim(string(pm_alts[j], format='(F16.7)'), 2)
      endfor
    endfor
    
    npoints = npoints + polypoints + 1
    if (npoints eq outline_conn_dim) then done = 1
    printf, lun, '</coordinates>'
    printf, lun, '</LineString>'
  endwhile
  
  printf, lun, '</MultiGeometry>'
  printf, lun, '</Placemark>'
  
  FREE_LUN, lun
  on_ioerror, null
  return, 1
  
  ioFailed:
  self->ErrorMessage, $
    [IDLitLangCatQuery('Error:Framework:FileWriteError'), !ERROR_STATE.msg], $
    title=IDLitLangCatQuery('Error:Error:Title'), severity=2
  return, 0 ; failure
end

;---------------------------------------------------------------------------
; Purpose:
;   Procedure for writing data out to the file.
;
; Arguments:
;   ImageData: An object reference to the data to be written.
;
; Keywords:
;   None.
;
function IDLitWriteKML::SetData, oWindow

  compile_opt idl2, hidden
  
  strFilename = self->GetFilename()
  if (strFilename eq '') then $
    return, 0 ; failure
    
  ; Get all the data space
  fullID = iGetCurrent()
  if (fullID[0] eq '') then return, 0
  oView = oWindow->GetCurrentView()
  if (~OBJ_VALID(oView)) then return, -1
  oSelect = oView->GetSelecteditems(/ALL)
  olayer =  oView->GetCurrentLayer()
  oWorld =  oLayer->GetWorld()
  oNormSaveitem = oWorld->GetDataSpaces()
  oData = objarr(n_elements(oNormSaveitem), 1)
  for i=0, n_elements(oNormSaveItem)-1 do $
    oData[i] = oNormSaveItem[i]->GetDataSpace(/UNNORMALIZED)
  
  
  sep = PATH_SEP()
  isKMZ = strcmp(strmid(file_basename(strFilename), $
    strlen(file_basename(strFilename))-3, 3), 'kmz', /FOLD_CASE)
  ; if it is .kmz, still write .kml file
  if (isKMZ) then strFilename = file_dirname(strFilename) + sep + $
    file_basename(strFilename, 'kmz')+'kml'
  
  foreach oMapData, oData do $
    if (~OBJ_VALID(oMapData)) then goto, failed
    
  on_ioerror, ioFailed
  OPENW, lun, strFilename, /GET_LUN
  printf, lun, '<?xml version="1.0" encoding="UTF-8"?>'
  printf, lun, '<kml xmlns="http://earth.google.com/kml/2.2">'
  printf, lun, '<Document>
  
  if (isKMZ) then begin
    printf, lun, '<name>'+file_basename(strFilename, 'kml')+'kmz</name>'
  endif else begin
    printf, lun, '<name>'+file_basename(strFilename)+'</name>'
  endelse
    
  l_id = 0l
  file_names = !null
  ; Multiple maps in a window
  foreach oMapData, oData  do begin
    otool = oMapData->GetTool()
    oProj =  oMapData->_GetMapProjection()
    sMap = oProj->_GetMapStructure()
    if (isa(sMap, /array)) then begin
    
      oTitle = oTool->GetByIdentifier(iGetID('title', $
        DATASPACE=omapData, TOOL=oTool->GetFullIdentifier()))
      name = ISA(oTitle) ? oTitle._string : "Region"

      res=omapdata->_GetXYZAxisRange(xrange, yrange, zrange)
      lat_lon=map_proj_inverse(xrange, yrange, map_structure=sMap)
      
      file_north = max([lat_lon[1,0], lat_lon[1,1]], min=file_south)
      file_east = max([lat_lon[0,0], lat_lon[0,1]], min=file_west)
      file_north_string = strtrim(string(file_north, format='(F16.7)'),2)
      file_south_string = strtrim(string(file_south, format='(F16.7)'),2)
      file_east_string = strtrim(string(file_east, format='(F16.7)'),2)
      file_west_string = strtrim(string(file_west, format='(F16.7)'),2)

      oDataParent = oMapData._parent
      image_area = oDataparent._screenxsize * oDataParent._screenysize
      half_sqrt_image_area = round(sqrt(image_area)/2, /l64)
      string_half_sqrt_image_area = strtrim(string(half_sqrt_image_area),2)
      
      printf, lun, '<NetworkLink>'
      printf, lun, '<name>' + name + '</name>'
      printf, lun, '<Region>'
      printf, lun, '<LatLonAltBox>'
      printf, lun, '<north>'+file_north_string+'</north>'
      printf, lun, '<south>'+file_south_string+'</south>'
      printf, lun, '<east>'+file_east_string+'</east>'
      printf, lun, '<west>'+file_west_string+'</west>'
      printf, lun, '</LatLonAltBox>'
      printf, lun, '<Lod>'
      printf, lun, '<minLodPixels>'+string_half_sqrt_image_area+'</minLodPixels>'
      printf, lun, '<maxLodPixels>-1</maxLodPixels>'
      printf, lun, '</Lod>'
      printf, lun, '</Region>'
      printf, lun, '</NetworkLink>'
    endif
    FREE_LUN, lun
    on_ioerror, null
    
    ; attach other visulaizations, ie: polyline, arrow, etc
    oAllVis = oMapData->GetVisualizations()
    if (~isa(oAllVis, /array)) && (~obj_valid(oAllVis)) then goto, failed
    
    foreach oVis,oAllVis do begin
      ; if image change projection
      if(isa(oVis, 'IDLITVISIMAGE')) then begin
        res = self->SetImageOverlay(oVis, l_id, FILE_NAME=file_name)
        file_names = [file_names, file_name]
        if (~res) then goto, failed
      endif
      
      ; if polyline, draw the polyline
      if (ISA(oVis, 'IDLITVISPOLYLINE') || ISA(oVis, 'IDLitVisPlot')) then begin
        res = self->SetLine(oVis, l_id)
        if (~res) then goto, failed
      endif
      
      ; if polygon, add polygon
      if(isa(oVis, 'IDLITVISPOLYGON') && $
        ~strcmp(oVis.name, 'Map Background', /FOLD_CASE)) then begin
        ; Do not include the continents or other map shapefiles.
        if (ISA(oVis._proxy, 'MAPCONTINENTS')) then continue
        res = self->SetPolygon(oVis, l_id, sMap)
        if (~res) then goto, failed
      endif
      
      ; if contour, add contour lines
      if(isa(oVis, 'IDLITVISCONTOUR')) then begin
        res = self->SetContour(oVis, l_id)
        if (~res) then goto, failed
      endif
      
    endforeach
    
    on_ioerror, ioFailed
    OPENW, lun, strFilename, /GET_LUN, /APPEND
  endforeach
  printf, lun, '</Document>
  printf, lun, '</kml>'
  FREE_LUN, lun
  on_ioerror, null
  
  if (isKMZ) then begin
    kmz_outfile = file_dirname(strFilename) + sep + $
      file_basename(strFilename, 'kml')+'kmz'
    FILE_ZIP, [strFilename, file_names], kmz_outfile
    FILE_DELETE, [strFilename, file_names]
  endif
  
  
  void = CHECK_MATH()  ; swallow underflow errors
  return, 1  ; success
  
  failed:
  self->ErrorMessage, $
    [IDLitLangCatQuery('Error:Framework:InvalidWriteData')], $
    title=IDLitLangCatQuery('Error:Error:Title'), severity=2
  return, 0 ; failure
  
  ioFailed:
  self->ErrorMessage, $
    [IDLitLangCatQuery('Error:Framework:FileWriteError'), !ERROR_STATE.msg], $
    title=IDLitLangCatQuery('Error:Error:Title'), severity=2
  return, 0 ; failure
  
end


;---------------------------------------------------------------------------
; Definition
;---------------------------------------------------------------------------
; Purpose:
;   Class definition.
;
pro IDLitWriteKML__Define

  compile_opt idl2, hidden
  
  void = {IDLitWriteKML, $
    inherits IDLitWriter $
    }
end
