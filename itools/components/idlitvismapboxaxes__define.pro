; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvismapboxaxes__define.pro#3 $
;
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLitvisMapBoxAxes
;
; PURPOSE:
;    The IDLitvisMapBoxAxes class implements
;
; CATEGORY:
;    Components
;
; SUPERCLASSES:
;   IDLitVisualization
;
;-
function IDLitvisMapBoxAxes::Init, oObj, _REF_EXTRA=_extra
  
  compile_opt idl2, hidden
  on_error, 2

  ; Init our superclass
  if (~self->IDLitVisualization::Init(TYPE="IDLBOXGRID", $
      ;ICON='fitwindow', $
      DESCRIPTION="A Box Grid Visualization", $
      NAME="BoxGrid", IMPACTS_RANGE=0, _EXTRA=_extra)) then $
      RETURN, 0
  
  if ISA(oObj, 'IDLitvisMapGrid') then self._oMapGrid = oObj
  
  ; Default thickness of the box grid
  self._GridBoxThickness = 4
  self._boxColorSet = 0
  
  self._oBorderMain  = OBJ_NEW('IDLgrPolyline', THICK=self._GridBoxThickness, $
    COLOR=[0,0,0], /HIDE)
  self.Add, self._oBorderMain
  
  ; Create an IDLgrPolyline object for the white boxes
  self._oWhiteBoxes = OBJ_NEW( 'IDLgrPolyline', THICK=self._GridBoxThickness - 2, $
    COLOR=[255,255,255], /HIDE )
  self.Add, self._oWhiteBoxes
  
  ; Set any properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitvisMapBoxAxes::SetProperty, _EXTRA=_extra
  
  return, 1

end

;----------------------------------------------------------------------------
pro IDLitVisMapBoxAxes::Cleanup
    compile_opt idl2, hidden
    
    ; Cleanup
    self._oBorderMain->Cleanup
    self._oWhiteBoxes->Cleanup
    
    ; Cleanup superclass
    self->IDLitVisualization::Cleanup
end

;----------------------------------------------------------------------------
pro IDLitVisMapBoxAxes::GetProperty, $
  BOX_COLOR=boxColor, $
  BOX_THICK=boxWidth, $
  BOX_ANTIALIAS=boxAntialias, $
  _REF_EXTRA=_extra

  compile_opt idl2, hidden

  if ARG_PRESENT(boxColor) then $
    self._oBorderMain.GetProperty, COLOR=boxColor

  if ARG_PRESENT(boxWidth) then begin
    case (self._GridBoxThickness) of
      4: begin
        boxWidth = 1
      end
      6: begin
        boxWidth = 2
      end
      8: begin
        boxWidth = 3
      end
      10: begin
        boxWidth = 4
      end
    endcase
  endif

  ; The two lines should always have the same antialias setting
  if ARG_PRESENT(boxAntialias) then $
    self._oBorderMain.GetProperty, ANTIALIAS=boxAntialias

  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisualization::GetProperty, _EXTRA=_extra

end

;----------------------------------------------------------------------------
pro IDLitVisMapBoxAxes::SetProperty, $
    COLOR=gridColor, $
    BOX_COLOR=boxColor, $
    BOX_THICK=boxThick, $
    BOX_ANTIALIAS=boxAntialias, $
    _REF_EXTRA=_extra

  compile_opt idl2, hidden

  ; Keep the box axes color in synch with the grid line color
  ; until the box_color is specifically set
  if (N_ELEMENTS(gridColor) gt 0 && $
      N_ELEMENTS(boxColor) eq 0 && $
      self._boxColorSet eq 0) then begin
    if (ISA(gridColor, 'STRING') || N_ELEMENTS(gridColor) eq 1) then $
      style_convert, gridColor[0], COLOR=gridColor
    if ISA(self._oBorderMain) then $
      self._oBorderMain.SetProperty, COLOR=gridColor
  endif

  if (N_ELEMENTS(boxColor) gt 0) then begin        
    if (ISA(boxColor, 'STRING') || N_ELEMENTS(boxColor) eq 1) then $
      style_convert, boxColor[0], COLOR=boxColor    
    if ISA(self._oBorderMain) then begin
      self._boxColorSet = 1
      self._oBorderMain.SetProperty, COLOR=boxColor
    endif
  endif

  if (N_ELEMENTS(boxAntialias) gt 0) then begin
      self._oBorderMain.SetProperty, ANTIALIAS=boxAntialias
      self._oWhiteBoxes.SetProperty, ANTIALIAS=boxAntialias
  endif

  if (N_ELEMENTS(boxThick) gt 0) then begin
    newThick = self._GridBoxThickness
    case (boxThick) of
    1: begin
      newThick = 4
    end
    2: begin
      newThick = 6
    end
    3: begin
      newThick = 8
    end
    4: begin
      newThick = 10
    end
    else: begin
      ; Clip the values to the range of values 1, 2, or 3
      if ( boxThick lt 1 ) then newThick = 4
      if ( boxThick gt 4 ) then newThick = 10
    end
    endcase
    if ( newThick ne self._GridBoxThickness ) then begin
      self._GridBoxThickness = newThick
      self._oBorderMain.SetProperty, THICK=newThick
      self._oWhiteBoxes.SetProperty, THICK=newThick - 2
    endif  
  endif

  ; Set any properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisualization::SetProperty, _EXTRA=_extra

end

;----------------------------------------------------------------------------
; Calculate the positions of the box axes relative to the visualization
;
pro IDLitvisMapBoxAxes::_UpdateBoxGrid

  compile_opt idl2, hidden

  ; If there is no data space, then return with no action taken
  oDataspace = self->GetDataspace( )
  if ~ISA(oDataSpace) then return

  if ~ISA(self._oMapGrid, 'IDLitvisMapGrid') then return

  ; Get the property values we need from the map grid
  self._oMapGrid.GetProperty, $
    LONGITUDE_MIN=gridLonMin, LONGITUDE_MAX = gridLonMax, $
      LATITUDE_MIN=gridLatMin, LATITUDE_MAX=gridLatMax, $
      GRID_LATITUDE=gridLatitude, GRID_LONGITUDE=gridLongitude
  sMap = self._oMapGrid.GetProjection( )
  hasMap = N_TAGS(sMap) gt 0
  
  ; Get the limits from the sMap structure because the
  ; grid stores the limits as adjusted values
  if (hasMap) then begin
    latMin = sMap.LL_BOX[0]
    lonMin = sMap.LL_BOX[1]
    latMax = sMap.LL_BOX[2]
    lonMax = sMap.LL_BOX[3]
  endif else begin
    latMin = gridLatMin
    lonMin = gridLonMin
    latMax = gridLatMax
    lonMax = gridLonMax
  endelse
  
  dLon = ABS( lonMax - lonMin )
  dLat = ABS( latMax - latMin )
  
  ; Bring in the lon endpoints just enough to
  ; prevent situations where the left and right side of
  ; the box axes are projected to the same location
  EPS = dLon * 1d-9
  lonMin += EPS
  lonMax -= EPS

  dLon = ABS( lonMax - lonMin )
  dLat = ABS( latMax - latMin )
  
  ; Get the locations of the longitude lines
  ; note that some of the stored lines may be hidden
  lonLineLocs = []
  oMapGridContainer = self._oMapGrid.Get( POSITION=0 )
  oLines = oMapGridContainer.Get( /ALL, COUNT=nline )
  for i = 0, nline - 1 do begin
    oLines[i].GetProperty, LOCATION=location, HIDE=hidden
    if ~hidden then lonLineLocs = [lonLineLocs, location]
  endfor
  
  ; Get the locations of the latitude lines
  ; note that some of the stored lines may be hidden
  latLineLocs = []
  oMapGridContainer = self._oMapGrid.Get( POSITION=1 )
  oLines = oMapGridContainer.Get( /ALL, COUNT=nline )
  for i = 0, nline - 1 do begin
    oLines[i].GetProperty, LOCATION=location, HIDE=hidden
    if ~hidden then latLineLocs = [latLineLocs, location]
  endfor
  
  if ~ISA(lonLineLocs) || ~ISA(latLineLocs) then begin
    self.SetProperty, /HIDE ; map is invalid so hide the box axes
    RETURN
  endif

  ; Make sure that the max and min are included list of longitudes
  lMin = MIN( lonLineLocs, MAX=lMax )
  if (ABS(lMin-lonMin) gt 10*EPS) then $
    lonLineLocs = [lonMin, lonLineLocs]
  if (ABS(lMax-lonMax) gt 10*EPS) then $
    lonLineLocs = [lonLineLocs,lonMax]
  nLon = N_ELEMENTS(lonLineLocs)

  ; Make sure that the max and min are included in the list of latitudes
  lMin = MIN( latLineLocs, MAX=lMax )
  if (lMin ne latMin) then latLineLocs = [latMin, latLineLocs]
  if (lMax ne latMax) then latLineLocs = [latLineLocs,latMax]
  nLat = N_ELEMENTS(latLineLocs)
  
  ; Create the border line
  ptsPerLine = 500L ; number of points per line to use for interpolation
  pts = dindgen(ptsPerLine)/(ptsPerLine-1)
  ptsPerLine += 2*(nLon > nLat)

  ; Add in values on either side of each longitude line, so that we end
  ; up with the black/white borders at the exact gridline.
  pLon = [pts * dLon + lonMin, $
    (lonLineLocs - EPS) > lonMin, (lonLineLocs + EPS) < lonMax]
  if (nLon lt nLat) then pLon = [pLon, REPLICATE(lonMin, 2*(nLat - nLon))]
  pLon = TRANSPOSE(pLon[SORT(pLon)])

  ; Add in values on either side of each latitude line, so that we end
  ; up with the black/white borders at the exact gridline.
  pLat = [pts * dLat + latMin, $
    (latLineLocs - EPS) > latMin, (latLineLocs + EPS) < latMax]
  if (nLat lt nLon) then pLat = [pLat, REPLICATE(latMin, 2*(nLon - nLat))]
  pLat = TRANSPOSE(pLat[SORT(pLat)])

  ; Be sure to go in the opposite direction for the top and left sides,
  ; so that the end caps of the lines overlap correctly.
  ; Test code:
  ; m = Map('Equirectangular', /BOX_AXES, BOX_THICK=4, LIMIT=[0,0,10,10])
  ;
  pzero = dblarr(1, ptsPerLine)
  bottomData = [pLon, pzero + latMin]
  rightData = [pzero + lonMax, pLat]
  topData = [REVERSE(pLon, 2), pzero + latMax]
  leftData = [pzero + lonMin, REVERSE(pLat, 2)]

  pts = lindgen(ptsPerLine)
  pBottom = [ptsPerLine, pts, 0]
  pRight = [ptsPerLine, pts + ptsPerLine, 0]
  pTop = [ptsPerLine, pts + 2*ptsPerLine, 0]
  pLeft = [ptsPerLine, pts + 3*ptsPerLine, 0]
  
  if (hasMap) then begin
    bd = MAP_PROJ_FORWARD( bottomData, MAP_STRUCTURE=sMap )
    td = MAP_PROJ_FORWARD( topData, MAP_STRUCTURE=sMap )
    rd = MAP_PROJ_FORWARD( rightData, MAP_STRUCTURE=sMap )
    ld = MAP_PROJ_FORWARD( leftData, MAP_STRUCTURE=sMap )
    ; Go around the map in counterclockwise order (shouldn't matter)
    boxData = [[bd],[rd],[td],[ld]]

    delta1 = MAX(ABS(bd[0,*] - bd[0,0]))
    delta2 = MAX(ABS(bd[1,*] - bd[1,0]))
    EPS = 0.5
    noBottom = delta1 lt EPS && delta2 lt EPS
    delta1 = MAX(ABS(td[0,*] - td[0,0]))
    delta2 = MAX(ABS(td[1,*] - td[1,0]))
    noTop = delta1 lt EPS && delta2 lt EPS
    delta1 = MAX(ABS(rd[0,*] - rd[0,0]))
    delta2 = MAX(ABS(rd[1,*] - rd[1,0]))
    noRight = delta1 lt EPS && delta2 lt EPS
    delta1 = MAX(ABS(ld[0,*] - ld[0,0]))
    delta2 = MAX(ABS(ld[1,*] - ld[1,0]))
    noLeft = delta1 lt EPS && delta2 lt EPS
    leftRightOverlap = MAX(ABS(ld - REVERSE(rd,2))) lt EPS

    if (noBottom) then begin
      pBottom[*] = 0
    endif else begin
      ; If first & last point are close, connect them.
      if (MAX(ABS(bd[*,0] - bd[*,-1])) lt EPS) then begin
        pBottom[0]++
        pBottom[-1] = pBottom[1]
      endif
    endelse

    if (noTop) then begin
      pTop[*] = 0
    endif else begin
      ; If first & last point are close, connect them.
      if (MAX(ABS(td[*,0] - td[*,-1])) lt EPS) then begin
        pTop[0]++
        pTop[-1] = pTop[1]
      endif
    endelse

    if (noRight || leftRightOverlap) then begin
      pRight[*] = 0
    endif

    if (noLeft || leftRightOverlap) then begin
      pLeft[*] = 0
    endif

  endif else begin
    ; Go around the map in counterclockwise order (shouldn't matter)
    boxData = [[bottomData],[rightData],[topData],[leftData]]    
  endelse
  
  ; If we have any non-finite values, then do not show the axes
  if (MIN(FINITE(boxData)) eq 0) then begin
    self.SetProperty, /HIDE
    RETURN
  endif
  
  ; Go around the map in counterclockwise order (shouldn't matter)
  polylines = [pBottom, pRight, pTop, pLeft]

  nBox = N_ELEMENTS(boxData)/2
  if ISA(self._oBorderMain) then begin
    self._oBorderMain.SetProperty, DATA=boxData, HIDE=0, POLYLINES=polylines
  endif

  ; Now calculate the connectivity array into the boxData for the white boxes
  if ISA( self._oWhiteBoxes ) then begin

    self._oBorderMain.GetProperty, COLOR=boxColor
    vertColors = REBIN(boxColor, 3, nBox)

    ; Calculate the white box axes for the longitude
    sortIndices = SORT( lonLineLocs )
    for i=1, N_ELEMENTS( sortIndices ) - 2, 2 do begin
      ; Compute indices separately for bottom & top since the top is reversed.
      indices = WHERE( bottomData[0,*] ge lonLineLocs[sortIndices[i]] and $
        bottomData[0,*] LE lonLineLocs[sortIndices[i + 1]], ni )
      if (ni gt 2) then begin
        vertColors[*, indices] = 255b
      endif
      indices = WHERE( topData[0,*] ge lonLineLocs[sortIndices[i]] and $
        topData[0,*] LE lonLineLocs[sortIndices[i + 1]], ni )
      if (ni gt 2) then begin
        vertColors[*, indices + 2*ptsPerLine] = 255b
      endif
    endfor
    
    ; Calculate the white box axes for the latitude
    sortIndices = SORT( latLineLocs )
    for i=1, N_ELEMENTS( sortIndices ) - 2, 2 do begin
      ; Compute indices separately for left & right since the left is reversed.
      indices = WHERE( rightData[1,*] ge latLineLocs[sortIndices[i]] and $
        rightData[1,*] LE latLineLocs[sortIndices[i + 1]], ni )
      if (ni gt 2) then begin
        vertColors[*, indices + ptsPerLine] = 255b
      endif
      indices = WHERE( leftData[1,*] ge latLineLocs[sortIndices[i]] and $
        leftData[1,*] LE latLineLocs[sortIndices[i + 1]], ni )
      if (ni gt 2) then begin
        vertColors[*, indices + 3*ptsPerLine] = 255b
      endif
    endfor

    ; Set the data and connectivity for the white part of the box axes
    self._oWhiteBoxes.SetProperty, DATA=boxData, HIDE=0, $
      POLYLINES=polylines, VERT_COLORS=vertColors
  endif

end


;----------------------------------------------------------------------------
function IDLitVisMapBoxAxes::GetXYZRange, xr, yr, zr, _REF_EXTRA=extra
  compile_opt idl2, hidden

  success = self->_IDLitVisualization::GetXYZRange(xr, yr, zr, _EXTRA=extra)

  if (success) then begin
    ; In the _UpdateBoxGrid we add points on either side of the grid lines
    ; that are +/-EPS. This has a side effect of increasing the X/Y ranges,
    ; which makes it seem like the Box Axes are extending beyond the dataspace.
    ; This then causes clipping to be turned on, which clips the box axes
    ; and the labels. To prevent this, shrink the ranges just a bit.
    EPS = ABS(xr[1]-xr[0]) * 1d-5
    xr[0] = (xr[0] lt xr[1]) ? xr[0]+EPS : xr[0]-EPS
    xr[1] = (xr[0] lt xr[1]) ? xr[1]-EPS : xr[1]+EPS
    EPS = ABS(yr[1]-yr[0]) * 1d-5
    yr[0] = (yr[0] lt yr[1]) ? yr[0]+EPS : yr[0]-EPS
    yr[1] = (yr[0] lt yr[1]) ? yr[1]-EPS : yr[1]+EPS
  endif

  return, success
end


;----------------------------------------------------------------------------
;+
; IDLitvisMapBoxAxes__define
;
; PURPOSE:
;    Defines the object structure for an IDLitvisMapBoxAxes object.
;
;-
pro IDLitvisMapBoxAxes__define
  compile_opt idl2, hidden
  void = {IDLitvisMapBoxAxes, $
          inherits IDLitVisualization, $
          _oBorderMain:OBJ_NEW( ), $
          _oWhiteBoxes:OBJ_NEW( ), $
          _oMapGrid:OBJ_NEW( ), $
          _boxColorSet:0B, $
          _GridBoxThickness:0B}
end
