; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitmanipvisscale2d__define.pro#1 $
;
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
;
; Purpose:
;   The IDLitManipVisScale2D class is the 2d scale manipulator visual.
;


;----------------------------------------------------------------------------
; Purpose:
;   This function method initializes the object.
;
; Syntax:
;   Obj = OBJ_NEW('IDLitManipVisScale2D')
;
;   or
;
;   Obj->[IDLitManipVisScale2D::]Init
;
; Result:
;   1 for success, 0 for failure.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
function IDLitManipVisScale2D::Init, $
    COLOR=color, $
    NAME=inName, $
    TOOL=oTool, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Prepare default name.
    name = (N_ELEMENTS(inName) ne 0) ? inName : "Scale2D Visual"

    ; Initialize superclasses.
    if (self->IDLitManipulatorVisual::Init( $
        NAME=name, $
        VISUAL_TYPE='Select', $
        _EXTRA=_extra) ne 1) then $
        return, 0


    self._oFont = OBJ_NEW('IDLgrFont', 'Symbol', SIZE=20)
    self._oSmallFont = OBJ_NEW('IDLgrFont', 'Symbol', SIZE=8d)

    ; Edges
    oLine = OBJ_NEW('IDLitManipulatorVisual', VISUAL_TYPE='Translate')
    oLine->Add, OBJ_NEW('IDLgrPolyline', $
                        COLOR=!color.dodger_blue, $
                        DATA=TRANSPOSE([[-1,-1,1,1,-1],[-1,1,1,-1,-1]]), $
                        ALPHA_CHANNEL=0)
    self->Add, oLine


    ; Corners handles
    types = ['-X-Y','+X-Y','+X+Y','-X+Y']
    data = [[-1,-1], $
            [1,-1], $
            [1,1], $
            [-1,1]]
    for i=0,3 do begin
        xyposition = [data[0:1,i], 0]
        oCorner = OBJ_NEW('IDLitManipulatorVisual', $
            VISUAL_TYPE='Scale/'+types[i])
        oText = OBJ_NEW('IDLgrText', '!M'+String(183b), $  ; bullet character
            FONT=self._oFont, $
            /ENABLE_FORMATTING, $
            ALIGN=0.48, $
            RECOMPUTE_DIM=2, $
            RENDER=0, $
            LOCATION=xyposition, $
            VERTICAL_ALIGNMENT=0.69, $
            ALPHA_CHANNEL=1, $
            COLOR=!color.DODGER_BLUE)
        oCorner->Add, oText
        self->Add, oCorner
    endfor

    ; Rotate handle
    if (ISA(oTool, 'GraphicsTool')) then begin
      oRotate = OBJ_NEW('IDLitManipulatorVisual', $
         VISUAL_TYPE='Rotate')
      oRotate->Add, OBJ_NEW('IDLgrText', '!M'+string(124b), $  ; connector line
         FONT=self._oSmallFont, $
         /ENABLE_FORMATTING, $
         LOCATION=[0,1], $
         VERTICAL_ALIGNMENT=-0.5, $
         COLOR=!color.DODGER_BLUE, $
         ALPHA_CHANNEL=0.4, $
         ALIGN=0.55, $
         RECOMPUTE_DIM=2, $
         RENDER=0)
      oRotate->Add, OBJ_NEW('IDLgrText', '!M'+string(183b), $  ; solid circle
         FONT=self._oFont, $
         /ENABLE_FORMATTING, $
         LOCATION=[0,1], $
         VERTICAL_ALIGNMENT=-0.36, $
         COLOR=!color.DODGER_BLUE, $
         ALPHA_CHANNEL=1, $
         ALIGN=0.52, $
         RECOMPUTE_DIM=2, $
         RENDER=0)
      self->Add, oRotate
    endif

    ; Edges handles
    types = ['-X','+X','-Y','+Y']
    for i=0,3 do begin
        oEdge = OBJ_NEW('IDLitManipulatorVisual', $
            VISUAL_TYPE='Scale/' + types[i])

        case i of
            0: data = [[-1,-1],[-1,1]] ; left
            1: data = [[ 1,-1],[1, 1]] ; right
            2: data = [[-1,-1],[1,-1]] ; bottom
            3: data = [[-1, 1],[1, 1]] ; top
        endcase

        isX = STRPOS(types[i], 'X') ne -1
        
        oEdge->Add, OBJ_NEW('IDLgrText', '!M'+string(168b), $  ; solid diamond
            FONT=self._oSmallFont, $
            /ENABLE_FORMATTING, $
            LOCATION=TOTAL(data,2)/2, $
            VERTICAL_ALIGNMENT=0.45, $
            BASELINE=(isX ? [0,1,0] : [1,0,0]), $
            UPDIR=(isX ? [1,0,0] : [0,1,0]), $
            ALPHA_CHANNEL=1, $
            COLOR=!color.DODGER_BLUE, $
            ALIGN=0.53, $
            RECOMPUTE_DIM=2, $
            RENDER=0)

        self->Add, oEdge
    endfor
    
    

    return, 1
end


;----------------------------------------------------------------------------
; Purpose:
;   This function method cleans up the object.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
pro IDLitManipVisScale2D::Cleanup

    compile_opt idl2, hidden

    OBJ_DESTROY, self._oFont
    OBJ_DESTROY, self._oSmallFont
    self->IDLitManipulatorVisual::Cleanup
end


;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; IDLitManipVisScale2D__Define
;
; Purpose:
;   Defines the object structure for an IDLitManipVisScale2D object.
;-
pro IDLitManipVisScale2D__Define

    compile_opt idl2, hidden

    struct = { IDLitManipVisScale2D, $
        inherits IDLitManipulatorVisual, $
        _oFont: OBJ_NEW(), $
        _oSmallFont: OBJ_NEW() $
        }
end
