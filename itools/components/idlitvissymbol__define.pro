; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvissymbol__define.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create a symbol
;
; :Params:
;    X : X coordinate
;    Y : Y coordinate
;
; :Keywords:
;    
;
;-
;-------------------------------------------------------------------------
function IDLitVisSymbol::Init, _EXTRA=_extra

  compile_opt idl2, hidden
  @graphic_error
 
  ; Initialize superclass
  if (~self->IDLitVisualization::Init( $
    NAME="Symbol", $
    TYPE="Symbol", $
    IMPACTS_RANGE=0, $
    ICON='Symbol', $
    /MANIPULATOR_TARGET, $
    DESCRIPTION="Symbol",$
    _EXTRA=_extra))then $
    return, 0

  self._oPolyline = OBJ_NEW('IDLgrPolyline', /REGISTER_PROPERTIES, LINESTYLE=6)
  self._oSymbol = OBJ_NEW('IDLitSymbol', PARENT=self._oPolyline)
  self._oPolyline->SetProperty, SYMBOL=self._oSymbol->GetSymbol()


  self._oLabel = OBJ_NEW( 'IDLgrText', /REGISTER_PROPERTIES, $
    /ENABLE_FORMATTING)
  self._oFont = OBJ_NEW('IDLitFont')
  self._oLabel->SetProperty, FONT=self._oFont->GetFont()


  ; The text symbol should be centered on the symbol location
  self._oTextSymbol = OBJ_NEW( 'IDLgrText', /HIDE, $
    ALIGNMENT=0.5, VERTICAL_ALIGNMENT=0.5, /ENABLE_FORMATTING)
  self._oTextSymFont = OBJ_NEW('IDLitFont')
  self._oTextSymbol->SetProperty, FONT=self._oTextSymFont->GetFont()
  
  oTool = self->GetTool()

  ; Add in our special manipulator visual.  This allows translation
  ; but doesn't allow scaling.  We don't want to allow scaling because
  ; it causes problems with the font sizing.
  oSelectBox = OBJ_NEW('IDLitManipVisSelect', /HIDE)
  oSelectBox->Add, OBJ_NEW('IDLgrPolyline', COLOR=!COLOR.DODGER_BLUE, $
      DATA=[[-1,-1],[1,-1],[1,1],[-1,1],[-1,-1]], ALPHA_CHANNEL=0.4)

  ; Rotate handle
  if (ISA(oTool, 'GraphicsTool')) then begin
    self._oLargeFont = OBJ_NEW('IDLgrFont', 'Symbol', SIZE=20)
    self._oSmallFont = OBJ_NEW('IDLgrFont', 'Symbol', SIZE=8)

    oRotate = OBJ_NEW('IDLitManipulatorVisual', $
       VISUAL_TYPE='Rotate')
    oRotate->Add, OBJ_NEW('IDLgrText', string(124b), $  ; connector line
      FONT=self._oSmallFont, $
      LOCATION=[0,1], $
      ALIGNMENT=0.55, $
      RECOMPUTE_DIM=2, RENDER=0, $
      VERTICAL_ALIGNMENT=-0.15, $
      COLOR=!color.DODGER_BLUE, $
      ALPHA_CHANNEL=0.4)
    oRotate->Add, OBJ_NEW('IDLgrText', string(183b), $  ; solid circle
      FONT=self._oLargeFont, $
      LOCATION=[0,1], $
      VERTICAL_ALIGNMENT=-0.15, $
      COLOR=!color.DODGER_BLUE, $
      ALIGNMENT=0.52, $
      RECOMPUTE_DIM=2, RENDER=0, $
      ALPHA_CHANNEL=1)
    oSelectBox->Add, oRotate
  endif
  self->SetDefaultSelectionVisual, oSelectBox
  
  self->Add, self._oPolyline, /NO_UPDATE
  self->Add, self._oSymbol, /NO_UPDATE
  
  ; Translate the text to have it to show up in the desired location
  self._labelPosition = 1

  ; Register all properties.
  self->IDLitVisSymbol::_RegisterProperties

  ; Set properties on this object
  if (ISA(_extra)) then self->SetProperty, _EXTRA=_extra
  
  return, 1

end


;-------------------------------------------------------------------------
pro IDLitVisSymbol::Cleanup

  compile_opt idl2, hidden
  
  self.IDLitVisualization::Cleanup

  Obj_Destroy, self._oFont
  Obj_Destroy, self._oTextSymFont
  Obj_Destroy, self._oSmallFont
  Obj_Destroy, self._oLargeFont
  Ptr_Free, self._labelString, self._symText, self._data

end


;----------------------------------------------------------------------------
; IDLitVisPolyline::_RegisterProperties
;
; Purpose:
;   Internal routine that will register all properties supported by
;   this object.
;
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLitVisSymbol::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

  compile_opt idl2, hidden

  registerAll = ~KEYWORD_SET(updateFromVersion)

  if (registerAll) then begin

    self->RegisterProperty, 'SYMBOL', /SYMBOL, $
      NAME='Symbol', DESCRIPTION='Symbol', /ADVANCED_ONLY

    self->RegisterProperty, 'SYM_SIZE', /FLOAT, $
        NAME='Symbol size', $
        DESCRIPTION='Symbol size'

    ; Use an underscore so the property sheet doesn't get angry
    ; if we set SYM_COLOR to a 3xN array of colors.
    self->RegisterProperty, '_SYM_COLOR', /COLOR, $
        NAME='Symbol color', $
        DESCRIPTION='Symbol color'

    self->RegisterProperty, 'SYM_THICK', /FLOAT, $
        NAME='Symbol thickness', $
        DESCRIPTION='Symbol thickness', $
        VALID_RANGE=[1.0,10.0, .1d]

    self->RegisterProperty, 'SYM_FILLED', /BOOLEAN, $
        NAME='Symbol filled', $
        DESCRIPTION='Fill the symbol'

    self->RegisterProperty, 'SYM_FILL_COLOR', /COLOR, $
        NAME='Symbol fill color', $
        DESCRIPTION='Symbol fill color'

    self->RegisterProperty, 'SYM_TRANSPARENCY', /INTEGER, $
        NAME='Symbol transparency', $
        DESCRIPTION='Symbol transparency', $
        VALID_RANGE=[0,100,5]

    ; Undoc'd property, so the property sheet can get/set a single string.
    self->RegisterProperty, '_LABEL_STRING', /STRING, $
      NAME='Label string', $
      DESCRIPTION='Label string'

    ; Undoc'd property, so the property sheet can get/set the position by index.
    self->RegisterProperty, '_LABEL_POSITION', $
      ENUMLIST=['Center', 'Right', 'Bottom right', 'Bottom', 'Bottom left', $
        'Left', 'Top left', 'Top', 'Top right'], $
      NAME='Label position', $
      DESCRIPTION='Label position'

    self->RegisterProperty, 'LABEL_COLOR', /COLOR, $
        NAME='Label color', $
        DESCRIPTION='Label color'

    self->RegisterProperty, 'LABEL_FILL_BACKGROUND', /BOOLEAN, $
        NAME='Label fill', $
        DESCRIPTION='Label fill background'

    self->RegisterProperty, 'LABEL_FILL_COLOR', /COLOR, $
        NAME='Label fill color', $
        DESCRIPTION='Label fill color'

    self->RegisterProperty, 'LABEL_ORIENTATION', /FLOAT, $
        NAME='Label orientation', $
        DESCRIPTION='Label orientation'

    self->RegisterProperty, 'LABEL_TRANSPARENCY', /INTEGER, $
        NAME='Label transparency', $
        DESCRIPTION='Label transparency', $
        VALID_RANGE=[0,100,5]

    self._oFont->GetPropertyAttribute, 'FONT_INDEX', ENUMLIST=fonts

    self->RegisterProperty, 'LABEL_FONT_INDEX', ENUMLIST=fonts, $
        NAME='Label font', $
        DESCRIPTION='Label name'

    self->RegisterProperty, 'LABEL_FONT_STYLE', $
        ENUMLIST=['Normal', 'Bold', 'Italic', 'Bold Italic'], $
        NAME='Label font style', $
        DESCRIPTION='Label font style'

    self->RegisterProperty, 'LABEL_FONT_SIZE', /FLOAT, $
        NAME='Label font size', $
        DESCRIPTION='Label font size in points'

    ; Undoc'd property, so the property sheet can get/set a single string.
    self->RegisterProperty, '_SYM_TEXT', /STRING, $
      NAME='Symbol text', DESCRIPTION='Symbol text'

    self->RegisterProperty, 'SYM_FONT_INDEX', ENUMLIST=fonts, $
        NAME='Symbol font', $
        DESCRIPTION='Symbol name', SENSITIVE=0

    self->RegisterProperty, 'SYM_FONT_STYLE', $
        ENUMLIST=['Normal', 'Bold', 'Italic', 'Bold Italic'], $
        NAME='Symbol font style', $
        DESCRIPTION='Symbol font style', SENSITIVE=0

    self->RegisterProperty, 'SYM_FONT_SIZE', /FLOAT, $
        NAME='Symbol font size', $
        DESCRIPTION='Symbol font size in points', SENSITIVE=0

    self->RegisterProperty, 'SYM_FONT_FILL_BACKGROUND', /BOOLEAN, $
        NAME='Symbol font fill', $
        DESCRIPTION='Symbol font fill background'

    self->RegisterProperty, 'SYM_FONT_FILL_COLOR', /COLOR, $
        NAME='Symbol font fill color', $
        DESCRIPTION='Symbol font fill color'

    self->RegisterProperty, 'SYM_ROTATE', /FLOAT, $
        NAME='Symbol text rotation', $
        DESCRIPTION='Symbol text rotation'

    self->RegisterProperty, 'LINESTYLE', /LINESTYLE, $
        NAME='Connecting line style', $
        DESCRIPTION='Connecting line style'

    self->RegisterProperty, 'LINE_COLOR', /COLOR, $
        NAME='Connecting line color', $
        DESCRIPTION='Connecting line color'

    self->RegisterProperty, 'LINE_THICK', /FLOAT, $
        NAME='Connecting line thickness', $
        DESCRIPTION='Connecting line thickness', $
        VALID_RANGE=[1.0,10.0, .1d]


  endif
end


;-------------------------------------------------------------------------
pro IDLitVisSymbol::GetProperty, $
    DATA=data, $
    LABEL_COLOR=labelColor, $
    LABEL_FILL_BACKGROUND=labelFillBackground, $
    LABEL_FILL_COLOR=labelFillColor, $
    LABEL_FONT_INDEX=labelFontIndex, $
    LABEL_FONT_NAME=labelFontName, $
    LABEL_FONT_SIZE=labelFontSize, $
    LABEL_FONT_STYLE=labelFontStyle, $
    LABEL_ORIENTATION=labelOrientation, $
    _LABEL_POSITION=labelPosIndex, $
    LABEL_POSITION=labelPosition, $
    LABEL_SHIFT=labelShift, $
    _LABEL_STRING=labelStrScalar, $
    LABEL_STRING=labelString, $
    LABEL_TRANSPARENCY=labelTransparency, $
    LINE_COLOR=lineColor, $
    LINESTYLE=lineStyle, $
    LINE_THICK=lineThick, $
    SYMBOL=symbolValue, $
    _SYM_COLOR=symbolColorPropSheet, $
    SYM_COLOR=symbolColor, $
    SYM_FILLED=symFilled, $
    SYM_FILL_COLOR=symFillColor, $
    SYM_FONT_INDEX=symFontIndex, $
    SYM_FONT_NAME=symFontName, $
    SYM_FONT_STYLE=symFontStyle, $
    SYM_FONT_SIZE=symFontSize, $
    SYM_FONT_FILL_BACKGROUND=symFontFillBackground, $
    SYM_FONT_FILL_COLOR=symFontFillColor, $
    SYM_INDEX=symbolIndex, $  ; keep for backwards compat
    SYM_ROTATE=symRotate, $
    SYM_SIZE=symbolSize, $
    _SYM_TEXT=symbolTextScalar, $
    SYM_TEXT=symbolText, $
    SYM_THICK=symbolThick, $
    SYM_TRANSPARENCY=symbolTransparency, $
    _REF_EXTRA=_extra

  compile_opt idl2, hidden
  @graphic_error
  
  
  if (Arg_Present(lineColor)) then $
    self._oPolyline->GetProperty, COLOR=lineColor

  if (Arg_Present(lineStyle)) then $
    self._oPolyline->GetProperty, LINESTYLE=lineStyle

  if (Arg_Present(lineThick)) then $
    self._oPolyline->GetProperty, THICK=lineThick

  ; Get the value of SYMBOL based on whether this is using a
  ; normal symbol or text symbol
  if ARG_PRESENT( symbolValue ) then begin
    self._oSymbol->GetProperty, SYMBOL=symbolValue
  endif
  

  if (Arg_Present(symbolText) || Arg_Present(symbolTextScalar)) then begin
    symbolText = Ptr_Valid(self._symText) ? *self._symText : ''
    ; Property sheet can only handle scalar strings.
    symbolTextScalar = symbolText[0]
  endif
  
  ; Get the appropriate Symbol properties
  self._oSymbol->GetProperty, SYM_FILLED=symFilled, $
    SYM_FILL_COLOR=symFillColor, SYM_INDEX=symbolIndex, SYM_SIZE=symbolSize, $
    SYM_THICK=symbolThick

  if (Arg_Present(symbolTransparency)) then begin
    self._oPolyline->GetProperty, ALPHA_CHANNEL=alpha
    symbolTransparency = 0 > ROUND(100 - alpha*100) < 100
  endif

  if (Arg_Present(symbolColorPropSheet)) then $
    self._oPolyline->GetProperty, COLOR=symbolColorPropSheet

  if (Arg_Present(symbolColor)) then begin
    self._oPolyline->GetProperty, COLOR=symbolColor, VERT_COLORS=vertColors
    if (ISA(vertColors, /ARRAY)) then $
      symbolColor = vertColors
  endif

  if (Arg_Present(data)) then $
    data = PTR_VALID(self._data) ? *self._data : !NULL
    
  ; Get the appropriate properties for the Text Symbol
  self._oTextSymbol->GetProperty, $
    FILL_BACKGROUND=symFontFillBackground, $
    FILL_COLOR=symFontFillColor
  
  self._oTextSymFont->GetProperty, $
    FONT_SIZE=symFontSize, FONT_STYLE=symFontStyle

  ; Get the appropriate Text properties
  self._oLabel->GetProperty, COLOR=labelColor, $
    FILL_BACKGROUND=labelFillBackground, $
    FILL_COLOR=labelFillColor, $
    ALPHA_CHANNEL=alpha

  labelTransparency = 0 > ROUND(100 - alpha*100) < 100

  if (Arg_Present(labelFontIndex) || Arg_Present(labelFontName) || $
    Arg_Present(labelFontSize) || Arg_Present(labelFontStyle)) then begin
    self._oFont->GetProperty, $
      FONT_INDEX=labelFontIndex, $
      FONT_NAME=labelFontName, $
      FONT_SIZE=labelFontSize, $
      FONT_STYLE=labelFontStyle
  endif

  if (Arg_Present(labelStrScalar)) then begin
    labelStrScalar = Ptr_Valid(self._labelString) ? $
      (*self._labelString)[0] : ''
  endif

  if (Arg_Present(labelString)) then begin
    labelString = Ptr_Valid(self._labelString) ? $
      *self._labelString : ''
  endif

  if (ARG_PRESENT(symFontIndex)) then $
    self._oTextSymFont->GetProperty, FONT_INDEX=symFontIndex

  if ( ARG_PRESENT( symFontName ) ) then $
    self._oTextSymFont->GetProperty, FONT_NAME=symFontName

  if ARG_PRESENT( labelOrientation ) then $
    labelOrientation = self._labelOrientation
    
  if ARG_PRESENT( symRotate ) then $
    symRotate = self._symRotation

  if ARG_PRESENT( labelPosition ) then $
    labelPosition = self->_PositionConvert( self._labelPosition, /NUM_TO_STRING )

  ; Undoc'd property, so the property sheet can get the position by index.
  if (ARG_PRESENT(labelPosIndex)) then $
    labelPosIndex = self._labelPosition

  if ARG_PRESENT( labelShift ) then $
    labelShift = self._labelShift

  self.IDLitVisualization::GetProperty, _EXTRA=_extra
  
end

;-------------------------------------------------------------------------
pro IDLitVisSymbol::SetProperty, $
    DATA=data, $
    LABEL_COLOR=labelColorIn, $
    LABEL_FILL_BACKGROUND=labelFillBackground, $
    LABEL_FILL_COLOR=labelFillColorIn, $
    LABEL_FONT_INDEX=labelFontIndex, $
    LABEL_FONT_NAME=labelFontName, $
    LABEL_FONT_SIZE=labelFontSize, $
    LABEL_FONT_STYLE=labelFontStyle, $
    LABEL_ORIENTATION=labelOrientationIn, $
    _LABEL_POSITION=labelPosIndex, $
    LABEL_POSITION=labelPositionIn, $
    LABEL_SHIFT=labelShift, $
    _LABEL_STRING=labelStrScalar, $
    LABEL_STRING=labelString, $
    LABEL_TRANSPARENCY=labelTransparency, $
    LINE_COLOR=lineColorIn, $
    LINESTYLE=lineStyle, $
    LINE_THICK=lineThick, $
    SYMBOL=symbolValue, $
    _SYM_COLOR=symbolColorPropSheet, $
    SYM_COLOR=symbolColorIn, $
    SYM_FILLED=symFilled, $
    SYM_FILL_COLOR=symFillColor, $
    SYM_FONT_INDEX=symFontIndex, $
    SYM_FONT_NAME=symFontName, $
    SYM_FONT_STYLE=symFontStyle, $
    SYM_FONT_SIZE=symFontSize, $
    SYM_FONT_FILL_BACKGROUND=symFontFillBackground, $
    SYM_FONT_FILL_COLOR=symFontFillColorIn, $
    SYM_INDEX=symbolIndex, $  ; keep for backwards compat
    SYM_ROTATE=symRotateIn, $
    SYM_SIZE=symbolSize, $
    _SYM_TEXT=symbolTextScalar, $
    SYM_TEXT=symbolText, $
    SYM_THICK=symbolThick, $
    SYM_TRANSPARENCY=symbolTransparency, $
    _EXTRA=_extra

  compile_opt idl2, hidden
  @graphic_error
  
  updateLabelLocation = 0
  
  self->IDLitComponent::GetProperty, INITIALIZING=isInit

  if ( ISA( symFontName, 'STRING' ) ) then begin
    self._oTextSymFont->SetProperty, FONT_NAME=symFontName
    updateLabelLocation = 1
  endif

  if (ISA(symFontIndex)) then $
    self._oTextSymFont->SetProperty, FONT_INDEX=symFontIndex

  ; Set the appropriate Symbol Properties
  if (ISA(symFilled) || ISA(symFillColor) || $
    ISA(symbolIndex) || ISA(symbolThick)) then begin
    self._oSymbol->SetProperty, $
      SYM_FILLED=symFilled, $
      SYM_FILL_COLOR=symFillColor, $
      SYM_INDEX=symbolIndex, $  ; keep for backwards compat
      SYM_THICK=symbolThick
  endif


  if (ISA(symbolColorPropSheet)) then symbolColorIn = symbolColorPropSheet

  if ( ISA( symbolColorIn ) ) then begin
    style_convert, symbolColorIn, COLOR=symbolColor
    ncolor = N_ELEMENTS(symbolColor[0,*])
    if (ncolor gt 1) then begin
      self._oPolyline->SetProperty, VERT_COLORS=symbolColor
    endif else begin
      self._oPolyline->SetProperty, COLOR=symbolColor, VERT_COLORS=-1
      self._oSymbol->SetProperty, SYM_COLOR=symbolColor
    endelse
    self._oTextSymbol->SetProperty, COLOR=symbolColor[*,0]
  endif


  if (ISA(symFontFillColorIn)) then begin
    Style_Convert, symFontFillColorIn, COLOR=symFontFillColor
    self._oTextSymbol->SetProperty, FILL_COLOR=symFontFillColor
  endif

  ; Set the appropriate properties for the Text Symbol
  if (ISA(symFontFillBackground)) then $
    self._oTextSymbol->SetProperty, FILL_BACKGROUND=symFontFillBackground

  if ISA(symbolTransparency) then begin
    alpha = 0 > ((100.-symbolTransparency)/100) < 1
    self._oPolyline->SetProperty, ALPHA_CHANNEL=alpha
    self._oTextSymbol->SetProperty, ALPHA_CHANNEL=alpha
  endif

  self._oTextSymFont->SetProperty, FONT_STYLE=symFontStyle

  if (ISA(labelFillColorIn)) then begin
    Style_Convert, labelFillColorIn, COLOR=labelFillColor
    self._oLabel->SetProperty, FILL_COLOR=labelFillColor
  endif

  if (Isa(labelColorIn)) then begin
    style_convert, labelColorIn, COLOR=labelColor
    self._oLabel->SetProperty, COLOR=labelColor
  endif

  if (Isa(labelFillBackground)) then $
    self._oLabel->SetProperty, FILL_BACKGROUND=labelFillBackground

  if ISA(labelTransparency) then $
    self._oLabel->SetProperty, ALPHA_CHANNEL=0 > ((100.-labelTransparency)/100) < 1

  if (ISA(labelFontIndex) || ISA(labelFontName) || $
    ISA(labelFontSize) || ISA(labelFontStyle)) then begin
    self._oFont->SetProperty, $
      FONT_INDEX=labelFontIndex, $
      FONT_NAME=labelFontName, $
      FONT_SIZE=labelFontSize, $
      FONT_STYLE=labelFontStyle
  endif

  if (ISA(symbolTextScalar)) then $
    symbolText = symbolTextScalar
  
  if ( ISA( symbolText ) ) then begin
    if (~Ptr_Valid(self._symText)) then $
      self._symText = Ptr_New(/ALLOC)
    *self._symText = symbolText

    usesTextSymbol = symbolText[0] ne ''
    self._oTextSymbol->GetProperty, HIDE=wasHidden
    ; Add/remove the text symbol and the polyline, so they don't
    ; mess up the X/Y/Z range.
    if (usesTextSymbol) then begin
      if (wasHidden) then begin
        self._oTextSymbol->SetProperty, HIDE=0
        if (~self->IsContained(self._oTextSymbol)) then $
          self->Add, self._oTextSymbol, /NO_UPDATE
        self->Remove, self._oPolyline, /NO_UPDATE
      endif
    endif else begin
      if (~wasHidden) then begin
        self._oTextSymbol->SetProperty, /HIDE
        self->Remove, self._oTextSymbol, /NO_UPDATE
        if (~self->IsContained(self._oPolyline)) then $
          self->Add, self._oPolyline, /NO_UPDATE
      endif
    endelse
    self->SetPropertyAttribute, ['SYMBOL', 'SYM_SIZE', $
      'SYM_THICK','SYM_FILLED','SYM_FILL_COLOR'], $
      SENSITIVE=~usesTextSymbol
    self->SetPropertyAttribute, ['SYM_FONT_INDEX', 'SYM_FONT_SIZE', $
      'SYM_FONT_STYLE', 'SYM_ROTATE', 'SYM_FONT_FILL_BACKGROUND', $
      'SYM_FONT_FILL_COLOR'], $
      SENSITIVE=usesTextSymbol

    ; Reset the symbol value to make sure that the properties are properly sensitized
    self._oSymbol->GetProperty, SYMBOL=_sym
    self._oSymbol->SetProperty, SYMBOL=_sym

    ; This will actually put the strings into the symbolText object.
    updateLabelLocation = 1
  endif


  usesTextSymbol = Ptr_Valid(self._symText) && (*self._symText)[0] ne ''


  if (ISA(lineColorIn)) then begin
    Style_Convert, lineColorIn, COLOR=lineColor
    self._oPolyline->SetProperty, COLOR=lineColor
  endif

  if (ISA(lineStyle)) then begin
    self._oPolyline->SetProperty, LINESTYLE=lineStyle
    ; Add/remove the polyline, so it doesn't mess up the X/Y/Z range.
    if (usesTextSymbol && (lineStyle eq 6)) then begin
      self->Remove, self._oPolyline, /NO_UPDATE
    endif else begin
      if (~self->IsContained(self._oPolyline)) then $
        self->Add, self._oPolyline, /NO_UPDATE
    endelse
    self->SetPropertyAttribute, ['LINE_COLOR', 'LINE_THICK'], $
      SENSITIVE=lineStyle[0] ne 6
  endif

  if (ISA(lineThick)) then $
    self._oPolyline->SetProperty, THICK=lineThick


  if ( ISA( symbolValue ) ) then begin
    ; Set the symbol property for a regular symbol
    self._oSymbol->SetProperty, SYMBOL=symbolValue
    updateLabelLocation = 1
  endif

  if (ISA(labelStrScalar)) then $
    labelString = labelStrScalar

  if ( ISA( labelString ) ) then begin
    if (~Ptr_Valid(self._labelString)) then $
      self._labelString = Ptr_New(/ALLOC)
    *self._labelString = labelString
    updateLabelLocation = 1
  endif

  if ( ISA( symbolSize ) ) then begin
    
    ; If SYM_SIZE is set while using a font as a symbol, then
    ; set the font size to be SYM_SIZE * 12.0
    if (usesTextSymbol) then begin
      self._oTextSymFont->SetProperty, FONT_SIZE=symbolSize * 12.0
      self._oTextSymFont->GetProperty, FONT_SIZE=fs
    endif
    
    ; Set the SYM_SIZE of the symbol
    self._oSymbol->SetProperty, SYM_SIZE=symbolSize
    updateLabelLocation = 1
  endif
  
  if ( ISA( symFontSize ) ) then begin
    self._oTextSymFont->SetProperty, FONT_SIZE=symFontSize
    updateLabelLocation = 1
  endif

  if ( ISA( labelShift ) ) then begin
    self._labelShift = DOUBLE( labelShift )
    updateLabelLocation = 1
  endif

  if ( ISA( data ) && ~isInit ) then begin
    if (~Ptr_Valid(self._data)) then $
      self._data = PTR_NEW(/ALLOC)
    *self._data = data
    self->OnProjectionChange
    ; OnProjectionChange will call ::UpdateLabelLocation
    updateLabelLocation = 1
  end

  ; Undoc'd property, so the property sheet can set the position by index.
  if (ISA(labelPosIndex)) then $
    labelPositionIn = labelPosIndex

  ; LABEL_POSITION
  if (ISA(labelPositionIn)) then begin
    if ISA( labelPositionIn, 'STRING' ) then begin
      labelIndex = self->_PositionConvert( labelPositionIn, /STRING_TO_NUM )
      if ISA( labelIndex ) then begin
        self._labelPosition = labelIndex
        updateLabelLocation = 1
      endif else begin
        message, 'LABEL_POSITION - Invalid value specified: ' + labelPositionIn + '.', /informational
      endelse
    endif else begin ; Also allow numbers from 0 -> 8
      inType = size( labelPositionIn, /TYPE )
      invalidTypes =[0,7,8,10,11]
      if ISA( where( invalidTypes eq inType, /NULL ) ) then begin
        message, 'LABEL_POSITION - Invalid value specified: ' + labelPositionIn + '.', /informational
      endif else begin
        self._labelPosition = 0 > FIX( labelPositionIn ) < 8
        updateLabelLocation = 1
      endelse
    endelse
  endif

  ; LABEL_ORIENTATION - rotate around the Z-Axis
  if ( ISA( labelOrientationIn ) ) then begin
    rotation = labelOrientationIn mod 360
    if (rotation lt -180) then rotation += 360
    if (rotation gt 180) then rotation -= 360
    cosr = cos(rotation*!DtoR)
    sinr = sin(rotation*!DtoR)
    self._oLabel->SetProperty, BASELINE=[cosr, sinr], UPDIR=[-sinr, cosr]
    self._labelOrientation = rotation    ; Store new orientation values
    
    updateLabelLocation = 1
  endif

  ; SYM_ROTATE - rotate around the Z-Axis
  if ( ISA( symRotateIn ) ) then begin
    rotation = symRotateIn mod 360
    if (rotation lt -180) then rotation += 360
    if (rotation gt 180) then rotation -= 360
    cosr = cos(rotation*!DtoR)
    sinr = sin(rotation*!DtoR)
    self._oTextSymbol->SetProperty, BASELINE=[cosr, sinr], UPDIR=[-sinr, cosr]
    ; Unrotate to 0
;    self._oPolyline->Rotate, [0,0,1], -self._symRotation
;    if (symbolRotation ne 0) then begin
;      ; Rotate to new orientation
;      self._oPolyline->Rotate, [0,0,1], symbolRotation
;    endif
    self._symRotation = rotation
    
    updateLabelLocation = 1
  endif

  self->IDLitVisualization::SetProperty, _EXTRA=_extra

  ; Update the location of the label, if necessary
  if ( updateLabelLocation && ~isInit) then begin
    self->_UpdateLabelLocation
  endif
  
end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to retrieve the data
;
; Arguments:
;   X, Y
;
; Keywords:
;   NONE
;
pro IDLitVisSymbol::GetData, x, y, _REF_EXTRA=_extra
  compile_opt idl2, hidden

  if (n_params() ne 2) then $
    MESSAGE, 'Incorrect number of arguments.'
    
  if (~Ptr_Valid(self._data) || N_Elements(*self._data) eq 0) then begin
    x = []
    y = []
    return
  endif

  ; This is in data coordinates
  x = reform((*self._data)[0,*])
  y = reform((*self._data)[1,*])

  self->GetProperty, _PARENT=oParent, DATA=dataIn
  if (OBJ_ISA(oParent, 'IDLitgrAnnotateLayer')) then begin
    ; Return normalized coordinates if in the annotation layer
    data = iConvertCoord(x, y, /ANNOTATION_DATA, /TO_NORMAL)
    x = data[0,*]
    y = data[1,*]
  end

  ; Get additional keyword property
;  self->GetProperty, _EXTRA=_extra

end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to directly set the data
;
; Arguments:
;   Varies
;
; Keywords:
;   NONE
;
pro IDLitVisSymbol::_SetData, x, y, _EXTRA=_extra
  compile_opt idl2, hidden

  nx = N_ELEMENTS(x)
  nparams = n_params()
  if (nparams ne 2) then $
    MESSAGE, 'Incorrect number of arguments.'

  if ~ISA( x, /NUMBER ) || ~ISA( y, /NUMBER ) then $
    message, 'Invalid input coordinates.'
  
  ;; Ensure data is in proper format
  self->GetProperty, _PARENT=oParent
  if (OBJ_ISA(oParent, 'IDLitgrAnnotateLayer')) then begin
    data = iConvertCoord(x, y, /NORMAL, /TO_ANNOTATION_DATA)
    data[2,*] = 0.99d
  endif else begin
    data = DBLARR(2, N_ELEMENTS(x))
    data[0,*] = x
    data[1,*] = y
  endelse
  
  ; Change the Data
  if (~Ptr_Valid(self._data)) then $
    self._data = Ptr_New(/ALLOC)

  *self._data = data

  ; OnProjectionChange will call ::UpdateLabelLocation
  self->OnProjectionChange

end



;----------------------------------------------------------------------------
pro IDLitVisSymbol::RemoveRotateHandle, _REF_EXTRA=_extra
    
    compile_opt idl2, hidden

    oSelectBox = self->GetDefaultSelectionVisual()
    oManipVis = oSelectBox->Get(/ALL)
    foreach oManip, oManipVis do begin
      if (ISA(oManip, 'IDLitManipulatorVisual')) then begin
        oManip->GetProperty, VISUAL_TYPE=vt
        if (STRUPCASE(vt) eq 'ROTATE') then begin
          oSelectBox->Remove, oManip
          OBJ_DESTROY, oManip
          break
        endif
      endif
    endforeach
    
end


;-------------------------------------------------------------------------
pro IDLitVisSymbol::_UpdateLabelLocation

  compile_opt idl2, hidden

  self._oPolyline->GetProperty, DATA=symPosition

  if (~ISA(symPosition)) then return

  ; If we only have a single symbol, remove the duplicate polyline point.
  if Array_Equal(symPosition[*,0], symPosition[*,1]) then $
    symPosition = symPosition[*,0]

  nloc = N_Elements(symPosition[0,*])

  symSize = [0d, 0, 0]

  usesTextSymbol = Ptr_Valid(self._symText) && (*self._symText)[0] ne ''

  if ( ~usesTextSymbol ) then begin
  
    self._oSymbol->GetProperty, SYM_SIZE=symSizeNorm
    (self._oSymbol)._oSymbol->GetProperty, SIZE=symSizeActual

    if (symSizeNorm eq 0) then symSizeNorm = 1
    symSize = symSizeActual + (1d/symSizeNorm)*symSizeActual

  endif else begin
    
    ; Convert any math/greek symbols.
    symbolText = Ptr_Valid(self._symText) ? *self._symText : ''
    symbolText = Tex2IDL(symbolText)

    ; Repeat strings as necessary.
    symbolText = symbolText[LINDGEN(nloc) mod N_ELEMENTS(symbolText)]

    textDims = [0,0]
    nchar = 1
    oTool = self->GetTool()
    ; Assume if we have a parent we are in the current tool hierarchy.
    self->IDLgrModel::GetProperty, PARENT=oParent
    if (obj_valid(oParent) && OBJ_VALID(oTool)) then begin
      oWin = oTool->GetCurrentWindow()
      if (OBJ_VALID(oWin)) then begin
        ; Replace multiple strings with a single string,
        ; so we can calculate just a single string's dimensions.
        self._oTextSymbol->SetProperty, STRING=symbolText[0], LOCATIONS=symPosition[*,0]
        nchar = Strlen(symbolText[0])
        textDims = oWin->GetTextDimensions(self._oTextSymbol)
        self._oTextSymbol->SetProperty, STRING=symText, LOCATIONS=loc
      endif
    endif

    self._oTextSymbol->SetProperty, $
      LOCATIONS=symPosition, $
      STRINGS=symbolText

    ; Determine padding based on an average character width
    symSize = textDims/2d
    symSize[0] = symSize[0] + 0.25*textDims[0]/nchar
  endelse
  

  location = symPosition
  
  case self._labelPosition of
  0: BEGIN  ; center
       align = 0.5
       vertAlign = 0.5
     END
  1: BEGIN  ; right
    location[0,*] += symSize[0]
    align = 0
    vertAlign = 0.5
  END
  2: BEGIN  ; bottom right
    location[0,*] += symSize[0]
    location[1,*] -= symSize[1]
    align = 0
    vertAlign = 1
  END
  3: BEGIN  ; bottom
    location[1,*] -= symSize[1]
    align = 0.5
    vertAlign = 1
  END
  4: BEGIN  ; bottom left
    location[0,*] -= symSize[0]
    location[1,*] -= symSize[1]
    align = 1
    vertAlign = 1
  END
  5: BEGIN  ; left
    location[0,*] -= symSize[0]
    align = 1
    vertAlign = 0.5
  END
  6: BEGIN  ; top left
    location[0,*] -= symSize[0]
    location[1,*] += symSize[1]
    align = 1
    vertAlign = 0
  END
  7: BEGIN  ; top
    location[1,*] += symSize[1]
    align = 0.5
    vertAlign = 0
  END
  8: BEGIN  ; top right
    location[0,*] += symSize[0]
    location[1,*] += symSize[1]
    align = 0
    vertAlign = 0
  END
  endcase

  ; Add in the label_shift (specified by the user)
  location[0,*] += self._labelShift[0]
  location[1,*] += self._labelShift[1]

  ; Convert any math/greek symbols.
  labelString = Ptr_Valid(self._labelString) ? *self._labelString : ''
  labelString = Tex2IDL(labelString)

  ; Repeat strings as necessary.
  labelString = labelString[LINDGEN(nloc) mod N_Elements(labelString)]

  self._oLabel->SetProperty, LOCATIONS=location, $
    ALIGNMENT=align, VERTICAL_ALIGNMENT=vertAlign, $
    STRING=labelString

  ; If we have no strings then remove the text object, so it doesn't
  ; mess up the X/Y/Z range.
  if (ARRAY_EQUAL(labelString, '')) then begin
    self->Remove, self._oLabel, /NO_UPDATE
  endif else begin
    if (~self->IsContained(self._oLabel)) then $
      self->Add, self._oLabel, /NO_UPDATE
  endelse
  self->SetPropertyAttribute, '_LABEL_STRING', HIDE=nloc gt 1

  ; In order to correctly compute the text dimensions and ranges,
  ; we have to force a redraw.
  oTool = self->GetTool()
  oWin = ISA(oTool) ? oTool->GetCurrentWindow() : !NULL
  oDS = self->GetDataspace(/UNNORMALIZED)
  if (ISA(oWin) && ISA(oDS)) then begin
    oWin->Draw
    !NULL = oDS->_GetXYZAxisRange(xRange, yRange, zRange)
    self->_IDLitVisualization::ClipToDataRange, XRange, YRange, ZRange
    self->UpdateSelectionVisual
    oTool->RefreshCurrentWindow
  endif
end


;-------------------------------------------------------------------------
function IDLitVisSymbol::_PositionConvert, inVal, $
    STRING_TO_NUM=strToNum, $
    NUM_TO_STRING=numToString

  compile_opt idl2, hidden
  
  stringOutVals = ['Center', $
                   'Right', $
                   'Bottom Right', $
                   'Bottom', $
                   'Bottom Left', $
                   'Left', $
                   'Top Left', $
                   'Top', $
                   'Top Right']
  
  stringInVals = ['CENTER', $
                  'RIGHT', $
                  'BOTTOMRIGHT', $
                  'BOTTOM', $
                  'BOTTOMLEFT', $
                  'LEFT', $
                  'TOPLEFT', $
                  'TOP', $
                  'TOPRIGHT']
                  
  stringAbbrevVals = ['C','R','BR','B','BL','L','TL','T','TR']
  
  ; Handle input strings regardless of case and use of spaces
  if ( KEYWORD_SET( strToNum ) ) then begin
    inString = STRUPCASE( STRCOMPRESS( inVal, /REMOVE_ALL ) )
    index = WHERE( stringInVals eq inString or stringAbbrevVals eq inString, /NULL )
    return, index
  endif else begin
    index = 0 > inVal < 8
    return, stringOutVals[index]
  endelse

end


;----------------------------------------------------------------------------
pro IDLitVisSymbol::OnProjectionChange, sMap

  compile_opt idl2, hidden

  if (~Ptr_Valid(self._data) || N_Elements(*self._data) lt 2) then return

  if (~N_ELEMENTS(sMap)) then $
    sMap = self->GetProjection()

  ; If we have data values out of the normal lonlat range, then
  ; assume these are not coordinates in degrees.
  if (N_TAGS(sMap) gt 0) then begin
    lon = (*self._data)[0,*]
    lat = (*self._data)[1,*]
    if (MIN(lon, MAX=mx) lt -360 || mx gt 720) then sMap = 0
    if (MIN(lat, MAX=mx) lt -90.1 || mx gt 90.1) then sMap = 0
  endif

  symPosition = *self._data

  if (N_TAGS(sMap) gt 0) then begin
    if (N_TAGS(sMap) ne 0) then begin
      lonlat = MAP_PROJ_FORWARD(symPosition[0,*], symPosition[1,*], MAP_STRUCTURE=sMap)
      symPosition[0:1,*] = lonlat
    endif
  endif

  singlePoint = Size(symPosition, /N_DIM) eq 1

  ; IDLgrPolyline needs at least 2 points.
  if (singlePoint) then $
    symPosition = [[symPosition], [symPosition]]

  self._oPolyline->SetProperty, DATA=symPosition

  ; If we only have one symbol, make the center of rotation be the
  ; location of the symbol, not the average location of the entire graphic.
  self->_IDLitVisualization::SetProperty, $
    CENTER_OF_ROTATION=singlePoint ? [symPosition[0],symPosition[1],0] : -1

  self->_UpdateLabelLocation

end


;----------------------------------------------------------------------------
; IDLitVisSymbol::OnDataRangeChange
;
; Purpose:
;   This procedure method handles notification that the data range
;   has changed.
;
; Arguments:
;   oSubject: A reference to the object sending notification of the
;     data range change.
;   XRange:  The new xrange, [xmin, xmax]
;   YRange:  The new yrange, [ymin, ymax]
;   ZRange:  The new zrange, [zmin, zmax]
;
pro IDLitVisSymbol::OnDataRangeChange, oSubject, XRange, YRange, ZRange
  compile_opt idl2, hidden

  self->_IDLitVisualization::ClipToDataRange, XRange, YRange, ZRange

end


;-------------------------------------------------------------------------
pro IDLitVisSymbol__define

  compile_opt idl2, hidden

  void = {IDLitVisSymbol, $
          inherits IDLitVisualization, $
          _oPolyline:OBJ_NEW( ), $
          _oSymbol: OBJ_NEW( ), $
          _oTextSymbol:OBJ_NEW( ), $
          _oLabel:OBJ_NEW( ), $
          _oFont:OBJ_NEW( ), $
          _oTextSymFont:OBJ_NEW( ), $
          _oLargeFont:OBJ_NEW( ), $
          _oSmallFont:OBJ_NEW( ), $
          _labelString: PTR_NEW(), $
          _symText: PTR_NEW(), $
          _data: PTR_NEW(), $
          _labelOrientation:0.0D, $
          _labelPosition:0, $
          _labelShift:DBLARR(2), $
          _symRotation:0.0D}
          
end
