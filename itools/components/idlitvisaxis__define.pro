; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvisaxis__define.pro#1 $
;
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
; PURPOSE:
;    The IDLitVisAxis class is the component wrapper for the axis.
;
; MODIFICATION HISTORY:


;----------------------------------------------------------------------------
; IDLitVisAxis::_RegisterProperties
;
; Purpose:
;   This procedure method registers properties associated with this class.
;
; Calling sequence:
;   oObj->[IDLitVisAxis::]_RegisterProperties
;
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLitVisAxis::_RegisterProperties, $
  UPDATE_FROM_VERSION=updateFromVersion
  
  compile_opt idl2, hidden
  
  registerAll = ~KEYWORD_SET(updateFromVersion)
  
  ;; get numeric formats
  result = IDLitGetResource(1, numericFormatNames, /NUMERICFORMAT, /NAMES)
  result = IDLitGetResource(1, numericFormatExamples, $
    /NUMERICFORMAT, /EXAMPLES)
    
  ;; get time formats
  ; to print examples of time formats
  ; result = IDLitGetResource(1, /TIMEFORMAT, /PRINT)
  result = IDLitGetResource(1, timeFormatNames, /TIMEFORMAT, /NAMES)
  result = IDLitGetResource(1, timeFormatExamples, /TIMEFORMAT, /EXAMPLES)
  
  if (registerAll) then begin
    self._oAxis->RegisterProperty, 'TICK_DEFINEDFORMAT', $
      DESCRIPTION='Predefined tick format', $
      ENUMLIST=['None', $
      'Use Tick Format Code', $
      numericFormatNames+' ('+numericFormatExamples+')', $
      timeFormatNames+' ('+timeFormatExamples+')' $
      ], $
      NAME='Tick format', /ADVANCED_ONLY
      
    ; Text properties. Register these on the axis (even though they
    ; belong to us) so they appear in the correct order.
    self._oAxis->RegisterProperty, 'AXIS_TITLE', /STRING, $
      DESCRIPTION='Axis title', $
      NAME='Title'
      
    self._oAxis->RegisterProperty, 'TEXT_COLOR', /COLOR, $
      DESCRIPTION='Text color', $
      NAME='Text color'
      
    ; Norm Location needs to be registered to allow copy/paste to work
    self._oAxis->RegisterProperty, 'NORM_LOCATION', USERDEF='', $
      DESCRIPTION='Normalized Location', $
      NAME='Normalized Location', $
      /HIDE, /ADVANCED_ONLY
    self._oAxis->RegisterProperty, 'LOCATION', USERDEF='', $
      DESCRIPTION='Location', $
      NAME='Location', $
      /HIDE, /ADVANCED_ONLY
    self._oAxis->RegisterProperty, 'RANGE', USERDEF='', $
      DESCRIPTION='Range', $
      NAME='Range', $
      /HIDE, /ADVANCED_ONLY
    self._oAxis->RegisterProperty, 'CRANGE', USERDEF='', $
      DESCRIPTION='CRange', $
      NAME='CRange', $
      /HIDE, /ADVANCED_ONLY
    self._oAxis->RegisterProperty, 'TICKFRMTDATA', USERDEF='', $
      DESCRIPTION='Tick Format Data', $
      NAME='Tick Format Data', $
      /HIDE, /ADVANCED_ONLY
      
    ; Change some property attributes.
    self._oAxis->SetPropertyAttribute, ['EXACT', 'EXTEND'], /HIDE
    self._oAxis->SetPropertyAttribute, 'TICKLEN', $
      DESCRIPTION='Major tick length relative to overall range', $
      VALID_RANGE=[0,1,0.01d]
      
    self._oAxis->SetPropertyAttribute, 'LOG', SENSITIVE=0
    
  endif else if (updateFromVersion lt 610) then begin
    ; Update enumerated list for the 'TICK_DEFINEDFORMAT' property.
    self->SetPropertyAttribute, 'TICK_DEFINEDFORMAT', $
      ENUMLIST=['None', $
      'Use Tick Format Code', $
      numericFormatNames+' ('+numericFormatExamples+')', $
      timeFormatNames+' ('+timeFormatExamples+')' $
      ]
  endif
  
  if (registerAll || (updateFromVersion lt 610)) then begin
    self->RegisterProperty, 'DATA_POSITION', /BOOLEAN, $
      DESCRIPTION='Lock to Data Position', $
      NAME='Lock to Data', /ADVANCED_ONLY
      
    self->RegisterProperty, 'TRANSPARENCY', /INTEGER, $
      NAME='Transparency', $
      DESCRIPTION='Transparency of axis', $
      VALID_RANGE=[0,100,5]
      
    self->RegisterProperty, 'TEXT_ORIENTATION', /FLOAT, $
      NAME='Text orientation', $
      DESCRIPTION='Text orientation'
      
    if (registerAll) then begin
      ; Aggregate the axis and font properties.
      self->Aggregate, self._oAxis
      self->Aggregate, self._oFont
    endif
    
    ; Hide ALPHA_CHANNEL - use TRANSPARENCY property instead.
    self._oAxis->SetPropertyAttribute, 'ALPHA_CHANNEL', /HIDE, $
      /ADVANCED_ONLY
      
  endif
  
end

;----------------------------------------------------------------------------
; Purpose:
;    Initialize this component
;
;
; OUTPUTS:
;    This function method returns 1 on success, or 0 on failure.
;
function IDLitVisAxis::Init, $
  NAME=NAME, $
  DESCRIPTION=DESCRIPTION, $
  _REF_EXTRA=_extra
  
  compile_opt idl2, hidden
  
  if(not keyword_set(name))then name ="Axis"
  if(not keyword_set(DESCRIPTION))then DESCRIPTION ="Axis Visualization"
  
  ; Initialize superclass
  success = self->IDLitVisualization::Init( TYPE="IDLAXIS", $
    /REGISTER_PROPERTIES, $
    NAME=NAME, $
    DESCRIPTION=DESCRIPTION, $
    ICON='axis', $
    IMPACTS_RANGE=0, $
    /MANIPULATOR_TARGET, $
    _EXTRA=_extra)
    
  if (not success) then $
    return, 0
    
  self._ticklen = 0.05d
  self._coordTransform = [0d, 1d]
  
  ; Request no (additional) axes.
  self->SetAxesRequest, 0, /ALWAYS
  
  ; Create the Axis object.
  self._oAxis = OBJ_NEW('IDLgrAxis', $
    /ANTIALIAS, $
    /EXACT, $
    /REGISTER_PROPERTIES, $
    SUBGRIDSTYLE=0, $  ; solid line for minor ticks
    MAJOR=-1, MINOR=-1, /private)
    
  ; Create the Font object. Use the current zoom factor of the tool window
  ; as the initial font zoom factor.   Likewise for view zoom, and normalization
  ; factor.
  oTool = self->GetTool()
  if (OBJ_VALID(oTool) && OBJ_ISA(oTool, 'IDLitTool')) then begin
    oWin = oTool->GetCurrentWindow()
    if (OBJ_VALID(oWin)) then begin
      oWin->GetProperty, CURRENT_ZOOM=fontZoom
      oView = oWin->GetCurrentView()
      if (OBJ_VALID(oView)) then begin
        oView->GetProperty, CURRENT_ZOOM=viewZoom
        normViewDims = oView->GetViewport(UNITS=3,/VIRTUAL)
        fontNorm = MIN(normViewDims)
      endif
    endif
  endif
  self._oFont = OBJ_NEW('IDLitFont', FONT_ZOOM=fontZoom, VIEW_ZOOM=viewZoom, $
    FONT_NORM=fontNorm)
  self._oAxis->GetProperty, TICKTEXT=oText
  oText->SetProperty, FONT=self._oFont->GetFont(), RECOMPUTE_DIMENSIONS=2
  
  ; Add our axis.
  ; NOTE: the IDLgrAxis and IDLitFont properties will be aggregated
  ; as part of the property registration process in an upcoming call
  ; to ::_RegisterProperties.
  self->Add, self._oAxis, /NO_NOTIFY, /NO_UPDATE
  
  ; Register all properties.
  self->IDLitVisAxis::_RegisterProperties
  
  ; Create and set our default selection visual.
  oSelectionVisual = OBJ_NEW('IDLitManipVisSelect', /HIDE)
  self._oAxisShadow = OBJ_NEW('IDLgrAxis', $
    COLOR=!COLOR.DODGER_BLUE, $
    ALPHA_CHANNEL=0.6, $
    /EXACT, $
    MINOR=0, $
    /NOTEXT, $
    THICK=3)
  oSelectionVisual->Add, self._oAxisShadow
  self->SetDefaultSelectionVisual, oSelectionVisual, POSITION=0
  self._oMySelectionVisual = oSelectionVisual
  
  self._dataPosition = 0b     ; by default, axes are locked to screen position
  ; not data position
  
  self._lastTickFormat=ptr_new('')
  ; Set any properties
  self->IDLitVisAxis::SetProperty, $
    _EXTRA=_extra
    
  RETURN, 1 ; Success
end


;----------------------------------------------------------------------------
; Purpose:
;    Cleanup this component
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
pro IDLitVisAxis::Cleanup

  compile_opt idl2, hidden
  
  OBJ_DESTROY, self._oTitle
  OBJ_DESTROY, self._oFont
  OBJ_DESTROY, self._fakeAxis
  Ptr_Free, self._pTicktext
  PTR_FREE, self._pTickvalues
  ptr_free, self._lastTickFormat
  
  ; Cleanup superclass
  self->IDLitVisualization::Cleanup
end

;----------------------------------------------------------------------------
; IDLitVisAxis::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisAxis::Restore
  compile_opt idl2, hidden
  
  ; Call superclass restore.
  self->_IDLitVisualization::Restore
  
  ; Call ::GetProperty on each aggregated graphic object
  ; to force its internal restore process to be called, thereby
  ; ensuring any new properties are registered.
  if (OBJ_VALID(self._oAxis)) then $
    self._oAxis->GetProperty
  if (OBJ_VALID(self._oFont)) then $
    self._oFont->GetProperty
    
  ; Register new properties.
  self->IDLitVisAxis::_RegisterProperties, $
    UPDATE_FROM_VERSION=self.idlitcomponentversion
    
  ; ---- Required for SAVE files transitioning ----------------------------
  ;      from IDL 6.0 to 6.1 or above:
  if (self.idlitcomponentversion lt 610) then begin
    ; Request no (additional) axes.
    self.axesRequest = 0 ; No request for axes
    self.axesMethod = 0 ; Never request axes
    if (OBJ_VALID(self._oTitle)) then begin
      if (OBJ_VALID(self._oRevTitle)) then begin
        self->IDLgrModel::Remove, self._oRevTitle
        OBJ_DESTROY, self._oRevTitle
        self._oTitle->SetProperty, HIDE=0
      endif
      self->GetProperty, TEXTPOS=textpos
      self->SetProperty, TEXTPOS=textpos
    endif
  endif
  
  if (self.idlitcomponentversion lt 620) then begin
    self._oAxis->GetProperty, DIRECTION=mydirection, $
      TEXTPOS=oldpos, TICKDIR=olddir, $
      TEXTBASELINE=mybaseline, TEXTUPDIR=myupdir
    isBaseReversed = MIN(mybaseline) lt 0
    isUpReversed = MIN(myupdir) lt 0
    ; Need to update our new local TICKDIR and TEXTPOS properties.
    case mydirection of
      0: doReverse = isUpReversed
      1: doReverse = isBaseReversed
      2: doReverse = isBaseReversed
    endcase
    if (doReverse) then begin
      self->IDLitVisAxis::SetProperty, $
        TESTPOS=1-oldpos, TICKDIR=1-olddir
    endif
  endif
end

;----------------------------------------------------------------------------
pro IDLitVisAxis::Add, oTargets, $
  _EXTRA=_extra
  
  compile_opt idl2, hidden
  
  ; an axis may be pasted when another axis is selected
  ; If so, it should be added to the axes group
  ; so that its tick length can be recomputed
  self->GetProperty, PARENT=oAxes
  for i=0, n_elements(oTargets)-1 do begin
    if (OBJ_ISA(oTargets[i], "IDLitVisAxis") && OBJ_VALID(oAxes)) then begin
      oAxes->Add, oTargets[i], _EXTRA=_extra
    endif else begin
      self->IDLitVisualization::Add, oTargets, _EXTRA=_extra
    endelse
  endfor
  
  
end

;----------------------------------------------------------------------------
; Internal method to ensure that if the user has input either their
; own ticktext object or strings, that the number of strings
; matches the number of major ticks. If the number matches then we
; use their values, otherwise we let the axis pick its own values.
;
pro IDLitVisAxis::_VerifyTicktext

  compile_opt idl2, hidden
  
  ; If user has switched the format, then disable
  ; the user's custom ticktext.
  if (self._tickDefinedFormat gt 0) then begin
    if (Obj_Valid(self._oUserText) || $
      Ptr_Valid(self._pTicktext)) then begin
      self._oAxis->SetProperty, TICKTEXT=Obj_New()
    endif
    return
  endif
  
  self._oAxis->GetProperty, MAJOR=major, TICKTEXT=oText
  
  ; User has input a ticktext object.
  if (Obj_Valid(self._oUserText)) then begin
    self._oUserText->GetProperty, STRINGS=ticknames
    if (N_Elements(ticknames) eq major) then begin
      if (oText ne self._oUserText) then $
        self._oAxis->SetProperty, TICKTEXT=self._oUserText
    endif else begin
      self._oAxis->SetProperty, TICKTEXT=Obj_New()
    endelse
    return
  endif
  
  if (~Obj_Valid(oText)) then return
  oText->GetProperty, STRINGS=strings
  
  self->getProperty, TICKVALUES=tickValues
  
  ; User has input a string array.
  if (Ptr_Valid(self._pTicktext) && Ptr_Valid(self._pTickvalues)) then begin
    ; Determine which values in the strings array must be replaced
    ; with values from *self._pTicktext. This keeps user specified labels
    ; connected with their proper axes as the graphic is scrolled/panned
    nelements = N_ELEMENTS(*self._pTickvalues)
    if (nelements gt N_ELEMENTS(*self._pTicktext)) then $
      MESSAGE, 'Tick name array must have length equal to the number of major tick marks.'
    for tickIndex=0,nelements - 1 do begin
      strings[WHERE( (*self._pTickvalues)[tickIndex] eq tickValues, /NULL )] = $
        (*self._pTicktext)[tickIndex]
    endfor
    
    oText->SetProperty, STRINGS=strings
  endif
end


;---------------------------------------------------------------------------
; Purpose:
;   Internal routine to update the axis normalized location.
;
pro IDLitVisAxis::_UpdateNormLocation, location

  compile_opt idl2, hidden
  
  self->GetProperty, PARENT=oAxes
  if ~OBJ_VALID(oAxes) then begin
    ; colorbar axis is not part of an axes group
    xrange = [-1.0, 1.0]
    yrange = xrange
    zrange = [0.0, 0.0]
  endif else begin
    oAxes->GetProperty, XRANGE=xrange, YRANGE=yrange, ZRANGE=zrange
  endelse
  
  dx = xRange[1] - xRange[0]
  dy = yRange[1] - yRange[0]
  dz = zRange[1] - zRange[0]
  
  ; save the normalized location so we can determine if this axis
  ; should be repositioned at the min or max of the range of one of the other axes.
  self._normLocation[0] = (dx gt 0) ? (location[0]-xRange[0])/dx : 0
  self._normLocation[1] = (dy gt 0) ? (location[1]-yRange[0])/dy : 0
  self._normLocation[2] = (dz gt 0) ? (location[2]-zRange[0])/dz : 0
  
end


;----------------------------------------------------------------------------
pro IDLitVisAxis::_UpdateAxisTicklen

  compile_opt idl2, hidden
  
  layer = self->_GetLayer()
  if (OBJ_ISA(layer, 'IDLitgrAnnotateLayer')) then begin
    xr=[-1.0, 1.0]
    yr=xr
    zr=[0.0, 0.0]
  endif else begin
    self->GetProperty, PARENT=oAxes
    if ~OBJ_VALID(oAxes) then begin
      ; colorbar axis is not part of an axes group
      xr = [-1.0, 1.0]
      yr = xr
      zr = [0.0, 0.0]
    endif else begin
      oAxes->GetProperty, XRANGE=xr, YRANGE=yr, ZRANGE=zr
    endelse
  endelse
  
  self._oAxis->GetProperty, DIRECTION=direction
  
  ; For X axis the ticks extend in Y direction,
  ; for Y & Z axes the ticks extend in X direction.
  range = (direction eq 0) ? yr : xr
  
  ; Sanity check for NaNs or Infinities.
  isFinite = FINITE(range)
  if (~isFinite[0]) then $
    range[0] = isFinite[1] ? range[1] : 0
  if (~isFinite[1]) then $
    range[1] = isFinite[0] ? range[0] : 0
    
  ticklen = ABS(range[1] - range[0]) * self._ticklen
  self._oAxis->SetProperty, TICKLEN=ticklen
  self._oAxisShadow->SetProperty, TICKLEN=ticklen

end


;----------------------------------------------------------------------------
pro IDLitVisAxis::_UpdateAxisLocation

  compile_opt idl2, hidden
  
  self._oAxis->GetProperty, DIRECTION=direction
  
  ; set the location based on the norm_location.
  self->IDLgrModel::GetProperty, PARENT=oAxes
  if (~OBJ_ISA(oAxes, 'IDLitVisDataAxes')) then return
  
  oAxes->GetProperty, XRANGE=xrange, YRANGE=yrange, ZRANGE=zrange, $
    XREVERSE=xreverse, YREVERSE=yreverse, ZREVERSE=zreverse
    
  if (xReverse) then $
    xrange = REVERSE(xrange)
  if (yReverse) then $
    yrange = REVERSE(yrange)
  if (zReverse) then $
    zrange = REVERSE(zrange)
    
  if (self._dataPosition) then begin
    ; calc new norm location based on old current location
    ; and new range.  not changing location so return
    self->GetProperty, LOCATION=location
    self->_UpdateNormLocation, location
    return
  endif
  
  x = xrange[0] + self._normLocation[0] * (xrange[1] - xrange[0])
  y = yrange[0] + self._normLocation[1] * (yrange[1] - yrange[0])
  z = zrange[0] + self._normLocation[2] * (zrange[1] - zrange[0])
  case direction of
    0: location = [0, y, z]
    1: location = [x, 0, z]
    2: location = [x, y, 0]
  endcase
  
  ; Set the new location directly on the contained axis and its shadow.
  ; In this case, the normalized location should NOT change,
  ; so do NOT do this: self->SetProperty, LOCATION=location
  self._oAxis->SetProperty, LOCATION=location
  self._oAxisShadow->SetProperty, LOCATION=location
  
  ; Ensure that the major tick labels are accurate
  self->_VerifyTicktext
  
end


;----------------------------------------------------------------------------
; For tick labels and the title, update the baseline, updir, tickdir,
; text alignment, and text position.
;
; Properties that affect the text:
;   axis direction (X, Y, Z)
;   text orientation (angle)
;   axis range (reversed or not)
;   textpos (below or above)
;   tickdir (inwards or outwards)
;
pro IDLitVisAxis::_UpdateTextAlignment, mydirection

  compile_opt idl2, hidden
  
  ; See if any of the dataspace axes have a reversed range.
  self->IDLgrModel::GetProperty, PARENT=oAxes
  xreverse = 0b
  yreverse = 0b
  zreverse = 0b
  if (ISA(oAxes, 'IDLitVisDataAxes')) then begin
    oAxes->GetProperty, XREVERSE=xreverse, YREVERSE=yreverse, $
      ZREVERSE=zreverse
  endif
  
  case (mydirection) of
    0: if (self._coordTransform[1] lt 0) then xreverse = ~xreverse
    1: if (self._coordTransform[1] lt 0) then yreverse = ~yreverse
    2: if (self._coordTransform[1] lt 0) then zreverse = ~zreverse
  endcase

  ; TICKLAYOUT = 0- Axis+labels
  ;              1- Labels only
  ;              2- Box style
  self._oAxis->GetProperty, TICKLAYOUT=myTickLayout
  
  
  ; Rotation of tick labels
  cosr = cos(self._textOrientation*!dpi/180)
  sinr = sin(self._textOrientation*!dpi/180)
  
  ;  oDataSpace = self->GetDataSpace(/UNNORMALIZED)
  ;  is3D = ISA(oDataSpace) && oDataSpace->Is3D()
  
  ; If orientation is not zero,
  ; then set the ONGLASS property so the text rotates properly.
  ; Otherwise it looks all stretched and squashed.
  onGlass = (self._textOrientation ne 0); && ~is3D
  oAxis = self._oAxis
  p = oAxis.ticktext
  if (ISA(p) && ISA(*p, /SCALAR)) then begin
    (*p)->SetProperty, ONGLASS=onGlass
  endif
  
  mybaseline = [cosr, sinr, 0]
  
  if (xreverse && ~onGlass) then $
    mybaseline = -mybaseline
    
  ; This is tricky. We want to change the text alignment depending
  ; upon both the text position and whether reversed or not.
  ; Normally, if you never set text alignment, then setting TEXTPOS
  ; will automatically change the alignment. However, since our axis
  ; may be reversed, we need to calculate the alignment manually.
  case mydirection of
    0: begin
      myupdir = [-sinr, cosr, 0]
      ;***
      ; If ONGLASS is false (i.e. we are allowing 3D text say for a surface),
      ; then we need to flip the X ticks UPDIR if the Y axis is reversed.
      ; If ONGLASS is true (i.e. the text has been rotated), then even though
      ; the Y axis might be reversed, the text will ignore the model transform
      ; and will not be upside down. Therefore, we don't need to flip UPDIR.
      if (yreverse && ~onGlass) then $
        myupdir = -myupdir
      isUpReversed = (mydirection eq 2) ? zreverse : yreverse
      mytextpos = isUpReversed ? 1-self._textpos : self._textpos
      ; The default is [0.5, 0] or [0.5, 1]
      defaultalignment = (mytextpos ne isUpReversed) ? [0.5, 0] : [0.5, 1]
      myalignment = (mytextpos ne isUpReversed) ? $
        [0.5d*(1-sinr), 0.5d*(1-cosr)] : [0.5d*(1+sinr), 0.5d*(1+cosr)]
      ; If Box layout then left justify
      if (myTickLayout eq 2) then $
        myalignment[0] = 0
      if (self._tickdir ne 2) then $
        mytickdir = isUpReversed ? 1-self._tickdir : self._tickdir
    end
    1: begin
      myupdir = [-sinr, cosr, 0]
      ; See note *** above.
      if (yreverse && ~onGlass) then $
        myupdir = -myupdir
      mytextpos = xreverse ? 1-self._textpos : self._textpos
      ; The default is [0, 0.5] or [1, 0.5]
      defaultalignment = (mytextpos ne xreverse) ? [0, 0.5] : [1, 0.5]
      myalignment = (mytextpos ne xreverse) ? $
        [0.5d*(1-cosr), 0.5d*(1+sinr)] : [0.5d*(1+cosr), 0.5d*(1-sinr)]
      ; If Box layout then left justify
      if (myTickLayout eq 2) then $
        myalignment[1] = 0
      if (self._tickdir ne 2) then $
        mytickdir = xreverse ? 1-self._tickdir : self._tickdir
    end
    2: begin
      if (onGlass) then begin
        mybaseline = [cosr, sinr, 0]
        ;        if (xreverse && ~onGlass) then $
        ;          mybaseline = -mybaseline
        myupdir = [-sinr, cosr, 0]
      endif else begin
        mybaseline = [cosr, 0, sinr]
        if (xreverse && ~onGlass) then $
          mybaseline = -mybaseline
        myupdir = [-sinr, 0, cosr]
      endelse
      ; See note *** above.
      if (zreverse && ~onGlass) then $
        myupdir = -myupdir
      mytextpos = xreverse ? 1-self._textpos : self._textpos
      ; The default is [0, 0.5] or [1, 0.5]
      defaultalignment = (mytextpos ne xreverse) ? [0, 0.5] : [1, 0.5]
      myalignment = (mytextpos ne xreverse) ? $
        [0.5d*(1-cosr), 0.5d*(1+sinr)] : [0.5d*(1+cosr), 0.5d*(1-sinr)]
      ; If Box layout then left justify
      if (myTickLayout eq 2) then $
        myalignment[1] = 0
      if (self._tickdir ne 2) then $
        mytickdir = xreverse ? 1-self._tickdir : self._tickdir
    end
  endcase
  
  
  self._oAxis->SetProperty, TEXTALIGNMENTS=myalignment, $
    TEXTPOS=mytextpos, TICKDIR=mytickdir, $
    TEXTBASELINE=mybaseline, TEXTUPDIR=myupdir
  self._oAxisShadow->SetProperty, TEXTALIGNMENTS=myalignment, $
    TEXTPOS=mytextpos, TICKDIR=mytickdir, $
    TEXTBASELINE=mybaseline, TEXTUPDIR=myupdir
    
  ; Keep my TITLE in sync with my TEXTBASELINE, TEXTUPDIR, and TEXTPOS.
  if (OBJ_VALID(self._oTitle)) then begin
    defaultbaseline = (xReverse ? [-1,0,0] : [1,0,0])
    case mydirection of
      0: begin
        titlebaseline = defaultbaseline
        titleupdir = (yReverse ? [0,-1,0] : [0,1,0])
        titlevertalign = defaultalignment[1]
      end
      1: begin
        titlebaseline = (yReverse ? [0,-1,0] : [0,1,0])
        titleupdir = -defaultbaseline
        titlevertalign = 1-defaultalignment[0]
      end
      2: begin
        titlebaseline = (zReverse ? [0,0,-1] : [0,0,1])
        titleupdir = -defaultbaseline
        titlevertalign = 1-defaultalignment[0]
      end
    endcase
    
    self._oTitle->SetProperty, UPDIR=titleupdir, BASELINE=titlebaseline, $
      VERTICAL_ALIGNMENT=titlevertalign
  endif

end


;----------------------------------------------------------------------------
pro IDLitVisAxis::_UpdateRange, rangeIn

  compile_opt idl2, hidden
  
  self._oAxis->GetProperty, DIRECTION=mydirection, RANGE=range
  oldrange = range
  
  ; Fill in the values that have changed.
  if (N_ELEMENTS(rangeIn) eq 2) then $
    range = rangeIn
    
  ; Sanity check for NaNs or Infinities.
  isFinite = FINITE(range)
  if (~isFinite[0]) then $
    range[0] = isFinite[1] ? range[1] : 0
  if (~isFinite[1]) then $
    range[1] = isFinite[0] ? range[0] : 0
    
  ; Don't allow zero length axis.
  if (range[1] eq range[0]) then $
    range[1] = (range[0] ne 0) ? range[0]*(1.000001d) : 1e-12
    
  ; Check the dataspace to determine if this axis should be
  ; log or not, rather than counting on the properties of this
  ; axis itself since we might be called from onDataRangeChange
  ; and our log property might not be updated yet.
  islogarithmic = 0
  oDataSpace = self->GetDataSpace(/UNNORMALIZED)
  if (OBJ_VALID(oDataSpace)) then begin
    oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, ZLOG=zLog
    case mydirection of
      0: islogarithmic = xLog
      1: islogarithmic = yLog
      2: islogarithmic = zLog
    endcase
  endif
  
  ; For a log axis, the grAxis needs the range to be set to the
  ; original linear range.  Take the inverse log if necessary.
  if (islogarithmic) then begin
    rangeLog = range
    range = 10^range
  endif
  
  mycoordconv = [0, 1]
  
  ; Need to do coordTransform?
  if (~ARRAY_EQUAL(self._coordTransform, [0d,0d]) && $
    ~ARRAY_EQUAL(self._coordTransform, [0d,1d])) then begin
    
    mycoordconv = self._coordTransform
    
    ; Dataspace range
    dsRange = islogarithmic ? rangeLog : range
    
    ; My new transformed range
    range = [range[0]*mycoordconv[1] + mycoordconv[0], $
      range[1]*mycoordconv[1] + mycoordconv[0]]
      
    ; Now compute coordinate conversion.
    ; If log then we need to compute in log units.
    myrange = islogarithmic ? ALOG10(range) : range
    
    ratio = (dsRange[1] - dsRange[0])/(myrange[1] - myrange[0])
    mycoordconv = [dsRange[0] - myrange[0]*ratio, ratio]
    
  endif
  
  case (mydirection) of
    0: self._oAxis->SetProperty, XCOORD_CONV=mycoordconv
    1: self._oAxis->SetProperty, YCOORD_CONV=mycoordconv
    2: self._oAxis->SetProperty, ZCOORD_CONV=mycoordconv
  endcase
  case (mydirection) of
    0: self._oAxisShadow->SetProperty, XCOORD_CONV=mycoordconv
    1: self._oAxisShadow->SetProperty, YCOORD_CONV=mycoordconv
    2: self._oAxisShadow->SetProperty, ZCOORD_CONV=mycoordconv
  endcase
  
  ; If user has specified their own axis range, then clamp our range.
  if (~ARRAY_EQUAL(self._axisRange, [0,0])) then begin
    if (self._dataPosition) then begin
      range = self._axisRange
    endif else begin
      newrange = [range[0] > self._axisRange[0], range[1] < self._axisRange[1]]
      ; Only honor the user's range if part of it is showing on the screen.
      ; Otherwise just use the current dataspace range.
      if (newrange[0] lt newrange[1]) then range = newrange
    endelse
  endif
  
  self._oAxis->SetProperty, $
    LOG=isLogarithmic, $
    RANGE=range
    
  self._oAxisShadow->SetProperty, $
    LOG=isLogarithmic, $
    RANGE=range
    
  ; Enable Log property if we are already logarithmic,
  ; or if our range minimum is > 0.
  self._oAxis->GetProperty, LOG=wasLog
  oldsensitive = wasLog || (oldrange[0] gt 0 && oldrange[1] gt 0)
  sensitive = islogarithmic || (range[0] gt 0 && range[1] gt 0)
  if (oldsensitive ne sensitive) then begin
    self->SetPropertyAttribute, 'LOG', SENSITIVE=sensitive
  endif
  
end


;----------------------------------------------------------------------------
function IDLitVisAxis::GetComputedRange, style, dataRange

  compile_opt idl2, hidden
  
  ; exact range
  if (style eq 1) then return, dataRange
  
  self._oAxis->GetProperty, LOG=isLog, TICKUNITS=tickunits
  
  if (~ISA(self._fakeAxis)) then begin
    self._fakeAxis = OBJ_NEW('IDLgrAxis')
  endif
  
  ; Watch out for zero ranges.
  if (dataRange[0] eq dataRange[1]) then $
    dataRange[1] = dataRange[0] + 1
  
  dr = isLog ? 10d^dataRange : dataRange
  dr = -1d300 > dr < 1d300
  void = CHECK_MATH()
    
  self._fakeAxis->SetProperty, EXACT=(style eq 1 || style eq 3), $
    EXTEND=(style ge 2), $
    LOG=isLog, $
    RANGE=dr, $
    TICKUNITS=tickunits
    
  self._fakeAxis->GetProperty, CRANGE=newRange
  
  return, newRange
  
end


;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; IIDLProperty Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; Purpose:
;   This procedure method retrieves the
;   value of a property or group of properties.
;
; Arguments:
;   None.
;
; Keywords:
;   Any keyword to IDLitVisAxis::Init followed by the word "Get"
;   can be retrieved using IDLitVisAxis::GetProperty.  In addition
;   the following keywords are available:
;
;   Note: While SetProperty must not set the log property of the grAxis
;         directly, GetProperty uses the aggregated property to retrieve
;         the value.
;
pro IDLitVisAxis::GetProperty, $
  TICK_DEFINEDFORMAT=tickDefinedFormat, $
  TICKLEN=axisTicklen, $
  AXIS_RANGE=axisRange, $
  AXIS_TITLE=axisTitle, $
  COORD_TRANSFORM=coordTransform, $
  CRANGE=crange, $
  LOCATION=location, $
  MAJOR=major, $
  MINOR=minor, $
  NORM_LOCATION=normLocation, $  ; not registered, need to pass directly
  NOTEXT=noText, $
  RANGE=range, $
  SHOWTEXT=showText, $
  TEXT_COLOR=textColor, $
  TEXT_ORIENTATION=textOrientation, $
  TRANSPARENCY=transparency, $
  TRANSFORM=transform, $
  DATA_POSITION=dataPosition, $
  TICKDIR=tickdir, $
  TEXTPOS=textpos, $
  TICKNAME=tickName, $
  TICKUNITS=tickunits, $
  _REF_EXTRA=_extra
  
  compile_opt idl2, hidden
  
  
  ; Get my properties
  if ARG_PRESENT(coordTransform) then $
    coordTransform = self._coordTransform
    
  ; The user-defined range (may be smaller than dataspace range)
  if ARG_PRESENT(axisRange) then $
    axisRange = self._axisRange
    
  ; The actual range contained within the IDLgrAxis
  if ARG_PRESENT(crange) then $
    self._oAxis->GetProperty, CRANGE=crange
    
  ; The dataspace range corresponding to this axis.
  if ARG_PRESENT(range) then begin
    self->IDLgrModel::GetProperty, PARENT=oAxes
    if OBJ_VALID(oAxes) then begin
      self._oAxis->GetProperty, DIRECTION=mydirection
      case (mydirection) of
        0: oAxes->GetProperty, XRANGE=range
        1: oAxes->GetProperty, YRANGE=range
        2: oAxes->GetProperty, ZRANGE=range
      endcase
    endif else begin
      range = [0,1]
    endelse
  endif
  
  if ARG_PRESENT(location) then begin
    self._oAxis->GetProperty, LOCATION=location
    ; Check the dataspace for log axes.
    oDataSpace = self->GetDataSpace(/UNNORMALIZED)
    if (OBJ_VALID(oDataSpace)) then begin
      oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, ZLOG=zLog
      if (xLog) then location[0] = 10^location[0]
      if (yLog) then location[1] = 10^location[1]
      if (zLog) then location[2] = 10^location[2]
    endif
  endif
  
  if ARG_PRESENT(axisTicklen) then $
    axisTicklen = self._ticklen
    
  if ARG_PRESENT(tickDefinedFormat) then begin
    tickDefinedFormat = self._tickDefinedFormat
  endif
  
  if ARG_PRESENT(transparency) then begin
    self._oAxis->GetProperty, ALPHA_CHANNEL=alpha
    transparency = 0 > ROUND(100 - alpha*100) < 100
  endif
  
  if ARG_PRESENT(axisTitle) then begin
    ; Only return title if object exists.
    if (OBJ_VALID(self._oTitle)) then begin
      self._oTitle->GetProperty, STRINGS=dispAxisTitle, UVALUE=axisTitle
      if (~ISA(axisTitle, 'STRING')) then begin
        self._oTitle->SetProperty, UVALUE=dispAxisTitle
        axisTitle = dispAxisTitle
      endif
    endif else axisTitle = ''
  endif
  
  ; Retrieve the text color from either the axis or the text.
  if ARG_PRESENT(textColor) then begin
  
    ; Default is to use the axis color.
    self._oAxis->GetProperty, COLOR=textColor, $
      USE_TEXT_COLOR=useColor
      
    ; Retrieve from one of the actual text objects.
    if (useColor) then begin
      self._oAxis->GetProperty, TICKTEXT=oText
      if (OBJ_VALID(oText)) then $
        oText->GetProperty, COLOR=textColor
    endif
  endif
  
  if ARG_PRESENT(textOrientation) then $
    textOrientation = self._textOrientation
    
  if ARG_PRESENT(normLocation) then $
    normLocation = self._normLocation
    
  if ARG_PRESENT(dataPosition) then $
    dataPosition = self._dataPosition
    
  ; Override our TRANSFORM property and return the LOCATION
  ; inside the TRANSFORM. Needed for Undo/Redo.
  if ARG_PRESENT(transform) then begin
    self._oAxis->GetProperty, LOCATION=location
    transform = IDENTITY(4)
    transform[3,0:2] = location
  endif
  
  ; See note in SetProperty about MAJOR/MINOR values.
  if ARG_PRESENT(major) then begin
    if ((self._majorminor and 1b) ne 0b) then $
      self._oAxis->GetProperty, MAJOR=major $
    else $
      major = -1
  endif
  
  if ARG_PRESENT(minor) then begin
    if ((self._majorminor and 2b) ne 0b) then $
      self._oAxis->GetProperty, MINOR=minor $
    else $
      minor = -1
  endif
  
  if (ARG_PRESENT(notext) || ARG_PRESENT(showText)) then begin
    self._oAxis->GetProperty, NOTEXT=noText
    showText = ~noText
  endif
  
  if ARG_PRESENT(textpos) then $
    textpos = self._textpos
    
  if ARG_PRESENT(tickdir) then $
    tickdir = self._tickdir
    
  if ARG_PRESENT(tickName) then begin
    self._oAxis->GetProperty, TICKTEXT=oText
    if Obj_Valid(oText) then $
      oText->GetProperty, STRINGS=tickName
  endif
  
  if ARG_PRESENT(tickUnits) then begin
    self._oAxis->GetProperty, TICKUNITS=tickunits, TICKFORMAT=tf
    if strlowcase(tf[0]) eq 'idlexponentformat' then begin
      tickunits='exponent'
    endif
    
    if strlowcase(tf[0]) eq 'idlscientificformat' then begin
      tickunits='scientific'
    endif
  endif
  
  ; get superclass properties
  if (N_ELEMENTS(_extra) gt 0) then begin
    self->IDLitVisualization::GetProperty, _EXTRA=_extra
    oTool = self.GetTool( )
    if ISA( oTool ) then begin
      if (~strmatch((self.GetTool()).identifier, 'GRAPHIC')) then begin
        foreach tagname, _extra do begin
          if (strmatch(tagname, "TICKFORMAT")) then begin
            returned_tickformat = scope_varfetch(tagname, /ref_extra)
            ; just returned
            (SCOPE_VARFETCH(tagname, /REF_EXTRA)) = returned_tickformat[0]
          endif
        endforeach
      endif
    endif
  endif
end

;----------------------------------------------------------------------------
; Purpose:
;   This procedure method sets the
;   value of a property or group of properties.
;
; Arguments:
;   None.
;
; Keywords:
;   Any keyword to IDLitVisAxis::Init followed by the word "Set"
;   can be retrieved using IDLitVisAxis::GetProperty.  In addition
;   the following keywords are available:
;
;   Note: While SetProperty must not set the log property of the grAxis
;         directly, GetProperty uses the aggregated property to retrieve
;         the value.
;
pro IDLitVisAxis::SetProperty,  $
  AXIS_RANGE=axisRange, $
  AXIS_TITLE=axisTitle, $
  COLOR=color, $
  COORD_TRANSFORM=coordTransform, $
  DATA_POSITION=dataPosition, $
  DIRECTION=direction, $
  GRIDSTYLE=gridstyle, $
  LOCATION=location, $  ; not registered, need to pass directly
  LOG=log, $
  MAJOR=major, $
  MINOR=minor, $
  NORM_LOCATION=normLocation, $  ; not registered, need to pass directly
  NOTEXT=notext, $
  PRIVATE=PRIVATE, $
  RANGE=rangeIn, $
  SHOWTEXT=showText, $
  SUBGRIDSTYLE=subgridstyle, $
  TEXT_COLOR=textColor, $
  TEXTALIGNMENTS=textAlignments, $
  TEXTBASELINE=textbaseline, $
  TEXT_ORIENTATION=textOrientation, $
  TEXTPOS=textPos, $
  TEXTUPDIR=textupdir, $
  THICK=thick, $
  TICK_DEFINEDFORMAT=tickDefinedFormat, $
  TICKUNITS=tickUnits, $
  TICKDIR=tickdir, $
  TICKFORMAT=tickFormatIn, $
  TICKFRMTDATA=tickFrmtData, $
  TICKINTERVAL=tickInterval, $
  TICKLAYOUT=tickLayout, $
  TICKLEN=axisTicklen, $
  TICKNAME=tickName, $
  TICKTEXT=tickText, $
  TICKVALUES=tickValues, $  ; not registered, need to pass directly
  TRANSFORM=transform, $
  TRANSPARENCY=transparency, $
  XCOORD_CONV=xcoordconv, $  ; not registered, need to pass directly
  YCOORD_CONV=ycoordconv, $  ; not registered, need to pass directly
  ZCOORD_CONV=zcoordconv, $  ; not registered, need to pass directly
  _REF_EXTRA=_extra
  
  
  compile_opt idl2, hidden
  
  updateAlignment = 0b
  
  self->IDLitComponent::GetProperty, INITIALIZING=isInit
  
  ; Tricky code to verify that tick formats are legal
  ; or the format is a valid function name.
  if (N_ELEMENTS(tickFormatIn) ne 0) then begin
    tickFormatCheck= strarr(n_elements(tickFormatIn))
    tickFormat= strarr(n_elements(tickFormatIn))
    for i=0, n_elements(tickFormatIn)-1 do begin
      tickFormatCheck[i] = STRTRIM(tickFormatIn[i], 2)
      if (tickFormatCheck[i] ne '') then begin
        if (STRMID(tickFormatCheck[i], 0, 1) eq '(') && $
          (STRMID(tickFormatCheck[i], $
          STRLEN(tickFormatCheck[i])-1, 1) eq ')') then begin
          
          CATCH, err
          if (err ne 0) then begin
            CATCH, /CANCEL
          endif else begin
            ; If this fails we will skip over the next line.
            test = STRING(0, FORMAT=tickFormatCheck[i])
            tickFormat[i] = tickFormatCheck[i]
          endelse
          
        endif else begin
          ; it's not a format, verify that it is a tickformat function
          CATCH, err
          if (err ne 0) then begin
            CATCH, /CANCEL
          endif else begin
            ; If this fails we will skip over the next line.
            resolve_routine, tickFormatCheck[i], $
              /IS_FUNCTION, /NO_RECOMPILE
            tickFormat[i] = tickFormatCheck[i]
          endelse
          
        endelse
        
      endif else $   ; null string just resets the format or function name
        tickFormat[i] = tickFormatCheck[i]
        
    endfor
    
    ; We will actually set the TICKFORMAT below.
  endif
  
  
  if (N_ELEMENTS(tickDefinedFormat) eq 1) then begin
    self._tickDefinedFormat = tickDefinedFormat
    ; unhide the custom format property if tickDefinedFormat
    ; is set to 'Use Tick Format Code'
    self._oAxis->SetPropertyAttribute, 'TICKFORMAT', $
      SENSITIVE=(tickDefinedFormat eq 1)
      
    result = IDLitGetResource(1, numericFormats, /NUMERICFORMAT, /FORMATS)
    result = IDLitGetResource(1, timeFormats, /TIMEFORMAT, /FORMATS)
    offset = (num = 2)      ; offset to account for first two formats
    case 1 of
      ;; no format
      tickDefinedFormat eq 0 : tickFormat=''
      ;; use custom TICKFORMAT code already in place
      tickDefinedFormat eq 1 : $
        if (strlen(*(self._lastTickFormat)) gt 0) then tickFormat=*(self._lastTickFormat)
      ;; numeric formats
      tickDefinedFormat lt ((num+=n_elements(numericFormats))) : $
        tickFormat = numericFormats[tickDefinedFormat-offset]
      ;; time formats
      tickDefinedFormat lt ((num+=n_elements(timeFormats))) : $
        tickFormat = $
        timeFormats[tickDefinedFormat-offset-n_elements(numericFormats)]
      else:
    endcase
    
    ; We will actually set the TICKFORMAT below.
  endif
  
  
  ; Set the TICKFORMAT now.
  ; if TICKFRMTDATA is defined it must be passed in through
  ; the same setproperty call with TICKFORMAT
  if (N_ELEMENTS(tickFormat) || N_ELEMENTS(tickFrmtData)) then begin
    if((n_elements(tickFormat) gt 1) || $
      ((n_elements(tickFormat) gt 0) && (strlen(tickFormat[0]) gt 0))) then begin
      if (ptr_valid(self._lastTickFormat)) then ptr_free, self._lastTickFormat
      self._lastTickFormat = ptr_new(tickFormat)
    endif
    w=where(strlowcase(tickFormat) eq 'idlexponentformat',count)
    if count gt 0 then tickFormat[w]='idlexponentformat'
    
    w=where(strlowcase(tickFormat) eq 'idlscientificformat',count)
    if count gt 0 then tickFormat[w]='idlscientificformat'
    self._oAxis->SetProperty, TICKFORMAT=tickFormat, $
      TICKFRMTDATA=tickFrmtData
    ; If TICKFORMAT starts with a (C( then assume it is a time format,
    ; and change the TICKUNITS.
    if (N_ELEMENTS(tickFormat) eq 1) then begin
      ; Either time or null string (same as type 'Numeric').
      if (~ISA(tickUnits)) then begin
        tickUnits = STRCMP(tickFormat, '(C(', 3, /FOLD_CASE) ? $
        'Time' : ''
        ; We will actually set the TICKUNITS below.
      endif
    endif
  endif
  
  ; TRANSPARENCY
  if (N_ELEMENTS(transparency)) then begin
    alpha = 0 > ((100.-transparency)/100) < 1
    
    self._oAxis->SetProperty, ALPHA_CHANNEL=alpha
    
    ; Set all the tick text.
    self._oAxis->GetProperty, TICKTEXT=oText
    for i=0,N_ELEMENTS(oText)-1 do $
      oText[i]->SetProperty, ALPHA_CHANNEL=alpha
      
    ; Set the title text.
    if (OBJ_VALID(self._oTitle)) then $
      self._oTitle->SetProperty, ALPHA_CHANNEL=alpha
  endif
  
  
  ; If text gets turned off, disable the properties.
  if (N_ELEMENTS(notext) || N_ELEMENTS(showText)) then begin
    self._oAxis->GetProperty, NOTEXT=noTextOld
    newNoText = N_ELEMENTS(notext) ? $
      KEYWORD_SET(noText) : ~KEYWORD_SET(showText)
    self._oAxis->SetProperty, NOTEXT=newNoText
    if (newNoText ne noTextOld) then begin
      self->IDLitComponent::SetPropertyAttribute, $
        ['TEXT_COLOR','TEXTPOS'], SENSITIVE=~newNoText
    endif
  endif
  

  ; TICKUNITS needs to be set on both axis and shadow.
  if ISA(tickUnits) then begin
    if strlowcase(tickUnits[0]) eq 'exponent' || $
      strlowcase(tickUnits[0]) eq 'scientific' then begin
      if (ptr_valid(self._lastTickFormat)) then ptr_free, self._lastTickFormat
      self._lastTickFormat = ptr_new(tickFormat)
      self._oAxis->SetProperty, TICKFORMAT=(strlowcase(tickUnits[0]) eq 'exponent') ? $
        'idlexponentformat' : 'idlscientificformat', TICKFRMTDATA=tickFrmtData
    endif else begin
      self._oAxis->GetProperty,TICKFORMAT=tf
      if (tf[0] eq 'idlexponentformat' || tf[0] eq 'idlscientificformat' ) && $
        ~isa(tickFormat) then begin
        self._oAxis->SetProperty,TICKFORMAT=''
      endif
      self._oAxis->GetProperty, NOTEXT=noTextCurrent
      ; If no text is displayed, only draw a single axis.
      if (noTextCurrent) then tickUnits = tickUnits[0]
      self._oAxis->SetProperty, TICKUNITS=tickUnits
      self._oAxisShadow->SetProperty, TICKUNITS=tickUnits
    endelse
  endif
  
  ; TICKDIR needs to be set on both axis and shadow.
  ; We need to handle this here rather than using the aggregate property
  ; since we could be setting TICKDIR to our internal TICKDIR=2 flag,
  ; which isn't part of the enumerated list (for now). In this case
  ; we disable the TICKDIR property.
  if (N_ELEMENTS(tickdir) gt 0) then begin
    self._tickdir = tickdir
    self->SetPropertyAttribute, 'TICKDIR', $
      SENSITIVE=(tickdir ne 2), UNDEFINED=(tickdir eq 2)
    ; We will set the property below, depending upon axes reversal.
    updateAlignment = 1b
  endif
  
  
  ; Override our TRANSFORM property and set the LOCATION instead.
  ; Needed for Undo/Redo.
  if (N_ELEMENTS(transform) gt 0) then $
    location = transform[3,0:2]
    
  if (N_ELEMENTS(dataPosition)) then begin
    self._dataPosition = KEYWORD_SET(dataPosition)
    self->_UpdateClipping
  endif
  
  ; For location, call our internal method.
  if (N_ELEMENTS(location) gt 0) then begin
    newlocation = location
    ; Check the dataspace for log axes.
    oDataSpace = self->GetDataSpace(/UNNORMALIZED)
    if (OBJ_VALID(oDataSpace)) then begin
      oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, ZLOG=zLog
      if (xLog && newlocation[0] gt 0) then $
        newlocation[0] = ALOG10(newlocation[0])
      if (yLog && newlocation[1] gt 0) then $
        newlocation[1] = ALOG10(newlocation[1])
      if (zLog && newlocation[2] gt 0) then $
        newlocation[2] = ALOG10(newlocation[2])
    endif
    self._oAxis->SetProperty, LOCATION=newlocation
    self._oAxisShadow->SetProperty, LOCATION=newlocation
    self->IDLitVisAxis::_UpdateNormLocation, newlocation
  endif
  
  
  if (N_ELEMENTS(direction) gt 0) then begin
  
    ; Support the ability to specify direction as string X,Y,Z
    if SIZE(direction,/TYPE) eq 7 && $
      STRLEN(direction) eq 1 then begin
      case STRLOWCASE(direction) of
        'y': direction = 1
        'z': direction = 2
        else: direction = 0
      endcase
    endif
    
    self._oAxis->SetProperty, DIRECTION=direction
    self._oAxisShadow->SetProperty, DIRECTION=direction
    updateAlignment = 1b
  endif
  
  
  self._oAxis->GetProperty, DIRECTION=mydirection
  
  
  if ISA(coordTransform) then begin
    if (~isInit && ~ARRAY_EQUAL(coordTransform, self._coordTransform)) then begin
      self._coordTransform = coordTransform[0:1]
      ; Be sure to update the range to reflect the new coord convert.
      if (~ISA(rangeIn)) then begin
        self->GetProperty, RANGE=rangeCurrent
        rangeIn = rangeCurrent
      endif
    endif
  endif
  
  if (ISA(gridstyle)) then begin
    gridstyle  = linestyle_convert(gridstyle)
    self._oAxis->SetProperty, GRIDSTYLE=gridstyle
  endif

  if (ISA(subgridstyle)) then begin
    subgridstyle  = linestyle_convert(subgridstyle)
    self._oAxis->SetProperty, SUBGRIDSTYLE=subgridstyle
  endif
  
  if (N_ELEMENTS(thick) gt 0) then begin
    self._oAxis->SetProperty, THICK=thick
    self._oAxisShadow->SetProperty, THICK=(thick + 2) < 10 ; slightly fatter
  endif
  
  ; CR60436: Make sure to set TICKINTERVAL before TICKNAME
  if (N_ELEMENTS(tickInterval) gt 0) then begin
    self._oAxis->SetProperty, TICKINTERVAL=tickInterval
    self._oAxisShadow->SetProperty, TICKINTERVAL=tickInterval
  endif
  
  if (N_ELEMENTS(tickLayout) gt 0) then begin
    self._oAxis->GetProperty, NOTEXT=noTextCurrent
    ; If no text is displayed, don't allow box axes.
    tlayout = noTextCurrent ? 0 : tickLayout
    self._oAxis->SetProperty, TICKLAYOUT=tlayout
    ; Convert TICKLAYOUT=1 (only labels) into TICKLAYOUT=0.
    self._oAxisShadow->SetProperty, TICKLAYOUT=(tlayout eq 2) ? 2 : 0
    updateAlignment = 1b
  endif
  
  if (N_ELEMENTS(tickValues) ne 0) then begin
    tvtmp = tickValues[SORT(tickValues)]
    self._oAxis->SetProperty,TICKVALUES=tvtmp
    self._oAxisShadow->SetProperty,TICKVALUES=tvtmp
  endif
  
  ; The TICKNAME keyword has precedence over TICKTEXT although
  ; they are otherwise identical
  if (N_ELEMENTS(tickName) gt 0) then tickText=tickName
  
  if (N_ELEMENTS(tickText) gt 0) then begin
  
    if (ISA(tickText, 'STRING', /SCALAR) && tickText eq '') then begin
      Ptr_Free, self._pTicktext
      self._oAxis->SetProperty, TICKTEXT=OBJ_NEW()
    endif else begin
    
      self._oAxis->GetProperty, TICKTEXT=oAxisText
      self->GetProperty, MAJOR=nmajor, TICKINTERVAL=tickintervalCurr
      
      ; CR60436: If TICKINTERVAL is set, don't allow MAJOR to override it.
      if (nmajor eq -1 && tickintervalCurr eq 0) then begin
        self._oAxis->SetProperty, MAJOR=N_ELEMENTS(tickText)
        nmajor = N_ELEMENTS(tickText)
      endif
      
      if obj_valid(oAxisText) then begin
        if OBJ_VALID(oAxisText) then begin
          oAxisText->GetProperty, STRINGS=strings
          PTR_FREE, self._pTickvalues
          self._oAxis->GetProperty, TICKVALUES = tickValuesNew
          self._pTickvalues = PTR_NEW(tickValuesNew)
        endif
      endif
      
      ; We will update the ticktext in _VerifyTicktext below.
      type = Size(tickText,/TYPE)
      if (type eq 7) then begin  ; string array
        Ptr_Free, self._pTicktext
        self._pTicktext = Ptr_New(Tex2IDL(tickText))
      endif else if (type eq 11) then begin  ; IDLgrText objref
        self._oUserText = tickText
      endif
      
    endelse
  endif
  
  ; If the color was set on the axis lines, and the user hasn't set
  ; the text color manually, then also update the text color on the
  ; property sheet.
  if (N_ELEMENTS(color) gt 0) then begin
    if (ISA(color, 'STRING') || N_ELEMENTS(color) eq 1) then $
      style_convert, color[0], COLOR=color
      
    ; Set the new axis color
    self->IDLitVisualization::SetProperty, COLOR=color
    
    ; Keep the text color and the axis color synchronized
    ; if necessary.
    self->GetProperty, TEXT_COLOR=txtColor
    self._oAxis->GetProperty, $
      USE_TEXT_COLOR=useColor
    if (~useColor) then $
      self->SetProperty, TEXT_COLOR=color
      
    ; if we have set color equal to text_color
    ; then synch up color and text_color
    if ARRAY_EQUAL(txtColor,color) then $
      self._oAxis->SetProperty, $
      USE_TEXT_COLOR=0
  endif
  
  ; TEXTBASELINE
  if (N_ELEMENTS(textbaseline) gt 0) then begin
    self._oAxis->SetProperty, TEXTBASELINE=textbaseline
    updateAlignment = 1b
  endif
  
  ; TEXTALIGNMENTS
  if (N_ELEMENTS(textAlignments) gt 0) then begin
    self._oAxis->SetProperty, TEXTALIGNMENTS=textAlignments
    updateAlignment = 1b
  endif
  
  ; TEXTUPDIR.
  if (N_ELEMENTS(textupdir) gt 0) then begin
    self._oAxis->SetProperty, TEXTUPDIR=textupdir
    updateAlignment = 1b
  endif
  
  
  if (ISA(textOrientation)) then begin
    self._textOrientation = textOrientation
    updateAlignment = 1b
  endif
  
  
  ; TEXTPOS.
  if (N_ELEMENTS(textPos) gt 0) then begin
    self._textpos = textpos
    ; We will set the property below, depending upon axes reversal.
    updateAlignment = 1b
  endif
  
  
  if (N_ELEMENTS(axisTicklen)) then begin
    self._ticklen = 0 > axisTicklen < 1
    self->_UpdateAxisTicklen
  endif
  
  
  ; The user-defined range (may be smaller than dataspace range)
  if (N_ELEMENTS(axisRange) gt 0) then begin
    if (~isInit && ~ARRAY_EQUAL(axisRange, self._axisRange)) then begin
      self._axisRange = axisRange[0:1]
      ; Be sure to update the range to reflect the new user-defined range.
      if (~ISA(rangeIn)) then begin
        self->GetProperty, RANGE=rangeCurrent
        rangeIn = rangeCurrent
      endif
    endif
  endif
  
  
  ; The dataspace range corresponding to this axis.
  if N_ELEMENTS(rangeIn) gt 0 then begin
    self->_UpdateRange, rangeIn
    self->_UpdateAxisLocation
    self->_UpdateAxisTicklen
    updateAlignment = 1b
  endif
  
  
  ; Change the axis title.
  if (N_ELEMENTS(axisTitle) ge 1) then begin
    ; Create object if it doesn't already exist.
    if (~OBJ_VALID(self._oTitle)) then begin
      ; Retrieve current text color so we can set the title color.
      self->GetProperty, TEXT_COLOR=currentTextColor
      self._oTitle = OBJ_NEW('IDLgrText', /ENABLE_FORMAT, $
        COLOR=currentTextColor, $
        RECOMPUTE_DIMENSIONS=2, $
        FONT=self._oFont->GetFont())
      self._oAxis->SetProperty, TITLE=self._oTitle
      updateAlignment = 1b
    endif
    self._oTitle->SetProperty, STRINGS=Tex2IDL(axisTitle), UVALUE=axisTitle
  endif
  
  
  if (updateAlignment && ~isInit) then begin
    self->_UpdateTextAlignment, mydirection
  endif
  
  
  ; Retrieve the text color from either the axis or the text.
  if (N_ELEMENTS(textColor) gt 0) then begin
  
    if (ISA(textColor, 'STRING') || N_ELEMENTS(textColor) eq 1) then $
      style_convert, textColor[0], COLOR=textColor
      
    ; Get the axisColor
    self->IDLitVisualization::GetProperty, COLOR=axisColor
    
    ; If the axis color does not match the text color
    ; we are now no longer using the axis color.
    self._oAxis->SetProperty, $
      USE_TEXT_COLOR=~ARRAY_EQUAL(axisColor, textColor)
      
    ; First set all the tick text.
    self._oAxis->GetProperty, TICKTEXT=oText
    for i=0,N_ELEMENTS(oText)-1 do $
      oText[i]->SetProperty, COLOR=textColor
      
    ; Now set the title text.
    if (OBJ_VALID(self._oTitle)) then $
      self._oTitle->SetProperty, COLOR=textColor
  endif
  
  
  ; Recompute text dimensions.
  if (N_ELEMENTS(rangeIn) gt 0) then begin
  
    self._oAxis->GetProperty, TICKTEXT=oText
    
    for i=0,N_ELEMENTS(oText)-1 do begin
      oText[i]->SetProperty, CHAR_DIMENSIONS=[0,0], RECOMPUTE=2
    endfor
    
    if (OBJ_VALID(self._oTitle)) then $
      self._oTitle->SetProperty, CHAR_DIMENSIONS=[0,0], RECOMPUTE=2
      
  endif
  
  if(n_elements(PRIVATE) gt 0)then $
    self->IDLitComponent::SetProperty, PRIVATE=PRIVATE
    
    
  if (N_ELEMENTS(log) gt 0) then begin
    ; don't allow the aggregated property exposed to the
    ; user to directly set log on the underlying grAxis
    ; Go through the dataspace first to start the process
    self._oAxis->GetProperty, DIRECTION=direction
    oDataSpace = self->GetDataSpace(/UNNORMALIZED)
    logtmp = log
    if (OBJ_VALID(oDataSpace)) then begin
      case direction of
        0: oDataSpace->SetProperty, XLOG=log
        1: oDataSpace->SetProperty, YLOG=log
        2: oDataSpace->SetProperty, ZLOG=log
        else:
      endcase
      ; If setting the LOG failed, just set it directly on ourself.
      ; This can happen for axes within annotations like colorbars.
      if (logtmp ne log) then self._oAxis->SetProperty, LOG=logtmp
    endif
  endif
  
  if(N_ELEMENTS(normLocation) gt 0) then begin
    self._normLocation = normLocation
  end
  
  ; We need to handle MAJOR/MINOR manually, because they have
  ; a special "-1" value which forces IDL to compute defaults.
  ; Unfortunately, IDLgrAxis::GetProperty just returns the actual
  ; number of major (or minor) ticks, regardless of whether it
  ; computed them or the user set the number. So keep a flag
  ; indicating whether they have been set by the user or not.
  if (N_ELEMENTS(major) gt 0) then begin
    ; If user has their own ticktext, temporarily disable it
    ; to avoid warnings. We will call _VerifyTicktext later.
    if (Obj_Valid(self._oUserText)) then begin
      self._oAxis->SetProperty, TICKTEXT=Obj_New()
    endif
    self._oAxis->SetProperty, MAJOR=major
    self._oAxisShadow->SetProperty, MAJOR=major
    ; Turn off/on MAJOR-has-been-set flag.
    if (major eq -1) then $
      self._majorminor and= (not 1b) $  ; MAJOR was reset
    else $
      self._majorminor or= 1b   ; MAJOR set to a value
  endif
  
  if (N_ELEMENTS(minor) gt 0) then begin
    self._oAxis->SetProperty, MINOR=minor
    ; Turn off/on MINOR-has-been-set flag.
    if (minor eq -1) then $
      self._majorminor and= (not 2b) $  ; MINOR was reset
    else $
      self._majorminor or= 2b   ; MINOR set to a value
  endif
  
  if n_elements(xcoordconv) ne 0 then begin
    self._oAxis->SetProperty,XCOORD_CONV=xcoordconv
  endif
  
  if n_elements(ycoordconv) ne 0 then begin
    self._oAxis->SetProperty,YCOORD_CONV=ycoordconv
  endif
  
  if n_elements(zcoordconv) ne 0 then begin
    self._oAxis->SetProperty,ZCOORD_CONV=zcoordconv
  endif
  
  self->_VerifyTicktext
  
  ; Only pass on those properties which affect the "shadow" axis.
  if (N_ELEMENTS(_extra) gt 0) then $
    self._oAxisShadow->SetProperty, $
    _EXTRA=['EXACT', 'EXTEND', 'GRIDSTYLE', $
    'LOCATION', 'TICKINTERVAL']
    
    
  ; Set superclass properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisualization::SetProperty, _EXTRA=_extra
    
    
end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLitVisAxis::GetXYZRange
;
; PURPOSE:
;   This function method overrides the _IDLitVisualization::GetXYZRange
;   function, taking into the tick labels.
;
; CALLING SEQUENCE:
;   Success = Obj->[_IDLitVisualization::]GetXYZRange( $
;    xRange, yRange, zRange [, /NO_TRANSFORM])
;
; ARGUMENTS
;    xRange:   Set this argument to a named variable that upon return
;       will contain a two-element vector, [xmin, xmax], representing the
;       X range of the objects that impact the ranges.
;    yRange:   Set this argument to a named variable that upon return
;       will contain a two-element vector, [ymin, ymax], representing the
;       Y range of the objects that impact the ranges.
;    zRange:   Set this argument to a named variable that upon return
;       will contain a two-element vector, [zmin, zmax], representing the
;       Z range of the objects that impact the ranges.
;
; KEYWORD PARAMETERS:
;    NO_TRANSFORM:  Set this keyword to indicate that this Visualization's
;       model transform should not be applied when computing the XYZ ranges.
;       By default, the transform is applied.
;
;    DATA:
;       Handle the DATA keyword of the superclass.  No change in behavior
;       is required in this method.
;
;
;-
function IDLitVisAxis::GetXYZRange, $
  outxRange, outyRange, outzRange, $
  DATA=data, $
  NO_TRANSFORM=noTransform
  
  compile_opt idl2, hidden
  
  ; Grab the transformation matrix.
  if (not KEYWORD_SET(noTransform)) then $
    self->IDLgrModel::GetProperty, TRANSFORM=transform
    
  self._oAxis->GetProperty, TICKTEXT=oText, $
    XRANGE=xRange, YRANGE=yRange, ZRANGE=zRange, $
    XCOORD_CONV=xcc, YCOORD_CONV=ycc, ZCOORD_CONV=zcc
    
  ; Apply coordinate conversion.
  xRange = xRange * xcc[1] + xcc[0]
  yRange = yRange * ycc[1] + ycc[0]
  zRange = zRange * zcc[1] + zcc[0]
  
  ; Apply coordinate conversion.
  self->_IDLitVisualization::_AccumulateXYZRange, 0, $
    outxRange, outyRange, outzRange, $
    xRange, yRange, zRange, $
    TRANSFORM=transform
    
  for i=0,N_ELEMENTS(oText)-1 do begin
  
    oText[i]->GetProperty, $
      XRANGE=xRange, YRANGE=yRange, ZRANGE=zRange
      
    ; Apply coordinate conversion.
    xRange = xRange * xcc[1] + xcc[0]
    yRange = yRange * ycc[1] + ycc[0]
    zRange = zRange * zcc[1] + zcc[0]
    
    self->_IDLitVisualization::_AccumulateXYZRange, 1, $
      outxRange, outyRange, outzRange, $
      xRange, yRange, zRange, $
      TRANSFORM=transform
      
  endfor
  
  return, 1
end


;----------------------------------------------------------------------------
; Purpose:
;   Override the superclass' method. We keep our selection visual in sync
;   with our visualization using SetProperty, so we don't need to
;   do any updates here.
;
pro IDLitVisAxis::UpdateSelectionVisual
  compile_opt idl2, hidden
  ; Do nothing.
end


;----------------------------------------------------------------------------
; IDLitVisAxis::OnDataRangeChange
;
; Purpose:
;   This procedure method handles notification that the data range
;   has changed.
;
; Arguments:
;   oSubject: A reference to the object sending notification of the
;     data range change.
;   XRange:  The new xrange, [xmin, xmax]
;   YRange:  The new yrange, [ymin, ymax]
;   ZRange:  The new zrange, [zmin, zmax]
;
pro IDLitVisAxis::OnDataRangeChange, oSubject, XRange, YRange, ZRange
  compile_opt idl2, hidden
  
  if (self._dataPosition) then begin
    self->_IDLitVisualization::ClipToDataRange, XRange, YRange, ZRange
  endif
end


;---------------------------------------------------------------------------
; IDLitVisAxis::OnViewportChange
;
; Purpose:
;   This procedure method handles notification that the viewport
;   has changed.
;
; Arguments:
;   oSubject: A reference to the object sending notification of the
;     viewport change.
;
;   oDestination: A reference to the destination in which the view
;     appears.
;
;   viewportDims: A 2-element vector, [w,h], representing the new
;     width and height of the viewport (in pixels).
;
;   normViewDims: A 2-element vector, [w,h], representing the new
;     width and height of the visible view (normalized relative to
;     the virtual canvas).
;
pro IDLitVisAxis::OnViewportChange, oSubject, oDestination, $
  viewportDims, normViewDims
  
  compile_opt idl2, hidden
  
  ; Check if destination zoom factor or normalized viewport has changed.
  ; If so, update the corresponding font properties.
  self._oFont->GetProperty, FONT_ZOOM=fontZoom, FONT_NORM=fontNorm
  if (OBJ_VALID(oDestination)) then $
    oDestination->GetProperty, CURRENT_ZOOM=zoomFactor $
  else $
    zoomFactor = 1.0
    
  normFactor = MIN(normViewDims)
  
  if ((fontZoom ne zoomFactor) || $
    (fontNorm ne normFactor)) then $
    self._oFont->SetProperty, FONT_ZOOM=zoomFactor, FONT_NORM=normFactor
    
  ; Allow superclass to notify all children.
  self->_IDLitVisualization::OnViewportChange, oSubject, oDestination, $
    viewportDims, normViewDims
end

;---------------------------------------------------------------------------
; IDLitVisAxis::OnViewZoom
;
; Purpose:
;   This procedure method handles notification that the view zoom factor
;   has changed
;
; Arguments:
;   oSubject: A reference to the object sending notification of the
;     view zoom factor change.
;
;   oDestination: A reference to the destination in which the view
;     appears.
;
;   viewZoom: The new zoom factor for the view.
;
pro IDLitVisAxis::OnViewZoom, oSubject, oDestination, viewZoom

  compile_opt idl2, hidden
  
  ; Check if view zoom factor has changed.  If so, update the font.
  self._oFont->GetProperty, VIEW_ZOOM=fontViewZoom
  
  if (fontViewZoom ne viewZoom) then $
    self._oFont->SetProperty, VIEW_ZOOM=viewZoom
    
  ; Allow superclass to notify all children.
  self->_IDLitVisualization::OnViewZoom, oSubject, oDestination, $
    viewZoom
end

;---------------------------------------------------------------------------
pro IDLitVisAxis::Translate, tx, ty, tz, $
  KEYMODS=keymods, $        ; undocumented keyword
  KEYVALUE=KeyValue, $      ; undocumented keyword
  PROBE_MESSAGE=probeMsg, $ ; undocumented keyword
  _REF_EXTRA=_extra
  
  compile_opt idl2, hidden, logical_predicate
  
  ; We weren't called with our special KEYMODS keyword
  ; probably being called from a macro - just provide
  ; default values
  if (N_ELEMENTS(keymods) eq 0) then $
    keymods=0
  if (N_ELEMENTS(KeyValue) eq 0) then $
    KeyValue=0
    
  self._oAxis->GetProperty, $
    DIRECTION=direction, $
    LOCATION=location
    
  oDataSpace = self->GetDataspace()
  is3D = oDataSpace->Is3D()
  
  ; <Ctrl+Shift> key changes the translation direction,
  ; or if the up-down arrow keys are used.
  switchdir = (KeyMods eq 3) || (KeyValue eq 7) || (KeyValue eq 8)
  
  case direction of
    0: tr = is3D && switchdir ? [0, 0, tz] : [0, ty, 0]
    1: tr = is3D && switchdir ? [0, 0, tz] : [tx, 0, 0]
    2: tr = switchdir ? [0, ty, 0] : [tx, 0, 0]
  endcase
  
  location += tr
  
  ; Call our method to actually set the location.
  self->IDLitVisAxis::SetProperty, LOCATION=location
  
  ; Fill in our probe message.
  case direction of
    0: probeMsg = is3D ? $
      STRING(location[1:2], FORMAT='(%"Y=%g, Z=%g")') : $
      STRING(location[1], FORMAT='(%"Y=%g")')
    1: probeMsg = is3D ? $
      STRING(location[0:2:2], FORMAT='(%"X=%g, Z=%g")') : $
      STRING(location[0], FORMAT='(%"X=%g")')
    2: probeMsg = STRING(location[0:1], FORMAT='(%"X=%g, Y=%g")')
  endcase
  
end


;---------------------------------------------------------------------------
pro IDLitVisAxis::Scale, sx, sy, sz, $
  _REF_EXTRA=_extra
  
  compile_opt idl2, hidden, logical_predicate
  
  ; Swallow scale calls. We don't want to allow user to scale axes.
  ; Normally, the scale would be disabled if an axis is selected.
  ; However, if it is part of a multiple selection, then the scale
  ; can get called on all selected items, including axes.
  
end


;----------------------------------------------------------------------------
;+
; IDLitVisAxis__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisAxis object.
;
;-
pro IDLitVisAxis__Define

  compile_opt idl2, hidden
  
  struct = { IDLitVisAxis,           $
    inherits IDLitVisualization, $
    _normLocation: [0d, 0d, 0d], $
    _coordTransform: [0d, 0d], $
    _axisRange: [0d, 0d], $
    _dataPosition: 0b, $
    _majorminor: 0b, $
    _ticklen: 0d, $
    _textOrientation: 0d, $
    _textpos: 0, $
    _tickdir: 0, $
    _tickDefinedFormat: 0L, $
    _lastTickFormat: ptr_new(), $
    _pTickvalues:PTR_NEW(), $
    _oAxis: OBJ_NEW(),           $
    _oAxisShadow: OBJ_NEW(),     $
    _fakeAxis: OBJ_NEW(), $
    _oMySelectionVisual: OBJ_NEW(), $
    _oTitle: OBJ_NEW(),          $
    _oRevTitle: OBJ_NEW(),       $
    _oFont: OBJ_NEW(), $
    _pTicktext: Ptr_New(), $
    _oUserText: Obj_New() $   ; not ours, do not destroy in Cleanup
  }
end
