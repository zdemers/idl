; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitannotatelegend__define.pro#1 $
;
; Copyright (c) 2011-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
; Purpose:
;   Create a legend annotation.
;

;---------------------------------------------------------------------------
function IDLitAnnotateLegend::Init, strType, _EXTRA=_extra

    compile_opt idl2, hidden

    ; Init our superclass
    status = self->IDLitManipAnnotation::Init(NAME='Legend', $
        KEYBOARD_EVENTS=0, $
        /TRANSIENT_DEFAULT, _EXTRA=_extra)

    if (status eq 0)then return, 0

    return, 1
end


;---------------------------------------------------------------------------
pro IDLitAnnotateLegend::Cleanup
  compile_opt idl2, hidden
  Obj_Destroy, self.oLegend
  self->IDLitManipAnnotation::Cleanup
end


;---------------------------------------------------------------------------
function IDLitAnnotateLegend::DoAction, oTool

  compile_opt idl2, hidden

  catch, iErr
  if (iErr ne 0) then begin
    catch, /CANCEL
    Message, /RESET
    return, 0
  endif
  legend = LEGEND()
  return, Isa(legend)
end


;---------------------------------------------------------------------------
function IDLitAnnotateLegend::QueryAvailability, oTool, selTypes

  compile_opt idl2, hidden

  if (~Isa(oTool)) then return, 0
  ; Trick the Legend class into creating itself without creating a legend.
  if (~Isa(self.oLegend)) then self.oLegend = Obj_New('Legend', self)
  ; See if we have any valid legend targets.
  oTargets = self.oLegend->_FindTargets(oTool)
  
  return, N_Elements(oTargets) gt 0
end


;---------------------------------------------------------------------------
pro IDLitAnnotateLegend__Define

    compile_opt idl2, hidden

    ; Just define this bad boy.
    void = {IDLitAnnotateLegend, $
            inherits IDLitManipAnnotation, $
            oLegend: Obj_New() $
           }

end
