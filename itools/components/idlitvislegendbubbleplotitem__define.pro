; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvislegendbubbleplotitem__define.pro#1 $
;
; Copyright (c) 2010-2012, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
; Purpose:
;   The IDLitVisLegendBubblePlotItem class is the component wrapper
;   for the barplot item subcomponent of the legend.
;
;

;----------------------------------------------------------------------------
; Purpose:
;   Initialize this component
;
function IDLitVisLegendBubblePlotItem::Init, _REF_EXTRA=_extra
  compile_opt idl2, hidden
  
  ; Initialize superclass
  if (~self->IDLitVisLegendItem::Init( $
    NAME="Bubbleplot Legend Item", $
    DESCRIPTION="A Bubbleplot Legend Entry", $
    _EXTRA=_extra)) then $
    return, 0
    
  return, 1 ; Success
  
end


;----------------------------------------------------------------------------
; IDLitVisLegendBubblePlotItem::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisLegendBubblePlotItem::Restore
  compile_opt idl2, hidden
  
  ; Call superclass restore.
  self->IDLitVisLegendItem::Restore
  
end


;----------------------------------------------------------------------------
pro IDLitVisLegendBubblePlotItem::RecomputeLayout, WITHIN_DRAW=withinDraw
  compile_opt idl2, hidden
  
  oTool = self->GetTool()
  self->GetProperty, PARENT=oParent
  if (OBJ_VALID(oTool) && OBJ_VALID(oParent)) then begin
    oWindow = oTool->GetCurrentWindow()
    if (~OBJ_VALID(oWindow)) then $
      return
    textDimensions = oWindow->GetTextDimensions(self._oText)
    if (OBJ_VALID(self._oPolygon)) then begin
      self->_CalculateBubble, self._shaded, self._color, $
        DATA=data, LINEDATA=lineData, VERT=vertColors, CONN=polygons
      scale = textDimensions[1]*0.4 ; made up factor for a nice sized bubble
      data *= scale
      lineData *=scale
      midPoint = self._sampleWidth/2.
      data[0,*] += midPoint
      lineData[0,*] += midPoint
      ; Add an extra point in the middle in case there is a plot symbol.
      self._oPolygon->SetProperty, DATA=data, VERT_COLORS=vertColors, $
                      POLYGONS=polygons
      self._oPolyline->SetProperty, DATA=lineData
      self._oText->SetProperty, $
        LOCATIONS=[[self._sampleWidth+self._horizSpacing, 0]]
    endif
  endif
  
  self->UpdateSelectionVisual
  
  ; Update the upper level legend
  self->GetProperty, PARENT=oLegend
  if OBJ_VALID(oLegend) then oLegend->RecomputeLayout
  
end


;----------------------------------------------------------------------------
PRO IDLitVisLegendBubblePlotItem::BuildItem
  compile_opt idl2, hidden
  
  ; Call our superclass first to set our properties.
  self->IDLitVisLegendItem::BuildItem
  
  self->AddOnNotifyObserver, self->GetFullIdentifier(), $
    self._oVisTarget->GetFullIdentifier()
    
  self._oVisTarget->GetProperty, $
    COLOR=color, $
    LINECOLOR=lineColor, $
    LINETHICK=lineThick, $
    LINESTYLE=lineStyle, $
    FILLED=filled, $
    SHADED=shaded, $
    BORDER=border, $
    TRANSPARENCY=transparency, $
    RGB_TABLE=rgbTable, $
    NAME=name
    
  if (n_elements(name) eq 0) then $
    name=''

  if ((SIZE(color, /N_DIMENSIONS) eq 1) && (N_ELEMENTS(color) ne 3)) then $
    color = rgbTable[*,color]
    
  self._shaded = shaded
  self._color = color[0:2]

  self._oPolygon = OBJ_NEW('IDLgrPolygon', $
    /ANTIALIAS, $
    ALPHA_CHANNEL=1-transparency/100., $
    NAME=name, $
    COLOR=color[0:2], $
    HIDE=~(KEYWORD_SET(filled) || KEYWORD_SET(shaded)), $
    /PRIVATE)
  self->Add, self._oPolygon

  self._oPolyline = OBJ_NEW('IDLgrPolyline', $
    /ANTIALIAS, $
    ALPHA_CHANNEL=1-transparency/100., $
    COLOR=lineColor[0:2], $
    NAME=name, $
    LINESTYLE=lineStyle, $
    THICK=lineThick, $
    HIDE=~border, $
    /PRIVATE)
  self->Add, self._oPolyline
  
  self._oText->SetProperty, STRINGS=name
  
  self->RecomputeLayout
  
end


;----------------------------------------------------------------------------
; IIDLDataObserver Interface
;----------------------------------------------------------------------------
;;---------------------------------------------------------------------------
;; IDLitVisLegend::OnNotify
;;
;;
;;  strItem - The item being observed
;;
;;  strMessage - What happend. For properties this would be
;;               "SETPROPERTY"
;;
;;  strUser    - Message related data. For SETPROPERTY, this is the
;;               property that changed.
;;
;;
pro IDLitVisLegendBubblePlotItem::OnNotify, strItem, StrMessage, strUser
  compile_opt idl2, hidden
  
  ; Let superclass handle other messages.
  if (strMessage ne 'SETPROPERTY') then begin
    ; Call our superclass.
    self->IDLitVisLegendItem::OnNotify, $
      strItem, StrMessage, strUser
    return
  endif
  
  oTool = self->GetTool()
  oSubject=oTool->GetByIdentifier(strItem)
  
  switch STRUPCASE(strUser) OF
  
    'BORDER' :
    'LINECOLOR' :
    'LINESTYLE' :
    'LINETHICK' : begin
      oSubject->GetProperty, BORDER=border, LINECOLOR=linecolor, $
        LINESTYLE=linestyle, LINETHICK=linethick 
        self._oPolyline->SetProperty, HIDE=~border, COLOR=linecolor[0:2], $
          THICK=linethick, LINESTYLE=linestyle   
      break
    end

    'FILLED' :
    'SHADED' :
    'RGB_TABLE' :
    'COLOR' : begin
      oSubject->GetProperty, COLOR=color, RGB_TABLE=rgbTable, $
        FILLED=filled, SHADED=shaded 
      if ((SIZE(color, /N_DIMENSIONS) eq 1) && (N_ELEMENTS(color) ne 3)) then $
        color = rgbTable[*,color]
      self._shaded = shaded
      self._color = color[0:2]
      self->_CalculateBubble, self._shaded, self._color, $
        DATA=data, VERT=vertColors, CONN=polygons
      self._oPolygon->SetProperty, COLOR=color, VERT_COLORS=vertColors, $
        HIDE=~(KEYWORD_SET(filled) || KEYWORD_SET(shaded)), POLYGONS=polygons
      break
    end
    
    'TRANSPARENCY' : begin
      oSubject->GetProperty, TRANSPARENCY=transparency
      if (N_ELEMENTS(transparency) gt 0) then begin
        alpha = 1-transparency/100.
        self._oPolygon->SetProperty, ALPHA_CHANNEL=alpha
        self._oPolyline->SetProperty, ALPHA_CHANNEL=alpha
      endif
      break
    end
    
    else : ; ignore unknown parameters
    
  endswitch
  
end


;----------------------------------------------------------------------------
;+
; IDLitVisLegendBubblePlotItem::_CalculateBubble
;
; PURPOSE:
;    Defines the object structure for an IDLitVisLegendBubblePlotItem object.
;
;-
pro IDLitVisLegendBubblePlotItem::_CalculateBubble, shaded, color, $
                                  CONN=conn, VERT=vert, DATA=data, $
                                  LINEDATA=linedata
  compile_opt idl2, hidden
  
  ; Define basic circle
  n = 42
  ind = INDGEN(n)
  ramp = DINDGEN(n)/(n-1)*2*!pi
  cirx = SIN(ramp)
  ciry = COS(ramp)
  linedata = TRANSPOSE([[cirx],[ciry]])

  if (~KEYWORD_SET(shaded)) then begin
    conn = 0
    vert = 0
    data = linedata
    return
  endif
  
  conn = (TRANSPOSE(REFORM([REPLICATE(3,n),ind,ind[shift(ind,-1)], $
                            REPLICATE(n,n)],n,4,/OVERWRITE)))[*]
  cntr = [-0.3,0.3]
  data = [[linedata],[cntr]]
  ; Shading goes from a bright spot of color+141 to a dark edge of color*0.6
  bright = BYTE(FIX(color)+141 < 255)
  dark = BYTE(0.8 * color)
  dst = SQRT(((cirx)-cntr[0])^2+((ciry)-cntr[1])^2)
  mn = MIN(dst)
  diff = MAX(dst)-mn
  vert = BYTARR(3,n+1)
  for j=0,n-1 do begin
    vert[*,j] = color - ((dst[j]-mn)/diff * (color-dark))
  endfor
  vert[*,n] = bright
  
end


;----------------------------------------------------------------------------
;+
; IDLitVisLegendBubblePlotItem__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisLegendBubblePlotItem object.
;
;-
pro IDLitVisLegendBubblePlotItem__Define
  compile_opt idl2, hidden
  
  struct = {IDLitVisLegendBubblePlotItem, $
            inherits IDLitVisLegendItem, $
            _oPolygon: OBJ_NEW(), $
            _oPolyline: OBJ_NEW(), $
            _shaded: 0b, $
            _color: [0b,0b,0b] $
           }
end
