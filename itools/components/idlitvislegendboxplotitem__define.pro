; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvislegendboxplotitem__define.pro#1 $
;
; Copyright (c) 2010-2012, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
; Purpose:
;   The IDLitVisLegendBoxPlotItem class is the component wrapper
;   for the barplot item subcomponent of the legend.
;
;

;----------------------------------------------------------------------------
; Purpose:
;   Initialize this component
;
function IDLitVisLegendBoxPlotItem::Init, _REF_EXTRA=_extra
  compile_opt idl2, hidden
  
  ; Initialize superclass
  if (~self->IDLitVisLegendItem::Init( $
    NAME="Boxplot Legend Item", $
    DESCRIPTION="A Boxplot Legend Entry", $
    _EXTRA=_extra)) then $
    return, 0
    
  self._boxConn = [5,0,1,2,3,0]
  self._medianConn = [2,4,5]
  self._whiskerConn = [2,10,11,2,12,13]
  self._endcapConn = [2,6,7,2,8,9]

  return, 1 ; Success
  
end


;----------------------------------------------------------------------------
; IDLitVisLegendBoxPlotItem::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisLegendBoxPlotItem::Restore
  compile_opt idl2, hidden
  
  ; Call superclass restore.
  self->IDLitVisLegendItem::Restore
  
  ; Call ::Restore on each aggregated ItVis object
  ; to ensure any new properties are registered.  Also
  ; call its UpdateComponentVersion method so that this
  ; will not be attempted later
  if (OBJ_VALID(self._oItSymbol)) then begin
    self._oItSymbol->Restore
    self._oItSymbol->UpdateComponentVersion
  endif
  
end


;----------------------------------------------------------------------------
pro IDLitVisLegendBoxPlotItem::RecomputeLayout, WITHIN_DRAW=withinDraw
  compile_opt idl2, hidden
  
  oTool = self->GetTool()
  self->GetProperty, PARENT=oParent
  if (OBJ_VALID(oTool) && OBJ_VALID(oParent)) then begin
    oWindow = oTool->GetCurrentWindow()
    if (~OBJ_VALID(oWindow)) then $
      return
    textDimensions = oWindow->GetTextDimensions(self._oText)
    if (OBJ_VALID(self._oPolygon)) then begin
      xdata = self._sampleWidth
      yLow = -0.4*textDimensions[1]
      yHigh = 0.4*textDimensions[1]
      xPoints = xdata*(FINDGEN(5)/4)
      self._oPolygon->SetProperty, $
        DATA=[[xPoints[1],yLow], [xPoints[1],yHigh], [xPoints[2],yHigh], $
              [xPoints[2],yLow], [xPoints[2],yLow], [xPoints[2],yHigh], $
              [xPoints[3],yHigh], [xPoints[3],yLow]]
      self._oPolyline->SetProperty, $
        DATA=[[xPoints[1],yHigh], [xPoints[3],yHigh], [xPoints[3],yLow], $
              [xPoints[1],yLow], [xPoints[2],yLow], [xPoints[2],yHigh], $
              [xPoints[0],yLow], [xPoints[0],yHigh], [xPoints[4],yLow], $
              [xPoints[4],yHigh], [xPoints[0],0], [xPoints[1],0], $
              [xPoints[3],0], [xPoints[4],0]]
      self._oText->SetProperty, $
        LOCATIONS=[[self._sampleWidth+self._horizSpacing, 0]]
    endif
  endif
  
  self->UpdateSelectionVisual
  
  ; Update the upper level legend
  self->GetProperty, PARENT=oLegend
  if OBJ_VALID(oLegend) then oLegend->RecomputeLayout
  
end


;----------------------------------------------------------------------------
PRO IDLitVisLegendBoxPlotItem::BuildItem
  compile_opt idl2, hidden
  
  ; Call our superclass first to set our properties.
  self->IDLitVisLegendItem::BuildItem
  
  self->AddOnNotifyObserver, self->GetFullIdentifier(), $
    self._oVisTarget->GetFullIdentifier()
    
  self._oVisTarget->GetProperty, $
    COLOR=color, $
    FILL_COLOR=fillColor, $
    LOWER_COLOR=lowerColor, $
    MEDIAN=median, $
    BOX=box, $
    WHISKERS=whiskers, $
    ENDCAPS=endcaps, $
    _FILLED=filled, $
    _LOWER_FILLED=lowerFilled, $
    THICK=thick, $
    TRANSPARENCY=transparency, $
    NAME=name, $
    /UNDOCUMENTED
    
  if (n_elements(name) eq 0) then $
    name=''

  if (lowerFilled) then begin
    vertColors = [[lowerColor], [lowerColor], [lowerColor], [lowerColor], $
                  [fillColor], [fillColor], [fillColor], [fillColor]]
  endif else begin
    vertColors = [[fillColor], [fillColor], [fillColor], [fillColor], $
                  [fillColor], [fillColor], [fillColor], [fillColor]]
  endelse
  
  self._oPolygon = OBJ_NEW('IDLgrPolygon', $
    DATA=[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]], $
    ALPHA_CHANNEL=1-transparency/100., $
    NAME=name, $
    VERT_COLORS=vertColors, $
    POLYGONS=[5,0,1,2,3,0,5,4,5,6,7,4], $
    HIDE=~filled, $
    /PRIVATE)
  self->Add, self._oPolygon

  polylines = [!NULL]
  if (box) then polylines = [polylines, self._boxConn]
  if (median) then polylines = [polylines, self._medianConn]
  if (whiskers) then polylines = [polylines, self._whiskerConn]
  if (endcaps) then polylines = [polylines, self._endcapConn]
  if (N_ELEMENTS(polylines) eq 0) then polylines = [0]
  
  self._oPolyline = OBJ_NEW('IDLgrPolyline', $
    DATA=[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],$
          [0,0],[0,0],[0,0],[0,0]], $
    ALPHA_CHANNEL=1-transparency/100., $
    COLOR=color, $
    NAME=name, $
    POLYLINES=polylines, $
    THICK=thick, $
    /PRIVATE)
  self->Add, self._oPolyline
  
  self._oText->SetProperty, STRINGS=name
  
  self->RecomputeLayout
  
end


;----------------------------------------------------------------------------
; IIDLDataObserver Interface
;----------------------------------------------------------------------------
;;---------------------------------------------------------------------------
;; IDLitVisLegend::OnNotify
;;
;;
;;  strItem - The item being observed
;;
;;  strMessage - What happend. For properties this would be
;;               "SETPROPERTY"
;;
;;  strUser    - Message related data. For SETPROPERTY, this is the
;;               property that changed.
;;
;;
pro IDLitVisLegendBoxPlotItem::OnNotify, strItem, StrMessage, strUser
  compile_opt idl2, hidden
  
  ; Let superclass handle other messages.
  if (strMessage ne 'SETPROPERTY') then begin
    ; Call our superclass.
    self->IDLitVisLegendItem::OnNotify, $
      strItem, StrMessage, strUser
    return
  endif
  
  oTool = self->GetTool()
  oSubject=oTool->GetByIdentifier(strItem)
  
  switch STRUPCASE(strUser) OF
  
    'FILL_COLOR' :
    'LOWER_COLOR' : begin
      oSubject->GetProperty, /UNDOCUMENTED, FILL_COLOR=fillColor, $
        LOWER_COLOR=lowerColor, _FILLED=filled, _LOWER_FILLED=lowerFilled
      if (lowerFilled) then begin
        vertColors = [[lowerColor], [lowerColor], [lowerColor], [lowerColor], $
                      [fillColor], [fillColor], [fillColor], [fillColor]]
      endif else begin
        vertColors = [[fillColor], [fillColor], [fillColor], [fillColor], $
                      [fillColor], [fillColor], [fillColor], [fillColor]]
      endelse
      self._oPolygon->SetProperty, VERT_COLORS=vertColors, HIDE=~filled   
      break
    end
    
    'BOX' :
    'MEDIAN' :
    'WHISKERS' :
    'ENDCAPS' :
    'COLOR' :
    'THICK': begin
      oSubject->GetProperty, THICK=thick, COLOR=color, $
        BOX=box, MEDIAN=median, WHISKERS=whiskers, ENDCAPS=endcaps
      polylines = [!NULL]
      if (box) then polylines = [polylines, self._boxConn]
      if (median) then polylines = [polylines, self._medianConn]
      if (whiskers) then polylines = [polylines, self._whiskerConn]
      if (endcaps) then polylines = [polylines, self._endcapConn]
      if (N_ELEMENTS(polylines) eq 0) then polylines = [0]
      self._oPolyline->SetProperty, THICK=thick, COLOR=color, $
        POLYLINES=polylines
      break
    end
    
    'TRANSPARENCY': begin
      oSubject->GetProperty, TRANSPARENCY=transparency
      if (N_ELEMENTS(transparency) gt 0) then begin
        alpha = 1-transparency/100.
        self._oPolygon->GetProperty, VERT_COLORS=vertColors
        vertColors[3,*] = alpha * 255b
        self._oPolygon->SetProperty, VERT_COLORS=vertColors
        self._oPolyline->SetProperty, ALPHA_CHANNEL=alpha
      endif
      break
    end
    
    else : ; ignore unknown parameters
    
  endswitch
  
end


;----------------------------------------------------------------------------
;+
; IDLitVisLegendBoxPlotItem__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisLegendBoxPlotItem object.
;
;-
pro IDLitVisLegendBoxPlotItem__Define
  compile_opt idl2, hidden
  
  struct = {IDLitVisLegendBoxPlotItem, $
            inherits IDLitVisLegendItem, $
            _oPolygon: OBJ_NEW(), $
            _oPolyline: OBJ_NEW(), $
            _boxConn: [5,0,0,0,0,0], $
            _medianConn: [2,0,0], $
            _whiskerConn: [2,0,0,2,0,0], $
            _endcapConn: [2,0,0,2,0,0] $
           }
end
