; $Id: //depot/InDevelopment/scrums/ENVI_Yukon/idl/idldir/lib/itools/components/IDLitVisLegendVectorItem__define.pro#6 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
; Purpose:
;   The IDLitVisLegendVectorItem class is the component wrapper
;   for the vector item subcomponent of the legend.
;
; Modification history:
;     Written by:   CT, Feb 2012.
;




;----------------------------------------------------------------------------
;pro IDLitVisLegendVectorItem::_RegisterProperties, $
;    UPDATE_FROM_VERSION=updateFromVersion
;
;    compile_opt idl2, hidden
;
;    registerAll = ~KEYWORD_SET(updateFromVersion)
;
;    if (registerAll) then begin
;
;
;    endif
;
;end


;----------------------------------------------------------------------------
; Purpose:
;   Initialize this component
;
function IDLitVisLegendVectorItem::Init, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Initialize superclass
    if (~self->IDLitVisLegendItem::Init( $
        NAME="Vector Legend Item", $
        DESCRIPTION="A Vector Legend Entry", $
        _EXTRA=_extra)) then $
        return, 0

;    self->IDLitVisLegendVectorItem::_RegisterProperties

    return, 1 ; Success
end




;----------------------------------------------------------------------------
; IDLitVisLegendVectorItem::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisLegendVectorItem::Restore
    compile_opt idl2, hidden

    ; Call superclass restore.
    self->IDLitVisLegendItem::Restore

;    ; Register new properties.
;    self->IDLitVisLegendVectorItem::_RegisterProperties, $
;        UPDATE_FROM_VERSION=self.idlitcomponentversion
end


;----------------------------------------------------------------------------
pro IDLitVisLegendVectorItem::RecomputeLayout, WITHIN_DRAW=withinDraw

  compile_opt idl2, hidden

  oTool = self->GetTool()
  self->GetProperty, PARENT=oParent
  if (~OBJ_VALID(oTool) || ~OBJ_VALID(oParent)) then return
  oWindow = oTool->GetCurrentWindow()
  if (~OBJ_VALID(oWindow)) then $
      return

  self._oText->GetProperty, STRINGS=label

  oVector = self._oVisTarget
  oVector->GetProperty, ARROW_THICK=shaftThick, $
    HEAD_SIZE=headSize, $
    LENGTH_SCALE=lengthScale, $
    LENGTHSCALEX=lengthScaleX, $
    MAX_MAGNITUDE=maxMagnitude, $
    MEAN_MAGNITUDE=meanMagnitude, $
    MIN_MAGNITUDE=minMagnitude, $
    NELEMENTS=nElts
  
  if (nElts eq 0) then nElts = 1
  sampleMagnitude = meanMagnitude

  if (self._sampleMagnitudeStr ne '') then begin
    case (STRUPCASE(self._sampleMagnitudeStr)) of
    'MIN': sampleMagnitude = minMagnitude
    'MAX': sampleMagnitude = maxMagnitude
    'MEAN': sampleMagnitude = meanMagnitude
    else:  ; do nothing
    endcase
    self._sampleMagnitude = sampleMagnitude
  endif else begin
    if (self._sampleMagnitude ne 0) then $
      sampleMagnitude = self._sampleMagnitude
  endelse

  lengthScaleX = lengthScaleX*(sampleMagnitude/maxMagnitude)

  if (~self._bHasLabelText) then begin
    label = STRING(sampleMagnitude, FORMAT='(G0)')
    if (self._units ne '') then $
      label += ' ' + Tex2IDL(self._units)
  endif

  coords = iConvertCoord([0,lengthScaleX],[0,0], /DATA, $
    TARGET=self._oVisTarget, /TO_ANNOTATION_DATA)

  length = coords[0,1] - coords[0,0]

  ; Just cache this width, even though we don't need to save it.
  self._sampleWidth = length

  ydata = 0

  self._oArrow->SetProperty, $
    HEAD_SIZE=(0.1 > 10*headSize*length/lengthScale), $
    THICK=0.5*shaftThick, $
    WITHIN_DRAW=withinDraw

  self._oArrow->_SetData, [0,length], [ydata,ydata], $
    WITHIN_DRAW=withinDraw
  self._oArrow->Reset
  if (self._sampleAngle ne 0) then $
    self._oArrow->Rotate, [0,0,1], self._sampleAngle

  xlen = length*(0.75 + 0.25d*abs(cos(self._sampleAngle*!dtor)))
  self._oText->SetProperty, STRINGS=label, $
    LOCATIONS=[[xlen + self._horizSpacing, 0]]
   
  self->UpdateSelectionVisual

  ; Update the upper level legend
  if (~KEYWORD_SET(withinDraw)) then begin
    self->GetProperty, PARENT=oLegend
    if OBJ_VALID(oLegend) then oLegend->RecomputeLayout
  endif

end


;----------------------------------------------------------------------------
PRO IDLitVisLegendVectorItem::BuildItem

    compile_opt idl2, hidden

    ; Call our superclass first to set our properties.
    self->IDLitVisLegendItem::BuildItem

    self->GetProperty, PARENT=oParent
    if (ISA(oParent, 'IDLitVisLegend')) then begin
      void = oParent->Get(/ALL, /SKIP_PRIVATE, COUNT=nitems)
      if (nitems eq 1) then $
        oParent->SetProperty, SHADOW=0, LINESTYLE=6
    endif

    self->AddOnNotifyObserver, self->GetFullIdentifier(), $
        self._oVisTarget->GetFullIdentifier()

    self._oVisTarget->GetProperty, $
      ARROW_STYLE=arrowStyle, $
      COLOR=color, $
      HEAD_ANGLE=headAngle, $
      HEAD_INDENT=headIndent, $
      TRANSPARENCY=transparency, $
      THICK=linethick

    self._oArrow = OBJ_NEW('IDLitVisArrow', $
      COLOR=color, $
      FILL_BACKGROUND=arrowStyle, $
      HEAD_ANGLE=headAngle, $
      HEAD_INDENT=headIndent, $
      /IMPACTS_RANGE, $
      NAME='Arrow', $
      TRANSPARENCY=transparency, $
      LINE_THICK=linethick)

    self._oText->SetProperty, STRINGS=''
    self->Add, self._oArrow

    self->RecomputeLayout

end


;----------------------------------------------------------------------------
; IIDLDataObserver Interface
;----------------------------------------------------------------------------
;;---------------------------------------------------------------------------
;; IDLitVisLegend::OnNotify
;;
;;
;;  strItem - The item being observed
;;
;;  strMessage - What happend. For properties this would be
;;               "SETPROPERTY"
;;
;;  strUser    - Message related data. For SETPROPERTY, this is the
;;               property that changed.
;;
;;
pro IDLitVisLegendVectorItem::OnNotify, strItem, StrMessage, strUser

   compile_opt idl2, hidden

    if (strMessage eq 'DRAW') then begin
      self->RecomputeLayout, /WITHIN_DRAW
      return
    endif

    if (strMessage eq 'ONDATARANGECHANGE') then begin
      self->RecomputeLayout, /WITHIN_DRAW
      return
    endif

    ; Let superclass handle other messages.
    if (strMessage ne 'SETPROPERTY') then begin
        ; Call our superclass.
        self->IDLitVisLegendItem::OnNotify, $
            strItem, StrMessage, strUser
        return
    endif

    oTool = self->GetTool()
    oSubject=oTool->GetByIdentifier(strItem)

    switch STRUPCASE(strUser) OF

      'ARROW_STYLE': begin
        oSubject->GetProperty, ARROW_STYLE=arrowStyle
        if (ISA(arrowStyle)) then $
          self._oArrow->SetProperty, FILL_BACKGROUND=arrowStyle
        break
        end

; AUTO_COLOR
      'COLOR': begin
        ; Call our superclass just for the COLOR property,
        ; so we can see if the AUTO_TEXT_COLOR property has been set.
        self->IDLitVisLegendItem::OnNotify, strItem, StrMessage, strUser

        oSubject->GetProperty, COLOR=color
        if (ISA(color)) then $
          self._oArrow->SetProperty, COLOR=color
        break
        end

      'HEAD_ANGLE': begin
        oSubject->GetProperty, HEAD_ANGLE=headAngle
        if (ISA(headAngle)) then $
          self._oArrow->SetProperty, HEAD_ANGLE=headAngle
        break
        end

      'HEAD_INDENT': begin
        oSubject->GetProperty, HEAD_INDENT=headIndent
        if (ISA(headIndent)) then $
          self._oArrow->SetProperty, HEAD_INDENT=headIndent
        break
        end

      'ARROW_THICK': ; fall thru
      'HEAD_SIZE': ; fall thru
      'MAX_VALUE': ; fall thru
      'LENGTH_SCALE': begin
        self->RecomputeLayout
        break
        end

      'TRANSPARENCY': begin
        oSubject->GetProperty, TRANSPARENCY=transparency
        if (ISA(transparency)) then $
          self._oArrow->SetProperty, TRANSPARENCY=transparency
        break
        end

      'THICK': begin
        oSubject->GetProperty, THICK=lineThick
        if (ISA(lineThick)) then $
          self._oArrow->SetProperty, LINE_THICK=lineThick
        break
        end


;      'NAME': ;Note that the LABEL_TEXT property of the legend item
;              ;is set on creation to the NAME of the visualization,
;              ;but this is not updated if the vis name is changed
;              ;after creation.  Users may change the text property
;              ;in the legend and this should not be overwritten.

    endswitch

end


;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; IDLitVisLegendVectorItem__Define
;
; PURPOSE:
;    Defines the object structure for an IDLitVisLegendVectorItem object.
;
;-
pro IDLitVisLegendVectorItem__Define

    compile_opt idl2, hidden

    struct = { IDLitVisLegendVectorItem,           $
        inherits IDLitVisLegendItem, $
        _oArrow: OBJ_NEW() $
    }
end
