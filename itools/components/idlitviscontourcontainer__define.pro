; $Id: //depot/InDevelopment/scrums/ENVI_Yukon/idl/idldir/lib/itools/components/idlitviscontour__define.pro#15 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
; Purpose:
;   The IDLitVisContourContainer class is a helper class for IDLitVisContour.
;


;----------------------------------------------------------------------------
;+
; :Description:
;    Override the <i>_IDLitVisualization::Add</i> method, so we can change
;    our contour level values if a new contour level is added.
;
; :Params:
;    oVis
;
; :Keywords:
;    _REF_EXTRA
;
; :Author: chris
;-
pro IDLitVisContourContainer::Add, oVis, INTERNAL=internal, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Loop thru one-by-one, to simplify the code below.
    if (N_Elements(oVis) gt 1) then begin
        for i=0,N_Elements(oVis)-1 do begin
            self->Add, oVis[i], INTERNAL=internal, _EXTRA=_extra
        endfor
        return
    endif
    
    if (~Obj_Valid(oVis) || ~Obj_Isa(oVis, 'IDLitVisContourLevel')) then return
    self->GetProperty, PARENT=oParent
    if (~Obj_Valid(oParent) || ~Obj_Isa(oParent, 'IDLitVisContour')) then $
      return
    oContour = oParent._oContour
    if (~Obj_Valid(oParent._oContour)) then $
      return


    if (~Keyword_Set(internal)) then begin
        ; Retrieve our cached contour value.
        oVis->GetProperty, _VALUE=value

        ; Retrieve the current contour values and insert the new one.
        oContour->GetProperty, C_VALUE=c_value
        n = N_Elements(c_value)
        if (n ge 1) then begin
            ; Insert into the sorted position.
            index = MAX(WHERE(c_value lt value)) + 1
            c_value = [c_value, 0]
            if (index lt n) then c_value[index+1:*] = c_value[index:n-1]
            c_value[index] = value
        endif else begin
        endelse
    endif


    self->_IDLitVisualization::Add, oVis, POSITION=index, _EXTRA=_extra

    
    if (~Keyword_Set(internal)) then begin
        oParent->SetProperty, C_VALUE=c_value
        void = oParent->_GetLevels()
    endif

end


;----------------------------------------------------------------------------
;+
; :Description:
;    Override the <i>_IDLitVisualization::Remove</i> method, so we can change
;    our contour level values if a contour level is deleted.
;
; :Params:
;    oVis
;
; :Keywords:
;    _REF_EXTRA
;
; :Author: chris
;-
pro IDLitVisContourContainer::Remove, oVis, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Loop thru one-by-one, to simplify the code below.
    if (N_Elements(oVis) gt 1) then begin
        for i=0,N_Elements(oVis)-1 do begin
            self->Remove, oVis[i], _REF_EXTRA=_extra
        endfor
        return
    endif
    
    if (~Obj_Valid(oVis) || ~Obj_Isa(oVis, 'IDLitVisContourLevel')) then $
        return

    ; Determine which contour level is being removed.
    void = self->IsContained(oVis, POSITION=index)

    ; Cache our current contour value back onto ourself.
    oVis->GetProperty, VALUE=oldValue
    oVis->SetProperty, _VALUE=oldValue

    self->_IDLitVisualization::Remove, oVis, _REF_EXTRA=_extra

    if (index lt 0) then return
    
    self->GetProperty, PARENT=oParent
    if (~Obj_Valid(oParent) || ~Obj_Isa(oParent, 'IDLitVisContour')) then $
      return
    oContour = oParent._oContour
    if (~Obj_Valid(oParent._oContour)) then $
      return
    
    ; Retrieve the current contour values and remove the dead one.
    oContour->GetProperty, C_VALUE=c_value
    n = N_Elements(c_value)
    if (n ge 2) then begin
        c_value = c_value[WHERE(LINDGEN(n) ne index)]
    endif else begin
    endelse
    oParent->SetProperty, C_VALUE=c_value
    void = oParent->_GetLevels()

end


;----------------------------------------------------------------------------
pro IDLitVisContourContainer__Define
    compile_opt idl2, hidden
    struct = { IDLitVisContourContainer, $
        inherits _IDLitVisualization }
end

