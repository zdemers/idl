; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/components/idlitvisplot__define.pro#2 $
;
; Copyright (c) 2001-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLitVisPlot
;
; PURPOSE:
;    The IDLitVisPlot class is the component wrapper for IDLgrPlot
;
; CATEGORY:
;    Components
;
; MODIFICATION HISTORY:
;     Written by:   AY, 02/2003
;-

;----------------------------------------------------------------------------
; Lifecycle Methods
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; Note that this discussion covers both 2D and 3D
; plots.  The IDLitVisplot object implements 2D plots, while the
; IDLitVisPlot3D object implements 3D plots.
;
; METHODNAMES:
;   IDLitVisPlot::Init
;   IDLitVisPlot3D::Init
;
; PURPOSE:
;   Initialize this component
;
;   NOTE: Init methods are special lifecycle methods, and as such
;   cannot be called outside the context of object creation.  This
;   means that in most cases, you cannot call the Init method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Init method
;   from within the Init method of the subclass.
;
; CALLING SEQUENCE:
;
;   Obj = OBJ_NEW('IDLitVisPlot', [[X,] Y]])
;   Obj = OBJ_NEW('IDLitVisPlot3D', X, Y, Z)
;   Obj = OBJ_NEW('IDLitVisPlot', Vertices2D)
;   Obj = OBJ_NEW('IDLitVisPlot3D', Vertices3D)
;   Obj = OBJ_NEW('IDLitVisPlot', [R], Theta, /POLAR)
;
; INPUTS:
;   X: Vector of X coordinates
;   Y: Vector of Y coordinates
;   Z: Vector of Z coordinates
;   Vertices2D: 2xN array of X and Y coordinates
;   Vertices2D: 3xN array of X, Y and Z coordinates
;   R: Radius for Polar plot
;   Theta: Angle in radians for Polar plot
;
;
; KEYWORD PARAMETERS:
;   All keywords that can be used for IDLgrPlot in addition to
;   the following:
;
;   ERRORBAR_COLOR (Get, Set):
;       RGB value specifying the color for the error bar.
;       Default = [0,0,0] (black)
;   ERRORBAR_CAPSIZE (Get, Set):
;       Float value specifying the size of the error bar endcaps.
;       Value ranges from 0 to 1.0.  A value of 1.0 results
;       in an endcap that is 10% of the width/height of the plot.
;   FILL_BACKGROUND (Get, Set):
;       Boolean: True or False fill the area under the plot.
;       Default = False.
;   FILL_COLOR (Get, Set):
;       RGB value specifying the color for the filled area.
;       Default = [255,255,255] (white)
;   FILL_LEVEL (Get, Set):
;       Float value specifying the Y value for the lower boundary
;       of the fill region.
;   HISTOGRAM (Get, Set) (IDLgrPlot)
;       Set this keyword to force only horizontal and vertical lines
;       to be used to connect the plotted points. By default, the
;       points are connected using a single straight line. The
;       histogram property applies to 2D plots only.  The histogram
;       keyword is ignored for 3D plots.
;   TRANSPARENCY (Get, Set):
;       Integer value specifying the transparency of the plot lines.
;       Valid values range from 0 to 100.  Default is 0 (opaque).
;   POLAR (Get, Set)
;       Set this keyword to display the plot as a polar plot. If this is
;       set the arguments will be interpreted as R and Theta or simply
;       Theta for a single argument.  If R is not supplied the plot will
;       use a vector of indices for the R argument. Default = False
;   SYM_INCREMENT (Get, Set):
;       Integer value specifying the number of vertices to increment
;       between symbol instances.  Default is 1 for a symbol on
;       every vertex.
;   SYMBOL (Get, Set) (IDLitSymbol):
;       The symbol index that specifies the particular symbol (or no symbol)
;       to use.
;   SYM_SIZE (Get, Set) (IDLitSymbol):
;       Float value from 0.0 to 1.0 specifying the size of the plot symbol.
;       A value of 1.0 results in an symbol that is 10% of the width/height
;       of the plot.
;   SYM_COLOR (Get, Set) (IDLitSymbol):
;       RGB value speciying the color for the plot symbol.  Note this
;       color is applied to the symbol only if the USE_DEFAULT_COLOR
;       property is false.
;   SYM_THICK (Get, Set) (IDLitSymbol):
;       Float value from 1 to 10 specifying the thickness of the plot symbol.
;   FILL_TRANSPARENCY (Get, Set):
;       Integer value specifying the transparency of the filled area.
;       Valid values range from 0 to 100.  Default is 0 (opaque).
;   USE_DEFAULT_COLOR (Get, Set) (IDLitSymbol):
;       Boolean: False to use the symbol color instead of matching the plot.
;   [XY]_ERRORBARS (Get, Set):
;       Boolean: Hide or Show the error bars. Default = Show
;   [XY]LOG (Get, Set):
;       Set this keyword to specify a logarithmic axis.  The minimum
;       value of the axis range must be greater than zero.
;   XY_SHADOW (Get, Set):
;   YZ_SHADOW (Get, Set):
;   XZ_SHADOW (Get, Set):
;       Set these keywords to display the shadow of the plot in a 3D plot.
;       The shadow lies in the plane specified by the first two letters of
;       the keyword, at the minimum value of the data
;
; OUTPUTS:
;   This function method returns 1 on success, or 0 on failure.
;
;
;-
function IDLitVisPlot::Init, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Initialize superclass
    if (~self->IDLitVisualization::Init(/REGISTER_PROPERTIES, $
        NAME='Plot', ICON='plot', TYPE='IDLPLOT',$
        DESCRIPTION='A Plot Visualization', $
        _EXTRA=_extra)) then return, 0

    self._oPalette = OBJ_NEW('IDLgrPalette')
    self._oSymbol = OBJ_NEW('IDLitSymbol', PARENT=self)
    self._oSymbolSpacer = OBJ_NEW('IDLgrSymbol', 0)     ; 0 for no symbol

    ; Create plot object and add it to this Visualization.
    self._oPlot = OBJ_NEW('IDLgrPlot', /REGISTER_PROPERTIES, $
        /ANTIALIAS, PALETTE=self._oPalette, $
        SYMBOL=self._oSymbol->GetSymbol(), $
        /PRIVATE)

    ; Add in the Symbol at the beginning, to make sure its draw method
    ; gets hit before our own draw - so we can update the symbol size.
    self->IDLgrModel::Add, self._oSymbol, POSITION=0

    ; NOTE: the IDLgrPlot and IDLitSymbol properties will be aggregated
    ; as part of the property registration process in an upcoming call
    ; to ::_RegisterProperties.
    self->Add, self._oPlot, /NO_NOTIFY

    self._fillColor = [128b,128b,128b]
    self._fillTransparency = 0
    self._fillLevel = 1d-300

    ; Request box style axes by default.
    self->SetAxesStyleRequest, 2, /NO_NOTIFY

   ; cap size of 1 covers approx. 10 % of the data range
    self._capSize = 0.2d ; reasonble default value

    ; Our own selection visual.
    oSelectionVisual = OBJ_NEW('IDLitManipVisSelect', /HIDE)
    ; Add a shadow plot. We will set the rest of the properties
    ; in _UpdateSelectionVisual.
    self._oPlotSelectionVisual = OBJ_NEW('IDLgrPlot', $
        ALPHA_CHANNEL=0.2, ANTIALIAS=0, $
        LINESTYLE=0, $
        THICK=6, $
        COLOR=!COLOR.DODGER_BLUE)
    oSelectionVisual->Add, self._oPlotSelectionVisual
    self->SetDefaultSelectionVisual, oSelectionVisual

    ; Register Data Parameters
    self->_RegisterParameters

    ; Register all properties and set property attributes
    self->IDLitVisPlot::_RegisterProperties

    self->SetPropertyAttribute, ['SYMBOL', 'SYM_SIZE', 'SYM_COLOR'], $
      ADVANCED_ONLY=0
      
    ; Defaults.
    self._symIncrement = 1

    ; Set any properties
    if(n_elements(_extra) gt 0)then $
      self->IDLitVisPlot::SetProperty,  _EXTRA=_extra

    RETURN, 1 ; Success
end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLitVisPlot::Cleanup
;
; PURPOSE:
;   This procedure method performs all cleanup on the object.
;
;   NOTE: Cleanup methods are special lifecycle methods, and as such
;   cannot be called outside the context of object destruction.  This
;   means that in most cases, you cannot call the Cleanup method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Cleanup method
;   from within the Cleanup method of the subclass.
;
; CALLING SEQUENCE:
;   OBJ_DESTROY, Obj
;
;    or
;
;   Obj->[IDLitVisPlot::]Cleanup
;
;-
pro IDLitVisPlot::Cleanup

    compile_opt idl2, hidden

    OBJ_DESTROY, self._oPalette
    OBJ_DESTROY, self._oSymbol
    OBJ_DESTROY, self._oSymbolSpacer
    OBJ_DESTROY, self._oXErrorBar
    OBJ_DESTROY, self._oYErrorBar

    ; Cleanup superclass
    self->IDLitVisualization::Cleanup
end

;----------------------------------------------------------------------------
pro IDLitVisPlot::_RegisterParameters

    compile_opt idl2, hidden

    self->RegisterParameter, 'Y', DESCRIPTION='Y Plot Data', $
        /INPUT, TYPES=['IDLVECTOR'], /OPTARGET

    self->RegisterParameter, 'X', DESCRIPTION='X Plot Data', $
        /INPUT, TYPES=['IDLVECTOR'], /OPTIONAL

    self->RegisterParameter, 'VERTICES', DESCRIPTION='Vertex Data', $
        /INPUT, TYPES=['IDLARRAY2D'], /OPTARGET, /OPTIONAL

    self->RegisterParameter, 'Y ERROR', DESCRIPTION='Y Error Data', $
        /INPUT, TYPES=['IDLVECTOR','IDLARRAY2D'], /OPTIONAL

    self->RegisterParameter, 'X ERROR', DESCRIPTION='X Error Data', $
        /INPUT, TYPES=['IDLVECTOR', 'IDLARRAY2D'], /OPTIONAL

    self->RegisterParameter, 'PALETTE', DESCRIPTION='RGB Color Table', $
        /INPUT, TYPES=['IDLPALETTE','IDLARRAY2D'], /OPTARGET, /OPTIONAL

    self->RegisterParameter, 'VERTEX_COLORS', DESCRIPTION='Vertex Colors', $
        /INPUT, /OPTIONAL, TYPES=['IDLVECTOR','IDLARRAY2D']

end

;----------------------------------------------------------------------------
; IDLitVisPlot::_RegisterProperties
;
; Purpose:
;   This procedure method registers properties associated with this class.
;
; Calling sequence:
;   oObj->[IDLitVisPlot::]_RegisterProperties
;
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLitVisPlot::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

    compile_opt idl2, hidden

    registerAll = ~KEYWORD_SET(updateFromVersion)

    if (registerAll) then begin
        ; Aggregate the plot and symbol properties.
        self->Aggregate, self._oPlot
        self->Aggregate, self._oSymbol

        self->RegisterProperty, 'VISUALIZATION_PALETTE', $
            NAME='Vertex color table', $
            USERDEF='Edit color table', $
            DESCRIPTION='Edit RGB Color Table', $
            SENSITIVE=0, /ADVANCED_ONLY

        self->RegisterProperty, 'X_ERRORBARS', $
            ENUMLIST=['Hide','Show'], $
            DESCRIPTION='X Error Bars', $
            NAME='X error bars', $
            /HIDE, /ADVANCED_ONLY

        self->RegisterProperty, 'Y_ERRORBARS', $
            ENUMLIST=['Hide','Show'], $
            DESCRIPTION='Y Error Bars', $
            NAME='Y error bars', $
            /HIDE, /ADVANCED_ONLY

        self->RegisterProperty, 'ERRORBAR_COLOR', /COLOR, $
            DESCRIPTION='Error bar color', $
            NAME='Error bar color', $
           /HIDE, /ADVANCED_ONLY

        self->RegisterProperty, 'ERRORBAR_CAPSIZE', /FLOAT, $
            DESCRIPTION='Length of error bar end cap', $
            NAME='Error bar endcap size', $
            VALID_RANGE=[0, 1, .01d], $
            /HIDE, /ADVANCED_ONLY

        self._oPlot->RegisterProperty, 'FILL_BACKGROUND', /BOOLEAN, $
            DESCRIPTION='Fill background', $
            NAME='Fill background', /ADVANCED_ONLY

        self._oPlot->RegisterProperty, 'FILL_LEVEL', /FLOAT, $
            DESCRIPTION='Fill level', $
            NAME='Fill level', $
            /HIDE, /UNDEFINED, /ADVANCED_ONLY

        self._oPlot->RegisterProperty, 'FILL_COLOR', /COLOR, $
            DESCRIPTION='Fill color', $
            NAME='Fill color', /ADVANCED_ONLY

        ; Override registered property attributes.
        self->SetPropertyAttribute, 'SYM_INCREMENT', HIDE=0

        ; Do not show until Y data is present.
        self->SetPropertyAttribute, ['MIN_VALUE', 'MAX_VALUE'], /HIDE, /UNDEFINED
    endif

    if (registerAll || (updateFromVersion lt 640)) then begin
        ; TRANSPARENCY became a new property FILL_TRANSPARENCY in IDL64.
        self._oPlot->RegisterProperty, 'FILL_TRANSPARENCY', /INTEGER, $
            NAME='Fill transparency', $
            DESCRIPTION='Fill transparency', $
            VALID_RANGE=[0, 100, 5], /ADVANCED_ONLY
    endif

    if (registerAll || (updateFromVersion lt 610)) then begin

        self->RegisterProperty, 'TRANSPARENCY', /INTEGER, $
            NAME='Plot transparency', $
            DESCRIPTION='Transparency of Plot', $
            VALID_RANGE=[0,100,5]

        ; Use TRANSPARENCY and FILL_TRANSPARENCY properties instead.
        self->SetPropertyAttribute, 'ALPHA_CHANNEL', /HIDE, /ADVANCED_ONLY
    endif

    if (registerAll || (updateFromVersion lt 620)) then begin
        self._oPlot->RegisterProperty, 'ZVALUE', /FLOAT, $
            NAME='Z value', $
            DESCRIPTION='Z value on which to project the plot', /ADVANCED_ONLY
    endif

    if (~registerAll && (updateFromVersion lt 640)) then begin
        ; Usage of TRANSPARENCY property changed from the fill to the plot.
        self->SetPropertyAttribute, 'TRANSPARENCY', $
            NAME='Plot transparency', DESCRIPTION='Transparency of Plot'
        ; PLOT_TRANSPARENCY became a new property TRANSPARENCY in IDL64
        self->RegisterProperty, 'PLOT_TRANSPARENCY', /INTEGER, $
            NAME='Plot transparency', $
            DESCRIPTION='Transparency of Plot', $
            VALID_RANGE=[0,100,5]
        self->SetPropertyAttribute, 'PLOT_TRANSPARENCY', /HIDE
    endif

    ; HISTOGRAM was changed and STAIRSTEP was added in IDL 8.8.2
    if (registerAll || (updateFromVersion lt 822)) then begin
      self._oPlot->SetPropertyAttribute, 'HISTOGRAM', ENUMLIST=['False', 'True']
      self._oPlot->RegisterProperty, 'STAIRSTEP', /BOOLEAN, $
        NAME='Stairstep', DESCRIPTION='Stairstep', /ADVANCED_ONLY
    endif

end

;----------------------------------------------------------------------------
; IDLitVisPlot::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisPlot::Restore
    compile_opt idl2, hidden

    ; Call superclass restore.
    self->_IDLitVisualization::Restore

    ; Call ::GetProperty on each aggregated graphic object
    ; to force its internal restore process to be called, thereby
    ; ensuring any new properties are registered.
    if (OBJ_VALID(self._oPlot)) then $
        self._oPlot->GetProperty
    if (OBJ_VALID(self._oSymbol)) then $
        self._oSymbol->GetProperty

    ; Register new properties.
    self->IDLitVisPlot::_RegisterProperties, $
        UPDATE_FROM_VERSION=self.idlitcomponentversion

    ; ---- Required for SAVE files transitioning ----------------------------
    ;      from IDL 6.0 to 6.1 or above:
    if (self.idlitcomponentversion lt 610) then begin
        ; Request crosshair axes if polar, box axes otherwise.
        self.axesStyleRequest = (self._polar) ? 3 : 2
        ; We used to use the self._min/maxSet value to keep track of
        ; whether min/max_value had been set. Now we just use the
        ; undefined attribute. So reset our min/max value to NaN
        ; if it was never set in the saved file.
        if (~self._maxSet) then begin
            self._oPlot->SetProperty, MAX_VALUE=!values.d_nan
            self->SetPropertyAttribute, 'MAX_VALUE', /UNDEFINED
        endif
        if (~self._minSet) then begin
            self._oPlot->SetProperty, MIN_VALUE=!values.d_nan
            self->SetPropertyAttribute, 'MIN_VALUE', /UNDEFINED
        endif
    endif

    ; In IDL62 we removed the symbol from the selection visual plot.
    if (self.idlitcomponentversion lt 620) then begin
        if (Obj_Valid(self._oPlotSelectionVisual)) then $
          self._oPlotSelectionVisual->SetProperty, SYMBOL=OBJ_NEW()
        OBJ_DESTROY, self._oSymbolSelection
        self->_UpdateSelectionVisual
    endif

end


;----------------------------------------------------------------------------
; Property Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::GetProperty
;
; PURPOSE:
;      This procedure method retrieves the
;      value of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]GetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLitVisPlot::Init followed by the word "Get"
;      can be retrieved using IDLitVisPlot::GetProperty.
;
;-
pro IDLitVisPlot::GetProperty, $
    EQUATION=equation, $
    EQN_SAMPLES=eqnSamples, $
    EQN_USERDATA=eqnUserdata, $
    ERRORBAR_CAPSIZE=capSize, $
    ERRORBAR_COLOR=barColor, $
    FILL_BACKGROUND=fillBackground, $
    FILL_COLOR=fillColor, $
    FILL_LEVEL=fillLevel, $
    HISTOGRAM=histogram, $
    MIN_VALUE=minValue, $
    MAX_VALUE=maxValue, $
    RGB_TABLE=rgbTable, $
    STAIRSTEP=stairstep, $
    SYMBOL=symValue, $
    SYM_INCREMENT=symIncrement, $
    FILL_TRANSPARENCY=fillTransparency, $
    PLOT_TRANSPARENCY=plotTransparencyOld, $  ; keep for backwards compat
    TRANSPARENCY=plotTransparency, $
    X_ERRORBARS=xErrorBars, $
    Y_ERRORBARS=yErrorBars, $
    VISUALIZATION_PALETTE=visPalette, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if ARG_PRESENT(barColor) then begin
        ; get the color from one of the error bars.
        ; Both X and Y error bars are the same color for now.
        if OBJ_VALID(self._oXErrorBar) then $
            self._oXErrorBar->GetProperty, COLOR=barColor $
        else $
            barColor = [0b,0b,0b]
    endif

    if ARG_PRESENT(capSize) then begin
        capSize = self._capSize
    endif

    if ARG_PRESENT(fillBackground) then begin
        if OBJ_VALID(self._oFill) then begin
            self._oFill->GetProperty, HIDE=fillHide
            fillBackground = ~fillHide
        endif else $
            fillBackground = 0
    endif

    if ARG_PRESENT(fillColor) then begin
        if OBJ_VALID(self._oFill) then $
            self._oFill->GetProperty, FILL_COLOR=fillColor $
        else $
            fillColor = self._fillColor
    endif

    if ARG_PRESENT(fillLevel) then $
        fillLevel = self._fillLevel

    if ARG_PRESENT(fillTransparency) then begin
        if OBJ_VALID(self._oFill) then $
            self._oFill->GetProperty, TRANSPARENCY=fillTransparency $
        else $
            fillTransparency = self._fillTransparency
    endif

    ; IDLgrPlot: HISTOGRAM=2 corresponds to our STAIRSTEP=1
    ; IDLgrPlot: HISTOGRAM=3 corresponds to our HISTOGRAM=1
    if (ARG_PRESENT(histogram) || ARG_PRESENT(stairstep)) then begin
      self._oPlot->GetProperty, HISTOGRAM=plothistogram
      stairstep = plothistogram eq 1 || plothistogram eq 2
      histogram = plothistogram eq 3
    endif

    if ARG_PRESENT(equation) then begin
      equation = self._equation
    endif
    
    if ARG_PRESENT(eqnSamples) then begin
      eqnSamples = self._eqnSamples
    endif
    
    if ARG_PRESENT(eqnUserdata) then begin
      eqnUserdata = PTR_VALID(self._eqnUserdata) ? *self._eqnUserdata : !NULL
    endif
    
    ; PLOT_TRANSPARENCY became TRANSPARENCY in IDL64.
    ; Keep for backwards compat.
    if ARG_PRESENT(plotTransparencyOld) then begin
        self._oPlot->GetProperty, ALPHA_CHANNEL=alpha
        plotTransparencyOld = 0 > ROUND(100 - alpha * 100) < 100
    endif

    if ARG_PRESENT(plotTransparency) then begin
        self._oPlot->GetProperty, ALPHA_CHANNEL=alpha
        plotTransparency = 0 > ROUND(100 - alpha * 100) < 100
    endif

    if ARG_PRESENT(symIncrement) then $
        symIncrement = self._symIncrement

    ; Handle SYMBOL manually so we don't return the IDLgrSymbol
    ; object by mistake.
    if ARG_PRESENT(symValue) then $
      self._oSymbol->GetProperty, SYMBOL=symValue

    if ARG_PRESENT(xErrorBars) then begin
        if OBJ_VALID(self._oXErrorBar) then begin
            self._oXErrorBar->GetProperty, HIDE=hide
            xErrorBars = ~hide
        endif else $
            xErrorBars = 1
    endif

    if ARG_PRESENT(yErrorBars) then begin
        if OBJ_VALID(self._oYErrorBar) then begin
            self._oYErrorBar->GetProperty, HIDE=hide
            yErrorBars = ~hide
        endif else $
            yErrorBars = 1
    endif

    if ARG_PRESENT(rgbTable) || ARG_PRESENT(visPalette) then begin
        self._oPalette->GetProperty, BLUE_VALUES=blue, $
            GREEN_VALUES=green, RED_VALUES=red
        visPalette = TRANSPOSE([[red], [green], [blue]])
        rgbTable = visPalette
    endif

    if ARG_PRESENT(maxValue) then begin
        ; If logarithmic, never report MAX_VALUE as an exponent.
        self._oPlot->GetProperty, MAX_VALUE=plotMaxValue
        maxValue = (FINITE(plotMaxValue) && (self._yVisLog gt 0)) ? $
            10^plotMaxValue : plotMaxValue
    endif

    if ARG_PRESENT(minValue) then begin
        ; If logarithmic, never report MIN_VALUE as an exponent.
        self._oPlot->GetProperty, MIN_VALUE=plotMinValue
        minValue = (FINITE(plotMinValue) && (self._yVisLog gt 0)) ? $
            10^plotMinValue : plotMinValue
    endif

    ; get superclass properties
    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisualization::GetProperty, _EXTRA=_extra

end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::SetProperty
;
; PURPOSE:
;      This procedure method sets the value
;      of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]SetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLitVisPlot::Init followed by the word "Set"
;      can be set using IDLitVisPlot::SetProperty.
;-
pro IDLitVisPlot::SetProperty, $
    ANTIALIAS=antialias, $
    COLOR=color, $
    EQUATION=equation, $
    EQN_SAMPLES=eqnSamples, $
    EQN_USERDATA=eqnUserdata, $
    ERRORBAR_CAPSIZE=capSize, $
    ERRORBAR_COLOR=barColor, $
    FILL_BACKGROUND=fillBackground, $
    FILL_COLOR=fillColor, $
    FILL_LEVEL=fillLevel, $
    HISTOGRAM=histogram, $
    LINESTYLE=linestyle, $
    MAX_VALUE=maxValue, $
    MIN_VALUE=minValue, $
    NSUM=nsum, $
    POLAR=polar, $
    RGB_TABLE=rgbTableIn, $
    SYM_INCREMENT=symIncrement, $
    FILL_TRANSPARENCY=fillTransparency, $
    PLOT_TRANSPARENCY=plotTransparencyOld, $  ; keep for backwards compat
    STAIRSTEP=stairstep, $
    TRANSPARENCY=plotTransparency, $
    ZVALUE=zvalue, $
    X_ERRORBARS=xErrorBars, $
    Y_ERRORBARS=yErrorBars, $
    X_VIS_LOG=xVisLog, $    ; Property not exposed, internal use only
    Y_VIS_LOG=yVisLog, $    ; Property not exposed, internal use only
    VISUALIZATION_PALETTE=visPalette, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ;; This routine can be called during object creatation/
    ;; initializing, which can cause issues with properties that
    ;; have about any value (max/min values). Check the initializing property
    self->IDLitComponent::GetProperty, INITIALIZING=isInit

    if (ISA(antialias)) then begin
      self._oPlot->SetProperty, ANTIALIAS=antialias
      if (ISA(self._oFill)) then $
        self._oFill->SetProperty, ANTIALIAS=antialias
    endif

    if (N_ELEMENTS(barColor) gt 0) then begin
      if (~isInit) then begin
        self->_VerifyErrorBars, 0
        self->_VerifyErrorBars, 1
      endif
      bc = size(barColor,/TYPE) eq 7 ? $
        colortable([barColor],NCOLORS=1,/transpose): barColor
      if (ISA(self._oXErrorBar)) then begin
        self._oXErrorBar->SetProperty, COLOR=bc
      endif
      if (ISA(self._oYErrorBar)) then begin
        self._oYErrorBar->SetProperty, COLOR=bc
      endif
    endif

    if (N_ELEMENTS(capSize) gt 0) then begin
        self->GetPropertyAttribute, 'ERRORBAR_CAPSIZE', $
            VALID_RANGE=validRange
        capSize >= validRange[0]
        capSize <= validRange[1]
        self._capSize = capSize
        self->_UpdateCapSize
    endif

    if (N_ELEMENTS(zvalue) gt 0) then begin
        self._oPlot->SetProperty, USE_ZVALUE=(zvalue ne 0), ZVALUE=zvalue
        self->_UpdateFill
        self->_UpdateSelectionVisual
        ; put the visualization into 3D mode if necessary
        self->Set3D, (zvalue ne 0), /ALWAYS
        self->OnDataChange, self
        self->OnDataComplete, self
    endif

    ; Our STAIRSTEP=1 corresponds to IDLgrPlot: HISTOGRAM=1
    ; Our HISTOGRAM=1 corresponds to IDLgrPlot: HISTOGRAM=2
    if (ISA(histogram) || ISA(stairstep)) then begin
      plothistogram = 0
      if (KEYWORD_SET(histogram)) then plothistogram = 3
      if (KEYWORD_SET(stairstep)) then plothistogram = 2
      ; Handle this explicitly in order to re-calculate fill.
      self._oPlot->SetProperty, HISTOGRAM=plothistogram
      ; Need to update our fill and selection visual manually.
      self->_UpdateFill
      self->_UpdateSelectionVisual
      self->OnDataChange, self
      self->OnDataComplete, self
    endif

    if (N_ELEMENTS(linestyle) gt 0) then begin
        ; Handle this explicitly in order to re-calculate fill.
        self._oPlot->SetProperty, LINESTYLE=linestyle
        ; Need to update our selection visual manually.
        self->_UpdateSelectionVisual
    endif

    if (N_ELEMENTS(maxValue) gt 0) then begin
        if (FINITE(maxValue)) then begin
            ; If logarithmic, translate given MAX_VALUE to an exponent.
            plotMaxValue = (self._yVisLog gt 0) ? alog10(maxValue) : maxValue
            self->SetPropertyAttribute, 'MAX_VALUE', UNDEFINED=0
            self._oPlot->SetProperty, MAX_VALUE=plotMaxValue
        endif else begin
            self->SetPropertyAttribute, 'MAX_VALUE', /UNDEFINED
            self._oPlot->SetProperty, MAX_VALUE=maxValue
        endelse
        self->_UpdateFill
        self->_UpdateSelectionVisual
        self->OnDataChange, self
        self->OnDataComplete, self
    endif

    if (N_ELEMENTS(minValue) gt 0) then begin
        if (FINITE(minValue)) then begin
            ; If logarithmic, translate given MIN_VALUE to an exponent.
            plotMinValue = (self._yVisLog gt 0) ? alog10(minValue) : minValue
            self->SetPropertyAttribute, 'MIN_VALUE', UNDEFINED=0
            self._oPlot->SetProperty, MIN_VALUE=plotMinValue
        endif else begin
            self->SetPropertyAttribute, 'MIN_VALUE', /UNDEFINED
            self._oPlot->SetProperty, MIN_VALUE=minValue
        endelse
        self->_UpdateFill
        self->_UpdateSelectionVisual
        self->OnDataChange, self
        self->OnDataComplete, self
    endif


    if (N_ELEMENTS(nsum) gt 0) then begin
        self._oPlot->GetProperty, NSUM=nsumOld
        if (nsum ne nsumOld) then begin
            oDataY = self->GetParameter('Y')
            if ((~OBJ_VALID(oDataY)) || (~oDataY->GetData(ydata))) then $
                return

            ndata = N_ELEMENTS(ydata)
            ; make sure there are at least two points for plot
            ; number to average must be one less than number of points
            nsum = nsum < (ndata-1)

            self._oPlot->SetProperty, NSUM=nsum
            self->_UpdateFill
            self->_UpdateSelectionVisual
            self->OnDataChange, self
            self->OnDataComplete, self
        endif
    endif


    ; POLAR plot.
    if (N_ELEMENTS(polar) gt 0) then begin

        self._polar = KEYWORD_SET(polar)

        self->IDLitVisualization::SetProperty, POLAR=self._polar
        self->OnDataChange, self
        self->OnDataComplete, self

        ; Do not allow polar plots to be filled.
        if (self._polar) then begin
            ; If POLAR has been turned on, we need to cache our old
            ; fill value and turn fill off.
            self->GetProperty, FILL_BACKGROUND=wasFilled
            self._wasFilled = wasFilled

            ; Turn fill off.
            ; Note: this assumes the fill code follows this code block.
            if (wasFilled) then fillBackground = 0

            self->SetPropertyAttribute, $
                'FILL_BACKGROUND', SENSITIVE=0

            ; Request crosshair axes.
            self->SetAxesStyleRequest, 3
        endif else begin
            ; Turn fill back on.
            ; Note: this assumes the fill code follows this code block.
            if (self._wasFilled) then $
                fillBackground = 1
            self->SetPropertyAttribute, $
                'FILL_BACKGROUND', /SENSITIVE

            ; Request box axes.
            self->SetAxesStyleRequest, 2
        endelse

        self->_UpdateSelectionVisual

    endif


    ; Fill properties. See if we need to create polygon.
    ; We need to do this regardless of whether POLAR is currently set
    ; or not, because we may need to change the fill props, say
    ; for preferences.
    IF (~OBJ_VALID(self._oFill) && KEYWORD_SET(fillBackground)) THEN BEGIN
      ;; Filled portion.
      self._oPlot->GetProperty, ANTIALIAS=antialias
      self._oFill = OBJ_NEW('IDLitVisPolygon', $
        ANTIALIAS=antialias, $
        COLOR=self._fillColor, $
        FILL_COLOR=self._fillColor, $
        IMPACTS_RANGE=0, $
        CLIP=0, $  ; the plot will take care of the clipping
        TRANSPARENCY=self._fillTransparency, $
        /HIDE, /PRIVATE, $
        LINESTYLE=6, $
        /TESSELLATE)
      ;; Add to the beginning so it is in the background.
      ; Force NO_UPDATE so the data range doesn't suddenly change.
      self->Add, self._oFill, POSITION=0, /NO_UPDATE
    ENDIF

    ; Note: This code must follow the POLAR code above, because
    ; we may need to turn on/off FILL_BACKGROUND.
    if (N_ELEMENTS(fillBackground)) then begin
        ; Don't allow FILL_BACKGROUND if POLAR is also set.
        if (self._polar) then $
            fillBackground = 0

        if (OBJ_VALID(self._oFill)) then begin
          self._oFill->GetProperty, HIDE=fillHide
          wasFilled = ~fillHide
        endif else wasFilled = -1

        ; Only tweak this if the value changed.
        if (wasFilled ne KEYWORD_SET(fillBackground)) then begin
        
          if KEYWORD_SET(fillBackground) then begin
              self._oFill->SetProperty, HIDE=0
              self->_UpdateFill
          endif else begin
              IF obj_valid(self._oFill) THEN $
                self._oFill->SetProperty, /HIDE
          endelse
          self->SetPropertyAttribute, $
              ['FILL_COLOR', 'FILL_LEVEL', 'FILL_TRANSPARENCY'], $
              SENSITIVE=KEYWORD_SET(fillBackground), HIDE=0

        endif
    endif

    IF (N_ELEMENTS(fillColor) gt 0) THEN BEGIN
      self._fillColor = fillColor
      IF obj_valid(self._oFill) THEN $
        self._oFill->SetProperty, FILL_COLOR=fillColor
    ENDIF

    if (N_ELEMENTS(fillLevel)) then begin
        self._fillLevel = fillLevel
        self._hasFillLevel = self._fillLevel ne 1d-300
        ; Start showing the actual value.
        self->SetPropertyAttribute, 'FILL_LEVEL', UNDEFINED=0
        self->_UpdateFill
        ; recompute data range since fill impacts range
        oDataY = self->GetParameter('Y')
        if ((OBJ_VALID(oDataY)) && (oDataY->GetData(ydata))) then begin
            self->OnDataChange, self
            self->OnDataComplete, self
        endif
    endif

    IF (N_ELEMENTS(fillTransparency)) THEN BEGIN
      self._fillTransparency = fillTransparency
      IF obj_valid(self._oFill) THEN $
        self._oFill->SetProperty, TRANSPARENCY=fillTransparency
    ENDIF

    ; PLOT_TRANSPARENCY became TRANSPARENCY in IDL64.
    ; Keep for backwards compat.
    if (N_ELEMENTS(plotTransparencyOld)) then $
        plotTransparency = plotTransparencyOld

    if (N_ELEMENTS(plotTransparency)) then begin
        self._oPlot->GetProperty, ALPHA_CHANNEL=alpha
        plotTransparencyOrig = 0 > ROUND(100 - alpha * 100) < 100
        self._oSymbol->GetProperty, SYM_TRANSPARENCY=symTrans
        self._oPlot->SetProperty, $
            ALPHA_CHANNEL=0 > ((100. - plotTransparency)/100) < 1
        if (plotTransparencyOrig eq symTrans) then $
          self._oSymbol->SetProperty, SYM_TRANSPARENCY=plotTransparency
    endif

    if (N_ELEMENTS(symIncrement) gt 0) then begin
        self._symIncrement = symIncrement
        self->_UpdateSymIncrement
    endif

    if (N_ELEMENTS(xErrorBars) gt 0) then begin
      if (~isInit) then begin
        self->_VerifyErrorBars, 0
      endif
      if (ISA(self._oXErrorBar)) then $
        self._oXErrorBar->SetProperty, HIDE=~xErrorBars
    endif

    if (N_ELEMENTS(yErrorBars) gt 0) then begin
      if (~isInit) then begin
        self->_VerifyErrorBars, 1
      endif
      if (ISA(self._oYErrorBar)) then $
        self._oYErrorBar->SetProperty, HIDE=~yErrorBars
    endif

    ; Internal use flag allowing the dataspace to
    ; control the state of the vis data
    if (N_ELEMENTS(xVisLog) gt 0 && xVisLog ne self._xVisLog) then begin
        self._xVisLog = xVisLog
        self._oPlot->GetProperty, DATA=data
        if N_ELEMENTS(data) gt 0 then begin
            newX = (xVisLog gt 0) ? alog10(data[0,*]) : 10^data[0,*]
            self._oPlot->SetProperty, DATAX=newX
        endif
        self->_UpdateErrorBars, 0
        self->_UpdateErrorBars, 1
        self->_UpdateFill
        self->_UpdateSelectionVisual
    endif

    ; Internal use flag allowing the dataspace to
    ; control the state of the vis data
    if (N_ELEMENTS(yVisLog) gt 0 && yVisLog ne self._yVisLog) then begin
        self._yVisLog = yVisLog
        self._oPlot->GetProperty, DATA=data, MIN_VALUE=oldMinVal, $
            MAX_VALUE=oldMaxVal
        if N_ELEMENTS(data) gt 0 then begin
            newY = (yVisLog gt 0) ? alog10(data[1,*]) : 10^data[1,*]
            minn = MIN(newY, MAX=maxx)
            ; If the MIN/MAX_VALUEs are not being explicitly set in
            ; this same call, then apply log to min/max values, too.
            if (FINITE(oldMinVal) && (N_ELEMENTS(minValue) eq 0)) then $
                newMinValue = (yVisLog gt 0) ? alog10(oldMinVal) : $
                    10^oldMinVal
            if (FINITE(oldMaxVal) && (N_ELEMENTS(maxValue) eq 0)) then $
                newMaxValue = (yVisLog gt 0) ? alog10(oldMaxVal) : $
                    10^oldMaxVal
            self._oPlot->SetProperty, DATAY=newY, $
                MIN_VALUE=newMinValue, MAX_VALUE=newMaxValue
        endif
        self->_UpdateErrorBars, 0
        self->_UpdateErrorBars, 1
        self->_UpdateFill
        self->_UpdateSelectionVisual
    endif

    if (ISA(equation) || ISA(eqnSamples) || $
      ISA(eqnUserdata) || ISA(eqnUserdata, /NULL)) then begin
      if (ISA(equation)) then begin
        eqn = STRLOWCASE(equation)
        if (~ISA(equation, 'STRING') || $
          STRPOS(eqn, '&') ge 0 || STRPOS(eqn, 'execute') ge 0) then begin
          MESSAGE, 'Illegal value for EQUATION property.'
        endif
        self._equation = equation
      endif
      if (ISA(eqnSamples)) then begin
        self._eqnSamples = eqnSamples > 0
      endif
      if (ISA(eqnUserdata) || ISA(eqnUserdata, /NULL)) then begin
        self._eqnUserdata = ISA(eqnUserdata) ? PTR_NEW(eqnUserdata) : PTR_NEW()
      endif
      self->_UpdateData
      self->_UpdateFill
      self->_UpdateSelectionVisual
    endif

    if (ISA(rgbTableIn) || ISA(visPalette)) then begin
      rgbTable = ISA(rgbTableIn) ? rgbTableIn : visPalette
      if (N_ELEMENTS(rgbTable) eq 1) then begin
        Loadct, rgbTable[0], RGB_TABLE=rgbTable
      endif
      dim = SIZE(rgbTable, /DIMENSIONS)
      if (N_ELEMENTS(dim) eq 2) then begin
        if (dim[0] gt dim[1]) then $
          rgbTable = TRANSPOSE(rgbTable)
        self._oPalette->SetProperty, BLUE_VALUES=rgbTable[2,*], $
          GREEN_VALUES=rgbTable[1,*], RED_VALUES=rgbTable[0,*]
        oDataRGB = self->GetParameter('PALETTE')
        if OBJ_VALID(oDataRGB) then $
          success = oDataRGB->SetData(rgbTable)
      endif
    endif

    if ISA(color) then begin
      ; If COLOR is set, and error bar color matches the current line
      ; color, then also set the error bar color.
      if OBJ_VALID(self._oXErrorBar) then begin
        self._oPlot->GetProperty, COLOR=oldColor
        self._oXErrorBar->GetProperty, COLOR=barColor
        if ARRAY_EQUAL(oldColor, barColor) then begin
          self._oXErrorBar->SetProperty, COLOR=color
          self._oYErrorBar->SetProperty, COLOR=color
        endif
      endif
      self->IDLitVisualization::SetProperty, COLOR=color
    endif

    if (N_ELEMENTS(_extra) gt 0) then begin
        self->IDLitVisualization::SetProperty, _EXTRA=_extra
    endif


end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to retrieve the data from the grPlot
;
; Arguments:
;   X, Y, and Z, although not necessarily in that order
;
; Keywords:
;   NONE
;
pro IDLitVisPlot::GetData, arg1, arg2, arg3, arg4, _EXTRA=_extra
  compile_opt idl2, hidden
  
  oParamSet = self->GetParameterSet()
  if (~ISA(oParamSet)) then return

  oDataVert = oParamSet->GetByName('Vertices')
  if (ISA(oDataVert) && oDataVert->GetData(vertices)) then begin
    xdata = REFORM(vertices[0,*])
    ydata = REFORM(vertices[1,*])
    if (N_Elements(vertices[*,0] ge 3)) then $
      zdata = REFORM(vertices[2,*])
  endif else begin
    oDataY = oParamSet->GetByName('Y')
    if (~ISA(oDataY) || ~oDataY->GetData(ydata)) then return
    ndata = N_ELEMENTS(ydata)
    oDataX = oParamSet->GetByName('X')
    if (~ISA(oDataX) || ~oDataX->GetData(xdata) || $
      (N_ELEMENTS(xdata) ne ndata)) then begin
      xdata = ISA(ydata, "FLOAT") ? FINDGEN(ndata) : DINDGEN(ndata)
    endif
  endelse
  

  ; PLOT, POLARPLOT, BARPLOT and ERRORPLOT use the same idlitvisplot object
  ; so assume that we are working with ERRORPLOT if the "Y ERROR"
  ; parameter is a valid object.
  oDataYErr = oParamSet->GetByName('Y ERROR')
  
  if ISA( oDataYErr ) then begin
    switch (N_PARAMS()) of
      4 : begin
        ; x, y, x_error=xe, y_error=ye
        arg1 = TEMPORARY(xdata)
        arg2 = TEMPORARY(ydata)
        oXErrData = self->GetParameter('X ERROR')
        if (~OBJ_VALID(oXErrData)) then return
        if (~oXErrData->GetData(arg3)) then return
        
        oYErrData = self->GetParameter('Y ERROR')
        if (~OBJ_VALID(oYErrData)) then return
        if (~oYErrData->GetData(arg4)) then return
        break
      end
      3 : begin
        ; x, y, y_error=ye
        arg1 = TEMPORARY(xdata)
        arg2 = TEMPORARY(ydata)
        oYErrData = self->GetParameter('Y ERROR')
        if (~OBJ_VALID(oYErrData)) then return
        if (~oYErrData->GetData(arg3)) then return
        break
      end
      2 : begin
        ; y, y_error=ye
        arg1 = ydata
        oYErrData = self->GetParameter('Y ERROR')
        if (~OBJ_VALID(oYErrData)) then return
        if (~oYErrData->GetData(arg2)) then return
        break
      end
      1 : arg1 = ydata
      else :
    endswitch
  endif else begin
    switch (N_PARAMS()) of
      3 : begin
        oDataZ = oParamSet->GetByName('Z')
        arg3 = !NULL
        if (ISA(oDataZ) && oDataZ->GetData(zdata)) then $
          arg3 = TEMPORARY(zdata)
        end  ; fall thru
      2 : begin
        arg1 = TEMPORARY(xdata)
        arg2 = TEMPORARY(ydata)
        break
      end
      1 : arg1 = TEMPORARY(ydata)
      else :
    endswitch
  endelse

end


;----------------------------------------------------------------------------
; Purpose:
;   This method is used to directly set the data
;
; Arguments:
;   Varies
;
; Keywords:
;   NONE
;
pro IDLitVisPlot::_SetData, arg1, arg2, arg3, arg4, _EXTRA=_extra
  compile_opt idl2, hidden
  
  ; PLOT, POLARPLOT, BARPLOT and ERRORPLOT use the same idlitvisplot object
  ; so assume that we are working with ERRORPLOT if the "Y ERROR"
  ; parameter is a valid object.
  oParamSet = self->GetParameterSet()
  oDataYErr = oParamSet->GetByName('Y ERROR')
  
  if ISA( oDataYErr ) then begin
    case n_params() of
    2: self->PutData, arg1, YERROR=arg2, _EXTRA=_extra
    3: self->PutData, arg1, arg2, YERROR=arg3, _EXTRA=_extra
    4: self->PutData, arg1, arg2, XERROR=arg3, YERROR=arg4, _EXTRA=_extra
    else: message, 'Incorrect number of arguments.'
    endcase
  endif else begin
    case n_params() of
    1: self->PutData, arg1, _EXTRA=_extra
    2: self->PutData, arg1, arg2, _EXTRA=_extra
    else: message, 'Incorrect number of arguments.'
    endcase
  endelse

end

;----------------------------------------------------------------------------
; Purpose:
;   This method is used to directly set the data
;
; Arguments:
;   X, Y, and Z, although not necessarily in that order
;
; Keywords:
;   NONE
;
pro IDLitVisPlot::PutData, arg1, arg2, arg3, arg4, _EXTRA=_extra
  compile_opt idl2, hidden
  
  RESOLVE_ROUTINE, 'iPlot', /NO_RECOMPILE

  case (N_Params()) of
    0: void = iPlot_GetParmSet(oParmSet, _EXTRA=_extra)
    1: void = iPlot_GetParmSet(oParmSet, arg1, _EXTRA=_extra)
    2: void = iPlot_GetParmSet(oParmSet, arg1, arg2, _EXTRA=_extra)
    3: void = iPlot_GetParmSet(oParmSet, arg1, arg2, arg3, _EXTRA=_extra)
    4: void = iPlot_GetParmSet(oParmSet, arg1, arg2, arg3, arg4, _EXTRA=_extra)
  endcase

  ; Get the data from the parameterset and set the parameters
  oDataYErr = oParmSet->GetByName('Y ERROR')
  if (OBJ_VALID(oDataYErr) && OBJ_VALID(self._oYErrorBar)) then begin
    self._oYErrorBar->GetProperty, HIDE=yHide
    self->SetParameter, 'Y ERROR', oDataYErr
    oDataYErr->SetProperty, /AUTO_DELETE
    self._oYErrorBar->SetProperty, HIDE=yHide
  endif
  oDataXErr = oParmSet->GetByName('X ERROR')
  if (OBJ_VALID(oDataXErr) && OBJ_VALID(self._oXErrorBar)) then begin
    self._oXErrorBar->GetProperty, HIDE=xHide
    self->SetParameter, 'X ERROR', oDataXErr
    oDataXErr->SetProperty, /AUTO_DELETE
    self._oXErrorBar->SetProperty, HIDE=xHide
  endif
  oDataY = oParmSet->GetByName('Y')
  if (OBJ_VALID(oDataY)) then begin
    self->SetParameter, 'Y', oDataY
    oDataY->SetProperty, /AUTO_DELETE
  endif
  oDataX = oParmSet->GetByName('X')
  if (OBJ_VALID(oDataX)) then begin
     self->SetParameter, 'X', oDataX
    oDataX->SetProperty, /AUTO_DELETE
  endif
  oDataV = oParmSet->GetByName('VERTICES')
  if (OBJ_VALID(oDataV)) then begin
    self->SetParameter, 'VERTICES', oDataV
    oDataV->SetProperty, /AUTO_DELETE
  endif

  ;; Notify of changed data
  self->OnDataChangeUpdate, oParmSet, '<PARAMETER SET>'

  ;; Clean up parameterset
  oParmSet->Remove, /ALL
  OBJ_DESTROY, oParmSet

  ; Send a notification message to update UI
  self->DoOnNotify, self->GetFullIdentifier(),"ADDITEMS", ''
  self->OnDataComplete, self

  oTool = self->GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->RefreshCurrentWindow
  
end


;----------------------------------------------------------------------------
; Purpose:
;   This function method is used to edit a user-defined property.
;
; Arguments:
;   Tool: Object reference to the tool.
;
;   PropertyIdentifier: String giving the name of the userdef property.
;
; Keywords:
;   None.
;
function IDLitVisPlot::EditUserDefProperty, oTool, identifier

    compile_opt idl2, hidden

    case identifier of

        'VISUALIZATION_PALETTE': begin
            success = oTool->DoUIService('PaletteEditor', self)
            if success then begin
                return, 1
            endif
        end

        else:

    endcase

    ; Call our superclass.
    return, self->IDLitVisualization::EditUserDefProperty(oTool, identifier)

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::_UpdateSelectionVisual
;
; PURPOSE:
;      This procedure method updates the selection visual based
;      on the plot data.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]_UpdateSelectionVisual
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
pro IDLitVisPlot::_UpdateSelectionVisual

    compile_opt idl2, hidden

    self->IDLitComponent::GetProperty, INITIALIZING=isInit
    if (isInit) then return

    ; Shadow plot.

    ; Retrieve all properties necessary to recreate the shadow plot.
    self._oPlot->GetProperty, DATA=data, $
        DOUBLE=double, $
        HISTOGRAM=histogram, $
        LINESTYLE=linestyle, $
        MIN_VALUE=minv, MAX_VALUE=maxv, $
        NSUM=nsum, $
        POLAR=polar, $
        XRANGE=xrange, YRANGE=yrange, $
        ZVALUE=zvalue

    ; Make sure we have data to share.
    ndata = N_ELEMENTS(data)
    if (ndata eq 0) then $
        return

    ; If we have 5000 or more points (15000 data elements)
    ; then switch to a thick outline.
    doOutline = ndata ge 15000 || linestyle[0] eq 6
    
    if (doOutline) then begin
      data = double ? DBLARR(3,5) : FLTARR(3,5)
      data[0,*] = xrange[[0,1,1,0,0]]
      data[1,*] = yrange[[0,0,1,1,0]]
    endif

    self._oPlotSelectionVisual->SetProperty, $
        DATAX=data[0,*], DATAY=data[1,*], $
        DOUBLE=double, $
        HISTOGRAM=doOutline ? 0 : histogram, $
        MIN_VALUE=minv, MAX_VALUE=maxv, $
        NSUM=doOutline ? 1 : nsum, $
        POLAR=doOutline ? 0 : polar, $
        THICK=doOutline ? 10 : 6, $
        UVALUE=doOutline, $
        USE_ZVALUE=(zvalue ne 0), ZVALUE=zvalue

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::_VerifyErrorBars, dim
;
; PURPOSE:
;      This procedure method creates the error bar container
;      and visualizations.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]_VerifyErrorBars
;
; INPUTS:
;      dim: 0 for X, 1 for Y
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
pro IDLitVisPlot::_VerifyErrorBars, dim

  compile_opt idl2, hidden
  
  oError = dim ? self._oYErrorBar : self._oXErrorBar
  if (OBJ_VALID(oError)) then $
    return
    
  ; 22 = horizontal line; 21 = vertical line
  oError = OBJ_NEW('IDLitVisPolyline', $
    IMPACTS_RANGE=0, $
    SELECT_TARGET=0, $
    SYMBOL=dim ? 22 : 21, $
    /PRIVATE)
  self->Add, oError, /NO_UPDATE, /NO_NOTIFY
  
  if (dim) then begin
    self._oYErrorBar = oError
  endif else begin
    self._oXErrorBar = oError
  endelse

end


;----------------------------------------------------------------------------
pro IDLitVisPlot::_UpdateData, xrange, yrange

  compile_opt idl2, hidden
  
  if (self._equation eq '') then return
  if (self._withinUpdateData) then return
  self._withinUpdateData = 1b

  ; If we don't have the XRANGE then this is a "new" equation,
  ; so we should try to pick a new range.
  newXrange = 0b
  if (~ISA(xrange)) then begin
    newXrange = 1b
    xrange = [-10,10]
    oDS = self.GetDataSpace()
    if (ISA(oDS)) then begin
      oDS.GetProperty, X_MINIMUM=xMin, X_MAXIMUM=xMax, $
        Y_MINIMUM=yMin, Y_MAXIMUM=yMax, $
        X_AUTO_UPDATE=xAutoUpdate, Y_AUTO_UPDATE=yAutoUpdate
      if (~xAutoUpdate) then xrange = [xMin, xMax]
      yrange = [yMin, yMax]
    endif
  endif
  

  n = (self._eqnSamples gt 0) ? self._eqnSamples : 1000
  delta = DOUBLE(xrange[1] - xrange[0])/(n-1)
  x = DINDGEN(n)*delta + xrange[0]

  if (IDL_VALIDNAME(self._equation) && $
    STRLOWCASE(self._equation) ne 'x') then begin
    if (PTR_VALID(self._eqnUserdata)) then begin
      y = CALL_FUNCTION(self._equation, x, *self._eqnUserdata)
    endif else begin
      y = CALL_FUNCTION(self._equation, x)
    endelse
    success = N_ELEMENTS(y) eq N_ELEMENTS(x)
  endif else begin
    success = EXECUTE('y = ' + self._equation)
  endelse

  if (success) then begin
    good = WHERE(FINITE(y), /NULL)
    if (ISA(good)) then begin
      mn = MIN(good, MAX=mx)
      x = x[mn:mx]
      y = y[mn:mx]
    
        self->PutData, x, y
      
      if (~self._hasFillLevel) then begin
        if (newXrange) then begin
          if (ISA(oDS)) then begin
            oDS.GetProperty, Y_MINIMUM=yMin
            self._fillLevel = yMin
          endif
        endif else begin
          self._fillLevel = yrange[0]
        endelse
      endif
    endif
  endif

  void = CHECK_MATH()

  self._withinUpdateData = 0b
  
end

;----------------------------------------------------------------------------
; METHODNAME:
;      IDLitVisPlot::_UpdateErrorBars, data, dim
;
; PURPOSE:
;      This procedure method updates the error bar geometry based
;      on the plot data.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]_UpdateErrorBars
;
; INPUTS:
;      data: A vector of plot data.
;      dim: 0 for X, 1 for Y
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;
pro IDLitVisPlot::_UpdateErrorBars, dim

    compile_opt idl2, hidden

    oErrData = self->GetParameter((dim ? 'Y' : 'X') + ' ERROR')
    if (~OBJ_VALID(oErrData)) then $
        return
    if (~oErrData->GetData(errdata)) then $
        return

    ; Retrieve X and Y data.
    oDataY = self->GetParameter('Y')
    if (~OBJ_VALID(oDataY) || ~oDataY->GetData(ydata)) then $
        return

    self._oPlot->GetProperty, DOUBLE=isDouble

    ndata = N_ELEMENTS(ydata)

    oDataX = self->GetParameter('X')
    ; Construct a findgen X vector if necessary.
    if (~OBJ_VALID(oDataX) || ~oDataX->GetData(xdata) || $
        (N_ELEMENTS(xdata) ne ndata)) then $
        xdata = isDouble ? DINDGEN(ndata) : FINDGEN(ndata)

    self->_VerifyErrorBars, dim

    ErrorData = isDouble ? DBLARR(2, 2*ndata) : FLTARR(2, 2*ndata)
    
    ; If needed, force errdata to be 2D
    nErr = N_ELEMENTS(errdata)
    if ((ndata eq 1) && (nErr ne 1)) then $
      errdata = reform(errdata, nErr, 1)

    ; set the other dimension's coordinates of polyline data
    ; same coordinate for both values of the specified dimension
    ErrorData[~dim, 0:*:2] = dim ? xdata : ydata
    ErrorData[~dim, 1:*:2] = dim ? xdata : ydata

    case size(errdata, /n_dimensions) of
    1: begin
        ; vector of error values applied as both + and - error
        ErrorData[dim, 0:*:2] = (dim ? ydata : xdata) - errdata
        ErrorData[dim, 1:*:2] = (dim ? ydata : xdata) + errdata
    end
    2: begin
        ; 2xN array, [0,*] is negative error, [1,*] is positive error
        ErrorData[dim, 0:*:2] = (dim ? ydata : xdata) - errdata[0,*]
        ErrorData[dim, 1:*:2] = (dim ? ydata : xdata) + errdata[1,*]
    end
    else:
    endcase

    ; Handle logarithmic axes.
    if (self._xVisLog) then begin
        ErrorData[0, *] = alog10(ErrorData[0, *])
    endif
    if (self._yVisLog) then begin
        ErrorData[1, *] = alog10(ErrorData[1, *])
    endif

    polylineDescript = LONARR(3*ndata)
    polylineDescript[0:*:3] = 2
    polylineDescript[1:*:3] = lindgen(ndata)*2
    polylineDescript[2:*:3] = lindgen(ndata)*2+1

    ;; filter out non-finite values
    infY = where(~finite(ErrorData[1,0:*:2]))
    IF (infY[0] NE -1) THEN BEGIN
      ;; determine a valid data point to set the unneeded error bar
      ;; data.  This has to be done as the error bar data impacts the
      ;; range so using a generic value like 0 or 1d300 could change
      ;; the plot range
      validY = where(finite(ydata))
      validY = (validY[0] EQ -1) ? 0 : ydata[validY[0]]
      ErrorData[1,where(~finite(ErrorData[1,*]))] = validY
      nInf = n_elements(infY)
      polylineDescript[reform(infY*3##replicate(1,3),3*nInf)+ $
                       reform([0,1,2]#replicate(1,nInf),3*nInf)] = 0
    ENDIF
    infX = where(~finite(ErrorData[0,0:*:2]))
    IF (infX[0] NE -1) THEN BEGIN
      ;; determine a valid data point to set the unneeded error bar
      ;; data.  This has to be done as the error bar data impacts the
      ;; range so using a generic value like 0 or 1d300 could change
      ;; the plot range
      validX = where(finite(xdata))
      validX = (validX[0] EQ -1) ? 0 : xdata[validX[0]]
      ErrorData[0,where(~finite(ErrorData[0,*]))] = validX
      nInf = n_elements(infX)
      polylineDescript[reform(infX*3##replicate(1,3),3*nInf)+ $
                       reform([0,1,2]#replicate(1,nInf),3*nInf)] = 0
    ENDIF

    oError = dim ? self._oYErrorBar : self._oXErrorBar

    ; We save an extra copy of the polylines in the UVALUE,
    ; for use in clipping to the plot range.
    ; Retrieve HIDE property - it may be specified on command line
    ; and set prior to processing of the parameter
    oError->GetProperty, HIDE=hide
    oError->SetProperty, __DATA=temporary(ErrorData), $
      /IMPACTS_RANGE, $
      HIDE=hide, $   ; may be hid from dataDisconnect
      __POLYLINES=polylineDescript, UVALUE=polylineDescript

    ; display the properties, even if the error bars themselves are hidden
    self->SetPropertyAttribute, [(dim ? 'Y' : 'X') + '_ERRORBARS', $
        'ERRORBAR_CAPSIZE', 'ERRORBAR_COLOR'], HIDE=0

    self->_UpdateCapSize

end


;----------------------------------------------------------------------------
; METHODNAME:
;      IDLitVisPlot::_UpdateCapSize
;
; PURPOSE:
;      This procedure method scales the error bar geometry
;      to the dataspace data range.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]_UpdateCapSize
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;
pro IDLitVisPlot::_UpdateCapSize

  compile_opt idl2, hidden

  if (OBJ_VALID(self._oXErrorBar)) then begin
    self._oXErrorBar->SetProperty, SYM_SIZE=self._capSize*7
  endif
  
  if (OBJ_VALID(self._oYErrorBar)) then begin
    self._oYErrorBar->SetProperty, SYM_SIZE=self._capSize*7
  endif
    
end


;----------------------------------------------------------------------------
; METHODNAME:
;      IDLitVisPlot::_UpdateSymIncrement
;
; PURPOSE:
;      This procedure method updates the plot, spacing
;      symbols at the desired symbol increment.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]_UpdateSymIncrement
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;
pro IDLitVisPlot::_UpdateSymIncrement

    compile_opt idl2, hidden

    oSymArray = [self._oSymbol->GetSymbol()]

    ; Symbol increment of 1 means every point, don't insert spacer
    if (self._symIncrement gt 1) then begin
        oSymArray = [oSymArray, $
            REPLICATE(self._oSymbolSpacer, self._symIncrement-1)]
    endif

    self._oPlot->SetProperty, SYMBOL=oSymArray

    self->_UpdateSelectionVisual
end


;----------------------------------------------------------------------------
; METHODNAME:
;      IDLitVisPlot::_UpdateSymSize
;
; PURPOSE:
;      This procedure method updates the symbol size
;      to the dataspace range by retrieving and then
;      setting the symbol size.  The IDLitSymbol object
;      scales the symbol to the dataspace when the size
;      is set.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]_UpdateSymSize
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;
pro IDLitVisPlot::_UpdateSymSize

    compile_opt idl2, hidden

    ; Setting the symbol size causes internal scaling to the data range
    ; Retrieve it's normalized value, and reset it to the same value
    ; to trigger the internal scaling.
    self._oSymbol->GetProperty, SYM_SIZE=symSize
    self._oSymbol->SetProperty, SYM_SIZE=symSize

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::_UpdateFill
;
; PURPOSE:
;      This procedure method updates the polygon representing
;      the filled area under the plot.  It must be updated when
;      the fill level (the lower boundary) changes or when going
;      into or out of histogram mode, for example.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisPlot::]_UpdateFill
;
; INPUTS:
;      DataspaceX/Yrange: Optional args giving the dataspace ranges.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
pro IDLitVisPlot::_UpdateFill, $
    dataspaceXrange, dataspaceYrange, dataspaceZrange

    compile_opt idl2, hidden

    sMap = self->GetProjection()
    hasMap = N_TAGS(sMap) gt 0

    ; Only update if we have a map or are filled.
    if (~hasMap && ~OBJ_VALID(self._oFill)) then $
        return

    ; Retrieve X and Y data.
    oDataY = self->GetParameter('Y')
    if ((~OBJ_VALID(oDataY)) || (~oDataY->GetData(ydata))) then $
        return

    ndata = N_ELEMENTS(ydata)

    oDataX = self->GetParameter('X')
    ; Construct a findgen X vector if necessary.
    if ((~OBJ_VALID(oDataX)) || (~oDataX->GetData(xdata)) || $
        (N_ELEMENTS(xdata) ne ndata)) then $
        xdata = FINDGEN(ndata)


    ; If we have data values out of the normal lonlat range, then
    ; assume these are not coordinates in degrees.
    if (hasMap) then begin
      minn = MIN(xdata, MAX=maxx)
      if (minn lt -360 || maxx gt 720) then hasMap = 0
      minn = MIN(ydata, MAX=maxx)
      if (minn lt -90 || maxx gt 90) then hasMap = 0
    endif

    if (hasMap) then begin
      xydata = MAP_PROJ_FORWARD(xdata, ydata, MAP=sMap)
      xdata = REFORM(xydata[0,*])
      ydata = REFORM(xydata[1,*])
      xydata = 0
      self._oPlot->SetProperty, DATAX=xdata, DATAY=ydata
    endif

    if (~OBJ_VALID(self._oFill)) then $
      return

    self._oPlot->GetProperty, $
        DOUBLE=isDouble, $
        HISTOGRAM=doHist, $
        MIN_VALUE=minValue, $
        MAX_VALUE=maxValue, $
        NSUM=nsum


    ; Check if we need to average points together.
    if (nsum gt 1) then begin

        ; If nsum is >= to the # of points, bail.
        if (nsum ge ndata) then begin
            ; Setting the fill to a scalar will cause it to be hidden.
            self._oFill->SetProperty, DATA=0
            return   ; we're done
        endif

        xdata = isDouble ? DOUBLE(xdata) : FLOAT(xdata)
        ydata = isDouble ? DOUBLE(ydata) : FLOAT(ydata)

        ; Remove degen dimensions for REBIN
        xdata = REFORM(xdata, /OVERWRITE)
        ydata = REFORM(ydata, /OVERWRITE)

        if ((ndata mod nsum) eq 0) then begin
            xdata = REBIN(xdata, (ndata + nsum - 1)/nsum)
            ydata = REBIN(ydata, (ndata + nsum - 1)/nsum)
        endif else begin
            nint = (ndata/nsum)*nsum
            xdata = [REBIN(xdata[0:nint-1], ndata/nsum), $
                TOTAL(xdata[nint:*])/(ndata-nint)]
            ydata = [REBIN(ydata[0:nint-1], ndata/nsum), $
                TOTAL(ydata[nint:*])/(ndata-nint)]
        endelse

        ; New number of data values.
        ndata = N_ELEMENTS(ydata)
    endif


    ; Create default FILL_LEVEL if necessary.
    ; We use a tiny number as a flag to indicate it is currently undefined.
    if (self._fillLevel eq 1d-300) then begin
        self._fillLevel = MIN(ydata, /NAN)
        ; Start showing the actual value.
        self->SetPropertyAttribute, 'FILL_LEVEL', UNDEFINED=0
    endif


    ; For a histogram style, duplicate the X and Y points.
    if (doHist eq 2) then begin
      ; Our STAIRSTEP=1
      ; Duplicate the X points, but subtract/add an offset
      ; equal to half the distance between neighboring points.
      dx = (ndata gt 1) ? (xdata[1:*] - xdata[0:ndata-2])/2d : 1
      dx = [dx[0], dx, dx[-1]]
      xdata1 = isDouble ? DBLARR(2*ndata) : FLTARR(2*ndata)
      xdata1[0:*:2] = xdata - dx[0:ndata-1]
      xdata1[1:*:2] = xdata + dx[1:*]
      xdata = TEMPORARY(xdata1)
      ; Duplicate all of the y data.
      ydata = REFORM(REBIN(REFORM(ydata, 1, ndata), 2, ndata), 2*ndata)
    endif else if (doHist eq 3) then begin
      ; Our HISTOGRAM=1
      ; Duplicate the X points, and tack on the last point, at a distance
      ; past the end equal to the delta between the last 2 points.
      xdata1 = isDouble ? DBLARR(2*ndata) : FLTARR(2*ndata)
      xdata1[0:*:2] = xdata
      xdata1[1:-3:2] = xdata[1:*]
      xdata1[-1] = xdata[-1] + (xdata[-1] - xdata[-2])
      xdata = TEMPORARY(xdata1)
      ; Duplicate all of the y data.
      ydata = REFORM(REBIN(REFORM(ydata, 1, ndata), 2, ndata), 2*ndata)
    endif

    fillLevel = self._fillLevel

    bad = 1b
    if (FINITE(minValue)) then bad = ydata lt minValue
    if (FINITE(maxValue)) then bad = bad or ydata gt maxValue
    if (N_ELEMENTS(bad) gt 1) then begin
      missing = WHERE(bad, /NULL)
      if (ISA(missing)) then begin
        ydata[missing] = fillLevel
      endif
    endif


    ; Restrict fill level to be within dataspace range.
    if (N_ELEMENTS(dataspaceYrange) ne 2) then begin
        oDataSpace = self->GetDataSpace(/UNNORMALIZED)
        if (OBJ_VALID(oDataSpace)) then begin
            oDataSpace->GetProperty, Y_AUTO_UPDATE=yAutoUpdate, $
                Y_MINIMUM=yMin, Y_MAXIMUM=yMax
            ; If y_auto_update is false then our fill needs
            ; to match the dataspace. If y_auto_update is true then
            ; because impacts_range=1 for our fill, the dataspace
            ; y range should change to match our fill range.
            dataspaceYrange = yAutoUpdate ? $
              [yMin < fillLevel, yMax > fillLevel] : [yMin, yMax]
        endif
    endif

    if (N_ELEMENTS(dataspaceYrange) eq 2) then begin
        fillLevel = dataspaceYrange[0] > fillLevel < dataspaceYrange[1]
    endif

    ndata = N_ELEMENTS(ydata)

    ; If no points survived the clipping, bail early.
    if (ndata eq 0) then begin   ; Clip everything
        ; Setting the fill to a scalar will cause it to be hidden.
        self._oFill->SetProperty, DATA=0
        return   ; we're done
    endif


    ; Construct our (2xN) data array.
    self._oPlot->GetProperty, ZVALUE=zvalue
    useZvalue = (zvalue ne 0)

    isFinite = FINITE(xdata) and FINITE(ydata)

    if (ARRAY_EQUAL(isFinite, 1)) then begin
      filldata = isDouble ? DBLARR(2+useZvalue, ndata + 3, /NOZERO) : $
        FLTARR(2+useZvalue, ndata + 3, /NOZERO)
        
      ; Fill in all the data points.
      filldata[0,1:ndata] = xdata
      filldata[1,1:ndata] = ydata
      
      ; Fill in the first and last points.
      filldata[0:1,0] = [xdata[0], fillLevel]
      filldata[0:1,ndata+1] = [xdata[ndata-1], fillLevel]
      filldata[0:1,ndata+2] = [xdata[0], fillLevel]
    endif else begin
      pos = [-1,UNIQ(isFinite)]
      if (isFinite[0] eq 1 && isFinite[-1] eq 1) then $
        pos = [pos, N_ELEMENTS(isFinite)-1]
      polygons = LIST()
      filldata = LIST()
      indexPoly = 0L
      for i=1,N_ELEMENTS(pos)-1 do begin
        if (~isFinite[pos[i]]) then continue
        n = pos[i] - pos[i-1]
        if (n lt 2) then continue
        polygons.Add, [n+3, indexPoly + LINDGEN(n+3)]
        indexPoly += n + 3
        filldata1 = isDouble ? DBLARR(2+useZvalue, n + 3, /NOZERO) : $
          FLTARR(2+useZvalue, n + 3, /NOZERO)
        dataIndex = pos[i-1] + 1 + LINDGEN(n)
        filldata1[0,1:n] = xdata[dataIndex]
        filldata1[1,1:n] = ydata[dataIndex]
        filldata1[0:1,0] = [xdata[dataIndex[0]], fillLevel]
        filldata1[0:1,n+1] = [xdata[dataIndex[-1]], fillLevel]
        filldata1[0:1,n+2] = [xdata[dataIndex[0]], fillLevel]
        filldata.Add, filldata1
      endfor
      polygons = polygons.ToArray(DIMENSION=1)
      if (N_ELEMENTS(polygons) eq 0) then return
      filldata = filldata.ToArray(DIMENSION=2)
    endelse

    if (useZvalue) then $
        filldata[2,*] = zvalue

    self._oFill->SetProperty, DATA=filldata, POLYGONS=polygons

end


;----------------------------------------------------------------------------
; If Data is an integer array, convert to double if the
; integer range requires it.
;
pro IDLitVisPlot::_ConvertDouble, data
  compile_opt hidden, idl2
  
  if (~ISA(data) || ISA(data, 'FLOAT') || ISA(data, 'DOUBLE')) then return
  
  mx = MAX(data, MIN=mn)
  mxabs = ABS(mn) > ABS(mx)
  if (mxabs gt 2000000 || (mx - mn) gt 2000000) then begin
    data = DOUBLE(data)
  endif
  void = CHECK_MATH()
end


;----------------------------------------------------------------------------
; IIDLDataObserver Interface
;----------------------------------------------------------------------------
;; IDLitVisPlot::OnDataDisconnect
;;
;; Purpose:
;;   This is called by the framework when a data item has disconnected
;;   from a parameter on the plot.
;;
;; Parameters:
;;   ParmName   - The name of the parameter that was disconnected.
;;
PRO IDLitVisPlot::OnDataDisconnect, ParmName
   compile_opt hidden, idl2

   ;; Just check the name and perform the desired action
   case ParmName of
       'X': begin
           self._oPlot->GetProperty, data=data
           if (N_ELEMENTS(data) eq 0) then break
           szDims = size(data,/dimensions)
           szdims = (size(data,/n_dimensions) eq 1) ? [szdims,1] : szdims 
           data=0b
           self._oPlot->SetProperty, datax=indgen(szDims[1])
           self->_UpdateSelectionVisual
       end
       'Y': begin
           self._oPlot->SetProperty, datax=[0,1], datay=[0,1], $
                MIN_VALUE=!values.d_nan, MAX_VALUE=!values.d_nan
           self->SetPropertyAttribute, ['MIN_VALUE', 'MAX_VALUE'], /UNDEFINED
           self->_UpdateSelectionVisual
           self._oPlot->SetProperty, /HIDE
       end
       'X ERROR': begin
           ; hide the error bars and their properties
           self._oXErrorBar->Setproperty, /HIDE
           self->SetPropertyAttribute, 'X_ERRORBARS', /HIDE
           self->GetPropertyAttribute, 'Y_ERRORBARS', HIDE=hideY
           if (hideY) then begin
           self->SetPropertyAttribute, $
               ['ERRORBAR_CAPSIZE', 'ERRORBAR_COLOR'], /HIDE
           endif

           ; recompute data range to eliminate effect of errorbars
           self._oXErrorBar->SetProperty, IMPACTS_RANGE=0
           self->OnDataChange, self
           self->OnDataComplete, self
       end
       'Y ERROR': begin
           ; hide the error bars and their properties
           self._oYErrorBar->Setproperty, /HIDE
           self->SetPropertyAttribute, 'Y_ERRORBARS', /HIDE
           self->GetPropertyAttribute, 'X_ERRORBARS', HIDE=hideX
           if (hideX) then begin
           self->SetPropertyAttribute, $
               ['ERRORBAR_CAPSIZE', 'ERRORBAR_COLOR'], /HIDE
           endif

           ; recompute data range to eliminate effect of errorbars
           self._oYErrorBar->SetProperty, IMPACTS_RANGE=0
           self->OnDataChange, self
           self->OnDataComplete, self
       end
       'VERTEX_COLORS':begin
           self._oPlot->SetProperty, VERT_COLORS=0
       end

       'PALETTE': begin
            self._oPalette->SetProperty, $
                RED_VALUES=BINDGEN(256), $
                GREEN_VALUES=BINDGEN(256), $
                BLUE_VALUES=BINDGEN(256)
            self->SetPropertyAttribute, 'VISUALIZATION_PALETTE', SENSITIVE=0
           end

       else:
   endcase

    ; Since we are changing a bunch of attributes, notify
    ; our observers in case the prop sheet is visible.
    self->DoOnNotify, self->GetFullIdentifier(), 'SETPROPERTY', ''

end


;----------------------------------------------------------------------------
; METHODNAME:
;    IDLitVisPlot::OnDataChangeUpdate
;
; PURPOSE:
;    This procedure method is called by a Subject via a Notifier when
;    its data has changed.  This method obtains the data from the
;    subject and updates the internal IDLgrPlot object.
;
; CALLING SEQUENCE:
;
;    Obj->[IDLitVisPlot::]OnDataChangeUpdate, oSubject, parmName
;
; INPUTS:
;    oSubject: The Subject object in the Subject-Observer relationship.
;    This object (the plot) is the observer, so it uses the
;    IIDLDataSource interface to get the data from the subject.
;    Then it puts the data in the IDLgrPlot object.
;
;    parmName: The name of the registered parameter.
;
; KEYWORDS:
;   NO_UPDATE: Undocumented keyword to suppress updates when adding
;       multiple data objects within a parameter set.
;
pro IDLitVisPlot::OnDataChangeUpdate, oSubject, parmName, $
    NO_UPDATE=noUpdate

    compile_opt idl2, hidden

    case strupcase(parmName) of
    '<PARAMETER SET>':begin
        ;; Get our data
        position = oSubject->Get(/ALL, count=nCount, NAME=name)
        for i=0, nCount-1 do begin
            if (name[i] eq '') then $
                continue
            oData = (oSubject->GetByName(name[i]))[0]
            if (~OBJ_VALID(oData)) then $
                continue
            if (oData->GetData(data, NAN=nan) le 0) then $
                continue

            case name[i] of

            'Y': begin
              if (~oData->GetData(ydata, NAN=nan)) then $
                break
              oDataX = oSubject->GetByName('X')
              if (OBJ_VALID(oDataX)) then begin
                void = oDataX->GetData(xdata, NAN=nan)
              endif

              void = self._oPlot->GetDataXYZRange(xRange, yRange, zRange)
              oDataSpace = self->GetDataSpace(/UNNORMALIZED)
              if (OBJ_VALID(oDataSpace)) then begin
                oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, $
                  XRANGE=xRange, YRANGE=yRange, ZRANGE=zRange
                self._xVisLog = xLog
                self._yVisLog = yLog
                if (yLog) then $
                  ydata = ALOG10(ydata)
                if (xLog && ISA(xdata)) then $
                  xdata = ALOG10(xdata)
              endif

              self->_ConvertDouble, xdata
              self->_ConvertDouble, ydata

              self._oPlot->SetProperty, DATAX=xdata, DATAY=ydata, HIDE=0
              self->SetPropertyAttribute, ['MIN_VALUE', 'MAX_VALUE'], HIDE=0
              self->IDLitVisPlot::OnDataChangeUpdate, oData, 'Y', /NO_UPDATE

              self->OnDataRangeChange, self, xRange, yRange, zRange

              self->_UpdateFill
              self->_UpdateSymSize        ; handle the case for an overplot with no range change
              self->_UpdateSelectionVisual


            end

            'X':  ; X is handled in the Y branch to control order

            ; Pass all other parameters on to ourself.
            else: self->IDLitVisPlot::OnDataChangeUpdate, oData, name[i]

            endcase

        endfor
        end

    'X': BEGIN
        if (~oSubject->GetData(data)) then $
            break
        ;; Retrieve the range of the plot data in case dataspace does
        ;; not exist
        void = self._oPlot->GetDataXYZRange(xRange, yRange, zRange)
        oDataSpace = self->GetDataSpace(/UNNORMALIZED)
        if (OBJ_VALID(oDataSpace)) then begin
            oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, $
                                     XRANGE=xRange, YRANGE=yRange, ZRANGE=zRange
            self._xVisLog = xLog
            self._yVisLog = yLog
            if (xLog gt 0) then $
                data=alog10(data)
        endif
        self._oPlot->SetProperty, DATAX=data
        if (~KEYWORD_SET(noUpdate)) then begin
          ;; Call OnDataChangeUpdate to update the visual stuff
          self->OnDataRangeChange, self, xRange, yRange, zRange
          self->_UpdateSelectionVisual
        endif

    END

    'Y': BEGIN
        if (~oSubject->GetData(data, NAN=nan)) then $
            break
        ;; Retrieve the range of the plot data in case dataspace does
        ;; not exist
        void = self._oPlot->GetDataXYZRange(xRange, yRange, zRange)
        oDataSpace = self->GetDataSpace(/UNNORMALIZED)
        if (OBJ_VALID(oDataSpace)) then begin
            oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, $
                                     XRANGE=xRange, YRANGE=yRange, ZRANGE=zRange
            self._xVisLog = xLog
            self._yVisLog = yLog
            if (yLog gt 0) then $
                data=alog10(data)
        endif
        self._oPlot->SetProperty, DATAY=data, HIDE=0
        self->SetPropertyAttribute, ['MIN_VALUE', 'MAX_VALUE'], HIDE=0
        if (~KEYWORD_SET(noUpdate)) then begin
          ;; Call OnDataChangeUpdate to update the visual stuff
          self->OnDataRangeChange, self, xRange, yRange, zRange
          self->_UpdateSelectionVisual
        endif

    END

    'VERTICES': BEGIN
        if (~oSubject->GetData(data)) then $
            break
        ; Sanity check.
        if ((SIZE(data, /DIM))[0] gt 3) then $
            break
        ;; Retrieve the range of the plot data in case dataspace does
        ;; not exist
        void = self._oPlot->GetDataXYZRange(xRange, yRange, zRange)
        oDataSpace = self->GetDataSpace(/UNNORMALIZED)
        if (OBJ_VALID(oDataSpace)) then $
            oDataSpace->GetProperty, XRANGE=xRange, YRANGE=yRange, ZRANGE=zRange
        self._oPlot->SetProperty, DATAY=data[1,*]
        self._oPlot->SetProperty, DATAX=data[0,*]
        if (~KEYWORD_SET(noUpdate)) then begin
          ;; Call OnDataChangeUpdate to update the visual stuff
          self->OnDataRangeChange, self, xRange, yRange, zRange
          self->_UpdateSelectionVisual
        endif

    END

    'X ERROR': begin
        self->_UpdateErrorBars, 0
        self->_UpdateSelectionVisual
      end

    'Y ERROR': begin
        self->_UpdateErrorBars, 1
        self->_UpdateSelectionVisual
      end

    'PALETTE': begin
        if (~oSubject->GetData(data)) then $
            break
        ; Sanity check.
        if ((SIZE(data, /DIM))[0] gt 3) then $
            break
        if (size(data, /TYPE) ne 1) then data=bytscl(temporary(data))
        self._oPalette->SetProperty, $
            RED_VALUES=data[0,*], $
            GREEN_VALUES=data[1,*], $
            BLUE_VALUES=data[2,*]
        self->SetPropertyAttribute, 'VISUALIZATION_PALETTE', /SENSITIVE
        oVertColors = self->GetParameter('VERTEX_COLORS')
        if OBJ_VALID(oVertColors) then begin
            success = oVertColors->GetData(colors)
        endif
        if ~OBJ_VALID(oVertColors) || $
            (size(colors, /n_dimensions) gt 1) then begin
            ; define default indices
            oVertColorsDefault = OBJ_NEW('IDLitDataIDLVector', BINDGEN(256), $
                NAME='<DEFAULT INDICES>')
            result = self->SetData(oVertColorsDefault, $
                PARAMETER_NAME='VERTEX_COLORS',/by_value)
        endif
        end

    'VERTEX_COLORS': begin
        if (~oSubject->GetData(data)) then $
            break
        if (size(data, /TYPE) ne 1) then data=bytscl(temporary(data))
        self._oPlot->SetProperty, VERT_COLORS=data

        oRgbTable = self->GetParameter('PALETTE')
        if ~OBJ_VALID(oRgbTable) && $
            (size(data, /n_dimensions) eq 1) then begin
            ; define default palette, allows editing colors
            ; only used if vertex colors parameter is supplied
            ; and vertex colors are indices not colors.
            ramp = BINDGEN(256)
            colors = transpose([[ramp],[ramp],[ramp]])
            oColorTable = OBJ_NEW('IDLitDataIDLPalette', colors, NAME='RGB Table')

            ;; Set the data as by_value, so the parameter interface
            ;; will manage it.
            result = self->SetData(oColorTable, PARAMETER_NAME='PALETTE',/by_value)
        endif
        end

    else: ; ignore unknown parameters

    endcase


    ; Since we are changing a bunch of attributes, notify
    ; our observers in case the prop sheet is visible.
    self->DoOnNotify, self->GetFullIdentifier(), 'SETPROPERTY', ''

end


;----------------------------------------------------------------------------
pro IDLitVisPlot::OnProjectionChange, sMap
  compile_opt idl2, hidden
  ; Make sure to clip the line to the projection's limits.
  self->_UpdateFill
  oDataSpace = self->GetDataSpace(/UNNORMALIZED)
  if (OBJ_VALID(oDataSpace)) then begin
    oDataSpace->GetProperty, XRANGE=xRange, YRANGE=yRange, ZRANGE=zRange
    self->OnDataRangeChange, oSubject, XRange, YRange, ZRange
  endif
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::GetSymbol
;
; PURPOSE:
;      This function method returns the symbol associated with
;      the plot.  This allows the legend to retrieve the object
;      reference to obtain symbol properties directly.
;
; CALLING SEQUENCE:
;      oSymbol = Obj->[IDLitVisPlot::]GetSymbol
;
; RETURN VALUE:
;      Object reference to the symbol associated with the plot.
;
; INPUTS:
;      There are no inputs for this method.
;
; OUTPUTS:
;      There are no outputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
function IDLitVisPlot::GetSymbol

    compile_opt idl2, hidden

    return, self._oSymbol

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::GetDataString
;
; PURPOSE:
;      Convert XY dataspace coordinates into actual data values.
;
; CALLING SEQUENCE:
;      strDataValue = Obj->[IDLitVisPlot::]GetDataString
;
; RETURN VALUE:
;      String value representing the specified data values.
;
; INPUTS:
;      3 element vector containing X,Y and Z data coordinates.
;
; OUTPUTS:
;      There are no outputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
function IDLitVisPlot::GetDataString, xyz

    compile_opt idl2, hidden

    xy = xyz[0:1]

    ; IDL-68894: The XYZ should already be in data coordinates.
    ; You should not need to check x/yVisLog and do 10^x or 10^y.
    ; If you think that you do, you should fix the caller of this function.

    void = CHECK_MATH()  ; swallow underflow errors

    ; If our plot is selected, then lock the reported coordinates to the
    ; nearest data point.
    isSelected = self.IsSelected()
    if (isSelected) then begin
      ; Use the undocumented _DATASPACE keyword in case we are a barplot
      ; with horizontal orientation.
      xy = self->GetValueAtLocation(xy[0], xy[1], /_DATASPACE)
      if (~ISA(xy)) then return, ''
    endif

    if (self._polar) then begin
      r = SQRT(xy[0]^2 + xy[1]^2)
      theta = ATAN(xy[1], xy[0])*180/!PI
      return, 'r: ' + STRTRIM(STRING(r, FORMAT='(G11.4)'),2) + $
        '  angle: ' + STRTRIM(STRING(theta, FORMAT='(G11.4)'),2)
    endif

    xy = STRCOMPRESS(STRING(xy, FORMAT='(G11.4)'))
    return, STRING(xy, FORMAT='("X: ",A,"  Y: ",A)')

end


;----------------------------------------------------------------------------
; IIDLDataRangeObserver Interface
;----------------------------------------------------------------------------


;----------------------------------------------------------------------------
; Override this method so we can take the FILL_LEVEL into account.
;
function IDLitVisPlot::GetXYZRange, $
    outxRange, outyRange, outzRange, $
    DATA=bDataRange, $
    NO_TRANSFORM=noTransform

    compile_opt idl2, hidden

    success = self->_IDLitVisualization::GetXYZRange(outxRange, outyRange, outzRange, $
      DATA=bDataRange, NO_TRANSFORM=noTransform)

    ; If we are using min/max_value to clip the Y values, we still want
    ; the X range to cover the entire plot (even the clipped values).
    ; This agrees with the Direct Graphics behavior, and makes more sense.
    ; However, IDLgrPlot::GetDataXYZRange only returns the "clipped" X range.
    ; So, retrieve the data and get its full unclipped X range.
    self._oPlot->GetProperty, MIN_VALUE=minValue, MAX_VALUE=maxValue
    if (FINITE(minValue) || FINITE(maxValue)) then begin
      self._oPlot->GetProperty, DATA=data
      minn = MIN(data, DIMENSION=2, MAX=maxx)
      outxRange[0] = outxRange[0] < minn[0]
      outxRange[1] = outxRange[1] > maxx[0]
    endif

    ; If our fill level has been set, and fill is turned on, then extend
    ; the Y range to include the fill level if necessary.
    if (success && self._hasFillLevel && ISA(self._oFill)) then begin
      self._oFill->GetProperty, HIDE=fillHide
      if (~fillHide) then begin
        fillLevel = self._yVisLog ? ALOG10(self._fillLevel) : self._fillLevel
        outyRange = [outyRange[0] < fillLevel, outyRange[1] > fillLevel]
      endif
    endif

    return, success
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::OnDataRangeChange
;
; PURPOSE:
;      This procedure method handles notification that the data range
;      has changed.
;
; CALLING SEQUENCE:
;    Obj->[IDLitVisPlot::]OnDataRangeChange, oSubject, $
;          XRange, YRange, ZRange
;
; INPUTS:
;      oSubject:  A reference to the object sending notification
;                 of the data range change.
;      XRange:    The new xrange, [xmin, xmax].
;      YRange:    The new yrange, [ymin, ymax].
;      ZRange:    The new zrange, [zmin, zmax].
;
; OUTPUTS:
;      There are no outputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
pro IDLitVisPlot::OnDataRangeChange, oSubject, XRange, YRange, ZRange

    compile_opt idl2, hidden

    if (self._equation ne '') then begin
      self->_UpdateData, XRange, YRange
      self->_UpdateFill
      self->_UpdateSelectionVisual
    endif

    ; Determine if the dataspace range requires double precision.
    ; If so, set the plot data to double precision.
    oDataSpace = self->GetDataSpace(/UNNORMALIZED)
    if (OBJ_VALID(oDataSpace)) then begin
        if (oDataSpace->RequiresDouble()) then $
            self._oPlot->SetProperty, /DOUBLE
    endif

    self._oPlotSelectionVisual->GetProperty, UVALUE=doOutline

    ; If we have a huge # of points and we are only doing an outline,
    ; make sure to restrict the outline range to the dataspace range.
    if (ISA(doOutline) && KEYWORD_SET(doOutline)) then begin
      self._oPlot->GetProperty, DOUBLE=double, $
        XRANGE=xPlotRange, YRANGE=yPlotRange
      xPlotRange[0] = MIN(xRange) > MIN(xPlotRange)
      xPlotRange[1] = MAX(xRange) < MAX(xPlotRange)
      yPlotRange[0] = MIN(yRange) > MIN(yPlotRange)
      yPlotRange[1] = MAX(yRange) < MAX(yPlotRange)
      data = double ? DBLARR(3,5) : FLTARR(3,5)
      data[0,*] = xPlotRange[[0,1,1,0,0]]
      data[1,*] = yPlotRange[[0,0,1,1,0]]
      self._oPlotSelectionVisual->SetProperty, $
        DATAX=data[0,*], DATAY=data[1,*]
    endif

    self->_IDLitVisualization::ClipToDataRange, XRange, YRange, ZRange

end


;----------------------------------------------------------------------------
; METHODNAME:
;   IDLitVisPlot::GetHitVisualization
;
; PURPOSE:
;   Overrides the default method, and always returns myself. Therefore, if
;   you click on the filled portion, you get the plot instead.
;
;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisPlot::GetHitVisualization
;
; PURPOSE:
;      This procedure method overrides the default method, and
;      always returns the self object reference. Therefore, if
;      a click occurs on the filled portion, you get the plot.
;
; CALLING SEQUENCE:
;    Obj->[IDLitVisPlot::]GetHitVisualization, oSubHitList
;
; INPUTS:
;      oSubHitList:  A reference to the object hit
;
; OUTPUTS:
;      There are no outputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
function IDLitVisPlot::GetHitVisualization, oSubHitList
    compile_opt idl2, hidden
    return, self
end


;----------------------------------------------------------------------------
; Purpose:
;   Override the superclass' method. We keep our selection visual in sync
;   with our visualization using SetProperty, so we don't need to
;   do any updates here.
;
pro IDLitVisPlot::UpdateSelectionVisual
    compile_opt idl2, hidden
    ; Do nothing.
end


;----------------------------------------------------------------------------
function IDLitVisPlot::GetValueAtLocation, arg1, arg2, $
  DATA=data, DEVICE=device, NORMAL=normal, $
  INTERPOLATE=interpolate, _DATASPACE=swallow
  
  compile_opt idl2, hidden

  if (N_PARAMS() lt 1) then MESSAGE, 'Incorrect number of arguments'
  hasY = N_ELEMENTS(arg2) gt 0
  xloc = arg1
  if hasY then yloc = arg2

  ; IDL-68894: The XYZ should already be in data coordinates.
  ; You should not need to check x/yVisLog and do 10^x or 10^y.
  ; If you think that you do, you should fix the caller of this function.

  if((keyword_set(device)) || (keyword_set(normal))) then begin
    converted = iconvertcoord(xloc, hasY ? yloc : 0, $
      DEVICE=device, NORMAL=normal, /TO_DATA)
    xloc = converted[0]
    if hasY then yloc = converted[1]
  endif

  oParamSet = self->GetParameterSet()
  oDataY = oParamSet->GetByName('Y')
  if (~ISA(oDataY) || ~oDataY->GetData(ydata)) then return, !NULL
  ndata = N_ELEMENTS(ydata)
  oDataX = oParamSet->GetByName('X')
  if (~OBJ_VALID(oDataX) || ~oDataX->GetData(xdata) || $
    (N_ELEMENTS(xdata) ne ndata)) then begin
    xdata = ISA(ydata, "FLOAT") ? FINDGEN(ndata) : DINDGEN(ndata)
  endif

  if (self._polar) then begin
    if (N_PARAMS() lt 2) then MESSAGE, 'Polarplot requires X and Y arguments.'
    
    xx = xdata*COS(ydata)
    yy = xdata*SIN(ydata)
    diff = (xx-xloc)^2 + (yy-yloc)^2
    if (~KEYWORD_SET(interpolate)) then begin
      minn = MIN(diff, mnloc)
      xloc = xx[mnloc]
      yloc = yy[mnloc]
    endif else begin
      ; Find the two closest data points.
      s = SORT(diff)
      loc1 = s[0]
      loc2 = s[1]
      x1 = xx[loc1]
      y1 = yy[loc1]
      x2 = xx[loc2]
      y2 = yy[loc2]
      ; Do a bunch of geometry.
      r2 = (x2 - x1)^2 + (y2 - y1)^2
      b2 = (xloc - x1)^2 + (yloc - y1)^2
      a2 = (xloc - x2)^2 + (yloc - y2)^2
      d_over_r = 0.5 + 0.5*(b2 - a2)/r2
      yloc = y1 + d_over_r*(y2 - y1)
      xloc = x1 + d_over_r*(x2 - x1)
    endelse
  endif else begin
    if (~KEYWORD_SET(interpolate)) then begin
      minn = MIN(ABS(xdata - xloc), mnloc)
      xloc = xdata[mnloc]
      yloc = ydata[mnloc]
    endif else begin ; interpolate
      ; find 2 nearest points
      minn = MIN(ABS(xdata - xloc), loc1)
      datalength = n_elements(xdata)
    
      if(loc1 eq datalength-1) then begin
        loc2 = loc1-1
      endif else if (loc1 eq 0) then begin
        loc2 = loc1+1
      endif else begin
        if (xloc lt xdata[loc1]) then loc2 = loc1-1 else loc2 = loc1+1
      endelse
      
      yloc = (((ydata[loc2]-ydata[loc1])/(xdata[loc2]-xdata[loc1])) * $
            (xloc-xdata[loc2])) + ydata[loc2]
    endelse
  endelse

  value = [xloc, yloc]
  return, value

end


;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; IDLitVisPlot__Define
;
; PURPOSE:
;      Defines the object structure for an IDLitVisPlot object.
;
; INPUTS:
;      There are no inputs for this method.
;
; OUTPUTS:
;      There are no outputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;
;-
;----------------------------------------------------------------------------
pro IDLitVisPlot__Define

    compile_opt idl2, hidden

    struct = { IDLitVisPlot,           $
        inherits IDLitVisualization, $   ; Superclass: _IDLitVisualization
        _oPlot: OBJ_NEW(),          $   ; IDLgrPlot object
        _oPlotSelectionVisual: OBJ_NEW(),          $   ; IDLgrPlot object
        _oPalette: OBJ_NEW(), $
        _oSymbol: OBJ_NEW(),    $
        _oSymbolSelection: OBJ_NEW(),    $  ; left in for IDL61 BC
        _oSymbolSpacer: OBJ_NEW(),    $
        _oXErrorBar: OBJ_NEW(),    $
        _oYErrorBar: OBJ_NEW(),    $
        _oFill: OBJ_NEW(), $
        _xVisLog: 0L,  $
        _yVisLog: 0L,  $
        _symIncrement: 1L,  $ ;default is 1 for every vertex
        _capSize: 0d,   $
        _maxSet: 0b,   $   ; needed for IDL60 backwards compat
        _minSet: 0b,   $   ; needed for IDL60 backwards compat
        _polar: 0b, $
        _wasFilled: 0b, $
        _hasFillLevel: 0b, $
        _fillColor: bytarr(3), $
        _fillTransparency: 0, $
        _fillLevel: 0.0d, $
        _equation: '', $
        _eqnSamples: 0L, $
        _eqnUserdata: PTR_NEW(), $
        _withinUpdateData: 0b $
    }
end

