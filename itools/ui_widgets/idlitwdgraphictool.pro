; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/ui_widgets/idlitwdgraphictool.pro#2 $
;
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;   IDLitwdGraphicTool
;
; PURPOSE:
;   Create the IDL UI (widget) interface for an associated tool object.
;
; CALLING SEQUENCE:
;   IDLitwdGraphicTool, Tool
;
; INPUTS:
;   Tool - Object reference to the tool object.
;
;-


;;-------------------------------------------------------------------------
;; IDLitwdGraphicTool_callback
;;
;; Purpose:
;;   Callback routine for the tool interface widget, allowing it to
;;   receive update messages from the system.
;;
;; Parameters:
;;   wBase     - Base id of this widget
;;
;;   strID     - ID of the message.
;;
;;   MessageIn - What is the message
;;
;;   userdata  - Data associated with the message
;
pro IDLitwdGraphicTool_callback, wBase, strID, messageIn, userdata
    compile_opt idl2, hidden

    if (~WIDGET_INFO(wBase, /VALID)) then $
        return

    ;; Grab the state of the widget
    WIDGET_CONTROL, WIDGET_INFO(wBase, /CHILD), GET_UVALUE=pState

    case STRUPCASE(messageIn) of

    ; Check the file name changes to display
    'FILENAME': begin
        ; Use the new filename to construct the title.
        ; Remove the path.
        filename = STRSPLIT(userdata, '/\', /EXTRACT)
        filename = filename[N_ELEMENTS(filename)-1]
        ; Append the filename onto the base title.
        newTitle = (*pState).title + ' [' + filename + ']'
        WIDGET_CONTROL, wBase, TLB_SET_TITLE=newTitle
    end

    ;; Virtual dims changed
    'VIRTUAL_DIMENSIONS': begin
        WIDGET_CONTROL, (*pState).wDraw, $
          DRAW_XSIZE=userdata[0], DRAW_YSIZE=userdata[1], $
          SCR_XSIZE=userdata[0], SCR_YSIZE=userdata[1]
        end

    ; The sensitivity is to be changed
    'SENSITIVE': begin
        WIDGET_CONTROL, wBase, SENSITIVE=userdata
    end

    else:  ; do nothing

    endcase

end


;;-------------------------------------------------------------------------
;; IDLitwdGraphicTool__cleanup
;;
;; Purpose:
;;   Called when the widget is dying, allowing the state ptr to be
;;   released.
;;
;; Parameters:
;;    wChild   - The id of the widget that contains this widgets
;;               state.
;;
pro IDLitwdGraphicTool_cleanup, wChild

    compile_opt hidden, idl2

    if (~WIDGET_INFO(wChild, /VALID)) then $
        return

    WIDGET_CONTROL, wChild, GET_UVALUE=pState

    if (PTR_VALID(pState)) then begin
      ; If we haven't seen a kill request then we have been killed
      ; via widget_control,/reset. In this case we are responsible
      ; for shutting down the tool. Also, at this point the widget
      ; is dead, so we do *not* want to do the Save prompt.
      if (~(*pState).bKillRequestSeen) then begin
        if (OBJ_VALID((*pState).oUI)) then begin
          ; CR61772: Be sure to disconnect our dying widget
          ; from the IDLitUI, otherwise it will try to kill it again.
          (*pState).oUI->SetProperty, GROUP_LEADER=0L
        endif
        oTool = (*pState).oTool
        if (OBJ_VALID(oTool)) then begin
          oTool->SetProperty, /NO_SAVEPROMPT
          void = oTool->DoAction("/SERVICES/SHUTDOWN")
        endif
      endif

      if (PTR_VALID(pState)) then PTR_FREE, pState
    endif
end


;;-------------------------------------------------------------------------
;; IDLitwdGraphicTool_Event
;;
;; Purpose:
;;    Event handler for the tool interface IDL widget.
;;
;; Parameters:
;;    event    - The widget event to process.
;;
pro IDLitwdGraphicTool_event, event
    compile_opt idl2, hidden

@idlit_on_error2

    ;; Get our state
    wChild = WIDGET_INFO(event.handler, /CHILD)
    WIDGET_CONTROL, wChild, GET_UVALUE=pState

    case TAG_NAMES(event, /STRUCTURE_NAME) of

    ;; Kill the widget?
    'WIDGET_KILL_REQUEST': begin
        ; Let the tool know that shutdown was requested.
        ; This code must be here, and not in the _cleanup, because
        ; the tool may not actually be killed, say if the user is
        ; asked if they want to "save", and they hit "Cancel" instead.
        oTool = (*pState).oTool

        ; If we have been killed using widget_control,/reset then we won't
        ; come thru here, and will go directly to IDLitwdGraphicTool_cleanup.
        ; Set a flag to indicate that we are doing a "normal" shutdown.
        (*pState).bKillRequestSeen = 1b

        ; This will trigger a call to IDLitwdGraphicTool_cleanup.
        void = oTool->DoAction("/SERVICES/SHUTDOWN")

        ; pState will usually be dead at this point, unless the user
        ; has hit "Cancel" on the Save dialog and the tool is still alive.
        ; In that case, just reset our flag.
        if (PTR_VALID(pState)) then (*pState).bKillRequestSeen = 0b
    end

    ;; Focus change
    'WIDGET_KBRD_FOCUS': begin
        if (ISA(pState) && ISA((*pState).oUI) && event.enter) then begin
            oTool = (*pState).oTool
            oCurrent = oTool->GetService("SET_AS_CURRENT_TOOL")
            void = oTool->DoAction(oCurrent->GetFullIdentifier())
            ;; DONT DO THIS our tools could enter a focus loop in the
            ;; following situation:
            ;;    IDL> Iplot & iPlot
            ;; widget_control, (*pState).wDraw, /input_Focus
        endif
    end

    ;; The TLB was resized
    'WIDGET_BASE': begin
        ; Compute the size change of the base relative to
        ; its stored former size.
        WIDGET_CONTROL, event.id, TLB_GET_SIZE=newSize
        ; Change the draw widget to match the new size, minus padding.
        xy = newSize - (*pState).padding
        xy[0] >= (*pState).minsize
        WIDGET_CONTROL, (*pState).wDraw, $
          DRAW_XSIZE=xy[0], DRAW_YSIZE=xy[1], $
          SCR_XSIZE=xy[0], SCR_YSIZE=xy[1]
        ; Give the itwindow a chance to process the "OnResize" itevent,
        ; so that the graphics toolbar can resize itself.
        void = WIDGET_EVENT((*pState).wDraw, /NOWAIT)
        end

    'WIDGET_TLB_MOVE': begin
      ; Set the new location on the system, for future windows.
      oSys = _IDLitSys_GetSystem(/NO_CREATE)
      if (ISA(oSys)) then oSys->CacheToolLocation, [event.x,event.y]
      end

    else: ; do nothing

    endcase

end


;;-------------------------------------------------------------------------
;; IDLitwdGraphicTool
;;
;; Purpose:
;;    This is the main entry point for the iTools common IDL Widget
;;    user interface. This is passed a tool and the routine will then
;;    build a UI that contains the contents of the tool object.
;;
;; Parameters:
;;   oTool    - The tool object to use.
;;
;; Keywords:
;;    TITLE          - The title for the tool. If not provided, IDL
;;                     iTool is used.
;;
;;    LOCATION       - Where to place the new widget. X,Y
;;
;;    DIMENSIONS     - The size of the drawable.
;;
;;    VIRTUAL_DIMENSIONS - The virtual size of the drawing area
;;
;;    USER_INTERFACE - If set to an IDL variable, will return the user
;;                     interface object built during this UI
;;                     construction.
;
pro IDLitwdGraphicTool, oTool, TITLE=titleIn, $
                 LOCATION=location, $
                 DIMENSIONS=dimensionsIn, $
                 NO_MENUBAR=noMenuBar, $
                 VIRTUAL_DIMENSIONS=virtualDimensions, $
                 XSIZE=swallow1, $  ; should use DIMENSIONS
                 YSIZE=swallow2, $  ; should use DIMENSIONS
                 USER_INTERFACE=oUI, $  ; output keyword
                 _REF_EXTRA=_extra

    compile_opt idl2, hidden

@idlit_on_error2

    if (~OBJ_VALID(oTool)) then $
        MESSAGE, IDLitLangCatQuery('UI:InvalidTool')

    title = (N_ELEMENTS(titleIn) gt 0) ? titleIn[0] : $
      IDLitLangCatQuery('UI:wdTool:Title')

    WIDGET_CONTROL, /HOURGLASS

    ;*** Base to hold everything
    ;
    noMenuBar = 1
    wBase = WIDGET_BASE(/COLUMN, $
                        TITLE=title, $
                        /TLB_KILL_REQUEST_EVENTS, $
                        /TLB_MOVE_EVENTS, $
                        /TLB_SIZE_EVENTS, $
                        /KBRD_FOCUS_EVENTS, $
                        _EXTRA=_extra)
    wMenubar = 0L

    ;*** Create a new UI tool object, using our iTool.
    ;
    oUI = OBJ_NEW('GraphicsUI', oTool, GROUP_LEADER=wBase)

    ;***  Drawing area.
    ;
    screen = GET_SCREEN_SIZE(RESOLUTION=cm_per_pixel)
    hasDimensions = (N_ELEMENTS(dimensionsIn) eq 2)
    dimensions = hasDimensions ? dimensionsIn : [640, 512]

    ; Just hardcode the minimum size.
    minsize = 50
    dimensions >= minsize


    ; Be sure to pass in our UI object so the widget_window
    ; doesn't create its own tool & UI.
    wDraw = WIDGET_WINDOW(wBase, UI=oUI, $
      XSIZE=dimensions[0], YSIZE=dimensions[1], $
      _EXTRA=['RENDERER'])
    
    ;***  Toolbars
    ;
    wToolbar = GRAPHICS_TOOLBAR(wBase, oUI, XSIZE=dimensions[0])

    ; Cache some information.
    ;
    wChild = WIDGET_INFO(wBase, /CHILD)

    if(n_elements(location) eq 0)then begin
        location = [(screen[0] - dimensions[0])/2 - 10, $
                    ((screen[1] - dimensions[1])/2 - 100) > 10]
    endif
    WIDGET_CONTROL, MAP=0
    WIDGET_CONTROL, wBase, $
        TLB_SET_XOFFSET=location[0], TLB_SET_YOFFSET=location[1]

    ;; State structure for the widget
    State = { $
              oTool     : oTool,    $
              oUI       : oUI,      $
              wBase      : wBase,    $
              title     : title,    $
              minsize   : minsize,  $
              padding   : [0L, 0L], $
              wDraw     : wDraw,    $
              wToolbar  : wToolbar, $
              bKillRequestSeen: 0b   }
    pState = PTR_NEW(state, /NO_COPY)
    WIDGET_CONTROL, wChild, SET_UVALUE=pState

    WIDGET_CONTROL, wBase, /REALIZE

    WIDGET_CONTROL, wDraw, GET_VALUE=oWindow
    oTool->_SetCurrentWindow, oWindow
    
    ; Retrieve the starting dimensions and store them.
    ; Used for window resizing in event processing.
    WIDGET_CONTROL, wBase, TLB_GET_SIZE=basesize, TLB_GET_OFFSET=newLocation
    info = WIDGET_INFO(wToolbar, /GEOM)
    (*pState).padding = [basesize[0] - info.xsize, basesize[1] - dimensions[1]]

    oSys = oTool->_GetSystem()

    ; Set the new location on the system, for future windows.
    oSys->CacheToolLocation, newLocation

    ;; Register ourself as a widget with the UI object.
    ;; Returns a string containing our identifier.
    myID = oUI->RegisterWidget(wBase, 'ToolBase', 'IDLitwdGraphicTool_callback')

    ;; Register for our messages.
    oUI->AddOnNotifyObserver, myID, oTool->GetFullIdentifier()
    ; Observe the system so that we can be desensitized when a macro is running
    oUI->AddOnNotifyObserver, myID, oSys->GetFullIdentifier()

    WIDGET_CONTROL, wChild, KILL_NOTIFY="IDLitwdGraphicTool_cleanup"
    WIDGET_CONTROL, wBase, /MAP ;show the user what we have made.

    ; Start event processing for the tool.
    XMANAGER, 'IDLitwdGraphicTool', wBase, /NO_BLOCK

    ; CT: Aug 2012: Process the initial expose event.
    ; This seems to be required, otherwise the first time you create a
    ; window you won't see any graphics.
    void = WIDGET_EVENT(/NOWAIT)
end

