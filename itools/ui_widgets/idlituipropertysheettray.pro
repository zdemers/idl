; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/ui_widgets/idlituipropertysheettray.pro#2 $
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;-------------------------------------------------------------------------
; Purpose:
;   This function implements the user interface for the
;   property sheet.
;
; MODIFICATION HISTORY:
;   Written by:  CT, RSI, June 2003
;   Modified:
;


;-------------------------------------------------------------------------
function IDLituiPropertySheetTray, oUI, oRequester

    compile_opt idl2, hidden

    wProp = oUI->GetWidgetByName('PropertySheetTLB')
    if WIDGET_INFO(wProp, /VALID) then begin
      WIDGET_CONTROL, wProp, SET_VALUE=oRequester
      return, 1
    endif

    ; Retrieve widget ID of top-level base.
    oUI->GetProperty, GROUP_LEADER=groupLeader

    if (ISA(groupleader) && WIDGET_INFO(groupleader, /VALID)) then begin
      ; Our group leader might not be the top-level base. Walk up to find it.
      wParent = groupLeader
      while (wParent ne 0) do begin
        wTLB = wParent
        wParent = WIDGET_INFO(wParent[0], /PARENT)
      endwhile
      oMon = Obj_New('IDLsysMonitorInfo', DISPLAY_NAME=inDisplayName)
      rects = oMon->GetRectangles()
      obj_destroy, oMon
      geom = WIDGET_INFO(wTLB, /GEOM)
      xoffset = (geom.scr_xsize + geom.xoffset) < (TOTAL(rects[2,*],/INT) - 300)
      yoffset = geom.yoffset
      if (STRUPCASE(!version.os_family) eq 'UNIX') then begin
        xoffset += 4
        yoffset = (yoffset > 22) - 22
      endif
    endif

    ysize = 18

    n = N_Elements(oRequester)
    ids = (n gt 1) ? Strarr(n) : ''
    for i=0,n-1 do ids[i] = oRequester[i]->GetFullIdentifier()

    success = IDLitwdPropertySheet(oUI, $
        GROUP_LEADER=wTLB, $
        TITLE=title, $
        VALUE=ids, $
        XOFFSET=xoffset, $
        YOFFSET=yoffset, $
        SCR_XSIZE=300, $
        TYPE='Visualization Browser', $
        YSIZE=ysize)

    return, success
end

