; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/framework/idlitvisdataaxes__define.pro#3 $
;
; Copyright (c) 2000-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
;+
; CLASS_NAME:
;   IDLitVisDataAxes
;
; PURPOSE:
;   The IDLitVisDataAxes class is a collection of axes
;   that as a group serve as a visual representation for a data space.
;
; CATEGORY:
;   Components
;
; SUPERCLASSES:
;   _IDLitVisualization
;
;-


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLitVisDataAxes::Init
;
; PURPOSE:
;   The IDLitVisDataAxes::Init function method initializes this
;   component object.
;
;   NOTE: Init methods are special lifecycle methods, and as such
;   cannot be called outside the context of object creation.  This
;   means that in most cases, you cannot call the Init method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Init method
;   from within the Init method of the subclass.
;
; CALLING SEQUENCE:
;   oDataAxes = OBJ_NEW('IDLitVisDataAxes')
;
;   or
;
;   Obj->[IDLitVisDataAxes::]Init
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;
;-
function IDLitVisDataAxes::Init, _REF_EXTRA=_extra

    compile_opt idl2, hidden

    ; Initialize superclasses.
    ; By default, the Data Axes will not impact axes or view vol.
    if (self->IDLitVisualization::Init( $
        NAME='IDLitVisDataAxes', $
        ICON='axis', $
        IMPACTS_RANGE=0, $
        /PROPERTY_INTERSECTION, $
        /REGISTER_PROPERTIES, $
        SELECT_TARGET=0, $ ; no need to interactively select this container
        TYPE="IDLAXES", $
        _EXTRA=_extra) ne 1) then $
        RETURN, 0

    ; Set my defaults.
    self.style = 0
    self.stylePrevious = 0
    self.xRange = [0.0, 0.0]
    self.yRange = [0.0, 0.0]
    self.zRange = [0.0, 0.0]

    self.aboutToDoReset = 0
    
    ; Request no (additional) axes.
    self->SetAxesRequest, 0, /ALWAYS

    ; Register properties and set property attributes
    self->IDLitVisDataAxes::_RegisterProperties

    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisDataAxes::SetProperty, _EXTRA=_extra

    RETURN, 1
end


;----------------------------------------------------------------------------
pro IDLitVisDataAxes::Cleanup
  compile_opt idl2, hidden
  ; Cleanup superclasses.
  self->IDLitVisualization::Cleanup
end


;----------------------------------------------------------------------------
pro IDLitVisDataAxes::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

    compile_opt idl2, hidden

    registerAll = ~KEYWORD_SET(updateFromVersion)

    if (registerAll) then begin
        ; Add new registered properties.
        self->IDLitComponent::RegisterProperty, 'AXIS_STYLE', $
            NAME='Axis Style', $
            ENUMLIST=['None', 'At Dataspace Minimum', 'Box Axes', 'Crosshairs'], $
            DESCRIPTION='Axis style'
    endif

    if (~registerAll && updateFromVersion lt 640) then begin
         ; No longer expose these on the Axes prop sheet.
        self->SetPropertyAttribute, ['X_LOG', 'Y_LOG', 'Z_LOG'], /HIDE
    endif

end

;----------------------------------------------------------------------------
; IDLitVisDataAxes::Restore
;
; Purpose:
;   This procedure method performs any cleanup work required after
;   an object of this class has been restored from a save file to
;   ensure that its state is appropriate for the current revision.
;
pro IDLitVisDataAxes::Restore
    compile_opt idl2, hidden

    ; Call superclass restore.
    self->_IDLitVisualization::Restore

    ; Register new properties.
    self->IDLitVisDataAxes::_RegisterProperties, $
        UPDATE_FROM_VERSION=self.idlitcomponentversion

    ; ---- Required for SAVE files transitioning ----------------------------
    ;      from IDL 6.0 to 6.1 or above:
    if (self.idlitcomponentversion lt 610) then begin
        ; Request no axes.
        self.axesRequest = 0 ; No request for axes
        self.axesMethod = 0 ; Never request axes

        self->GetProperty, TYPE=oldTypes
        iValid = WHERE(oldTypes ne '', nValid)
        newTypes = (nValid ? ["IDLAXES", oldTypes[iValid]] : "IDLAXES")
        self->SetProperty, TYPE=newTypes
    endif
end


;----------------------------------------------------------------------------
;+
; IDLitVisDataAxes::_UpdateAxesRanges
;
; Purpose:
;   Internal routine to configure the axes.
;-
pro IDLitVisDataAxes::_UpdateAxesRanges, XRange, YRange, ZRange

    compile_opt idl2, hidden

    if (N_ELEMENTS(XRange) eq 0) then begin
         XRange = self.xRange
         YRange = self.yRange
         ZRange = self.zRange
    endif

    ; Keep track of reversal change.
    xWasRev = self._xReverse
    yWasRev = self._yReverse
    zWasRev = self._zReverse

    ; Determine whether any axis ranges are reversed.
    oDataSpace = self->GetDataSpace(/UNNORMALIZED)
    if (OBJ_VALID(oDataSpace)) then begin
      oDataSpace->GetProperty, $
        XREVERSE=xreverse, YREVERSE=yreverse, ZREVERSE=zreverse
    endif else begin
        xReverse = 0b
        yReverse = 0b
        zReverse = 0b
    endelse

    self._xReverse = xReverse
    self._yReverse = yReverse
    self._zReverse = zReverse

    oAxes = self->Get(/ALL, ISA='IDLitVisAxis', COUNT=count)

    for i=0, count-1 do begin
        oAxes[i]->GetProperty, DIRECTION=direction, $
            TEXTBASE=textbase, TEXTUPDIR=textupdir, $
            NOTEXT=notext
        case direction of
            0: begin
                range = xRange
                textbaseline = (xReverse ? [-1,0,0] : [1,0,0])
                textupdir = (yReverse ? [0,-1,0] : [0,1,0])
            end

            1: begin
                range = yRange
                textbaseline = (xReverse ? [-1,0,0] : [1,0,0])
                textupdir = (yReverse ? [0,-1,0] : [0,1,0])
            end

            2: begin
                range = zRange
                textbaseline = (xReverse ? [-1,0,0] : [1,0,0])
                textupdir = (zReverse ? [0,0,-1] : [0,0,1])
            end
        endcase

        oAxes[i]->SetProperty, NOTEXT=notext, $
          TEXTBASELINE=textbaseline, TEXTUPDIR=textupdir
        catch, iErr
        if (iErr eq 0) then begin
          oAxes[i]->SetProperty, RANGE=range
        endif
        catch, /cancel
        ; Clear out any floating overflow's (from bad ranges)
        void = CHECK_MATH()
    endfor

    ; Keep our selection box in sync with our new range, just in case
    ; we are currently selected.
   self->UpdateSelectionVisual

end


;----------------------------------------------------------------------------
;+
; IDLitVisDataAxes::_GetAxis
;
; Purpose:
;   Find an axis in the group matching the specified direction
;   and normLocation.  If no axis is found, create an axis with
;   the specified properties and add it to the container.
;
;   Return the axis with the specified properties.
;-
function IDLitVisDataAxes::_GetAxis, $
    CROSSHAIR=crosshair, $
    DIRECTION=direction, $
    NORM_LOCATION=normLocation

    compile_opt idl2, hidden

    if N_ELEMENTS(direction) eq 0 then direction = 0
    if N_ELEMENTS(normLocation) eq 0 then normLocation = [0d, 0d, 0d]

    oAxes = self->Get(/ALL, ISA='IDLitVisAxis', COUNT=count)

    oAxis = OBJ_NEW()
    eps = 0.01

    for i=0, count-1 do begin

        oAxes[i]->GetProperty, $
            DIRECTION=currentDirection, $
            NORM_LOCATION=currentNormLocation
        if (currentDirection ne direction) then $
            continue

        diff = ABS(currentNormLocation - normLocation)

        case direction of
            0: if ((diff[1] lt eps) && (diff[2] lt eps)) then $
                    oAxis = oAxes[i]
            1: if ((diff[0] lt eps) && (diff[2] lt eps)) then $
                    oAxis = oAxes[i]
            2: if ((diff[0] lt eps) && (diff[1] lt eps)) then $
                    oAxis = oAxes[i]
        endcase

        if (OBJ_VALID(oAxis)) then $
            break

    endfor

    if (~OBJ_VALID(oAxis)) then begin
        oTool = self->GetTool()
        if (OBJ_VALID(oTool)) then begin
            oAxisDesc = oTool->GetVisualization("AXIS")
            oAxis = oAxisDesc->GetObjectInstance()
        endif else $
            oAxis = OBJ_NEW('IDLitVisAxis')

        if (~OBJ_VALID(oAxis)) then $
            return, OBJ_NEW()

        ; See if user has a name preference.
        oAxis->GetProperty, NAME=basename
        name = (basename ? basename : 'Axis') + ' ' + STRTRIM(self._index,2)

        oAxis->SetProperty, $
            /EXACT, $
            NAME=name, $
            IDENTIFIER='Axis' + STRTRIM(self._index,2), $
            NORM_LOCATION=normLocation, $
            TOOL=oTool

        self->Add, oAxis, /AGGREGATE, /NO_UPDATE

        self._index++
    endif

    if KEYWORD_SET(crosshair) then begin
      ; Set crosshair defaults.
      oAxis->SetProperty, DIRECTION=direction, NOTEXT=0, TEXTPOS=0, TICKDIR=2, $
        HIDE=0, PRIVATE=0
    endif else begin
      ; Reset defaults.
      oAxis->SetProperty, DIRECTION=direction, TICKDIR=0, $
        HIDE=0, PRIVATE=0
    endelse

    return, oAxis
end
;----------------------------------------------------------------------------
;+
; IDLitVisDataAxes::ConfigAxes
;
; Purpose:
;   Internal routine to configure the axes.
;-
pro IDLitVisDataAxes::_ConfigAxes

    compile_opt idl2, hidden

    oAxes = self->Get(/ALL, ISA='IDLitVisAxis', COUNT=count)

    oDataSpace = self->GetDataSpace(/UNNORMALIZED)
    if (~OBJ_VALID(oDataSpace)) then $
        return
    threeD = oDataSpace->_IDLitVisualization::Is3D()

    self._initRangeChange = 1

    ; Hide axes
    for i=0, count-1 do begin
        oAxes[i]->GetProperty, NORM_LOCATION=normLocation, DIRECTION=direction
        ; If we are very close to either end (or in the middle if
        ; the previous style was crosshair), then we should be
        ; considered a "default" axis.  default axes are used to
        ; implement an axes style (box, dataspace minimum) and may
        ; be hidden.  axes at non-default locations are not hidden
        ; when switching styles.
        eps = 0.01
        if self.stylePrevious eq 3 then begin

            closeToMiddle = (ABS(0.5 - normLocation) lt eps)

            if threeD then begin
                case direction of
                    0: defaultLocation = closeToMiddle[1] && $
                        closeToMiddle[2]  ; Y & Z
                    1: defaultLocation = closeToMiddle[0] && $
                        closeToMiddle[2]  ; X & Z
                    2: defaultLocation = closeToMiddle[0] && $
                        closeToMiddle[1]  ; X & Y
                endcase
            endif else begin
                case direction of
                    0: defaultLocation = closeToMiddle[1]  ; Y
                    1: defaultLocation = closeToMiddle[0]  ; X
                    else:
                endcase
            endelse
        endif else begin
            closeToEnd = (ABS(normLocation) lt eps) or $
                (ABS(1 - normLocation) lt eps)
            case direction of
                0: defaultLocation = closeToEnd[1] && closeToEnd[2]  ; Y & Z
                1: defaultLocation = closeToEnd[0] && closeToEnd[2]  ; X & Z
                2: defaultLocation = closeToEnd[0] && closeToEnd[1]  ; X & Y
            endcase
        endelse
        if defaultLocation then begin
            oAxes[i]->SetProperty, HIDE=1, PRIVATE=1
        endif
    endfor

    ; Handle supported axis styles
    case self.style of
        0: begin ; ignore call from init
        end
        1: begin
            ; Axes at origin only
            ;
            ; Axis assignments:
            ; 0 X direction
            ; 1 Y direction
            ; 2 Z direction
            ; 3-11 unused (hidden)
            ; Set Ranges

            oAxisX = self->_GetAxis( $
                    DIRECTION=0, $
                    NORM_LOCATION=[0d,0d,0d])
            oAxisX->SetProperty, $
                    NOTEXT=0, TICKDIR=0, TEXTPOS=0

            oAxisY = self->_GetAxis( $
                    DIRECTION=1, $
                    NORM_LOCATION=[0d,0d,0d])
            oAxisY->SetProperty, $
                    NOTEXT=0, TICKDIR=0, TEXTPOS=0

            if (threeD) then begin
                oAxisZ = self->_GetAxis( $
                    DIRECTION=2, $
                    NORM_LOCATION=[0d,0d,0d])
                oAxisZ->SetProperty, $
                    NOTEXT=0, TICKDIR=0, TEXTPOS=0
            endif

            ; After direction of axes is set we can set the ranges
            self->_UpdateAxesRanges

        end

        2: begin
            ; Box axes
            ;
            ; Axis assignments:
            ;     X Direction
            ;        0 - Y low Z low
            ;        1 - Y high Z low
            ;        2 - Y low Z high
            ;        3 - Y high Z high
            ;     Y Direction
            ;        4 - X low Z low
            ;        5 - X high Z low
            ;        6 - X low Z high
            ;        7 - X high Z high
            ;     Z Direction
            ;        8 - X low Y low
            ;        9 - X high Y low
            ;        10 - X low Y high
            ;        11 - X high Y high


            ; First add a single X, Y, Z axis so that the order in the
            ; container matches the AXIS_STYLE=1 order.

            oAxisX1 = self->_GetAxis( $
                DIRECTION=0, $
                NORM_LOCATION=[0d,0d,0d])   ; YLO, ZLO

            ; Y axes.
            oAxisY1 = self->_GetAxis( $
                DIRECTION=1, $
                NORM_LOCATION=[0d,0d,0d])   ; XLO, ZLO

            if threeD then begin
                ; Z axes.
                oAxisZ1 = self->_GetAxis( $
                    DIRECTION=2, $
                    NORM_LOCATION=[0d,0d,0d])   ; XLO, /YLO
                oAxisZ1->SetProperty, $
                    NOTEXT=0, TICKDIR=0, TEXTPOS=0
            endif

            ; Now add the next set of X, Y, Z axes.
            oAxisX2 = self->_GetAxis( $
                DIRECTION=0, $
                NORM_LOCATION=[0d,1d,0d])   ; YHI, ZLO
            oAxisX2->SetProperty, $
                TICKDIR=1, TEXTPOS=1, /NOTEXT

            oAxisY2 = self->_GetAxis( $
                DIRECTION=1, $
                NORM_LOCATION=[1d,0d,0d])   ; XHI, ZL
            oAxisY2->SetProperty, $
                TICKDIR=1, TEXTPOS=1, /NOTEXT

            if threeD then begin
                oAxisZ2 = self->_GetAxis( $
                    DIRECTION=2, $
                    NORM_LOCATION=[1d,0d,0d])   ; XHI, /YLO
                oAxisZ2->SetProperty, $
                    TICKDIR=1, TEXTPOS=1, /NOTEXT
            endif

            ; Finally, if 3D, add the rest of the box axes,
            ; making sure to do them in groups of X,Y,Z.
            if threeD then begin
                oAxisX3 = self->_GetAxis( $
                    DIRECTION=0, $
                    NORM_LOCATION=[0d,0d,1d])   ; YLO, /ZHI
                oAxisX3->SetProperty, $
                    /NOTEXT

                oAxisY3 = self->_GetAxis( $
                    DIRECTION=1, $
                    NORM_LOCATION=[0d,0d,1d])   ; XLO, ZHI
                oAxisY3->SetProperty, $
                    /NOTEXT

                oAxisZ3 = self->_GetAxis( $
                    DIRECTION=2, $
                    NORM_LOCATION=[0d,1d,0d])   ; XLO, /YHI
                oAxisZ3->SetProperty, $
                        /NOTEXT

                oAxisX4 = self->_GetAxis( $
                    DIRECTION=0, $
                    NORM_LOCATION=[0d,1d,1d])   ; YHI, ZHI
                oAxisX4->SetProperty, $
                    TICKDIR=1, TEXTPOS=1, /NOTEXT

                oAxisY4 = self->_GetAxis( $
                    DIRECTION=1, $
                    NORM_LOCATION=[1d,0d,1d])   ;XHI, ZHI
                oAxisY4->SetProperty, $
                    TICKDIR=1, TEXTPOS=1, /NOTEXT

                oAxisZ4 = self->_GetAxis( $
                    DIRECTION=2, $
                    NORM_LOCATION=[1d,1d,0d])   ;XHI, YHI
                oAxisZ4->SetProperty, $
                    TICKDIR=1, TEXTPOS=1, /NOTEXT

            endif

            ; After direction of axes is set we can set the ranges
            self->_UpdateAxesRanges

        end
        3: begin
            ; Crosshairs
            ;
            ; Axis assignments:
            ; 0 X direction
            ; 1 Y direction
            ; 2 Z direction
            ; Set Ranges
            z = threeD ? 0.5 : 0

            oAxisX = self->_GetAxis( $
                DIRECTION=0, $
                NORM_LOCATION=[0d, 0.5d, z], /CROSSHAIR)

            oAxisY = self->_GetAxis( $
                DIRECTION=1, $
                NORM_LOCATION=[0.5d, 0d, z], /CROSSHAIR)

            if (threeD) then begin
                oAxisZ = self->_GetAxis( $
                    DIRECTION=2, $
                    NORM_LOCATION=[0.5d, 0.5d, 0d], /CROSSHAIR)
            endif

            ; After direction of axes is set we can set the ranges
            self->_UpdateAxesRanges

        end

        4: begin ; ignore call from init
        end

        else: begin
            MESSAGE, IDLitLangCatQuery('Message:Framework:InvalidStyle')
        end
    endcase

    if (self.style eq 1 || self.style eq 2 || self.style eq 3) then begin
        ; Notify our observers to update their tree view, since we may have
        ; marked items as private=0.
        self->DoOnNotify, self->GetFullIdentifier(), 'UPDATEITEM', ''

        ; Save style for comparison on next style change
        self.stylePrevious = self.style
    endif

end


;----------------------------------------------------------------------------
;+
; IDLitVisDataAxes::_UpdateStyle
;
; Purpose:
;   Internal routine to update the style.
;-
pro IDLitVisDataAxes::_UpdateStyle, XSTYLE=xStyle, YSTYLE=yStyle, ZSTYLE=zStyle

  compile_opt idl2, hidden

  oDataspace = self->GetDataSpace(/UNNORMALIZED)

  ; CT, VIS, Sept 2013:
  ; If we aren't setting one of the styles, make sure to re-set it to the
  ; current value so we pick up the correct range. Otherwise, we can run
  ; into problems if you do a log plot and the dataspace range is in log
  ; coordinates but the data range is in "data" coordinates.
  ; TODO: Completely rewrite all axis range, style, and log code.
  if ~ISA(xStyle) then $
    xStyle = ([1,0,2,3])[self.xStyle]
  if ~ISA(yStyle) then $
    yStyle = ([1,0,2,3])[self.yStyle]
  threeD = oDataSpace->_IDLitVisualization::Is3D()
  if (threeD && ~ISA(zStyle)) then $
    zStyle = ([1,0,2,3])[self.zStyle]

  void=odataspace->getXYZRange(dataXRange, dataYRange, dataZRange)
  odataspace->GetProperty, XRANGE = dataspaceXRange, YRANGE=dataspaceYRange, $
    ZRANGE=dataspaceZRange, XLOG=isXLog, YLOG=isYLog, ZLOG=isZLog, $
    X_AUTO_UPDATE=xAutoUpdate, Y_AUTO_UPDATE=yAutoUpdate, Z_AUTO_UPDATE=zAutoUpdate, $
    XREVERSE=xreverse, YREVERSE=yreverse, ZREVERSE=zreverse

  dataspaceXRange = xReverse? reverse(dataspaceXRange) : dataspaceXRange
  dataspaceYRange = yReverse? reverse(dataspaceYRange) : dataspaceYRange
  dataspaceZRange = zReverse? reverse(dataspaceZRange) : dataspaceZRange

  oAxes = self->Get(/ALL, ISA='IDLitVisAxis', COUNT=count)

  if (N_ELEMENTS(xStyle) && xAutoUpdate) then begin
    ; if style value is invalid, set to default
    if((xStyle lt 0) || (xStyle gt 3)) then MESSAGE, 'XSTYLE must be 0, 1, 2, or 3.
    ; DANGER: For backwards compatibility, store style=1 as 0 (and vice versa).
    self.xStyle = ([1,0,2,3])[xStyle]
    ; calculate padding if needed
    for i=0, count-1 do begin
      oAxes[i]->GetProperty, DIRECTION=direction
      if (direction eq 0) then begin
        newRange = oAxes[i]->GetComputedRange(xStyle, dataXRange)
        if (xReverse) then newRange = newRange[[1,0]]
        ; Only update if necessary (otherwise we get strange recursive updates)
        if (~ARRAY_EQUAL(dataspaceXRange,newRange)) then begin
          dataspaceXRange = newRange
          needOnDataRangeChange = 1b
          break
        endif
      endif
    endfor
  endif

  if (N_ELEMENTS(yStyle) && yAutoUpdate) then begin
    ; if style value is invalid, set to default
    if((yStyle lt 0) || (yStyle gt 3)) then MESSAGE, 'YSTYLE must be 0, 1, 2, or 3.
    ; DANGER: For backwards compatibility, store style=1 as 0 (and vice versa).
    self.yStyle = ([1,0,2,3])[yStyle]
    ; calculate padding if needed
     for i=0, count-1 do begin
      oAxes[i]->GetProperty, DIRECTION=direction
      if (direction eq 1) then begin
        newRange = oAxes[i]->GetComputedRange(yStyle, dataYRange)
        if (yReverse) then newRange = newRange[[1,0]]
        ; Only update if necessary (otherwise we get strange recursive updates)
        if (~ARRAY_EQUAL(dataspaceYRange,newRange)) then begin
          dataspaceYRange = newRange
          needOnDataRangeChange = 1b
          break
        endif
      endif
    endfor
  endif

  if (N_ELEMENTS(zStyle) && zAutoUpdate) then begin
    ; if style value is invalid, set to default
    if((zStyle lt 0) || (zStyle gt 3)) then MESSAGE, 'ZSTYLE must be 0, 1, 2, or 3.
    ; DANGER: For backwards compatibility, store style=1 as 0 (and vice versa).
    self.zStyle = ([1,0,2,3])[zStyle]
    ; calculate padding if needed
     for i=0, count-1 do begin
      oAxes[i]->GetProperty, DIRECTION=direction
      if (direction eq 2) then begin
        newRange = oAxes[i]->GetComputedRange(zStyle, dataZRange)
        if (zReverse) then newRange = newRange[[1,0]]
        ; Only update if necessary (otherwise we get strange recursive updates)
        if (~ARRAY_EQUAL(dataspaceZRange,newRange)) then begin
          dataspaceZRange = newRange
          needOnDataRangeChange = 1b
          break
        endif
      endif
    endfor
  endif

  if (KEYWORD_SET(needOnDataRangeChange)) then begin
    odataspace->OnDataRangeChange, odataspace, $
      dataspaceXRange, dataspaceYRange, dataspaceZRange
  endif

end


;----------------------------------------------------------------------------
; IIDLProperty Interface
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisDataAxes::GetProperty
;
; PURPOSE:
;      The IDLitVisDataAxes::GetProperty procedure method retrieves the
;      value of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisDataAxes::]GetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLitVisDataAxes::Init followed by the word "Get"
;      can be retrieved using IDLitVisDataAxes::GetProperty.  In addition
;      the following keywords are available:
;
;      ALL: Set this keyword to a named variable that will contain
;              an anonymous structure containing the values of all the
;              retrievable properties associated with this object.
;              NOTE: UVALUE is not returned in this struct.
;-
pro IDLitVisDataAxes::GetProperty, $
    AXIS_STYLE=axisStyle, $
    FONT_COLOR=fontColor, FONT_NAME=fontName, $
    FONT_SIZE=fontSize, FONT_STYLE=fontStyle, $
    XRANGE=xrange, $
    YRANGE=yrange, $
    ZRANGE=zrange, $
    STYLE=style, $   ; renamed in IDL72 to AXIS_STYLE
    XLOG=xLog, $
    YLOG=yLog, $
    ZLOG=zLog, $
    X_LOG=x_Log, $ ; renamed in IDL64, keep for backwards compat
    Y_LOG=y_Log, $ ; renamed in IDL64, keep for backwards compat
    Z_LOG=z_Log, $ ; renamed in IDL64, keep for backwards compat
    XREVERSE=xReverse, $
    YREVERSE=yReverse, $
    ZREVERSE=zReverse, $
    XAXIS_RANGE=xAxisRange, YAXIS_RANGE=yAxisRange, ZAXIS_RANGE=zAxisRange, $
    XCOLOR=xColor, YCOLOR=yColor, ZCOLOR=zColor, $
    XCOORD_TRANSFORM=xCoordTransform, YCOORD_TRANSFORM=yCoordTransform, ZCOORD_TRANSFORM=zCoordTransform, $
    XSTYLE=xStyle, YSTYLE=yStyle, ZSTYLE=zStyle, $
    XGRIDSTYLE=xGridStyle, YGRIDSTYLE=yGridStyle, ZGRIDSTYLE=zGridStyle, $
    XMAJOR=xMajor, YMAJOR=yMajor, ZMAJOR=zMajor, $
    XMINOR=xMinor, YMINOR=yMinor, ZMINOR=zMinor, $
    XSHOWTEXT=xShowText, YSHOWTEXT=yShowText, ZSHOWTEXT=zShowText, $
    XSUBGRIDSTYLE=xSubGridStyle, YSUBGRIDSTYLE=ySubGridStyle, ZSUBGRIDSTYLE=zSubGridStyle, $
    XTEXT_COLOR=xTextColor, YTEXT_COLOR=yTextColor, ZTEXT_COLOR=zTextColor, $
    XTEXT_ORIENTATION=xTextOrientation, YTEXT_ORIENTATION=yTextOrientation, $
    ZTEXT_ORIENTATION=zTextOrientation, $
    XTEXTPOS=xTextPos, YTEXTPOS=yTextPos, ZTEXTPOS=zTextPos, $
    XTHICK=xThick, YTHICK=yThick, ZTHICK=zThick, $
    XTICKDIR=xTickdir, YTICKDIR=yTickdir, ZTICKDIR=zTickdir, $
    XTICKFONT_INDEX=xTickFontIndex, YTICKFONT_INDEX=yTickFontIndex, ZTICKFONT_INDEX=zTickFontIndex, $
    XTICKFONT_NAME=xTickFontName, YTICKFONT_NAME=yTickFontName, ZTICKFONT_NAME=zTickFontName, $
    XTICKFONT_SIZE=xTickFontSize, YTICKFONT_SIZE=yTickFontSize, ZTICKFONT_SIZE=zTickFontSize, $
    XTICKFONT_STYLE=xTickFontStyle, YTICKFONT_STYLE=yTickFontStyle, ZTICKFONT_STYLE=zTickFontStyle, $
    XTICKFORMAT=xTickFormat, YTICKFORMAT=yTickFormat, ZTICKFORMAT=zTickFormat, $
    XTICKINTERVAL=xTickInterval, YTICKINTERVAL=yTickInterval, ZTICKINTERVAL=zTickInterval, $
    XTICKLAYOUT=xTickLayout, YTICKLAYOUT=yTickLayout, ZTICKLAYOUT=zTickLayout, $
    XTICKLEN=xTickLen, YTICKLEN=yTickLen, ZTICKLEN=zTickLen, $
    XSUBTICKLEN=xSubTickLen, YSUBTICKLEN=ySubTickLen, ZSUBTICKLEN=zSubTickLen, $
    XTICKNAME=xTickName, YTICKNAME=yTickName, ZTICKNAME=zTickName, $
    XTICKUNITS=xTickUnits, YTICKUNITS=yTickUnits, ZTICKUNITS=zTickUnits, $
    XTICKVALUES=xTickValues, YTICKVALUES=yTickValues, ZTICKVALUES=zTickValues, $
    XTITLE=xTitle, YTITLE=yTitle, ZTITLE=zTitle, $
    XTRANSPARENCY=xTransparency, YTRANSPARENCY=yTransparency, $
    ZTRANSPARENCY=zTransparency, $ 
    _REF_EXTRA=_extra

    compile_opt idl2, hidden

    if ARG_PRESENT(xrange) then $
        xrange = self.xRange

    if ARG_PRESENT(yrange) then $
        yrange = self.yRange

    if ARG_PRESENT(zrange) then $
        zrange = self.zRange

    if ARG_PRESENT(axisStyle) then $
        axisStyle = self.style
    if ARG_PRESENT(style) then $
        style = self.style

    if ARG_PRESENT(xReverse) then $
        xReverse = self._xReverse

    if ARG_PRESENT(yReverse) then $
        yReverse = self._yReverse

    if ARG_PRESENT(zReverse) then $
        zReverse = self._zReverse

    ; DANGER: For backwards compatibility, store style=1 as 0 (and vice versa).
    if ARG_PRESENT(xStyle) then $
        xStyle = ([1,0,2,3])[self.xStyle]

    ; DANGER: For backwards compatibility, store style=1 as 0 (and vice versa).
    if ARG_PRESENT(yStyle) then $
        yStyle = ([1,0,2,3])[self.yStyle]

    ; DANGER: For backwards compatibility, store style=1 as 0 (and vice versa).
    if ARG_PRESENT(zStyle) then $
        zStyle = ([1,0,2,3])[self.zStyle]

    ; Ask the dataspace
    if (Arg_Present(xLog) || Arg_Present(yLog) || Arg_Present(zLog) || $
        Arg_Present(x_Log) || Arg_Present(y_Log) || Arg_Present(z_Log)) then begin
        oDataSpace = self->GetDataSpace(/UNNORMALIZED)
        if (OBJ_VALID(oDataSpace)) then begin
            oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, ZLOG=zLog
            x_Log = xLog
            y_Log = yLog
            z_Log = zLog
        endif else begin
            xLog=0
            yLog=0
            zLog=0
            x_Log=0
            y_Log=0
            z_Log=0
        endelse
    endif

    if (ARG_PRESENT(fontColor) || ARG_PRESENT(fontName) || $
      ARG_PRESENT(fontSize) || ARG_PRESENT(fontStyle)) then begin
      self->IDLitVisDataAxes::GetProperty, $
        XTEXT_COLOR=fontColor, XTICKFONT_NAME=fontName, $
        XTICKFONT_SIZE=fontSize, XTICKFONT_STYLE=fontStyle
    endif

    ; Get command line property settings from appropriate axes
    hasXkeywords = ARG_PRESENT(xAxisRange) || $
      ARG_PRESENT(xColor) || ARG_PRESENT(xCoordTransform) || $
      ARG_PRESENT(xGridStyle) || ARG_PRESENT(xMajor) || $
      ARG_PRESENT(xMinor) || ARG_PRESENT(xShowText) || $
      ARG_PRESENT(xSubGridStyle) || $
      ARG_PRESENT(xTextColor) || ARG_PRESENT(xTextOrientation) || $
      ARG_PRESENT(xTextPos) || $
      ARG_PRESENT(xTickdir) || ARG_PRESENT(xTickFontIndex) || $
      ARG_PRESENT(xTickFontName) || $
      ARG_PRESENT(xTickFontSize) || ARG_PRESENT(xTickFontStyle) || $
      ARG_PRESENT(xTickFormat) || ARG_PRESENT(xThick) || $
      ARG_PRESENT(xTickInterval) || ARG_PRESENT(xTickLayout) ||$
      ARG_PRESENT(xTickLen) || ARG_PRESENT(xSubTickLen) || $
      ARG_PRESENT(xTickName) || ARG_PRESENT(xTickUnits) || $
      ARG_PRESENT(xTickValues) || ARG_PRESENT(xTitle) || ARG_PRESENT(xTransparency)
    hasYkeywords = ARG_PRESENT(yAxisRange) || $
      ARG_PRESENT(yColor) || ARG_PRESENT(yCoordTransform) || $
      ARG_PRESENT(yGridStyle) || ARG_PRESENT(yMajor) || $
      ARG_PRESENT(yMinor) || ARG_PRESENT(yShowText) || $
      ARG_PRESENT(ySubGridStyle) || $
      ARG_PRESENT(yTextColor) || ARG_PRESENT(yTextOrientation) || $
      ARG_PRESENT(yTextPos) || $
      ARG_PRESENT(yTickdir) || ARG_PRESENT(yTickFontIndex) || $
      ARG_PRESENT(yTickFontName) || $
      ARG_PRESENT(yTickFontSize) || ARG_PRESENT(yTickFontStyle) || $
      ARG_PRESENT(yTickFormat) || ARG_PRESENT(yThick) || $
      ARG_PRESENT(yTickInterval) || ARG_PRESENT(yTickLayout) ||$
      ARG_PRESENT(yTickLen) || ARG_PRESENT(ySubTickLen) || $
      ARG_PRESENT(yTickName) || ARG_PRESENT(yTickUnits) || $
      ARG_PRESENT(yTickValues) || ARG_PRESENT(yTitle) || ARG_PRESENT(yTransparency)
    hasZkeywords = ARG_PRESENT(zAxisRange) || $
      ARG_PRESENT(zColor) || ARG_PRESENT(zCoordTransform) || $
      ARG_PRESENT(zGridStyle) || ARG_PRESENT(zMajor) || $
      ARG_PRESENT(zMinor) || ARG_PRESENT(zShowText) || $
      ARG_PRESENT(zSubGridStyle) || $
      ARG_PRESENT(zTextColor) || ARG_PRESENT(zTextOrientation) || $
      ARG_PRESENT(zTextPos) || $
      ARG_PRESENT(zTickdir) || ARG_PRESENT(zTickFontIndex) || $
      ARG_PRESENT(zTickFontSize) || ARG_PRESENT(zTickFontStyle) || $
      ARG_PRESENT(zTickFontName) || $
      ARG_PRESENT(zTickFormat) || ARG_PRESENT(zThick) || $
      ARG_PRESENT(zTickInterval) || ARG_PRESENT(zTickLayout) ||$
      ARG_PRESENT(zTickLen) || ARG_PRESENT(zSubTickLen) || $
      ARG_PRESENT(zTickName) || ARG_PRESENT(zTickUnits) || $
      ARG_PRESENT(zTickValues) || ARG_PRESENT(zTitle) || ARG_PRESENT(zTransparency)

    if (hasXkeywords || hasYkeywords || hasZkeywords) then begin
        oAxes = self->Get(/ALL, ISA='IDLitVisAxis', COUNT=count)
        xfound = 0
        yfound = 0
        zfound = 0
        for i=0, count-1 do begin
            ; When looking for properties from a particular axis,
            ; stop once the first axis in the specified direction
            ; is found
            oAxes[i]->GetProperty, DIRECTION=direction
            case direction of
            0: if (hasXkeywords && xfound eq 0) then begin
              xfound = 1 ; Only retrieve props from the first axis found
              oAxes[i]->GetProperty, $
                AXIS_RANGE=xAxisRange, COLOR=xColor, $
                COORD_TRANSFORM=xCoordTransform, $
                GRIDSTYLE=xGridStyle, $
                MAJOR=xMajor, MINOR=xMinor, $
                TEXT_COLOR=xTextColor, $
                TEXT_ORIENTATION=xTextOrientation, $
                TEXTPOS=xTextPos, $
                FONT_INDEX=xTickFontIndex, FONT_SIZE=xTickFontSize, $
                FONT_NAME=xTickFontName, $
                FONT_STYLE=xTickFontStyle, $
                SHOWTEXT=xShowText, $
                SUBGRIDSTYLE=xSubGridStyle, $
                TICKFORMAT=xTickFormat, $
                TICKINTERVAL=xTickInterval, TICKLAYOUT=xTickLayout, $
                TICKDIR=xTickdir, TICKLEN=xTickLen, SUBTICKLEN=xSubTickLen, $
                TICKNAME=xTickName, TICKUNITS=xTickUnits, $
                TICKVALUES=xTickValues, AXIS_TITLE=xTitle, THICK=xThick, $
                TRANSPARENCY=xTransparency
              endif
            1: if (hasYkeywords && yfound eq 0) then begin
              yfound = 1 ; Only retrieve props from the first axis found
              oAxes[i]->GetProperty, $
                AXIS_RANGE=yAxisRange, COLOR=yColor, $
                COORD_TRANSFORM=yCoordTransform, $
                GRIDSTYLE=yGridStyle, $
                MAJOR=yMajor, MINOR=yMinor, $
                TEXT_COLOR=yTextColor, $
                TEXT_ORIENTATION=yTextOrientation, $
                TEXTPOS=yTextPos, $
                FONT_INDEX=yTickFontIndex, FONT_SIZE=yTickFontSize, $
                FONT_NAME=yTickFontName, $
                FONT_STYLE=yTickFontStyle, $
                SHOWTEXT=yShowText, $
                SUBGRIDSTYLE=ySubGridStyle, $
                TICKFORMAT=yTickFormat, $
                TICKINTERVAL=yTickInterval, TICKLAYOUT=yTickLayout, $
                TICKDIR=yTickdir, TICKLEN=yTickLen, SUBTICKLEN=ySubTickLen, $
                TICKNAME=yTickName, TICKUNITS=yTickUnits, $
                TICKVALUES=yTickValues, AXIS_TITLE=yTitle, THICK=yThick, $
                TRANSPARENCY=yTransparency
              endif
            2: if (hasZkeywords && zfound eq 0) then begin
              zfound = 1 ; Only retrieve props from the first axis found
              oAxes[i]->GetProperty, $
                AXIS_RANGE=zAxisRange, COLOR=zColor, $
                COORD_TRANSFORM=zCoordTransform, $
                GRIDSTYLE=zGridStyle, $
                MAJOR=zMajor, MINOR=zMinor, $
                SUBGRIDSTYLE=zSubGridStyle, $
                TEXT_COLOR=zTextColor, $
                TEXT_ORIENTATION=zTextOrientation, $
                TEXTPOS=zTextPos, $
                FONT_INDEX=zTickFontIndex, FONT_SIZE=zTickFontSize, $
                FONT_NAME=zTickFontName, $
                FONT_STYLE=zTickFontStyle, $
                SHOWTEXT=zShowText, $
                TICKFORMAT=zTickFormat, $
                TICKINTERVAL=zTickInterval, TICKLAYOUT=zTickLayout, $
                TICKDIR=zTickdir, TICKLEN=zTickLen, SUBTICKLEN=zSubTickLen, $
                TICKNAME=zTickName, TICKUNITS=zTickUnits, $
                TICKVALUES=zTickValues, AXIS_TITLE=zTitle, THICK=zThick, $
                TRANSPARENCY=zTransparency
              endif
            endcase
        endfor

    endif

    ; get superclass properties
    if (N_ELEMENTS(_extra) gt 0) then $
        self->IDLitVisualization::GetProperty, _EXTRA=_extra


end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLitVisDataAxes::SetProperty
;
; PURPOSE:
;      The IDLitVisDataAxes::SetProperty procedure method sets the value
;      of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisDataAxes::]SetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLitVisDataAxes::Init followed by the word "Set"
;      can be set using IDLitVisDataAxes::SetProperty.
;-
pro IDLitVisDataAxes::SetProperty, $
    AXIS_STYLE=axisStyle, $
    INTERNAL=internal, $
    FONT_COLOR=fontColor, FONT_NAME=fontName, $
    FONT_SIZE=fontSize, FONT_STYLE=fontStyle, $
    LOCATION=swallow, $  ; do not want to pass to the contained axes
    THICK=!NULL, $
    TRANSPARENCY=swallow2, $
    XRANGE=xswallow, $ ; read-only, should only get changed in OnDataRangeChange
    YRANGE=yswallow, $ ; read-only, should only get changed in OnDataRangeChange
    ZRANGE=zswallow, $ ; read-only, should only get changed in OnDataRangeChange
    XLOG=xLog, $
    YLOG=yLog, $
    ZLOG=zLog, $
    X_LOG=x_Log, $ ; renamed in IDL64, keep for backwards compat
    Y_LOG=y_Log, $ ; renamed in IDL64, keep for backwards compat
    Z_LOG=z_Log, $ ; renamed in IDL64, keep for backwards compat
    XAXIS_RANGE=xAxisRange, YAXIS_RANGE=yAxisRange, ZAXIS_RANGE=zAxisRange, $
    XCOLOR=xColor, YCOLOR=yColor, ZCOLOR=zColor, $
    XCOORD_TRANSFORM=xCoordTransform, YCOORD_TRANSFORM=yCoordTransform, ZCOORD_TRANSFORM=zCoordTransform, $
    XSTYLE=xStyle, YSTYLE=yStyle, ZSTYLE=zStyle, $
    XGRIDSTYLE=xGridStyle, YGRIDSTYLE=yGridStyle, ZGRIDSTYLE=zGridStyle, $
    XMAJOR=xMajor, YMAJOR=yMajor, ZMAJOR=zMajor, $
    XMINOR=xMinor, YMINOR=yMinor, ZMINOR=zMinor, $
    XSHOWTEXT=xShowText, YSHOWTEXT=yShowText, ZSHOWTEXT=zShowText, $
    XSUBGRIDSTYLE=xSubGridStyle, YSUBGRIDSTYLE=ySubGridStyle, ZSUBGRIDSTYLE=zSubGridStyle, $
    XTEXT_COLOR=xTextColor, YTEXT_COLOR=yTextColor, ZTEXT_COLOR=zTextColor, $
    XTEXT_ORIENTATION=xTextOrientation, YTEXT_ORIENTATION=yTextOrientation, $
    ZTEXT_ORIENTATION=zTextOrientation, $
    XTEXTPOS=xTextPos, YTEXTPOS=yTextPos, ZTEXTPOS=zTextPos, $
    XTHICK=xThick, YTHICK=yThick, ZTHICK=zThick, $
    XTICKDIR=xTickdir, YTICKDIR=yTickdir, ZTICKDIR=zTickdir, $
    XTICKFONT_INDEX=xTickFontIndex, YTICKFONT_INDEX=yTickFontIndex, ZTICKFONT_INDEX=zTickFontIndex, $
    XTICKFONT_NAME=xTickFontName, YTICKFONT_NAME=yTickFontName, ZTICKFONT_NAME=zTickFontName, $
    XTICKFONT_SIZE=xTickFontSize, YTICKFONT_SIZE=yTickFontSize, ZTICKFONT_SIZE=zTickFontSize, $
    XTICKFONT_STYLE=xTickFontStyle, YTICKFONT_STYLE=yTickFontStyle, ZTICKFONT_STYLE=zTickFontStyle, $
    XTICKFORMAT=xTickFormat, YTICKFORMAT=yTickFormat, ZTICKFORMAT=zTickFormat, $
    XTICKINTERVAL=xTickInterval, YTICKINTERVAL=yTickInterval, ZTICKINTERVAL=zTickInterval, $
    XTICKLAYOUT=xTickLayout, YTICKLAYOUT=yTickLayout, ZTICKLAYOUT=zTickLayout, $
    XTICKLEN=xTickLen, YTICKLEN=yTickLen, ZTICKLEN=zTickLen, $
    XSUBTICKLEN=xSubTickLen, YSUBTICKLEN=ySubTickLen, ZSUBTICKLEN=zSubTickLen, $
    XTICKNAME=xTickName, YTICKNAME=yTickName, ZTICKNAME=zTickName, $
    XTICKUNITS=xTickUnits, YTICKUNITS=yTickUnits, ZTICKUNITS=zTickUnits, $
    XTICKVALUES=xTickValues, YTICKVALUES=yTickValues, ZTICKVALUES=zTickValues, $
    XTITLE=xTitle, YTITLE=yTitle, ZTITLE=zTitle, $
    XTRANSPARENCY=xTransparency, YTRANSPARENCY=yTransparency, $
    ZTRANSPARENCY=zTransparency, $
    _REF_EXTRA=_extra

    compile_opt idl2, hidden


    if (N_ELEMENTS(axisStyle) ne 0) then begin
      if (KEYWORD_SET(internal)) then begin
        ; If we set the axis_style internally, only change it if the
        ; user didn't manually set it.
        if (~self.axisStyleSetByUser) then begin
          self.style = axisStyle
          self->_ConfigAxes   ; change the style
        endif
      endif else begin
        ; If the user set the axis_style, then remember that and
        ; don't let the internal "axes request" code change it.
        self.style = axisStyle
        self.axisStyleSetByUser = 1b
        self->_ConfigAxes   ; change the style
      endelse
    endif
    
    oDataspace = self->GetDataSpace(/UNNORMALIZED)

    if (ISA(xTickValues) || ISA(yTickValues) || ISA(zTickValues)) then begin
      odataspace->GetProperty, XRANGE = dataspaceXRange, $
        YRANGE=dataspaceYRange, ZRANGE=dataspaceZRange, $
        XLOG=xLog, YLOG=yLog, ZLOG=zLog
      bUpdateRange = 0b

      if (n_elements(xTickValues) ne 0) then begin
        ; Find the min & max range of all ranges and tick values.
        xtickv = xlog ? ALOG10(xtickvalues) : xtickvalues
        xmin = min([xtickv, dataspaceXRange], MAX=xmax)
        newRange = [xmin, xmax]
        if (~ARRAY_EQUAL(newRange, dataspaceXRange)) then begin
          dataspaceXRange = newRange
          bUpdateRange = 1b
        endif
      endif
      if (n_elements(yTickValues) ne 0) then begin
        ; Find the min & max range of all ranges and tick values.
        ytickv = ylog ? ALOG10(ytickvalues) : ytickvalues
        ymin = min([ytickv, dataspaceYRange], MAX=ymax)
        newRange = [ymin, ymax]
        if (~ARRAY_EQUAL(newRange, dataspaceYRange)) then begin
          dataspaceYRange = newRange
          bUpdateRange = 1b
        endif
      endif
      if (n_elements(zTickValues) ne 0) then begin
        ; Find the min & max range of all ranges and tick values.
        ztickv = zlog ? ALOG10(ztickvalues) : ztickvalues
        zmin = min([ztickv, dataspaceZRange], MAX=zmax)
        newRange = [zmin, zmax]
        if (~ARRAY_EQUAL(newRange, dataspaceZRange)) then begin
          dataspaceZRange = newRange
          bUpdateRange = 1b
        endif
      endif
      
      if (bUpdateRange) then begin
        oDataSpace->GetProperty, $
          XREVERSE=xreverse, YREVERSE=yreverse, ZREVERSE=zreverse
        if (xReverse) then dataspaceXRange = dataspaceXRange[[1,0]]
        if (yReverse) then dataspaceYRange = dataspaceYRange[[1,0]]
        if (zReverse) then dataspaceZRange = dataspaceZRange[[1,0]]
        odataspace->OnDataRangeChange, odataspace, $
          dataspaceXRange, dataspaceYRange, dataspaceZRange
      endif
    endif


    if (ISA(xTickFormat) || ISA(xTickUnits)) then begin
      self->GetProperty, XSTYLE=styleCurrent, $
        XTICKFORMAT=oldtickformat, XTICKUNITS=oldtickunits
      ; Force an update of the axis range
      if ((ISA(xTickUnits) && ~ARRAY_EQUAL(oldtickunits, xTickUnits)) || $
        (ISA(xTickFormat) && ~ARRAY_EQUAL(oldtickformat, xTickFormat))) then begin
        xNewStyle = ISA(xstyle) ? xstyle : styleCurrent
        xstyle = !NULL
      endif
    endif

    if (ISA(yTickFormat) || ISA(yTickUnits)) then begin
      self->GetProperty, YSTYLE=styleCurrent, $
        YTICKFORMAT=oldtickformat, YTICKUNITS=oldtickunits
      ; Force an update of the axis range
      if ((ISA(yTickUnits) && ~ARRAY_EQUAL(oldtickunits, yTickUnits)) || $
        (ISA(yTickFormat) && ~ARRAY_EQUAL(oldtickformat, yTickFormat))) then begin
        yNewStyle = ISA(ystyle) ? ystyle : styleCurrent
        ystyle = !NULL
      endif
    endif

    if (ISA(zTickFormat) || ISA(zTickUnits)) then begin
      self->GetProperty, ZSTYLE=styleCurrent, $
        ZTICKFORMAT=oldtickformat, ZTICKUNITS=oldtickunits
      ; Force an update of the axis range
      if ((ISA(zTickUnits) && ~ARRAY_EQUAL(oldtickunits, zTickUnits)) || $
        (ISA(zTickFormat) && ~ARRAY_EQUAL(oldtickformat, zTickFormat))) then begin
        zNewStyle = ISA(zstyle) ? zstyle : styleCurrent
        zstyle = !NULL
      endif
    endif


    if (ISA(xLog) && ~ISA(xstyle)) then begin
      self->GetProperty, XSTYLE=xstyleCurrent, XLOG=oldXlog
      if (oldXlog ne xLog) then $
        xstyle = xstyleCurrent
    endif

    if (ISA(yLog) && ~ISA(ystyle)) then begin
      self->GetProperty, YSTYLE=ystyleCurrent, YLOG=oldYlog
      if (oldYlog ne yLog) then $
        ystyle = ystyleCurrent
    endif

    if (ISA(zLog) && ~ISA(zstyle)) then begin
      self->GetProperty, ZSTYLE=zstyleCurrent, ZLOG=oldZlog
      if (oldZlog ne zLog) then $
        zstyle = zstyleCurrent
    endif


    ; renamed in IDL64, keep for backwards compat
    if (N_Elements(x_Log)) then xLog = x_Log
    if (N_Elements(y_Log)) then yLog = y_Log
    if (N_Elements(z_Log)) then zLog = z_Log

    ; Tell the dataspace - it is in charge
    ; Do not set log on individual axes. IDLitVisAxis
    ; will set appropriately based on the dataspace setting
    if (N_Elements(xLog) || N_Elements(yLog) || N_Elements(zLog)) then begin
      if (OBJ_VALID(oDataSpace)) then begin
        oDataSpace->SetProperty, XLOG=xLog, YLOG=yLog, ZLOG=zLog
      endif
    endif

    ; Normally, we want to update the style before setting any of the
    ; individual axis properties. That way if the axis range changes, things
    ; like ticknames will be applied to the new range.
    ; However, there are cases where we also need to update the style
    ; after setting things like the tickformat and tickunits because
    ; these may change the computed axis range. So we will also call
    ; _UpdateStyle after setting the axis properties (see ***** below)
    if (ISA(xStyle) || ISA(yStyle) || ISA(zStyle)) then begin
      if(ISA(xstyle) && ((xStyle lt 0) || (xStyle gt 3))) then $
        MESSAGE, 'XSTYLE must be 0, 1, 2, or 3.
      if(ISA(ystyle) && ((yStyle lt 0) || (yStyle gt 3))) then $
        MESSAGE, 'YSTYLE must be 0, 1, 2, or 3.
      if(ISA(zstyle) && ((zStyle lt 0) || (zStyle gt 3))) then $
        MESSAGE, 'ZSTYLE must be 0, 1, 2, or 3.
      self->_UpdateStyle, XSTYLE=xStyle, YSTYLE=yStyle, ZSTYLE=zStyle
    endif


    ; Quick way to set them all.
    if (ISA(fontColor)) then begin
      xTextColor = fontColor
      yTextColor = fontColor
      zTextColor = fontColor
    endif
    if (ISA(fontName)) then begin
      xTickFontName = fontName
      yTickFontName = fontName
      zTickFontName = fontName
    endif
    if (ISA(fontSize)) then begin
      xTickFontSize = fontSize
      yTickFontSize = fontSize
      zTickFontSize = fontSize
    endif
    if (ISA(fontStyle)) then begin
      xTickFontStyle = fontStyle
      yTickFontStyle = fontStyle
      zTickFontStyle = fontStyle
    endif

    ; pass command line property settings on to appropriate axes
    hasXkeywords = ISA(xAxisRange) || ISA(xColor) || ISA(xCoordTransform) || $
      ISA(xGridStyle) || ISA(xMajor) || $
      ISA(xMinor) || ISA(xShowText) || $
      ISA(xSubGridStyle) || $
      ISA(xTextColor) || ISA(xTextOrientation) || $
      ISA(xTextPos) || $
      ISA(xTickdir) || ISA(xTickFontIndex) || $
      ISA(xTickFontName) || $
      ISA(xTickFontSize) || ISA(xTickFontStyle) || $
      ISA(xTickFormat) || ISA(xThick) || $
      ISA(xTickInterval) || ISA(xTickLayout) ||$
      ISA(xTickLen) || ISA(xSubTickLen) || $
      ISA(xTickName) || ISA(xTickUnits) || $
      ISA(xTickValues) || ISA(xTitle) || ISA(xTransparency)
    hasYkeywords = ISA(yAxisRange) || ISA(yColor) || ISA(yCoordTransform) || $
      ISA(yGridStyle) || ISA(yMajor) || $
      ISA(yMinor) || ISA(yShowText) || $
      ISA(ySubGridStyle) || $
      ISA(yTextColor) || ISA(yTextOrientation) || $
      ISA(yTextPos) || $
      ISA(yTickdir) || ISA(yTickFontIndex) || $
      ISA(yTickFontName) || $
      ISA(yTickFontSize) || ISA(yTickFontStyle) || $
      ISA(yTickFormat) || ISA(yThick) || $
      ISA(yTickInterval) || ISA(yTickLayout) ||$
      ISA(yTickLen) || ISA(ySubTickLen) || $
      ISA(yTickName) || ISA(yTickUnits) || $
      ISA(yTickValues) || ISA(yTitle) || ISA(yTransparency)
    hasZkeywords = ISA(zAxisRange) || ISA(zColor) || ISA(zCoordTransform) || $
      ISA(zGridStyle) || ISA(zMajor) || $
      ISA(zMinor) || ISA(zShowText) || $
      ISA(zSubGridStyle) || $
      ISA(zTextColor) || ISA(zTextOrientation) || $
      ISA(zTextPos) || $
      ISA(zTickdir) || ISA(zTickFontIndex) || $
      ISA(zTickFontSize) || ISA(zTickFontStyle) || $
      ISA(zTickFontName) || $
      ISA(zTickFormat) || ISA(zThick) || $
      ISA(zTickInterval) || ISA(zTickLayout) ||$
      ISA(zTickLen) || ISA(zSubTickLen) || $
      ISA(zTickName) || ISA(zTickUnits) || $
      ISA(zTickValues) || ISA(zTitle) || ISA(zTransparency)

    if (hasXkeywords || hasYkeywords || hasZkeywords) then begin
        oAxes = self->Get(/ALL, ISA='IDLitVisAxis', COUNT=count)
        for i=0, count-1 do begin
            oAxes[i]->GetProperty, DIRECTION=direction
            case direction of
            0: if (hasXkeywords) then begin
              ; For box axis flip the directions for the other axis.
              if (ISA(xTickdir)) then $
                xTickdir1 = ISA(xTickdir1) ? ~xTickdir : xTickdir
              if (ISA(xTextPos)) then $
                xTextPos1 = ISA(xTextPos1) ? ~xTextPos : xTextPos
              oAxes[i]->SetProperty, $
                AXIS_RANGE=xAxisRange, $
                COLOR=xColor, $
                COORD_TRANSFORM=xCoordTransform, $
                GRIDSTYLE=xGridStyle, $
                MAJOR=xMajor, MINOR=xMinor, $
                TEXT_COLOR=xTextColor, $
                TEXT_ORIENTATION=xTextOrientation, $
                TEXTPOS=xTextPos1, $
                FONT_INDEX=xTickFontIndex, FONT_SIZE=xTickFontSize, $
                FONT_NAME=xTickFontName, $
                FONT_STYLE=xTickFontStyle, $
                SHOWTEXT=xShowText, $
                SUBGRIDSTYLE=xSubGridStyle, $
                TICKFORMAT=xTickFormat, $
                TICKINTERVAL=xTickInterval, TICKLAYOUT=xTickLayout, $
                TICKDIR=xTickdir1, TICKLEN=xTickLen, SUBTICKLEN=xSubTickLen, $
                TICKNAME=xTickName, TICKUNITS=xTickUnits, $
                TICKVALUES=xTickValues, AXIS_TITLE=xTitle, THICK=xThick, $
                TRANSPARENCY=xTransparency
              endif
            1: if (hasYkeywords) then begin
              ; For box axis flip the directions for the other axis.
              if (ISA(yTickdir)) then $
                yTickdir1 = ISA(yTickdir1) ? ~yTickdir : yTickdir
              if (ISA(yTextPos)) then $
                yTextPos1 = ISA(yTextPos1) ? ~yTextPos : yTextPos
              oAxes[i]->SetProperty, $
                AXIS_RANGE=yAxisRange, $
                COLOR=yColor, $
                COORD_TRANSFORM=yCoordTransform, $
                GRIDSTYLE=yGridStyle, $
                MAJOR=yMajor, MINOR=yMinor, $
                TEXT_COLOR=yTextColor, $
                TEXT_ORIENTATION=yTextOrientation, $
                TEXTPOS=yTextPos1, $
                FONT_INDEX=yTickFontIndex, FONT_SIZE=yTickFontSize, $
                FONT_NAME=yTickFontName, $
                FONT_STYLE=yTickFontStyle,  $
                SHOWTEXT=yShowText, $
                SUBGRIDSTYLE=ySubGridStyle, $
                TICKFORMAT=yTickFormat, $
                TICKINTERVAL=yTickInterval, TICKLAYOUT=yTickLayout, $
                TICKDIR=yTickdir1, TICKLEN=yTickLen, SUBTICKLEN=ySubTickLen, $
                TICKNAME=yTickName, TICKUNITS=yTickUnits, $
                TICKVALUES=yTickValues, AXIS_TITLE=yTitle, THICK=yThick, $
                TRANSPARENCY=yTransparency
              endif
            2: if (hasZkeywords) then begin
              ; For box axis flip the directions for the other axis.
              if (ISA(zTickdir)) then $
                zTickdir1 = ISA(zTickdir1) ? ~zTickdir : zTickdir
              if (ISA(zTextPos)) then $
                zTextPos1 = ISA(zTextPos1) ? ~zTextPos : zTextPos
              oAxes[i]->SetProperty, $
                AXIS_RANGE=zAxisRange, $
                COLOR=zColor, $
                COORD_TRANSFORM=zCoordTransform, $
                GRIDSTYLE=zGridStyle, $
                MAJOR=zMajor, MINOR=zMinor, $
                TEXT_COLOR=zTextColor, $
                TEXT_ORIENTATION=zTextOrientation, $
                TEXTPOS=zTextPos, $
                FONT_INDEX=zTickFontIndex, FONT_SIZE=zTickFontSize, $
                FONT_NAME=zTickFontName, $
                FONT_STYLE=zTickFontStyle, $
                SHOWTEXT=zShowText, $
                SUBGRIDSTYLE=zSubGridStyle, $
                TICKFORMAT=zTickFormat, $
                TICKINTERVAL=zTickInterval, TICKLAYOUT=zTickLayout, $
                TICKDIR=zTickdir, TICKLEN=zTickLen, SUBTICKLEN=zSubTickLen, $
                TICKNAME=zTickName, TICKUNITS=zTickUnits, $
                TICKVALUES=zTickValues, AXIS_TITLE=zTitle, THICK=zThick, $
                TRANSPARENCY=zTransparency
              endif
            endcase
        endfor

    endif


    ; ***** Set the style again if necessary. See the big note above.
    if (ISA(xNewStyle) || ISA(yNewStyle) || ISA(zNewStyle)) then begin
      self->_UpdateStyle, XSTYLE=xNewStyle, YSTYLE=yNewStyle, ZSTYLE=zNewStyle
    endif



   
; Not needed: See note above about Range keywords.
;    if (rangeChange) then begin
;        self->_UpdateAxesRanges
;    endif

    ; Set superclass properties
    if (N_ELEMENTS(_extra) gt 0) then $
        self->_IDLitVisualization::SetProperty, _EXTRA=_extra
end


;----------------------------------------------------------------------------
; IIDLitVisDataAxes Interface
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; IDLitVisDataAxes::GetXYZRange
;
; Purpose:
;   Override the GetXYZRange to add some padding for the tick labels.
;-
function IDLitVisDataAxes::GetXYZRange, xRange, yRange, zRange, $
    NO_TRANSFORM=noTransform

    compile_opt idl2, hidden

;    success = self->IDLgrModel::GetXYZRange(xRange, yRange, zRange, $
;        NO_TRANSFORM=noTransform)
;
;    if (success) then begin
;        dx = xRange[1] - xRange[0]
;        dy = yRange[1] - yRange[0]
;        dz = zRange[1] - zRange[0]
;        factor = 0.05
;        xRange = xRange + factor*dx*[-1,1]
;        yRange = yRange + factor*dy*[-1,1]
;        zRange = zRange + factor*dz*[-1,1]
;    endif

    xRange = self.xRange
    yRange = self.yRange
    zRange = self.zRange

    return, 1 - ARRAY_EQUAL([xRange[0], yRange[0], zRange[0]], $
        [xRange[1], yRange[1], zRange[1]])

end

;----------------------------------------------------------------------------
; IIDLDataRangeObserver Interface
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      _IDLitVisDataAxes::OnDataRangeChange
;
; PURPOSE:
;      This procedure method handles notification that the data
;      range has changed.
;
; CALLING SEQUENCE:
;      Obj->[IDLitVisDataAxes::]OnDataRangeChange, oSubject, $
;          XRange, YRange, ZRange
;
; INPUTS:
;      oSubject:  A reference to the object sending notification
;                 of the data range change.
;      XRange:    The new xrange, [xmin, xmax].
;      YRange:    The new xrange, [ymin, ymax].
;      ZRange:    The new xrange, [zmin, zmax].
;-
pro IDLitVisDataAxes::OnDataRangeChange, oSubject, XRange, YRange, ZRange

    compile_opt idl2, hidden

      self.xRange = XRange
      self.yRange = YRange
      oDataSpace = self->GetDataSpace(/UNNORMALIZED)
      if (~OBJ_VALID(oDataSpace)) then $
        return
      threeD = oDataSpace->_IDLitVisualization::Is3D()
      changeTo3D = 0b
      if (threeD) then begin
        oAxes = self->Get(/ALL, ISA='IDLitVisAxis', COUNT=count)
        for i=count-1,0,-1 do begin
          oAxes[i]->GetProperty, DIRECTION=direction
          if (direction eq 2) then break
        endfor
        changeTo3D = i eq -1
      endif
      self.zRange = ZRange

      if (~self._initRangeChange || changeTo3D) then begin
        self->_ConfigAxes
      endif else begin
        self->_UpdateAxesRanges, XRange, YRange, ZRange
      endelse
      
      ; if it is a reset
      if(self.aboutTodoReset) then begin
        oDS = self->GetDataSpace(/UNNORMALIZE)
        oDS._bLockDataChange = 0
        self->GetProperty, XSTYLE=xStyle, YSTYLE=yStyle, ZSTYLE=zStyleTmp
        ; Only set the ZSTYLE if we actually have a Z range,
        ; otherwise we might incorrectly force a range update.
        if (ZRange[0] ne ZRange[1]) then zStyle = zStyleTmp
        self->SetProperty, XSTYLE=xStyle, YSTYLE=yStyle, ZSTYLE=zStyle
        oDS._bLockDataChange = 1
      endif

  ; The style change above might have changed our ranges. If so, modify
  ; the input range arguments and keep going.
  xRange = self.xRange
  yRange = self.yRange
  zRange = self.zRange
  self->_IDLitVisualization::OnDataRangeChange, oSubject, XRange, YRange, ZRange

end


;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
;+
; IDLitVisDataAxes__Define
;
; Purpose:
;   Defines the object structure for an IDLitVisDataAxes object.
;-
pro IDLitVisDataAxes__Define

    compile_opt idl2, hidden

    struct = { IDLitVisDataAxes, $
        inherits IDLitVisualization, $
        xStyle:0b, $
        yStyle:0b, $
        zStyle:0b, $
        aboutTodoReset:0b, $
        xRange: DBLARR(2), $
        yRange: DBLARR(2), $
        zRange: DBLARR(2), $
        _xReverse: 0b, $
        _yReverse: 0b, $
        _zReverse: 0b, $
        _index: 0L, $
        style: 0L, $
        stylePrevious: 0L, $
        axisStyleSetByUser: 0b, $
        _initRangeChange: 0b $
        }
end

