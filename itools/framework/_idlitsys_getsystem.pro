; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/framework/_idlitsys_getsystem.pro#1 $
;
; Copyright (c) 2002-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;  _IDLitsys_GetSystem
;
; PURPOSE:
;   Provides a procedurual interface that retrieves access to the
;   underlying system object. This is used by the command line api for
;   the IDL tool system to bridge the gap between the procedureal and
;   object space.
;
;   This is an internal routine.
;
; CALLING SEQUENCE:
;     oSystem = _IDLitSys_GetSystem()
;
; PARAMETERS
;  None
;
; KEYWORDS
;  NO_CREATE  - If set and if the system doesn't exist, it won't be
;               created. In this case a null is returned
;
;
; RETURN VALUE
;  The system object or an null value if the system cannot be created.
;-
function _IDLitSys_GetSystem, NO_CREATE=NO_CREATE, CLEAN=clean
   compile_opt hidden, idl2

   ;; Maintain the object in a common block!!
   common __IDLitSys$SystemCache$__, c_oSystem

  if (~obj_valid(c_oSystem))then begin

    c_oSystem = OBJ_NEW()

    if (~Keyword_Set(NO_CREATE)) then begin

      canCreate = IDLitGetResource('', dirname, /USERDIR, /WRITE)
      filename = dirname + PATH_SEP() + 'idlitsystem.sav'

; Retrieve the current version.
@idlitconfig.pro

      if (FILE_TEST(filename, /READ) && ~KEYWORD_SET(clean)) then begin
        RESTORE, filename, /RELAXED_STRUCTURE_ASSIGNMENT
        ; Check the restore version with the current version of the tools.
        ; If they are out of sync, destroy the system, and reset the preferences.
        if (~ISA(preferences_version) || preferences_version ne itools_string_version) then begin
          OBJ_DESTROY, c_oSystem
          clean = 1b
        endif else begin
          ; Otherwise, make sure to load the current preference values.
          void = c_oSystem->_RestoreSettings()
        endelse
      endif

      if (~OBJ_VALID(c_oSystem)) then begin
        c_oSystem = obj_new("IDLitSystem")

        ; Delete the iTools preferences
        if KEYWORD_SET(clean) then $
          c_oSystem->_ResetSettings

        if (canCreate) then begin
          preferences_version =  itools_string_version
          SAVE, c_oSystem, preferences_version, FILE=filename
        endif

      endif

    endif

  endif

   
  return, c_oSystem

end
