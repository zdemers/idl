; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/framework/idlitsrvpdf__define.pro#3 $
;
; Copyright (c) 2000-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
; IDLitsrvPDF
;
; Purpose:
;  This file contains the implementation of the IDLitsrvPDF.
;  This class provides a printer service that the entire system can use.
;
;---------------------------------------------------------------------------
; Lifecycle Routines
;---------------------------------------------------------------------------
; IDLitsrvPDF::Init
;
; Purpose:
;    The constructor of the IDLitsrvPDF object.
;
;-------------------------------------------------------------------------
function IDLitsrvPDF::Init, _extra=_extra
    compile_opt idl2, hidden

    ; Just call our super class
    return, self->IDLitsrvCopyWindow::Init("IDLgrPDF", $
                                           _extra=_extra)
end


;-------------------------------------------------------------------------
function IDLitsrvPDF::DoWindowCopy, oWindow, oSource, $
  APPEND=append, $
  CLOSE=close, $
  CENTIMETERS=centimeters, $
  RESOLUTION=resolutionIn, $
  FILENAME=filename, $
  HEIGHT=height, $
  LANDSCAPE=landscapeIn, $
  PAGE_SIZE=pageSize, $
  WIDTH=width, $
  XMARGIN=xmargin, $
  YMARGIN=ymargin, $
  VECTOR=vector, $
  _EXTRA=_extra   ; Note (CT): Do *not* change this to _REF_EXTRA

  compile_opt idl2, hidden

  if(~self->_InitializeOutputDevice(oWindow, oSource))then $
    return, 0

  landscape = KEYWORD_SET(landscapeIn)

  centimeters = KEYWORD_SET(centimeters)
  if (~ISA(pageSize) || pageSize[0] eq 0) then begin
    ; This is true even for landscape.
    pageSize = [8.5d, 11]
  endif else begin
    ; Always convert to inches
    if centimeters then pageSize /= 2.54d
  endelse

  oDev = self->GetDevice()
  oDev->SetProperty, UNITS=1  ; inches
  oDev->IDLgrPDF::AddPage, DIMENSIONS=pageSize, LANDSCAPE=landscape

  ; Some graphics objects assume device coordinates.
  oDev->SetProperty, UNITS=0

  ; Get dimensions
  oWindow->GetProperty, DIMENSIONS=winDims
  oDev->GetProperty, SCREEN_DIMENSIONS=maxDims

  oMon = obj_new('IDLsysMonitorInfo')
  screenRes = (oMon->GetResolutions())[0]
  OBJ_DESTROY, oMon

  shrinkFactor = 1

  ; If user specified either WIDTH or HEIGHT, determine the other one
  ; from the specified one. If both are specified, WIDTH wins.
  if ((ISA(width) && width gt 0) || (ISA(height) && height gt 0)) then begin
    if (ISA(width) && width gt 0) then begin
      height = width*(DOUBLE(winDims[1])/winDims[0])
    endif else begin
      width = height*(DOUBLE(winDims[0])/winDims[1])
    endelse
    ; Always convert to inches
    if centimeters then begin
      width /= 2.54d
      height /= 2.54d
    endif
  endif else begin
    ; If user didn't specify a width or a height, then
    ; try to make the "print" dimensions match the "screen" dimensions
    width = winDims[0]*screenRes/2.54
    height = winDims[1]*screenRes/2.54
    ; Force the plot to fit on the paper.
    shrinkFactor = ((pageSize[landscape]-0.5d)/width) < $
      ((pageSize[1-landscape]-0.5)/height)
    shrinkFactor <= 1
    width *= shrinkFactor
    height *= shrinkFactor
  endelse

  ; Always convert to inches
  if (ISA(xmargin) && centimeters) then xmargin /= 2.54d
  if (ISA(ymargin) && centimeters) then ymargin /= 2.54d

  ; IDL-68849: If the resolution is too high (greater than IDLgrBuffer's max),
  ; decrease the resolution to preserve the user's width and height.
  resolution = resolutionIn < MIN(DOUBLE(maxDims)/[width, height])
  bounds   = ([width, height] * resolution) < maxDims

  ; Figure out the best dimensions and resolution to fit the plot on page
  self._scale = min(bounds/winDims)
  dimensions  = winDims*self._scale

  ; For /LANDSCAPE we need to flip the page size when computing the offset.
  if (landscape) then pagesize = pagesize[[1,0]]
  offset = (pagesize*resolution - dimensions) / 2.

  if (ISA(xmargin)) then offset[0] = xmargin*resolution
  if (ISA(ymargin)) then offset[1] = ymargin*resolution

  cmPerPixel  = 2.54 / resolution
  
  ; IDL-68749: Attempt to make the font sizes match on the screen and PDF,
  ; relative to the size of the graphic (not necessarily point size).
  fontScale = shrinkFactor*(KEYWORD_SET(vector) ? 0.99 : 0.75)

  oDev->SetProperty, DIMENSIONS=dimensions, $
                     RESOLUTION=[cmPerPixel, cmPerPixel], $
                     LOCATION=offset, FONT_SCALE=fontScale
                     
  success = self->IDLitSrvCopyWindow::DoWindowCopy(oWindow, oSource, $
    VECTOR=vector, _EXTRA=_extra)
  if (~success) then return, 0

  oDev->Save, filename

  if (~KEYWORD_SET(append) || KEYWORD_SET(close)) then begin
    oDev->IDLgrPDF::Clear
    ; The PDF doesn't always seem to clean up everything. So start over.
    OBJ_DESTROY, oDev
  endif

  return, 1
end


;-------------------------------------------------------------------------
; IDLitsrvPDF__define
;
; Purpose:
;   Class definition.
pro IDLitsrvPDF__define

    compile_opt idl2, hidden
    struc = {IDLitsrvPDF, $
             inherits IDLitsrvCopyWindow }

end

