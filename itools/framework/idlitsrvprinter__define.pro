; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/itools/framework/idlitsrvprinter__define.pro#1 $
;
; Copyright (c) 2000-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
;; IDLitsrvPrinter
;;
;; Purpose:
;;  This file contains the implementation of the IDLitsrvPrinter.
;;  This class provides a printer service that the entire system can use.
;;
;;---------------------------------------------------------------------------
;; Lifecycle Routines
;;---------------------------------------------------------------------------
;; IDLitsrvPrinter::Init
;;
;; Purpose:
;;    The constructor of the IDLitsrvPrinter object.
;;
;;-------------------------------------------------------------------------
function IDLitsrvPrinter::Init, _extra=_extra
    compile_opt idl2, hidden

    ;; Just call our super class
    return, self->IDLitsrvCopyWindow::Init("IDLgrPrinter", $
                                           _extra=_extra)
end
;;---------------------------------------------------------------------------
;; IDLitsrvPrinter::EndDraw
;;
;; Purpose:
;;   Called when the drawing process is finished. We override this to
;;   send and end document call.

pro IDLitsrvPrinter::EndDraw, oDevice
   compile_opt hidden, idl2

   oDevice->NewDocument
   
   ; Reset these values, so they don't "stick".
   oDevice->SetProperty, LANDSCAPE=0, N_COPIES=1

end
;;---------------------------------------------------------------------------
;; IDLitsrvPrinter::_InitializeOutputDevice
;;
;; Purpose:
;;   Verify that the output device is setup to match our window
;;   attributes
;;
;; Parameters:
;;   oWindow
;;
;; Return Value:
;;   0 - Error
;;   1 - Success

function IDLitsrvPrinter::_InitializeOutputDevice, oWindow, oSource
   compile_opt idl2, hidden

   if(~self->IDLitsrvCopyWindow::_InitializeOutputDevice(oWindow))then $
     return, 0

   oDev = self->GetDevice()

   ;; Setup our scaling factor if one hasn't been set. This will
   ;; enable WYSIWYG
   if(self._bHasScale eq 0)then begin
       oDev->GetProperty, DIMENSION=printdim, resolution=printerRes
       oWindow->GetProperty, DIMENSIONS=windim, resolution = winRes
       self->IDLitsrvCopyWindow::setProperty, SCALE_FACTOR= winRes/printerRes, $
                                              XOFFSET=0, YOFFSET=0
   endif

   return, 1
end

;;---------------------------------------------------------------------------
;; IDLitsrvPrinter::DoAction
;;
;; Purpose:
;;   Sets properties on the device and executes the print
;;
;; Parameters:
;;   oTool
;;
FUNCTION IDLitsrvPrinter::DoAction, oTool, $
    PRINT_NCOPIES=ncopies, $
    PRINT_ORIENTATION=print_orientation, $
    PRINT_XMARGIN=print_xmargin, PRINT_YMARGIN=print_ymargin, $
    PRINT_WIDTH=print_width, PRINT_HEIGHT=print_height, $
    PRINT_UNITS=print_units, PRINT_CENTER=print_center, $
    _EXTRA=_extra

  compile_opt idl2, hidden

  oWindow = oTool->GetCurrentWindow()
  if (~OBJ_VALID(oWindow)) then $
    return, OBJ_NEW()

  hasPreviewSettings = ISA(print_width) && print_width ne 0

  ;; get print settings from window
  oWindow->GetProperty, DIMENSIONS=windim, RESOLUTION=winRes

  oDev = self->GetDevice()

  ; Only set the value if it has been passed in. Otherwise, the user might
  ; have set landscape on the Print Dialog, and we don't want to override it.
  if (KEYWORD_SET(print_orientation)) then $
    oDev->SetProperty, LANDSCAPE=KEYWORD_SET(print_orientation)

  if (ISA(ncopies)) then $
    oDev->SetProperty, N_COPIES=ncopies

  oDev->GetProperty, DIMENSION=printerDims, $
    OFFSETS=printerOffsets, RESOLUTION=printerRes

  offset = [0d, 0d]
  if N_ELEMENTS(print_xmargin) then offset[0] = print_xmargin
  if N_ELEMENTS(print_ymargin) then offset[1] = print_ymargin
  ;; convert from inches to centimeters if needed
  if ~KEYWORD_SET(print_units) then $
    offset *= 2.54d

  ; Correct for the difference between the window's reported resolution,
  ; usually 72 dpi, and the screen's actual resolution, for example 96 dpi.
  oMon = OBJ_NEW('IDLsysMonitorInfo')
  if (OBJ_VALID(oMon)) then begin
    index = oMon->GETPRIMARYMONITORINDEX()
    res = oMon->GETRESOLUTIONS()   ; centimeters per pixel
    res = res[0,index > 0]
    if (res gt 0) then $
      winRes = res
    OBJ_DESTROY, oMon
  endif

  if (hasPreviewSettings) then begin
    printDimCM = [print_width, print_height]
    ;; convert from inches to centimeters if needed
    if ~KEYWORD_SET(print_units) then $
      printDimCM *= 2.54d
    printDims = printDimCM/printerRes
  endif else begin
    ; leave a 1/2 inch margin around the paper,
    ; taking into account any existing margin
    ; this is in pixels:
    printerOffsets = (2.54d/printerRes - 2*printerOffsets) > 0
    maxDims = printerDims - printerOffsets

    ; this is in pixels
    printDims = windim*winres/printerRes

    ; if our plot goes off the paper, shrink it back down
    shrinkFactor = MIN(maxDims/printDims)
    if (shrinkFactor lt 1) then $
      printDims *= shrinkFactor
  endelse

  ; get file writer service - that service will be used to
  ; retrieve the image data
  oWriteFile = oTool->GetService("WRITE_FILE")
  
  ; Get image data
  image = oWriteFile.GetImage(oWindow, WIDTH=printDims[0], HEIGHT=printDims[1])
  imgDims = (SIZE(image, /DIMENSIONS))[1:2]

  ; This offset is in Printer pixels.
  offset = KEYWORD_SET(print_center) ? $
    (printerDims - imgDims)/2d : offset/printerRes
    
  ; Now, print our image.
  oViewTemp = IDLgrView(DIMENSIONS=imgDims, LOCATION=offset, $
    VIEWPLANE_RECT = [0, 0, imgDims])
  oImageTemp = IDLgrImage(image)
  oModelTemp = IDLgrModel()
  oModelTemp->Add, oImageTemp
  oViewTemp->Add, oModelTemp
  oDev->Draw, oViewTemp, /VECTOR  ; VECTOR, so IDLgrPrinter doesn't break up the image into tiles
  Obj_Destroy, oViewTemp
  self->EndDraw, oDev

  ;; Cannot "undo" a copy/print.
  return, obj_new()

END

;;-------------------------------------------------------------------------
;; IDLitsrvPrinter::SetProperty
;;
;; Purpose:
;;   Used to catch the setting of the scale factor property
;;
pro IDLitsrvPrinter::SetProperty, SCALE_FACTOR=scale_factor, $
                   DEFAULT_SCALE=default_scale,$
                   _EXTRA=_EXTRA

    compile_opt hidden, idl2

    if(n_elements(default_scale) gt 0)then $
      self._bHasScale=0

    if(n_elements(scale_factor) gt 0)then $
      self._bHasScale=1

    if(n_elements(_extra) gt 0)then begin
      self->IDLitSrvCopyWindow::SetProperty, _extra=_extra, $
        SCALE_FACTOR=scale_factor
    endif
end
;;-------------------------------------------------------------------------
;; IDLitsrvPrinter__define
;;
;; Purpose:
;;   Class definition.
pro IDLitsrvPrinter__define

    compile_opt idl2, hidden
    struc = {IDLitsrvPrinter, $
             inherits IDLitsrvCopyWindow, $
            _bHasScale : 0b} ;; has the user set the scale factor

end

