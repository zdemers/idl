; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/ncdf_exists.pro#1 $
;
; Copyright (c) 1992-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; NAME:
;	NCDF_EXISTS
;
; PURPOSE:
;	Test for the existence of the NetCDF library
;
; CATEGORY:
;	File Formats
;
; CALLING SEQUENCE:
;	Result = NCDF_EXISTS()
;
; INPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; OUTPUTS:
;	Returns TRUE (1) if the NetCDF data format library is supported
;	Returns FALSE(0) if it is not.
;
; COMMON BLOCKS:
;	NCDFTEST
;
; EXAMPLE:
;	IF ncdf_exists() EQ 0 THEN Fail,"Library not supported on this machine"
;
; MODIFICATION HISTORY
;	Written by:	Joshua Goldstein,  12/8/92
; Rewritten: CT, VIS, Jan 2012
;
;-

FUNCTION ncdf_exists

	has_ncdf = MAX(ROUTINE_INFO(/SYSTEM, /FUNCTIONS) eq 'NCDF_INQUIRE')
	return, has_ncdf
END
