; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/grib_putdata.pro#1 $
;
; Copyright (c) 2010, Research Systems, Inc.  All rights reserved.
;       Unauthorized reproduction prohibited.
;+
; :Description:
;    Puts the value in the record/key
;
; :Params:
;    filename
;       Input GRIB file
;
;    record
;       A fully qualified record (perferably from GRIB_TEMPLATE)
; -

;-------------------------------------------------------------------------------
pro grib_putdata, filename, record
  compile_opt idl2
  on_error, 2
  
  ; Sanity
  if (filename eq '' || ~isa(record)) then message,'Invalid input parameters.'

  ; Determine the type of record, and get the sample
  if file_test(filename) then begin
    f = grib_open(filename)
    h_f = grib_new_from_file(f)
    kiter = grib_keys_iterator_new(h_f)
    while grib_keys_iterator_next(kiter) do begin
      key = grib_keys_iterator_get_name(kiter)
      if key eq 'GRIBEditionNumber' then begin
        version = grib_get(h_f,key)
        if version eq '1' then begin
          h = grib_new_from_samples('grib1')
        endif else if version eq '2' then begin
          h = grib_new_from_samples('grib2')
        endif else begin
          return
        endelse
      endif
    endwhile
    grib_keys_iterator_delete, kiter
    grib_release, h_f
    grib_close,f
  endif else begin
    if ~isa(record['GRIBEditionNumber']) then return
    if record['GRIBEditionNumber'] eq '1' then begin
      h = grib_new_from_samples('grib1')
    endif else if record['GRIBEditionNumber'] eq '2' then begin
      h = grib_new_from_samples('grib2')
    endif else begin
      message, 'GRIBEditionNumber not specified.'
    endelse
  endelse
  
  keys = record.keys()
  
  catch,ierr
  if ierr ne 0 then begin
    if strcmp(!error_state.msg,'GRIB Error: Invalid type',/fold_case) then begin
        message,/reset
      endif else begin
        catch,/cancel
        message, !error_state.msg
      endelse
  endif

  foreach k, keys do begin
    if isa(record[k],/array) then begin
      grib_set_array, h, k, record[k]
    endif else begin
      grib_set, h, k, record[k]
    endelse
  endforeach
  
  if file_test(filename) then begin
    tmp_filename = filepath('IDL_tmp_grib_'+strtrim(fix(randomu(seed)*100000),2)+'.grb', /tmp)
    grib_write_message, tmp_filename, h
    
    openu,to_lun,filename,/get_lun,/append
    writeu,to_lun, read_binary(tmp_filename)
    close,to_lun

    file_delete,tmp_filename
  endif else begin
    grib_write_message, filename, h
  endelse
  grib_release, h
end