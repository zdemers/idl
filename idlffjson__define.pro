; docformat = 'rst'
; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/idlffjson__define.pro#4 $
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
;+
; Creates a IDLffJSON instance
;
;
; :KEYWORDS:
;   None
;
;
; :AUTHOR:
;   Dawn Lenz, VIS
;
; :HISTORY:
;   05May2010:  Dawn Lenz, VIS - Original version
;-
;----------------------------------------------------------------------------

function IDLffJSON::Init

  compile_opt idl2, hidden
  
  ON_ERROR, 2
  HEAP_NOSAVE, self
  
  RETURN, self->IDL_Object::Init()
  
end


;----------------------------------------------------------------------------
pro IDLffJSON::Cleanup

  compile_opt idl2, hidden
  self->IDL_Object::Cleanup
end


;----------------------------------------------------------------------------
;+
; Escapes special characters in JSON formatting
;
; :PARAMS:
;   atomString : in, required
;      the JSON string in which to escape special characters
;
; :RETURNS:
;   The string with special characters converted to escape characters.
;
; :AUTHOR:
;      Dawn Lenz, VIS
;
; :HISTORY:
;      16Aug2010:  Dawn Lenz, VIS - Original version
;-
;----------------------------------------------------------------------------
function IDLffJSON::EscapeSpecialChars, atomString

  compile_opt idl2, hidden, static
  ON_ERROR, 2

  ; Reference:  http://www.json.org
  ; Note: We are not going to escape the / slash (solidus) character.
  ; The JSON spec requires that you handle it if it is escaped (which we
  ; do in ::UnescapeSpecialChars), but it does not require that you escape it.
  ; It is better to not escape it, because then URL's in strings don't
  ; end up looking goofy. If necessary, we could add a keyword to control
  ; this behavior in the future.
  specials = STRING(TRANSPOSE([92b,34b,8b,12b,10b,13b,9b]))
  escapes = ['\\','\"','\b','\f','\n','\r','\t']
  
  newString = atomString
  for i=0,N_ELEMENTS(specials)-1 do begin
    tmp = STRTOK(newString, specials[i], /EXTRACT, /PRESERVE)
    newString = STRJOIN(tmp, escapes[i])
  endfor

  return, newString
end


;----------------------------------------------------------------------------
;+
; Unescapes special characters in JSON formatting
;
; :PARAMS:
;   atomValue : in, required
;      the IDL string variable in which to unescape special characters
;
; :RETURNS:
;   The string with escape characters converted to special characters.
;
; :AUTHOR:
;      Dawn Lenz, VIS
;
; :HISTORY:
;      16Aug2010:  Dawn Lenz, VIS - Original version
;-
;----------------------------------------------------------------------------
function IDLffJSON::UnescapeSpecialChars, atomValue

  compile_opt idl2, hidden
  ON_ERROR, 2
  
  ; Handle all escape characters, including the / slash (solidus).
  ; See the note in ::EscapeSpecialChars.
  ; Reference:  http://www.json.org
  escapes = ['\','/','"','b','f','n','r','t']
  specials = STRING(TRANSPOSE([92b,47b,34b,8b,12b,10b,13b,9b]))
  
  tmp = STRTOK(atomValue, '\', /EXTRACT, /PRESERVE)
  for j=1,N_ELEMENTS(tmp)-1 do begin
    ; A null string indicates an escaped backslash: \\
    if (STRLEN(tmp[j]) eq 0) then begin
      tmp[j] = '\'
      ; Skip over the backslash so we don't accidently escape the next char
      j++
      continue
    endif
    index = (WHERE(STRMID(tmp[j],0,1) eq escapes))[0]
    if (index ge 0) then tmp[j] = specials[index] + STRMID(tmp[j],1)
  endfor

  newString = STRJOIN(tmp)

  return, newString
end


;----------------------------------------------------------------------------
;+
; Generates an IDL variable from a JSON-formatted atom value string
;     An atomic JSON value is a string, number, or true/false/null
;
;
; :PARAMS:
;   atomString : in, required, type="STRING"
;      a JSON-formatted string representing an atomic JSON value
;
; :RETURNS:
;   atomValue : out, type="NULL, BYTE, STRING, LONG64, DOUBLE"
;      an IDL variable representing the atomic JSON value
;
;
; :AUTHOR:
;   Dawn Lenz, VIS
;
; :HISTORY:
;   05May2010:  Dawn Lenz, VIS - Original version
;   21Jun2010:  Dawn Lenz, VIS PSG - Refactored
;-
;----------------------------------------------------------------------------

function IDLffJSON::ParseAtom, atomStringIn

  compile_opt idl2, hidden
  ON_ERROR, 2
  
  atomString = STRTRIM(atomStringIn, 2)
  inputBytes = BYTE(atomString)
  
  ; Default type is number
  type = 'number'

  strStart = STRLOWCASE(STRMID(atomString, 0, 5))
  if (atomString eq 'null' || $
    atomString eq 'true' || $
    atomString eq 'false') then begin
    type = 'byte'
  endif else if (STRMID(atomString, 0, 1) eq '"') then begin
    type = 'string'
  endif
  
  case type of
  
    'byte' : $
      begin
      
      case strStart of
        'null' : atomValue=!NULL
        'true' : atomValue=1B
        'false' : atomValue=0B
      endcase
      
    end
    
    'string' : $
      begin
      
      ; Remove surrounding quotes
      atomValue = STRMID(atomString, 1, STRLEN(atomString)-2)
      atomValue = self->UnescapeSpecialChars(atomValue)
      
    end
    
    'number' : $
      begin
      
      ; If atomString contains '.' or 'e' or 'E', store as double
      ; Otherwise, store as long64
      isFloat = STRPOS(atomString, '.') ge 0 || $
        STRPOS(atomString, 'e') ge 0 || STRPOS(atomString, 'E') ge 0

      on_ioerror, ioErr
      atomValue = isFloat ? DOUBLE(atomString) : LONG64(atomString)
      break
ioErr:
      on_ioerror, null
      atomValue = !NULL
    end
    
  endcase
  
  return, atomValue
end


;----------------------------------------------------------------------------
;+
; Generates a JSON value from JSON-formatted string data
;
;
; :PARAMS:
;   JSONString : in, required, type="STRING"
;      a JSON-formatted string
;
; :RETURNS:
;   JSONValue : out, type="NULL, BYTE, STRING, NUMERIC, HASH, LIST"
;      the parsed JSON value
;        IDL type          JSON type
;        ---------------------------------
;        !NULL             null
;        BYTE (1B/0B)      true/false
;        STRING            string
;        LONG64/DOUBLE       number
;        HASH              object
;        LIST              array
;
;
; :AUTHOR:
;   Dawn Lenz, VIS
;
; :HISTORY:
;   05May2010:  Dawn Lenz, VIS - Original version
;   21Jun2010:  Dawn Lenz, VIS PSG - Refactored
;   04Oct2013:  BJF, VIS - Changed hash to orderedHash to preserve order in the
;                          JSON string.
;-
;----------------------------------------------------------------------------

function IDLffJSON::Parse, DICTIONARY=dictionary, JSONStringIn, DEBUG=debug, $
  TOARRAY=toArrayIn, TOSTRUCT=toStructIn

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then $
    ON_ERROR, 2
  
  JSONString = BYTE(JSONStringIn)
  goodChars = JSONString ne 9b and JSONString ne 10b and JSONString ne 13b
  JSONString = STRTRIM(STRING(JSONString[WHERE(goodChars)]), 2)
  inputBytes = BYTE(JSONString)
  nInputBytes = N_ELEMENTS(inputBytes)
  ; list of objects/arrays starting with the topmost one, down to the current one
  nonAtomList = []
  ; the current object key at each depth (!NULL for arrays)
  keyList = LIST()
  ; denotes how far down the object/array list we are. 0 is the topmost object/array
  depth = 0
  toStruct = KEYWORD_SET(toStructIn)
  toArray = KEYWORD_SET(toArrayIn)
  outClass = KEYWORD_SET(dictionary) ? 'dictionary' : 'OrderedHash'
                         
  
  ; Default top-level type is atom (string, number, true/false/null)
  thisType = 'atom'
  
  char0 = STRMID(JSONString,0,1)
  if (char0 eq '{') then thisType = 'object'
  if (char0 eq '[') then thisType = 'array'
  
  if (thisType eq 'object' || thisType eq 'array') then begin

    ; Initialize top-level value - this is also the return value.
    case thisType of
    
      'object' : $
        begin
        nonAtomList = PTR_NEW(toStruct ? {} : OBJ_NEW(outClass))
        keyList.ADD, !NULL
      end
      'array' : $
        begin
        nonAtomList = PTR_NEW(LIST())
        keyList.ADD, !NULL
      end

    endcase
    
    ; Initialize loop variables
    prevByte = 0B
    isEscaped = 0B
    subStringStart = 1L
    inString = 0
    
    dquote = 34b
    backslash = 92b
    leftbrace = 123b
    rightbrace = 125b
    leftbracket = 91b
    rightbracket = 93b
    colon = 58b
    comma = 44b

    ; Parse the input string byte by byte.  Visit each byte only once.
    for iByte = 1, nInputBytes-1 do begin
    
      thisByte = inputBytes[iByte]
      
      if (thisByte eq dquote && ~isEscaped) then begin
        inString = ~inString
      endif
      
      if ~inString then begin
      
        switch (thisByte) of
        
          ; Left brace/bracket (object/array start)
          leftbrace: begin
            newValue = PTR_NEW(toStruct ? {} : OBJ_NEW(outClass))
            ; fall thru
            end
          leftbracket: begin
            if (thisByte eq leftbracket) then $
              newValue = PTR_NEW(LIST())

            nonAtomList = [nonAtomList, newValue]            
            keyList.ADD, !NULL
            
            ; Add the new depth level
            depth++
            subStringStart = iByte + 1
            break
          end
          
          ; Right brace/bracket (object/array end)
          rightbrace: ; fall thru
          rightbracket: begin
            
            thisSubstring = (iByte gt subStringStart) ? $
              STRING(inputBytes[subStringStart: iByte-1]) : ''
            subStringStart = iByte + 1

            ; If we have a substring, add it to the current object/array
            if STRTRIM(thisSubstring,2) ne '' then begin
            
              atomValue = self->ParseAtom(thisSubstring)
              
              case self->ValueType(*nonAtomList[depth], /PARSE) of
              
                'object' : begin
                  if (toStruct) then begin
                    if (~ISA(atomValue)) then atomValue = "!NULL"
                    *nonAtomList[depth] = CREATE_STRUCT(*nonAtomList[depth], $
                      IDL_VALIDNAME(thisKey, /CONVERT_ALL), atomValue)
                  endif else begin
                    (*nonAtomList[depth])[thisKey] = atomValue
                  endelse
                end
                'array' : begin
                  (*nonAtomList[depth]).Add, atomValue
                end
                
              endcase
              
              
            endif
            
            if depth ne 0 then begin

              newValue = *nonAtomList[-1]
              nonAtomList = nonAtomList[0:-2]
              if (ISA(newValue, 'LIST') && toArray) then begin
                ; For multi-dimensional arrays we want to treat the innermost
                ; subscript as "moving the fastest", i.e. because IDL is
                ; column major it needs to be the outermost dimension.
                ; So use TRANSPOSE. However, also use REFORM so that
                ; one-dimensional arrays don't get turned into 1xN.
                newValue = REFORM(newValue.ToArray(/TRANSPOSE))
              endif else if (ISA(newValue, 'HASH') && toStruct) then begin
                newValue = newValue.ToStruct()
              endif

              case self->ValueType(*nonAtomList[depth-1], /PARSE) of
              
                'object' : begin
                  if (toStruct) then begin
                    if (~ISA(newValue)) then newValue = "!NULL"
                    *nonAtomList[depth-1] = CREATE_STRUCT(*nonAtomList[depth-1], $
                      IDL_VALIDNAME(keylist[depth-1], /CONVERT_ALL), newValue)
                  endif else begin
                    (*nonAtomList[depth-1])[keylist[depth-1]] = newValue
                  endelse
                end
                'array' : begin
                  (*nonAtomList[depth-1]).Add, newValue
                end
                
              endcase
              
              ; Pop to next higher depth
              depth--
              keyList.Remove
              
            endif

            break
          end
          
          ; Colon (key/value pair separator)
          colon: begin
            thisSubstring = (iByte gt subStringStart) ? $
              STRING(inputBytes[subStringStart: iByte-1]) : ''
            subStringStart = iByte + 1
            thisKey = self->ParseAtom(thisSubstring)
            keyList[depth] = thisKey
            break
          end
          
          ; Comma (object/array element separator)
          comma: begin

            thisSubstring = (iByte gt subStringStart) ? $
              STRING(inputBytes[subStringStart: iByte-1]) : ''
            subStringStart = iByte + 1
            
            ; If the current value is an atom, add it at the current depth
            if STRTRIM(thisSubstring,2) ne '' then begin
            
              atomValue = self->ParseAtom(thisSubstring)
              
              case self->ValueType(*nonAtomList[depth], /PARSE) of
              
                'object' : begin
                  if (toStruct) then begin
                    if (~ISA(atomValue)) then atomValue = "!NULL"
                    *nonAtomList[depth] = CREATE_STRUCT(*nonAtomList[depth], $
                      IDL_VALIDNAME(thisKey, /CONVERT_ALL), atomValue)
                  endif else begin
                    (*nonAtomList[depth])[thisKey] = atomValue
                  endelse
                end
                'array' : begin
                  (*nonAtomList[depth]).Add, atomValue
                end
                
              endcase
              
            endif
            
            break
          end
          
          else: break
        endswitch ; thisByte
      
      endif
      
      ; If we have a \ then the next character is escaped, unless we
      ; had an escaped backslash: \\
      isEscaped = (thisByte eq backslash && ~isEscaped)
      prevByte = thisByte
      
    endfor
    
    JSONValue = *nonAtomList[0]
    
  endif else begin
  
    ; If we get here, the topmost value is an atom
    JSONValue = self->ParseAtom(JSONString)
    
  endelse

  if (ISA(JSONValue, 'LIST') && toArray) then $
    JSONValue = JSONValue.ToArray()

  return, JSONValue
end


;----------------------------------------------------------------------------
;+
; Generates a JSON-formatted atom value string from an IDL variable
;     An atomic JSON value is a string, number, or true/false/null
;
;
; :PARAMS:
;   atomValue : in, required, type="NULL, BYTE, STRING, NUMERIC"
;      an IDL variable representing the atomic JSON value
;
; :RETURNS:
;   atomString : out, type="STRING"
;      a JSON-formatted string representing an atomic JSON value
;
;
; :AUTHOR:
;   Dawn Lenz, VIS
;
; :HISTORY:
;   22Jun2010:  Dawn Lenz, VIS - Original version
;-
function IDLffJSON::SerializeAtom, atomValue

  compile_opt idl2, hidden
  ON_ERROR, 2
  
  ; Initialize atomString
  atomString = ''
  tname = SIZE(atomValue, /Tname)
  if (ISA(atomValue, /NULL)) then tname = 'NULL'
  
  switch tname of
  
    'NULL' : begin
      atomString = 'null'
      break
    end
    
    'BYTE' : ; fall thru
    'INT': ; fall thru
    'LONG': ; fall thru
    'UINT': ; fall thru
    'ULONG': ; fall thru
    'LONG64': ; fall thru
    'ULONG64': ; fall thru
    'FLOAT': ; fall thru
    'DOUBLE' : begin
      atomString = self->SerializeNumeric(atomValue)
      break
    end
    
    'STRING' : begin
      ; Look for our special "null" string, which indicates a null value.
      if (STRCMP(atomValue, '!NULL', /FOLD_CASE)) then begin
        atomString = 'null'
      endif else begin
        atomValue = self->EscapeSpecialChars(atomValue)
        atomString = '"' + atomValue + '"'
      endelse
      break
    end
    
  else : MESSAGE, 'Type ' + tname + ' not allowed with JSON.', /NONAME
  
  endswitch

  return, atomString
end


;----------------------------------------------------------------------------
; Super fast handling of array data.
; Also handles scalars.
;
function IDLffJSON::SerializeNumeric, array
  compile_opt idl2, hidden, static

  tname = SIZE(array, /TNAME)
  
  switch tname of
  
    'BYTE' : begin
      ; All non-zero values are "true"
      r = ['false', REPLICATE('true', 255)]
      result = STRJOIN(r[array], ',')
      break
    end
    
    'INT': ; fall thru
    'LONG': ; fall thru
    'UINT': ; fall thru
    'ULONG': ; fall thru
    'LONG64': ; fall thru
    'ULONG64': begin
      result = STRJOIN(STRTRIM(array,2), ',')
      break
    end
    
    'FLOAT': ; fall thru
    'DOUBLE' : begin
      n = N_ELEMENTS(array)
      result = ISA(array, /ARRAY) ? STRARR(SIZE(array, /DIM)) : ''
      a1 = ABS(array)
      needexp = WHERE((a1 ge 1e7 or a1 le 1e-4) and a1 ne 0, /NULL, $
        COMPLEMENT=needregular)
      if (ISA(needexp)) then begin
        arr = STRING(array[needexp], FORMAT='(e0.15)')
        exponentPos = STRPOS(arr, 'e')
        if ISA(exponentPos, /ARRAY) then $
          exponentPos = TRANSPOSE(exponentPos)
        mantissa = STRMID(arr, 0, exponentPos)
        ; Convert "e+00" to just "e", or "e-00" to just "e-"
        exponent = FIX(STRMID(arr, exponentPos + 1))
        exponent = 'e' + STRTRIM(exponent, 2)
        result[needexp] = mantissa
      endif
      if (ISA(needregular)) then begin
        arr = STRING(array[needregular], FORMAT='(f0.15)')
        result[needregular] = arr
      endif
      ; Remove all trailing zeroes. The * makes it so we don't have to check
      ; whether there actually were any trailing zeroes.
      foreach rstr, result, i do begin
        trailingZeroPos = STREGEX(rstr, '0*$')
        if ISA(trailingZeroPos, /ARRAY) then $
          trailingZeroPos = TRANSPOSE(trailingZeroPos)
        result[i] = STRMID(rstr, 0, trailingZeroPos)
      endforeach
      ; Make sure #'s like "1." get written out as "1.0"
      endsWithDecimalPt = WHERE(STRMID(result, 0, /REV) eq '.', /NULL)
      if (ISA(endsWithDecimalPt)) then $
        result[endsWithDecimalPt] += '0'
      if (ISA(exponent)) then $
        result[needexp] += exponent
      result = STRJOIN(result, ",")
      break
    end

    else : MESSAGE, 'Type ' + tname + ' not allowed with JSON.', /NONAME
  endswitch

  ; Concatenate multi-dimensional arrays into a single scalar string.
  while (N_ELEMENTS(result) gt 1) do begin
    result = STRJOIN('[' + result + ']', ",")
  endwhile

  return, result

end


;----------------------------------------------------------------------------
;+
; Generates a JSON value from JSON-formatted string data
;
;
; :PARAMS:
;   JSONValue : in, required, type="NULL, BYTE, STRING, NUMERIC, HASH, LIST"
;      the JSON value to serialize
;
; :RETURNS:
;   JSONString : out, type="STRING"
;      a JSON-formatted string
;
;
; :AUTHOR:
;   Dawn Lenz, VIS
;
; :HISTORY:
;   05May2010:  Dawn Lenz, VIS - Original version
;   22Jun2010:  Dawn Lenz, VIS PSG - Refactored
;-
;----------------------------------------------------------------------------

function IDLffJSON::Serialize, JSONValue, DEBUG=debug

  compile_opt idl2, hidden

  if (~KEYWORD_SET(debug)) then $
    ON_ERROR, 2
  
  JSONString = ''
  
  nElements = N_ELEMENTS(JSONValue)

  type = self->ValueType(JSONValue)
  isStruct = ISA(JSONValue, 'STRUCT')
  
  if (type eq 'object') then begin
    if (isStruct) then begin
      keys = TAG_NAMES(JSONValue)
    endif else begin
      ; Use ordered keys if present
      if JSONValue.HASKEY('keyorder') then begin
        keys = JSONValue['keyorder']
      endif else begin
        keys = JSONValue.KEYS()
      endelse
    endelse
    nElements = N_ELEMENTS(keys)
  endif
  
  case type of
    'object' : JSONString += '{'
    'array' : JSONString += '['
    else : ; do nothing
  endcase

  if (nElements eq 0) then begin

    ; Handle undefined variables and !null
    if (type eq 'atom') then $
      JSONString += self->SerializeAtom(JSONValue)

  endif else begin

    if (ISA(JSONValue, /NUMBER)) then begin

      ; Fast path for arrays of numbers
      JSONString += self->SerializeNumeric(JSONValue)

    endif else begin

      jsonElements = STRARR(nElements)

      for iElement = 0, nElements-1 do begin
      
        idx = (type eq 'object') ? keys[iElement] : iElement
        
        thisElement = (isStruct && (type ne 'array')) ? $
          JSONValue.(iElement) : JSONValue[idx]
        
        if (self->ValueType(thisElement) eq 'atom') then begin
          jsonElements[iElement] = self->SerializeAtom(thisElement)
        endif else begin
          jsonElements[iElement] = self->Serialize(thisElement)
        endelse
      
        if (type eq 'object') then begin
          jsonElements[iElement] = '"' + idx + '":' + jsonElements[iElement]
        endif
          
      endfor

      JSONString += STRJOIN(jsonElements, ',')

    endelse
  
  endelse
  
  case type of
    'object' : JSONString += '}'
    'array' : JSONString += ']'
    else : ; do nothing
  endcase

  return, JSONString
end


;----------------------------------------------------------------------------
;+
; Returns variable type
;
;
; :PARAMS:
;   variable : in ,required
;      the variable whose type to return
;
;
; :AUTHOR:
;   Dawn Lenz, VIS
;
; :HISTORY:
;   22Jun2010:  Dawn Lenz, VIS - Original version
;-
;----------------------------------------------------------------------------

function IDLffJSON::ValueType, variable, PARSE=parse

  compile_opt idl2, hidden
  ON_ERROR, 2
  
  ; Fast return
  if (ISA(variable, /NUMBER, /SCALAR)) then $
    return, 'atom'

  if (KEYWORD_SET(parse) && ISA(variable, /NULL)) then $
    return, 'object'

  ; Be sure to check for a HASH before checking for isArray, since
  ; a Hash will also have a number of elements > 1, but it is still an object.
  if (ISA(variable, 'HASH')) then $
    return, 'object'
  
  ; If we have a structure, it is "always" an array. So only
  ; return "object" if the structure has a single element.
  if (ISA(variable, 'STRUCT') && (N_ELEMENTS(variable) eq 1)) then $
    return, 'object'

  if (ISA(variable, 'LIST') || ISA(variable, /ARRAY)) then $
    return, 'array'

  RETURN, 'atom'
  
end


;----------------------------------------------------------------------------
;+
; IDLffJSON class
;     Manages JSON formatting
;     Reference:  http://www.json.org
;
; :COPYRIGHT:
; Copyright VIS.  Not for external distribution.
;
;
; :EXAMPLES:
;
;   parsing:
;        o = OBJ_NEW('IDLffJSON')
;        jsonString = '{ "fruit": { "apple" : "red" } }'
;        JSONValue = o->Parse(jsonString)
;        print, JSONValue["fruit"]
;        OBJ_DESTROY, o
;
;   serialization:
;        << Run the parsing example above first >>
;        o = OBJ_NEW('IDLffJSON')
;        serialString = o->Serialize(JSONValue)
;        PRINT, serialString
;        OBJ_DESTROY, o
;
;
; :REQUIRES:
;   IDL 8.0 or later
;
; :AUTHOR:
;   Dawn Lenz, VIS
;
; :HISTORY:
;   05May2010:  Dawn Lenz, VIS - Original version
;   23Jun2010:  DDL, VIS PSG - Refactored to use list/hash
;-
;----------------------------------------------------------------------------

pro IDLffJSON__define

  compile_opt idl2, hidden
  
  void = {IDLffJSON_SpecialChar, control:'', escaped:'', escapeStr:''}
  struct = {IDLffJSON, $
    inherits IDL_Object $
    }
    
end

