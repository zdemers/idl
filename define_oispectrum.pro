pro define_oispectrum,oivis_unit,nwave=nwave
;+
; NAME:
;     define_oispectrum

if (n_elements(nwave) eq 0) then nwave=1

oivis_unit = { $
  extver: 0	,$
  nwave: fix(nwave)     ,$    
  oi_revn: 1  	,$
  date_obs: " " ,$
  arrname:  " " ,$
  insname:  " " ,$
  target_id: 0	,$
  time: 0d0	,$
  mjd: 0d0	,$
  int_time: 0d0	,$
  wavelength: ptr_new(dindgen(nwave)), $
  band: ptr_new(dindgen(nwave)), $  
  spectrum: ptr_new(dindgen(nwave)), $
  spectrum_error : ptr_new(dindgen(nwave)), $
  interf_spectrum: ptr_new(dindgen(nwave)), $
  interf_spectrum_error: ptr_new(dindgen(nwave)) $
  }

end
