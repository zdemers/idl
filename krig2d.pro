; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/krig2d.pro#2 $
;
; Copyright (c) 1993-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.

;Return Exponential Covariance Fcn
; C(d) = C1 exp(-3 d/A)   if d > 0
;      = C1 + C0  if d == 0
;
FUNCTION Krig_expon, d, t
  COMPILE_OPT idl2, hidden

  r = t[2] * exp((-3./t[0]) * d)
  r[WHERE(d eq 0, /NULL)] = t[1] + t[2]
  return, r
end

;Return Spherical Covariance Fcn
; C(d) = C1 [ 1 - 1.5(d/A) + 0.5(d/A)^3] if d < A
;      = C1 + C0  if d == 0
;      = 0        if d > A
;
FUNCTION Krig_sphere, d, t
  COMPILE_OPT idl2, hidden

  ; The clipping to < 1 will handle the case d > A, so that v = 0.
  r = d/t[0] < 1
  v = t[2]*(1 - r*(1.5 - 0.5*r*r))
  v[WHERE(r eq 0, /NULL)] = t[1] + t[2]
  return, v
end

;Return Gaussian Covariance Fcn
; C(d) = C1 exp(-3 d^2/A^2)   if d > 0
;      = C1 + C0  if d == 0
;
FUNCTION Krig_gaussian, d, t
  COMPILE_OPT idl2, hidden

  r = t[2] * exp(-3.*(d/t[0])^2)
  r[WHERE(d eq 0, /NULL)] = t[1] + t[2]
  return, r
end


;Return Gaussian Covariance Fcn
; C(d) = C1 (1 - d/A)   if d > 0
;      = C1 + C0  if d == 0
;
FUNCTION Krig_linear, d, t
  COMPILE_OPT idl2, hidden

  r = t[2] * (1 - (d/t[0] < 1))
  r[WHERE(d eq 0, /NULL)] = t[1] + t[2]
  return, r
end


;+
; NAME:
; KRIG_2D
;
; PURPOSE:
; This function interpolates a regularly or irregularly gridded
; set of points Z = F(X,Y) using kriging.
;
; CATEGORY:
; Interpolation, Surface Fitting
;
; CALLING SEQUENCE:
; Result = KRIG2D(Z [, X, Y])
;
; INPUTS:
; X, Y, Z:  arrays containing the X, Y, and Z coordinates of the
;     data points on the surface. Points need not be
;     regularly gridded. For regularly gridded input data,
;     X and Y are not used: the grid spacing is specified
;     via the XGRID and YGRID (or XVALUES and YVALUES)
;     keywords, and Z must be a two dimensional array.
;     For irregular grids, all three parameters must be
;     present and have the same number of elements.
;
; KEYWORD PARAMETERS:
;   Model Parameters:
; EXPONENTIAL: if set (with parameters [A, C0, C1]), use an exponential
;        semivariogram model.
; SPHERICAL:   if set (with parameters [A, C0, C1]), use a spherical
;        semivariogram model.
;
;   Both models use the following parameters:
; A:    the range. At distances beyond A, the semivariogram
;     or covariance remains essentialy constant.
;     See the definition of the functions below.
; C0:   the "nugget," which provides a discontinuity at the
;     origin.
; C1:   the covariance value for a zero distance, and the variance
;     of the random sample Z variable. If only a two element
;     vector is supplied, C1 is set to the sample variance.
;     (C0 + C1) = the "sill," which is the variogram value for
;     very large distances.
;
;  Input grid description:
; REGULAR:  if set, the Z parameter is a two dimensional array
;     of dimensions (N,M), containing measurements over a
;     regular grid. If any of XGRID, YGRID, XVALUES, YVALUES
;     are specified, REGULAR is implied. REGULAR is also
;     implied if there is only one parameter, Z. If REGULAR is
;     set, and no grid (_VALUE or _GRID) specifications are
;     present, the respective grid is set to (0, 1, 2, ...).
; XGRID:    contains a two element array, [xstart, xspacing],
;     defining the input grid in the X direction. Do not
;     specify both XGRID and XVALUES.
; XVALUES:  if present, XVALUES(i) contains the X location
;     of Z(i,j). XVALUES must be dimensioned with N elements.
; YGRID:    contains a two element array, [ystart, yspacing],
;     defining the input grid in the Y direction. Do not
;     specify both YGRID and YVALUES.
; YVALUES:  if present, YVALUES(i) contains the Y location
;     of Z(i,j). YVALUES must be dimensioned with N elements.
;
;  Output grid description:
; GS:   If present, GS must be a two-element vector [XS, YS],
;     where XS is the horizontal spacing between grid points
;     and YS is the vertical spacing. The default is based on
;     the extents of X and Y. If the grid starts at X value
;     Xmin and ends at Xmax, then the default horizontal
;     spacing is (Xmax - Xmin)/(NX-1). YS is computed in the
;     same way. The default grid size, if neither NX or NY
;     are specified, is 26 by 26.
; BOUNDS:   If present, BOUNDS must be a four element array containing
;     the grid limits in X and Y of the output grid:
;     [Xmin, Ymin, Xmax, Ymax]. If not specified, the grid
;     limits are set to the extent of X and Y.
; NX:       The output grid size in the X direction. NX need not
;       be specified if the size can be inferred from GS and
;     BOUNDS. The default value is 26.
; NY:       The output grid size in the Y direction. See NX.
;
; OUTPUTS:
; This function returns a two dimensional floating point array
; containing the interpolated surface, sampled at the grid points.
;
; PROCEDURE:
; Ordinary kriging is used to fit the surface described by the
; data points X,Y, and Z. See: Isaaks and Srivastava,
; "An Introduction to Applied Geostatistics," Oxford University
; Press, 1989, Chapter 12.
;
; The parameters of the data model, the range, nugget, and
; sill, are highly dependent upon the degree and type of spatial
; variation of your data, and should be determined statistically.
; Experimentation, or preferrably rigorus analysis, is required.
;
; For N data points, a system of N+1 simultaneous
; equations are solved for the coefficients of the
; surface. For any interpolation point, the interpolated value
; is:
;          F(x,y) = Sum( w(i) * C(x(i),y(i), x, y)
;
; Formulas used to model the variogram functions:
;   d(i,j) = distance from point i to point j.
;   V = variance of samples.
;   C(i,j) = Covariance of sample i with sample j.
;               C(x0,y0,x1,y1) = Covariance of point (x0,y0) with (x1,y1).
;
;       Exponential covar: C(d) = C1 * EXP(-3*d/A)   if d ne 0.
;                               = C1 + C0          if d eq 0.
;
;       Spherical covar:   C(d) = C1(1.0 - 1.5 * d/a + 0.5 * (d/a)^3)
;                               = C1 + C0           if d eq 0.
;                               = 0                 if d > a.
;
; EXAMPLES:
; Example 1: Irregularly gridded cases
; Make a random set of points that lie on a gaussian:
;   n = 15    ;# random points
;   x = RANDOMU(seed, n)
;   y = RANDOMU(seed, n)
;   z = exp(-2 * ((x-.5)^2 + (y-.5)^2)) ;The gaussian
;
;   get a 26 by 26 grid over the rectangle bounding x and y:
;   e = [ 0.25, 0.0]  ;Range and nugget are 0.25, and 0.
;       ;(These numbers are dependent upon
;       ;your data model.)
;   r = krig2d(z, x, y, EXPON = e)  ;Get the surface.
;
;   Or: get a surface over the unit square, with spacing of 0.05:
;   r = krig2d(z, x, y, EXPON=e, GS=[0.05, 0.05], BOUNDS=[0,0,1,1])
;
;   Or: get a 10 by 10 surface over the rectangle bounding x and y:
;   r = krig2d(z, x, y, EXPON=e, NX=10, NY=10)
;
; Example 2: Regularly gridded cases
;   s = [ 10., 0.2]     ;Range and sill, data dependent.
;   z = randomu(seed, 5, 6)   ;Make some random data

; interpolate to a 26 x 26 grid:
;   CONTOUR, krig2d(z, /REGULAR, SPHERICAL = s)
;
; MODIFICATION HISTORY:
; DMS, RSI, March, 1993. Written.
;       GSL, RSI, October 1997.  Replaced obsolete LUDCMP,LUBKSB with
;                                LUDC and LUSOL.
;   CT, RSI, July 2003: Subtract fudge factor when computing NX, NY
;       to avoid roundoff errors.
;   CT, VIS, Oct 2013: Per Mike's suggestion, speed up the algorithm by
;       moving LUSOL out of the loops, and vectorizing the inner loop.
;       Add DOUBLE, GAUSSIAN, LINEAR, XOUT, YOUT keywords.
;       Fix the spherical covariance computation for d > A.
;       
;-
FUNCTION krig2d, z, x, y, $
  DOUBLE=doubleIn, $
  REGULAR = regular, XGRID=xgrid, $
  XVALUES = xvalues, YGRID = ygrid, YVALUES = yvalues, $
  GS = gs, BOUNDS = bounds, NX = nx0, NY = ny0, $
  EXPONENTIAL = exponential, GAUSSIAN=gaussian, $
  LINEAR=linear, SPHERICAL = spherical, $
  XOUT=xout, YOUT=yout

  compile_opt idl2, hidden
  on_error, 2

  s = size(z)   ;Assume 2D
  nx = s[1]
  ny = s[2]

  reg = keyword_set(regular) || (n_params() eq 1)
  dbl = KEYWORD_SET(doubleIn)
  clearExcept = CHECK_MATH(/NOCLEAR) eq 0

  if n_elements(xgrid) eq 2 then begin
    x = (dbl ? dindgen(nx) : findgen(nx)) * xgrid[1] + xgrid[0]
    reg = 1b
  endif else if n_elements(xvalues) gt 0 then begin
    if n_elements(xvalues) ne nx then $
      message,'Xvalues must have '+string(nx)+' elements.'
    x = xvalues
    reg = 1b
  endif

  if n_elements(ygrid) eq 2 then begin
    y = (dbl ? dindgen(ny) : findgen(ny)) * ygrid[1] + ygrid[0]
    reg = 1b
  endif else if n_elements(yvalues) gt 0 then begin
    if n_elements(yvalues) ne ny then $
      message,'Yvalues must have '+string(ny)+' elements.'
    y = yvalues
    reg = 1b
  endif

  if reg then begin
    if s[0] ne 2 then message,'Z array must be 2D for regular grids'
    if n_elements(x) ne nx then x = findgen(nx)
    if n_elements(y) ne ny then y = findgen(ny)
    x = x # replicate(dbl ? 1d : 1., ny)  ;Expand to full arrays.
    y = replicate(dbl ? 1d : 1.,nx) # y
  endif

  n = n_elements(x)
  if n ne n_elements(y) || n ne n_elements(z) then $
    message,'x, y, and z must have same number of elements.'

  case 1 of
    keyword_set(exponential): begin
      t = exponential
      fname = 'KRIG_EXPON'
      end
    keyword_set(spherical): begin
      t = spherical
      fname = 'KRIG_SPHERE'
    end
    keyword_set(gaussian): begin
      t = gaussian
      fname = 'KRIG_GAUSSIAN'
    end
    keyword_set(linear): begin
      t = linear
      fname = 'KRIG_LINEAR'
    end
    else: MESSAGE, 'You must choose one of the following models: ' + $
      'EXPONENTIAL, GAUSSIAN, LINEAR, or SPHERICAL'
  endcase

  if n_elements(t) eq 2 then begin    ;Default value for variance?
    mz = total(z) / n   ;Mean of z
    var = total((z - mz)^2, DOUBLE=dbl)/n ;Variance of Z
    t = [t, var-t[1]] ;Default value for C1
  endif

  m = n + 1     ;# of eqns to solve
  a = dbl ? dblarr(m, m) : fltarr(m, m)

  ; Construct the symmetric distance matrix.
  for i=0, n-2 do begin
    j = LINDGEN(n-i) + i  ; do the columns all at once
    d = (x[i]-x[j])^2 + (y[i]-y[j])^2  ;Distance squared
    ;symmetric
    a[i,j] = d
    a[j,i] = d
  endfor

  a = call_function(fname, sqrt(a), t)        ;Get coefficient matrix
  a[n,*] = 1.0            ;Fill edges
  a[*,n] = 1.0
  a[n,n] = 0.0

  LUDC, a, indx, DOUBLE=dbl             ;Solution using LU decomposition

  if n_elements(nx0) le 0 then nx0 = 26 ;Defaults for nx and ny
  if n_elements(ny0) le 0 then ny0 = 26

  xmin = min(x, max = xmax)   ;Make the grid...
  ymin = min(y, max = ymax)

  if n_elements(bounds) lt 4 then bounds = [xmin, ymin, xmax, ymax]

  if n_elements(gs) lt 2 then $   ;Compute grid spacing from bounds
    gs = [(bounds[2]-bounds[0])/(nx0-1.), (bounds[3]-bounds[1])/(ny0-1.)]

  ; Subtract off a fudge factor to lessen roundoff errors.
  nx = ceil((bounds[2]-bounds[0])/gs[0] - 1e-5)+1 ;# of elements
  ny = ceil((bounds[3]-bounds[1])/gs[1] - 1e-5)+1

  xout = bounds[0] + gs[0]*(dbl ? DINDGEN(nx) : FINDGEN(nx))
  yout = bounds[1] + gs[1]*(dbl ? DINDGEN(ny) : FINDGEN(ny))

  ; One extra for Lagrange constraint
  d = dbl ? DBLARR(m) : FLTARR(m)
  result = dbl ? DBLARR(nx,ny,/nozero) : FLTARR(nx,ny,/nozero)

  az = LUSOL(a, indx, [REFORM(z,N_ELEMENTS(z)),0.0], DOUBLE=dbl)
  az = REBIN(TRANSPOSE(az), nx, n+1)
  xx = REBIN(REFORM(x, 1, n), nx, n)
  yy = REBIN(REFORM(y, 1, n), nx, n)
  dxsquare = (xx - REBIN(xout, nx, n))^2

  ; Do each row separately
  for j=0,ny-1 do begin
    y0 = yout[j]
    ; Do all of the columns in parallel
    d = sqrt(dxsquare + (yy - y0)^2)
    d = CALL_FUNCTION(fname, d, t)
    ; Be sure to add the last row of AZ, which is the Lagrange constraint
    result[*,j] = TOTAL(d*az, 2) + az[*,n]
  endfor

  ; Clear any harmless floating underflows.
  if (clearExcept) then void = CHECK_MATH(MASK=32)

  return, result
end
