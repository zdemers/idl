; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/idli18nfontname.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;----------------------------------------------------------------------------
;+
; :Description:
;    The IDLi18nFontName function passes back a font name to use for object
;    graphics annotations. The font is chosen based on the language in effect
;    determined by LANGUAGE_GET().
;    
;    Note: This function is a utility routine for use in iTools, Graphics
;    and ENVI. It is currently not documented for general usage.
;
; :Returns:
;    True: Valid font name passed back for the current language
;    False: Not able to determine applicable font or not on Windows
;   
; :Params:
;    fontName (output):
;      The font name for the current locale. Several attempts may be made to
;      find a font. The sequence is based on testing and recommendations.
;
; :Keywords:
;    None
;
;
; :Author:
;   AY, VIS, Aug 2012. 
;-
;------------------------------------------------------------------------------
function _IDLi18nFontNameIsAvailable, fontNames, fontName

  compile_opt idl2, hidden

  void = where(strupcase(fontNames) eq strupcase(fontName), count)
  return, (count gt 0)
end

function IDLi18nFontName, fontName

  compile_opt idl2, hidden

  ; This function is for use only on Windows
  if (!version.os_family ne "Windows") then begin
    fontName = ''
    return, 0
  endif
  
  ; Fast path
  if (LANGUAGE_GET() eq 1033) then begin
    fontName = 'Helvetica'   ; English
    return, 0
  endif

  oBuffer = Obj_New('IDLgrBuffer', DIMENSIONS=[2,2])
  fontNames = oBuffer->GetFontNames('*', STYLES='')
  Obj_Destroy, oBuffer
  ; Debugging:
  ; foreach index, sort(fontNames) do print, fontNames(index)
  
  ; Lists of CJK fonts and availability based on OS
  ; http://en.wikipedia.org/wiki/List_of_CJK_fonts
  
  case LANGUAGE_GET() of
  1041: begin ; Japanese
    fontName = 'MS UI Gothic' ; fontname based on default UI font
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'MS PGothic' ; fontname supplied by VIS development/testing
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'MS Gothic' ; fontname supplied by VIS development/testing
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'Arial Unicode MS' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = ''
    return, 0    
  end
  2052: begin ; Chinese 
    fontName = 'Microsoft YaHei' ; fontname based on default UI font
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'Microsoft JhengHei' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'MS Hei' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'SimHei' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'Arial Unicode MS' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = ''
    return, 0    
  end
  1042: begin ; Korean 
    fontName = 'Malgun Gothic' ; fontname based on default UI font
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'Dotum' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'DotumChe' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'GulimChe' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = 'Arial Unicode MS' ; from http://en.wikipedia.org/wiki/List_of_CJK_fonts
    if (_IDLi18nFontNameIsAvailable(fontNames, fontName)) then return, 1
    fontName = ''
    return, 0    
  end
  else: begin                    ; unknown, in future might want try an uber font
                                 ; such as: 'Arial Unicode MS'
    fontName = ''
    return, 0
  end
  endcase
  
  return, 1
end


