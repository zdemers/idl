; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/file_tar.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+


;---------------------------------------------------------------------------
function FileTar_Header, filename, finfo, filemode, POSIX=posix

  compile_opt idl2, hidden

  header = BYTARR(512)
  header[257:261] = BYTE('ustar')
  header[263:264] = BYTE('00')
  posix = KEYWORD_SET(posix)
  needPosix = posix
  if (STRLEN(filename) gt 255) then needPosix = 1b

  if (~needPosix) then begin
    if (STRLEN(filename) le 100) then begin
      header[0] = BYTE(filename)    
    endif else begin
      ; Do not use /MARK_DIR (so says the tar spec)
      dir = FILE_DIRNAME(filename)
      name = FILE_BASENAME(filename)
      while (STRLEN(dir) gt 155) do begin
        name = FILE_BASENAME(dir) + '/' + name
        dir = FILE_DIRNAME(dir)
      endwhile
      if (STRLEN(name) gt 100 || dir eq '' || dir eq '.') then begin
        needPosix = 1b
;        MESSAGE, /NONAME, 'Filename too long: "' + filename + '"'
      endif else begin
        name = STRJOIN(STRTOK(name, '\', /EXTRACT, /PRESERVE), '/')
        dir = STRJOIN(STRTOK(dir, '\', /EXTRACT, /PRESERVE), '/')
        if (finfo.directory) then name += '/'
        header[0] = BYTE(STRMID(name, 0, 100))
        header[345] = BYTE(STRMID(dir, 0, 155))
      endelse
    endelse
  endif

  if (needPosix) then begin
    fakeFilename = './PaxHeaders/' + FILE_BASENAME(filename)
    header[0] = BYTE(STRMID(fakeFilename, 0, 99))
    typeflag = 'x'
    fmode = "644   ; rw-r--r--
    time = ULONG64(SYSTIME(1,/UTC))
    record = ' path=' + filename + STRING(10b)
    len = STRLEN(record)
    for digit=1,9 do begin
      if (len lt (10ull^digit - digit)) then break
    endfor
    len += digit
    n = 512*((len + 511ull)/512)
    paxdata = BYTARR(n)
    paxdata[0] = BYTE(STRTRIM(len,2) + record)
    ; Size of the extended PAX header
    nbytes = len
    fakename = FILE_BASENAME(filename)
    if (finfo.directory) then fakename += '/'
    newheader = FileTar_Header(STRMID(fakename,0,99), finfo, filemode)
  endif else begin
    typeflag = finfo.directory ? '5' : '0'
    fmode = filemode
    nbytes = finfo.size
    time = finfo.mtime
  endelse

  header[156] = BYTE(typeflag)
  header[100:106] = BYTE(STRING(fmode, FORMAT='(o7.7)'))
  header[124:134] = BYTE(STRING(nbytes, FORMAT='(o11.11)'))
  header[136:146] = BYTE(STRING(time, FORMAT='(o11.11)'))

  ; To compute the header checksum, fill the checksum value with space
  ; characters, then add up the entire header. Then fill in the checksum value.
  header[148:155] = 32b
  checksum = TOTAL(header, /INTEGER)
  header[148:153] = BYTE(STRING(checksum, FORMAT='(o6.6)'))
  header[154:155] = [0b, 32b]

  if (needPosix) then begin
    header = [header, paxdata, newheader]
  endif

  RETURN, header
end


;---------------------------------------------------------------------------
pro FILE_TAR, filesIn, outfileIn, $
  BUFFER=buffer, $
  DEBUG=debug, $
  FILES=files, $
  GZIP=gzip, $
  LIST=listIn, $
  POSIX=posix, $
  VERBOSE=verboseIn

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then $
    ON_ERROR, 2

  if (N_PARAMS() eq 0) then $
    MESSAGE, 'Incorrect number of arguments.'
  
  sep = PATH_SEP()
  files = ''
  fileList = LIST()
  filenames = LIST()
  doList = KEYWORD_SET(listIn)
  verbose = KEYWORD_SET(verboseIn)
  hasBuffer = ARG_PRESENT(buffer)

  if (N_PARAMS() eq 2 && hasBuffer) then $
    MESSAGE, 'BUFFER keyword can not be used with FileOut argument.'
    
  if (ISA(buffer) && ~hasBuffer) then $
    MESSAGE, 'BUFFER must be set to a named variable, not an expression.'
    
  for i=0, N_ELEMENTS(filesIn)-1 do begin
    if (~FILE_TEST(filesIn[i], /READ)) then $
      MESSAGE, 'Unable to open file: ' + filesIn[i]
    if (FILE_TEST(filesIn[i], /DIRECTORY)) then begin
      dir = filesIn[i]
      c = STRMID(dir, 0, /REVERSE_OFFSET)
      if (c ne '/' && c ne '\') then dir += sep
      ; Remove the directory's parent from both the directory and all
      ; of the contained files/subdirectories.
      parentdir = FILE_DIRNAME(dir)
      parentdir = (parentdir ne '.') ? parentdir + sep : ''
      parentlen = STRLEN(parentdir)
      fileList.Add, dir
      if (~doList) then $
        filenames.Add, FILE_BASENAME(dir) + sep
      subfiles = FILE_SEARCH(dir, '*', COUNT=nf, /MARK_DIR, /MATCH_INITIAL_DOT)
      for j=0,nf-1 do begin
        subfile = subfiles[j]
        c = STRMID(subfile, 0, /REVERSE_OFFSET)
        isDir = (c eq '/' || c eq '\')
        if (~isDir && ~FILE_TEST(subfile, /REGULAR)) then continue
        if (~STRCMP(subfile, dir, parentlen)) then continue
        fileList.Add, subfile
        if (~doList) then begin
          name = STRMID(subfile, parentlen)
          filenames.Add, name
        endif
      endfor
    endif else begin
      ; TODO: Filter out symlinks?
      if (FILE_TEST(filesIn[i], /REGULAR)) then begin
        fileList.Add, filesIn[i]
        if (~doList) then $
          filenames.Add, FILE_BASENAME(filesIn[i])
      endif
    endelse
  endfor

  files = fileList.ToArray(/NO_COPY)
  fileList = 0
  
  if (doList) then begin
    if (verbose) then begin
      foreach f, files do MESSAGE, /INFO, /NONAME, f
    endif
    return
  endif

  filenames = filenames.ToArray(/NO_COPY)
  nfiles = N_ELEMENTS(files)

  if (N_PARAMS() eq 1 || hasBuffer) then begin
    if (nfiles eq 0) then $
      MESSAGE, 'Incorrect number of arguments.'
    fname = FILE_BASENAME(filesIn[0])
    dotPos = STRPOS(fname, '.', /REVERSE_SEARCH)
    if (dotPos gt 0) then fname = STRMID(fname, 0, dotPos)
    outfile = FILE_DIRNAME(filesIn[0], /MARK) + fname + '.tar'
  endif else begin
    if (N_ELEMENTS(outfileIn) gt 1) then $
      MESSAGE, 'Output file must be a scalar string.'
    outfile = outfileIn[0]
    outfilegz = outfile
    hasDot = STRPOS(outfile, '.', /REVERSE_SEARCH)
    suffix = (hasDot ge 0) ? STRMID(outfile, hasDot) : ''
    sufflow = STRLOWCASE(suffix)
    if (sufflow eq '.gz' || sufflow eq '.tgz') then begin
      gzip = 1b
      outfile = STRMID(outfile, 0, hasDot)
    endif else begin
      if (KEYWORD_SET(gzip)) then outfilegz += '.gz'
    endelse
  endelse

  if (~hasBuffer) then begin
    dir = FILE_DIRNAME(outfile)
    if (~FILE_TEST(dir, /WRITE)) then begin
      MESSAGE, 'Unable to write to the file: ' + outfile
    endif
  endif
  
  if (verbose) then tic
  
  sep = PATH_SEP()
  
  if (hasBuffer) then begin
    outlist = LIST()
  endif else begin
    OPENW, tar_lun, outfile, /GET_LUN
  endelse
  
  total_size = 0LL

  for i=0, nfiles-1 do begin

    if (sep eq '\') then begin
      filenames[i] = STRJOIN(STRTOK(filenames[i], '\', /EXTRACT, /PRESERVE), '/')
    endif

    finfo = FILE_INFO(files[i])
    void = FILE_TEST(files[i], GET_MODE=filemode)
    file_arr = []

    if (finfo.regular && finfo.size gt 0) then begin
      lun = 0
      CATCH, iErr
      if (iErr ne 0) then begin
        CATCH, /CANCEL
        if (lun ne 0) then FREE_LUN, lun
        goto, handleError
      endif

      OPENR, lun, files[i], /GET_LUN        
      file_arr = BYTARR(finfo.size, /NOZERO)
      READU, lun, file_arr
      FREE_LUN, lun
      lun = 0      
    endif else begin
      finfo.size = 0
    endelse

    total_size += finfo.size

    header = FileTar_Header(filenames[i], finfo, filemode, POSIX=posix)

    if (hasBuffer) then begin
      outlist.Add, header
    endif else begin
      WRITEU, tar_lun, header
    endelse
    
    if (finfo.size gt 0) then begin
      nextra = 512 - finfo.size mod 512
      if (hasBuffer) then begin
        outlist.Add, file_arr
        if (nextra gt 0 && nextra lt 512) then begin
          outlist.Add, BYTARR(nextra)
        endif
      endif else begin
        WRITEU, tar_lun, file_arr
        if (nextra gt 0 && nextra lt 512) then begin
          WRITEU, tar_lun, BYTARR(nextra)
        endif
      endelse
    endif

    if (verbose) then begin
      msg = files[i]
      if (finfo.size gt 0) then begin
        msg += ', ' + STRTRIM(finfo.size,2) + ' bytes'
      endif
      MESSAGE, /INFO, /NONAME, msg
    endif

  endfor

  if (hasBuffer) then begin
    outlist.Add, BYTARR(1024)
    buffer = outlist.ToArray(DIMENSION=1, /NO_COPY)
    outlist = 0
  endif else begin
    WRITEU, tar_lun, BYTARR(1024)
    FREE_LUN, tar_lun, /FORCE
  endelse
  tar_lun = 0
  
  if (verbose) then begin
    bytes = STRTRIM(total_size,2) + ' bytes'
    MESSAGE, /INFO, /NONAME, 'Total ' + STRTRIM(nfiles,2) + $
      ' files, ' + bytes
  endif

  if (~ISA(files)) then files = ''

  if (KEYWORD_SET(gzip)) then begin
    if (~hasBuffer && ISA(outfilegz)) then begin
      FILE_GZIP, outfile, outfilegz, $
        /DELETE, VERBOSE=verbose
    endif else begin
      if (hasBuffer) then begin
        buffer = ZLIB_COMPRESS(buffer, /GZIP_HEADER)
      endif else begin
        FILE_GZIP, outfile, /DELETE, VERBOSE=verbose
      endelse
    endelse
  endif

  if (verbose) then toc
  
  return

handleError:
  if (tar_lun ne 0) then FREE_LUN, tar_lun
  FILE_DELETE, outfile, /QUIET
  files = ''
  MESSAGE, /REISSUE_LAST

end

