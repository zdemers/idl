; $Id: //depot/InDevelopment/scrums/IDL_Kraken/idl/idldir/lib/toc.pro#7 $
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;-------------------------------------------------------------------------------
;+
; :Description:
;   Returns the toc of the clock
;
; :Parameters:
;   clock - (Optional) ID of clock to toc, can be an array of IDs
;   
; :Keywords:
;   REPORT - Generate a profiler report
;-
function toc, clock, REPORT=profilerReport

  compile_opt idl2, hidden
  on_error, 2
  common tictoc, tictoc_time, tictoc_profiler

  time = SYSTIME(/SECONDS)

  if (~ISA(clock) && ~ISA(tictoc_time)) then begin
    MESSAGE, 'No tic, no toc', /INFORMATIONAL
    return, !null
  endif
  
  if (~ISA(clock) && KEYWORD_SET(tictoc_profiler)) then begin
    tictoc_profiler = 0b
    PROFILER, /SYSTEM, /CLEAR
    PROFILER, /CLEAR
    !null = IDLNotify('IDLProfilerRefresh')
  endif

  if (ARG_PRESENT(profilerReport)) then $
    PROFILER, /REPORT, DATA=profilerReport

  result = ISA(clock) ? time - clock.time : time - tictoc_time
  
  return, result
end


;-------------------------------------------------------------------------------
;+
; :Description:
;   Prints the toc of the clock
;
; :Parameters:
;   clock - (Optional) ID of clock to toc, can be an array
;   
; :Keywords:
;   LUN - Print out to LUN instead of to the screen
;   
;   REPORT - Generate a profiler report
;-
pro toc, clock, LUN=lun, REPORT=profilerReport
  
  compile_opt idl2, hidden
  on_error, 2
  common tictoc, tictoc_time, tictoc_profiler

  time = TOC(clock, REPORT=profilerReport)
  if (~ISA(time)) then return
  
  for i=0,N_ELEMENTS(time)-1 do begin
    str = '% Time elapsed'
    if (ISA(clock) && clock[i].name ne '') then begin
      str += ' ' + clock[i].name
    endif
    str += ': ' + STRTRIM(time[i],2) + ' seconds.'
    if (~ISA(lun)) then begin 
      print, str
    endif else begin
      printf, lun, str    
    endelse
  endfor

end
