;$Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/read_spr.pro#1 $
;
; Copyright (c) 1994-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;       READ_SPR
;
; PURPOSE:
;       This function reads a row-indexed sparse matrix from a specified
;	file and returns it as the result.    Row-indexed sparse matrices
;	are created by using the Numerical Recipes routine SPRSIN.
;
; CATEGORY:
;      	Sparse Matrix File I/O
;
; CALLING SEQUENCE:
;       result = READ_SPR('Filename')
;
; INPUTS:
;	Filename:  Name of file containing a row-indexed sparse matrix
;
; KEYWORD PARAMETERS;
;	NONE
;
; OUTPUTS:
;	result:  Row-indexed sparse matrix
;
;
; MODIFICATION HISTORY:
;       Written by:     BMH, 1/94.
;       CT, VIS, Oct 2010: Handle files written with 64-bit IDL.
;-

FUNCTION  READ_SPR, filename

COMPILE_OPT idl2, hidden

; result format = {sa:FLTARR(nmax) or sa:DBLARR(nmax), ija:LONARR(nmax) or LON64ARR(nmax)}
;
nmax = 0L
type = 0L

ON_IOERROR, BADFILE


OPENR, fileLUN, filename, /GET_LUN

;Read type and size information
; Note: This assumes "nmax" is a 32-bit integer. This will work fine even on
; 64-bit IDL as long as you have < 2^31 elements in your sparse matrix.
READU, fileLUN, nmax, type

fsize = (FSTAT(fileLUN)).size

floatSize = (type eq 4) ? 4 : 8
sizeIfLong64 = 8LL + nmax*(floatSize + 8LL)
isLong64 = fsize eq sizeIfLong64

;Define resulting structure based on the data size and type
result = { $
  SA: (type eq 4) ? FLTARR(nmax, /NOZERO) : DBLARR(nmax, /NOZERO), $
  IJA: isLong64 ? LON64ARR(nmax, /NOZERO) : LONARR(nmax, /NOZERO) $
  }

;Read sparse matrix
READU, fileLUN, result

FREE_LUN, fileLUN

; If we read a 64-bit file on a 32-bit IDL (or vice versa)
; then convert the indices to the appropriate memory integer size.
if (!version.memory_bits eq 32 && isLong64) then begin
  result = {SA: result.sa, IJA: LONG(result.ija)}
endif else if (!version.memory_bits eq 64 && ~isLong64) then begin
  result = {SA: result.sa, IJA: LONG64(result.ija)}
endif

RETURN, result


BADFILE:
IF (N_Elements(fileLUN) GT 0L) THEN $
   FREE_LUN, fileLUN
MESSAGE, 'Error reading sparse matrix file: ' + filename

END



