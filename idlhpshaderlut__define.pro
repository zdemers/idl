; $Id:
;
; Copyright (c) 2005-2013, Exelis Visual Information Solutions, Inc.  All rights reserved.
;    Unauthorized reproduction prohibited.
;
;----------------------------------------------------------------------------
; Class Name:
;   IDLhpShaderLUT
;
; Purpose:
;   This contains the GLSL implementation of IDLhpLookupTableFilter. It will
;   be used instead of the C/C++ implementation in IDLhpLookupTableFilter
;   if GPU HW support is available.
;

;----------------------------------------------------------------------------
; Lifecycle Methods
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; IDLhpShaderLUT::Init
;
function IDLhpShaderLUT::Init, _EXTRA=_extra
  compile_opt idl2, hidden
  
  if (~self->IDLgrShader::Init(_EXTRA=_extra, OUTPUT_DATA_TYPE=1)) then $
     return, 0

  ;; LoadShader will be called when NATIVE_RASTER property is
  ;; set. Need to wait until then to discover how many bands/channels
  ;; there are.
  
  ;; Note: It doesn't matter if maxTaxDims supported by the HW 
  ;; is larger than 1024, we only really need 512x512 to store a lut large
  ;; enough for ushort (16-bit) data.
  self.maxTexDims = [1024,1024]
  self.pRedValues = Ptr_New(/ALLOCATE)
  self.pGreenValues = Ptr_New(/ALLOCATE)
  self.pBlueValues = Ptr_New(/ALLOCATE)

  self.pChangeColorFrom = Ptr_New(/ALLOCATE)

  ;; LUT objects to pass to shader
  self.oRedLUT = Obj_New('IDLgrImage', INTERNAL_DATA_TYPE=1, /GREYSCALE)
  self.oGreenLUT = Obj_New('IDLgrImage', INTERNAL_DATA_TYPE=1, /GREYSCALE)
  self.oBlueLUT = Obj_New('IDLgrImage', INTERNAL_DATA_TYPE=1, /GREYSCALE)

  ;; Initial uniform vars
  self->SetProperty, NODATA_COLOR=[0.0, 0.0, 0.0, 0.0], $
                     MASKED_COLOR=[0.0, 0.0, 0.0, 0.0], $
                     OUTSIDEROI_COLOR=[0.0, 0.0, 0.0, 0.0], $
                     CHANGE_COLOR_FROM=[0.0, 0.0, 0.0], $
                     CHANGE_COLOR_TO=[0.0, 0.0, 0.0, 0.0]

  self->SetProperty, _EXTRA=_extra

  return, 1
end

;----------------------------------------------------------------------------
; IDLhpShaderLUT::Cleanup
;
pro IDLhpShaderLUT::Cleanup
  compile_opt idl2, hidden

  Obj_Destroy, self.oRedLUT
  Obj_Destroy, self.oGreenLUT
  Obj_Destroy, self.oBlueLUT
  Ptr_Free, self.pRedValues
  Ptr_Free, self.pGreenValues
  Ptr_Free, self.pBlueValues
  Ptr_Free, self.pChangeColorFrom

  self->IDLgrShader::Cleanup

end

;----------------------------------------------------------------------------
; Get/SetProperty
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; IDLhpShaderLUT::SetProperty
;
pro IDLhpShaderLUT::SetProperty, RED_BINSIZE=redBinSize, $
                                 GREEN_BINSIZE=greenBinSize, $
                                 BLUE_BINSIZE=blueBinSize, $
                                 RED_RANGE=redRange, $
                                 GREEN_RANGE=greenRange, $
                                 BLUE_RANGE=blueRange, $
                                 RED_VALUES=redValues, $
                                 GREEN_VALUES=greenValues, $
                                 BLUE_VALUES=blueValues, $
                                 NODATA_COLOR=nodataColor, $
                                 MASKED_COLOR=maskedColor, $
                                 OUTSIDEROI_COLOR=outsideRoiColor, $
                                 CHANGE_COLOR_ENABLE=changeColorEnable, $
                                 CHANGE_COLOR_FROM=changeColorFrom, $
                                 CHANGE_COLOR_TO=changeColorTo, $
                                 NATIVE_RASTER=nativeRaster, $
                                 _EXTRA=_extra
  compile_opt idl2, hidden

  bUpdateUniformVarsR = 0
  bUpdateUniformVarsG = 0
  bUpdateUniformVarsB = 0

  if (N_Elements(nativeRaster) gt 0) then begin
    self.oRaster = nativeRaster
    self.inputDataType = nativeRaster.data_type
    self.nChannels = nativeRaster.bands
    self->LoadShader
    bUpdateUniformVarsR = 1
    bUpdateUniformVarsG = 1
    bUpdateUniformVarsB = 1
    ;; ChangeColorFrom depends on inputDataType, 
    ;; make sure it gets updated
    self->UpdateChangeColorFrom, *self.pChangeColorFrom
  endif

  if (N_Elements(redValues) gt 0) then begin
    *self.pRedValues = redValues
    bUpdateUniformVarsR = 1
  endif

  if (N_Elements(redRange) gt 0) then begin
    self.redRange = redRange
    bUpdateUniformVarsR = 1
  endif

  if (N_Elements(redBinSize) gt 0) then begin
    self.redBinSize = redBinSize
    bUpdateUniformVarsR = 1
  endif

  if (N_Elements(greenValues) gt 0) then begin
    *self.pGreenValues = greenValues
    bUpdateUniformVarsG = 1
  endif

  if (N_Elements(greenRange) gt 0) then begin
    self.greenRange = greenRange
    bUpdateUniformVarsG = 1
  endif

  if (N_Elements(greenBinSize) gt 0) then begin
    self.greenBinSize = greenBinSize
    bUpdateUniformVarsG = 1
  endif

  if (N_Elements(blueValues) gt 0) then begin
    *self.pBlueValues = blueValues
    bUpdateUniformVarsB = 1
  endif

  if (N_Elements(blueRange) gt 0) then begin
    self.blueRange = blueRange
    bUpdateUniformVarsB = 1
  endif

  if (N_Elements(blueBinSize) gt 0) then begin
    self.blueBinSize = blueBinSize
    bUpdateUniformVarsB = 1
  endif

  if (N_Elements(nodataColor) gt 0) then begin
    fNodataColor = Fltarr(4)
    switch (N_Elements(nodataColor)) of
      1: begin
        fNodataColor[0] = Float(nodataColor) / 255.0
        fNodataColor[1] = Float(nodataColor) / 255.0
        fNodataColor[2] = Float(nodataColor) / 255.0
        fNodataColor[3] = 1.0
        break
      end
      2: begin
        fNodataColor[0] = Float(nodataColor[0]) / 255.0
        fNodataColor[1] = Float(nodataColor[0]) / 255.0
        fNodataColor[2] = Float(nodataColor[0]) / 255.0
        fNodataColor[3] = Float(nodataColor[1]) / 255.0
        break
      end
      3: begin
        fNodataColor[0] = Float(nodataColor[0]) / 255.0
        fNodataColor[1] = Float(nodataColor[1]) / 255.0
        fNodataColor[2] = Float(nodataColor[2]) / 255.0
        fNodataColor[3] = 1.0
        break
      end
      4: begin
        fNoDataColor = Float(nodataColor) / 255.0
        break
      end
    endswitch

    self->IDLgrShader::SetUniformVariable, 'nodataColor', fNodataColor
  endif

  if (N_Elements(maskedColor) gt 0) then begin
    fMaskedColor = Fltarr(4)
    switch (N_Elements(maskedColor)) of
      1: begin
        fMaskedColor[0] = Float(maskedColor) / 255.0
        fMaskedColor[1] = Float(maskedColor) / 255.0
        fMaskedColor[2] = Float(maskedColor) / 255.0
        fMaskedColor[3] = 1.0
        break
      end
      2: begin
        fMaskedColor[0] = Float(maskedColor[0]) / 255.0
        fMaskedColor[1] = Float(maskedColor[0]) / 255.0
        fMaskedColor[2] = Float(maskedColor[0]) / 255.0
        fMaskedColor[3] = Float(maskedColor[1]) / 255.0
        break
      end
      3: begin
        fMaskedColor[0] = Float(maskedColor[0]) / 255.0
        fMaskedColor[1] = Float(maskedColor[1]) / 255.0
        fMaskedColor[2] = Float(maskedColor[2]) / 255.0
        fMaskedColor[3] = 1.0
        break
      end
      4: begin
        fMaskedColor = Float(maskedColor) / 255.0
        break
      end
    endswitch

    self->IDLgrShader::SetUniformVariable, 'maskedColor', fMaskedColor
  endif  
  
    if (N_Elements(outsideRoiColor) gt 0) then begin
    fOutsideRoiColor = Fltarr(4)
    switch (N_Elements(outsideRoiColor)) of
      1: begin
        fOutsideRoiColor[0] = Float(outsideRoiColor) / 255.0
        fOutsideRoiColor[1] = Float(outsideRoiColor) / 255.0
        fOutsideRoiColor[2] = Float(outsideRoiColor) / 255.0
        fOutsideRoiColor[3] = 1.0
        break
      end
      2: begin
        fOutsideRoiColor[0] = Float(outsideRoiColor[0]) / 255.0
        fOutsideRoiColor[1] = Float(outsideRoiColor[0]) / 255.0
        fOutsideRoiColor[2] = Float(outsideRoiColor[0]) / 255.0
        fOutsideRoiColor[3] = Float(outsideRoiColor[1]) / 255.0
        break
      end
      3: begin
        fOutsideRoiColor[0] = Float(outsideRoiColor[0]) / 255.0
        fOutsideRoiColor[1] = Float(outsideRoiColor[1]) / 255.0
        fOutsideRoiColor[2] = Float(outsideRoiColor[2]) / 255.0
        fOutsideRoiColor[3] = 1.0
        break
      end
      4: begin
        fOutsideRoiColor = Float(outsideRoiColor) / 255.0
        break
      end
    endswitch

    self->IDLgrShader::SetUniformVariable, 'outsideRoiColor', fOutsideRoiColor
  endif  

  if (N_Elements(changeColorEnable) gt 0) then begin
    if (changeColorEnable ne self.changeColorEnable) then begin      
      self.changeColorEnable = changeColorEnable
      ;; Need to load a different shader
      self->LoadShader
      bUpdateUniformVarsR = 1
      bUpdateUniformVarsG = 1
      bUpdateUniformVarsB = 1
    endif
  endif    

  if (N_Elements(changeColorFrom) gt 0) then begin
    *self.pChangeColorFrom = changeColorFrom    
    if (self.inputDataType ne 0) then begin
      ;; We know the datatype, update ChangeColorFrom.
      ;; If the DT is unknown, UpdateChangeColorFrom will
      ;; be called when we find out (when NATIVE_RASTER prop is set)
      self->UpdateChangeColorFrom, changeColorFrom      
    endif
  endif

  if (N_Elements(changeColorTo) gt 0) then begin
    fChangeColorTo = Fltarr(4)
    switch (N_Elements(changeColorTo)) of
      1: begin
        fChangeColorTo[0] = Float(changeColorTo) / 255.0
        fChangeColorTo[1] = Float(changeColorTo) / 255.0
        fChangeColorTo[2] = Float(changeColorTo) / 255.0
        fChangeColorTo[3] = 1.0
        break
      end
      2: begin
        fChangeColorTo[0] = Float(changeColorTo[0]) / 255.0
        fChangeColorTo[1] = Float(changeColorTo[0]) / 255.0
        fChangeColorTo[2] = Float(changeColorTo[0]) / 255.0
        fChangeColorTo[3] = Float(changeColorTo[1]) / 255.0
        break
      end
      3: begin
        fChangeColorTo[0] = Float(changeColorTo[0]) / 255.0
        fChangeColorTo[1] = Float(changeColorTo[1]) / 255.0
        fChangeColorTo[2] = Float(changeColorTo[2]) / 255.0
        fChangeColorTo[3] = 1.0
        break
      end
      4: begin
        fChangeColorTo = Float(changeColorTo) / 255.0
        break
      end
    endswitch

    self->IDLgrShader::SetUniformVariable, 'changeColorTo', fChangeColorTo
  endif    

  if (bUpdateUniformVarsR) then begin
    self->UpdateUniformVars, 'lutR', *self.pRedValues, $
                             'minmaxR', self.redRange, $
                             'binSizeR', self.redBinSize, $
                             self.oRedLUT, 'lutTexSizeR'
  endif

  if (bUpdateUniformVarsG) then begin
    self->UpdateUniformVars, 'lutG', *self.pGreenValues, $
                             'minmaxG', self.greenRange, $
                             'binSizeG', self.greenBinSize, $
                             self.oGreenLUT, 'lutTexSizeG'
  endif

  if (bUpdateUniformVarsB) then begin
    self->UpdateUniformVars, 'lutB', *self.pBlueValues, $
                             'minmaxB', self.blueRange, $
                             'binSizeB', self.blueBinSize, $
                             self.oBlueLUT, 'lutTexSizeB'
  endif

  self->IDLgrShader::SetProperty, _EXTRA=_extra
end

;----------------------------------------------------------------------------
; IDLhpShaderLUT::GetProperty
;
pro IDLhpShaderLUT::GetProperty, _REF_EXTRA=_extra
  compile_opt idl2, hidden
  
  self->IDLgrShader::GetProperty, _EXTRA=_extra
end

;----------------------------------------------------------------------------
; IDLhpShaderLUT::LoadShader
;
pro IDLhpShaderLUT::LoadShader
  compile_opt idl2, hidden

  if (self.nChannels eq 0) then begin
    ;; Don't know how many channels we have yet, can't continue.
    ;; LoadShader will be called again when NATIVE_RASTER property is set.
    return
  endif

  vertexProgram = [ $
                  'void main() {', $
                  'gl_TexCoord[0] = gl_MultiTexCoord0;', $
                  'gl_Position = ftransform();', $
                  '}']

  if (self.changeColorEnable) then begin
    if (self.nChannels lt 3) then begin
      ;; Greyscale image, Change Color Enabled
      fragmentProgram = [ $
                        'uniform sampler2D _IDL_ImageTexture;', $
                        'uniform sampler2D lutR;', $
                        'uniform vec2 minmaxR;', $
                        'uniform vec2 lutTexSizeR;', $
                        'uniform float binSizeR;', $
                        'uniform vec4 nodataColor;', $
                        'uniform vec4 maskedColor;', $
                        'uniform vec4 outsideRoiColor;', $
                        'uniform vec3 changeColorFrom;', $
                        'uniform vec4 changeColorTo;', $
                        'void main() {', $
                        'vec4 fragc;', $
                        'vec2 tc;', $
                        'float q, f;', $
                        ;; Sample image
                        'vec4 c = texture2D(_IDL_ImageTexture, gl_TexCoord[0].xy);', $
                        'if (c.a > 0.8)', $
                        '{', $
                        ;; alpha == 1.0, do LUT lookup or ChangeColor.
                        '  float epsilon = 1e-5;', $
                        '  if (abs(c.r - changeColorFrom.r) > epsilon)', $
                        '  {', $
                        ;; Texel does not match ChangeColorFrom, do LUT lookup.
                        ;; Subtract min, divide by binsize (reciprocal) and clamp to 0.0,max 
                        '    float ri = clamp((c.r - minmaxR[0]) * binSizeR, 0.0, minmaxR[1]);', $
                        ;; Convert index to tex coord to lookup 2D LUT
                        '    f = ri * lutTexSizeR[0];', $
                        '    q = floor(f);', $
                        '    tc.s = f - q;', $
                        '    tc.t = q * lutTexSizeR[1];', $
                        ;; Do LUT lookup
                        '    fragc = texture2D(lutR, tc);', $ 
                        '    fragc.a = 1.0;', $
                        '    gl_FragColor = fragc;', $
                        '  }', $
                        '  else', $
                        '  {', $
                        ;; Texel matches ChangeColorFrom, change it
                        '    gl_FragColor = changeColorTo;', $
                        '  }', $
                        '}', $
                        'else if (c.a > 0.6)', $
                        '{', $
                        ;; alpha == 0.75, pixel is masked
                        '  gl_FragColor = maskedColor;', $
                        '}', $
                        'else if (c.a > 0.3)', $
                        '{', $
                        ;; alpha == 0.5, pixel is OutsideROI
                        '  gl_FragColor = outsideROIColor;', $
                        '}', $
                        'else', $
                        '{', $
                        ;; alpha == 0.25, pixel is NoData
                        '  gl_FragColor = nodataColor;', $
                        '}', $
                        '}']
    endif else begin
      ;; RGB(A) image, Change Color Enabled      
      fragmentProgram = [ $
                        'uniform sampler2D _IDL_ImageTexture;', $
                        'uniform sampler2D lutR;', $
                        'uniform sampler2D lutG;', $
                        'uniform sampler2D lutB;', $
                        'uniform vec2 minmaxR;', $
                        'uniform vec2 minmaxG;', $
                        'uniform vec2 minmaxB;', $
                        'uniform vec2 lutTexSizeR;', $
                        'uniform vec2 lutTexSizeG;',$ 
                        'uniform vec2 lutTexSizeB;', $
                        'uniform float binSizeR;', $
                        'uniform float binSizeG;', $
                        'uniform float binSizeB;', $
                        'uniform vec4 nodataColor;', $
                        'uniform vec4 maskedColor;', $
                        'uniform vec4 outsideROIColor;', $
                        'uniform vec3 changeColorFrom;', $
                        'uniform vec4 changeColorTo;', $
                        'void main() {', $
                        'vec4 fragc;', $
                        'vec2 tc;', $
                        'float q, f;', $
                        ;; Sample image
                        'vec4 c = texture2D(_IDL_ImageTexture, gl_TexCoord[0].xy);', $
                        'if (c.a > 0.8)', $
                        '{', $
                        ;; alpha == 1.0, do LUT lookup or ChangeColor.
                        '  vec3 epsilon = vec3(1e-5, 1e-5, 1e-5);', $
                        '  if (!all(lessThan(abs(c.rgb - changeColorFrom), epsilon)))', $
                        '  {', $
                        ;; Texel does not match ChangeColorFrom, do LUT lookup.
                        ;; Subtract min, divide by binsize (reciprocal) and clamp to 0.0,max 
                        '    float ri = clamp((c.r - minmaxR[0]) * binSizeR, 0.0, minmaxR[1]);', $
                        '    float gi = clamp((c.g - minmaxG[0]) * binSizeG, 0.0, minmaxG[1]);', $
                        '    float bi = clamp((c.b - minmaxB[0]) * binSizeB, 0.0, minmaxB[1]);', $
                        ;; Convert index to tex coord to lookup 2D LUT
                        ;; Red
                        '    f = ri * lutTexSizeR[0];', $
                        '    q = floor(f);', $
                        '    tc.s = f - q;', $
                        '    tc.t = q * lutTexSizeR[1];', $
                        ;; Do LUT lookup
                        '    fragc.r = texture2D(lutR, tc).r;', $
                        ;; Green
                        '    f = gi * lutTexSizeG[0];', $
                        '    q = floor(f);', $
                        '    tc.s = f - q;', $
                        '    tc.t = q * lutTexSizeG[1];', $
                        '    fragc.g = texture2D(lutG, tc).r;', $
                        ;; Blue
                        '    f = bi * lutTexSizeB[0];', $
                        '    q = floor(f);', $
                        '    tc.s = f - q;', $
                        '    tc.t = q * lutTexSizeB[1];', $
                        '    fragc.b = texture2D(lutB, tc).r;', $
                        '    fragc.a = 1.0;', $
                        '    gl_FragColor = fragc;', $
                        '  }', $
                        '  else', $
                        '  {', $
                        ;; Texel matches ChangeColorFrom, change it
                        '    gl_FragColor = changeColorTo;', $
                        '  }', $
                        '}', $
                        'else if (c.a > 0.6)', $
                        '{', $
                        ;; alpha == 0.75, pixel is masked
                        '  gl_FragColor = maskedColor;', $
                        '}', $
                        'else if (c.a > 0.3)', $
                        '{', $
                        ;; alpha == 0.5, pixel is OutsideROI
                        '  gl_FragColor = outsideRoiColor;', $
                        '}', $
                        'else', $
                        '{', $
                        ;; alpha == 0.25, pixel is NoData
                        '  gl_FragColor = nodataColor;', $
                        '}', $
                        '}']
    endelse
  endif else begin    
    if (self.nChannels lt 3) then begin
      ;; Greyscale image, Change Color disabled
      fragmentProgram = [ $
                        'uniform sampler2D _IDL_ImageTexture;', $
                        'uniform sampler2D lutR;', $
                        'uniform vec2 minmaxR;', $
                        'uniform vec2 lutTexSizeR;', $
                        'uniform float binSizeR;', $
                        'uniform vec4 nodataColor;', $
                        'uniform vec4 maskedColor;', $
                        'uniform vec4 outsideRoiColor;', $
                        'void main() {', $
                        'vec4 fragc;', $
                        'vec2 tc;', $
                        'float q, f;', $
                        ;; Sample image
                        'vec4 c = texture2D(_IDL_ImageTexture, gl_TexCoord[0].xy);', $
                        'if (c.a > 0.8)', $
                        '{', $
                        ;; alpha == 1.0, do LUT lookup.
                        ;; Subtract min, divide by binsize (reciprocal) and clamp to 0.0,max 
                        '  float ri = clamp((c.r - minmaxR[0]) * binSizeR, 0.0, minmaxR[1]);', $
                        ;; Convert index to tex coord to lookup 2D LUT
                        '  f = ri * lutTexSizeR[0];', $
                        '  q = floor(f);', $
                        '  tc.s = f - q;', $
                        '  tc.t = q * lutTexSizeR[1];', $
                        ;; Do LUT lookup
                        '  fragc = texture2D(lutR, tc);', $ 
                        '  fragc.a = 1.0;', $
                        '  gl_FragColor = fragc;', $
                        '}', $
                        'else if (c.a > 0.6)', $
                        '{', $
                        ;; alpha == 0.75, pixel is masked
                        '  gl_FragColor = maskedColor;', $
                        '}', $
                        'else if (c.a > 0.3)', $
                        '{', $
                        ;; alpha == 0.5, pixel is outsideROI
                        '  gl_FragColor = outsideRoiColor;', $
                        '}', $
                        'else', $
                        '{', $
                        ;; alpha == 0.25, pixel is NoData
                        '  gl_FragColor = nodataColor;', $
                        '}', $
                        '}']
    endif else begin
      ;; RGB(A) image, Change Color disabled.
      fragmentProgram = [ $
                        'uniform sampler2D _IDL_ImageTexture;', $
                        'uniform sampler2D lutR;', $
                        'uniform sampler2D lutG;', $
                        'uniform sampler2D lutB;', $
                        'uniform vec2 minmaxR;', $
                        'uniform vec2 minmaxG;', $
                        'uniform vec2 minmaxB;', $
                        'uniform vec2 lutTexSizeR;', $
                        'uniform vec2 lutTexSizeG;',$ 
                        'uniform vec2 lutTexSizeB;', $
                        'uniform float binSizeR;', $
                        'uniform float binSizeG;', $
                        'uniform float binSizeB;', $
                        'uniform vec4 nodataColor;', $
                        'uniform vec4 maskedColor;', $
                        'uniform vec4 outsideRoiColor;', $
                        'void main() {', $
                        'vec4 fragc;', $
                        'vec2 tc;', $
                        'float q, f;', $
                        ;; Sample image
                        'vec4 c = texture2D(_IDL_ImageTexture, gl_TexCoord[0].xy);', $
                        'if (c.a > 0.8)', $
                        '{', $
                        ;; alpha == 1.0, do LUT lookup.
                        ;; Subtract min, divide by binsize (reciprocal) and clamp to 0.0,max 
                        '  float ri = clamp((c.r - minmaxR[0]) * binSizeR, 0.0, minmaxR[1]);', $
                        '  float gi = clamp((c.g - minmaxG[0]) * binSizeG, 0.0, minmaxG[1]);', $
                        '  float bi = clamp((c.b - minmaxB[0]) * binSizeB, 0.0, minmaxB[1]);', $
                        ;; Convert index to tex coord to lookup 2D LUT
                        ;; Red
                        '  f = ri * lutTexSizeR[0];', $
                        '  q = floor(f);', $
                        '  tc.s = f - q;', $
                        '  tc.t = q * lutTexSizeR[1];', $
                        ;; Do LUT lookup
                        '  fragc.r = texture2D(lutR, tc).r;', $
                        ;; Green
                        '  f = gi * lutTexSizeG[0];', $
                        '  q = floor(f);', $
                        '  tc.s = f - q;', $
                        '  tc.t = q * lutTexSizeG[1];', $
                        '  fragc.g = texture2D(lutG, tc).r;', $
                        ;; Blue
                        '  f = bi * lutTexSizeB[0];', $
                        '  q = floor(f);', $
                        '  tc.s = f - q;', $
                        '  tc.t = q * lutTexSizeB[1];', $
                        '  fragc.b = texture2D(lutB, tc).r;', $
                        '  fragc.a = 1.0;', $
                        '  gl_FragColor = fragc;', $
                        '}', $
                        'else if (c.a > 0.6)', $
                        '{', $
                        ;; alpha == 0.75, pixel is masked
                        '  gl_FragColor = maskedColor;', $
                        '}', $
                        'else if (c.a > 0.3)', $
                        '{', $
                        ;; alpha == 0.5, pixel is outsideROI
                        '  gl_FragColor = outsideRoiColor;', $
                        '}', $
                        'else', $
                        '{', $
                        ;; alpha == 0.25, pixel is NoData
                        '  gl_FragColor = nodataColor;', $
                        '}', $
                        '}']
    endelse
  endelse

  self->IDLgrShader::SetProperty, $
     VERTEX_PROGRAM_STRING=STRJOIN(vertexProgram, STRING(10B)), $
     FRAGMENT_PROGRAM_STRING=STRJOIN(fragmentProgram, STRING(10B))
end

pro IDLhpShaderLUT::UpdateChangeColorFrom, changeColorFrom
  compile_opt idl2, hidden

  fChangeColorFrom = Fltarr(3)
  switch (N_Elements(changeColorFrom)) of
    1: begin
      fChangeColorFrom[0] = Float(changeColorFrom)
      fChangeColorFrom[1] = Float(changeColorFrom) 
      fChangeColorFrom[2] = Float(changeColorFrom) 
      break
    end
    2: begin
      fChangeColorFrom[0] = Float(changeColorFrom[0]) 
      fChangeColorFrom[1] = Float(changeColorFrom[0]) 
      fChangeColorFrom[2] = Float(changeColorFrom[0])
      break
    end
    3: begin
      fChangeColorFrom[0] = Float(changeColorFrom[0]) 
      fChangeColorFrom[1] = Float(changeColorFrom[1]) 
      fChangeColorFrom[2] = Float(changeColorFrom[2])
      break
    end
    4: begin
      fChangeColorFrom[0] = Float(changeColorFrom[0]) 
      fChangeColorFrom[1] = Float(changeColorFrom[1]) 
      fChangeColorFrom[2] = Float(changeColorFrom[2])
      break
    end
  endswitch

  switch (self.inputDataType) of
    1: begin
      fChangeColorFrom /= 255.0
      break
    end
    2: begin
      fChangeColorFrom = (fChangeColorFrom * 2 + 1) / 65535.0
      break
    end
    12: begin
      fChangeColorFrom /= 65535.0
      break
    end
  endswitch

  self->IDLgrShader::SetUniformVariable, 'changeColorFrom', fChangeColorFrom
end

pro IDLhpShaderLUT::UpdateUniformVars, lutName, lutValues, $
                                       rangeName, range, $ 
                                       binSizeName, binSize, $
                                       oLUT, texSizeName

  len = N_Elements(lutValues)
  minValue = range[0]
  maxValue = range[1]
  
  if (len ge 1 && binSize gt 0.0) then begin
    if (len gt self.maxTexDims[0]) then begin
      ;; Because texture maps are limited in size,
      ;; e.g. 1024 we can't use a 1D texture map in case
      ;; the LUT is larger than 1024. Instead convert LUT array to 2D
      ;; and index appropriately in the shader.
      h = ceil(float(len) / self.maxTexDims[0])
      ;; Round up to nearest power of 2
      lutTexH = 2
      while (lutTexH lt h) do lutTexH *= 2
      lutTexW = self.maxTexDims[0]            
      len2 = lutTexW * lutTexH
    endif else begin
      len2 = 2
      ;; Round up to nearest power of 2
      while (len2 lt len) do len2 *= 2
      lutTexH = 2
      lutTexW = len2 / lutTexH
    endelse
    
    lut = BytArr(len2, /NOZERO)
    if (len gt 1) then $
       lut[0:len-1] = lutValues
    
    ;; If we have floating point image data we can sometimes read the
    ;; value past the end of the LUT due to rounding. Replicate the
    ;; last LUT value in case this happens.
    if (len ne len2) then $
       lut[len] = lut[len-1]
    
    ;; Reform to 2D array
    lut = Reform(lut, lutTexW, lutTexH, /OVERWRITE)
    
    dataScale = 1.0
    case self.inputDataType of
      1: dataScale = 256.0
      2: dataScale = 32768.0
      4: dataScale = 1.0
      12: dataScale = 65536.0
      else: begin
        ;; Datatype that is not supported by shaders
        ;; Default to 1.0. There's no problem as the 
        ;; shader won't actually be used.
        dataScale = 1.0
      end
    endcase
    
    oLUT->SetProperty, DATA=lut
    self->IDLgrShader::SetUniformVariable, lutName, oLUT
    ;; Send reciprocals so GPU can do a * instead of a /
    self->IDLgrShader::SetUniformVariable, texSizeName, $
                                           [dataScale / lutTexW, $
                                            1.0 / lutTexH]
    self->IDLgrShader::SetUniformVariable, rangeName, $
                                           [minValue / dataScale, $
                                            Float(len-1) / dataScale]

    ;; Send reciprocal so GPU can do a * instead of a /
    self->IDLgrShader::SetUniformVariable, binSizeName, 1.0 / binSize

  endif
end


;----------------------------------------------------------------------------
; IDLhpShaderLUT::Filter
;
; Purpose:
;
;   Currently this is not used, if no HW support then zoom uses the
;   existing raster pipeline processing mechanism rather than the
;   Shader SW fallback. 
;
; Arguments:
;   Image: image data to be filtered
;
function IDLhpShaderLUT::Filter, Image

  return, Image

end

;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; IDLhpShaderLUT__Define
;
pro IDLhpShaderLUT__Define
  compile_opt idl2, hidden

  void = { IDLhpShaderLUT, $
           inherits IDLgrShader, $
           oRedLUT: OBJ_NEW(), $
           oGreenLUT: OBJ_NEW(), $
           oBlueLUT: OBJ_NEW(), $
           maxTexDims: [0,0], $
           redBinSize: 0.0d, $
           greenBinSize: 0.0d, $
           blueBinSize: 0.0d, $
           redRange: [0.0d, 0.0d], $
           greenRange: [0.0d, 0.0d], $
           blueRange: [0.0d, 0.0d], $
           pRedValues: Ptr_New(), $
           pGreenValues: Ptr_New(), $
           pBlueValues: Ptr_New(), $
           oRaster: Obj_New(), $
           nChannels: 0UL, $
           inputDataType: 0UL, $
           changeColorEnable: 0, $
           pChangeColorFrom: Ptr_New() $
         }
end
