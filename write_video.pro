; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/write_video.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
; WRITE_VIDEO
;
;-

;----------------------------------------------------------------------------
;+
; Helper object
;
; PURPOSE:
;      Creates an object for a video read handle object
;-
;----------------------------------------------------------------------------
function writevideohandle::Init, file, _EXTRA=_extra
  compile_opt idl2, hidden
  on_error, 2
  
  catch, err
  if (err ne 0) then begin
    catch, /CANCEL
    return, 0
  endif
  
  if (SIZE(file, /TNAME) eq 'STRING') then begin
    self.oVid = OBJ_NEW('IDLffVideoWrite', file[0], _EXTRA=_extra)
  endif
  
  if (~OBJ_VALID(self.oVid)) then return, 0
  
  self.vidStreamInd = -1 ; no current stream set
  self.audStreamInd = -1 ; no current stream set
  
  return, 1
  
end

;-----------------------------------------------------------------------------
; Clean up
;
pro writevideohandle::Cleanup, _EXTRA=_extra
  compile_opt idl2, hidden
  on_error, 2
  
  OBJ_DESTROY, self.oVid
  
end

;----------------------------------------------------------------------------
; Add video stream
;
pro writevideohandle::AddVideoStream, width, height, fps, $
                      BIT_RATE=bitRate, CODEC=codec
  compile_opt idl2, hidden
  on_error, 2
  
  self.vidStreamInd = self.oVid->AddVideoStream(width, height, fps, $
                                                BIT_RATE=bitRate, CODEC=codec)
                                    
end

;----------------------------------------------------------------------------
; Add audio stream
;
pro writevideohandle::AddAudioStream, rate, $
                      BIT_RATE=bitRate, CHANNELS=channels, CODEC=codec
  compile_opt idl2, hidden
  on_error, 2

  self.audStreamInd = self.oVid->AddAudioStream(rate, BIT_RATE=bitRate, $
                                                CHANNELS=channels, CODEC=codec)
                                                   
end

;----------------------------------------------------------------------------
; Has data been written yet to the file?
;
function writevideohandle::HasWritten
  compile_opt idl2, hidden
  on_error, 2

  return, (self.vidStreamInd ne -1) || (self.audStreamInd ne -1)
  
end

;----------------------------------------------------------------------------
; Write metadata
;
pro writevideohandle::SetMetadata, metadata
  compile_opt idl2, hidden
  on_error, 2
  
  if (SIZE(metadata, /TNAME) ne 'STRING') then return
  if ((SIZE(metadata, /N_DIMENSIONS) ne 2) && $
      (N_ELEMENTS(metadata) ne 2)) then return
  dims = SIZE(metadata, /DIMENSIONS) 
  if (dims[0] ne 2) then return
  
  for i=0,dims[1]-1 do begin
    self.oVid->SetMetadata, metadata[0,i], metadata[1,i]
  endfor
  
end

;----------------------------------------------------------------------------
; Do the work of reading the data
;
function writevideohandle::Write, data, type
  compile_opt idl2, hidden
  on_error, 2

  catch, err
  if (err ne 0) then begin
    catch, /CANCEL
    return, 0
  endif

  streamIndex = -1l
  nPackets = 1l
  
  ndims = SIZE(data, /N_DIMENSIONS)
  dims = SIZE(data, /DIMENSIONS)
  if (ndims eq 4) then begin
    nPackets = dims[3]
  endif
  
  ; Get stream
  typeVideo = 1
  typeAudio = 2
  case type of
    typeVideo : streamIndex = self.vidStreamInd
    typeAudio : streamIndex = self.audStreamInd
    else : return, 0 ; invalid type
  endcase
  
  for i=0,nPackets-1 do begin
    !NULL = self.oVid->Put(streamIndex, data[*,*,*,i])
  endfor

  return, 1
  
end

;-----------------------------------------------------------------------------
; Object definition
;
pro writevideohandle__define
  compile_opt idl2, hidden
  
  struct = {writevideohandle, $
            oVid: OBJ_NEW(), $
            vidStreamInd: 0l, $
            audStreamInd: 0l $
           }
  
end

;----------------------------------------------------------------------------
;+
; Primary routine
;
; NAME:
;      Read_Video
;-
;----------------------------------------------------------------------------
pro write_video, file, data, $
                 HANDLE=handleIn, CLOSE=closeIn, METADATA=metadataIn, $
                 BIT_RATE=bitrateIn, FORMAT=formatIn, $
                 AUDIO_RATE=audRateIn, AUDIO_CHANNELS=nChannelsIn, $
                 VIDEO_FPS=vidFpsIn, VIDEO_DIMENSIONS=vidDimsIn, $
                 AUDIO_CODEC=audCodecIn, VIDEO_CODEC=vidCodecIn
                 
  compile_opt idl2, hidden
  on_error, 2

  ; Check for valid handle
  if (ISA(handleIn, 'writevideohandle')) then begin
    hasHandle = 1b
    oVid = handleIn
  endif else begin
    if ((N_ELEMENTS(file) ne 1) || (SIZE(file, /TNAME) ne 'STRING')) then begin
      message, 'Invalid file specification'
      return
    endif
    if ((N_ELEMENTS(file) ne 1) || (SIZE(file, /TNAME) ne 'STRING')) then begin
      message, 'Invalid file specification'
      return
    endif
    hasHandle = 0b
    catch, err
    if (err eq 0) then begin
      oVid = OBJ_NEW('writevideohandle', file, FORMAT=formatIn)
    endif else begin
      catch, /cancel
      message, 'Unable to create video file: '+file
      return
    endelse
    catch, /cancel
  endelse

  ; Determine type
  typeVideo = 1
  typeAudio = 2
  
  type = 0
  ndims = SIZE(data, /N_DIMENSIONS)
  dims = SIZE(data, /DIMENSIONS)
  if (ndims eq 1) then begin
    type = typeAudio
  endif
  if ((ndims eq 2) && (dims[0] eq 2)) then begin
    type = typeAudio
  endif
  if ((ndims eq 3) && (dims[0] eq 3)) then begin
    type = typeVideo
  endif
  if ((ndims eq 4) && (dims[0] eq 3)) then begin
    type = typeVideo
  endif
  if (type eq 0) then begin
    ; Data is required if not retrieving handle, closing or setting metadata
    if (~KEYWORD_SET(closeIn) && (N_ELEMENTS(metadataIn) eq 0) && $
        ~ARG_PRESENT(handleIn)) then begin
      message, 'Invalid data'
      return
    endif
  endif
  
  case type of
    typeVideo : begin
      if (N_ELEMENTS(vidDimsIn) eq 0) then begin
        vidDimsIn = [dims[1], dims[2]]
      endif
    end
    typeAudio : begin
      if (N_ELEMENTS(nChannelsIn) eq 0) then begin
        nChannelsIn = ndims
      endif
    end
    else :
  endcase

  ; Check for inputs
  if ((type eq typeVideo) && (~hasHandle) && $
      (N_ELEMENTS(vidFpsIn) ne 1)) then begin
    message, 'VIDEO_FPS must be set before or during the first write to a file'
    return
  endif
  if ((type eq typeAudio) && (~hasHandle) && $
      (N_ELEMENTS(audRateIn) ne 1)) then begin
    message, 'AUDIO_RATE must be set before or during the first write to a file'
    return
  endif

  ; Write metadata
  if (N_ELEMENTS(metadataIn) ne 0) then begin
    if (oVid->HasWritten()) then begin
      message, 'Metadata must be written before any video or audio data'
    endif
    oVid->SetMetadata, metadataIn
  endif

  ; Set up streams, if needed
  if (~oVid->HasWritten()) then begin
    if ((N_ELEMENTS(vidFpsIn) ne 0) && (N_ELEMENTS(vidDimsIn) eq 2)) then begin
      oVid->AddVideoStream, vidDimsIn[0], vidDimsIn[1], vidFpsIn, $
        BIT_RATE=bitrateIn, CODEC=vidCodecIn
    endif
    if (N_ELEMENTS(audRateIn) ne 0) then begin
      if (N_ELEMENTS(audCodecIn) eq 0) then begin
        audCodecIn = 'mp2'
      endif
      oVid->AddAudioStream, audRateIn, BIT_RATE=bitrateIn, $
        CHANNELS=nChannelsIn, CODEC=audCodecIn
    endif
  endif
  
  ; Write data
  if (type ne 0) then begin
    if (~oVid->Write(data, type)) then begin
      message, 'Unable to write to file'
      return
    endif
  endif

  ; Close video object if not needed anymore                     
  if (KEYWORD_SET(closeIn)) then begin
    OBJ_DESTROY, oVid
  endif
  if (~ARG_PRESENT(handleIn) && (hasHandle eq 0)) then begin
    OBJ_DESTROY, oVid
  endif
  
  ; Return handle if requested
  if ((hasHandle eq 0) && ARG_PRESENT(handleIn) && OBJ_VALID(oVid)) then begin
    handleIn = oVid
  endif

end
