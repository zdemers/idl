; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/h5_putdata.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; NAME:
;    H5_PUTDATA
;
; PURPOSE:
;    An easy one step way to write a data set to an HDF5 file
;
; MODIFICATION HISTORY:
;     Written by:   AGEH, 7/2013
;-

; Helper routine to create a new HDF5 file using H5_CREATE
pro _h5_putdata_newfile, fileIn, id, data
  compile_opt idl2, hidden
  
  catch, err
  if (err ne 0) then begin
    catch, /cancel
    message, 'Unable to create file: '+fileIn
  endif
  
  groups = STRSPLIT(id, '/', /EXTRACT)
  isTop = STRMID(id, 0, 1) eq '/'
  if (isTop) then $
    groups = ['/',groups]
  nGroups = N_ELEMENTS(groups)-1
  
  struct = CREATE_STRUCT('_NAME', groups[-1], '_TYPE', 'dataset', $
                         '_DATA', data)
  for i=nGroups-1,0,-1 do begin
    struct = CREATE_STRUCT('_NAME', groups[i], '_TYPE', 'group', $
                           'STRUCT' , struct)  
  endfor
  
  H5_CREATE, fileIn, struct
  
end
  
;-------------------------------------------
pro h5_putdata, fileIn, id, data
  compile_opt idl2, hidden
  on_error, 2

  if (N_PARAMS() ne 3) then begin
    message, 'FILE, ID and DATA must all be supplied'
  endif
  if (N_ELEMENTS(fileIn) ne 1) then begin
    message, 'FILE must be one element'
  endif
  if (N_ELEMENTS(id) ne 1) then begin
    message, 'ID must be one element'
  endif
  
  currentlyOpen = 0b
  case SIZE(fileIn, /TNAME) of
    'LONG' : begin
      catch, err
      if (err ne 0) then begin
        catch, /cancel
        message, 'Invalid HDF5 file/group ID: '+STRTRIM(fileIn,2)
      endif
      !NULL = H5G_GET_OBJINFO(fileIn, '/')
      fid = fileIn
      currentlyOpen = 1b
      catch, /cancel
    end
    'STRING' : begin
      ; Does file exist?
      str = FILE_INFO(fileIn)
      if (str.exists) then begin
        if (~H5F_IS_HDF5(fileIn)) then begin
          message, 'File is not a valid HDF5 file: '+fileIn
        endif
        if (~str.write) then begin
          message, 'File does not have write permissions: '+fileIn
        endif
        fid = H5F_OPEN(fileIn, /WRITE)
      endif else begin
        catch, err
        if (err ne 0) then begin
          catch, /cancel
          message, 'Unable to create file: '+fileIn
        endif
        _h5_putdata_newfile, fileIn, id, data
        return
      endelse
    end
    else : begin
      message, 'Invalid file specification: '+STRING(fileIn)
    end
  endcase
  
  catch, err
  if (err ne 0) then begin
    catch, /cancel
    if (~currentlyOpen) then begin
      H5F_CLOSE, fid
    endif
    message, 'Unable to create group'
  endif
  
  groups = STRSPLIT(id, '/', /EXTRACT)
  nGroups = N_ELEMENTS(groups)-1
  if (nGroups eq 0) then begin
    gid = fid
  endif
  
  ; Check for or create groups
  gid = fid
  for i=0,nGroups-1 do begin
    catch, err
    if (err eq 0) then begin
      gid = H5G_OPEN(gid, groups[i])
    endif else begin
      catch, /cancel
      gid = H5G_CREATE(gid, groups[i])
    endelse
  endfor
  
  catch, err
  if (err ne 0) then begin
    catch, /cancel
    if (~currentlyOpen) then begin
      H5F_CLOSE, fid
    endif
    message, 'Unable to write dataset to file'
  endif
  
  ; Add dataset to group
  dtid = H5T_IDL_CREATE(data)
  if (N_ELEMENTS(data) eq 1) && ~SIZE(data, /N_DIMENSIONS) then begin
    dsid = H5S_CREATE_SCALAR()
  endif else begin
    dsid = H5S_CREATE_SIMPLE(SIZE(data, /DIMENSIONS) > 1)
  endelse
  datasetname = groups[-1]
  ; Does the dataset exist?
  exists = 0b
  nObjs = H5G_GET_NUM_OBJS(gid)
  for i=0,nObjs-1 do begin
    if (H5G_GET_MEMBER_NAME(gid, '/', i) eq datasetname) then begin
      exists = 1b
      break
    endif
  endfor
  if (~exists) then begin
    ; Create the dataset
    did = H5D_CREATE(gid, datasetname, dtid, dsid)
  endif else begin
    ; Open the dataset
    did = H5D_OPEN(gid, datasetname)
  endelse
  ; Write the data to the dataset
  H5D_WRITE, did, data
  ; Close the identifiers
  H5S_CLOSE, dsid
  H5T_CLOSE, dtid
  H5D_CLOSE, did
  
  ; Close the file
  if (~currentlyOpen) then begin
    H5F_CLOSE, fid
  endif
  
end