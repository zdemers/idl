; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/lambertw.pro#2 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;-----------------------------------------------------------------------------
; NAME:
;  LambertW
;
; PURPOSE:
;  Returns W(z) given z and k from the function: W(z)*EXP(W(z))=z.
;
; CATEGORY:
;  Math.
;
; CALLING SEQUENCE:
;  result = LAMBERTW(z[, k, /LOWER_BRANCH])
;
; INPUTS:
;  z: An array of values for which W(z) will be determined.
;  k: An array of branches of the Lambert W function to be used.
;
; KEYWORDS:
;  /LOWER_BRANCH - This keyword calculates the value of the W(-1) branch.
;  It only works for values between -1/e and 0.
;
; OUTPUTS:
;  The value of the Lambert W function for a given z and k.
;  If k is used, then the output will be complex.
;  If a vector of z or k or both is used, then the output will be a vector of complex values.
;
; SIDE EFFECTS:
;  None.
;
; EXAMPLE CALLING SEQUENCE:
;  result = LAMBERTW(6, 4)
;  result = LAMBERTW(0.2, /LOWER_BRANCH)
;  result = LAMBERTW([6,5,8,6,5,65,4], 4)
;
; REFERENCES:
; http://en.wikipedia.org/wiki/Lambert_W_function
; http://mathworld.wolfram.com/LambertW-Function.html
; http://www.had2know.com/academics/lambert-w-function-calculator.html
; http://193.49.146.171/~chapeau/papers/lambertw.pdf
; http://www.apmaths.uwo.ca/~djeffrey/Offprints/wbranch.pdf
; http://functions.wolfram.com/01.32.06.0001.02
;
; MODIFICATION HISTORY:
;  Jul 2013 - Peter Kneusel - Initial Version
;-----------------------------------------------------------------------------

function LambertW, z, kIn, LOWER_BRANCH=lowerBranchIn

  compile_opt idl2, hidden

  ON_ERROR, 2
  
  if (N_PARAMS() lt 1) then $
    MESSAGE, 'Incorrect number of arguments.'
  
  if (ISA(kIn)) then k = kIn

  lowerBranch = KEYWORD_SET(lowerBranchIn)
  if (lowerBranch) then k = -1

  isComplex = SIZE(z, /TYPE) eq 6 || SIZE(z, /TYPE) eq 9

  if (lowerBranch && isComplex) then $
    MESSAGE, 'LOWER_BRANCH can not be used with complex input.'

  if (lowerBranch && ISA(kIn)) then $
    MESSAGE, 'LOWER_BRANCH can not be used with branch K value.'
    
  ; ----------- Newton's Method

  ;--------------- Guess
  if (ISA(k)) then begin
    L1 = ALOG(DCOMPLEX(z)) + 2*!DPI*LONG(k)*!CONST.I
    L2 = ALOG(L1)
  endif else begin
    L1 = ALOG(DCOMPLEX(z))
    L2 = ALOG(L1)
  endelse
  Wj = L1 - L2 + L2/L1 + L2*(L2 - 2)/(2*L1^2) + L2*(6 - 9*L2 + 2*L2^2)/(6*L1^3)
;    print, L1 - L2, L1 - L2 + L2/L1, L1 - L2 + L2/L1 + L2*(L2 - 2)/(2*L1^2), Wj

  ; The expansion will fail near zero.
  if (ISA(k)) then begin
    smallZ = WHERE(ABS(z) lt 2 and LONG(k) eq 0, /NULL)
  endif else begin
    smallZ = WHERE(ABS(z) lt 2, /NULL)
  endelse
  if (ISA(smallZ)) then $
    Wj[smallZ] = DCOMPLEX(0.5d, (IMAGINARY(z[smallZ]) ge 0) - 0.5d)

  ; Use Halley's method to approximate
  ;                      Wj exp(Wj) - z
  ; Wj+1 = Wj - ------------------------------------------------
  ;             exp(Wj)(Wj+1) - (Wj+2)(Wj exp(Wj) - z)/(2Wj + 2)
  ;
  for j=1,15 do begin
    expwj = exp(wj)
    p1 = wj*expwj - z
    wj1 = wj + 1
    p2 = expwj*wj1 - 0.5*(wj + 2)*p1/wj1
    Wj -= (p1/p2)
  endfor
  
  ; Special values.
  if (ISA(k)) then begin
    isNeg1 = WHERE(z eq -EXP(-1d) and (LONG(k) eq 0 or LONG(k) eq -1), /NULL)
    Wj[isNeg1] = -1
    Wj[WHERE(z eq 0 and LONG(k) eq 0, /NULL)] = 0
    Wj[WHERE(z eq 0 and LONG(k) ne 0, /NULL)] = -!VALUES.d_infinity
  endif else begin
    Wj[WHERE(z eq -EXP(-1d), /NULL)] = -1
    Wj[WHERE(z eq 0, /NULL)] = 0
  endelse

  ; For non-complex input replace out-of-bounds values with NaN's.
  if (~isComplex) then begin
    if (~ISA(k) || lowerBranch) then begin
      Wj = DOUBLE(Wj)
      Wj[WHERE(z lt -EXP(-1d), /NULL)] = !VALUES.d_nan
    endif
    if (lowerBranch) then begin
      Wj = DOUBLE(Wj)
      Wj[WHERE(z gt 0, /NULL)] = !VALUES.d_nan
    endif
  endif

  void = CHECK_MATH()
  return,wj  
end

