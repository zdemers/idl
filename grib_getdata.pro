; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/grib_getdata.pro#1 $
;
; Copyright (c) 2010, Research Systems, Inc.  All rights reserved.
;       Unauthorized reproduction prohibited.
;+
; :Description:
;    Gets the value of the associated record/key from the grib file
;
; :Params:
;    file
;       Input GRIB file
;    
;    recordin
;       Record desired
;    
; -

;-------------------------------------------------------------------------------
function grib_getdata, filename, record
  compile_opt idl2
  on_error, 2
  
  ; Sanity
  if (filename eq '' || ~isa(record)) then message,'Invalid input parameters.'
  if (record le 0) then message,'Invalid record number.'
  
  f = grib_open(filename)
  h = grib_new_from_file(f)
  result = orderedHash()
  record_start=1
  
  while (record_start ne record) && (h ne !null) do begin
    grib_release, h
    h = grib_new_from_file(f)
    record_start++
  endwhile
  
  if (h ne !null) then begin
    kiter = grib_keys_iterator_new(h)
    
    catch, ierr
    if (ierr ne 0) then begin
      if strcmp(!error_state.msg,'GRIB Error: Invalid type',/fold_case) then begin
        result.Set,key
        message,/reset
      endif else begin
        catch,/cancel
        message, !error_state.msg
      endelse
    endif
    
    while grib_keys_iterator_next(kiter) do begin
      key = grib_keys_iterator_get_name(kiter)
      if (grib_get_size(h, key) gt 1) then begin
        result.Set,key,grib_get_array(h, key)
      endif else begin
        result.Set,key,grib_get(h, key)
      endelse
    endwhile
    
    grib_keys_iterator_delete, kiter
    grib_release, h
    h = grib_new_from_file(f)
  endif
  
  grib_close, f
  return, result
end