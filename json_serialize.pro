; $Id: //depot/InDevelopment/scrums/ENVI_Yukon/idl/idldir/lib/idl_base64.pro#3 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;----------------------------------------------------------------------------
;+
; :Description:
;    The JSON_SERIALIZE function takes an IDL LIST or HASH variable and
;    serializes the variable as a JSON string.
;
; :Returns:
;    The result is a string containing the JSON string. If the input value
;    is a LIST, then the JSON string will be encoded as an "array", with
;    square brackets and comma-separated values. If the input value is
;    a HASH, then the JSON string will be encoded as an "object", with
;    curly braces, and name-value pairs.
;
;    *Note*: When converting IDL variables, the following rules are used:
;      Undefined variables (or !NULL) become "null".
;      Byte values become "true" if nonzero, or "false" if zero.
;      All integers are passed on unchanged (if these are converted back
;      using JSON_PARSE, they will be type LONG64).
;      All floating-point numbers are passed on unchanged (if these are
;      converted back using JSON_PARSE, they will be type DOUBLE).
;      All strings are surrounded by double-quotes. The following special
;      characters will be escaped: \\ (backslash), \" (double quote),
;      \b (backspace 8b), \f (form feed 12b), \n (line feed 10b),
;      \r (carriage return 13b), \t (tab 9b). (Note that forward slash
;      characters are not escaped - this allows URL's to still look normal
;      in the resulting JSON).
;      LIST values become JSON arrays, with each list element being
;      converted using these rules.
;      HASH key-value pairs become JSON objects, with each value being
;      converted using these rules.
;   
;    *Note*: Since the HASH stores its name-value pairs in an arbitrary
;    order, the name-value pairs in the resulting JSON string may be
;    in a different order than the HASH::Keys() method would return.
;    
; :Params:
;    Value:
;      *Value* must be a HASH or LIST variable.
;
; :Keywords:
;    None
; :
;
; :Author:
;   CT, VIS, Jan 2012. Based on the IDLffJSON, written by Dawn Lenz.
;-
function JSON_Serialize, Value, DEBUG=debug

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then begin
    ON_ERROR, 2
    
    ; Catch errors from our method and fake the call stack.
    CATCH, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      MESSAGE, !ERROR_STATE.msg
    endif
  endif
  
  ; Make sure the object definition is compiled, in case this
  ; will be included in save files.
  IDLffJSON__Define

  obj = OBJ_NEW("IDLffJSON")
  result = obj->Serialize(Value, DEBUG=debug)
  OBJ_DESTROY, obj
  return, result
end

