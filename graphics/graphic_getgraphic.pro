; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/graphic_getgraphic.pro#1 $
;
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; PURPOSE:
;    Wrap objects in the appropriate Graphics class
;
; MODIFICATION HISTORY:
;     Written by:   AGEH, 05/2010
;-
function Graphic_GetGraphic, oObj
  compile_opt idl2, hidden

  if (ISA(oObj, 'Graphic')) then return, oObj

  if (~OBJ_VALID(oObj)) then return, OBJ_NEW()
  
  ; If we already have a proxy, just return it (don't create a new one).
  oObj->IDLitComponent::GetProperty, _PROXY=graphic
  if (OBJ_VALID(graphic)) then return, graphic

  case TYPENAME(oObj) of
    'IDLITVISTEXT': class = 'Text'
    'IDLITVISMAPPROJECTION': class = 'MapProjection'
    'IDLITVISSHAPEPOLYGON': class = 'MapContinents'
    'IDLITVISMAPGRID': class = 'MapGrid'
    'IDLITVISMAPGRIDLINE': class = 'MapGridline'
    'IDLITVISMAPGRIDCONTAINER': class = 'MapGridline'
    'IDLITVISPLOT' : class = 'Plot'
    'IDLITVISPLOT3D' : class = 'Plot3D'
    'IDLITVISAXIS' : class = 'Axis'
    'IDLITVISCONTOUR' : class = 'Contour'
    'IDLITVISIMAGE' : class = 'Image'
    'IDLITVISSTREAMLINE' : class = 'Streamline'
    'IDLITVISSURFACE' : class = 'Surface'
    'IDLITVISVECTOR' : class = 'Vector'
    'IDLITVISVOLUME' : class = 'Volume'
    'IDLITVISCOLORBAR' : class = 'Colorbar'
    'IDLITVISPOLYLINE' : class = 'Polyline'
    'IDLBARPLOT': class = 'Barplot'
    'IDLBOXPLOT': class = 'Boxplot'
    'IDLBUBBLEPLOT': class = 'Bubbleplot'
    'IDLSCATTERPLOT': class = 'Scatterplot'
    'IDLSCATTERPLOT3D': class = 'Scatterplot3D'
    'IDLITVISARROW': class = 'Arrow'
    'IDLCROSSHAIR': class = 'Crosshair'
    'IDLITVISPOLYGON' : begin
      class = 'Polygon'
      oObj->GetProperty, IDENTIFIER=id
      if (STRPOS(id, 'OVAL') ne -1) then $
        class = 'Ellipse'
    end
    else : begin
      ; Handle all legend items here
      if (ISA(oObj, 'IDLitVisLegendItem')) then begin
        class = 'LegendItem'
      endif else begin
        ; Return a default GRAPHIC if the object does not have a wrapper
        class = 'Graphic'
      endelse
    end
  endcase
  
  oGraphic = OBJ_NEW(class, oObj)
  oObj->IDLitComponent::SetProperty, _PROXY=oGraphic
  return, oGraphic
  
end
