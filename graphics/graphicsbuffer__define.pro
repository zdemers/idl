; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;------------------------------------------------------------------------
;+
; :Description:
;    Initialize the object.
;
; :Params:
;
; :Keywords:
;    _EXTRA
;
;-
function GraphicsBuffer::Init, _EXTRA=_extra
    
  compile_opt idl2, hidden
  
  if (~self->Graphic::Init(self)) then return, 0
  if (~self->IDLitgrBuffer::Init(_EXTRA=_extra)) then return, 0
  return, 1
end


;------------------------------------------------------------------------
pro GraphicsBuffer::Cleanup
  compile_opt idl2, hidden
  
  self->IDLitgrBuffer::Cleanup
end


;---------------------------------------------------------------------------
pro GraphicsBuffer::GetProperty, NAME=name, _REF_EXTRA=ex
  compile_opt idl2, hidden

  ; Be sure to retrieve the NAME property from our Tool, not ourself.
  ; That way the window name returned by GetWindows() will
  ; match the "window" name (which is really the tool name!).
  ;
  if (ARG_PRESENT(name)) then begin
    if (ISA(self.tool)) then begin
      self.tool->GetProperty, NAME=name
    endif else name = ''
  endif

  self->Graphic::GetProperty, _EXTRA=ex
  self->IDLitgrBuffer::GetProperty, _EXTRA=ex
end


;---------------------------------------------------------------------------
pro GraphicsBuffer::SetProperty, NAME=name, TITLE=title, _EXTRA=ex
  compile_opt idl2, hidden

  ; Be sure to set the NAME property on our Tool, not ourself.
  ; That way the window name returned by GetWindows() will
  ; match the "window" name (which is really the tool name!).
  ;
  if (ISA(name) && ISA(self.tool)) then begin
    self.tool->SetProperty, NAME=name
  endif

  self->Graphic::SetProperty, TITLE=title, _EXTRA=ex
  self->IDLitgrBuffer::SetProperty, _EXTRA=ex
end


;---------------------------------------------------------------------------
function GraphicsBuffer::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  ; This is slightly different from the GraphicsWin::QueryProperty
  ; because we don't include the Mouse/Keyboard handler properties
  ; in the "printed" list, but we do include them in /ALL (just so they
  ; don't throw errors, even though they are useless for a buffer.)
  myprops = ['BACKGROUND_COLOR', 'DIMENSIONS', 'RESOLUTION', $
    'NAME', 'TITLE','WINDOW_TITLE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, $
      'AUTO_RESIZE', $
      'CURRENT_ZOOM', $
      'COLOR_MODEL', 'DRAG_QUALITY', $
      'EVENT_HANDLER', $
      'FONT_COLOR', 'FONT_NAME', 'FONT_SIZE', 'FONT_STYLE', $
      'IS_BANDING', 'IS_LIGHTING', 'IS_SELECTING', $
      'LAYOUT_INDEX', $
      'MINIMUM_VIRTUAL_DIMENSIONS', $
      'MOUSE_DOWN_HANDLER', 'MOUSE_UP_HANDLER', $
      'MOUSE_MOTION_HANDLER', 'MOUSE_WHEEL_HANDLER', $
      'KEYBOARD_HANDLER', 'SELECTION_CHANGE_HANDLER', $
      'N_COLORS', 'PALETTE', 'QUALITY', $
      '_PARENT', $
      'VIRTUAL_DIMENSIONS', $
      'VIRTUAL_HEIGHT', 'VIRTUAL_WIDTH', 'VISIBLE_LOCATION', $
      'WINDOW', $
      'ZOOM_ON_RESIZE']
  endif

  return, myprops
end


;------------------------------------------------------------------------
pro GraphicsBuffer::Erase, COLOR=bgColor
  compile_opt idl2, hidden
  
  ; Call our private superclass method.
  self->Graphic::_Erase, COLOR=bgColor
end


;---------------------------------------------------------------------------
pro GraphicsBuffer::SetCurrent
  compile_opt idl2, hidden
  
  ; Let the system know the tool has possibly changed
  oSystem = _IDLitSys_GetSystem(/NO_CREATE)
  if (Obj_Valid(oSystem)) then $
    oSystem->_SetCurrentTool, self.tool, /NO_NOTIFY
end


;---------------------------------------------------------------------------
pro GraphicsBuffer::Show
  compile_opt idl2, hidden
  ; Being a buffer there is no "show", so just set to current
  self->SetCurrent
end


;---------------------------------------------------------------------------
function GraphicsBuffer::HitTest, X, Y, _REF_EXTRA=_extra
  compile_opt idl2, hidden
  ; Swallow everything
  return, OBJ_NEW()
end


;------------------------------------------------------------------------
pro GraphicsBuffer__define
  compile_opt idl2, hidden
  
  void = {GraphicsBuffer, $
    inherits Graphic, $
    inherits IDLitgrBuffer $
    }
    
end

