; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/createboxplotdata.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create IDL BoxPlot data
;
; :Params:
;    data : input data array
;
;-
function createboxplotdata_singlebox, dataIn, CI_VALUES=ciVal, $
                                      MEAN_VALUES=meanVal, $
                                      OUTLIER_VALUES=outlierVal, $
                                      SUSPECTED_OUTLIER_VALUES=suspectedVal
  compile_opt idl2, hidden
  
  ; Sort data
  data = dataIn[sort(dataIn)]
  n = N_ELEMENTS(data)

  ; Get first quartile value
  q1pos = (n+1)*0.25 - 1
  if (m = q1pos mod 1) then begin
    q1 = m*data[CEIL(q1pos)] + (1-m)*data[FLOOR(q1pos)]
  endif else begin
    q1 = data[q1pos]
  endelse

  ; Get second quartile value
  q2pos = (n+1)*0.50 - 1
  if (q2pos mod 1) then begin
    q2 = (data[CEIL(q2pos)] + data[FLOOR(q2pos)]) / 2.
  endif else begin
    q2 = data[q2pos]
  endelse

  ; Get third quartile value
  q3pos = (n+1)*0.75 - 1
  if (m = q3pos mod 1) then begin
    q3 = m*data[CEIL(q3pos)] + (1-m)*data[FLOOR(q3pos)]
  endif else begin
    q3 = data[q3pos]
  endelse

  ; Inner quartile range
  iqr = q3 - q1
  
  ; Mean value
  if (ARG_PRESENT(meanVal)) then begin
    meanVal = MEAN(data)
  endif

  ; Confidence interval value
  if (ARG_PRESENT(ciVal)) then begin
    ciVal = 1.57*iqr/SQRT(n)
  endif
  
  ; Min and max of data
  q0 = min(data)
  q4 = max(data)
  
  ; Find outliers
  if (ARG_PRESENT(outlierVal)) then begin
    outlierVal = [!NULL]
    upperFence = q3 + 3*iqr
    lowerFence = q1 - 3*iqr
    wh = where(data lt lowerFence, cnt)
    if (cnt ne 0) then begin
      outlierVal = [outlierVal, data[wh]]
      ; update min value 
      q0 = data[max(wh)+1]
    endif
    wh = where(data gt upperFence, cnt)
    if (cnt ne 0) then begin
      outlierVal = [outlierVal, data[wh]]
      ; update max value
      q4 = data[min(wh)-1]
    endif
  endif
  
  ; Find suspected outliers
  if (ARG_PRESENT(suspectedVal)) then begin
    suspectedVal = [!NULL]
    upperOuterFence = q3 + 3*iqr
    lowerOuterFence = q1 - 3*iqr
    upperInnerFence = q3 + 1.5*iqr
    lowerInnerFence = q1 - 1.5*iqr
    wh = where(data lt lowerInnerFence and data gt lowerOuterFence, cnt)
    if (cnt ne 0) then begin
      suspectedVal = [suspectedVal, data[wh]]
      ; update min value
      q0 = data[max(wh)+1]
    endif
    wh = where(data gt upperInnerFence and data lt upperOuterFence, cnt)
    if (cnt ne 0) then begin
      suspectedVal = [suspectedVal, data[wh]]
      ; update max value
      q4 = data[min(wh)-1]
    endif
  endif
  
  return, [q0,q1,q2,q3,q4]
  
end
                                      
;--------------------------------------------
function createboxplotdata, dataIn, CI_VALUES=ciVal, IGNORE=ignoreVal, $
                            MEAN_VALUES=meanVal, $
                            OUTLIER_VALUES=outlierVal, $
                            SUSPECTED_OUTLIER_VALUES=suspectedVal
  compile_opt idl2, hidden
  ON_ERROR, 2
                        
  ; Determine data type: 1-MxN array 2-M element pointer array 3-M element list
  if (ISA(dataIn, 'list')) then begin
    dataType = 3
  endif else if (SIZE(dataIn, /TNAME) eq 'POINTER') then begin
    dataType = 2
  endif else if (ISA(dataIn, /NUMBER)) then begin
    if (SIZE(dataIn, /N_DIMENSIONS) le 2) then $
      dataType = 1
  endif else begin
    dataType = 0
  endelse
  
  case dataType of
    1: begin
      if (SIZE(dataIn, /N_DIMENSIONS)) eq 1 then begin
        nData = 1
      endif else begin
        nData = (SIZE(dataIn, /DIMENSIONS))[0]
      endelse
    end
    2: nData = N_ELEMENTS(dataIn)
    3: nData = dataIn.Count()
    else: begin
      message, 'Invalid input data'
      return, 0
    endelse
  endcase
  
  outData = dblarr(nData,5)
  ciValOut = [!NULL]
  meanValOut = [!NULL]
  outlierValOut = [!NULL]
  suspectedValOut = [!NULL]
  
  for i=0,nData-1 do begin
    ; Get next set of data
    case dataType of
      1: begin
        if (SIZE(dataIn, /N_DIMENSIONS)) eq 1 then begin
          data = REAL_PART(dataIn)
        endif else begin
          data = REAL_PART(dataIn[i,*])
        endelse
      end
      2: data = PTR_VALID(dataIn[i]) ? *dataIn[i] : 0
      3: data = dataIn[i]
    endcase
    ; Filter out infinites and NaNs
    wh = WHERE(FINITE(data), cnt)
    if (cnt gt 0) then begin
      data = data[wh]
    endif else begin
      message, 'Input data contains no finite values'
      return, 0
    endelse
    ; Filter out ignore value
    if (N_ELEMENTS(ignoreVal) ne 0) then begin
      if ((N_ELEMENTS(ignoreVal) eq 1) && ISA(ignoreVal, /NUMBER)) then begin
        wh = WHERE(data ne ignoreVal[0], cnt)
        if (cnt gt 0) then begin
          data = data[wh]
        endif else begin
          message, 'Input data contains no acceptable values'
          return, 0
        endelse
      endif else begin
        message, 'Invalid ignore value'
        return, 0
      endelse
    endif
    ; Validate data
    if (~ISA(data, /NUMBER)) then begin
      message, 'Input data must be numbers'
      return, 0
    endif
    if (N_ELEMENTS(data) lt 5) then begin
      message, 'Each input set must contain at least 5 values'
      return, 0
    endif
    
    ; Clear out old temp value
    ciValTmp = !NULL
    meanValTmp = !NULL
    outValTmp = !NULL
    susValTmp = !NULL
    
    ; Get valuve for this set of data
    outData[i,*] = createboxplotdata_singlebox(data, $
      CI_VALUES=(ARG_PRESENT(ciVal) ? ciValTmp : !NULL), $
      MEAN_VALUES=(ARG_PRESENT(meanVal) ? meanValTmp : !NULL), $
      OUTLIER_VALUES=(ARG_PRESENT(outlierVal) ? outValTmp : !NULL), $
      SUSPECTED_OUTLIER_VALUES=(ARG_PRESENT(suspectedVal) ? susValTmp : !NULL))
      
    ; Save keyword options
    if (N_ELEMENTS(ciValTmp) ne 0) then $
      ciValOut = [ciValOut, ciValTmp]
    if (N_ELEMENTS(meanValTmp) ne 0) then $
      meanValOut = [meanValOut, meanValTmp]
    if (N_ELEMENTS(outValTmp) ne 0) then $
      outlierValOut = [[outlierValOut], $
        [transpose([[replicate(i,N_ELEMENTS(outValTmp))],[outValTmp]])]]
    if (N_ELEMENTS(susValTmp) ne 0) then $
      suspectedValOut = [[suspectedValOut], $
        [transpose([[replicate(i,N_ELEMENTS(susValTmp))],[susValTmp]])]]
  endfor

  ; Update output keywords
  if (ARG_PRESENT(ciVal)) then $
    ciVal = ciValOut
  if (ARG_PRESENT(meanVal)) then $
    meanVal = meanValOut
  if (ARG_PRESENT(outlierVal)) then $
    outlierVal = outlierValOut
  if (ARG_PRESENT(suspectedVal)) then $
    suspectedVal = suspectedValOut
    
  return, outData
  
end
