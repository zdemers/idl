; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/symbol__define.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create a Symbol.
;
; :Params:
;    X, Y, SYMBOL
;
; :Keywords:
;    
;
;-


;---------------------------------------------------------------------------
;pro Symbol::GetProperty, SYMBOL=symbolValue, _REF_EXTRA=ex
;
;  if ARG_PRESENT( symbolValue ) then begin
;    self->Graphic::GetProperty, SYM_INDEX=_symIndex, _SYM_TEXT=_symText
;  endif
;
;  self->Graphic::GetProperty, _EXTRA=ex
;
;end


;---------------------------------------------------------------------------
function Symbol::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['CLIP', $
    'LABEL_COLOR', 'LABEL_FILL_BACKGROUND', 'LABEL_FILL_COLOR', $
    'LABEL_FONT_NAME', 'LABEL_FONT_SIZE', 'LABEL_FONT_STYLE', 'LABEL_ORIENTATION', $
    'LABEL_POSITION','LABEL_SHIFT', 'LABEL_STRING', 'LABEL_TRANSPARENCY', $
    'LINE_COLOR', 'LINE_THICK', 'LINESTYLE', 'NAME', 'POSITION', $
    'SYM_COLOR', 'SYM_FILLED', 'SYM_FILL_COLOR', $
    'SYM_FONT_FILL_BACKGROUND', 'SYM_FONT_FILL_COLOR', 'SYM_FONT_NAME', $
    'SYM_FONT_SIZE', 'SYM_FONT_STYLE', $
    'SYM_ROTATE', 'SYM_SIZE', 'SYM_TEXT', 'SYM_THICK', 'SYM_TRANSPARENCY', 'SYMBOL']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end


;;---------------------------------------------------------------------------
;pro Symbol::Scale, X, Y, Z, _EXTRA=_extra
;  compile_opt idl2, hidden
;
;@graphic_error
;
;  ;Scale operations are not allowed for Symbols
;  message, 'Symbol: Scale operation unavailable.'
;  
;end


;-------------------------------------------------------------------------
pro symbol__define
  compile_opt idl2, hidden
  void = {Symbol, inherits Graphic}
end