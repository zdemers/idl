; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/graphic__define.pro#3 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.

;---------------------------------------------------------------------------
function Graphic::Init, obj, _EXTRA=ex
  compile_opt idl2, hidden

  if (ISA(obj) && ISA(obj, 'OBJREF')) then begin
    self.__obj__ = obj
    obj->IDLitComponent::SetProperty, _PROXY = self
  endif
  return, 1

end

;---------------------------------------------------------------------------
;+
; :Description:
;    Close the graphic window.
;
; :Params:
;    Filename
;
; :Keywords:
;
;-
pro Graphic::Close
  compile_opt idl2, hidden

@graphic_error
  oTool = ISA(self.__obj__) ? self.__obj__->GetTool() : self->GetTool()
  if (ISA(oTool)) then $
    iDelete, oTool->GetFullIdentifier(), /NO_PROMPT

  OBJ_DESTROY, self
end


;---------------------------------------------------------------------------
;+
; :Description:
;    Save the graphic.
;
; :Params:
;    Filename
;
; :Keywords:
;
;-
pro Graphic::Save, filename, _REF_EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error
  if (N_PARAMS() eq 0) then MESSAGE, 'Incorrect number of arguments.'
  if (~ISA(filename, 'STRING')) then MESSAGE, 'Filename must be a string.'
  obj = ISA(self.__obj__) ? self.__obj__ : self
  iSave, filename, TARGET_IDENTIFIER=obj->GetFullIdentifier(), _EXTRA=_extra

end


;---------------------------------------------------------------------------
;+
; :Description:
;    Print the graphic.
;
; :Params:
;    Filename
;
; :Keywords:
;
;-
pro Graphic::Print, $
  HEIGHT=height, $
  LANDSCAPE=landscape, $
  CENTIMETERS=centimeters, $
  NCOPIES=ncopies, $
  WIDTH=width, $
  XMARGIN=xmargin, $
  YMARGIN=ymargin

  compile_opt idl2, hidden

@graphic_error
  
  oTool = ISA(self.__obj__) ? self.__obj__->GetTool() : self->GetTool()
  if (~ISA(oTool)) then $
    return
  oSystem = oTool->_GetSystem()
  oSysPrint = oSystem->GetService("PRINTER")
  oSysPrint->_setTool, oTool
  
  center = ~ISA(xmargin) && ~ISA(ymargin)

  iStatus = oSysPrint->DoAction( oTool, $
    PRINT_ORIENTATION=landscape, $
    PRINT_XMARGIN=xmargin, $
    PRINT_YMARGIN=ymargin, $
    PRINT_NCOPIES=ncopies, $
    PRINT_WIDTH=width, $
    PRINT_HEIGHT=height, $
    PRINT_UNITS=centimeters, $
    PRINT_CENTER=center, $
    _EXTRA=_extra)
    
end
;---------------------------------------------------------------------------
;+
; :Description:
;    Copy the graphic to the system clipboard.
;
; :Params:
;
;
; :Keywords:
;
;-
pro Graphic::CopyToClipboard, _REF_EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error
  
  oTool = ISA(self.__obj__) ? self.__obj__->GetTool() : self->GetTool()
  if (~ISA(oTool)) then $
    return
  oSystem = oTool->_GetSystem()
  oSysCopy = oSystem->GetService("SYSTEM_CLIPBOARD_COPY")
  owin = oTool->GetcurrentWindow()
  if (~OBJ_VALID(oWin)) then $
      return
  iStatus = oSysCopy->DoWindowCopy( oWin, oWin->getCurrentView(), _EXTRA=_extra)
end


;---------------------------------------------------------------------------
;+
; :Description:
;    Edit the graphic using a Property Sheet.
;
; :Params:
;
;
; :Keywords:
;
;-
pro Graphic::Edit, _REF_EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  oRequester = ISA(self.__obj__) ? self.__obj__ : self
  oTool = oRequester->GetTool()
  if (~ISA(oTool)) then return
  
  void = oTool->DoUIService( 'PropertySheet', oRequester )

end


;------------------------------------------------------------------------
pro Graphic::_Erase, COLOR=bgColor
  compile_opt idl2, hidden
@graphic_error

  if (~ISA(self.__obj__)) then return

  oTool = self.__obj__.GetTool()
  oWin = oTool.GetCurrentWindow()

  !NULL=IDLitSys_CreateTool(EV_VIEW=oWin->GetCurrentView(), /EV_REMOVE_ALL)
  
  if (ISA(bgColor)) then $
    self->Graphic::SetProperty, BACKGROUND_COLOR=bgColor
end


;---------------------------------------------------------------------------
; Internal method to retrieve the correct object(s) on which to get/set
; the font properties.
;
function Graphic::_GetFontPropertyObjects

  compile_opt idl2, hidden

  ; If our object has font properties, just return our object.
  if (ISA(self, 'TEXT') || $
    ISA(self, 'MAPGRID') || ISA(self, 'MAPGRIDLINE') || $
    ISA(self, 'AXIS') || $
    ISA(self, 'CONTOUR') || $
    ISA(self, 'COLORBAR') || $
    ISA(self, 'LEGEND') || $
    ISA(self, 'LEGENDITEM')) then $
    return, self.__obj__

  oResult = !null

  ; Get/Set the font properties on the current title object (if there is one).
  self._GetProperty, TITLE=oTitle
  if (ISA(oTitle)) then oResult = [oResult, oTitle]

  ; Also set the font properties on the axes (if they exist).
  if (ISA(self.__obj__, '_IDLitVisualization')) then begin
    oDataSpace = self.__obj__.GetDataSpace()
    oAxes = ISA(oDataSpace) ? oDataSpace->GetAxes(/CONTAINER) : !NULL
    if (ISA(oAxes)) then oResult = [oResult, oAxes]
  endif

  return, oResult
end


;---------------------------------------------------------------------------
; Internal method to retrieve the POSITION property.
;
function Graphic::_GetPosition, position

  compile_opt idl2, hidden

  ; This will work for Legend and Colorbar
  self.__obj__->GetProperty, POSITION=position
  if (ISA(position)) then return, position

  if (~ISA(self.__obj__, '_IDLitVisualization')) then return, !NULL

  ; Is this an annotation in the Annotation Layer?
  id = self.__obj__->GetFullIdentifier()
  if (STRPOS(id, 'ANNOTATION LAYER') ge 0) then begin
    void = self.__obj__.GetXYZRange(xrange, yrange, zrange)
    conv = iConvertCoord(xrange, yrange, /ANNOTATION_DATA, /TO_NORMAL)
    position = [conv[0:1,0],conv[0:1,1]]
    return, position
  endif

  oTarget = self.__obj__.GetManipulatorTarget()

  ; First retrieve the objects X/Y Range
  if (ISA(oTarget, 'IDLitVisNormDataSpace')) then begin

    oDS = oTarget
    oDS.GetProperty, X_MINIMUM=xMin, X_MAXIMUM=xMax, $
      Y_MINIMUM=yMin, Y_MAXIMUM=yMax
    xrange = [xMin,xMax]
    yrange = [yMin,yMax]

    ; For 3D reset the rotation matrix so we get the correct position.
    if (oTarget->Is3D()) then begin
      oDS.GetProperty, TRANSFORM=origTransform
      self->Rotate, /RESET, /NO_TRANSACT
    endif
    
  endif else begin
    ; Not a Dataspace, must be an annotation object within the dataspace.
    oDS = self.__obj__->GetDataspace()
    ; Retrieve the target's extent, in data coordinates.
    void = oTarget.GetXYZRange(xrange, yrange, zrange)
  endelse

  ; Make sure to use the Dataspace for the coordinate conversions,
  ; not the target object, because it might already have some
  ; model transformations.
  conv = iConvertCoord(xrange, yrange, /DATA, /TO_NORMAL, TARGET=oDS)
  position = [conv[0:1,0],conv[0:1,1]]

  ; Restore the original transform matrix if we had to reset it.
  if (ISA(origTransform)) then begin
    oDS.SetProperty, TRANSFORM=origTransform
  endif
  
  return, position
end


;---------------------------------------------------------------------------
; Internal method to retrieve the POSITION property.
;
pro Graphic::_SetPosition, positionIn, DEVICE=device

  compile_opt idl2, hidden

  position = positionIn

  if (ISA(self, 'COLORBAR') || ISA(self, 'LEGEND')) then begin
    self.__obj__->SetProperty, POSITION=position
    return
  endif

  if (~ISA(self.__obj__, '_IDLitVisualization')) then return

  np = N_ELEMENTS(position)
  if (np ne 2 && np ne 4) then MESSAGE, 'POSITION must have 2 or 4 elements.'

  ; Convert from DEVICE (pixel) coordinates to normalized.
  if (KEYWORD_SET(device)) then begin
    if (np eq 2) then begin
      position = iConvertCoord(position[0], position[1], /DEVICE, /TO_NORMAL)
    endif else begin
      cc = iConvertCoord(position[[0,2]], position[[1,3]], /DEVICE, /TO_NORMAL)
      position = [cc[0,0], cc[1,0], cc[0,1], cc[1,1]]
    endelse
  endif

  ; Is this an annotation in the Annotation Layer?
  id = self.__obj__->GetFullIdentifier()
  if (STRPOS(id, 'ANNOTATION LAYER') ge 0) then begin
    ; Prevent crazyness by resetting the transform matrix.
    self.__obj__->SetProperty, TRANSFORM=IDENTITY(4)
    void = self.__obj__.GetXYZRange(xrange, yrange, zrange)
    conv = iConvertCoord(xrange, yrange, /ANNOTATION_DATA, /TO_NORMAL)
    curPos = [conv[0:1,0],conv[0:1,1]]
  
    curCenter = [curPos[0]+curPos[2], curPos[1]+curPos[3]]/2d
    center = (np eq 2) ? position : $
      [position[0]+position[2], position[1]+position[3]]/2d
    dx = center[0] - curCenter[0]
    dy = center[1] - curCenter[1]
    self->Translate, dx, dy, /NORMAL

    if (np eq 4 && ~ISA(self, 'Text')) then begin
      curWidth = curPos[2] - curPos[0]
      curHeight = curPos[3] - curPos[1]
      newWidth = position[2] - position[0]
      newHeight = position[3] - position[1]
      ; Sanity check to prevent "infinite" scales
      if (curWidth ne 0 && curHeight ne 0 && newWidth ne 0 && newHeight ne 0) then begin
        sx = newWidth/curWidth
        sy = newHeight/curHeight
        sz = SQRT(sx*sy)
        self->Scale, sx, sy, 1, /PREMULTIPLY
      endif
    endif

    return
  endif


  oTarget = self.__obj__.GetManipulatorTarget()

  if (ISA(oTarget, 'IDLitVisNormDataSpace')) then begin

    oTarget.GetProperty, X_MINIMUM=xMin, X_MAXIMUM=xMax, $
      Y_MINIMUM=yMin, Y_MAXIMUM=yMax, TRANSFORM=origTransform
    xrange = [xMin,xMax]
    yrange = [yMin,yMax]

    if (oTarget->Is3D()) then begin
      ; Remove the current rotation so we can compute the normalized
      ; position as if the dataspace wasn't rotated.
      self->Rotate, /RESET, /NO_TRANSACT
      ; Compute the original rotation matrix so we can restore it.
      oTarget->GetProperty, TRANSFORM=currentTransform
      rotMatrix = INVERT(currentTransform) ## origTransform
    endif

  endif else begin

    ; Not a Dataspace, must be an annotation object within the dataspace.
    oDS = self.__obj__->GetDataspace()
    ; Prevent crazyness by resetting the transform matrix.
    self.__obj__->SetProperty, TRANSFORM=IDENTITY(4)
    ; Retrieve the target's extent, in data coordinates.
    void = oTarget.GetXYZRange(xrange, yrange, zrange)
  endelse

  conv = iConvertCoord(xrange, yrange, /DATA, /TO_NORMAL, TARGET=oTarget)
  curPos = [conv[0:1,0],conv[0:1,1]]

  curCenter = [curPos[0]+curPos[2], curPos[1]+curPos[3]]/2d
  center = (np eq 2) ? position : $
    [position[0]+position[2], position[1]+position[3]]/2d
  dx = center[0] - curCenter[0]
  dy = center[1] - curCenter[1]
  self->Translate, dx, dy, /NORMAL

  if (np eq 4 && ~ISA(self, 'Text')) then begin
    curWidth = curPos[2] - curPos[0]
    curHeight = curPos[3] - curPos[1]
    newWidth = position[2] - position[0]
    newHeight = position[3] - position[1]
    ; Sanity check to prevent "infinite" scales
    if (curWidth ne 0 && curHeight ne 0 && newWidth ne 0 && newHeight ne 0) then begin
      sx = newWidth/curWidth
      sy = newHeight/curHeight
      sz = SQRT(sx*sy)
      self->Scale, sx, sy, sz, /PREMULTIPLY
    endif
  endif

  ; Restore the original rotation for 3D visualizations.
  if (ISA(rotMatrix)) then begin
    oTarget.GetProperty, TRANSFORM=newTransform
    oTarget.SetProperty, TRANSFORM=newTransform ## rotMatrix
  endif
  
end


;---------------------------------------------------------------------------
pro Graphic::_GetProperty, $
  AXES=axes, $
  ASPECT_RATIO=aspectRatio, $
  ASPECT_Z=aspectZ, $
  BACKGROUND_COLOR=bgColor, $
  BACKGROUND_TRANSPARENCY=bgTransparency, $
  CROSSHAIR=crosshair, $
  DEPTH_CUE=depthCue, $
  EYE=eye, $
  FONT_COLOR=fontColor, $
  FONT_NAME=fontName, $
  FONT_SIZE=fontSize, $
  FONT_STYLE=fontStyle, $
  LATITUDES=oLatitudes, $
  LONGITUDES=oLongitudes, $
  LIMIT=limit, $
  MAPGRID=oGrid, $
  MAP_PROJECTION=mapProjString, $
  MAPPROJECTION=oMap, $
  PERSPECTIVE=perspective, $
  POSITION=position, $
  TITLE=oTitle, $
  WINDOW=oWin, $
  PARENT=oParent, $
  XRANGE=xrange, YRANGE=yrange, ZRANGE=zrange, $
  ZCLIP=zclip, $
  SCALE_FACTOR=scale_factor, $
  SCALE_CENTER=scale_center, $
  _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  if (~ISA(self.__obj__)) then return
  
  if (~ISA( self.__obj__,'GRAPHICSWIN' ) && $
      ~ISA(self.__obj__,'GRAPHICSBUFFER') && $
      ISA(ex)) then begin

      ; If we find at least one Axis keyword, then pass
      ; everything in _EXTRA to GetProperty on the
      ; Axes object
      if (ISA(self.__obj__, '_IDLitVisualization')) then begin
        tagnameChar = STRMID(ex, 0, 1)
        foundKeyword = tagnameChar eq 'X' or tagnameChar eq 'Y' or tagnameChar eq 'Z'
        if (MAX(foundKeyword) eq 1) then begin
          oDataSpace = self.__obj__.GetDataSpace()
          if ISA(oDataSpace) then $
            oAxes = oDataSpace->GetAxes(/CONTAINER)
          if (ISA(oAxes)) then begin
            oAxes.GetProperty, _EXTRA=ex
          endif
        endif
      endif

    ; Get Map Grid properties from the MapGrid object, but watch out for
    ; infinite loops, so don't include the MapGrid itself or its children.
    if (~ISA(self, 'MapGrid') && ~ISA(self, 'MapGridLine')) then begin
      ; Only try to retrieve valid MapGrid properties.
      gridProps = MapGrid_Properties(/ALL)
      foreach ex1, ex do begin
        if (MAX(gridProps eq ex1) eq 1b) then begin
          lookingForGridProp = 1b
          break
        endif
      endforeach
      if (KEYWORD_SET(lookingForGridProp)) then begin
        oGrid = self['MAP GRID']
        if (ISA(oGrid)) then begin
          oGrid.GetProperty, _EXTRA=gridProps
        endif
      endif
    endif

  endif
  
  if (ARG_PRESENT(fontColor) || ARG_PRESENT(fontName) || $
    ARG_PRESENT(fontSize) || ARG_PRESENT(fontStyle)) then begin
    ; We need to retrieve the correct object to get the font properties from.
    oFontPropObj = self->_GetFontPropertyObjects()
    if (ISA(oFontPropObj)) then begin
      ; Only retrieve the properties from the first object
      ; (either the title, or if no title, then the axes)
      oFontPropObj[0]->GetProperty, FONT_COLOR=fontColor, FONT_NAME=fontName, $
        FONT_SIZE=fontSize, FONT_STYLE=fontStyle
    endif
  endif

  if ARG_PRESENT(axes) then begin
     oDS = ISA(self.__obj__, '_IDLitVisualization') ? $
       self.__obj__.GetDataSpace() : OBJ_NEW()
    if (ISA(oDS)) then begin
      ids = iGetID(oDS->GetFullIdentifier() + '/*axis*')
      axes = self[ids]
    endif else begin
      axes = self['*axis*']
    endelse
  endif

  if (ARG_PRESENT(aspectRatio)) then begin
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace()
      if (ISA(oDS)) then $
        oDS.GetProperty, ASPECT_RATIO=aspectRatio
    endif
  endif

  if (ARG_PRESENT(aspectZ)) then begin
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace()
      if (ISA(oDS)) then $
        oDS.GetProperty, ASPECT_Z=aspectZ
    endif
  endif

  if ARG_PRESENT(bgColor) then begin
    bgColor = [255b, 255b, 255b]
    if (ISA(self.__obj__, '_IDLitgrDest')) then begin
      oTool = self.__obj__.GetTool()
      oWin = oTool.GetCurrentWindow()
      oView = ISA(oWin) ? oWin.GetCurrentView() : OBJ_NEW()
      oLayer = ISA(oView) ? oView.GetCurrentLayer() : OBJ_NEW()
      if ISA(oLayer) then $
        oLayer.GetProperty, COLOR=bgColor
    endif else begin
      oDS = self.__obj__->GetDataspace(/UNNORMALIZED)
      if (ISA(oDS)) then $
        oDS->GetProperty, FILL_COLOR=bgColor
    endelse
  endif


  if (ARG_PRESENT(bgTransparency)) then begin
    bgTransparency = 0
    oTool = self.__obj__.GetTool()
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__->GetDataspace(/UNNORMALIZED)
      if (ISA(oDS)) then $
        oDS->GetProperty, FILL_TRANSPARENCY=bgTransparency
    endif
  endif

  if (ARG_PRESENT(crosshair)) then begin
    oDS = ISA(self.__obj__, '_IDLitVisualization') ? $
      self.__obj__.GetDataSpace(/UNNORMALIZED) : OBJ_NEW()
    crosshair = OBJ_NEW()
    if (ISA(oDS)) then begin
      oDS->GetProperty, CROSSHAIR=oCrosshair
      crosshair = Graphic_GetGraphic(oCrosshair)
    endif
  endif

  if ARG_PRESENT(depthCue) || ARG_PRESENT(perspective) then begin
    oTool = self.__obj__.GetTool()
    oWin = oTool.GetCurrentWindow()
    oView = ISA(oWin) ? oWin.GetCurrentView() : OBJ_NEW()
    oLayer = ISA(oView) ? oView.GetCurrentLayer() : OBJ_NEW()
    if ISA(oLayer) then $
      oLayer.GetProperty, DEPTH_CUE=depthCue, PERSPECTIVE=perspective
  endif

  ; Allow MAP_PROJECTION to be retrieved from any graphics object.
  if ARG_PRESENT(mapProjString) then begin
    oDS = ISA(self.__obj__, '_IDLitVisualization') ? $
      self.__obj__.GetDataSpace(/UNNORMALIZED) : OBJ_NEW()
    mapProjStruct = ISA(oDS) ? oDS->GetProjection() : 0
    if (N_TAGS(mapProjStruct) gt 0) then begin
      mapProjString = mapProjStruct.up_name
    endif else begin
      mapProjString = ''
    endelse
  endif

  if ARG_PRESENT(oLatitudes) then begin
    self.__obj__->IDLitComponent::GetProperty, IDENTIFIER=id
    oLatitudes = self[id + '/LATITUDES/*']
  endif

  if ARG_PRESENT(oLongitudes) then begin
    self.__obj__->IDLitComponent::GetProperty, IDENTIFIER=id
    oLongitudes = self[id + '/LONGITUDES/*']
  endif

  if ARG_PRESENT(limit) then begin
    if (~ISA(self, 'MapProjection')) then begin
      self->_GetProperty, MAPPROJECTION=oMap
      if (ISA(oMap)) then begin
        oMap->_GetProperty, LIMIT=limit
      endif else begin
        limit = [0,0,0,0]
      endelse
    endif else begin
      self.__obj__.GetProperty, LIMIT=limit
    endelse
  endif

  if ARG_PRESENT(oGrid) then $
    oGrid = self['MAP GRID']

  if ARG_PRESENT(oMap) then begin
    ; Try to retrieve the map projection object the brute
    ; force way (from the dataspace), in case the user gave
    ; it their own name/identifier.
    oDS = ISA(self.__obj__, '_IDLitVisualization') ? $
      self.__obj__.GetDataSpace(/UNNORMALIZED) : OBJ_NEW()
    mapProj = ISA(oDS) ? oDS->_GetMapProjection(/NO_CREATE) : OBJ_NEW()
    if (ISA(mapProj)) then begin
      oMap = (mapProj ne self.__obj__) ? Graphic_GetGraphic(mapProj) : self
    endif else begin
      ; If all else fails, try to get it using the default name.
      oMap = self['MAP PROJECTION']
    endelse
  endif

  if ARG_PRESENT(oParent) then begin
    self.__obj__->GetProperty, PARENT=oP
    if (ISA(oP, 'IDLitVisDataspace')) then $
      oP = oP->GetDataspace()
    oParent = OBJ_NEW('Graphic', oP)
  end

  if ARG_PRESENT(oTitle) then begin
    ; Get the title from the proper data space
    oTool = self.__obj__.GetTool()
    oSelf = oTool->GetByIdentifier(self.__obj__->GetFullIdentifier())
    dsID = ''
    if (ISA(oSelf, '_IDLitVisualization')) then begin
      oDS = oSelf->GetDataspace()
      oDS->GetProperty, IDENTIFIER=dsID
      dsID += '/'
    endif 
    oTitle = self[dsID+'TITLE']
  endif

  if ARG_PRESENT(oWin) then begin
    oTool = self.__obj__->GetTool()
    oWin = oTool->GetCurrentWindow()
  endif

  if (ARG_PRESENT(position)) then begin
    position = self->_GetPosition()
  endif

  if (ARG_PRESENT(xrange) || ARG_PRESENT(yrange) || ARG_PRESENT(zrange)) then begin
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace()
      if (ISA(oDS)) then begin
        oDS.GetProperty, X_MINIMUM=xMin, X_MAXIMUM=xMax, $
          Y_MINIMUM=yMin, Y_MAXIMUM=yMax, $
          Z_MINIMUM=zMin, Z_MAXIMUM=zMax
        xrange = [xMin, xMax]
        yrange = [yMin, yMax]
        zrange = [zMin, zMax]
      endif
    endif
  endif
  
  if (ARG_PRESENT(eye) || ARG_PRESENT(zclip)) then begin
    oTool = self.__obj__.GetTool()
    oWin = oTool.GetCurrentWindow()
    oView = ISA(oWin) ? oWin.GetCurrentView() : OBJ_NEW()
    oLayer = ISA(oView) ? oView.GetCurrentLayer() : OBJ_NEW()
    if ISA(oLayer) then $
      oLayer.GetProperty, EYE=eye, ZCLIP=zclip
  endif

  if ARG_PRESENT(scale_factor) then begin
    oTool = self.__obj__.GetTool()
    self.__obj__._getDataDimensions,xr,yr
    self.getProperty, POSITION=position, XRANGE=xrange,YRANGE=yrange
    oTool.ConvertScaleFactor, SCALE_FACTOR=scale_factor, $
                              IMAGE_SIZE=[xr[1],yr[1]], $
                              XRANGE=xrange, $
                              YRANGE=yrange
  endif
  
  if ARG_PRESENT(scale_center) then begin
    self.getProperty, XRANGE=xr,YRANGE=yr
    
    scale_center = [(xr[0]+xr[1])/2.,(yr[0]+yr[1])/2.]
  endif

  if (ISA(ex)) then begin
    ; Hack to avoid keyword conflicts with WINDOW keyword.
    ; Just get the WINDOW_TITLE out of the ref_extra.
    if (MAX(ex eq 'WINDOW_TITLE') eq 1) then begin
      oTool = self.__obj__.GetTool()
      oWin = ISA(oTool) ? oTool->GetCurrentWindow() : OBJ_NEW()
      winTitle = ''
      if (ISA(oWin, 'IDLgrWindow')) then $
        oWin->IDLgrWindow::GetProperty, TITLE=winTitle
      (SCOPE_VARFETCH('WINDOW_TITLE', /REF_EXTRA)) = winTitle
    endif
    if (self.__obj__ ne self) then begin
      (self.__obj__)->GetProperty, _EXTRA=ex
    endif
  endif

end


;---------------------------------------------------------------------------
pro Graphic::_SetProperty, $
  ASPECT_RATIO=aspectRatio, $
  ASPECT_Z=aspectZ, $
  BACKGROUND_COLOR=bgColor, $
  BACKGROUND_TRANSPARENCY=bgTransparency, $
  DEPTH_CUE=depthCue, $
  DEVICE=device, $
  EYE=eye, $
  FONT_COLOR=fontColor, $
  FONT_NAME=fontName, $
  FONT_SIZE=fontSize, $
  FONT_STYLE=fontStyle, $
  LIMIT=limit, $
  MAP_PROJECTION=mapProjString, $
  PERSPECTIVE=perspective, $
  POSITION=position, $
  TITLE=title, $
  WINDOW_TITLE=winTitle, $
  XRANGE=xrange, YRANGE=yrange, ZRANGE=zrange, $
  ZCLIP=zclip, $
  SCALE_FACTOR=scale_factor, $
  SCALE_CENTER=scale_center, $
  _EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  if (~ISA(self.__obj__)) then return

  oTool = self->GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->DisableUpdates, PREVIOUSLY_DISABLED=wasDisabled

  ; If we find at least one Axis keyword, then pass
  ; everything in _EXTRA to SetProperty on the
  ; Axes object
  if (ISA(ex) && ISA(self.__obj__, '_IDLitVisualization')) then begin
    tagnameChar = STRMID(TAG_NAMES(ex), 0, 1)
    foundKeyword = tagnameChar eq 'X' or tagnameChar eq 'Y' or tagnameChar eq 'Z'
    if (MAX(foundKeyword) eq 1) then begin
      oDataSpace = self.__obj__.GetDataSpace()
      if ISA(oDataSpace) then $
        oAxes = oDataSpace->GetAxes(/CONTAINER)
      if (ISA(oAxes)) then begin
        ; setting property individually, reset {x|y|z}RangeSet to honor {x|y|z}STYLE
        if (ISA(ex)) then begin
          tagnames = TAG_NAMES(ex)
          if (MAX(tagnames eq 'XSTYLE')) then $
            oDataSpace->SetProperty, X_AUTO_UPDATE=1
          if (MAX(tagnames eq 'YSTYLE')) then $
            oDataSpace->SetProperty, Y_AUTO_UPDATE=1
          if (MAX(tagnames eq 'ZSTYLE')) then $
            oDataSpace->SetProperty, Z_AUTO_UPDATE=1
        endif
        oAxes.SetProperty, _EXTRA=ex
      endif
    endif
  endif

  if (ISA(ex)) then begin
    tagnames = TAG_NAMES(ex)
    extra = {}
    gridExtra = {}
    gridProps = MapGrid_Properties(/ALL)
  
    foreach tagname, tagnames, index do begin
      value = ex.(index)
      if (strmatch(tagname, "*COLOR*") && $
        (tagname ne "USE_DEFAULT_COLOR") && (tagname ne "AUTO_COLOR")) || $
        (tagname eq "BOTTOM") || (tagname eq "AMBIENT") then begin
        valueIn = value
        Style_Convert, valueIn, COLOR=value
      endif
      
      if ((tagname eq "LINESTYLE" || tagname eq 'C_LINESTYLE') && isa(value, 'STRING')) then $
        value  = linestyle_convert(value)
      
      if (~ISA( self, 'SYMBOL' ) && ((tagname eq "SYM_INDEX") || (tagname eq "SYMBOL")) && isa(value, 'STRING'))  then $
        value  = symbol_convert(value)
  
      extra = CREATE_STRUCT(extra, tagname, value)
      
      ; If we have any map grid properties, cache them for later.
      ; Don't do this if we are the MapGrid or a MapGrid child.
      if (ISA(self, 'MapProjection') && $
        ~ISA(self, 'MapGrid') && ~ISA(self, 'MapGridLine') && $
        MAX(gridProps eq tagname) eq 1) then begin
        gridExtra = CREATE_STRUCT(gridExtra, tagname, value)
      endif
    endforeach
  endif


  ; If we had map grid properties, find our map grid (if any) and set them.
  if (ISA(gridExtra)) then begin
    oGrid = self['map grid']
    if (ISA(oGrid)) then $
      oGrid._SetProperty, _EXTRA=gridExtra
  endif


  if (ISA(aspectRatio)) then begin
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace()
      if (ISA(oDS)) then begin
        oDS.SetProperty, ASPECT_RATIO=aspectRatio
        oTool.RefreshCurrentWindow
      endif
    endif
  endif
  
  if (ISA(aspectZ)) then begin
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace()
      if (ISA(oDS)) then begin
        oDS.SetProperty, ASPECT_Z=aspectZ
        oTool.RefreshCurrentWindow
      endif
    endif
  endif

  if (ISA(bgColor)) then begin
    Style_Convert, bgColor, COLOR=backgroundColor
    if (ISA(self.__obj__, '_IDLitgrDest')) then begin
      oWin = oTool.GetCurrentWindow()
      oView = ISA(oWin) ? oWin.GetCurrentView() : OBJ_NEW()
      oLayer = ISA(oView) ? oView.GetCurrentLayer() : OBJ_NEW()
      if ISA(oLayer) then oLayer.SetProperty, COLOR=backgroundColor
    endif else begin
      oDS = self.__obj__->GetDataspace(/UNNORMALIZED)
      if (ISA(oDS)) then begin
        oDS->SetProperty, FILL_COLOR=backgroundColor
        oTool.RefreshCurrentWindow
      endif
    endelse
  endif

  if (ISA(bgTransparency)) then begin
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__->GetDataspace(/UNNORMALIZED)
      if (ISA(oDS)) then begin
        oDS->SetProperty, FILL_TRANSPARENCY=bgTransparency
        oTool.RefreshCurrentWindow
      endif
    endif
  endif

  if (ISA(depthCue) || ISA(perspective)) then begin
    oWin = oTool.GetCurrentWindow()
    oView = ISA(oWin) ? oWin.GetCurrentView() : OBJ_NEW()
    oLayer = ISA(oView) ? oView.GetCurrentLayer() : OBJ_NEW()
    if ISA(oLayer) then $
      oLayer.SetProperty, DEPTH_CUE=depthCue, PERSPECTIVE=perspective
  endif


  if (ISA(fontColor) || ISA(fontName) || $
    ISA(fontSize) || ISA(fontStyle)) then begin
    ; We need to retrieve the correct object(s) to set the font properties on.
    oFontPropObj = self->_GetFontPropertyObjects()
    foreach obj, oFontPropObj do begin
      obj->SetProperty, FONT_COLOR=fontColor, FONT_NAME=fontName, $
        FONT_SIZE=fontSize, FONT_STYLE=fontStyle
    endforeach
    oTool->RefreshCurrentWindow
  endif


  ; Map projection limit.
  if (ISA(limit)) then begin
    if (~ISA(self, 'MapProjection')) then begin
      self->_GetProperty, MAPPROJECTION=oMap
      oMap->_SetProperty, LIMIT=limit
    endif else begin
      self.__obj__.GetProperty, LIMIT=oldlimit
      self.__obj__.SetProperty, LIMIT=limit
      ; If the user sets the limit, then force the
      ; dataspace UV range to match the new limit. This ensures that if you
      ; overplot new data on top of the map, that it doesn't affect the range.
      if (~ARRAY_EQUAL(oldlimit, limit)) then begin
        sMap = self.__obj__->GetProjection()
        if (N_Tags(sMap) gt 0) then begin
          xrangeNew = sMap.uv_box[[0,2]]
          yrangeNew = sMap.uv_box[[1,3]]
          self->_SetProperty, XRANGE=xrangeNew, YRANGE=yrangeNew
        endif
      endif
    endelse
  endif

  
  ; Allow MAP_PROJECTION to be set on any graphics object.
  if ISA(mapProjString) then begin
    ; Watch out for infinite loop if I am the MapProjection object.
    if ISA(self, 'MapProjection') then begin
      self.__obj__.SetProperty, MAP_PROJECTION=mapProjString
    endif else begin
      ; Retrieve the MapProjection object, and set the projection on it.
      self.GetProperty, MAPPROJECTION=oMap
      if (ISA(oMap)) then begin
        oMap.SetProperty, MAP_PROJECTION=mapProjString
      endif else begin
        MESSAGE, 'Unable to retrieve map projection.'
      endelse
    endelse
  endif

  if (ISA(position)) then begin
    self->_SetPosition, position, DEVICE=device
    oTool.RefreshCurrentWindow
  endif


  if (ISA(title)) then begin
    if ~ISA(title,'STRING') then MESSAGE, 'TITLE must be a string.'
    ; Retrieve the current title object.
    self._GetProperty, TITLE=oTitle
    ; If we don't have a title yet, then create one.
    if (~ISA(oTitle)) then begin
      id = self.__obj__->GetFullIdentifier()
      iText, '', TARGET=id, /TITLE
      oTitle = self['TITLE']
    endif
    if ~ISA(oTitle) then MESSAGE, 'Unable to retrieve title.'
    oTitle.string = title
  endif

  if (ISA(winTitle,'STRING')) then begin
    ; Pass the window title to the workbench.
    if (ISA(oTool)) then begin
      void = IDLNotify('IDLitThumbnail', $
        winTitle + '::' + oTool->GetFullIdentifier(), '')
      oWin = ISA(oTool) ? oTool->GetCurrentWindow() : OBJ_NEW()
      if (ISA(oWin, 'IDLgrWindow')) then $
        oWin->IDLgrWindow::SetProperty, TITLE=winTitle
    endif
  endif

  if (ISA(xrange)) then begin
    if (N_ELEMENTS(xrange) gt 2) then MESSAGE, 'XRANGE must have 1 or 2 elements.'
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace(/UNNORMALIZED)
      if (ISA(oDS)) then begin
        oDS.SetProperty, XRANGE=xrange
        oTool.RefreshCurrentWindow
      endif
    endif
  endif

  if (ISA(yrange)) then begin
    if (N_ELEMENTS(yrange) gt 2) then MESSAGE, 'YRANGE must have 1 or 2 elements.'
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace(/UNNORMALIZED)
      if (ISA(oDS)) then begin
        oDS.SetProperty, YRANGE=yrange
        oTool.RefreshCurrentWindow
      endif
    endif
  endif

  if (ISA(zrange)) then begin
    if (N_ELEMENTS(zrange) gt 2) then MESSAGE, 'ZRANGE must have 1 or 2 elements.'
    if (ISA(self.__obj__, '_IDLitVisualization')) then begin
      oDS = self.__obj__.GetDataSpace(/UNNORMALIZED)
      if (ISA(oDS)) then begin
        oDS.SetProperty, ZRANGE=zrange
        oTool = self.__obj__.GetTool()
        oTool.RefreshCurrentWindow
      endif
    endif
  endif
  
  if (ISA(scale_factor)) then begin
    if n_elements(scale_factor) gt 1 then MESSAGE, 'SCALE_FACTOR must be a scalar.'
    
    self.__obj__._getDataDimensions,txr,tyr
    self.getProperty, POSITION=position, XRANGE=xr,YRANGE=yr
    
    oTool.ApplyScaleFactor, SCALE_FACTOR=scale_factor, $
                              SCALE_CENTER=[(xr[0]+xr[1])/2,(yr[0]+yr[1])/2], $
                              IMAGE_SIZE=[txr[1],tyr[1]], $
                              XRANGE=xrange, $
                              YRANGE=yrange
                               
    self.setProperty,XRANGE=xrange,YRANGE=yrange
  endif
  
    
  if (ISA(scale_center)) then begin
    if n_elements(scale_center) ne 2 then MESSAGE, 'SCALE_CENTER must be a two element array.'
    
    self.__obj__._getDataDimensions,txr,tyr
    self.getProperty, SCALE_FACTOR=scale_factor
                               
    oTool.ApplyScaleFactor, SCALE_FACTOR=scale_factor, $
                              SCALE_CENTER=scale_center, $
                              IMAGE_SIZE=[txr[1],tyr[1]], $
                              XRANGE=xrange, $
                              YRANGE=yrange
                               
    self.setProperty,XRANGE=xrange,YRANGE=yrange
  endif
  
  if (ISA(eye) || ISA(zclip)) then begin
    oWin = oTool.GetCurrentWindow()
    oView = ISA(oWin) ? oWin.GetCurrentView() : OBJ_NEW()
    oLayer = ISA(oView) ? oView.GetCurrentLayer() : OBJ_NEW()
    if ISA(oLayer) then begin
      oLayer.SetProperty, EYE=eye, ZCLIP=zclip
      oTool->RefreshCurrentWindow
    endif
  endif

  if (ISA(extra) && self.__obj__ ne self) then begin
    self.__obj__->SetProperty, _EXTRA=extra
    oTool = self.__obj__->GetTool()
    if (OBJ_VALID(oTool)) then begin
      tags = TAG_NAMES(extra)
      id = self.__obj__->GetfullIdentifier()
      idTool = otool->getFullIdentifier()
      for i=0,N_ELEMENTS(extra)-1 do begin
        ; Do not send notification for UVALUE changes, because it might be
        ; expensive to compute the value, and no one should care anyway.
        if (tags[i] eq 'UVALUE') then continue
        oTool->DoOnNotify, id, "SETPROPERTY", tags[i]
        if (ISA(extra.(i), 'STRUCT')) then continue
        if (id eq '/') then continue
        propValue = strtrim(string(FIX((extra.(i))[0], TYPE=7, /PRINT)),2)
        ; Send a maximum of 256 elements of any array. That should be plenty.
        jmax = n_elements(extra.(i)) < 256
        for j=1,jmax-1  do begin 
          propValue = propValue+','+ strtrim(string(FIX((extra.(i))[j], TYPE=7, /PRINT)),2)
        endfor 
        void=IDLNotify('IDLPropertyChanged', idTool, $
          strtrim(string(tags[i]),2)+':'+propValue)
      endfor
      oTool->RefreshCurrentWindow
    endif
  endif

  if (ISA(wasDisabled) && ~wasDisabled) then $
    oTool->EnableUpdates
end


;---------------------------------------------------------------------------
pro Graphic::GetProperty, $
  UNDOCUMENTED=undocumented, $  ; allow undocumented properties
  _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  if (~ISA(self.__obj__)) then return
  
  if (ISA(ex) && ~KEYWORD_SET(undocumented)) then $
    self->VerifyProperty, ex

  self._GetProperty, _EXTRA=ex
end


;---------------------------------------------------------------------------
pro Graphic::SetProperty, $
  UNDOCUMENTED=undocumented, $  ; allow undocumented properties
  _EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  if (~ISA(self.__obj__)) then return

  if (N_TAGS(ex) gt 0 && ~KEYWORD_SET(undocumented)) then $
    self->VerifyProperty, TAG_NAMES(ex)

  self._SetProperty, _EXTRA=ex
end


;---------------------------------------------------------------------------
; Note: CT, VIS, April 2011: Currently undocumented.
;
pro Graphic::SetPropertyAttribute, property, _EXTRA=ex
  compile_opt idl2, hidden
@graphic_error
  if (ISA(self.__obj__)) then begin
    self.__obj__->IDLitComponent::SetPropertyAttribute, property, _EXTRA=ex
  endif
end


;---------------------------------------------------------------------------
function Graphic::GetFullIdentifier
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    return, (self.__obj__)->GetFullIdentifier()
  endif else if (ISA(self, 'IDLitComponent')) then begin
    return, self->IDLitComponent::GetFullIdentifier()
  endif
  return, ''

end


;---------------------------------------------------------------------------
function Graphic::GetTool
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__, 'IDLitIMessaging')) then $
    return, (self.__obj__)->IDLitIMessaging::GetTool()

  return, OBJ_NEW()

end

;---------------------------------------------------------------------------
pro Graphic::Translate, X, Y, Z, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then $
    iTranslate, self->GetFullIdentifier(), X, Y, Z, _EXTRA=_extra
  
end

;---------------------------------------------------------------------------
pro Graphic::Rotate, degrees, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then $
    iRotate, self->GetFullIdentifier(), degrees, _EXTRA=_extra
  
end

;---------------------------------------------------------------------------
pro Graphic::Scale, X, Y, Z, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then $
    iScale, self->GetFullIdentifier(), X, Y, Z, _EXTRA=_extra
  
end

;---------------------------------------------------------------------------
pro Graphic::Order, orderIn, BRING_TO_FRONT=bringToFront, $
                             BRING_FORWARD=bringForward, $
                             SEND_TO_BACK=sendToBack, $
                             SEND_BACKWARD=sendBackward, $
                             _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    ; Determine order
    order = 'Bring to Front'
    if (N_ELEMENTS(orderIn) eq 1) then begin
      orderUP = STRUPCASE(orderIn)
      if (STRPOS(orderUP, 'BRING') ne -1) then $
        order = 'Bring Forward'
      if (STRPOS(orderUP, 'FORWARD') ne -1) then $
        order = 'Bring Forward'
      if (STRPOS(orderUP, 'FRONT') ne -1) then $
        order = 'Bring to Front'
      if (STRPOS(orderUP, 'SEND') ne -1) then $
        order = 'Send Backward'
      if (STRPOS(orderUP, 'BACK') ne -1) then $
        order = 'Send to Back'
      if (STRPOS(orderUP, 'BACKWARD') ne -1) then $
        order = 'Send Backward'
    endif
    if (KEYWORD_SET(sendBackward)) then $
      order = 'Send Backward'
    if (KEYWORD_SET(bringForward)) then $
      order = 'Bring Forward'
    if (KEYWORD_SET(sendToBack)) then $
      order = 'Send to Back'
    if (KEYWORD_SET(bringToFront)) then $
      order = 'Bring to Front'

    oTool = self.__obj__->GetTool()

    oTool->DisableUpdates, PREVIOUSLY_DISABLED=wasDisabled
    
    ; Call the Order action
    oDesc = oTool->GetOperations(IDENTIFIER='Edit/Order/Order')
    oOrder = oDesc->GetObjectInstance()
    oCmd = oOrder->DoAction(oTool, order, TARGET=self.__obj__)
    
    if (~wasDisabled) then $
      oTool->EnableUpdates

    if (ISA(oCmd)) then $
      oTool->_TransactCommand, oCmd
      
  endif
  
end


;---------------------------------------------------------------------------
pro Graphic::Refresh, DISABLE=disable
  compile_opt idl2, hidden

  if (ISA(self.__obj__)) then begin
    oTool = self.__obj__->GetTool()
    if (ISA(oTool)) then begin
      if (KEYWORD_SET(disable)) then begin
        oTool->DisableUpdates
      endif else begin
        oTool->DisableUpdates, PREVIOUSLY_DISABLED=wasDisabled
        oTool->EnableUpdates
        ; If we weren't disabled before, then the EnableUpdates will not
        ; actually do a re-draw. So force the re-draw.
        if (~wasDisabled) then begin
          oWin = oTool->GetCurrentWindow()
          if (ISA(oWin)) then oWin->Draw          
        endif
      endelse
    endif
  endif
end


;---------------------------------------------------------------------------
function Graphic::ConvertCoord, X, Y, Z, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    case n_params() of
    1: return, (self.__obj__)->ConvertCoord(X, _EXTRA=_extra)    
    2: return, (self.__obj__)->ConvertCoord(X, Y, _EXTRA=_extra)
    3: return, (self.__obj__)->ConvertCoord(X, Y, Z, _EXTRA=_extra)
    endcase
  endif else if (ISA(self, '_IDLitVisualization')) then begin
    return, self->_IDLitVisualization::ConvertCoord(X, Y, Z, _EXTRA=_extra)
  endif

  return, 0

end

;---------------------------------------------------------------------------
pro Graphic::PutData, arg1, arg2, arg3, arg4, arg5, arg6, arg7, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    case n_params() of
    1: (self.__obj__)->PutData, arg1, _EXTRA=_extra
    2: (self.__obj__)->PutData, arg1, arg2, _EXTRA=_extra
    3: (self.__obj__)->PutData, arg1, arg2, arg3, _EXTRA=_extra
    4: (self.__obj__)->PutData, arg1, arg2, arg3, arg4, _EXTRA=_extra
    5: (self.__obj__)->PutData, arg1, arg2, arg3, arg4, arg5, _EXTRA=_extra
    6: (self.__obj__)->PutData, arg1, arg2, arg3, arg4, arg5, arg6, _EXTRA=_extra
    7: (self.__obj__)->PutData, arg1, arg2, arg3, arg4, arg5, arg6, arg7, _EXTRA=_extra
    else: MESSAGE, 'Incorrect number of arguments.'
    endcase
  endif else if (ISA(self, '_IDLitVisualization')) then begin
;    return, self->_IDLitVisualization::ConvertCoord(X, Y, Z, _EXTRA=_extra)
  endif

end


;---------------------------------------------------------------------------
pro Graphic::SetData, arg1, arg2, arg3, arg4, arg5, arg6, arg7, $
                      _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  ; Validate _extra names
  if (N_ELEMENTS(_extra) ne 0) then begin
    validExtra = ['ORDER','CONNECTIVITY','MEAN_VALUES','CI_VALUES', $
                  'OUTLIER_VALUES','SUSPECTED_OUTLIER_VALUES']
    extraNames = TAG_NAMES(_extra)
    for i=0,N_ELEMENTS(extraNames)-1 do begin
      !NULL = where(extraNames[i] eq validExtra, cnt)
      if (cnt eq 0) then begin
        message, 'Keyword '+extraNames[i]+' not allowed in call to SetData'
        return
      endif 
    endfor
  endif

  if ( ~OBJ_HASMETHOD( self.__obj__, '_SETDATA' ) ) then $
    MESSAGE, 'Not supported for ' + OBJ_CLASS( self ) + ' graphics.'

  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    case n_params() of
    0: (self.__obj__)->_SetData, _EXTRA=_extra
    1: (self.__obj__)->_SetData, arg1, _EXTRA=_extra
    2: (self.__obj__)->_SetData, arg1, arg2, _EXTRA=_extra
    3: (self.__obj__)->_SetData, arg1, arg2, arg3, _EXTRA=_extra
    4: (self.__obj__)->_SetData, arg1, arg2, arg3, arg4, _EXTRA=_extra
    5: (self.__obj__)->_SetData, arg1, arg2, arg3, arg4, arg5, _EXTRA=_extra
    6: (self.__obj__)->_SetData, arg1, arg2, arg3, arg4, arg5, arg6, $
      _EXTRA=_extra
    7: (self.__obj__)->_SetData, arg1, arg2, arg3, arg4, arg5, arg6, arg7, $
      _EXTRA=_extra
    else: MESSAGE, 'Incorrect number of arguments.'
    endcase
  endif
  
end


;---------------------------------------------------------------------------
pro Graphic::GetData, arg1, arg2, arg3, arg4, arg5, arg6, arg7, _REF_EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if ( ~OBJ_HASMETHOD( self.__obj__, 'GETDATA' ) ) then $
    MESSAGE, 'Not supported for ' + OBJ_CLASS( self ) + ' graphics.'

  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    case n_params() of
    0: begin
      (self.__obj__)->GetData, _EXTRA=_extra
      end
    1: begin
      (self.__obj__)->GetData, arg1, _EXTRA=_extra
      arg1 = reform(arg1)
      end
    2: begin
      (self.__obj__)->GetData, arg1, arg2, _EXTRA=_extra
      arg1 = reform(arg1)
      arg2 = reform(arg2)
      end
    3: begin
      (self.__obj__)->GetData, arg1, arg2, arg3, _EXTRA=_extra
      arg1 = reform(arg1)
      arg2 = reform(arg2)
      arg3 = reform(arg3)
      end
    4: begin
      (self.__obj__)->GetData, arg1, arg2, arg3, arg4, _EXTRA=_extra
      arg1 = reform(arg1)
      arg2 = reform(arg2)
      arg3 = reform(arg3)
      arg4 = reform(arg4)
      end
    5: begin
      (self.__obj__)->GetData, arg1, arg2, arg3, arg4, arg5, _EXTRA=_extra
      arg1 = reform(arg1)
      arg2 = reform(arg2)
      arg3 = reform(arg3)
      arg4 = reform(arg4)
      arg5 = reform(arg5)
      end
    6: begin
      (self.__obj__)->GetData, arg1, arg2, arg3, arg4, arg5, arg6, _EXTRA=_extra
      arg1 = reform(arg1)
      arg2 = reform(arg2)
      arg3 = reform(arg3)
      arg4 = reform(arg4)
      arg5 = reform(arg5)
      arg6 = reform(arg6)
      end
    7: begin
      (self.__obj__)->GetData, arg1, arg2, arg3, arg4, arg5, arg6, arg7, _EXTRA=_extra
      arg1 = reform(arg1)
      arg2 = reform(arg2)
      arg3 = reform(arg3)
      arg4 = reform(arg4)
      arg5 = reform(arg5)
      arg6 = reform(arg6)
      arg7 = reform(arg7)
      end
    else: MESSAGE, 'Incorrect number of arguments.'
    endcase
  endif else begin
    ; window.GetData, arg1... Not Supported
    MESSAGE, 'Not supported for ' + OBJ_CLASS( self ) + ' graphics.'
  endelse

end


;---------------------------------------------------------------------------
function Graphic::GetValueAtLocation, arg1, arg2, arg3, _EXTRA=_extra
  compile_opt idl2, hidden

  @graphic_error

  if ( ~OBJ_HASMETHOD( self.__obj__, 'GETVALUEATLOCATION' ) ) then $
    MESSAGE, 'Not supported for ' + OBJ_CLASS( self ) + ' graphics.'
    
  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    case n_params() of
    1:begin
      if(~isa(arg1, /number) || ~finite(arg1)) then MESSAGE, 'Invalid points specified'
      value = (self.__obj__)->GetValueAtLocation(arg1, _STRICT_EXTRA=_extra)
      end
    2: begin
      if(~isa(arg1, /number) || ~finite(arg1) || ~isa(arg2, /number) || ~finite(arg2)) then $
        MESSAGE, 'Invalid points specified'
      value = (self.__obj__)->GetValueAtLocation(arg1, arg2, _STRICT_EXTRA=_extra)
      end
    3: begin
      if(~isa(arg1, /number) || ~finite(arg1) || ~isa(arg2, /number) || ~finite(arg2) || $
          ~isa(arg2, /number) || ~finite(arg2) ) then $
        MESSAGE, 'Invalid points specified'
      value = (self.__obj__)->GetValueAtLocation(arg1, arg2, arg3, _STRICT_EXTRA=_extra)
      end
    
    else: MESSAGE, 'Incorrect number of arguments.'
    endcase
  endif
    
  return, value  
    
end

;---------------------------------------------------------------------------
pro Graphic::Delete, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) && $
    ISA(self.__obj__, '_IDLitVisualization') && OBJ_VALID(self.__obj__) then begin
    oTool = self.__obj__->GetTool()
    oTool->DisableUpdates, PREVIOUSLY_DISABLED=previouslyDisabled
    
    ; some of what itools does in IDLitopEditDelete::_DeleteTargets

    ; cache the id for use later
    idToDelete = self.__obj__->GetFullIdentifier()
    
    ; de-select
    self.__obj__->Select, /UNSELECT, /SKIP_MACRO
    
    ;; Remove target from its parent.
    self.__obj__->GetProperty, _PARENT=oParent, PARENT=oRealParent
    if (OBJ_VALID(oParent)) then begin
      ; Retrieve the current position. Also sanity check to make
      ; sure object is indeed contained.
      if (oParent->IsContained(self.__obj__, position=position)) then begin
        oParent->Remove, self.__obj__
      endif
    endif

    ; Send a delete message
    self.__obj__->OnNotify, idToDelete, "DELETE", ''
    self.__obj__->DoOnNotify, idToDelete, 'DELETE', ''
    Obj_Destroy, self.__obj__

    ; Now destroy the graphic itself
    Obj_Destroy, self
    
    if (~previouslyDisabled) then begin
      oTool->EnableUpdates
      oTool->RefreshCurrentWindow
    endif

  endif

end


;---------------------------------------------------------------------------
pro Graphic::Select, ALL=all, CLEAR=clear, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    oTool = self.__obj__->GetTool()
    oTool->DisableUpdates, PREVIOUSLY_DISABLED=previouslyDisabled
    
    (self.__obj__)->Select, UNSELECT=clear, _EXTRA=_extra
    ; Select everything
    if (KEYWORD_SET(all) || KEYWORD_SET(clear)) then begin
      oVis = []
      ; Get all data spaces
      dsID = iGetID('DATA SPACE*', TOOL=oTool->GetFullIdentifier())
      for i=0,N_ELEMENTS(dsID)-1 do begin
        oDS = oTool->GetByIdentifier(dsID[i])
        if (OBJ_VALID(oDS)) then begin
          oDSVis = oDS->GetVisualizations()
          if (OBJ_VALID(oDSVis[0])) then $
            oVis = [oVis, oDSVis]
        endif
      endfor
      ; Get annotations
      annID = iGetID('ANNOTATION LAYER/*', TOOL=oTool->GetFullIdentifier())
      for i=0,N_ELEMENTS(annID)-1 do begin
        oAnn = oTool->GetByIdentifier(annID[i])
        if (ISA(oAnn, '_IDLitVisualization')) then $
          oVis = [oVis, oAnn]
      endfor
      for i=0,N_ELEMENTS(oVis)-1 do $
        oVis[i]->Select, ADD=KEYWORD_SET(all), UNSELECT=KEYWORD_SET(clear) 
    endif

    ; Make sure if items are programmatically selected, that our internal
    ; manipulator state is in sync with the current selection.
    oManip = oTool->GetByIdentifier('MANIPULATORS/SELECT')
    if (ISA(oManip, 'GraphicsManip')) then begin
      oWin = oTool->GetCurrentWindow()
      oManip->_UpdateSelectionCache, oWin
    endif
    
    if (~previouslyDisabled) then begin
      oTool->EnableUpdates
      oTool->RefreshCurrentWindow
    endif

    ; Make sure this is the current tool. This will also
    ; notify the workbench to bring the window to the front
    iSetCurrent, oTool->GetFullIdentifier(), /SHOW
  endif

end


;---------------------------------------------------------------------------
function Graphic::GetSelect, _EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  oGraphics = []
  if (ISA(self.__obj__) && self.__obj__ ne self) then begin
    oTool = self.__obj__->GetTool()
  endif else begin
    ; Window and Buffer objects are real objs, not just wrappers
    oTool = self->GetTool()
  endelse
  if (ISA(oTool)) then begin
    oSel = oTool->GetSelectedItems(count=nSel)
    for i=0,nSel-1 do begin
      oGraphics = [oGraphics, Graphic_GetGraphic(oSel[i])]
    endfor
    if (N_ELEMENTS(oGraphics) eq 1) then $
      oGraphics = oGraphics[0]
  endif

  return, ISA(oGraphics) ? oGraphics : !NULL
  
end


;-------------------------------------------------------------------------
;    Returns the graphics items at a specified location or within a box specified by dimensions
;
; :Params:
;    X - The X location to use for the hit test
;    Y - The Y location to use for the hit test
;
; :Keywords:
;    DIMENSIONS - A two element vector [X,Y] containing the dimensions
;       of a box centered on X, Y to use for the hit test.
FUNCTION Graphic::HitTest, $
  X, Y, $
  DIMENSIONS=dimensionsIn, $
  _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  if (ISA(dimensionsIn)) then begin
    if (N_ELEMENTS(dimensionsIn) ne 2) then $
      MESSAGE, 'DIMENSIONS must be a two-element vector.'
    dimensions = ABS(LONG(dimensionsIn)) > 1
  endif

  ; Get the tool and the window if necessary
  if ~ISA(self, 'GRAPHICSWIN') then begin
    oTool = self->GetTool()
    oWin = oTool->GetCurrentWindow()
  endif else oWin=self

  ; Do the hit test
  oVisualizations = oWin->DoHitTest(X, Y, DIMENSIONS=dimensions)
  oGraphics=[]
  foreach vis, oVisualizations do begin
    oGraphic = Graphic_GetGraphic(vis)
    if (obj_valid(oGraphic) && OBJ_CLASS(oGraphic) ne 'GRAPHIC') then $
      oGraphics=[oGraphics, oGraphic]
  endforeach

  return, ISA(oGraphics) ? oGraphics : OBJ_NEW() 
  
end


;---------------------------------------------------------------------------
function Graphic::_overloadPrint
  compile_opt idl2, hidden
@graphic_error

  result = OBJ_CLASS(self) + $
    ' <' + STRTRIM(OBJ_VALID(self,/GET_HEAP_ID),2) + '>'
  if (OBJ_ISA(self.__obj__, 'IDLitComponent')) then begin
    props = self->QueryProperty()
    n = N_ELEMENTS(props)
    if (n gt 0) then begin
      props = props[SORT(props)]
      result = [result, STRARR(n)]
      for i=0,n-1 do begin
        result[i+1] = STRING(props[i], FORMAT='("  ", A-25, " = ")')
        CATCH, iErr
        ; Quietly keep going if an error occurs.
        if (iErr ne 0) then begin
          CATCH, /CANCEL
          continue
        endif
        if (self.GetPropertyByIdentifier(props[i], value)) then begin
          sval = STRING(value, /PRINT)
          isMultiLine = N_ELEMENTS(sval) gt 1
          sval = STRTRIM(sval[0], 2)
          if (ISA(value, 'STRING')) then sval = "'" + sval
          result[i+1] += STRTRIM(sval,2)
          if isMultiLine then result[i+1] += ' ...'
          if (ISA(value, 'STRING')) then result[i+1] += "'"
        endif
      endfor
    endif
    ; Ensure that we print out 1 line per string.
    result = REFORM(result, 1, N_ELEMENTS(result), /OVERWRITE)
  endif

  return, result

end


;---------------------------------------------------------------------------
function Graphic::_overloadImpliedPrint, varname
  compile_opt idl2, hidden
  @graphic_error

  if (varname eq '<Expression>') then begin
    result = OBJ_CLASS(self) + $
      ' <' + STRTRIM(OBJ_VALID(self,/GET_HEAP_ID),2) + '>'
    return, result
  endif

  return, self->_overloadPrint()

end


;---------------------------------------------------------------------------
function Graphic::_overloadHelp, varname
  compile_opt idl2, hidden

  result = varname
  slen = STRLEN(varname)
  if (slen lt 15) then result += STRJOIN(REPLICATE(' ', 15 - slen))
  result += ' ' + OBJ_CLASS(self) + $
    ' <' + STRTRIM(OBJ_VALID(self,/GET_HEAP_ID),2) + '>'

  return, result

end


;---------------------------------------------------------------------------
function Graphic::_overloadBracketsRightSide, isRange, $
  i1, i2, i3, i4, i5, i6, i7, i8

  compile_opt idl2, hidden
@graphic_error

  ; If we have a string then find the object by identifier.
  if (ISA(i1, 'STRING') && ISA(self.__obj__)) then begin
    oTool = self.__obj__->GetTool()
    if (~OBJ_VALID(oTool)) then return, OBJ_NEW()

    if (ISA(self.__obj__, '_IDLitVisualization')) then $
      oDS = self.__obj__.GetDataSpace()

    myid = Isa(oDS) ? oDS->GetFullIdentifier() : $
      self.__obj__->GetFullIdentifier()

    result = !NULL

    for i=0,n_elements(i1)-1 do begin
      identifier = i1[i]

      ; If this is a relative identifier, then first try to find the
      ; object within my graphic's container.
      if (myid ne '' && STRMID(identifier,0,1) ne '/') then begin
;        id = iGetID(myid + '/*' + identifier)
        id = iGetID(identifier+'*', DATASPACE=myid, TOOL=oTool->GetFullIdentifier())
        isTitle = STRPOS(STRUPCASE(identifier), 'TITLE') ne -1
        if (N_ELEMENTS(id) eq 1 && id eq '' && ~isTitle) then begin
          ; Watch out - iGetID only returns the first partial match,
          ; which might not be correct. So throw a wildcard * on front.
          id = iGetID('*' + identifier, TOOL=oTool->GetFullIdentifier())
        endif
        ; If we retrieved more than 1 match, and we were not using a wildcard,
        ; then try to find an exact match. For example, this avoids a bug
        ; when trying to retrieve "Kansas" from a shapefile graphic,
        ; and you incorrectly get both "Arkansas" and "Kansas".
        if (N_ELEMENTS(id) gt 1 && STRPOS(identifier, '*') eq -1) then begin
          w = WHERE(STRPOS(id, '/' + STRUPCASE(identifier)) ge 0, /NULL)
          if (ISA(w)) then begin
            id = id[w[0]]
          endif else begin
            ; See which identifiers live in the same dataspace,
            ; and pick the first match.
            w = WHERE(STRCMP(id, myid, STRLEN(myid)), /NULL)
            id = ISA(w) ? id[w[0]] : id[0]
          endelse
        endif
      endif else begin
        id = iGetID(identifier, TOOL=oTool->GetFullIdentifier())
      endelse
  
      n = N_ELEMENTS(id)
      graphics = (n gt 1) ? OBJARR(n) : OBJ_NEW()
  
      for j=0,n-1 do begin
        if (id[j] eq '') then continue
        obj1 = oTool->GetByIdentifier(id[j])
        if (~ISA(obj1)) then continue

        ; If our own object is the one we found, just return ourself.
        ; Otherwise wrap the new object in a Graphic subclass.
        graphics[j] = (obj1 ne self.__obj__) ? Graphic_GetGraphic(obj1) : self
      endfor

      result = ISA(result) ? [result, graphics] : graphics
    endfor
    
    good = WHERE(OBJ_VALID(result), ngood)
    if (ngood gt 1) then begin
      result = result[good]
    endif else if (ngood eq 1) then begin
      if (ISA(result, /ARRAY)) then $
        result = result[good[0]]
    endif else begin
      result = OBJ_NEW()
    endelse
    return, result

  endif

  return, self->IDL_Object::_overloadBracketsRightSide( $
    isRange, i1, i2, i3, i4, i5, i6, i7, i8)

end


;---------------------------------------------------------------------------
; This should only get called if someone does a Foreach on a scalar
; Graphic object, say as the result of a call to graphic['*']. The user might
; want to just do a Foreach without having to check whether multiple items
; were returned, or a single Graphic.
;
function Graphic::_overloadForeach, value, index

  compile_opt idl2, hidden
  ON_ERROR, 2

  if (index eq !null) then index = 0 else index++

  ; If index is 0, just return ourself. Otherwise we're done.
  if index lt 1 then begin
    value = self
    return, 1
  endif
  return, 0

end

;---------------------------------------------------------------------------
; Returns a 3xMxN array of image data from the current graphics window
;
function Graphic::CopyWindow, WIDTH=width, $
                              HEIGHT=height, $
                              RESOLUTION=resolutionIn, $
                              _EXTRA=_extra

    compile_opt idl2, hidden

@graphic_error

  oTool = ISA(self.__obj__) ? self.__obj__->GetTool() : self->GetTool()
  if (~ISA(oTool)) then message, "Unable to retrieve graphics window."
  
  oWin = oTool->GetCurrentWindow( )

  ; get file writer service - that service will be used to
  ; retrieve the image data
  oWriteFile = oTool->GetService("WRITE_FILE")
  
  ; If RESOLUTION is set, it must be a long integer
  if (keyword_set(resolutionIn)) then $
      resolution = double(resolutionIn[0])
  
  ; If no resolution, width, or height have been specified, return
  ; an image the same size as the graphics window
  if (~keyword_set(resolutionIn) && $
      ~keyword_set(width) && $
      ~keyword_set(height)) then width = (oWin.DIMENSIONS)[0]
  
  ; Get image data
  return, oWriteFile.GetImage( oWin, WIDTH=width, HEIGHT=height, RESOLUTION=resolution, _EXTRA=_extra )
  
end


;---------------------------------------------------------------------------
; Transform map coordinates from longitude and latitude to
; Cartesian (x, y) coordinates.
;
function Graphic::MapForward, arg1, arg2, _REF_EXTRA=_extra

  compile_opt idl2, hidden

@graphic_error

  if (N_PARAMS() eq 0) then MESSAGE, 'Incorrect number of arguments.'

  oDataSpace = self.__obj__.GetDataSpace(/UNNORM)
  if (~ISA(oDataSpace)) then MESSAGE, 'Unable to retrieve map projection.'

  sMap = oDataSpace->GetProjection()
  if (N_TAGS(sMap) eq 0) then MESSAGE, 'Graphic does not contain a map projection.'

  if (N_PARAMS() eq 1) then begin
    result = MAP_PROJ_FORWARD(arg1, _EXTRA=_extra, MAP_STRUCTURE=sMap)
  endif else begin
    result = MAP_PROJ_FORWARD(arg1, arg2, _EXTRA=_extra, MAP_STRUCTURE=sMap)
  endelse
  return, result
end


;---------------------------------------------------------------------------
; Transform map coordinates from Cartesian (x, y) coordinates
; to longitude and latitude.
;
function Graphic::MapInverse, arg1, arg2, _REF_EXTRA=_extra

  compile_opt idl2, hidden

@graphic_error

  if (N_PARAMS() eq 0) then MESSAGE, 'Incorrect number of arguments.'

  oDataSpace = self.__obj__.GetDataSpace(/UNNORM)
  if (~ISA(oDataSpace)) then MESSAGE, 'Unable to retrieve map projection.'

  sMap = oDataSpace->GetProjection()
  if (N_TAGS(sMap) eq 0) then MESSAGE, 'Graphic does not contain a map projection.'

  if (N_PARAMS() eq 1) then begin
    result = MAP_PROJ_INVERSE(arg1, _EXTRA=_extra, MAP_STRUCTURE=sMap)
  endif else begin
    result = MAP_PROJ_INVERSE(arg1, arg2, _EXTRA=_extra, MAP_STRUCTURE=sMap)
  endelse
  return, result
end


;---------------------------------------------------------------------------
; Undocumented method:
;   Check that the input properties are valid for this class.
;   If any properties are not valid, throw an error.
;
pro Graphic::VerifyProperty, properties
  compile_opt idl2, hidden
@graphic_error
  myprops = self.QueryProperty(/ALL)
  ; Add in some properties that everyone should support.
  myprops = [myprops, 'IDENTIFIER', 'UVALUE']
  foreach property, properties do begin
    tot = total(strcmp(property,myprops,strlen(property)))
    if tot ne 1 then begin
      if tot gt 1 then begin
        if total(property eq myprops) ne 1 then begin
          MESSAGE, OBJ_CLASS(self) + ': Duplicate property: ' + property
        endif
      endif else begin
        MESSAGE, OBJ_CLASS(self) + ': Unknown property: ' + property
      endelse
    end
  endforeach
end


;---------------------------------------------------------------------------
; Purpose:
;   Returns a string array of all property names supported by this class.
;
; Keywords:
; 
; ALL: By default QueryProperty only returns the properties that should be
;   reported to the user when the object is printed out to the console.
;   Set this keyword to return all of the valid properties for the object
;   This is typically used by the Init, GetProperty, and SetProperty
;   methods to check for valid keyword names.
;
function Graphic::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ASPECT_RATIO','ASPECT_Z', $
    'BACKGROUND_COLOR', 'BACKGROUND_TRANSPARENCY', $
    'CLIP','CROSSHAIR','DEPTH_CUE','EYE', $
    'HIDE','NAME','POSITION', $
    'TITLE','WINDOW_TITLE','XRANGE','YRANGE','ZRANGE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    ax = Axis_Properties(/ALL)
    myprops = [myprops, $
      'AUTO_CROSSHAIR', $  ; undocumented
      'AXES', $
      'AXIS_STYLE', $
      'FONT_COLOR', 'FONT_NAME', 'FONT_SIZE', 'FONT_STYLE', $
      'GEOTIFF', $
      'MAPGRID', $
      'PARENT', 'PERSPECTIVE', $
      'NODATA', 'VERT_COLORS', $
      'WINDOW', $
      'X' + ax, $
      'Y' + ax, $
      'Z' + ax]
  endif

  return, myprops
end


;---------------------------------------------------------------------------
function Plot::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','COLOR', $
    'EQUATION', 'EQN_SAMPLES', 'EQN_USERDATA', $
    'ERRORBAR_COLOR', 'ERRORBAR_CAPSIZE', $
    'FILL_BACKGROUND','FILL_COLOR','FILL_LEVEL', $
    'FILL_TRANSPARENCY','HISTOGRAM','LINESTYLE', $
    'MAX_VALUE','MIN_VALUE','RGB_TABLE', 'STAIRSTEP','SYM_COLOR','SYM_FILLED', $
    'SYM_FILL_COLOR','SYM_INCREMENT','SYM_OBJECT','SYMBOL','SYM_SIZE','SYM_THICK', $
    'SYM_TRANSPARENCY','THICK','TRANSPARENCY','ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'DATA', $
      'MAP_PROJECTION', 'MAPPROJECTION', $
      'POLAR', 'XERROR', 'YERROR', 'ZERROR']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Barplot::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','BOTTOM_COLOR','BOTTOM_VALUES','COLOR', $
    'C_RANGE','FILL_COLOR','HISTOGRAM','HORIZONTAL','INDEX','LINESTYLE', $
    'NBARS','OUTLINE','THICK','TRANSPARENCY','WIDTH']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'MAP_PROJECTION', 'XERROR', 'YERROR', 'ZERROR', 'POLAR']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Boxplot::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','COLOR','FILL_COLOR','HORIZONTAL','LINESTYLE', $
    'THICK','TRANSPARENCY','WIDTH','BOX','ENDCAPS','MEDIAN','WHISKERS',$
    'NOTCH','LOWER_COLOR']
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops,'MEAN_VALUES','CI_VALUES','OUTLIER_VALUES',$
               'SUSPECTED_OUTLIER_VALUES','SYMBOL_MEANS',$
               'SYMBOL_OUTLIERS','SYMBOL_SUSPECTED_OUTLIERS']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Bubbleplot::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','COLOR','LINECOLOR','LINESTYLE', 'RGB_TABLE', $
    'LINETHICK','TRANSPARENCY','SHADED','LABELS','LABEL_POSITION', $
    'MAGNITUDE','SIZING','FILLED','BORDER','EXPONENT','LABEL_ALIGNMENT', $
    'LABEL_BASELINE','LABEL_FONT_COLOR','LABEL_FONT_NAME','LABEL_FONT_SIZE', $
    'LABEL_FONT_STYLE','LABEL_UPDIR','LABEL_VERTICAL_ALIGNMENT','MAX_VALUE']
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'FACE_SHOW','FACE_SMILE','FACE_EYES']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end

;---------------------------------------------------------------------------
function Plot3D::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','COLOR', $
;    'ERRORBAR_COLOR', 'ERRORBAR_CAPSIZE', $
    'LINESTYLE', $
    'SHADOW_COLOR','SYM_COLOR','SYM_FILLED', $
    'SYM_FILL_COLOR','SYM_INCREMENT','SYM_OBJECT','SYMBOL','SYM_SIZE','SYM_THICK', $
    'SYM_TRANSPARENCY','THICK','TRANSPARENCY', $
    'XY_SHADOW', 'XZ_SHADOW', 'YZ_SHADOW']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, $
      'DATA', 'MAP_PROJECTION', 'RGB_TABLE', $
      'XERROR', 'YERROR', 'ZERROR']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Image::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['GRID_UNITS','INTERPOLATE', $
    'MAX_VALUE','MIN_VALUE','RGB_TABLE','SCALE_FACTOR', 'SCALE_CENTER']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, $
      'IMAGE_DIMENSIONS', 'IMAGE_LOCATION', 'ORDER', $
      MapProjection_Properties(/ALL), $
      MapGrid_Properties(/ALL)]
  endif else begin
    ; Properties we want to display from mapprojection_properties or mapgrid_properties
    myprops = [myprops, 'MAP_PROJECTION','TRANSPARENCY','ZVALUE']
  endelse

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Surface::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','BOTTOM_COLOR','COLOR', $
    'EQUATION', 'EQN_SAMPLES', 'EQN_USERDATA', $
    'GRID_UNITS','HIDDEN_LINES', $
    'LINESTYLE','MAX_VALUE','MIN_VALUE','SHADING', $
    'SHOW_SKIRT','SKIRT','STYLE','TEXTURE_INTERP','THICK', $
    'TRANSPARENCY']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, $
      'MAP_PROJECTION', 'RGB_TABLE', 'TEXTURE_IMAGE']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Scatterplot::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS', $
    'FILL_BACKGROUND','MAGNITUDE','MAX_VALUE','MIN_VALUE','RGB_TABLE',$
    'SYM_COLOR','SYM_FILLED', $
    'SYM_FILL_COLOR','SYM_INCREMENT','SYM_OBJECT','SYMBOL','SYM_SIZE',$
    'SYM_THICK','SYM_TRANSPARENCY','TRANSPARENCY','ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'DATA', 'MAP_PROJECTION', 'MAPPROJECTION']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Scatterplot3d::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS', $
    'FILL_BACKGROUND','MAGNITUDE','MAX_VALUE','MIN_VALUE','RGB_TABLE',$
    'SYM_COLOR','SYM_FILLED', $
    'SYM_FILL_COLOR','SYM_INCREMENT','SYM_OBJECT','SYMBOL','SYM_SIZE',$
    'SYM_THICK','SYM_TRANSPARENCY','TRANSPARENCY','ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'DATA', 'MAP_PROJECTION', 'MAPPROJECTION']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Contour::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','C_COLOR','C_FILL_PATTERN','C_LABEL_INTERVAL', $
    'C_LABEL_NOGAPS', 'C_LABEL_OBJECTS', 'C_LABEL_SHOW', 'C_LINESTYLE', $
    'COLOR', 'C_THICK', 'C_USE_LABEL_COLOR', 'C_USE_LABEL_ORIENTATION', $
    'C_VALUE', 'DAYS_OF_WEEK', 'DOWNHILL', $
    'EQUATION', 'EQN_SAMPLES', 'EQN_USERDATA', $
    'FILL', 'GRID_UNITS', $
    'LABEL_COLOR', 'LABEL_FORMAT', 'LABEL_FRMTDATA', 'LABEL_UNITS', $
    'MAX_VALUE','MIN_VALUE','MONTHS','N_LEVELS','PLANAR', $
    'SHADE_RANGE','SHADING', $
    'TICKINTERVAL','TICKLEN', $
    'TRANSPARENCY','USE_TEXT_ALIGNMENTS','ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'MAP_PROJECTION','MAPPROJECTION', $
      'RGB_INDICES', 'RGB_TABLE']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Text::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ALIGNMENT','BASELINE','CLIP','COLOR', $
    'FILL_BACKGROUND', 'FILL_COLOR', $
    'FONT_COLOR', 'FONT_NAME', 'FONT_SIZE', 'FONT_STYLE', $
    'HIDE','NAME', 'ONGLASS', 'POSITION', 'STRING', $
    'TRANSPARENCY','UPDIR', 'VERTICAL_ALIGNMENT']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'ORIENTATION', 'WINDOW']

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end


;---------------------------------------------------------------------------
function Polygon::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','CLIP','COLOR', $
    'FILL_BACKGROUND', 'FILL_COLOR', 'FILL_PATTERN', 'FILL_TRANSPARENCY', $
    'HIDE','LINESTYLE','NAME','POSITION','THICK', $
    'TRANSPARENCY']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'CONNECTIVITY', $
      'RGB_TABLE', 'VERT_COLORS', 'WINDOW']
  endif

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end


;---------------------------------------------------------------------------
function Polyline::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','CLIP','COLOR', $
    'HIDE','LINESTYLE','NAME','POSITION','THICK', $
    'TRANSPARENCY']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'CONNECTIVITY', 'WINDOW']

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end

;---------------------------------------------------------------------------
function Arrow::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','ARROW_STYLE','CLIP','COLOR', $
    'FILL_BACKGROUND', 'FILL_COLOR', 'FILL_TRANSPARENCY', $
    'HEAD_ANGLE', 'HEAD_INDENT', 'HEAD_SIZE', 'HIDE',$
    'LINE_THICK','NAME', 'POSITION', 'THICK', 'TRANSPARENCY']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end

;---------------------------------------------------------------------------
function Ellipse::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','CLIP','COLOR', 'FILL_TRANSPARENCY', $
    'FILL_BACKGROUND', 'FILL_COLOR', $
    'HIDE', 'LINESTYLE', 'NAME', 'POSITION', 'THICK', $
    'TRANSPARENCY']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since Text is just an annotation.
  return, myprops
end


;---------------------------------------------------------------------------
function MapGrid::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  ; Since MapGrid_Properties() is also used by Image::QueryProperty, it doesn't
  ; include the "standard" properties like CLIP, FONT_NAME, etc. Otherwise there
  ; would be duplicates in Image. So we need to include these manually here.
  myprops = ['CLIP','FONT_NAME', 'FONT_SIZE', 'FONT_STYLE','HIDE','NAME', $
             MapGrid_Properties(ALL=all)]

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since this is just an annotation.
  return, myprops
end


;---------------------------------------------------------------------------
function Vector::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','ARROW_STYLE','ARROW_THICK', $
    'AUTO_COLOR', 'AUTO_RANGE', 'AUTO_SUBSAMPLE','COLOR', $
    'DATA_LOCATION','DIRECTION_CONVENTION', $
    'GRID_UNITS','HEAD_ANGLE','HEAD_INDENT','HEAD_PROPORTIONAL','HEAD_SIZE',$
    'LENGTH_SCALE','MAX_VALUE','MIN_VALUE', $
    'SUBSAMPLE_METHOD', $
    'SYM_COLOR','SYM_FILLED','SYM_FILL_COLOR','SYM_OBJECT','SYMBOL','SYM_SIZE', $
    'SYM_THICK','SYM_TRANSPARENCY','THICK', $
    'TRANSPARENCY', $
    'USE_DEFAULT_COLOR','VECTOR_STYLE', $
    'X_SUBSAMPLE','Y_SUBSAMPLE', 'ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    ; Need to add STREAMLINE props, since Streamline just calls Vector.
    myprops = [myprops, $
      'ARROW_COLOR','ARROW_OFFSET', 'ARROW_SIZE', $
      'ARROW_TRANSPARENCY', 'MAPPROJECTION', $
      'MAP_PROJECTION', 'RGB_TABLE', 'STREAMLINE','STREAMLINE_NSTEPS','STREAMLINE_STEPSIZE',$
      'X_STREAMPARTICLES','Y_STREAMPARTICLES', $
      'VECTOR_COLORS']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Streamline::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS', $
    'ARROW_COLOR','ARROW_OFFSET', 'ARROW_SIZE', 'ARROW_THICK', $
    'ARROW_TRANSPARENCY', $
    'AUTO_COLOR', 'AUTO_RANGE', 'COLOR', $
    'DIRECTION_CONVENTION', $
    'GRID_UNITS',$
    'MAP_PROJECTION', 'RGB_TABLE', 'STREAMLINE_NSTEPS','STREAMLINE_STEPSIZE', $
    'THICK', 'TRANSPARENCY', $
    'X_STREAMPARTICLES','Y_STREAMPARTICLES', 'ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
;  if (KEYWORD_SET(all)) then $
;    myprops = [myprops]

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function MapGridline::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','COLOR', $
    'FONT_NAME', 'FONT_SIZE', 'FONT_STYLE', $
    'HIDE','LABEL_ALIGN','LABEL_ANGLE','LABEL_COLOR', $
    'LABEL_FILL_BACKGROUND', 'LABEL_FILL_COLOR', 'LABEL_FORMAT', $
    'LABEL_POSITION', $
    'LABEL_SHOW', 'LABEL_VALIGN', $
    'LINESTYLE','LOCATION', 'NAME','THICK', $
    'TRANSPARENCY','ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end


;---------------------------------------------------------------------------
function Crosshair::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','COLOR', $
    'HIDE','INTERPOLATE','LINESTYLE','LOCATION','NAME','THICK', $
    'SNAP','STYLE','TRANSPARENCY']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end


;---------------------------------------------------------------------------
function Volume::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['AUTO_RENDER','BOUNDS', $
    'MIN_VALUE', 'MAX_VALUE', $
    'CLIP_PLANES', 'COLOR', $
    'COMPOSITE_FUNCTION','EXTENTS_TRANSPARENCY', $
    'HINTS','INTERPOLATE', $
    'RENDER_EXTENTS','RENDER_QUALITY', $
    'RENDER_STEP', $
    'TWO_SIDED', $
    'ZERO_OPACITY_SKIP']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, $
      'AMBIENT', 'LIGHTING_MODEL', 'ZBUFFER', $
      'OPACITY_TABLE0', 'OPACITY_TABLE1', 'RGB_TABLE0', 'RGB_TABLE1', $
      'VOLUME_DIMENSIONS', 'VOLUME_LOCATION']
  endif

  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function Text::GetTextDimensions, DESCENT=descent
  compile_opt idl2, hidden
  
  if (~ISA(self.__obj__)) then return, 0

  return, self.__obj__->_GetTextDimensions(DESCENT=descent)
end


;------------------------------------------------------------------------
pro Graphic__define
  compile_opt idl2, hidden
 
  void = {Graphic, inherits IDL_Object}

  void = {Plot, inherits Graphic}
  void = {Barplot, inherits Graphic}
  void = {Boxplot, inherits Graphic}
  void = {Bubbleplot, inherits Graphic}
  void = {Plot3D, inherits Graphic}
  void = {Image, inherits Graphic}
  void = {Surface, inherits Graphic}
  void = {Scatterplot, inherits Graphic}
  void = {Scatterplot3d, inherits Graphic}
  void = {Contour, inherits Graphic}
  void = {Vector, inherits Graphic}
  void = {Streamline, inherits Graphic}
  void = {Volume, inherits Graphic}
  void = {Text, inherits Graphic}
  void = {Polygon, inherits Graphic}
  void = {Polyline, inherits Graphic}
  void = {Ellipse, inherits Graphic}
  void = {MapGrid, inherits Graphic}
  void = {Arrow, inherits Graphic}
  void = {MapGridline, inherits Graphic}
  void = {Crosshair, inherits Graphic}

end
