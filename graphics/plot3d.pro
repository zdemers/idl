;+
; :Description:
;    Create a 3D plot.
;
; :Params:
;    X
;    Y
;    Z
;    Format
;
;-
function Plot3d, x, y, z, format, DEBUG=debug, $
                                  COLOR=color, $
                                  LINESTYLE=linestyle, $
                                  SYMBOL=symbol, $
                                  THICK=thick, $
                                  _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams lt 3 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'
  
  if (hasTestKW) then begin
    ; Create some data.
    t = FINDGEN(4001)/100
    x = COS(t)*(1 + t/10)
    y = SIN(t)*(1 + t/10)
    z = SIN(2*t)
    if (~ISA(color)) then begin
      format = 'b'
    endif
  endif

  switch (nparams) of
  4: if ~ISA(format, 'STRING') then MESSAGE, 'Format argument must be a string.'
  3: if ~ISA(z, /ARRAY) then MESSAGE, 'Z argument must be an array.'
  2: if ~ISA(y, /ARRAY) then MESSAGE, 'Y argument must be an array.'
  1: if ~ISA(x, /ARRAY) then MESSAGE, 'X argument must be an array.'
  endswitch
  
  if (isa(format, 'STRING')) then begin
    style = format
    nparams--
  endif
   
  
  if (n_elements(style)) then begin
    style_convert, style, COLOR=color, LINESTYLE=linestyle, SYMBOL=SYMBOL, THICK=thick
  endif
  
  mindim=size(x,/dimensions)-1
  if (~(size(x,/dimensions) eq size(y,/dimensions) eq size(z,/dimensions))) then begin
    mindim=min([size(x,/dimensions)-1,size(y,/dimensions)-1,size(z,/dimensions)-1])
  endif
  
  Graphic, 'Plot', x[0:mindim], y[0:mindim], z[0:mindim], $
    ERROR_CLASS='Plot3d', $
    COLOR=color, $
    LINESTYLE=linestyle, $
    SYMBOL=SYMBOL, THICK=thick, _EXTRA=ex, $
    GRAPHIC=graphic

  return, graphic
end
