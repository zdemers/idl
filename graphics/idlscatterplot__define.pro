; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlscatterplot__define.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLScatterplot
;
; PURPOSE:
;    The IDLScatterplot class is the component wrapper for IDLgrPlot
;
; CATEGORY:
;    Components
;
; MODIFICATION HISTORY:
;     Written by:   AGEH, 12/2012
;-

;----------------------------------------------------------------------------
; Lifecycle Methods
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAMES:
;   IDLScatterplot::Init
;
; PURPOSE:
;   Initialize this component
;
;   NOTE: Init methods are special lifecycle methods, and as such
;   cannot be called outside the context of object creation.  This
;   means that in most cases, you cannot call the Init method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Init method
;   from within the Init method of the subclass.
;
; CALLING SEQUENCE:
;
;   Obj = OBJ_NEW('IDLScatterplot', [X, Y])
;
; INPUTS:
;   X: Vector of X coordinates
;   Y: Vector of Y coordinates
;
; OUTPUTS:
;   This function method returns 1 on success, or 0 on failure.
;
;-
function IDLScatterplot::Init, _REF_EXTRA=_extra
  compile_opt idl2, hidden

  ; Initialize superclass
  if (~self->IDLitVisPlot::Init(/REGISTER_PROPERTIES, NAME='IDLScatterplot', $
                                ICON='plot', TYPE='IDLPLOT', $
                                DESCRIPTION='An IDLScatterplot Visualization', $
                                _EXTRA=_extra)) then return, 0

  self.magnitude = PTR_NEW(/ALLOCATE_HEAP)
  self._userMagnitude = PTR_NEW(/ALLOCATE_HEAP)
  
  self->IDLitVisPlot::SetProperty, RGB_TABLE=0, LINESTYLE=6
  
  ; Set any properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLScatterplot::SetProperty,  _EXTRA=_extra

  RETURN, 1 ; Success

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLScatterplot::Cleanup
;
; PURPOSE:
;   This procedure method performs all cleanup on the object.
;
;   NOTE: Cleanup methods are special lifecycle methods, and as such
;   cannot be called outside the context of object destruction.  This
;   means that in most cases, you cannot call the Cleanup method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Cleanup method
;   from within the Cleanup method of the subclass.
;
; CALLING SEQUENCE:
;   OBJ_DESTROY, Obj
;     or
;   Obj->[IDLScatterplot::]Cleanup
;
;-
pro IDLScatterplot::Cleanup, _EXTRA=_extra
  compile_opt idl2, hidden

  PTR_FREE, [self.magnitude, self._userMagnitude]
  
  ; Cleanup superclass
  self->IDLitVisPlot::Cleanup

end


;----------------------------------------------------------------------------
; Property Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLScatterplot::GetProperty
;
; PURPOSE:
;      This procedure method retrieves the
;      value of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLScatterplot::]GetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLScatterplot::Init followed by the word "Get"
;      can be retrieved using IDLScatterplot::GetProperty.
;
;-
pro IDLScatterplot::GetProperty, $
    MAGNITUDE=magnitude, $
    _REF_EXTRA=_extra

  compile_opt idl2, hidden

  if (ARG_PRESENT(magnitude)) then $
    magnitude = *self.magnitude

  ; get superclass properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisPlot::GetProperty, _EXTRA=_extra

end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLScatterplot::SetProperty
;
; PURPOSE:
;      This procedure method sets the value
;      of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLScatterplot::]SetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLScatterplot::Init followed by the word "Set"
;      can be set using IDLScatterplot::SetProperty.
;-
pro IDLScatterplot::SetProperty, $
    MAGNITUDE=magnitude, $
    RGB_TABLE=rgbTableIn, $
    _EXTRA=_extra

  compile_opt idl2, hidden

  if (N_ELEMENTS(rgbTableIn) gt 0) then begin
    rgbTable = rgbTableIn
    self->IDLitVisPlot::GetProperty, RGB_TABLE=rgbTableOld
    nColOld = N_ELEMENTS(rgbTableOld[0,*])
    self->IDLitVisPlot::SetProperty, RGB_TABLE=rgbTable
    self->IDLitVisPlot::GetProperty, RGB_TABLE=rgbTableNew
    nColNew = N_ELEMENTS(rgbTableNew[0,*])
    if ((nColOld ne nColNew) && (N_ELEMENTS(magnitude) eq 0) && $
        (N_ELEMENTS(*self._userMagnitude) ne 0)) then begin
      magnitude = *self._userMagnitude
    endif
  endif
  
  if (N_ELEMENTS(magnitude) gt 0) then begin
    self->IDLitVisPlot::GetData, y
    self->IDLitVisPlot::GetProperty, RGB_TABLE=rgbTable
    nCol = N_ELEMENTS(rgbTable[0,*])
    if (ARRAY_EQUAL(rgbTable, [-1,-1,-1])) then begin
      self->IDLitVisPlot::SetProperty, RGB_TABLE=0
      nCol = 256
    endif
    *self._userMagnitude = magnitude
    if (SIZE(magnitude, /TNAME) ne 'BYTE') then begin
      magnitude = BYTSCL(magnitude, TOP=nCol-1)
    endif
    *self.magnitude = magnitude[*]
    self->IDLitVisPlot::SetProperty, VERT_COLORS=magnitude
  endif

  if (N_ELEMENTS(_extra) gt 0) then begin
    self->IDLitVisPlot::SetProperty, _EXTRA=_extra
  endif

end


;----------------------------------------------------------------------------
;+
; IDLScatterplot__Define
;
; PURPOSE:
;      Defines the object structure for an IDLScatterplot object.
;-
;----------------------------------------------------------------------------
pro IDLScatterplot__Define
  compile_opt idl2, hidden

  struct = {IDLScatterplot, $
            inherits IDLitVisPlot, $
            _userMagnitude: PTR_NEW(), $
            magnitude: PTR_NEW() $
           }
             
end
