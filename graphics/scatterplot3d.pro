; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/scatterplot3d.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create IDL Scatterplot3d graphic.
;
; :Params:
;    arg1 : optional generic argument
;    arg2 : optional generic argument
;
; :Keywords:
;    _REF_EXTRA
;
;-

;-----------------------------------------------------------------------
; Helper routine to construct the parameter set.
; If no parameters are supplied then oParmSet will be undefined.
;
function iScatterplot3d_GetParmSet, oParmSet, parm1, parm2, parm3, $
                                    TEST=test, RGB_TABLE=rgbTableIn, $
                                    _EXTRA=_EXTRA
  compile_opt idl2, hidden
  
  nParams = N_Params()
  if (Keyword_Set(test)) then begin
    n = 1209
    parm1 = randomn(seed,n)
    parm2 = randomn(seed,n)
    parm3 = randomn(seed,n)
    min = ABS(MIN([parm1,parm2,parm3]))
    parm1 += min
    parm2 += min
    parm3 += min
    nParams = 4
  endif
  
  if (nParams ne 4) then return, ''
  
  oParmSet = OBJ_NEW('IDLitParameterSet', NAME='Plot parameters', $
                     ICON='plot', DESCRIPTION='Plot parameters')
  oParmSet->SetAutoDeleteMode, 1b
  
  ; Check for undefined variables.
  if (N_ELEMENTS(parm1) eq 0) then $
    MESSAGE, 'First argument is an undefined variable.'
  if (N_ELEMENTS(parm2) eq 0) then $
    MESSAGE, 'Second argument is an undefined variable.'
  if (N_ELEMENTS(parm3) eq 0) then $
    MESSAGE, 'Second argument is an undefined variable.'
    
  ; eliminate leading dimensions of 1
  parm1 = parm1[*]
  parm2 = parm2[*]
  parm3 = parm3[*]
  if ((N_ELEMENTS(parm1) eq N_ELEMENTS(parm2)) && $
      (N_ELEMENTS(parm1) eq N_ELEMENTS(parm3))) then begin
    oDataX = obj_new('IDLitDataIDLVector', parm1, NAME='X')
    oDataY = obj_new('IDLitDataIDLVector', parm2, NAME='Y')
    oDataZ = obj_new('IDLitDataIDLVector', parm3, NAME='Z')
    oParmSet->add, oDataX, PARAMETER_NAME='X'
    oParmSet->add, oDataY, PARAMETER_NAME='Y'
    oParmSet->add, oDataZ, PARAMETER_NAME='Z'
  endif else begin
    MESSAGE, 'Arguments have invalid dimensions'
  endelse
  
  ; Check for color table. If set, add that to the data container.
  if (N_ELEMENTS(rgbTableIn) gt 0) then begin
    rgbTable = rgbTableIn
    if (N_ELEMENTS(rgbTable) eq 1) then $
      rgbTable = Colortable(rgbTable[0])
    if (SIZE(rgbTable, /N_DIMENSIONS) EQ 2) then begin
      dim = SIZE(rgbTable, /DIMENSIONS)
      ;; Handle either 3xM or Mx3, but convert to 3xM to store.
      is3xM = dim[0] eq 3
      if ((is3xM || (dim[1] eq 3)) && (MAX(dim) le 256)) then begin
        tableEntries = is3xM ? rgbTable : TRANSPOSE(rgbTable)
        if (SIZE(tableEntries, /TYPE) ne 1) then $
          tableEntries=BYTSCL(tableEntries)
      endif
    endif
    if (N_ELEMENTS(tableEntries) gt 0) then begin
      ramp = BINDGEN(256)
      palette = TRANSPOSE([[ramp],[ramp],[ramp]])
      palette[*,0:N_Elements(tableEntries[0,*]) -1] = tableEntries
      oPalette = OBJ_NEW('IDLitDataIDLPalette', $
        palette, NAME='Palette')
      oParmSet->Add, oPalette, PARAMETER_NAME="PALETTE"
    endif else begin
      MESSAGE, "Incorrect dimensions for RGB_TABLE."
    endelse
  endif

  ; Set the appropriate visualization type.
  visType = 'PLOT'
  oParmSet->SetProperty, TYPE=visType
  
  return, visType
  
end

;-------------------------------------------------------------------------
; Needed because Graphic calls 'i'+graphicname
pro iScatterplot3d, parm1, parm2, parm3, $
    DEBUG=debug, $
    IDENTIFIER=identifier, $
    _REF_EXTRA=_extra
    
  compile_opt hidden, idl2
  
  ; Note: The error handler will clean up the oParmSet container.
  @idlit_itoolerror.pro
  
  visType = iScatterplot3d_GetParmSet(oParmSet, parm1, parm2, parm3, $
                                      _EXTRA=_extra)
  
  identifier = IDLitSys_CreateTool("Plot Tool", $
                                   INITIAL_DATA=oParmSet, $
                                   WINDOW_TITLE='IDL Scatter Plot', $
                                   VISUALIZATION_TYPE='Scatterplot3d', $
                                   _EXTRA=_extra)
    
end

;-------------------------------------------------------------------------
function Scatterplot3d, arg1, arg2, arg3, $
                      SYMBOL=symbolIn, $
                      LAYOUT=layoutIn, _REF_EXTRA=ex
  compile_opt idl2, hidden
  ON_ERROR, 2
  
  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if ((nparams ne 3) && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'
  
  if (nparams ne 0) then begin
    if (~ISA(arg3, /NUMBER)) then MESSAGE, 'Input must be an array.'
    if (~ISA(arg2, /NUMBER)) then MESSAGE, 'Input must be an array.'
    if (~ISA(arg1, /NUMBER)) then MESSAGE, 'Input must be an array.'
  endif
  
  symbol = N_ELEMENTS(symbolIn) ne 0 ? symbolIn[0] : 'Diamond'
  
  layout = N_ELEMENTS(layoutIn) eq 3 ? layoutIn : [1,1,1]
  ; default the xystyle to 0
  xstyle = 0
  ystyle = 0
  
  name = 'Scatterplot3d'
  case nparams of
    0: Graphic, name, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      SYMBOL=symbol, /AUTO_CROSSHAIR
    3: Graphic, name, arg1, arg2, arg3, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      SYMBOL=symbol, /AUTO_CROSSHAIR
  endcase

  oTool = graphic->GetTool()
  oTool->RefreshCurrentWindow
  
  return, graphic
  
end
