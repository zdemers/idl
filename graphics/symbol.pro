
;+
; :Description:
;    Create IDL Symbol to display in a graphic tool
;
; :Params:
;    X : X coordinate
;    Y : Y coordinate
;
; :Keywords:
;    TARGET
;    _EXTRA
;
;-
function symbol, x, y, symbolIn, $
                       DATA=data, $
                       NORMAL=normal, $
                       DEVICE=device, $
                       POSITION=position, $
                       RELATIVE=relative, $
                       TARGET=target, $
                       SYM_FONT_NAME=symFontName, $
                       SYM_TEXT=symtextIn, $
                       _REF_EXTRA=_extra 

  compile_opt idl2, hidden
@graphic_error
  
  ; Must have two parameters
  if n_params( ) lt 2 then $
    message, 'Incorrect number of arguments.'
  
  if (~ISA(symbolIn)) then symbolIn = 'None'

  if ~ISA( x, /NUMBER ) || ~ISA( y, /NUMBER ) then $
    message, 'Invalid input coordinates.'
  
  if isa( target, 'Graphic' ) then begin
    oTool = target.GetTool( )
  endif else begin
    ;; Get the system object and find all potential targets
    oSystem = _IDLitSys_GetSystem(/NO_CREATE)
    if ~isa( oSystem ) then $
      message, 'Unable to insert symbol.'
    oTool = oSystem._GetCurrentTool( )
  endelse
  
  if ~isa( oTool ) then $
    message, 'Internal Error: unable to insert symbol.'
  
  oWin = oTool->GetCurrentWindow()
  oView = Obj_Valid(oWin) ? oWin->GetCurrentView() : Obj_New()

  ; If nothing selected, retrieve the first visualization in the current view.
  if keyword_Set( data ) then begin
    oLayer = Obj_Valid(oView) ? oView->GetCurrentLayer() : Obj_New()
    oWorld = Obj_Valid(oLayer) ? oLayer->GetWorld() : Obj_New()
    oDS = Obj_Valid(oWorld) ? oWorld->GetCurrentDataSpace() : Obj_New()
  endif
    
  ; Check for unknown or illegal properties.
  if (N_ELEMENTS(_extra) gt 0) then $
    Graphic, _EXTRA=_extra, ERROR_CLASS='Symbol', /VERIFY_KEYWORDS

    ;; Convert the points
  toVisLayer = keyword_set( data )
  fullID = (iGetID(target, TOOL=oTool))[0]
  points = iConvertCoord(x, y, TO_ANNOTATION_DATA=(~toVisLayer), $
                         TO_DATA=toVisLayer, TARGET_IDENTIFIER=fullID, $
                         TOOL=oTool, DATA=data, NORMAL=normal, DEVICE=device, $
                         RELATIVE=relative, _EXTRA=_extra)
  
  odesc = oTool->GetAnnotation( 'Symbol' )
  oSymbol = odesc.GetObjectInstance( )
  
  ; Add annotation to proper layer in the window
  if keyword_Set( data ) then begin
    oDS->Add, oSymbol
    oSymbol->RemoveRotateHandle
  endif else begin
    oWin->Add, oSymbol, LAYER='ANNOTATION'
  endelse
  
  ; Set the properties of the symbol - set SYMBOL=0 here to avoid flashing
;  oSymbol->SetProperty, SYMBOL=0
  
  ; Wrap the symbol in a graphics object
  oGraphic = OBJ_NEW('Symbol', oSymbol)

  ; Backwards compatibility code.
  ; In IDL 8.1 we used the SYMBOL property to pass in either the symbol code,
  ; or the text for a text symbol, depending upon whether the SYM_FONT_NAME
  ; was set or not.
  ; In IDL 8.2 we no longer key off of the SYM_FONT_NAME but instead we
  ; added the SYM_TEXT property.
  if (ISA(symFontName)) then begin
    symtext = ISA(symtextIn) ? symtextIn : symbolIn
  endif else begin
    if (ISA(symtextIn)) then symtext = symtextIn
    symbol = symbolIn
  endelse

  ; Set the symbol and refresh
  oSymbol->SetProperty, DATA=points, $
    SYMBOL=symbol, SYM_TEXT=symtext, LABEL_STRING=labelStringIn, $
    SYM_FONT_NAME=symFontName, _EXTRA=_extra

  if (ISA(position)) then begin
    oGraphic->_SetProperty, POSITION=position, DEVICE=device
  endif

  return, oGraphic

end
