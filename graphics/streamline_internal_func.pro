; $Id: //depot/InDevelopment/scrums/IDL_Kraken/idl/idldir/lib/graphics/arrow.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;

;+
; :Description:
;    Internal routine to work around the limitations of resolve_all so a
;    resolve_all on the streamline procedure will not include new graphics 
;
;-
function streamline_internal_func, arg1, arg2, arg3, arg4, DEBUG=debug, $
                                                           LAYOUT=layoutIn, $
                                                           _REF_EXTRA=ex

  compile_opt idl2, hidden

  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams eq 0 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'

  switch (nparams) of
  4: if ~ISA(arg4, /ARRAY) then MESSAGE, 'Input must be an array.'
  3: if ~ISA(arg3, /ARRAY) then MESSAGE, 'Input must be an array.'
  2: if ~ISA(arg2, /ARRAY) then MESSAGE, 'Input must be an array.'
  1: if ~ISA(arg1, /ARRAY) then MESSAGE, 'Input must be an array.'
  endswitch

  layout = N_ELEMENTS(layoutIn) eq 3 ? layoutIn : [1,1,1]
  
  name = 'Vector'
  case nparams of
    0: Graphic, name, /STREAMLINE, _EXTRA=ex, $
      LAYOUT=layout, GRAPHIC=graphic, WINDOW_TITLE='Streamline'
    1: Graphic, name, arg1, /STREAMLINE, _EXTRA=ex, $
      LAYOUT=layout, GRAPHIC=graphic, WINDOW_TITLE='Streamline'
    2: Graphic, name, arg1, arg2, /STREAMLINE, _EXTRA=ex, $
      LAYOUT=layout, GRAPHIC=graphic, WINDOW_TITLE='Streamline'
    3: Graphic, name, arg1, arg2, arg3, /STREAMLINE, _EXTRA=ex, $\
      LAYOUT=layout, GRAPHIC=graphic, WINDOW_TITLE='Streamline'
    4: Graphic, name, arg1, arg2, arg3, arg4, /STREAMLINE, _EXTRA=ex, $
      LAYOUT=layout, GRAPHIC=graphic, WINDOW_TITLE='Streamline'
  endcase

  return, graphic
end