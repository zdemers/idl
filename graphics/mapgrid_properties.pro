; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/mapgrid_properties.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Return the MapGrid properties.
;
; :Params:
;    None
;
; :Keywords:
;    
;
;-
;-------------------------------------------------------------------------
function MapGrid_Properties, ALL=all

  compile_opt idl2, hidden

  myprops = ['ANTIALIAS','BOX_ANTIALIAS','BOX_AXES', $
    'BOX_COLOR','BOX_THICK','COLOR','FILL_COLOR', $
    'GRID_LONGITUDE', 'GRID_LATITUDE', $
    'HORIZON_COLOR', 'HORIZON_LINESTYLE', 'HORIZON_THICK', $
    'LABEL_ALIGN','LABEL_ANGLE','LABEL_COLOR', $
    'LABEL_FILL_BACKGROUND', 'LABEL_FILL_COLOR', $
    'LABEL_FORMAT', 'LABEL_POSITION', $
    'LABEL_SHOW', 'LABEL_VALIGN', $
    'LATITUDE_MAX', 'LATITUDE_MIN', 'LONGITUDE_MAX', 'LONGITUDE_MIN', $
    'LINESTYLE','THICK', $
    'TRANSPARENCY','ZVALUE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    myprops = [myprops, 'LATITUDES', 'LONGITUDES', 'MAPPROJECTION']
  endif

  return, myprops
end

