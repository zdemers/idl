; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/graphics_toolbar.pro#2 $
; Copyright (c) 2011-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;   GRAPHICS_TOOLBAR
;
; PURPOSE:
;   This function implements the compound widget for a Graphics toolbar.
;
;-


;-------------------------------------------------------------------------
function graphics_toolbar_getvalue, wToolbar

  compile_opt idl2, hidden

  if (~WIDGET_INFO(wToolbar, /VALID)) then return, ''
  wChild = WIDGET_INFO(wToolbar, /CHILD)
  WIDGET_CONTROL, wChild, GET_UVALUE=state
  if (~ISA(state)) then return, ''
  if (~WIDGET_INFO(state.wStatus, /VALID)) then return, ''
  WIDGET_CONTROL, state.wStatus, GET_VALUE=value
  return, value
end


;-------------------------------------------------------------------------
pro graphics_toolbar_setvalue, wToolbar, value

  compile_opt idl2, hidden

  if (~WIDGET_INFO(wToolbar, /VALID)) then return
  wChild = WIDGET_INFO(wToolbar, /CHILD)
  WIDGET_CONTROL, wChild, GET_UVALUE=state
  if (~ISA(state)) then return
  if (~WIDGET_INFO(state.wStatus, /VALID)) then return
  WIDGET_CONTROL, state.wStatus, SET_VALUE=STRING(value)
end


;-------------------------------------------------------------------------
pro graphics_toolbar_callback, wToolbar, strID, messageIn, userdata

  compile_opt idl2, hidden

  if (~WIDGET_INFO(wToolbar, /VALID)) then $
      return

  wChild = WIDGET_INFO(wToolbar, /CHILD)
  WIDGET_CONTROL, wChild, GET_UVALUE=state
  if (~ISA(state)) then return

  case STRUPCASE(messageIn) of

  'MESSAGE': begin
      value = (STRLEN(userdata) gt 0) ? userdata : ' '
      if (~WIDGET_INFO(state.wStatus, /VALID)) then break
      WIDGET_CONTROL, state.wStatus, GET_VALUE=oldvalue
      if (oldvalue ne value) then $
        WIDGET_CONTROL, state.wStatus, SET_VALUE=value
    end

  'SETPROPERTY': begin
      ; See if we have a resize notification.
      if (STRMID(strID, 6, 7, /REVERSE_OFFSET) ne '/WINDOW') then break
      oWin = state.oTool->GetCurrentWindow()
      if (~OBJ_VALID(oWin)) then break
      oWin->IDLitWindow::GetProperty, DIMENSIONS=dim
      if (~WIDGET_INFO(state.wStatus, /VALID)) then break
      WIDGET_CONTROL, state.wStatus, SCR_XSIZE=(dim[0] - state.buttonWidth) > 10
    end

  else: ; Do nothing

  endcase

end


;-------------------------------------------------------------------------
function graphics_toolbar, parent, oObj, $
  XSIZE=xsizeIn, $
  FRAME=frame, $
  NO_COPY=noCopy, $
  TAB_MODE=tab_mode, $
  UNAME=uname, $
  UVALUE=uvalue

  compile_opt idl2, hidden
@graphic_error

  oTool = oObj->GetTool()

  if (ISA(oObj, 'GRAPHICSWIN')) then begin
    oUI = oObj.UI
  endif else if (ISA(oObj, 'IDLitUI')) then begin
    oUI = oObj
  endif else begin
    MESSAGE, 'Object must be a GRAPHICSWIN or GRAPHICSUI.'
  endelse

  geomParent = WIDGET_INFO(parent, /GEOMETRY)
  xsize = ISA(xsizeIn) ? xsizeIn : geomParent.xsize - 4

  wToolbar = WIDGET_BASE(parent, /ROW, XPAD=0, YPAD=0, SPACE=2, $
    FUNC_GET_VALUE='graphics_toolbar_getvalue', $
    PRO_SET_VALUE='graphics_toolbar_setvalue', $
    FRAME=frame, $
    NO_COPY=noCopy, $
    TAB_MODE=tab_mode, $
    UNAME=uname, $
    UVALUE=uvalue)

  wButtons = WIDGET_BASE(wToolbar, /ROW, XPAD=0, YPAD=0, SPACE=0)
  wTools = CW_ITTOOLBAR(wButtons, oUI, 'Toolbar/File', TOOLBAR=0)
  wTools = CW_ITTOOLBAR(wButtons, oUI, 'Toolbar/Edit', /DROPDOWN, $
    VALUE='undoredo', /BITMAP, $
    TOOLTIP=IDLitLangCatQuery('Menu:Edit'), $
    TOOLBAR=0)
  wTools = CW_ITTOOLBAR(wButtons, oUI, 'Manipulators', /DROPDOWN, $
    VALUE='draw', /BITMAP, $
    TOOLTIP=IDLitLangCatQuery('Menu:Insert'), $
    TOOLBAR=0)

  geom = WIDGET_INFO(wButtons, /GEOMETRY)
  buttonWidth = geom.scr_xsize + 4

  segId = oTool->GetFullIdentifier() + '/STATUS_BAR/PROBE'
  wStatus = WIDGET_LABEL(wToolbar, VALUE=' ', UNAME=segId, /ALIGN_RIGHT, $
    SCR_XSIZE=(xsize - buttonWidth) > 1)

  ; Register for notification messages.
  idUIadaptor = oUI->RegisterToolBar(wToolbar, segId, $
     'graphics_toolbar_callback')
  oUI->AddOnNotifyObserver, idUIadaptor, segId
  ; Observe the window for resize notifications.
  oUI->AddOnNotifyObserver, idUIadaptor, oTool->GetFullIdentifier() + '/WINDOW'

  state = { $
    buttonWidth: buttonWidth, $
    wStatus: wStatus, $
    oTool: oTool $
    }
    
  ; This is the first child of the base toolbar.
  WIDGET_CONTROL, wButtons, SET_UVALUE=state

  return, wToolbar
end

