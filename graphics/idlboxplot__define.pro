; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlboxplot__define.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLBoxPlot
;
; PURPOSE:
;    The IDLBoxPlot class is the component wrapper for IDLgrPlot
;
; CATEGORY:
;    Components
;
; MODIFICATION HISTORY:
;     Written by:   AGEH, 10/2012
;-

;----------------------------------------------------------------------------
; Lifecycle Methods
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAMES:
;   IDLBoxPlot::Init
;
; PURPOSE:
;   Initialize this component
;
;   NOTE: Init methods are special lifecycle methods, and as such
;   cannot be called outside the context of object creation.  This
;   means that in most cases, you cannot call the Init method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Init method
;   from within the Init method of the subclass.
;
; CALLING SEQUENCE:
;
;   Obj = OBJ_NEW('IDLBoxPlot', [[X,] Y]])
;
; INPUTS:
;   X: Vector of X coordinates
;   Y: Vector of Y coordinates
;
; OUTPUTS:
;   This function method returns 1 on success, or 0 on failure.
;
;-
function IDLBoxPlot::Init, _REF_EXTRA=_extra
  compile_opt idl2, hidden

  ; Initialize superclass
  if (~self->IDLitVisPlot::Init(/REGISTER_PROPERTIES, NAME='IDLBoxPlot', $
                                ICON='plot', TYPE='IDLPLOT', $
                                DESCRIPTION='A IDLBoxPlot Visualization', $
                                _EXTRA=_extra)) then return, 0

  ; Defaults
  self.data = PTR_NEW(!VALUES.F_NAN)
  self.x_data = PTR_NEW(!VALUES.F_NAN)
  self.means = PTR_NEW(!VALUES.F_NAN)
  self.notch_values = PTR_NEW(!VALUES.F_NAN)
  self.outliers = PTR_NEW(!VALUES.F_NAN)
  self.suspected_outliers = PTR_NEW(!VALUES.F_NAN)
  self.width = PTR_NEW([0.4])
  self.fill_color = [0b,0b,0b]
  self.use_fill_color = 0b
  self.lower_color = [0b,0b,0b]
  self.use_lower_color = 0b
  self.color = [0b,0b,0b]
  self.outline_thick = 1
  self.outline_hide = 0b
  self.outline_style = 0b
  self.horizontal = 0b
  self.show_box = 1b
  self.show_endcaps = 1b
  self.show_median = 1b
  self.show_whiskers = 1b
  self.show_notch = 0b
  self.show_means = 1b
  self.show_outliers = 1b
  self.show_suspected_outliers = 1b
  self.notch_depth = 0.28
  self.symbol_outliers = OBJ_NEW('IDLitSymbol', PARENT=self)
  self.symbol_suspected_outliers = OBJ_NEW('IDLitSymbol', PARENT=self)

  self->SetAxesStyleRequest, 2 ; Request box style axes by default.

  ; Bar
  self._oFill = OBJ_NEW('IDLitVisPolygon', $
                       COLOR=self._fillColor, CLIP=0, $
                       FILL_COLOR=self._fillColor, $
                       TRANSPARENCY=self._fillTransparency, $
                       /HIDE, /PRIVATE, LINESTYLE=6, /TESSELLATE)
  ;; Add to the beginning so it is in the background.
  self->Add, self._oFill, POSITION=0
  ; Whiskers
  self._oLineWhisker = OBJ_NEW('IDLitVisPolyline', $
                               COLOR=self.color, CLIP=0, $
                               TRANSPARENCY=self._fillTransparency, $
                               /HIDE, /PRIVATE)
  self->Add, self._oLineWhisker, POSITION=1
  ; Box
  self._oLineBox = OBJ_NEW('IDLitVisPolyline', $
                           COLOR=self.color, CLIP=0, $
                           TRANSPARENCY=self._fillTransparency, $
                           /HIDE, /PRIVATE)
  self->Add, self._oLineBox, POSITION=1

  ; Means plot
  self._oMeansPlot = OBJ_NEW('IDLitVisPolyline', /HIDE, /PRIVATE, LINESTYLE=6, $
    SYMBOL='*', CLIP=0)
  self.symbol_means = OBJ_NEW('Symbol', self._oMeansPlot->GetSymbol())
  self->Add, self._oMeansPlot, POSITION=1

  ; Outlier plot
  self._oOutliersPlot = OBJ_NEW('IDLitVisPolyline', /HIDE, /PRIVATE, $
    LINESTYLE=6, SYMBOL='Star', CLIP=0)
  self.symbol_outliers = OBJ_NEW('Symbol', self._oOutliersPlot->GetSymbol())
  self->Add, self._oOutliersPlot, POSITION=1

  ; Suspected outlier plot
  self._oSuspectedOutliersPlot = OBJ_NEW('IDLitVisPolyline', /HIDE, /PRIVATE, $
    LINESTYLE=6, SYMBOL='o', CLIP=0)
  self.symbol_suspected_outliers = $
    OBJ_NEW('Symbol', self._oSuspectedOutliersPlot->GetSymbol())
  self->Add, self._oSuspectedOutliersPlot, POSITION=1
  
  ; Register all properties and set property attributes
  self->IDLBoxPlot::_RegisterProperties

  ; Set any properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLBoxPlot::SetProperty,  _EXTRA=_extra

  self->RemoveAggregate, self._oSymbol
  
  RETURN, 1 ; Success

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLBoxPlot::Cleanup
;
; PURPOSE:
;   This procedure method performs all cleanup on the object.
;
;   NOTE: Cleanup methods are special lifecycle methods, and as such
;   cannot be called outside the context of object destruction.  This
;   means that in most cases, you cannot call the Cleanup method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Cleanup method
;   from within the Cleanup method of the subclass.
;
; CALLING SEQUENCE:
;   OBJ_DESTROY, Obj
;     or
;   Obj->[IDLBoxPlot::]Cleanup
;
;-
pro IDLBoxPlot::Cleanup, _EXTRA=_extra
  compile_opt idl2, hidden

  OBJ_DESTROY, [self._oFill, self._oLineBox, self._oLineWhisker, $
                self.symbol_means, self.symbol_outliers, $
                self.symbol_suspected_outliers, self._oMeansPlot, $
                self._oOutliersPlot, self._oSuspectedOutliersPlot]
  PTR_FREE, [self.data, self.x_data, self.outliers, self.suspected_outliers, $
             self.means, self.width]

  ; Cleanup superclass
  self->IDLitVisPlot::Cleanup

end


;----------------------------------------------------------------------------
pro IDLBoxPlot::_RegisterParameters
  compile_opt idl2, hidden
  
  self->RegisterParameter, 'Y', DESCRIPTION='Barplot Values', $
    /INPUT, TYPES=['IDLARRAY2D'], /OPTARGET
    
  self->RegisterParameter, 'X', DESCRIPTION='Barplot Locations', $
    /INPUT, TYPES=['IDLVECTOR'], /OPTARGET

  self->IDLitVisPlot::_RegisterParameters
  
end
  
  
;----------------------------------------------------------------------------
; IDLBoxPlot::_RegisterProperties
;
; Purpose:
;   This procedure method registers properties associated with this class.
;
; Calling sequence:
;   oObj->[IDLBoxPlot::]_RegisterProperties
;
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLBoxPlot::_RegisterProperties, $
    UPDATE_FROM_VERSION=updateFromVersion

  compile_opt idl2, hidden

  registerAll = ~KEYWORD_SET(updateFromVersion)

  if (registerAll) then begin
  endif

end


;----------------------------------------------------------------------------
; Property Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::GetProperty
;
; PURPOSE:
;      This procedure method retrieves the
;      value of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]GetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLBoxPlot::Init followed by the word "Get"
;      can be retrieved using IDLBoxPlot::GetProperty.
;
;-
pro IDLBoxPlot::GetProperty, $
    ANTIALIAS=antialias, $
    WIDTH=width, $
    COLOR=color, $
    FILL_COLOR=fill_color, $
    TRANSPARENCY=transparency, $
    LOWER_COLOR=lowerColor, $
    THICK=thick, $
    LINESTYLE=linestyle, $
    HORIZONTAL=horizontal, $
    BOX=box, $
    ENDCAPS=endcaps, $
    MEDIAN=median, $
    WHISKERS=whiskers, $
    NOTCH=notch, $
    INDENT_DEPTH=notchDepth, $
    MEAN_VALUES=meanValues, $
    CI_VALUES=ciValues, $
    OUTLIER_VALUES=outlierValues, $
    SUSPECTED_OUTLIER_VALUES=suspOutValues, $
    SYMBOL_MEANS=symbolMeans, $
    SYMBOL_OUTLIERS=symbolOutliers, $
    SYMBOL_SUSPECTED_OUTLIERS=symbolSuspectedOutliers, $
    _FILLED=filled, $
    _LOWER_FILLED=lowerFilled, $
    _REF_EXTRA=_extra

  compile_opt idl2, hidden

  if (ARG_PRESENT(antialias)) then $
    self._oLineBox->GetProperty, ANTIALIAS=antialias

  if (ARG_PRESENT(width)) then $
    width = *self.width

  if (ARG_PRESENT(color)) then $
    color = self.color

  if (ARG_PRESENT(transparency)) then $
    self._oFill->GetProperty, TRANSPARENCY=transparency

  if (ARG_PRESENT(fill_color)) then $
    fill_color = self.fill_color

  if (ARG_PRESENT(lowerColor)) then $
    lowerColor = self.lower_color

  if (ARG_PRESENT(filled)) then $
    filled = self.use_fill_color

  if (ARG_PRESENT(lowerFilled)) then $
    lowerFilled = self.use_lower_color

  if (ARG_PRESENT(thick)) then $
    thick = self.outline_thick

  if (ARG_PRESENT(linestyle)) then $
    linestyle = self.outline_style

  if (ARG_PRESENT(horizontal)) then $
    horizontal = self.horizontal

  if (ARG_PRESENT(box)) then $
    box = self.show_box

  if (ARG_PRESENT(endcaps)) then $
    endcaps = self.show_endcaps

  if (ARG_PRESENT(median)) then $
    median = self.show_median

  if (ARG_PRESENT(whiskers)) then $
    whiskers = self.show_whiskers

  if (ARG_PRESENT(notch)) then $
    notch = self.show_notch

  if (ARG_PRESENT(notchDepth)) then begin
    notchDepth = self.notch_depth
  endif

  if (ARG_PRESENT(meanValues)) then begin
    if (self.show_means && (MAX(FINITE(*self.means))) && $  
        (N_ELEMENTS(*self.means) eq N_ELEMENTS(*self.x_data))) then begin
      meanValues = *self.means
    endif else begin
      meanValues = 0
    endelse
  endif

  if (ARG_PRESENT(ciValues)) then begin
    if (self.show_notch && $
        (N_ELEMENTS(*self.x_data) eq N_ELEMENTS(*self.notch_values))) then begin 
      ciValues = *self.notch_values
    endif else begin
      ciValues = 0
    endelse
  endif

  if (ARG_PRESENT(outlierValues)) then begin
    if (self.show_outliers && (N_ELEMENTS(*self.outliers) gt 1)) then begin
      outlierValues = *self.outliers
    endif else begin
      outlierValues = 0
    endelse
  endif

  if (ARG_PRESENT(suspOutValues)) then begin
    if (self.show_suspected_outliers && $
        (N_ELEMENTS(*self.suspected_outliers) gt 1)) then begin
       suspOutValues = *self.suspected_outliers
    endif else begin
      suspOutValues = 0
    endelse
  endif

  if (ARG_PRESENT(symbolMeans)) then begin
    symbolMeans = self.symbol_means
  endif

  if (ARG_PRESENT(symbolOutliers)) then begin
    symbolOutliers = self.symbol_outliers
  endif

  if (ARG_PRESENT(symbolSuspectedOutliers)) then begin
    symbolSuspectedOutliers = self.symbol_suspected_outliers
  endif

  ; get superclass properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisPlot::GetProperty, _EXTRA=_extra

end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::SetProperty
;
; PURPOSE:
;      This procedure method sets the value
;      of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]SetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLBoxPlot::Init followed by the word "Set"
;      can be set using IDLBoxPlot::SetProperty.
;-
pro IDLBoxPlot::SetProperty, $
    ANTIALIAS=antialias, $
    WIDTH=width, $
    COLOR=color, $
    FILL_COLOR=fill_color, $
    TRANSPARENCY=transparency, $
    LOWER_COLOR=lowerColor, $
    THICK=thick, $
    LINESTYLE=linestyle, $
    HORIZONTAL=horizontal, $
    BOX=box, $
    ENDCAPS=endcaps, $
    MEDIAN=median, $
    WHISKERS=whiskers, $
    NOTCH=notch, $
    INDENT_DEPTH=notchDepth, $
    MEAN_VALUES=meanValues, $
    CI_VALUES=ciValues, $
    OUTLIER_VALUES=outlierValues, $
    SUSPECTED_OUTLIER_VALUES=suspOutValues, $
    SYMBOL_MEANS=symbolMeans, $
    SYMBOL_OUTLIERS=symbolOutliers, $
    SYMBOL_SUSPECTED_OUTLIERS=symbolSuspectedOutliers, $
    _FILLED=filled, $
    _LOWER_FILLED=lowerFilled, $
    _REF_EXTRA=_extra

  compile_opt idl2, hidden

  update = 0b
  
  if (N_ELEMENTS(antialias) gt 0) then begin
    self._oLineBox->SetProperty, ANTIALIAS=antialias
    self._oLineWhisker->SetProperty, ANTIALIAS=antialias
    self._oFill->SetProperty, ANTIALIAS=antialias
  endif

  if (N_ELEMENTS(width) gt 0) then begin
    oldWidth = *self.width
    *self.width = FLOAT(width) > 0.00001
    update = ~ARRAY_EQUAL(oldWidth, *self.width)
  endif
  
  if (N_ELEMENTS(color) gt 0) then begin
    self.color = self->_GetColor(color)
    self._oLineBox->SetProperty, COLOR=self.color
    self._oLineWhisker->SetProperty, COLOR=self.color
    update = 1b
  endif
  
  if (N_ELEMENTS(transparency) gt 0) then begin
    trans = transparency > 0 < 100
    self->IDLitVisPlot::SetProperty, FILL_TRANSPARENCY=trans
    self._oLineBox->SetProperty, TRANSPARENCY=trans
    self._oLineWhisker->SetProperty, TRANSPARENCY=trans
    update = 1b
  endif
  
  if (N_ELEMENTS(thick) ne 0) then begin
    self.outline_thick = thick > 0
    self._oLineBox->SetProperty, THICK=self.outline_thick
    self._oLineWhisker->SetProperty, THICK=self.outline_thick
  endif

  if (N_ELEMENTS(linestyle) ne 0) then begin
    self.outline_style = linestyle > 0
    self._oLineWhisker->SetProperty, LINESTYLE=self.outline_style
    update = 1
  endif

  if (N_ELEMENTS(box) ne 0) then begin
    self.show_box = KEYWORD_SET(box)
    update = 1
  endif

  if (N_ELEMENTS(endcaps) ne 0) then begin
    self.show_endcaps = KEYWORD_SET(endcaps)
    update = 1
  endif

  if (N_ELEMENTS(median) ne 0) then begin
    self.show_median = KEYWORD_SET(median)
    update = 1
  endif

  if (N_ELEMENTS(whiskers) ne 0) then begin
    self.show_whiskers = KEYWORD_SET(whiskers)
    update = 1
  endif

  if (N_ELEMENTS(notch) ne 0) then begin
    self.show_notch = KEYWORD_SET(notch)
    update = 1
  endif

  if (N_ELEMENTS(notchDepth) ne 0) then begin
    self.notch_depth = FLOAT(notchDepth[0]) > 0.0 < 1.0
    update = 1
  endif

  if (N_ELEMENTS(horizontal) eq 1) then begin
    oldHoriz = self.horizontal
    self.horizontal = KEYWORD_SET(horizontal)
    update = oldHoriz ne self.horizontal
    doRangeUpdate = 1b
    if (update) then begin
      oDataspace = self->GetDataSpace(/UNNORMALIZED)
      if (OBJ_VALID(oDataSpace)) then begin
        oDataSpace->GetProperty, XLOG=xLog, YLOG=yLog, $
          XRANGE=xRange, YRANGE=yRange
        if (xLog || yLog) then begin
          if (xLog) then xRange = 10d^xRange
          if (yLog) then yRange = 10d^yRange
          oDataSpace->SetProperty, XLOG=0, YLOG=0
          oDataSpace->SetProperty, XRANGE=yRange, YRANGE=xRange
          oDataSpace->SetProperty, XLOG=yLog, YLOG=xLog
          ; We've already done all the updates.
          update = 1
          doRangeUpdate = 0b
        endif
      endif
    endif
  endif

  if (N_ELEMENTS(fill_color) gt 0) then begin
    self.fill_color = self->_GetColor(fill_color)
    if (ISA(fill_color, /NUMBER) && (N_ELEMENTS(fill_color) eq 1)) then begin
      self.use_fill_color = 0b
      self._use_lower_color = self.use_lower_color
      self.use_lower_color = 0b
    endif else begin
      self.use_fill_color = 1b
      self.use_lower_color = self._use_lower_color
    endelse
    update = 1b
  endif
  
  if (N_ELEMENTS(lowercolor) gt 0) then begin
    self.lower_color = self->_GetColor(lowercolor)
    if (ISA(lowerColor, /NUMBER) && (N_ELEMENTS(lowerColor) eq 1)) then begin
      self.use_lower_color = 0b
    endif else begin
      self.use_lower_color = 1b
    endelse
    self._use_lower_color = self.use_lower_color
    update = 1b
  endif
  
  if (N_ELEMENTS(filled) gt 0) then begin
    self.use_fill_color = KEYWORD_SET(filled)
  end

  if (N_ELEMENTS(lowerFilled) gt 0) then begin
    self.use_lower_color = KEYWORD_SET(lowerFilled)
  end

  if (N_ELEMENTS(meanValues) gt 0) then begin
    *self.means = meanValues[*]
    update = 1b
  endif
  
  if (N_ELEMENTS(ciValues) gt 0) then begin
    *self.notch_values = ciValues[*]
    update = 1b
  endif
  
  if (N_ELEMENTS(outlierValues) gt 0) then begin
    dims = size(outlierValues, /DIMENSIONS)
    !NULL = where(dims eq 2, cnt)
    if ((cnt ne 0) && (N_ELEMENTS(dims) eq 2)) then begin
      *self.outliers = (dims[0] eq 2) ? outlierValues : transpose(outlierValues)
      self.show_outliers = 1b
    endif else begin
      self.show_outliers = 0b
    endelse
    update = 1b
  endif
  
  if (N_ELEMENTS(suspOutValues) gt 0) then begin
    dims = size(suspOutValues, /DIMENSIONS)
    !NULL = where(dims eq 2, cnt)
    if ((cnt ne 0) && (N_ELEMENTS(dims) eq 2)) then begin
      *self.suspected_outliers = $
        (dims[0] eq 2) ? suspOutValues : transpose(suspOutValues)
      self.show_suspected_outliers = 1b
    endif else begin
      self.show_suspected_outliers = 0b
    endelse
    update = 1b
  endif
  
  if (N_ELEMENTS(_extra) gt 0) then begin
    self->IDLitVisPlot::SetProperty, _EXTRA=_extra
    update = 1b
  endif

  if (update) then begin
    if (N_ELEMENTS(horizontal) ne 0) then begin
      !NULL = self->GetXYZRange(xRange, yRange, zRange)
    endif else begin
      oDS = self->GetDataspace(/UNNORMALIZED)
      if (OBJ_VALID(oDS)) then $
        !NULL = oDS->_GetXYZAxisRange(xRange, yRange, zRange)
    endelse
    self->_UpdateLine
    self->_UpdateFill, xRange, yRange, zRange
    self->_UpdateSymbols
    self->_UpdateSelectionVisual
  endif
  
  if (ISA(doRangeUpdate) && doRangeUpdate) then begin
    oTool = self->GetTool()
    if (OBJ_VALID(oTool)) then begin
      oDesc = oTool->GetOperations(IDENTIFIER='Edit/DataspaceReset')
      oDSR = OBJ_VALID(oDesc) ? oDesc->GetObjectInstance() : OBJ_NEW()
      if (OBJ_VALID(oDSR)) then $
        !NULL = oDSR->DoAction(oTool, SELECTION=self)
    endif
  endif
  
end


;----------------------------------------------------------------------------
; IIDLDataObserver Interface
;----------------------------------------------------------------------------
;; IDLBoxPlot::OnDataDisconnect
;;
;; Purpose:
;;   This is called by the framework when a data item has disconnected
;;   from a parameter on the plot.
;;
;; Parameters:
;;   ParmName   - The name of the parameter that was disconnected.
;;
PRO IDLBoxPlot::OnDataDisconnect, ParmName
  compile_opt hidden, idl2
  
  ;; Just check the name and perform the desired action
  case ParmName of
    'X': begin
      ; Replace X values with indgen
      self._oFill->GetProperty, data=data
      szDims = size(data,/dimensions)
      data[0,*] = indgen(szDims[1])
      self._oFill->SetProperty, data=data
      self->_UpdateSelectionVisual
    end
    'Y': begin
      ; Set dummy data and hide bar
      self._oFill->SetProperty, data=[[0,0,0],[1,0,0],[0,1,0]]
      self->_UpdateSelectionVisual
      self._oFill->SetProperty, /HIDE
      self._oLineBox->SetProperty, /HIDE
      self._oLineWhisker->SetProperty, /HIDE
    end
  
    else:
  endcase

  ; Since we are changing a bunch of attributes, notify
  ; our observers in case the prop sheet is visible.
  self->DoOnNotify, self->GetFullIdentifier(), 'SETPROPERTY', ''

end


;----------------------------------------------------------------------------
; METHODNAME:
;    IDLBoxPlot::OnDataChangeUpdate
;
; PURPOSE:
;    This procedure method is called by a Subject via a Notifier when
;    its data has changed.  This method obtains the data from the
;    subject and updates the internal IDLgrPlot object.
;
; CALLING SEQUENCE:
;
;    Obj->[IDLBoxPlot::]OnDataChangeUpdate, oSubject, parmName
;
; INPUTS:
;    oSubject: The Subject object in the Subject-Observer relationship.
;    This object (the plot) is the observer, so it uses the
;    IIDLDataSource interface to get the data from the subject.
;    Then it puts the data in the IDLgrPlot object.
;
;    parmName: The name of the registered parameter.
;
; KEYWORDS:
;   NO_UPDATE: Undocumented keyword to suppress updates when adding
;       multiple data objects within a parameter set.
;
pro IDLBoxPlot::OnDataChangeUpdate, oSubject, parmName, $
    NO_UPDATE=noUpdate
    
  compile_opt idl2, hidden
  
  case strupcase(parmName) of
    '<PARAMETER SET>':begin
      ;; Get our data
      position = oSubject->Get(/ALL, count=nCount, NAME=name)
      for i=0, nCount-1 do begin
        if (name[i] eq '') then $
          continue
        oData = (oSubject->GetByName(name[i]))[0]
        if (~OBJ_VALID(oData)) then $
          continue
        if (oData->GetData(data, NAN=nan) le 0) then $
          continue
        
        case name[i] of
      
          'Y': begin
            self->IDLBoxPlot::OnDataChangeUpdate, oData, 'Y', /NO_UPDATE
          
            oData = oSubject->GetByName('X')
            if (OBJ_VALID(oData)) then begin
              self->IDLBoxPlot::OnDataChangeUpdate, oData, 'X', /NO_UPDATE
            endif
          
            self->_UpdateLine
            self->_UpdateFill
            self->_UpdateSymbols
          end
        
          'X':  ; X is handled in the Y branch to control order
        
          ; Pass all other parameters on to ourself.
          else: self->IDLBoxPlot::OnDataChangeUpdate, oData, name[i]
        
        endcase
      
      endfor
    end
  
    'X': begin
      if (~oSubject->GetData(data)) then $
        break
      *self.x_data = data
      if (~KEYWORD_SET(noUpdate)) then begin
        ;; Call OnDataChangeUpdate to update the visual stuff
        if (self->GetXYZRange(xRange, yRange, zRange)) then $
          self->OnDataRangeChange, self, xRange, yRange, zRange
      endif
    end
  
    'Y': begin
      if (~oSubject->GetData(data)) then $
        break
      *self.data = data
      if (~KEYWORD_SET(noUpdate)) then begin
        ;; Call OnDataChangeUpdate to update the visual stuff
        if (self->GetXYZRange(xRange, yRange, zRange)) then $
          self->OnDataRangeChange, self, xRange, yRange, zRange
      endif
    end
  
    else: ; ignore unknown parameters
  
  endcase

  self->_UpdateSelectionVisual

  ; Since we are changing a bunch of attributes, notify
  ; our observers in case the prop sheet is visible.
  self->DoOnNotify, self->GetFullIdentifier(), 'SETPROPERTY', ''

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::GetData
;
; PURPOSE:
;      This procedure method retrieves the
;      data values of the plot.
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]GetData, arg1 [, arg2]
;
; ARGUMENTS:
;      Up to two parameters may be supplied to retrieve the data. These
;      should be named variables which will receive the requested data.
;      The data arguments are retrieved based on the documented calling
;      sequence and the number of arguments supplied by the caller:
;      One parameter:
;        obj->GetData, Values
;      Two parameters:
;        obj->GetData, Locations, Values
;
; KEYWORD PARAMETERS:
;      The mean, outlier, or suspected outlier values can be retrieved by 
;      supplying the MEANS, OUTLIER, or SUSPECTED_OUTLIER keywords.
;
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot::GetData, arg1, arg2, $
                         _REF_EXTRA=_extra
  compile_opt idl2, hidden

  case n_params() of
    0:
    1: begin
        arg1 = *self.data
      end
    2: begin
        arg1 = *self.x_data
        arg2 = *self.data
      end
    else: MESSAGE, 'Incorrect number of arguments.'
  endcase
  
  ; get superclass properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->GetProperty, _EXTRA=_extra

end    


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::_SetData
;
; PURPOSE:
;      This procedure method sets the data values of the plot.
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]_SetData, arg1 [, arg2]
;
; ARGUMENTS:
;      Up to two parameters may be supplied to set the data.
;      One parameter:
;        obj->_SetData, Values
;      Two parameters:
;        obj->_SetData, Locations, Values
;
; KEYWORD PARAMETERS:
;      The mean, outlier, or suspected outlier values can be set by 
;      supplying the MEANS, OUTLIER, or SUSPECTED_OUTLIER keywords.
;
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot::_SetData, arg1, arg2, $
                          MEAN_VALUES=means, CI_VALUES=ciValues, $
                          OUTLIER_VALUES=outliers, $
                          SUSPECTED_OUTLIER_VALUES=suspectedOutliers, $
                          _EXTRA=_extra
  compile_opt idl2, hidden
  
  RESOLVE_ROUTINE, 'boxplot', /NO_RECOMPILE, /IS_FUNCTION

  if (N_ELEMENTS(means) ne 0) then begin
    self->SetProperty, MEAN_VALUES=means
  endif

  if (N_ELEMENTS(ciValues) ne 0) then begin
    self->SetProperty, CI_VALUES=ciValues
  endif

  if (N_ELEMENTS(outliers) ne 0) then begin
    self->SetProperty, OUTLIER_VALUES=outliers
  endif

  if (N_ELEMENTS(suspectedOutliers) ne 0) then begin
    self->SetProperty, SUSPECTED_OUTLIER_VALUES=suspectedOutliers
  endif
  
  case (N_Params()) of
    0: if (N_ELEMENTS(_extra) ne 0) then $
      !NULL = iBoxPlot_GetParmSet(oParmSet, _EXTRA=_extra)
    1: !NULL = iBoxPlot_GetParmSet(oParmSet, arg1, _EXTRA=_extra)
    2: !NULL = iBoxPlot_GetParmSet(oParmSet, arg1, arg2, _EXTRA=_extra)
  endcase
  if (ISA(oParmSet)) then begin
    oDataY = oParmSet->GetByName('Y')
    if (OBJ_VALID(oDataY)) then begin
      self->SetParameter, 'Y', oDataY
      oDataY->SetProperty, /AUTO_DELETE
    endif
    oDataX = oParmSet->GetByName('X')
    if (OBJ_VALID(oDataX)) then begin
       self->SetParameter, 'X', oDataX
      oDataX->SetProperty, /AUTO_DELETE
    endif
  
    ;; Notify of changed data
    self->OnDataChangeUpdate, oParmSet, '<PARAMETER SET>'
  
    ;; Clean up parameterset
    oParmSet->Remove, /ALL
    OBJ_DESTROY, oParmSet
  endif
  
  ; Send a notification message to update UI
  self->DoOnNotify, self->GetFullIdentifier(),"ADDITEMS", ''
  self->OnDataComplete, self

  oTool = self->GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->RefreshCurrentWindow
  
end

;----------------------------------------------------------------------------
function IDLBoxPlot::GetValueAtLocation, arg1, arg2, $
  _DATASPACE=_ds, _EXTRA=_extra
  
  compile_opt idl2, hidden

  if (N_PARAMS() lt 1) then MESSAGE, 'Incorrect number of arguments'

  ; Undocumented _DATASPACE keyword let's us know that we are being
  ; called with raw dataspace coordinates - if we are horizontal then
  ; we need to flip our X and Y.
  if (KEYWORD_SET(_ds) && self.horizontal) then begin
    value = self->IDLitVisPlot::GetValueAtLocation(arg2, arg1, _STRICT_EXTRA=_extra)
    value = value[[1,0]]
  endif else begin
    value = self->IDLitVisPlot::GetValueAtLocation(arg1, _STRICT_EXTRA=_extra)
  endelse

  return, value
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLitVisAxis::GetXYZRange
;
; PURPOSE:
;   This function method overrides the _IDLitVisualization::GetXYZRange
;   function, taking into the tick labels.
;
function IDLBoxPlot::GetXYZRange, outxRange, outyRange, outzRange, _EXTRA=_extra
    compile_opt idl2, hidden

  if ((MAX(FINITE((*self.data))) eq 0) || (MAX(FINITE((*self.x_data))) eq 0)) $
    then return, 0
  
  outzRange = [0,0]
  
  outxRange = DOUBLE([MIN(*self.x_data, MAX=max),max])
  outyRange = DOUBLE([MIN(*self.data, MAX=max),max])

  ; Add padding to x values
  outxRange[0] -= CEIL((*self.width)[0])
  outxRange[1] += CEIL((*self.width)[-1])
  
  ; Account for extreme notch values
  hasNotch = self.show_notch && $
    (N_ELEMENTS(*self.x_data) eq N_ELEMENTS(*self.notch_values))
  if (hasNotch) then begin
    top = (*self.data)[*,2]+*self.notch_values
    bottom = (*self.data)[*,2]-*self.notch_values
    outyRange[1] >= MAX(top)
    outyRange[0] <= MIN(bottom)
  endif
  
  ; Account for additional points (outliers...)
  if (self.show_means && (MAX(FINITE(*self.means))) && $
      (N_ELEMENTS(*self.means) eq N_ELEMENTS(*self.x_data))) then begin
    outyRange[1] >= MAX(*self.means)
    outyRange[0] <= MIN(*self.means)
  endif
    
  if (self.show_outliers && (N_ELEMENTS(*self.outliers) gt 1)) then begin
    outyRange[1] >= MAX((*self.outliers)[1,*])
    outyRange[0] <= MIN((*self.outliers)[1,*])
  endif
  
  if (self.show_suspected_outliers && $
      (N_ELEMENTS(*self.suspected_outliers) gt 1)) then begin
    outyRange[1] >= MAX((*self.suspected_outliers)[1,*])
    outyRange[0] <= MIN((*self.suspected_outliers)[1,*])
  endif

  if (self.horizontal) then begin
    tmp = outxRange
    outxRange = outyRange
    outyRange = tmp
  end

  return, 1
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::_UpdateData
;
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot::_UpdateData
  compile_opt idl2, hidden

  ; Simply ensure that the x_data matches the data
  if ((N_ELEMENTS(*self.x_data) ne N_ELEMENTS((*self.data)[*,0])) || $
      (MAX(FINITE(*self.x_data)) eq 0)) then begin
    dims = SIZE(*self.data, /DIMENSIONS)
    if (dims[0] gt 0) then begin
      *self.x_data = indgen(dims[0])
    endif
  endif
  
END


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::_GetColor
;
; PURPOSE:
;      This procedure returns a color.
;
;-
;----------------------------------------------------------------------------
function IDLBoxPlot::_GetColor, color
  compile_opt idl2, hidden

  if (SIZE(color, /TNAME) eq 'STRING') then begin
    index = where(STRUPCASE(color[0]) eq TAG_NAMES(!color), cnt)
    if (cnt) then begin
      outColor = !color.(index[0])
    endif else begin
      outColor = [0b,0b,0b]
    endelse
    return, outColor
  endif
  
  if (N_ELEMENTS(color) eq 3) then return, BYTE(color)
  
  return, color
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::_UpdateSelectionVisual
;
; PURPOSE:
;      This procedure method updates the selection visual based
;      on the plot data.
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]_UpdateSelectionVisual
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot::_UpdateSelectionVisual
  compile_opt idl2, hidden

  if (self->GetXYZRange(xRange, yRange, zRange)) then begin
    self._oLineBox->GetProperty, DATA=bdata, HIDE=bhide
    self._oLineWhisker->GetProperty, DATA=wdata, HIDE=whide
  
    dataX = [xRange[0], xRange[0], xRange[1], xRange[1], xRange[0]]
    dataY = [yRange[0], yRange[1], yRange[1], yRange[0], yRange[0]]
  
    self._oPlotSelectionVisual->SetProperty, $
      DATAX=dataX, DATAY=dataY, HIDE=(whide && bhide)
  endif else begin
    self._oPlotSelectionVisual->SetProperty, HIDE=1
  endelse
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::_UpdateSymbols
;
; PURPOSE:
;      This procedure method updates the mean, outlier and suspected outlier 
;      symbols for the bars..
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]_UpdateSymbols
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot::_UpdateSymbols
  compile_opt idl2, hidden

  self->_UpdateData
  
  if (self.show_means && (MAX(FINITE(*self.means))) && $
      (N_ELEMENTS(*self.means) eq N_ELEMENTS(*self.x_data))) then begin
    data = self.horizontal ? transpose([[*self.means], [*self.x_data]]) : $
      transpose([[*self.x_data], [*self.means]])
    self._oMeansPlot->PutData, data
    self._oMeansPlot->SetProperty, HIDE=0
  endif else begin
    self._oMeansPlot->SetProperty, HIDE=1
  endelse
    
  if (self.show_outliers && (N_ELEMENTS(*self.outliers) gt 1)) then begin
    xdata = (*self.x_data)[(*self.outliers)[0,*]]
    ydata = (*self.outliers)[1,*]
    data = self.horizontal ? [ydata, xdata] : [xdata, ydata]
    self._oOutliersPlot->PutData, data
    self._oOutliersPlot->SetProperty, HIDE=0
  endif else begin
    self._oOutliersPlot->SetProperty, HIDE=1
  endelse
  
  if (self.show_suspected_outliers && $
      (N_ELEMENTS(*self.suspected_outliers) gt 1)) then begin
    xdata = (*self.x_data)[(*self.suspected_outliers)[0,*]]
    ydata = (*self.suspected_outliers)[1,*]
    data = self.horizontal ? [ydata, xdata] : [xdata, ydata]
    self._oSuspectedOutliersPlot->PutData, data
    self._oSuspectedOutliersPlot->SetProperty, HIDE=0
  endif else begin
    self._oSuspectedOutliersPlot->SetProperty, HIDE=1
  endelse

end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::_UpdateFill
;
; PURPOSE:
;      This procedure method updates the polygon representing
;      the filled area under the plot.  It must be updated when
;      the fill level (the lower boundary) changes or when going
;      into or out of histogram mode, for example.
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]_UpdateFill
;
; INPUTS:
;      DataspaceX/Yrange: Optional args giving the dataspace ranges.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot::_UpdateFill, DSxRange, DSyRange, DSzRange
  compile_opt idl2, hidden

  self->_UpdateData
  
  if (~FINITE((*self.data)[0])) then return
  if (MAX(FINITE(*self.data)) eq 0) then return
  
  if (~self.use_fill_color) then begin
    self._oFill->SetProperty, HIDE=1
    return
  endif
  
  hasNotch = self.show_notch && $
    (N_ELEMENTS(*self.x_data) eq N_ELEMENTS(*self.notch_values)) 
  notchDepth = 1-self.notch_depth

  barX = [!NULL]
  barY = [!NULL]
  barConn = [!NULL]
  barVertCol = [!NULL]
  barConnCtr = 0
  
  for i=0,N_ELEMENTS(*self.x_data)-1 do begin
    newX = [!NULL]
    newY = [!NULL]
    newVert = [!NULL]
    newConn = [!NULL]
    
    x = (*self.x_data)[i]
    y = (*self.data)[i,*]
    xl = x-(*self.width)[i mod N_ELEMENTS(*self.width)]
    xr = x+(*self.width)[i mod N_ELEMENTS(*self.width)]

    upperColor = self.fill_color
    lowerColor = self.use_lower_color ? self.lower_color : self.fill_color

    ; Upper portion
    if (hasNotch) then begin
      if (y[2]+(*self.notch_values)[i] gt y[3]) then begin
        crossX = x - interpol([x-(notchDepth*(x-xl)),xl],$
                              [y[2],y[2]+(*self.notch_values)[i]],y[3])
        ; main trapezoid
        newX = [newX, x-crossX, x-(notchDepth*(x-xl)), $
                x+(notchDepth*(x-xl)), x+crossX]
        newY = [newY, y[3], y[2], y[2], y[3]]
        newVert = [[newVert], [upperColor], [upperColor], $
                   [upperColor], [upperColor]]
        cnt = 4
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
        ; left triangle
        newX = [newX, xl, x-crossX, xl]
        newY = [newY, y[3], y[3], y[2]+(*self.notch_values)[i]]
        newVert = [[newVert], [upperColor], [upperColor], $
                   [upperColor]]
        cnt = 3
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
        ; right triangle
        newX = [newX, xr, x+crossX, xr]
        newY = [newY, y[3], y[3], y[2]+(*self.notch_values)[i]]
        newVert = [[newVert], [upperColor], [upperColor], $
                   [upperColor]]
        cnt = 3
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
      endif else begin
        newX = [newX, xl, xl, x-(notchDepth*(x-xl)), $
                x+(notchDepth*(x-xl)), xr, xr]
        newY = [newY, y[3], y[2]+(*self.notch_values)[i], y[2], y[2], $
                y[2]+(*self.notch_values)[i], y[3]]
        newVert = [[newVert], [upperColor], [upperColor], [upperColor], $
                   [upperColor], [upperColor], [upperColor]]
        cnt = 6
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
      endelse
    endif else begin
      newX = [newX, xl, xl, xr, xr]
      newY = [newY, y[2], y[3], y[3], y[2]]
      newVert = [[newVert], [upperColor], [upperColor], $
                 [upperColor], [upperColor]]
      cnt = 4
      newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
      barConnCtr += cnt
    endelse
    
    ; Lower portion
    if (hasNotch) then begin
      if (y[2]-(*self.notch_values)[i] lt y[1]) then begin
        crossX = x - interpol([x-(notchDepth*(x-xl)),xl],$
                              [y[2],y[2]-(*self.notch_values)[i]],y[1])
        ; main trapezoid
        newX = [newX, x-crossX, x-(notchDepth*(x-xl)), $
                x+(notchDepth*(x-xl)), x+crossX]
        newY = [newY, y[1], y[2], y[2], y[1]]
        newVert = [[newVert], [lowerColor], [lowerColor], $
                   [lowerColor], [lowerColor]]
        cnt = 4
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
        ; left triangle
        newX = [newX, xl, x-crossX, xl]
        newY = [newY, y[1], y[1], y[2]-(*self.notch_values)[i]]
        newVert = [[newVert], [lowerColor], [lowerColor], $
                   [lowerColor]]
        cnt = 3
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
        ; right triangle
        newX = [newX, xr, x+crossX, xr]
        newY = [newY, y[1], y[1], y[2]-(*self.notch_values)[i]]
        newVert = [[newVert], [lowerColor], [lowerColor], $
                   [lowerColor]]
        cnt = 3
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
      endif else begin
        newX = [newX, xl, xl, x-(notchDepth*(x-xl)), $
                x+(notchDepth*(x-xl)), xr, xr]
        newY = [newY, y[1], y[2]-(*self.notch_values)[i], y[2], y[2], $
                y[2]-(*self.notch_values)[i], y[1]]
        newVert = [[newVert], [lowerColor], [lowerColor], $
                   [lowerColor], [lowerColor], $
                   [lowerColor], [lowerColor]]
        cnt = 6
        newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
        barConnCtr += cnt
      endelse
    endif else begin
      newX = [newX, xl, xl, xr, xr]
      newY = [newY, y[2], y[1], y[1], y[2]]
      newVert = [[newVert], [lowerColor], [lowerColor], $
                 [lowerColor], [lowerColor]]
      cnt = 4
      newConn = [newConn, cnt+1, indgen(cnt)+barConnCtr, barConnCtr]
      barConnCtr += cnt
    endelse
    
    barX = [barX, newX]
    barY = [barY, newY]
    barVertCol = [[barVertCol], [newVert]]
    barConn = [barConn, newConn]
  endfor

  boxData = transpose([[barX], [barY]])

  ; Handle horizontal
  if (self.horizontal) then begin
    tmp = boxData[0,*]
    boxData[0,*] = boxData[1,*]
    boxData[1,*] = tmp
  endif

  self._oFill->SetProperty, __DATA=boxData, $
    __POLYGONS=barConn, HIDE=0, FILL_COLOR=[0,0,0], $ 
    VERT_COLORS=barVertCol, TESSELLATE=1

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBoxPlot::_UpdateLine
;
; PURPOSE:
;      This procedure method updates the polyline that draws the box
;
; CALLING SEQUENCE:
;      Obj->[IDLBoxPlot::]_UpdateLine
;
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot::_UpdateLine, newx, newy, conn
  compile_opt idl2, hidden

  self->_UpdateData
  
  if (N_ELEMENTS(*self.data) le 1) then return
  
  hasNotch = self.show_notch && $
    (N_ELEMENTS(*self.x_data) eq N_ELEMENTS(*self.notch_values)) 
  notchDepth = 1-self.notch_depth

  ; LineBox
  bnewx = [!NULL]
  bnewy = [!NULL]
  bconn = [!NULL]
  bconnctr = 0
  ; LineWhiskers
  wnewx = [!NULL]
  wnewy = [!NULL]
  wconn = [!NULL]
  wconnctr = 0

  for i=0,N_ELEMENTS(*self.x_data)-1 do begin
    x = (*self.x_data)[i]
    y = (*self.data)[i,*]
    xl = x-(*self.width)[i mod N_ELEMENTS(*self.width)]
    xr = x+(*self.width)[i mod N_ELEMENTS(*self.width)]

    if (self.show_box) then begin
      ; lower box
      cnt = 0
      if (hasNotch) then begin
        bnewx = [bnewx, x - (notchDepth*(x-xl)), xl]
        bnewy = [bnewy, y[2], y[2]-(*self.notch_values)[i]]
        cnt += 2
      endif else begin
        bnewx = [bnewx, xl]
        bnewy = [bnewy, y[2]]
        cnt += 1
      endelse
      bnewx = [bnewx, xl, xr]
      bnewy = [bnewy, y[1], y[1]]
      cnt += 2
      if (hasNotch) then begin
        bnewx = [bnewx, xr, x + (notchDepth*(x-xl))]
        bnewy = [bnewy, y[2]-(*self.notch_values)[i], y[2]]
        cnt += 2
      endif else begin
        bnewx = [bnewx, xr]
        bnewy = [bnewy, y[2]]
        cnt += 1
      endelse
      bconn = [bconn, cnt, indgen(cnt)+bconnctr]
      bconnctr += cnt
      ; upper box
      cnt = 0
      if (hasNotch) then begin
        bnewx = [bnewx, x - (notchDepth*(x-xl)), xl]
        bnewy = [bnewy, y[2], y[2]+(*self.notch_values)[i]]
        cnt += 2
      endif else begin
        bnewx = [bnewx, xl]
        bnewy = [bnewy, y[2]]
        cnt += 1
      endelse
      bnewx = [bnewx, xl, xr]
      bnewy = [bnewy, y[3], y[3]]
      cnt += 2
      if (hasNotch) then begin
        bnewx = [bnewx, xr, x + (notchDepth*(x-xl))]
        bnewy = [bnewy, y[2]+(*self.notch_values)[i], y[2]]
        cnt += 2
      endif else begin
        bnewx = [bnewx, xr]
        bnewy = [bnewy, y[2]]
        cnt += 1
      endelse
      bconn = [bconn, cnt, indgen(cnt)+bconnctr]
      bconnctr += cnt
    endif

    ; median line
    if (self.show_median) then begin
      cnt = 0
      if (hasNotch) then begin
        bnewx = [bnewx, x - (notchDepth*(x-xl)), x + (notchDepth*(x-xl))]
      endif else begin
        bnewx = [bnewx, xl, xr]
      endelse
      bnewy = [bnewy, y[2], y[2]]
      cnt = +2
      bconn = [bconn, cnt, indgen(cnt)+bconnctr]
      bconnctr += cnt
    endif

    ; whiskers
    if (self.show_whiskers) then begin
      ; bottom whisker
      cnt = 0
      wnewx = [wnewx, x, x]
      wnewy = [wnewy, y[0], y[1]]
      cnt += 2
      wconn = [wconn, cnt, indgen(cnt)+wconnctr]
      wconnctr += cnt
      ; top whisker
      cnt = 0
      wnewx = [wnewx, x, x]
      wnewy = [wnewy, y[3], y[4]]
      cnt += 2
      wconn = [wconn, cnt, indgen(cnt)+wconnctr]
      wconnctr += cnt
    endif
  
    ; end caps
    if (self.show_endcaps) then begin
      ; bottom endcap
      cnt = 0
      wnewx = [wnewx, xl, xr]
      wnewy = [wnewy, y[0], y[0]]
      cnt += 2
      wconn = [wconn, cnt, indgen(cnt)+wconnctr]
      wconnctr += cnt
      ; top endcap
      cnt = 0
      wnewx = [wnewx, xl, xr]
      wnewy = [wnewy, y[4], y[4]]
      cnt += 2
      wconn = [wconn, cnt, indgen(cnt)+wconnctr]
      wconnctr += cnt
    endif
  endfor

  ; Handle horizontal
  if (self.horizontal) then begin
    tmp = bnewx
    bnewx = bnewy
    bnewy = TEMPORARY(tmp)
    tmp = wnewx
    wnewx = wnewy
    wnewy = TEMPORARY(tmp)
  endif

  if (N_ELEMENTS(bnewx) gt 1) then begin
    self._oLineBox->SetProperty, __DATA=transpose([[bnewx],[bnewy]]), $
      __POLYLINES=bconn, HIDE=self.hide
  endif else begin
    self._oLineBox->SetProperty, HIDE=1
  endelse

  if (N_ELEMENTS(wnewx) gt 1) then begin
    self._oLineWhisker->SetProperty, __DATA=transpose([[wnewx],[wnewy]]), $
      __POLYLINES=wconn, HIDE=0
  endif else begin
    self._oLineWhisker->SetProperty, HIDE=1
  endelse

end

;----------------------------------------------------------------------------
;+
; IDLBoxPlot__Define
;
; PURPOSE:
;      Defines the object structure for an IDLBoxPlot object.
;-
;----------------------------------------------------------------------------
pro IDLBoxPlot__Define
  compile_opt idl2, hidden

  struct = {IDLBoxPlot,           $
            inherits IDLitVisPlot, $   ; Superclass: _IDLitVisualization
            _oLineBox: OBJ_NEW(), $ 
            _oLineWhisker: OBJ_NEW(), $ 
            _oMeansPlot: OBJ_NEW(), $
            _oOutliersPlot: OBJ_NEW(), $
            _oSuspectedOutliersPlot: OBJ_NEW(), $
            data: PTR_NEW(), $
            x_data: PTR_NEW(), $
            color: [0b,0b,0b], $
            fill_color: [0b,0b,0b], $
            use_fill_color: 0b, $
            lower_color: [0b,0b,0b], $
            use_lower_color: 0b, $
            _use_lower_color: 0b, $
            outline_thick: 0, $
            outline_hide: 0b, $
            outline_style: 0b, $
            horizontal: 0b, $
            show_box: 0b, $
            show_median: 0b, $
            show_whiskers: 0b, $
            show_endcaps: 0b, $
            show_notch: 0b, $
            show_means: 0b, $
            show_outliers: 0b, $
            show_suspected_outliers: 0b, $
            notch_depth: 0.0, $
            outliers: PTR_NEW(), $
            suspected_outliers: PTR_NEW(), $
            symbol_means: OBJ_NEW(), $
            symbol_outliers: OBJ_NEW(), $
            symbol_suspected_outliers: OBJ_NEW(), $
            means: PTR_NEW(), $
            notch_values: PTR_NEW(), $
            width: PTR_NEW() $
           }
             
end
