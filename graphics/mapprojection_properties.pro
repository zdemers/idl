; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/mapprojection_properties.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Return the MapGrid properties.
;
; :Params:
;    None
;
; :Keywords:
;    
;
;-
;-------------------------------------------------------------------------
function MapProjection_Properties, ALL=all

  compile_opt idl2, hidden

  myprops = ['MAP_PROJECTION', 'ELLIPSOID', $
    'SEMIMAJOR_AXIS', 'SEMIMINOR_AXIS', 'CENTER_LONGITUDE', 'CENTER_LATITUDE', $
    'LIMIT', 'FALSE_EASTING', 'FALSE_NORTHING', $
    'STANDARD_PAR1', 'STANDARD_PAR2', 'TRUE_SCALE_LATITUDE', $
    'MERCATOR_SCALE', 'HEIGHT', 'HOM_AZIM_ANGLE', 'HOM_AZIM_LONGITUDE', $
    'HOM_LONGITUDE1', 'HOM_LATITUDE1', 'HOM_LONGITUDE2', 'HOM_LATITUDE2', $
    'SOM_LANDSAT_NUMBER', 'SOM_LANDSAT_PATH', 'SOM_INCLINATION', $
    'SOM_LONGITUDE', 'SOM_PERIOD', 'SOM_RATIO', 'SOM_FLAG', $
    'OEA_SHAPEM', 'OEA_SHAPEN', 'OEA_ANGLE', 'IS_ZONES', 'IS_JUSTIFY', 'ZONE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if KEYWORD_SET(all) then begin
    myprops = [myprops, $
      'SPHERE_RADIUS', 'STANDARD_PARALLEL']
  endif

  return, myprops
end

