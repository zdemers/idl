; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/graphicstool__define.pro#1 $
;
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
;+
; CLASS_NAME:
;   GraphicsTool
;
; PURPOSE:
;   This file implements the Tool class for IDL Graphics.
;
;-

;---------------------------------------------------------------------------
function GraphicsTool::Init, _REF_EXTRA=_EXTRA
  ;; Pragmas
  compile_opt idl2, hidden

  ;; Call our super class
  if (~self->IDLitTool::Init(_EXTRA=_extra)) then $
    return, 0

  oSystem = self->_GetSystem()

  ;---------------------------------------------------------------------
  ; Register all operations


  ;*** File operations

  self->createfolders,'Operations/File',NAME=IDLitLangCatQuery('Menu:File')

   self->RegisterOperation, IDLitLangCatQuery('Menu:File:Print'), $
        'IDLitopFilePrint', $
        ACCELERATOR='Ctrl+P', $
        DESCRIPTION='Print the contents of the active window', $
        IDENTIFIER='File/Print', ICON='print'
    ; Need both Save and Save As operations.
    self->RegisterOperation, IDLitLangCatQuery('Menu:File:Save'), $
        'IDLitopFileSave', $
        IDENTIFIER='File/Save', ICON='save'
    self->RegisterOperation, IDLitLangCatQuery('Menu:File:SaveAs'), $
        'IDLitopFileSaveAs', $
        ACCELERATOR='Ctrl+S', $
        IDENTIFIER='File/SaveAs', ICON='save', /SEPARATOR

    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Properties'), $
        'IDLitopEditPropertySheet', $
        DESCRIPTION='Display the Property Sheet for the item', $
        IDENTIFIER='Edit/Properties', $
        ICON='properties'

    self->RegisterOperation, IDLitLangCatQuery('Menu:Window:ResetDataspaceRanges'), $
        'IDLitopDataspaceReset', $
        DESCRIPTION='Reset dataspace ranges', $
        IDENTIFIER='Edit/DataspaceReset', $
        ICON='resetrange', /PRIVATE


  ;*** Edit operations

    self->createfolders,'Operations/Edit',NAME=IDLitLangCatQuery('Menu:Edit')

    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Undo'), $
        'IDLitopUndo', $
        ACCELERATOR='Ctrl+Z', $
        IDENTIFIER='Edit/Undo', ICON='undo', /disable

    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Redo'), $
        'IDLitopRedo', $
        ACCELERATOR='Ctrl+Y', $
        IDENTIFIER='Edit/Redo', ICON='redo', /disable

    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Delete') , $
        PROXY='/REGISTRY/OPERATIONS/Delete', $
        IDENTIFIER='Edit/Delete'

  self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:BringToFront'), $
        'IDLitopBringToFront', $
        IDENTIFIER='Edit/Order/BringToFront', $
        ICON='bringtofront', /SEPARATOR

    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:SendToBack'), $
        'IDLitopSendToBack', $
        IDENTIFIER='Edit/Order/SendToBack', $
        ICON='sendtoback'

    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Order'), $
        'IDLitopOrder', $
        IDENTIFIER='Edit/Order/Order', $
        ICON='sendtoback', /PRIVATE
        
    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Cut'), $
        'IDLitopClipCut', $
        ACCELERATOR='Ctrl+X', $
        IDENTIFIER='Edit/Cut', ICON='cut', /SEPARATOR
    
    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Copy'), $
        'IDLitopclipcopy', $
        ACCELERATOR='Ctrl+Insert', $
        IDENTIFIER='Edit/Copy', ICON='copy'
        
    self->RegisterOperation, IDLitLangCatQuery('Menu:Edit:Paste'), $
        'IDLitopClipPaste', $
        ACCELERATOR='Ctrl+V', $
        IDENTIFIER='Edit/Paste', ICON='paste'


  ;*** Insert operations

    self->createfolders,'Operations/Insert', $
                        NAME=IDLitLangCatQuery('Menu:Insert')

  self->RegisterOperation, $
    IDLitLangCatQuery('Menu:Insert:Map:Grid'), $
    'IDLitopInsertMapGrid', $
    IDENTIFIER='Insert/Map/Grid', $
    ICON='axis'

  self->RegisterOperation, $
    IDLitLangCatQuery('Menu:Insert:Map:Continents'), $
    'IDLitopInsertMapShape', $
    IDENTIFIER='Insert/Map/Continents', $
    ICON='demo'


  ;*** Map operations

  self->createfolders,'Operations/Operations', $
                        NAME=IDLitLangCatQuery('Menu:Operations')

  ; We must register the MapRegisterImage operation,
  ; so that we can use the GEOTIFF keyword in IMAGE().
  self->RegisterOperation, $
    IDLitLangCatQuery('Menu:Operations:MapRegisterImage'), $
    'IDLitopMapRegisterImage', $
    IDENTIFIER='Operations/MapRegisterImage', $
    ICON='demo'

  ; Turn off the UI in the MapRegisterImage operation, so that you don't
  ; get any dialogs popping up if a map projection error occurs.
  oDesc = self->GetByIdentifier('Operations/Operations/MapRegisterImage')
  if (ISA(oDESC)) then begin
    oMapRegister = oDesc->GetObjectInstance()
    if (ISA(oMapRegister)) then begin
      oMapRegister->SetProperty, SHOW_EXECUTION_UI=0
    endif
  endif

    self->RegisterOperation, IDLitLangCatQuery('Menu:Operations:MapProjection'), $
        'IDLitopMapProjection', $
        IDENTIFIER='Operations/Map Projection', ICON='surface'


  ;---------------------------------------------------------------------
  ; Create the Toolbar, which contains proxies to the above operations.
  ;

  self->Register, IDLitLangCatQuery('Menu:File:Print'), $
        PROXY='Operations/File/Print', $
        IDENTIFIER='Toolbar/File/Print'

  self->Register, IDLitLangCatQuery('Menu:File:SaveAs'), $
        PROXY='Operations/File/SaveAs', $
        IDENTIFIER='Toolbar/File/SaveAs'

  self->Register, IDLitLangCatQuery('Menu:Edit:Properties'), $
        PROXY='Operations/Edit/Properties', $
        IDENTIFIER='Toolbar/File/Properties'

  self->Register, IDLitLangCatQuery('Menu:Window:ResetDataspaceRanges'), $
        PROXY='Operations/Edit/DataspaceReset', $
        IDENTIFIER='Toolbar/File/DataspaceReset'

  self->Register, IDLitLangCatQuery('Menu:Edit:Undo'), $
        PROXY='Operations/Edit/Undo', $
        IDENTIFIER='Toolbar/Edit/Undo'

  self->Register, IDLitLangCatQuery('Menu:Edit:Redo'), $
        PROXY='Operations/Edit/Redo', $
        IDENTIFIER='Toolbar/Edit/Redo'

  self->Register, IDLitLangCatQuery('Menu:Edit:BringToFront'), $
        PROXY='Operations/Edit/Order/BringToFront', $
        IDENTIFIER='Toolbar/Edit/BringToFront'
        
  self->Register, IDLitLangCatQuery('Menu:Edit:SendToBack'), $
        PROXY='Operations/Edit/Order/SendToBack', $
        IDENTIFIER='Toolbar/Edit/SendToBack'
        
  self->Register, IDLitLangCatQuery('Menu:Edit:Cut'), $
        PROXY='Operations/Edit/Cut', $
        IDENTIFIER='Toolbar/Edit/Cut'

  self->Register, IDLitLangCatQuery('Menu:Edit:Copy'), $
        PROXY='Operations/Edit/Copy', $
        IDENTIFIER='Toolbar/Edit/Copy'

  self->Register, IDLitLangCatQuery('Menu:Edit:Paste'), $
        PROXY='Operations/Edit/Paste', $
        IDENTIFIER='Toolbar/Edit/Paste'


  ;---------------------------------------------------------------------
  ; Manipulators
  ;
  
  ; Note: Do not give an icon - we don't want the "Arrow" select manip
  ; to show up in the toolbar. But it needs to exist for manipulators
  ; to work properly.
  self->RegisterManipulator, 'Select', 'GraphicsManip', $
      ICON='', /DEFAULT, IDENTIFIER="Select"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Text'), $
    'IDLitAnnotateText', $
    ICON='text', IDENTIFIER="Text"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Line'), $
    'IDLitAnnotateLine', $
    ICON='line', IDENTIFIER="Line"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Rectangle'), $
    'IDLitAnnotateRectangle', $
    ICON='rectangle', IDENTIFIER="Rectangle"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Oval'), $
    'IDLitAnnotateOval', $
    ICON='ellipse', IDENTIFIER="Oval"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Polygon'), $
    'IDLitAnnotatePolygon', $
    ICON='polygon', IDENTIFIER="Polygon"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Freehand'), $
    'IDLitAnnotateFreehand', $
    ICON='freehand', IDENTIFIER="Freehand"
      
  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Arrow'), $
    'IDLitAnnotateArrow', $
    ICON='arrow', IDENTIFIER="Arrow"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:NewLegend'), $
      'IDLitAnnotateLegend', $
      ICON='legend', IDENTIFIER="Legend"

  self->RegisterManipulator, IDLitLangCatQuery('Menu:Insert:Colorbar'), $
      'AnnotateColorbar', $
      ICON='colorbar', IDENTIFIER="Colorbar"


  return, 1

end


;---------------------------------------------------------------------------
pro GraphicsTool::_UpdateToolByType, strType
   compile_opt hidden, idl2
   ; Do nothing
   ; We do not want to use "tool morphing" to add additional operations
   ; specific to the vis type.
end


;---------------------------------------------------------------------------
; GraphicsTool__Define
;
; Purpose:
;   This method defines the IDLitTool class.
;

pro GraphicsTool__Define
  ; Pragmas
  compile_opt idl2, hidden
  void = { GraphicsTool,                     $
           inherits IDLitTool       $ ; Provides iTool interface
           }
end
