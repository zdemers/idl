; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/legend__define.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create a Legend.
;
; :Params:
;    Projection
;
; :Keywords:
;    
;
;-
;-------------------------------------------------------------------------
function Legend::Init, oObj, $
  DATA=data, DEVICE=device, NORMAL=normal, RELATIVE=relative, TARGET=target, $
  UNDOCUMENTED=undocumented, $  ; allow undocumented properties
  _REF_EXTRA=_extra

  compile_opt idl2, hidden
@graphic_error

  if (ISA(oObj)) then $
    return, self->Graphic::Init(oObj)

  ; Check for unknown or illegal properties.
  if (ISA(_extra) && ~KEYWORD_SET(undocumented)) then $
    self->VerifyProperty, _extra

  oCmd = self->_Create(oLegend, $
    DATA=data, DEVICE=device, NORMAL=normal, RELATIVE=relative, TARGET=target, $
    _EXTRA=_extra)

  if (ISA(oLegend)) then begin
    void = self->Graphic::Init(oLegend)
    oTool = oLegend->GetTool()
    if (OBJ_VALID(oTool)) then $
      oTool->_TransactCommand, oCmd
    return, 1
  endif
  
  return, 0
  
end


;-------------------------------------------------------------------------
pro Legend::_FilterTargets, oTargets
  compile_opt idl2, hidden

  ; filter to acceptable visualizations
  oVisTargets = []
  for i=0, N_ELEMENTS(oTargets)-1 do begin
    if ( OBJ_ISA(oTargets[i], 'IDLitVisPlot') || $
         OBJ_ISA(oTargets[i], 'IDLitVisPlot3D') || $
         OBJ_ISA(oTargets[i], 'IDLitVisContour') || $
         OBJ_ISA(oTargets[i], 'IDLitVisVector') ) $
      then begin
      oVisTargets = [oVisTargets, oTargets[i]]
    endif
  endfor
  oTargets = oVisTargets

end


;-------------------------------------------------------------------------
function Legend::_FindTargets, oTool
  compile_opt idl2, hidden

  if (~Isa(oTool)) then return, []

  ; Retrieve the current selected item(s).
  oTargets = oTool->GetSelectedItems(count=nTargets)
  if (nTargets gt 0) then begin
    self._FilterTargets, oTargets
    return, oTargets
  endif

  ;; Get window
  oWin = oTool->GetCurrentWindow()
  oView = ISA(oWin) ? oWin->GetCurrentView() : !NULL
  oLayer = ISA(oView) ? oView->GetCurrentLayer() : !NULL
  oWorld = ISA(oLayer) ? oLayer->GetWorld() : !NULL
  oDataspaces = ISA(oWorld) ? oWorld->GetDataSpaces(COUNT=nDS) : !NULL
  ;; Loop through data spaces until the first data space with
  ;; acceptable legend targets are found
  foreach oDS, oDataspaces do begin
    if (OBJ_VALID(oDS)) then begin
      oDSObjs = oDS->GetVisualizations()
      self._FilterTargets, oDSObjs  
      if (ISA(oDSObjs)) then return, oDSObjs
    endif
  endforeach
  return, []
end


;-------------------------------------------------------------------------
function Legend::_Create, oLegend, $
                          DEVICE=device, $
                          RELATIVE=relative, $
                          DATA=data, $
                          POSITION=positionIn, $
                          TARGET=targetIn, $
                          _REF_EXTRA=_extra
  compile_opt idl2, hidden

  nTargets = 0
  if (MIN(OBJ_VALID(targetIn) eq 1)) then begin
    oTool = (targetIn[0]).GetTool()
    oTargets = []
    if (ISA(oTool)) then begin
      for i=0,N_ELEMENTS(targetIn)-1 do begin
        oTargets = [oTargets, $
          oTool->GetByIdentifier(targetIn[i]->GetFullIdentifier())]
      endfor
      ; filter to acceptable visualizations
      self._FilterTargets, oTargets
    endif 
  endif else begin
    !NULL = iGetCurrent(TOOL=oTool)
    oTargets = self->_FindTargets(oTool)
  endelse

  nTargets = N_ELEMENTS(oTargets)

  if (nTargets eq 0) then begin
    message, 'No suitable legend items found.'
    return, OBJ_NEW()
  endif
    
  idTargets = STRARR(nTargets)
  for i=0,nTargets-1 do $
    idTargets[i] = oTargets[i]->GetFullIdentifier()
    
  if (N_ELEMENTS(positionIn) ge 2) then begin
    position = DOUBLE(positionIn)
    ; Note: Logarithmic axes are automatically handled by iConvertCooord.
    newPos = iConvertCoord(position[0], position[1], /TO_NORMAL, $
                           DATA=data, DEVICE=device, RELATIVE=relative, TARGET=idTargets[0])
    position = newPos[0:1]
  endif    

  oCreate = oTool->GetService("CREATE_VISUALIZATION")
  if (~Obj_Valid(oCreate)) then return, OBJ_NEW()

  oVisDesc = oTool->GetAnnotation('LEGEND')

  ; Call _Create so we don't have to worry about type matching.
  ; We know we want to create a legend.
  oCmd = oCreate->_Create(oVisDesc, $
                          ID_VISUALIZATION=visID, $
                          LAYER='ANNOTATION', $
                          VIS_TARGET=idTargets, $
                          /MANIPULATOR_TARGET, $
                          POSITION=position, $
                          LOCATION=[1.05d,0.95d], $  ; initially in upper right corner
                          /NO_SELECT, $
                          _EXTRA=_extra)
  oLegend = oTool->GetByIdentifier(visID)

  ; Apply any properties to the newly-created legend items.
  if (ISA(_extra)) then begin
    oNewLegendItems = oLegend->Get(/ALL, ISA='IDLitVisLegendItem', COUNT=count)
    for i=0,count-1 do begin
      oNewLegendItems[i]->SetProperty, _EXTRA=LegendItem_Properties()
    endfor
    oLegend->RecomputeLayout
  endif


  oCmd[0]->SetProperty, NAME='Legend'
  return, oCmd

end

;-------------------------------------------------------------------------
pro Legend::Remove, arg, $
                 _REF_EXTRA=_extra
  compile_opt idl2, hidden
  
@graphic_error

  case n_params() of
  0: self.__obj__->RemoveFromLegend, _EXTRA=_extra
  1: self.__obj__->RemoveFromLegend, arg, _EXTRA=_extra
  endcase
end

;-------------------------------------------------------------------------
pro Legend::Add, targetIn, $
                 _REF_EXTRA=_extra
  compile_opt idl2, hidden

@graphic_error

  nTargets = 0
  ; Get tool
  if (MIN(OBJ_VALID(targetIn) eq 1)) then begin
    oTool = targetIn.GetTool()
    oTargets = []
    if (ISA(oTool)) then begin
      for i=0,N_ELEMENTS(targetIn)-1 do $
        oTargets = [oTargets, $
          oTool->GetByIdentifier(targetIn[i]->GetFullIdentifier())]
      nTargets = N_ELEMENTS(oTargets)
    endif 
  endif else begin
    !NULL = iGetCurrent(TOOL=oTool)
    if (ISA(oTool)) then begin
      ; Retrieve the current selected item(s).
      oTargets = oTool->GetSelectedItems(count=nTargets)
      if (nTargets eq 0) then begin
        ;; Get window
        oWin = oTool->GetCurrentWindow()
        oView = OBJ_VALID(oWin) ? oWin->GetCurrentView() : []
        ;; Get first data space
        if (OBJ_VALID(oView)) then begin
          dsID = (oView->FindIdentifiers('*DATA SPACE*'))[0]
          oDS = oTool->GetByIdentifier(dsID)
        endif
        if (OBJ_VALID(oDS)) then begin
          oDSObjs = oDS->GetVisualizations()
          if ((N_ELEMENTS(oDSObjs) eq 1) && (OBJ_VALID(oDSObjs))) then begin
            oTargets = oDSObjs[0]
            nTargets = 1
          endif
        endif
      endif
    endif
  endelse

  for i=0,nTargets-1 do begin
    self.__obj__->AddToLegend, oTargets[i], oNewLegendItems
  endfor

  ; Apply any properties to the newly-created legend items.
  if (ISA(_extra)) then begin
    for i=0,nTargets-1 do begin
      oNewLegendItems[i]->SetProperty, _EXTRA=_extra
    endfor
    self.__obj__->RecomputeLayout
  endif

  oTool = self.GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->RefreshCurrentWindow
  
end


;---------------------------------------------------------------------------
function Legend::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = ['AUTO_TEXT_COLOR', 'COLOR', $
    'HORIZONTAL_ALIGNMENT','HORIZONTAL_SPACING','LINESTYLE', $
    'NAME','ORIENTATION','POSITION','SHADOW', 'THICK', $
    'TRANSPARENCY', 'VERTICAL_ALIGNMENT', 'VERTICAL_SPACING']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then begin
    ; Also include the LegendItem properties, so we can set them on creation.
    myprops = [myprops, $
      LegendItem_Properties(/ALL), $
      'WINDOW']
  endif else begin
    myprops=[myprops,'HIDE']
  endelse

  ; Do not return Graphic's properties, since Text is just an annotation.
  return, myprops
end


;---------------------------------------------------------------------------
function Legend::_overloadBracketsRightSide, isRange, $
  i1, i2, i3, i4, i5, i6, i7, i8

  compile_opt idl2, hidden
@graphic_error

  ; If we have a string then find the object by identifier.
  if (ISA(self.__obj__)) then begin

    if (ISA(i1, 'STRING')) then begin

      if (i1 eq '' || i1 eq '*') then begin
        obj = self.__obj__->Get(ALL=i1 eq '*', $
          ISA='IDLitVisLegendItem', COUNT=count)
      endif

    endif else if N_ELEMENTS(isRange) eq 1 then begin
      
      obj = self.__obj__->Get(/ALL, $
        ISA='IDLitVisLegendItem', COUNT=count)

      if (isRange[0] eq 1) then begin
        ; Index range
        obj = obj[i1[0]:i1[1]:i1[2]]
      endif else begin
        ; Scalar or array of indices.
        obj = obj[i1]
      endelse
      
      count = N_ELEMENTS(obj)
    endif

    if (ISA(count) && count gt 0) then begin
      result = (count eq 1) ? OBJ_NEW() : OBJARR(count)
      for i=0,count-1 do begin
        result[i] = Graphic_GetGraphic(obj[i])
      endfor
      return, result
    endif

  endif

  return, self->Graphic::_overloadBracketsRightSide( $
    isRange, i1, i2, i3, i4, i5, i6, i7, i8)


end


;---------------------------------------------------------------------------
function LegendItem_Properties, ALL=all
  compile_opt idl2, hidden

  myprops = ['HIDE', 'FONT_NAME', 'FONT_SIZE', 'FONT_STYLE', $
    'LABEL', 'SAMPLE_ANGLE', 'SAMPLE_MAGNITUDE', 'SAMPLE_WIDTH', $
    'TEXT_COLOR', 'UNITS']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since this is just a helper graphic.
  return, myprops
end


;---------------------------------------------------------------------------
function LegendItem::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden
  return, LegendItem_Properties(ALL=all)
end


;-------------------------------------------------------------------------
pro LegendItem__define
  compile_opt idl2, hidden
  void = {LegendItem, inherits Graphic}
end


;-------------------------------------------------------------------------
pro Legend__define
  compile_opt idl2, hidden
  void = {Legend, inherits Graphic}
end

