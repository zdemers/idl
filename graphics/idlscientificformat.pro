; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlscientificformat.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Returns number in scientific notation
;
; :Params:
;    axis - not used
;    index - not used
;    number - number to be converted
;-
;-------------------------------------------------------------------------------
function IDLScientific_Format,number
  compile_opt idl2,hidden
  
  if strmid(number,0,1) eq '-' then begin
    neg='-'
    number=strmid(number,1,strlen(number))
  endif else begin
    neg=''
  endelse
  
  for i=0,strlen(number) do begin
    if strmid(number,i,1) eq '0' || strmid(number,i,1) eq '.' then continue
    
    first = strmid(number,i,1)
    rest = i eq strlen(number) ? '' : strmid(number,i+1,strlen(number))
    rest=strjoin(strsplit(rest,'.',/extract),'')
    break
  endfor
  
  return,strjoin([neg,first,'.',rest],'')
end

;-------------------------------------------------------------------------------
function IDLScientific_Trim,number,format=format
  compile_opt idl2,hidden
  
  n=strtrim(number,2)
  if strpos(n,'.') ge 0 then begin
    while strmid(n,strlen(n)-1,1) eq '0' do begin
      n=strmid(n,0,strlen(n)-1)
    endwhile
  endif
  
  if keyword_set(format) then begin
    if strmid(n,strlen(n)-1,1) eq '.' then begin
      ;n=strmid(n,0,strlen(n)-1)
      n+='0'
    endif
  endif
  return,n
end

;-------------------------------------------------------------------------------
function IDLScientificFormat, axis, index, number, EXPONENT=EXPONENT
  compile_opt idl2,hidden
  
  b=''
  e=''
  strnum=IDLScientific_Trim(string(number,format='(g25.16)'))

  if number eq 0 then begin
    return,'0'
  endif 
  
  if strpos(strnum,'e') ge 0 then begin
    b=IDLScientific_Trim(strmid(strnum,0,strpos(strnum,'e')))
    e=strmid(strnum,strpos(strnum,'e')+1,strlen(strnum))
  endif else begin
    i = strpos(strnum,'.') ge 0 ? strlen(strmid(strnum,0,strpos(strnum,'.'))) : strlen(strnum)
    e = abs(number) ge 1 ? strtrim(floor(alog10(abs(strmid(strnum,0,i)))),2) : $
                           strtrim(floor(alog10(abs(strmid(strnum,i-1,strlen(strnum))))),2)
    b = IDLScientific_Format(strnum)
  endelse
    
  return, (~keyword_set(exponent) ? IDLScientific_Trim(b,/format)+'x' : '') + '10!U' + e + '!N'
end