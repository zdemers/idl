;+
; :Description:
;    Create IDL Contour graphic.
;
; :Params:
;    Array :
;    X :
;    Y :
;
; :Keywords:
;    _REF_EXTRA
;    
; :Returns:
;    Object Reference
;
;-
function contour, arg1, arg2, arg3, DEBUG=debug, $
  IRREGULAR=irregular, NODATA=nodata, $
  LAYOUT=layoutIn, PLANAR=planar, _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams eq 0 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'

  if (~hasTestKW && ISA(arg1, 'STRING') && N_ELEMENTS(arg1) eq 1) then begin
    equation = arg1[0]
    arg1 = DINDGEN(4,4)
    arg2 = [-10,-5,5,10]
    arg3 = arg2
    nparams = 3
  endif

  if (nparams eq 2) then $
    MESSAGE, 'Incorrect number of arguments.'

  if (nparams ge 1 && ~ISA(arg1, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'
  if (nparams ge 2 && ~ISA(arg2, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'
  if (nparams ge 3 && ~ISA(arg3, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'

  if (ISA(layoutIn)) then begin
    if N_ELEMENTS(layoutIn) ne 3 then $
      MESSAGE, 'LAYOUT must have 3 elements.'
    layout = layoutIn
  endif else begin
    ; If LAYOUT was not specified, and PLANAR is not 0, then set default layout.
    if (~ISA(planar) || planar ne 0) then $
      layout = [1,1,1]
  endelse
  
  RESOLVE_ROUTINE, 'GRAPHIC', /NO_RECOMPILE

  name = 'Contour'
  case nparams of
    0: Graphic, name, NODATA=nodata, $
      _EXTRA=ex, LAYOUT=layout, PLANAR=planar, GRAPHIC=graphic
    1: Graphic, name, arg1, _EXTRA=ex, LAYOUT=layout, $
      PLANAR=planar, GRAPHIC=graphic, NODATA=nodata
    3: begin
      if (~ISA(irregular)) then begin
        irregular = Graphic_IsIrregular(arg1, arg2, arg3, /CONTOUR, $
          _EXTRA='GRID_UNITS', SPHERE=sphere)
      endif
      if (KEYWORD_SET(irregular)) then begin
        Graphic_GridData, arg1, arg2, arg3, zOut, xOut, yOut, SPHERE=sphere
        Graphic, name, zOut, xOut, yOut, _EXTRA=ex, LAYOUT=layout, $
          PLANAR=planar, GRAPHIC=graphic, NODATA=nodata
      endif else begin
        Graphic, name, arg1, arg2, arg3, _EXTRA=ex, LAYOUT=layout, $
          PLANAR=planar, GRAPHIC=graphic, NODATA=nodata, HIDE=ISA(equation)
      endelse
    end
  endcase

  if (ISA(equation)) then begin
    graphic.SetProperty, EQUATION=equation, HIDE=0
    arg1 = equation
  endif
  
  return, graphic
end
