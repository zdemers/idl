; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/widget_window.pro#2 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create a draw widget.
;
; :Params:
;    wBase
;
; :Keywords:
;    
;
;-

;-------------------------------------------------------------------------
pro widget_window_realize, wDraw

  compile_opt idl2, hidden
  on_error, 2

  ; Retrieve the draw window object reference.
  ; Temporarily turn off the func_get_value (if set) to avoid
  ; calling into the user's function.
  fGetValue = WIDGET_INFO(wDraw, /FUNC_GET_VALUE)
  WIDGET_CONTROL, wDraw, FUNC_GET_VALUE=''
  WIDGET_CONTROL, wDraw, GET_VALUE=oWindow
  if (fGetValue ne '') then $
    WIDGET_CONTROL, wDraw, FUNC_GET_VALUE=fGetValue

  geom = WIDGET_INFO(wDraw, /GEOMETRY)
  autoResize = geom.xsize eq geom.draw_xsize && geom.ysize eq geom.draw_ysize

  winDims = oWindow->GetDimensions(VIRTUAL_DIMENSIONS=virtualDims)

  ; Set the MINIMUM_VIRTUAL_DIMENSIONS to match the virtual dims.
  ; We also manually set the VIRTUAL_DIMENSIONS property, because if the
  ; X/Y_SCROLL_SIZE was the same as the X/YSIZE on the WIDGET_DRAW,
  ; then the VIRTUAL_DIMENSIONS are set to [0,0], which implies
  ; matching the dimensions.
  ; Set AUTO_RESIZE to true if VIRTUAL_DIMENSIONS was passed in.
  oWindow->SetProperty, $
    MINIMUM_VIRTUAL_DIMENSIONS=[0,0], $
    VIRTUAL_DIMENSIONS=virtualDims, $
    AUTO_RESIZE=autoResize

  ;XXX Retrieve our cached state from the PRO_SET_VALUE string.
  state = WIDGET_INFO(wDraw, /PRO_SET_VALUE)
  state = STRTOK(state, ',', /EXTRACT, /PRESERVE_NULL)
  uiID = ULONG(state[0])
  wNotifyRealize = state[1]
  MouseDownHandler = state[2]
  MouseUpHandler = state[3]
  MouseMotionHandler = state[4]
  MouseWheelHandler = state[5]
  KeyboardHandler = state[6]
  SelectionChangeHandler = state[7]
  eventHandler = OBJ_VALID(state[8], /CAST)

  oWindow->SetProperty, $
    EVENT_HANDLER=eventHandler, $
    MOUSE_DOWN_HANDLER=MouseDownHandler, $
    MOUSE_UP_HANDLER=MouseUpHandler, $
    MOUSE_MOTION_HANDLER=MouseMotionHandler, $
    MOUSE_WHEEL_HANDLER=MouseWheelHandler, $
    KEYBOARD_HANDLER=KeyboardHandler, $
    SELECTION_CHANGE_HANDLER=SelectionChangeHandler

  if (uiID ne 0) then oUI = OBJ_VALID(uiID, /CAST)

  ; Cache our UI object if it was passed in.
  if (ISA(oUI)) then begin
    oWindow->SetProperty, UI=oUI
  endif else begin
    oWindow->GetProperty, UI=oUI
    if (~ISA(oUI)) then begin
      oWindow->_CreateTool
      oWindow->GetProperty, UI=oUI
      oTool = oUI->GetTool()
      oTool->_SetCurrentWindow, oWindow
    endif
  endelse

  if (ISA(oUI)) then begin

    ; IDL-68788: If we don't have a group_leader, set it to ourself.
    ; That way, if our widget_window is inside a modal widget then
    ; any dialogs (like the property sheet) will become modal as well.
    oUI->GetProperty, GROUP_LEADER=wGroupLeader
    if (wGroupLeader eq 0) then $
      oUI->SetProperty, GROUP_LEADER=wDraw

    oTool = oUI->GetTool()
    oTool->_SetCurrentWindow, oWindow

    ; Register ourself as a widget with the UI object.
    ; Returns a string containing our identifier.
    strObserverIdentifier = oUI->RegisterWidget(wDraw, 'ToolDraw')
  
    ; Start out with a 1x1 gridded layout.
    oWindow->SetProperty, LAYOUT_INDEX=1
  endif


  ; Call the user's NOTIFY_REALIZE, if it was provided.
  if (wNotifyRealize ne '') then begin
    CALL_PROCEDURE, wNotifyRealize, wDraw
  endif

end


;-------------------------------------------------------------------------
function widget_window, wParent, $
  FRAME=frame, $
  FUNC_GET_VALUE=swallow2, $
  NOTIFY_REALIZE=wNotifyRealize, $
  PRO_SET_VALUE=swallow3, $
  UI=oUI, $
  X_SCROLL_SIZE=xScrollIn, Y_SCROLL_SIZE=yScrollIn, $
  XSIZE=xsizeIn, YSIZE=ysizeIn, $
    EVENT_HANDLER=eventHandler, $
    MOUSE_DOWN_HANDLER=MouseDownHandler, $
    MOUSE_UP_HANDLER=MouseUpHandler, $
    MOUSE_MOTION_HANDLER=MouseMotionHandler, $
    MOUSE_WHEEL_HANDLER=MouseWheelHandler, $
    KEYBOARD_HANDLER=KeyboardHandler, $
    SELECTION_CHANGE_HANDLER=SelectionChangeHandler, $
  _REF_EXTRA=_extra

  compile_opt idl2, hidden

  on_error, 2
  catch, iErr
  if (iErr ne 0) then begin
    catch, /cancel
    msg = !ERROR_STATE.MSG
    ; Replace mention of widget_draw with my own name.
    i = STRPOS(msg, 'WIDGET_DRAW')
    if (i ge 0) then $
      msg = STRMID(msg, 0, i) + 'WIDGET_WINDOW' + STRMID(msg, i+11)
    MESSAGE, msg, /NONAME
  endif

  if (ISA(eventHandler)) then begin
    if (~ISA(eventHandler, 'OBJREF')) then $
      MESSAGE, /NONAME, 'EVENT_HANDLER must be an object.'
  endif else eventHandler = ''

  if (ISA(wNotifyRealize)) then begin
    if (~ISA(wNotifyRealize, 'STRING')) then $
      MESSAGE, /NONAME, 'NOTIFY_REALIZE must be a string.'
  endif else wNotifyRealize = ''
  
  if (ISA(MouseDownHandler)) then begin
    if (~ISA(MouseDownHandler, 'STRING')) then $
      MESSAGE, /NONAME, 'MOUSE_DOWN_HANDLER must be a string.'
  endif else MouseDownHandler = ''
  
  if (ISA(MouseUpHandler)) then begin
    if (~ISA(MouseUpHandler, 'STRING')) then $
      MESSAGE, /NONAME, 'MOUSE_UP_HANDLER must be a string.'
  endif else MouseUpHandler = ''
  
  if (ISA(MouseMotionHandler)) then begin
    if (~ISA(MouseMotionHandler, 'STRING')) then $
      MESSAGE, /NONAME, 'MOUSE_MOTION_HANDLER must be a string.'
  endif else MouseMotionHandler = ''
  
  if (ISA(MouseWheelHandler)) then begin
    if (~ISA(MouseWheelHandler, 'STRING')) then $
      MESSAGE, /NONAME, 'MOUSE_WHEEL_HANDLER must be a string.'
  endif else MouseWheelHandler = ''
  
  if (ISA(KeyboardHandler)) then begin
    if (~ISA(KeyboardHandler, 'STRING')) then $
      MESSAGE, /NONAME, 'KEYBOARD_HANDLER must be a string.'
  endif else KeyboardHandler = ''
  
  if (ISA(SelectionChangeHandler)) then begin
    if (~ISA(SelectionChangeHandler, 'STRING')) then $
      MESSAGE, /NONAME, 'SELECTION_CHANGE_HANDLER must be a string.'
  endif else SelectionChangeHandler = ''
 
  ; Retrieve the system object first - otherwise if we try to do this within
  ; the GraphicsWin::Init we get a crash on the Mac, because of the call
  ; to Get_ScreenSize within IDLitSystem::Init.
  void = _IDLitSys_GetSystem()
  
  xScroll = ISA(xScrollIn) ? xScrollIn[0] : (ISA(xsizeIn) ? xsizeIn[0] : 640)
  yScroll = ISA(yScrollIn) ? yScrollIn[0] : (ISA(ysizeIn) ? ysizeIn[0] : 512)
  xsize = ISA(xsizeIn) ? xsizeIn[0] : xScroll
  ysize = ISA(ysizeIn) ? ysizeIn[0] : yScroll

  appScroll = xScroll lt xsize || yScroll lt ysize

  isFrame = KEYWORD_SET(frame)
  if (isFrame) then begin
    xScroll += 2
    yScroll += 2
  endif

  if (~appScroll) then begin
    xScroll = !NULL
    yScroll = !NULL
  endif

  ;XXX This is awful, but we cannot use the UVALUE of the widget because
  ; the user might have set it. Instead, just shove it into the
  ; PRO_SET_VALUE field as a string.
  state = STRTRIM(OBJ_VALID(oUI, /GET_HEAP_ID),2) + ',' + wNotifyRealize + $
    ',' + MouseDownHandler + $
    ',' + MouseUpHandler + $
    ',' + MouseMotionHandler + $
    ',' + MouseWheelHandler + $
    ',' + KeyboardHandler + $
    ',' + SelectionChangeHandler + $
    ',' + STRTRIM(OBJ_VALID(eventHandler, /GET_HEAP_IDENTIFIER), 2)
  
  ; Drawing area.
  wDraw = WIDGET_DRAW(wParent, $
      CLASSNAME='GraphicsWin', $  ; Component window
      FRAME=isFrame, $
      GRAPHICS_LEVEL=2, $         ; Object graphics
      NOTIFY_REALIZE='widget_window_realize', $
      APP_SCROLL=appScroll, $
      PRO_SET_VALUE=state, $
      RETAIN=0, $
      X_SCROLL_SIZE=xScroll, Y_SCROLL_SIZE=yScroll, $
      SCR_XSIZE=xScroll, SCR_YSIZE=yScroll, $
      XSIZE=xsize, YSIZE=ysize, $
      _STRICT_EXTRA=_extra)

  return, wDraw

end
