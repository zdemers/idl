; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/axis_properties.pro#1 $
;
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Return the axis properties.
;
; :Params:
;    None
;
; :Keywords:
;    
;
;-
;-------------------------------------------------------------------------
function Axis_Properties, ALL=all

  compile_opt idl2, hidden

  myprops = ['AXIS_RANGE', 'ANTIALIAS', 'CLIP', $
    'COLOR', 'COORD_TRANSFORM', 'DATA', $
    'GRIDSTYLE','HIDE','LOCATION','LOG', $
    'MAJOR', 'MINOR', $
    'SHOWTEXT', 'STYLE', 'SUBGRIDSTYLE', 'SUBTICKLEN', $
    'TEXT_COLOR', 'TEXT_ORIENTATION', $
    'TEXTPOS', 'THICK', 'TICKDIR', $
    'TICKFONT_NAME', 'TICKFONT_SIZE', 'TICKFONT_STYLE', $
    'TICKFORMAT', 'TICKINTERVAL', 'TICKLAYOUT', 'TICKLEN', $
    'TICKNAME', 'TICKUNITS', 'TICKVALUES', 'TITLE', $
    'TRANSPARENCY']

  ; Return all valid properties (not just the ones for the PRINT method)
;  if (KEYWORD_SET(all)) then begin
;    myprops = [myprops, ]
;  endif

  return, myprops
end

