;+
; :Description:
;    Create IDL Surface graphic.
;
; :Params:
;    arg1 : optional generic argument
;    arg2 : optional generic argument
;
; :Keywords:
;    _REF_EXTRA
;
;-
function surface, arg1, arg2, arg3, $
  IRREGULAR=irregular, $
  DEBUG=debug, SKIRT=skirt, STYLE=styleIn, _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams eq 0 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'

  if (~hasTestKW && ISA(arg1, 'STRING') && N_ELEMENTS(arg1) eq 1) then begin
    equation = arg1[0]
    arg1 = DINDGEN(4,4)
    arg2 = [-10,-5,5,10]
    arg3 = arg2
    nparams = 3
  endif
  
  if (nparams eq 2) then $
    MESSAGE, 'Incorrect number of arguments.'

  if (nparams ge 1 && ~ISA(arg1, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'
  if (nparams ge 2 && ~ISA(arg2, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'
  if (nparams ge 3 && ~ISA(arg3, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'

  if (ISA(skirt)) then ss = 1

  if (ISA(styleIn)) then begin
    if (ISA(styleIn, 'STRING')) then begin
      case STRCOMPRESS(STRUPCASE(styleIn),/REMOVE) of
      'POINTS': style = 0
      'MESH': style = 1
      'FILLED': style = 2
      'RULEDXZ': style = 3
      'RULEDYZ': style = 4
      'LEGO': style = 5
      'LEGOFILLED': style = 6
      endcase
    endif else $
      style = LONG(styleIn)
  endif

  RESOLVE_ROUTINE, 'GRAPHIC', /NO_RECOMPILE

  name = 'Surface'
  case nparams of
    0: Graphic, name, SHOW_SKIRT=ss, SKIRT=skirt, STYLE=style, $
      _EXTRA=ex, GRAPHIC=graphic
    1: Graphic, name, arg1, SHOW_SKIRT=ss, SKIRT=skirt, STYLE=style, $
      _EXTRA=ex, GRAPHIC=graphic
    3: begin
      if (~ISA(irregular)) then begin
        irregular = Graphic_IsIrregular(arg1, arg2, arg3, $
          _EXTRA='GRID_UNITS', /CONTOUR, SPHERE=sphere)
      endif
      if (KEYWORD_SET(irregular)) then begin
        Graphic_GridData, arg1, arg2, arg3, zOut, xOut, yOut, SPHERE=sphere
        Graphic, name, zOut, xOut, yOut, $
          SHOW_SKIRT=ss, SKIRT=skirt, STYLE=style, $
          _EXTRA=ex, GRAPHIC=graphic
      endif else begin
        Graphic, name, arg1, arg2, arg3, $
          SHOW_SKIRT=ss, SKIRT=skirt, STYLE=style, $
          _EXTRA=ex, GRAPHIC=graphic
      endelse
      end
  endcase

  if (ISA(equation)) then begin
    graphic.SetProperty, EQUATION=equation, HIDE=0
    arg1 = equation
  endif
  
  return, graphic
end
