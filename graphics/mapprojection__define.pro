; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/mapprojection__define.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create a Map.
;
; :Params:
;    Projection
;
; :Keywords:
;    
;
;-

;-------------------------------------------------------------------------
pro MapProjection::GetProperty, $
  CLIP=clip, $
  FILL_COLOR=fillColor, $
  HIDE=hide, $
  _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error
  
  if ARG_PRESENT(clip) || ARG_PRESENT(fillColor) || ARG_PRESENT(hide) then begin
    oGrid = self['map grid']
    if ~ISA(oGrid) then MESSAGE, 'Unable to retrieve the map grid.'
    oGrid->GetProperty, CLIP=clip, $
      FILL_COLOR=fillColor, $
      HIDE=hide
  endif

  if (ISA(ex)) then self->Graphic::GetProperty, _EXTRA=ex
end


;-------------------------------------------------------------------------
pro MapProjection::SetProperty, $
  CLIP=clip, $
  FILL_COLOR=fillColor, $
  HIDE=hide, $
  _EXTRA=ex

  compile_opt idl2, hidden
@graphic_error
  
  if ISA(clip) || ISA(fillColor) || ISA(hide) then begin
    oGrid = self['map grid']
    if ~ISA(oGrid) then MESSAGE, 'Unable to retrieve the map grid.'
    oGrid->SetProperty, CLIP=clip, $
      FILL_COLOR=fillColor, $
      HIDE=hide
  endif

  if (ISA(ex)) then self->Graphic::SetProperty, _EXTRA=ex
end


;---------------------------------------------------------------------------
function MapProjection::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = MapProjection_Properties(ALL=all)

  ; Return all valid properties (not just the ones for the PRINT method)
  if KEYWORD_SET(all) then begin
    ; Add MapGrid properties since these can be set on initialization.
    myprops = [myprops, MapGrid_Properties(/ALL)]
  endif
  
  return, [myprops, self->Graphic::QueryProperty(ALL=all)]
end


;---------------------------------------------------------------------------
function MapProjection::GetMapStructure
  compile_opt idl2, hidden
  
  if ISA(self.__obj__) then begin
    return, self.__obj__->_GetMapStructure()
  endif
  return, 0
end


;-------------------------------------------------------------------------
pro MapProjection__define
  compile_opt idl2, hidden
  void = {MapProjection, inherits Graphic}
end

