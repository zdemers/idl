; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/axis__define.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create an Axis.
;
; :Params:
;    Projection
;
; :Keywords:
;    
;
;-
;-------------------------------------------------------------------------
; CT: Do not put DIRECTION and TARGET in _extra, because they
; are Init-only keywords, and VerifyProperty will throw an error.
;
function Axis::Init, oObj, $
    DIRECTION=direction, $
    TARGET=target, $
    TICKNAME=tickName, $
    TICKFONT_NAME=tickfontName, $
    TICKFONT_STYLE=tickfontStyle, $
    TICKFONT_SIZE=tickfontSize, $
    UNDOCUMENTED=undocumented, $  ; allow undocumented properties
    _REF_EXTRA=_extra
  
  compile_opt idl2, hidden
@graphic_error

  if (ISA(oObj)) then $
    return, self->Graphic::Init(oObj)

  ; Check for unknown or illegal properties.
  if (ISA(_extra) && ~KEYWORD_SET(undocumented)) then $
    self->VerifyProperty, _extra

  oCmd = self->_Create(oAxis, $
    DIRECTION=direction, $
    TARGET=target, $
    _EXTRA=_extra, /NO_SELECT)
  if (ISA(oAxis)) then begin
    void = self->Graphic::Init(oAxis)
    oTool = oAxis->GetTool()
    if (OBJ_VALID(oTool)) then $
      oTool->_TransactCommand, oCmd
      
    ; Set the TICKNAME property separately to avoid
    ; double setting the value during creation
    self->SetProperty, TICKNAME=tickName, $
      TICKFONT_NAME=tickfontName, $
      TICKFONT_STYLE=tickfontStyle, $
      TICKFONT_SIZE=tickfontSize
      
    ; Deselect the new axi
;    self->Select, /Clear
    
    return, 1
  endif
  
  return, 0
  
end

;-------------------------------------------------------------------------
function Axis::_FilterDataSpaceIdentifiers, targets, oTool

  compile_opt idl2, hidden

  dataspaces = []
  foreach t, targets do begin
    startpos = STRPOS( t, "DATA SPACE" )
    if STRPOS( t, "/", startpos ) EQ -1 THEN BEGIN
      oDS = oTool->GetByIdentifier(t)
      ; Must not have a Map Projection to be a valid target
      if (~KEYWORD_SET(oDS.GetProjection())) THEN $
        dataspaces = [dataspaces, t]
    ENDIF
  endforeach

  return, dataspaces

end

;-------------------------------------------------------------------------
function Axis::_Create, oAxis, $
        DIRECTION=direction, $
        TICKLEN=ticklen, $
        TICKDIR=tickdir, $
        TEXTPOS=textpos, $
        LOCATION=location, $
        TITLE=title, $
        TARGET=targetIn, $
        _EXTRA=_extra

    compile_opt idl2, hidden

    nTargets = 0
    ; Get tool
    if (MIN(OBJ_VALID(targetIn) eq 1)) then begin
      oTool = targetIn.GetTool()
      oTargets = []
      if (ISA(oTool)) then begin
        for i=0,N_ELEMENTS(targetIn)-1 do $
          oTargets = [oTargets, $
            oTool->GetByIdentifier(targetIn[i]->GetFullIdentifier())]
          nTargets = N_ELEMENTS(oTargets)
      endif 
    endif else begin
      !NULL = iGetCurrent(TOOL=oTool)
      if (ISA(oTool)) then begin
        ; Retrieve the current selected item(s).
        oTargets = oTool->GetSelectedItems(count=nTargets)
        if (nTargets gt 0) then begin
          ; If all selected items are within a single dataspace
          ; then we have a valid target, not including map projections
          dsIDs = []
          foreach oTarg, oTargets do begin
            oDS = oTarg.GetDataSpace( )
            DSIdent = oDS.GetFullIDentifier( )
            if (~KEYWORD_SET( oDS.GetProjection())) then begin
;              if ( n_elements( dsIDs ) eq 0 ) then begin
;                dsIDs = [DSIdent]
;              endif else begin
                indices = where( dsIDs eq DSIDent, count )
                if ( count eq 0 ) then dsIDs = [dsIDs, DSIdent]
;              endelse
            endif
          endforeach
        endif else begin ; Nothing selected - find a suitable dataspace
          ;; Get window
          oWin = oTool->GetCurrentWindow()
          oView = OBJ_VALID(oWin) ? oWin->GetCurrentView() : []
          ; If nothing is selected and there is only one dataspace
          ; then set that data space as the target 
          if (OBJ_VALID(oView)) then begin
            dsID_List = (oView->FindIdentifiers('*DATA SPACE*'))
            dsIDs = self._FilterDataSpaceIdentifiers( dsID_List, oTool )
          endif
        endelse
      endif
      
      ; Only one valid target is allowed
      if (N_ELEMENTS(dsIDs) eq 1) then begin
        oTargets = oTool->GetByIdentifier(dsIDs[0])
        nTargets = 1
      endif else begin
        nTargets = N_ELEMENTS( dsIDs )
      endelse
      
    endelse
  
    if (nTargets eq 0) then begin
      message, 'Cannot insert axis: no valid targets found.'
      return, OBJ_NEW()
    endif
    
    if (nTargets gt 1) then begin
      message, 'Cannot insert axis: too many targets found.'
      return, OBJ_NEW()
    endif
    
    oDataSpace = oTargets[0]->GetDataSpace( )
    if (~ISA(oDataSpace, 'IDLITVISNORMDATASPACE')) then $
      message, 'Cannot insert axis: no valid targets found.'

    ; Prepare the service that will create the axis visualization.
    oCreate = oTool->GetService("CREATE_VISUALIZATION")
    if (not OBJ_VALID(oCreate)) then $
        return, OBJ_NEW();

    oDataSpaceUnNorm = oDataSpace->GetDataSpace(/UNNORMALIZED)
    is3D = oDataSpaceUnNorm->Is3D()
    oAxes = (oDataSpaceUnNorm->Get(/ALL, ISA='IDLitVisDataAxes'))[0]
    if (~OBJ_VALID(oAxes)) then $
        return, OBJ_NEW();
    destination = oAxes->GetFullIdentifier()
    oAxisDesc = oTool->GetVisualization("AXIS")

    oAxes->GetProperty, XLOG=xlog, YLOG=ylog, ZLOG=zlog, $
        XRANGE=xrange, YRANGE=yrange, ZRANGE=zrange, $
        XTICKLEN=xticklen, YTICKLEN=yticklen, ZTICKLEN=zticklen
    if (xlog) then xrange = 10^xrange
    if (ylog) then yrange = 10^yrange
    if (zlog) then zrange = 10^zrange

    if N_ELEMENTS(location) eq 0 then begin
      case direction of
      0: begin     ; X axis
           range = xRange
           location = [0, yRange[0]-(yRange[1]-yRange[0])/10.0, $
                           zRange[0]]
      end
      1: begin     ; Y axis
           range = yRange
           location = [xRange[0]-(xRange[1]-xRange[0])/10.0, 0, $
                       zRange[0]]
      end
      2: begin     ; Z axis
           range = zRange
           location = [xRange[0]-(xRange[1]-xRange[0])/10.0, $
                       yRange[0], 0]
      end
      else:
      endcase
    endif
    
    ; Convert location if specified
    case size(location,/type) of
      7: begin
        xl=xRange[0]
        yl=yRange[0]
        zl=zRange[0]
        case strlowcase(location[0]) of
          'right': begin
            case direction of
              0: begin     ; X axis
                yl=yRange[-1]  
              end
              1: begin     ; Y axis
                xl=xRange[-1]
              end
              2: begin     ; Z axis
                xl=xRange[-1]
                yl=yRange[-1]
              end
              else:
            endcase
            if n_elements(tickdir) eq 0 then tickdir=1
            if n_elements(textpos) eq 0 then textpos=1
          end
          'left': begin
            case direction of
              0: begin     ; X axis
                yl=yRange[0]  
              end
              1: begin     ; Y axis
                xl=xRange[0]
              end
              2: begin     ; Z axis
                xl=xRange[0]
                yl=yRange[0]
              end
              else:
            endcase
          end
          'top': begin
            case direction of
              0: begin     ; X axis
                yl=yRange[-1]  
              end
              1: begin     ; Y axis
                xl=xRange[-1]
              end
              2: begin     ; Z axis
                xl=xRange[-1]
                yl=yRange[-1]
              end
              else:
            endcase
            if n_elements(tickdir) eq 0 then tickdir=1
            if n_elements(textpos) eq 0 then textpos=1
          end
          'bottom': begin
            case direction of
              0: begin     ; X axis
                yl=yRange[0]  
              end
              1: begin     ; Y axis
                xl=xRange[0]
              end
              2: begin     ; Z axis
                xl=xRange[0]
                yl=yRange[0]
              end
              else:
            endcase
          end
          'center': begin
            case direction of
              0: begin     ; X axis
                yl=median(yRange,/EVEN)
                zl=median(zRange,/EVEN)  
              end
              1: begin     ; Y axis
                xl=median(xRange,/EVEN)
                zl=median(zRange,/EVEN)
              end
              2: begin     ; Z axis
                xl=median(xRange,/EVEN)
                yl=median(yRange,/EVEN)
              end
              else:
            endcase
          end
          else: begin
            message,/noname,'Invalid location'
          end
        endcase
        if n_elements(location) eq 2 then begin
          case strlowcase(location[1]) of
            'right': begin
              case direction of
                0: begin     ; X axis
                  zl=zRange[-1]  
                end
                1: begin     ; Y axis
                  zl=zRange[-1]
                end
                2: begin     ; Z axis
                  yl=yRange[-1]
                end
                else:
              endcase
            end
            'left': begin
              case direction of
                0: begin     ; X axis
                  zl=zRange[0]  
                end
                1: begin     ; Y axis
                  zl=zRange[0]
                end
                2: begin     ; Z axis
                  yl=yRange[0]
                end
                else:
              endcase
            end
            'top': begin
              case direction of
                0: begin     ; X axis
                  zl=zRange[-1]  
                end
                1: begin     ; Y axis
                  zl=zRange[-1]
                end
                2: begin     ; Z axis
                  yl=yRange[-1]
                end
                else:
              endcase
            end
            'bottom': begin
              case direction of
                0: begin     ; X axis
                  zl=zRange[0]  
                end
                1: begin     ; Y axis
                  zl=zRange[0]
                end
                2: begin     ; Z axis
                  yl=yRange[0]
                end
                else:
              endcase
            end
            'center': begin
              case direction of
                0: begin     ; X axis
                  zl=median(zRange,/EVEN)  
                end
                1: begin     ; Y axis
                  zl=median(zRange,/EVEN)
                end
                2: begin     ; Z axis
                  yl=median(yRange,/EVEN)
                end
                else:
              endcase
            end
            else: begin
              message,/noname,'Invalid location'
            end
          endcase
        endif
        location=[xl,yl,zl]
      end
      else: begin
        if n_elements(location) eq 1 then begin
          case direction of
            0: begin     ; X axis
              location = [0, location, zRange[0]]
            end
            1: begin     ; Y axis
              location = [location, 0, zRange[0]]
            end
            2: begin     ; Z axis
              location = [location, yRange[0], 0]
            end
            else:
          endcase
        endif else if n_elements(location) eq 2 then begin
          case direction of
            0: begin     ; X axis
              location = is3D ? [0, location[0], location[1]] : [0, location[1], 0]
            end
            1: begin     ; Y axis
              location = is3D ? [location[0], 0, location[1]] : [location[0], 0, 0]
            end
            2: begin     ; Z axis
              location = [location[0], location[1], 0]
            end
            else:
          endcase
        endif
      end
    endcase

    if ~ISA(ticklen) then begin
      ; Set the tick length to the same tick length as the existing axes.
      ; That way the lengths look consistent.
      if (~ISA(xticklen)) then xticklen = 0.05
      case (direction) of
        0: ticklen = xticklen
        1: ticklen = yticklen
        2: ticklen = ISA(zticklen) ? zticklen : xticklen
        else:
      endcase
    endif

    oTool->DisableUpdates, PREVIOUSLY_DISABLED=wasDisabled

    oCommand = oCreate->_Create( $
                        oAxisDesc, $
                        DESTINATION=destination, $
                        DIRECTION=direction, $
                        ID_VISUALIZATION=idVis, $
                        RANGE = range, $
                        LOCATION = location, $
                        TICKLEN=ticklen, $ ; initial default
                        /MANIPULATOR_TARGET, $
                        /NO_TRANSACT, $
                        AXIS_TITLE=title, $
                        TICKDIR=tickdir, $
                        TEXTPOS=textpos, $
                        _EXTRA=_extra)

    oAxis = oTool->GetByIdentifier(idVis)
    if OBJ_VALID(oAxis) then oAxes->Aggregate, oAxis

    oAxes->_UpdateAxesRanges

    if (~wasDisabled) then $
        oTool->EnableUpdates

    return, oCommand

end


;-------------------------------------------------------------------------
pro Axis::GetProperty, TITLE=title, $
    TICKFONT_NAME=tickfontName, $
    TICKFONT_STYLE=tickfontStyle, $
    TICKFONT_SIZE=tickfontSize, $
    _REF_EXTRA=_extra
    
    compile_opt idl2, hidden

    @graphic_error
    
    if ARG_PRESENT(title) then begin
        oAxis = self.__obj__
        oAxis->GetProperty, AXIS_TITLE=title
    endif
    
    if ARG_PRESENT(tickfontName) then $
      self.Graphic::GetProperty, FONT_NAME=tickfontName, /UNDOCUMENTED

    if ARG_PRESENT(tickfontStyle) then $
      self.Graphic::GetProperty, FONT_STYLE=tickfontStyle, /UNDOCUMENTED

    if ARG_PRESENT(tickfontSize) then $
      self.Graphic::GetProperty, FONT_SIZE=tickfontSize, /UNDOCUMENTED

    if (N_ELEMENTS(_extra) gt 0) then $
      self.Graphic::GetProperty, _EXTRA=_extra

end



;-------------------------------------------------------------------------
pro Axis::SetProperty, TITLE=title, $
    TICKFONT_NAME=tickfontName, $
    TICKFONT_STYLE=tickfontStyle, $
    TICKFONT_SIZE=tickfontSize, $
    _EXTRA=_extra
    
    compile_opt idl2, hidden

    @graphic_error
    
    if (N_ELEMENTS(title) eq 1) then begin
        oAxis = self.__obj__
        oAxis->SetProperty, AXIS_TITLE=title
    endif
    
    if (ISA(tickfontName)) then $
      self.Graphic::SetProperty, FONT_NAME=tickfontName, /UNDOCUMENTED

    if (ISA(tickfontStyle)) then $
      self.Graphic::SetProperty, FONT_STYLE=tickfontStyle, /UNDOCUMENTED

    if (ISA(tickfontSize)) then $
      self.Graphic::SetProperty, FONT_SIZE=tickfontSize, /UNDOCUMENTED

    if (N_ELEMENTS(_extra) gt 0) then $
      self.Graphic::SetProperty, _EXTRA=_extra

    if N_ELEMENTS(_extra) eq 0 then begin
        oTool = self.__obj__->GetTool()
        if (OBJ_VALID(oTool)) then begin
            oTool->RefreshCurrentWindow
        endif
    endif

end


;---------------------------------------------------------------------------
function Axis::QueryProperty, propNames, ALL=all
  compile_opt idl2, hidden

  myprops = Axis_Properties(ALL=all)

  ; Add other properties outside of Axis_Properties,
  ; so for example you can't set [XYZ]NAME.
  myprops = [myprops, 'DIRECTION', 'NAME', 'XRANGE', 'YRANGE', 'ZRANGE']

  ; Return all valid properties (not just the ones for the PRINT method)
  if (KEYWORD_SET(all)) then $
    myprops = [myprops, 'WINDOW']

  ; Do not return Graphic's properties, since Axis is just an annotation.
  return, myprops
end


;-------------------------------------------------------------------------
pro Axis__define
  compile_opt idl2, hidden
  void = {Axis, inherits Graphic}
end

