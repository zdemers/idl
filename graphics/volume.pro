;+
; :Description:
;    Create IDL Volume graphic.
;
; :Params:
;    arg1 : optional generic argument
;    arg2 : optional generic argument
;
; :Keywords:
;    _REF_EXTRA
;
;-
function volume, arg1, arg2, arg3, arg4, DEBUG=debug, _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams eq 0 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'

  switch (nparams) of
  4: if ~ISA(arg4, /ARRAY) then MESSAGE, 'Input must be an array.'
  3: if ~ISA(arg3, /ARRAY) then MESSAGE, 'Input must be an array.'
  2: if ~ISA(arg2, /ARRAY) then MESSAGE, 'Input must be an array.'
  1: if ~ISA(arg1, /ARRAY) then MESSAGE, 'Input must be an array.'
  endswitch

  name = 'Volume'
  case nparams of
    0: Graphic, name, _EXTRA=ex, GRAPHIC=graphic
    1: Graphic, name, arg1, _EXTRA=ex, GRAPHIC=graphic
    2: Graphic, name, arg1, arg2, _EXTRA=ex, GRAPHIC=graphic
    3: Graphic, name, arg1, arg2, arg3, _EXTRA=ex, GRAPHIC=graphic
    4: Graphic, name, arg1, arg2, arg3, arg4, _EXTRA=ex, GRAPHIC=graphic
  endcase

  return, graphic
end
