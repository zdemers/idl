; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/graphicseventadapter__define.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.

;----------------------------------------------------------------------------
; Purpose:
;   An Adapter class for use with the EVENT_HANDLER property
;   on the Graphics Window class. Implements all of the event handler
;   methods and returns the default value of 1.
;

;----------------------------------------------------------------------------
function GraphicsEventAdapter::Init
  compile_opt idl2, hidden
  return, self->IDL_Object::Init()
end

;----------------------------------------------------------------------------
pro GraphicsEventAdapter::Cleanup
  compile_opt idl2, hidden
  self->IDL_Object::Cleanup
end

;----------------------------------------------------------------------------
function GraphicsEventAdapter::MouseDown, win, x, y, button, keymods, clicks
  compile_opt idl2, hidden
  return, 1
end

;----------------------------------------------------------------------------
function GraphicsEventAdapter::MouseUp, win, x, y, button
  compile_opt idl2, hidden
  return, 1
end

;----------------------------------------------------------------------------
function GraphicsEventAdapter::MouseMotion, win, x, y, keymods
  compile_opt idl2, hidden
  return, 1
end

;----------------------------------------------------------------------------
function GraphicsEventAdapter::MouseWheel, win, x, y, delta, keymods
  compile_opt idl2, hidden
  return, 1
end

;----------------------------------------------------------------------------
function GraphicsEventAdapter::KeyHandler, Window, IsASCII, Character, $
  KeyValue, X, Y, Press, Release, KeyMods
  compile_opt idl2, hidden
  return, 1
end

;----------------------------------------------------------------------------
function GraphicsEventAdapter::SelectChange, Window, Graphic, Mode, WasSelected
  compile_opt idl2, hidden
  return, 1
end

;----------------------------------------------------------------------------
pro GraphicsEventAdapter__define
  compile_opt idl2, hidden
  void = {GraphicsEventAdapter, inherits IDL_Object}
end
