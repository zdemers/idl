; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/tex2idl.pro#1 $
;
; Copyright (c) 2011-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.

function tex2idl_unicodetable

  compile_opt idl2, hidden

  result = [ $
  ['34', '2200', '&forall;'], $
  ['36', '2203', '&exist;'], $
  ['38', '0026', '&amp;'], $
  ['39', '220B', '&ni;'], $
  ['42', '2217', '&lowast;'], $
  ['45', '2212', '&minus;'], $
  ['60', '003C', '&lt;'], $
  ['62', '003E', '&gt;'], $
  ['64', '2245', '&cong;'], $
  ['65', '0391', '&Alpha;'], $
  ['66', '0392', '&Beta;'], $
  ['67', '03A7', '&Chi;'], $
  ['68', '0394', '&Delta;'], $
  ['69', '0395', '&Epsilon;'], $
  ['70', '03A6', '&Phi;'], $
  ['71', '0393', '&Gamma;'], $
  ['72', '0397', '&Eta;'], $
  ['73', '0399', '&Iota;'], $
  ['74', '03D1', '&thetasym;'], $
  ['75', '039A', '&Kappa;'], $
  ['76', '039B', '&Lambda;'], $
  ['77', '039C', '&Mu;'], $
  ['78', '039D', '&Nu;'], $
  ['79', '039F', '&Omicron;'], $
  ['80', '03A0', '&Pi;'], $
  ['81', '0398', '&Theta;'], $
  ['82', '03A1', '&Rho;'], $
  ['83', '03A3', '&Sigma;'], $
  ['84', '03A4', '&Tau;'], $
  ['85', '03A5', '&Upsilon;'], $
  ['86', '03C2', '&sigmaf;'], $
  ['87', '03A9', '&Omega;'], $
  ['88', '039E', '&Xi;'], $
  ['89', '03A8', '&Psi;'], $
  ['90', '0396', '&Zeta;'], $
  ['92', '2234', '&there4;'], $
  ['94', '22A5', '&perp;'], $
  ['96', '00AF', '&oline;'], $
  ['97', '03B1', '&alpha;'], $
  ['98', '03B2', '&beta;'], $
  ['99', '03C7', '&chi;'], $
  ['100', '03B4', '&delta;'], $
  ['101', '03B5', '&epsilon;'], $
  ['102', '03D5', '&phi;'], $
  ['103', '03B3', '&gamma;'], $
  ['104', '03B7', '&eta;'], $
  ['105', '03B9', '&iota;'], $
  ['106', '03C6', '&#x3d5;'], $
  ['107', '03BA', '&kappa;'], $
  ['108', '03BB', '&lambda;'], $
  ['109', '03BC', '&mu;'], $
  ['110', '03BD', '&nu;'], $
  ['111', '03BF', '&omicron;'], $
  ['112', '03C0', '&pi;'], $
  ['113', '03B8', '&theta;'], $
  ['114', '03C1', '&rho;'], $
  ['115', '03C3', '&sigma;'], $
  ['116', '03C4', '&tau;'], $
  ['117', '03C5', '&upsilon;'], $
  ['118', '03D6', '&piv;'], $
  ['119', '03C9', '&omega;'], $
  ['120', '03BE', '&xi;'], $
  ['121', '03C8', '&psi;'], $
  ['122', '03B6', '&zeta;'], $
  ['126', '223C', '&sim;'], $
  ['160', '20AC', '&euro;'], $
  ['161', '03D2', '&upsih;'], $
  ['162', '2032', '&prime;'], $
  ['163', '2264', '&le;'], $
  ['164', '2044', '&frasl;'], $
  ['165', '221E', '&infin;'], $
  ['166', '0192', '&fnof;'], $
  ['167', '2663', '&clubs;'], $
  ['168', '2666', '&diams;'], $
  ['169', '2665', '&hearts;'], $
  ['170', '2660', '&spades;'], $
  ['171', '2194', '&harr;'], $
  ['172', '2190', '&larr;'], $
  ['173', '2191', '&uarr;'], $
  ['174', '2192', '&rarr;'], $
  ['175', '2193', '&darr;'], $
  ['176', '00B0', '&deg;'], $
  ['177', '00B1', '&plusmn;'], $
  ['178', '2033', '&Prime;'], $
  ['179', '2265', '&ge;'], $
  ['180', '00D7', '&times;'], $
  ['181', '221D', '&prop;'], $
  ['182', '2202', '&part;'], $
  ['183', '2022', '&bull;'], $
  ['184', '00F7', '&divide;'], $
  ['185', '2260', '&ne;'], $
  ['186', '2261', '&equiv;'], $
  ['187', '2248', '&asymp;'], $
  ['188', '2026', '&hellip;'], $
  ['189', '23D0', ''], $
  ['190', '23AF', ''], $
  ['191', '21B5', '&crarr;'], $
  ['192', '2135', '&alefsym;'], $
  ['193', '2111', '&image;'], $
  ['194', '211C', '&real;'], $
  ['195', '2118', '&weierp;'], $
  ['196', '2297', '&otimes;'], $
  ['197', '2295', '&oplus;'], $
  ['198', '2205', '&empty;'], $
  ['199', '2229', '&cap;'], $
  ['200', '222A', '&cup;'], $
  ['201', '2283', '&sup;'], $
  ['202', '2287', '&supe;'], $
  ['203', '2284', '&nsub;'], $
  ['204', '2282', '&sub;'], $
  ['205', '2286', '&sube;'], $
  ['206', '2208', '&isin;'], $
  ['207', '2209', '&notin;'], $
  ['208', '2220', '&ang;'], $
  ['209', '2207', '&nabla;'], $
  ['210', '00AE', '&reg;'], $
  ['211', '00A9', '&copy;'], $
  ['212', '2122', '&trade;'], $
  ['213', '220F', '&prod;'], $
  ['214', '221A', '&radic;'], $
  ['215', '22C5', '&sdot;'], $
  ['216', '00AC', '&not;'], $
  ['217', '2227', '&and;'], $
  ['218', '2228', '&or;'], $
  ['219', '21D4', '&hArr;'], $
  ['220', '21D0', '&lArr;'], $
  ['221', '21D1', '&uArr;'], $
  ['222', '21D2', '&rArr;'], $
  ['223', '21D3', '&dArr;'], $
  ['224', '25CA', '&loz;'], $
  ['225', '2329', '&lang;'], $
  ['226', '00AE', '&reg;'], $
  ['227', '00A9', '&copy;'], $
  ['228', '2122', '&trade;'], $
  ['229', '2211', '&sum;'], $
  ['230', '239B', ''], $
  ['231', '239C', ''], $
  ['232', '239D', ''], $
  ['233', '23A1', '&lceil;'], $
  ['234', '23A2', ''], $
  ['235', '23A3', '&lfloor;'], $
  ['236', '23A7', ''], $
  ['237', '23A8', ''], $
  ['238', '23A9', ''], $
  ['239', '23AA', ''], $
  ['240', '2318', ''], $
  ['241', '232A', '&rang;'], $
  ['242', '222B', '&int;'], $
  ['243', '2320', ''], $
  ['244', '23AE', ''], $
  ['245', '2321', ''], $
  ['246', '239E', ''], $
  ['247', '239F', ''], $
  ['248', '23A0', ''], $
  ['249', '23A4', '&rceil;'], $
  ['250', '23A5', ''], $
  ['251', '23A6', '&rfloor;'], $
  ['252', '23AB', ''], $
  ['253', '23AC', ''], $
  ['254', '23AD', '']]

  return, result
end


;-------------------------------------------------------------------------
; Returns a string array with pairs of strings - the first string
; is the TeX code and the second string is the embedded formatting string.
;
function tex2idl_symboltable

  compile_opt idl2, hidden

  symbolTable = [ $
    '\\alpha', 'a', $
    '\\beta',  'b', $
    '\\chi',   'c', $
    '\\delta', 'd', $
    '\\epsilon','e', $
    '\\eta',   'h', $
    '\\gamma', 'g', $
    '\\iota',  'i', $
    '\\kappa', 'k', $
    '\\lambda','l', $
    '\\mu',    'm', $
    '\\nu',    'n', $
    '\\omega', 'w', $
    '\\omicron','o', $
    '\\phi',   'f', $
    '\\pi',    'p', $
    '\\psi',   'y', $
    '\\rho',   'r', $
    '\\sigma', 's', $
    '\\tau',   't', $
    '\\theta', 'q', $
    '\\upsilon','u', $
    '\\xi',    'x', $
    '\\zeta',  'z', $
    ;
    '\\Alpha', 'A', $
    '\\Beta',  'B', $
    '\\Chi',   'C', $
    '\\Delta', 'D', $
    '\\Epsilon','E', $
    '\\Eta',   'H', $
    '\\Gamma', 'G', $
    '\\Iota',  'I', $
    '\\Kappa', 'K', $
    '\\Lambda','L', $
    '\\Mu',    'M', $
    '\\Nu',    'N', $
    '\\Omega', 'W', $
    '\\Omicron','O', $
    '\\Phi',   'F', $
    '\\Pi',    'P', $
    '\\Psi',   'Y', $
    '\\Rho',   'R', $
    '\\Sigma', 'S', $
    '\\Tau',   'T', $
    '\\Theta', 'Q', $
    '\\Upsilon',String(161b), $
    '\\Xi',    'X', $
    '\\Zeta',  'Z', $
    '\\varepsilon','e', $
    '\\varphi', 'j', $
    '\\varpi', 'v', $
    '\\varsigma','V', $
    '\\vartheta', 'J', $
    ;
    '\\aleph', String(192b), $
    '\\angle', String(208b), $
    '\\approxeq','@', $
    '\\approx', String(187b), $
    '\\bot',   '\^',  $
    '\\bullet', String(183b), $
    '\\cap', String(199b), $
    '\\cdot', String(215b), $
    '\\circledR', String(210b), $
    '\\circ', String(176b), $
    '\\clubsuit', String(167b), $
    '\\copyright', String(211b), $
    '\\cup', String(200b), $
    '\\deg', String(176b), $
    '\\diamondsuit', String(168b), $
    '\\diamond', String(224b), $
    '\\div', String(184b), $
    '\\downarrow', String(175b), $
    '\\Downarrow', String(223b), $
    '\\emptyset', String(198b), $
    '\\equiv', String(186b), $
    '\\exists', '$', $
    '\\forall', '"', $
    '\\geq', String(179b), $
    '\\heartsuit', String(169b), $
    '\\Im', String(193b), $
    '\\infty', String(165b), $
    '\\int', String(242b), $
    '\\in', String(206b), $
    '\\langle', String(225b), $
    '\\lceil', String(233b), $
    '\\ldots', String(188b), $
    '\\leftarrow', String(172b), $
    '\\Leftarrow', String(220b), $
    '\\leftrightarrow', String(171b), $
    '\\Leftrightarrow', String(219b), $
    '\\leq', String(163b), $
    '\\lfloor', String(235b), $
    '\\mid', String(189b), $
    '\\nabla', String(209b), $
    '\\neq', String(185b), $
    '\\ni', '''', $
    '\\notin', String(207b), $
    '\\nsubset', String(203b), $
    '\\oplus', String(197b), $
    '\\otimes', String(196b), $
    '\\overline', '!M`', $
    '\\partial', String(182b), $
    '\\perp', '\^', $
    '\\pm', String(177b), $
    '\\"',String(178b), $
    '\\primeprime',String(178b), $
    "\\'",String(162b), $
    '\\prime',String(162b), $
    '\\prod', String(213b), $
    '\\propto', String(181b), $
    '\\rangle', String(241b), $
    '\\rceil', String(249b), $
    '\\Re', String(194b), $
    '\\rfloor', String(251b), $
    '\\rightarrow', String(174b), $
    '\\Rightarrow', String(222b), $
    '\\sim','~', $
    '\\slash', String(164b), $
    '\\spadesuit', String(170b), $
    '\\sqrt', '!S' + String(214b)+'!R!M`', $
    '\\subseteq', String(205b), $
    '\\subset', String(204b), $
    '\\sum', String(229b), $
    '\\supseteq', String(202b), $
    '\\supset', String(201b), $
    '\\therefore', '\', $
    '\\times', String(180b), $
    '\\uparrow', String(173b), $
    '\\Uparrow', String(221b), $
    '\\vee', String(218b), $
    '\\wedge', String(217b), $
    '\\wp', String(195b), $
    '-', '-' $
    ]

  symbolTable[1:*:2] = '!M' + symbolTable[1:*:2]
  
  symbolTable = [ $
    symbolTable, $
    '\\ ', String(160b), $ ; fixed-width space
    '\\aa', String(229b), $
    '\\AA', String(197b), $
    '\\ae', String(230b), $
    '\\AE', String(198b), $
    '\\DH', String(208b), $
    '\\dh', String(240b), $
    '\\o', String(248b), $
    '\\O', String(216b), $
    '\\ss', String(223b), $
    '\\TH', String(222b), $
    '\\th', String(254b), $
    '\\bf', '!4', $
    '\\rm', '!X', $
    '\\it', '!5', $
    '\\bi', '!6' $
    ]
  
  unicode = [ $
    '\\dagger', '2020', $
    '\\ddagger', '2021', $
    '\\permil', '2030' $
    ]
  unicode[1:*:2] = '!Z(' + unicode[1:*:2] + ')'

  ; These require !10 in front to switch to the DejaVuSans font.
  mathUnicode = [ $
    '\\Sun', '2609', $
    '\\Mercury', '263f', $
    '\\Venus', '2640', $
    '\\Earth', '2641', $
    '\\Mars', '2642', $
    '\\Jupiter', '2643', $
    '\\Saturn', '2644', $
    '\\Uranus', '2645', $
    '\\Neptune', '2646', $
    '\\Pluto', '2647', $
    '\\Moon', '263d', $
    '\\rightmoon', '263d', $
    '\\leftmoon', '263e', $
    '\\boxcheck', '2611', $
    '\\boxx', '2612', $
    '\\box', '2610', $
    '\\cdots', '22EF', $
    '\\ddots', '22F1', $
    '\\ell', '2113', $
    '\\flat', '266D', $
    '\\hbar', '210F', $
    '\\mho', '2127', $
    '\\natural', '266E', $
    '\\odot', '2299', $
    '\\oint', '222E', $
    '\\ominus', '2296', $
    '\\oslash', '2298', $
    '\\parallel', '2225', $
    '\\\|', '2225', $
    '\\notparallel', '2226', $
    '\\sharp', '266F', $
    '\\vdots', '22EE', $
    '\\Arrrr', '2620', $
    '\\Frosty', '2603' $
    ]
  mathUnicode[1:*:2] = '!10!Z(' + mathUnicode[1:*:2] + ')!X'

  symbolTable = [unicode, mathUnicode, symbolTable]
  
  return, symbolTable

end


;-------------------------------------------------------------------------
; Given a string with a left brace "{", find the matching right brace,
; making sure to skip any nested brace pairs.
;
; Result
;  An integer giving the string position of the matching right brace.
;
; Arguments
; String: The string in which to find the match.
; Start: The offset within the string at which to start searching
;        for the left brace.
;        
; Keywords
; BRACE: Set this keyword to the brace to match, either {, (, or [.
;        The default is curly brace {.
;
function tex2idl_matchbrace, str, startIn, BRACE=braceIn

  compile_opt idl2, hidden

  lbrace = ISA(braceIn) ? braceIn : '{'
  case (lbrace) of
  '{': rbrace = '}'
  '(': rbrace = ')'
  '[': rbrace = ']'
  endcase

  start = startIn
  count = 1
  slen = STRLEN(str)
  while (start lt slen) do begin
    left = STRPOS(str, lbrace, start)
    right = STRPOS(str, rbrace, start)
    if (right eq -1) then return, slen
    if (left ge 0 && left lt right) then begin
      count++
      start = left + 1
    endif else begin
      count--
      start = right + 1
      if (count eq 0) then return, right
    endelse
  endwhile
  return, slen
end


;-------------------------------------------------------------------------
; Convert subscripts and superscripts to embedded formatting.
; Calls itself recursively (just once) for nested sub/superscripts.
;
function tex2idl_substring, str, SUBLEVEL=sublevel

  compile_opt idl2, hidden

  result = str
  start = 0
  previousGroupLength = 0

  while (start lt STRLEN(result)) do begin
    slen = STRLEN(result)
    super = STRPOS(result, '^', start)
    sub = STRPOS(result, '_', start)
    if (super lt 0 && sub lt 0) then break
    if (super lt 0) then super = slen
    if (sub lt 0) then sub = slen
    if (super lt sub) then begin
      hersheyCode = KEYWORD_SET(sublevel) ? '!E' : '!U'
      beginGroup = super
      opposite = '_'
    endif else begin
      hersheyCode = KEYWORD_SET(sublevel) ? '!I' : '!D'
      beginGroup = sub
      opposite = '^'
    endelse
    
    ; Assume just a single character following the sub/superscript
    endGroup = beginGroup + 2

    ; Is sub/super surrounded by braces?
    hasBrace = STRMID(result, beginGroup + 1, 1) eq '{'
    if (hasBrace) then begin
      endGroup = tex2idl_matchbrace(result, beginGroup + 2)
    endif

    len = endGroup - (beginGroup+1+hasBrace)
    tmp = (beginGroup gt 0) ? STRMID(result, 0, beginGroup) : ''

    if (len gt 0) then begin
      groupString = STRMID(result, beginGroup+1+hasBrace, len)
      ; See if we have a superscript followed by a subscript, or vice versa.
      ; In this case we want to stack the two on top of each other.
      followChar = STRMID(result, endGroup+hasBrace, 1)
      subSuperCombined = followChar eq opposite
      if (subSuperCombined) then tmp += '!S'
      tmp += hersheyCode
      
      ; If we are already within a subscript (or a superscript) then we
      ; cannot go to another level.
      ; Otherwise, decode the group, looking for sub-subscripts, etc.
      if (~KEYWORD_SET(sublevel)) then begin
        groupString = tex2idl_substring(groupString, SUBLEVEL=hersheyCode)
      endif
        if (previousGroupLength gt 0) then begin
          nBangs = TOTAL(BYTE(groupString) eq 33b)  ; 33 = '!'
          currGroupLength = STRLEN(groupString) - 2*nBangs
          ; For superscripts and subscripts, the spacing works better if
          ; the longer group come last. If it doesn't, then additional
          ; space characters are added to make up the difference.
          if (previousGroupLength gt currGroupLength) then begin
            ; If the first group was longer, insert "fixed width" spaces
            ; to make up the difference.
            groupString += $;'!3' + $
              STRJOIN(REPLICATE(160b, previousGroupLength-currGroupLength)); + '!X'
          endif
          previousGroupLength = 0
        endif


      if (subSuperCombined) then begin
        nBangs = TOTAL(BYTE(groupString) eq 33b)  ; 33 = '!'
        previousGroupLength = STRLEN(groupString) - 2*nBangs
        groupString += '!R'
      endif else begin
        groupString += KEYWORD_SET(sublevel) ? sublevel : '!N'
      endelse

      tmp += groupString
    endif

    result = tmp + STRMID(result, endGroup + hasBrace)
    start = STRLEN(tmp)
  endwhile

  return, result
end


;-------------------------------------------------------------------------
; Replace Unicode \U(****) with !10!Z(****)!X
;
function tex2idl_handleunicode, stringIn

  compile_opt idl2, hidden

  s = STRTOK(stringIn, '\\U\(', /EXTRACT, /PRESERVE_NULL, /REGEX)
  ns = N_ELEMENTS(s)

  if (ns le 1) then return, stringIn

  for i=0,ns-2 do begin
    position = tex2idl_matchbrace('(' + s[i+1], 1, BRACE='(') - 1
    ; Replace the "\U(" with "!10!Z("
    s[i] += '!10!Z('
    ; In the next string replace the matching right parentheses with ")!X".
    s[i+1] = STRMID(s[i+1], 0, position) + ')!X' + STRMID(s[i+1], position+1)
  endfor
  
  return, STRJOIN(s)
end


;-----------------------------------------------------------------------
; Internal routine to plot the character widths for IDL fonts.
; Each width is normalized by the width of the "overline" character,
; which is the backquote ` in the symbol font.
; These widths are used for the hardcoded widths in tex2idl_overlineWidth.
; CT, Jan 2013
pro tex2idl_plot_charwidths

  compile_opt idl2, hidden

  w = IDLgrWindow()
  v = IDLgrView()
  w.SetProperty, GRAPHICS_TREE=v
  m = IDLgrModel()
  v.Add, m
  ft = IDLgrFont(NAME='Times', SIZE=72)
  fh = IDLgrFont(NAME='Helvetica', SIZE=72)
  fs = IDLgrFont(NAME='Symbol', SIZE=72)
  t = IDLgrText("A", RECOMPUTE=2)
  m.Add, t
  w.Draw
  n = 126 - 32 + 1
  chars = BINDGEN(n) + 32b
  widthHelv = FLTARR(n)
  widthTimes = FLTARR(n)
  widthSymbol = FLTARR(n)
  for i=0,n-1 do begin
    t.SetProperty, FONT=fh, STRING=STRING(chars[i])
    w.Draw
    wid = (w.GetTextDimensions(t))[0]
    widthHelv[i] = wid
    t.SetProperty, FONT=ft
    w.Draw
    wid = (w.GetTextDimensions(t))[0]
    widthTimes[i] = wid
    t.SetProperty, FONT=fs, STRING=STRING(chars[i])
    w.Draw
    wid = (w.GetTextDimensions(t))[0]
    widthSymbol[i] = wid
  endfor
  ; Find the width of the overline bar
  t.SetProperty, FONT=IDLgrFont(NAME='Symbol', SIZE=72)
  t.SetProperty, STRING='`'
  w.Draw
  barWidth = (w.GetTextDimensions(t))[0]
  Obj_Destroy, w
  ft = 0
  fh = 0
  fs = 0
  
  s = SORT(widthTimes)
  widthTimes = widthTimes[s]/barWidth
  charTimes = chars[s]
  
  s = SORT(widthHelv)
  widthHelv = widthHelv[s]/barWidth
  charHelv = chars[s]
  
  s = SORT(widthSymbol)
  widthSymbol = widthSymbol[s]/barWidth
  charSymbol = chars[s]
  
  p = PLOT([0,56], [0,2], /NODATA, XSTYLE=1, YSTYLE=1, $
    DIM=[1000,1100], POSITION=[0.15,0.1,0.95,0.9], $
    TITLE=['Font widths'], $
    YTITLE='Relative to the overline!M`  character', $
    FONT_SIZE=18, $
    XTICKV=[0,18.5,38,56], XMINOR=0, $
    YTICKV=[0,0.25,0.5,0.75,1,1.25,1.5,1.75,2], $
    XTICKLEN=0.5, XGRIDSTYLE=[2,'AAAA'x], XSHOWTEXT=0, XTHICK=0.5, $
    YTICKLEN=0.5, YSUBTICKLEN=0.01, YGRIDSTYLE=[2,'AAAA'x], YTHICK=0.5)
  t = TEXT(9,0.12,'Helvetica', /DATA, FONT_SIZE=18, ALIGN=0.5)
  t = TEXT(28,0.12,'Times', /DATA, FONT_SIZE=18, FONT_NAME='Times', ALIGN=0.5)
  t = TEXT(47,0.12,'Symbol', /DATA, FONT_SIZE=18, ALIGN=0.5)
  extra = {DATA: 1, CLIP:0, $
    FILL_COLOR: 'light gray', FILL_BACKGROUND: 1, $
    FONT_SIZE:14, ALIGN:0.5, VERTICAL_ALIGN:0.5}
  for i=0,n-1 do begin
    t1 = TEXT((i mod 17)+1, widthHelv[i], STRING(charHelv[i]), $
      FONT_NAME='Helvetica', _EXTRA=extra)
    t2 = TEXT((i mod 17)+20, widthTimes[i], STRING(charTimes[i]), $
      FONT_NAME='Times', _EXTRA=extra)
    t2 = TEXT((i mod 17)+39, widthSymbol[i], STRING(charSymbol[i]), $
      FONT_NAME='Symbol', _EXTRA=extra)
  endfor
  ;p.Save, 'fontwidths.pdf', /BITMAP  
end


;-------------------------------------------------------------------------
; Estimate the width in "overline" units for characters within the string.
; The "overline" character is about the same width as the letter "z".
; This will produce good results for proportional fonts like Helvetica & Times,
; but poor results for fixed-width fonts like Courier.
;
function tex2idl_overlineWidth, stringIn
  compile_opt idl2, hidden

  ; Approximate width of different characters, in a proportional font
  ; like Helvetica.
  width0_5 = '''ijlfI ,.t;"\/:-!(r)[]'
  width1_0 = 'zvx*|_{}J`sy?ckLa0123456789$ehun'
  width1_25 = 'ToFpqZdgbYXVPKAES'
  width1_5 = 'BR&DCUHNwG#OQ>M~=<+%m'
  width1_75 = 'W@^'
  len = 0d
  for i=0,STRLEN(stringIn)-1 do begin
    char = STRMID(stringIn,i,1)
    len += 0.5d*(STRPOS(width0_5, char) ge 0)
    len += (STRPOS(width1_0, char) ge 0)
    len += 1.25d*(STRPOS(width1_25, char) ge 0)
    len += 1.5d*(STRPOS(width1_5, char) ge 0)
    len += 1.75d*(STRPOS(width1_75, char) ge 0)
    len += (BYTE(char) ge 128)
  endfor

  len = ROUND(len) > 1
  return, len

end


;-------------------------------------------------------------------------
; Replace \mathCode{***} with a line on top, long enough to cover the
; contained characters.
; 
; If /SQRT is set then add a sqrt symbol in front of the overline.
;
function tex2idl_overline_withInitialChar, stringIn, mathCode, SQRT=sqrt

  compile_opt idl2, hidden
  
  s = STRTOK(stringIn, '\' + mathCode + '{', /EXTRACT, /PRESERVE_NULL, /REGEX)
  ns = N_ELEMENTS(s)
  
  if (ns le 1) then return, stringIn
  
  for i=0,ns-2 do begin
    length = tex2idl_matchbrace('{' + s[i+1], 1) - 1
    stringWithinOverline = STRMID(s[i+1], 0, length)
    ; Add the rest of the string that isn't under the overline.
    s[i+1] = STRMID(s[i+1], length+1)
    
    ; We need to remove any TeX commands within the "overline" string,
    ; otherwise it will include the commands within the string length.
    ; We might as well use TeX2IDL, and then we don't have to convert
    ; this substring later.
    stringWithinOverline = TeX2IDL('$' + stringWithinOverline + '$')
    ; Filter out all of the ! hershey formatting commands.
    findCommands = STRSPLIT(stringWithinOverline, '!', COUNT=c, $
      /EXTRACT, /PRESERVE_NULL)
    ; First string is either not a command (it didn't have a !)
    ; or it is a null string (if it had a !). Either way we do not
    ; need to remove the first character.
    stringWithoutCommands = findCommands[0]
    if (c gt 1) then begin
      ; Remove the first character (the "command") from all other substrings.
      ; This will fail for bogus Hershey commands, oh well.
      stringWithoutCommands += STRJOIN(STRMID(findCommands,1))
    endif

    ; If we have multiple characters, extend the overline to cover them.
    overlineWidth = tex2idl_overlineWidth(stringWithoutCommands)
    

    if (KEYWORD_SET(sqrt)) then begin
      overline = '!9' + STRJOIN(REPLICATE('`', overlineWidth)) + '!X'
      root = '!M' + String(214b)
      ; draw the sqrt, go back, draw the overline, go back, draw the sqrt,
      ; and then draw the text within the root
      s[i] += '!S!S' + root + '!R' + overline + '!R' + root + $
        stringWithinOverline
    endif else begin
      overline = (overlineWidth gt 0) ? $
        '!9' + STRJOIN(REPLICATE('`', overlineWidth-1)) + '!X' : ''
      ; draw the overline, go back, draw 1 more overline segment (to get
      ; into the correct position), then draw the text within the overline
      s[i] += '!M`!S' + overline + '!R' + stringWithinOverline
    endelse

  endfor
  
  return, STRJOIN(s)
end


;-------------------------------------------------------------------------
; Replace \sqrt{***} with the square root symbol and a line on top
; long enough to cover the radical.
;
function tex2idl_handlesqrt, stringIn

  compile_opt idl2, hidden

  r = tex2idl_overline_withInitialChar(stringIn, '\sqrt', /SQRT)
  return, r
end


;-------------------------------------------------------------------------
; Replace \sqrt{***} with the square root symbol and a line on top
; long enough to cover the radical.
;
function tex2idl_handleoverline, stringIn

  compile_opt idl2, hidden
  
  r = tex2idl_overline_withInitialChar(stringIn, '\overline')
  return, r
end


;-------------------------------------------------------------------------
; Convert an individual $***$ string to embedded formatting commands.
;
function tex2idl_convert, texIn

  compile_opt idl2, hidden

  result = texIn

  ; Replace escaped versions of backslashes before converting special chars.
  split = STRTOK(result, '\\\\', /EXTRACT, /PRESERVE_NULL, /REGEX)
  result = STRJOIN(split, '@092@')

  result = tex2idl_handleUnicode(result)

  result = tex2idl_handleSqrt(result)

  result = tex2idl_handleOverline(result)

  symbolTable = tex2idl_symboltable()

  foreach sym, symbolTable, idx do begin
    ; If a symbol is followed by a space, then swallow the space.
    if (STRLEN(sym) ge 3) then begin
      split = STRTOK(result, sym + ' ', /EXTRACT, /PRESERVE_NULL, /REGEX, COUNT=c)
      if (c gt 1) then begin
        ; only keep a single slash at the beginning of the symbol
        userSym = STRMID(sym,STRPOS(sym, '\', /REVERSE_SEARCH))
        result = STRJOIN(split, userSym)
      endif
    endif
    symIDL = symbolTable[idx+1]
    ; First protect all special \ characters within subscripts or superscripts,
    ; because these will be turned into multiple characters.
    split = STRTOK(result, '\^' + sym, /EXTRACT, /PRESERVE_NULL, /REGEX)
    result = STRJOIN(split, '^{' + symIDL + '}')
    split = STRTOK(result, '\_' + sym, /EXTRACT, /PRESERVE_NULL, /REGEX)
    result = STRJOIN(split, '_{' + symIDL + '}')
    ; Now replace all special characters with the IDL sequence.
    split = STRTOK(result, sym, /EXTRACT, /PRESERVE_NULL, /REGEX)
    result = STRJOIN(split, symIDL)
    idx++  ; step by two's
  endforeach

  ; Replace escaped versions of special characters with dummy sequences,
  ; so we don't have to worry about them below.
  specialChars = ['\','{','}','^','_']
  foreach char, specialChars do begin
    charByte = STRING(BYTE(char), FORMAT='(I3.3)')
    split = STRTOK(result, '\\\' + char, /EXTRACT, /PRESERVE_NULL, /REGEX)
    result = STRJOIN(split, '@' + charByte + '@')
  endforeach

  result = tex2idl_substring(result)

  ; Convert any dummy sequences back into the normal characters.
  specialChars = ['\','{','}','^','_']
  foreach char, specialChars do begin
    charByte = STRING(BYTE(char), FORMAT='(I3.3)')
    split = STRTOK(result, '@' + charByte + '@', /EXTRACT, /PRESERVE_NULL, /REGEX)
    result = STRJOIN(split, char)
  endforeach

  return, result

end


;-------------------------------------------------------------------------
;+
; :Description:
;    The Tex2IDL function converts a string with TeX-like syntax
;    into a string that uses IDL's graphics formatting commands.
;    The string result can then be used by Direct Graphics (with XYOUTS),
;    or Object Graphics (with IDLgrText), or the Graphics functions
;    (with TEXT).
;
; :Return Value:
;    The result is a string where all of the $...$ sequences have been
;    converted into embedded formatting commands.
;
; :Arguments:
;    String
;      A scalar string to be converted.
;    
; :Keywords:
;    None
;
;-
function tex2idl, texIn

  compile_opt idl2, hidden
  
  ON_ERROR, 2

  if (~ISA(texIn, 'STRING')) then $
    MESSAGE, 'Input must be a string or string array.'

  if (N_ELEMENTS(texIn) gt 1) then begin
    result = texIn
    foreach tex, texIn, idx do $
      result[idx] = TeX2IDL(tex)
    return, result
  endif

  result = texIn[0]

  ; Look for pairs of $ $ that enclose "math mode" sequences.
  ; First replace escaped versions of $.
  split = STRTOK(result, '\\\$', /EXTRACT, /PRESERVE_NULL, /REGEX)
  result = STRJOIN(split, '@036@')
  ; Look for matching $ pairs
  index = WHERE(BYTE(result) eq 36b, nindex)
  if (nindex ge 2) then begin
    if (nindex mod 2) then begin
      index[-1] = STRLEN(result)+1
      nindex = (nindex/2)*2
    endif else begin
      index = [index, STRLEN(result)+1]
    endelse
    str = result
    result = (index[0] gt 0) ? STRMID(str, 0, index[0]) : ''
    for i=0,nindex-1,2 do begin
      lenMath = index[i+1] - index[i] - 1
      ; Convert each $...$ math substring.
      if (lenMath ge 1) then begin
        result += TeX2IDL_Convert(STRMID(str, index[i]+1, lenMath))
      endif
      ; Add on each regular non-TeX substring.
      lenReg = index[i+2] - index[i+1] - 1
      if (lenReg ge 1) then begin
        result += STRMID(str, index[i+1]+1, lenReg)
      endif
    endfor
  endif

  ; Put any escaped $'s back into the string.
  split = STRTOK(result, '@036@', /EXTRACT, /PRESERVE_NULL, /REGEX)
  result = STRJOIN(split, '$')

  return, result
end

