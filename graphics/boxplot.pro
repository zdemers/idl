; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/boxplot.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create IDL BoxPlot graphic.
;
; :Params:
;    arg1 : optional generic argument
;    arg2 : optional generic argument
;
; :Keywords:
;    _REF_EXTRA
;
;-

;-----------------------------------------------------------------------
; Helper routine to validate the data (monotonically increasing)
function iBoxPlot_ValidateData, data
  compile_opt idl2, hidden
  
  dims = size(data, /DIMENSIONS)
  for i=0,dims[0]-1 do begin
    tmp = data[i,*]
    tmp = tmp[sort(tmp)]
    if (~ARRAY_EQUAL(tmp, data[i,*])) then begin
      if (i) then return, 0
      if (ARRAY_EQUAL(REVERSE(tmp, /OVERWRITE), data[i,*])) then begin
        data = REVERSE(data, 2, /OVERWRITE)
      endif else begin
        return, 0
      endelse
    endif
  endfor
  
  return, 1
  
end

;-----------------------------------------------------------------------
; Helper routine to construct the parameter set.
; If no parameters are supplied then oParmSet will be undefined.
;
function iBoxPlot_GetParmSet, oParmSet, parm1, parm2, $
                              TEST=test, _EXTRA=_EXTRA
  compile_opt idl2, hidden
  
  nParams = N_Params()
  if (Keyword_Set(test)) then begin
    parm1 = indgen(9)+1
    parm2 = fltarr(9,5)
    for i=0,8 do begin
      rand = randomu(seed,5)
      rand = rand[sort(rand)]
      parm2[i,*] = rand
    endfor
    nParams = 3
  endif
  
  if (nParams le 1) then return, ''
  
  oParmSet = OBJ_NEW('IDLitParameterSet', NAME='Plot parameters', $
                     ICON='plot', DESCRIPTION='Plot parameters')
  oParmSet->SetAutoDeleteMode, 1b
  
  case (nParams) of
    2: begin
      ; Check for undefined variables.
      if (N_ELEMENTS(parm1) eq 0) then $
        MESSAGE, 'First argument is an undefined variable.'
        
      ; eliminate leading dimensions of 1
      parm1 = reform(parm1)
      
      ; values only
      case (SIZE(parm1, /N_DIMENSIONS)) of
        1: begin
          ; accept case of one box
          if (N_ELEMENTS(parm1) eq 5) then begin
            parm1 = reform(parm1, 1, 5)
            if (~iBoxPlot_ValidateData(parm1)) then begin
              message, 'Boxplot data must be monotonically increasing'
            endif
            oDataXY = OBJ_NEW('IDLitDataIDLArray2D', NAME='Y', parm1)
            oParmSet->Add, oDataXY, PARAMETER_NAME='Y'
          endif else begin
            MESSAGE, 'First argument has invalid dimensions'
          endelse
        end
        2: begin
          if ((SIZE(parm1, /DIMENSIONS))[1] ne 5) then $
            parm1 = TRANSPOSE(parm1)
          ; 2D plot, x,y in one 2xN array
          if (~iBoxPlot_ValidateData(parm1)) then begin
            message, 'Boxplot data must be monotonically increasing'
          endif
          oDataXY = OBJ_NEW('IDLitDataIDLArray2D', NAME='Y', parm1)
          oParmSet->Add, oDataXY, PARAMETER_NAME='Y'
        end
        else: MESSAGE, 'First argument has invalid dimensions'
      endcase
    end
    3: begin
      ; Check for undefined variables.
      if (N_ELEMENTS(parm1) eq 0) then $
        MESSAGE, 'First argument is an undefined variable.'
        
      ; Check for undefined variables.
      if (N_ELEMENTS(parm2) eq 0) then $
        MESSAGE, 'Second argument is an undefined variable.'
        
      ; eliminate leading dimensions of 1
      parm1 = reform(parm1)
      parm2 = reform(parm2)
      if (N_ELEMENTS(parm2) eq 5) then $
        parm2 = reform(parm2, 1, 5)
      
      ; x and y for 2D plot
      if ((SIZE(parm2, /DIMENSIONS))[[1]] ne 5) then $
        parm2 = TRANSPOSE(parm2)
      if ((SIZE(parm1, /N_DIMENSIONS) eq 1) AND $
          (SIZE(parm2, /N_DIMENSIONS) eq 2) AND $
          (N_ELEMENTS(parm1) eq N_ELEMENTS(parm2[*,0]))) then begin
        if (~iBoxPlot_ValidateData(parm2)) then begin
          message, 'Boxplot data must be monotonically increasing'
        endif
        oDataX = obj_new('IDLitDataIDLVector', parm1, NAME='X')
        oDataY = obj_new('IDLitDataIDLArray2D', parm2, NAME='Y')
        oParmSet->add, oDataX, PARAMETER_NAME='X'
        oParmSet->add, oDataY, PARAMETER_NAME='Y'
      endif else begin
        MESSAGE, 'Arguments have invalid dimensions'
      endelse
    end
  endcase
  
  ; Set the appropriate visualization type.
  visType = 'PLOT'
  oParmSet->SetProperty, TYPE=visType
  
  return, visType
  
end

;-------------------------------------------------------------------------
; Needed because Graphic calls 'i'+graphicname
pro iboxplot, parm1, parm2, $
    DEBUG=debug, $
    IDENTIFIER=identifier, $
    _REF_EXTRA=_extra
    
  compile_opt hidden, idl2
  
  ; Note: The error handler will clean up the oParmSet container.
  @idlit_itoolerror.pro
  
  nParams = N_Params()

  case (nParams) of
    0: visType = iBoxPlot_GetParmSet(oParmSet, _EXTRA=_extra)
    1: visType = iBoxPlot_GetParmSet(oParmSet, parm1, _EXTRA=_extra)
    2: visType = iBoxPlot_GetParmSet(oParmSet, parm1, parm2, _EXTRA=_extra)
    else: message, 'Invalid inputs'
  endcase
  
  identifier = IDLitSys_CreateTool("Plot Tool", $
    INITIAL_DATA=oParmSet, $
    WINDOW_TITLE='IDL Box and Whisker Plot', $
    VISUALIZATION_TYPE='BoxPlot', $
    _EXTRA=_extra)
    
end

;-------------------------------------------------------------------------
function boxplot, arg1, arg2, arg3, $
                  LAYOUT=layoutIn, _REF_EXTRA=ex
  compile_opt idl2, hidden
  ON_ERROR, 2
  
  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams eq 0 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'
  
  switch (nparams) of
  3: if ~ISA(arg3, 'STRING') then MESSAGE, 'Format argument must be a string.'
  2: if (~ISA(arg2, /ARRAY) && ~ISA(arg2, 'STRING')) then $
    MESSAGE, 'Input must be an array or a Format string.'
  1: if ~ISA(arg1, /ARRAY) then MESSAGE, 'Input must be an array.'
  endswitch
  
  if (isa(arg1, 'STRING')) then begin
    if (~hasTestKW) then $
      MESSAGE, 'Format argument must be passed in after data.'
    style = arg1
    nparams--
  endif
  if (isa(arg2, 'STRING'))  then begin
    if (isa(arg3)) then $
      MESSAGE, 'Format argument must be passed in after data.'
    style = arg2
    nparams--
  endif
  if (isa(arg3, 'STRING')) then begin
    style = arg3
    nparams--
  endif
  
  if (n_elements(style)) then $
    style_convert, style, COLOR=color, LINESTYLE=linestyle, THICK=thick
  
  layout = N_ELEMENTS(layoutIn) eq 3 ? layoutIn : [1,1,1]
  ; default the xystyle to 0
  xstyle = 0
  ystyle = 0
  
  name = 'BoxPlot'
  case nparams of
    0: Graphic, name, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      LINESTYLE=linestyle, THICK=thick, COLOR=color, /AUTO_CROSSHAIR
    1: Graphic, name, arg1, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      LINESTYLE=linestyle, THICK=thick, COLOR=color, /AUTO_CROSSHAIR
    2: Graphic, name, arg1, arg2, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      LINESTYLE=linestyle, THICK=thick, COLOR=color, /AUTO_CROSSHAIR
    3: Graphic, name, arg1, arg2, arg3, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      LINESTYLE=linestyle, THICK=thick, COLOR=color, /AUTO_CROSSHAIR
  endcase

  oTool = graphic->GetTool()
  oTool->RefreshCurrentWindow
  
  return, graphic
  
end
