; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlbubbleplot__define.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLBubbleplot
;
; PURPOSE:
;    The IDLBubbleplot class is the component wrapper for IDLgrPlot
;
; CATEGORY:
;    Components
;
; MODIFICATION HISTORY:
;     Written by:   AGEH, 1/2013
;-

;----------------------------------------------------------------------------
; Lifecycle Methods
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAMES:
;   IDLBubbleplot::Init
;
; PURPOSE:
;   Initialize this component
;
;   NOTE: Init methods are special lifecycle methods, and as such
;   cannot be called outside the context of object creation.  This
;   means that in most cases, you cannot call the Init method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Init method
;   from within the Init method of the subclass.
;
; CALLING SEQUENCE:
;
;   Obj = OBJ_NEW('IDLBubbleplot', [X,] Y)
;
; INPUTS:
;   X: Vector of X coordinates
;   Y: Vector of Y coordinates
;
; OUTPUTS:
;   This function method returns 1 on success, or 0 on failure.
;
;-
function IDLBubbleplot::Init, _REF_EXTRA=_extra
  compile_opt idl2, hidden

  ; Initialize superclass
  if (~self->IDLitVisPlot::Init(/REGISTER_PROPERTIES, NAME='IDLBubbleplot', $
                                ICON='plot', TYPE='IDLPLOT', $
                                DESCRIPTION='An IDLBubbleplot Visualization', $
                                _EXTRA=_extra)) then return, 0

  ; Defaults
  self._oBubbles = PTR_NEW(!NULL)
  self.data = PTR_NEW(!NULL)
  self.x_data = PTR_NEW(!NULL)
  self.user_data = PTR_NEW(!NULL)
  self.user_x_data = PTR_NEW(!NULL)
  self._sort = PTR_NEW(!NULL)
  self._diameter = PTR_NEW(!NULL)
  self._szfactor = 3.0d ; internal factor for a nice size of the largest bubble
  self.exponent = 1.0d
  self.sizing = 1.0d
  self.shaded = 0b
  self.filled = 1b
  self.border = 1b
  self.color = PTR_NEW([205b,6b,65b]) ;unm cherry
  self.linecolor = PTR_NEW([164b,163b,167b]) ;unm silver
  self.linestyle = 0
  self.rgbTable = PTR_NEW(transpose([[(ramp=bindgen(256))],[ramp],[ramp]]))
  self.labels = PTR_NEW(!NULL)
  self.labelPos = PTR_NEW('')
  self.magnitude = PTR_NEW(!NULL)
  self._symsize = [0d,0,0]
  self.transparency = 0
  self.thick = 1
  self._cirx = PTR_NEW(!NULL)
  self._ciry = PTR_NEW(!NULL)
  self._conn = PTR_NEW(!NULL)
  self._lineConn = PTR_NEW(!NULL)
  self._vertColors = PTR_NEW(!NULL)
  self.alignment = PTR_NEW([0.5])
  self.vertAlign = PTR_NEW([0.5])
  self.baseline = [1.0,0.0,0.0]
  self.updir = [0.0,1.0,0.0]
  self.fontColor = [0b,0b,0b]
  self.fontName = 'Helvetica'
  self.fontSize = 12.0
  self.fontStyle = 'Normal'
  self._oFont = OBJ_NEW('IDLgrFont', SIZE=self.fontSize)
  self._oitFont = OBJ_NEW('IDLitFont')
  self._faceShow = 0b
  self._faceSmile = PTR_NEW([0.7])
  self._faceEyes = PTR_NEW([0.8])
  self._faceConn = PTR_NEW(!NULL)

  ; Define basic circle
  n = 42
  ind = INDGEN(n)
  ramp = DINDGEN(n)/(n)*2*!pi
  *self._cirx = SIN(ramp)
  *self._ciry = COS(ramp)
  ; Define connectivity array for shaded bubbles
  *self._conn = (TRANSPOSE(REFORM([REPLICATE(3,n),ind,ind[SHIFT(ind,-1)], $
                                   REPLICATE(n,n)],n,4,/OVERWRITE)))[*]
  ; Define connectivity arrays for lines
  *self._lineConn = [n+1, ind, 0]
  *self._faceConn = [*self._lineConn, 11, indgen(11)+n, $ ; 11 must match number
                     n/3+1, [indgen(n/3),0]+n+11, n/3+1, $ ; in _CalcBubble
                     [indgen(n/3),0]+n+11+n/3]

  ; Set any properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisPlot::SetProperty,  _EXTRA=_extra

  self->RemoveAggregate, self._oSymbol

  RETURN, 1 ; Success

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLBubbleplot::Cleanup
;
; PURPOSE:
;   This procedure method performs all cleanup on the object.
;
;   NOTE: Cleanup methods are special lifecycle methods, and as such
;   cannot be called outside the context of object destruction.  This
;   means that in most cases, you cannot call the Cleanup method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Cleanup method
;   from within the Cleanup method of the subclass.
;
; CALLING SEQUENCE:
;   OBJ_DESTROY, Obj
;     or
;   Obj->[IDLBubbleplot::]Cleanup
;
;-
pro IDLBubbleplot::Cleanup, _EXTRA=_extra
  compile_opt idl2, hidden

  ; Cleanup superclass (but cannot pass _extra as it does not accept arguments?)
  self->IDLitVisPlot::Cleanup

  OBJ_DESTROY, [self._oFont, self._oitFont]
  for i=0,N_ELEMENTS(*self._oBubbles)-1 do $
    OBJ_DESTROY, (*self._oBubbles)[i]
    
  PTR_FREE, [self._oBubbles, $
             self.data, self.x_data, self._diameter, self.color, self._sort, $
             self.labels, self.labelPos, self.magnitude, self.linecolor, $
             self._cirx, self._ciry, self._conn, self._vertColors, $
             self.alignment, self.vertAlign, self.user_data, self.user_x_data, $
             self.rgbTable, self._lineConn, $
             self._faceSmile, self._faceEyes, self._faceConn]

end


;----------------------------------------------------------------------------
; METHODNAME:
;    IDLBubblePlot::OnDataChangeUpdate
;
; PURPOSE:
;    This procedure method is called by a Subject via a Notifier when
;    its data has changed.  This method obtains the data from the
;    subject and updates the internal IDLgrPlot object.
;
; CALLING SEQUENCE:
;
;    Obj->[IDLBubblePlot::]OnDataChangeUpdate, oSubject, parmName
;
; INPUTS:
;    oSubject: The Subject object in the Subject-Observer relationship.
;    This object (the plot) is the observer, so it uses the
;    IIDLDataSource interface to get the data from the subject.
;    Then it puts the data in the IDLgrPlot object.
;
;    parmName: The name of the registered parameter.
;
; KEYWORDS:
;   NO_UPDATE: Undocumented keyword to suppress updates when adding
;       multiple data objects within a parameter set.
;
pro IDLBubblePlot::OnDataChangeUpdate, oSubject, parmName, $
                                       NO_UPDATE=noUpdate
  compile_opt idl2, hidden
  
  flag = 0b
  
  case strupcase(parmName) of
    '<PARAMETER SET>':begin
      ;; Get our data
      position = oSubject->Get(/ALL, count=nCount, NAME=name)
      for i=0, nCount-1 do begin
        if (name[i] eq '') then $
          continue
        oData = (oSubject->GetByName(name[i]))[0]
        if (~OBJ_VALID(oData)) then $
          continue
        if (oData->GetData(data, NAN=nan) le 0) then $
          continue
        
        case name[i] of
          'Y': begin
            self->IDLBubblePlot::OnDataChangeUpdate, oData, 'Y', /NO_UPDATE
            oData = oSubject->GetByName('X')
            if (OBJ_VALID(oData)) then begin
              self->IDLBubblePlot::OnDataChangeUpdate, oData, 'X', /NO_UPDATE
            endif
            flag = 1b
          end
          'X':  ; X is handled in the Y branch to control order
          ; Pass all other parameters on to ourself.
          else: self->IDLBubblePlot::OnDataChangeUpdate, oData, name[i]
        endcase
      endfor
    end
  
    'X': begin
      if (~oSubject->GetData(data)) then $
        break
      *self.user_x_data = data
      if (~KEYWORD_SET(noUpdate)) then begin
        ;; Call OnDataChangeUpdate to update the visual stuff
        if (self->GetXYZRange(xRange, yRange, zRange)) then $
          self->OnDataRangeChange, self, xRange, yRange, zRange
        flag = 1b
      endif
    end
  
    'Y': begin
      if (~oSubject->GetData(data)) then $
        break
      *self.user_data = data
      if (~KEYWORD_SET(noUpdate)) then begin
        ;; Call OnDataChangeUpdate to update the visual stuff
        if (self->GetXYZRange(xRange, yRange, zRange)) then $
          self->OnDataRangeChange, self, xRange, yRange, zRange
        flag = 1b
      endif
    end
  
    else: ; ignore unknown parameters
  
  endcase

  ; Ensure palette is set for colorbar use
  oPalette = self->GetParameter('PALETTE')
  if (~OBJ_VALID(oPalette)) then begin
    if (N_ELEMENTS(*self.color) eq 3) then begin
      color = reform(*self.color,3,1)
    endif else begin
      color = *self.color
    endelse
    oColorTable = OBJ_NEW('IDLitDataIDLPalette', color, $
                          NAME='RGB Table')
    self->SetParameter, 'PALETTE', oColorTable, /BY_VALUE
  endif

  if (flag) then begin
    self->_UpdateData
    self->_UpdateSelectionVisual
  endif

  ; Since we are changing a bunch of attributes, notify
  ; our observers in case the prop sheet is visible.
  self->DoOnNotify, self->GetFullIdentifier(), 'SETPROPERTY', ''

end


;----------------------------------------------------------------------------
pro IDLBubbleplot::OnProjectionChange, sMap
  compile_opt idl2, hidden

  self->_UpdateData, sMap

end


;----------------------------------------------------------------------------
; Property Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubbleplot::GetProperty
;
; PURPOSE:
;      This procedure method retrieves the
;      value of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLBubbleplot::]GetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLBubbleplot::Init followed by the word "Get"
;      can be retrieved using IDLBubbleplot::GetProperty.
;
;-
pro IDLBubbleplot::GetProperty, $
    COLOR=color, $
    RGB_TABLE=rgbTable, $
    LINECOLOR=lineColor, $
    LINESTYLE=linestyle, $
    MAGNITUDE=mag, $
    EXPONENT=exp, $
    SIZING=size, $
    SHADED=shaded, $
    FILLED=filled, $
    LINETHICK=thick, $
    BORDER=border, $
    TRANSPARENCY=trans, $
    ANTIALIAS=antialias, $
    MAX_VALUE=maxValue, $
    LABELS=labels, $
    LABEL_POSITION=labelPos, $
    LABEL_ALIGNMENT=align, $
    LABEL_BASELINE=baseline, $
    LABEL_FONT_COLOR=fontColor, $
    LABEL_FONT_NAME=fontName, $
    LABEL_FONT_SIZE=fontSize, $
    LABEL_FONT_STYLE=fontStyle, $
    LABEL_UPDIR=updir, $
    LABEL_VERTICAL_ALIGNMENT=vertAlign, $
    FACE_SMILE=faceSmile, $
    FACE_EYES=faceEyes, $
    FACE_SHOW=faceShow, $
    _REF_EXTRA=_extra

  compile_opt idl2, hidden

  if (ARG_PRESENT(color)) then $
    color = *self.color
  
  if (ARG_PRESENT(rgbTable)) then $
    rgbTable = *self.rgbTable
  
  if (ARG_PRESENT(lineColor)) then $
    lineColor = *self.linecolor

  if (ARG_PRESENT(linestyle)) then $
    linestyle = self.linestyle

  if (ARG_PRESENT(labels)) then $
    labels = *self.labels

  if (ARG_PRESENT(labelPos)) then $
    labelPos = *self.labelPos

  if (ARG_PRESENT(mag)) then $
    mag = *self.magnitude

  if (ARG_PRESENT(exp)) then $
    exp = self.exponent

  if (ARG_PRESENT(size)) then $
    size = self.sizing

  if (ARG_PRESENT(shaded)) then $
    shaded = self.shaded

  if (ARG_PRESENT(filled)) then $
    filled = self.filled

  if (ARG_PRESENT(thick)) then $
    thick = self.thick

  if (ARG_PRESENT(border)) then $
    border = self.border

  if (ARG_PRESENT(trans)) then $
    trans = self.transparency

  if (ARG_PRESENT(antialias)) then begin
    if ((N_ELEMENTS(*self._oBubbles) ne 0) && $
        OBJ_VALID((*self._oBubbles)[0])) then begin
      (*self._oBubbles)[0]->GetProperty, ANTIALIAS=antialias
    endif else begin
      antialias = 0
    endelse
  endif

  if (ARG_PRESENT(maxValue)) then $
    maxValue = self.maxValue ne 0 ? self.maxValue : $
      N_ELEMENTS(*self.magnitude) eq 0 ? 0 : MAX(*self.magnitude) 

  if (ARG_PRESENT(align)) then $
    align = *self.alignment

  if (ARG_PRESENT(baseline)) then $
    baseline = self.baseline

  if (ARG_PRESENT(fontColor)) then $
    fontColor = self.fontColor

  if (ARG_PRESENT(updir)) then $
    updir = self.updir

  if (ARG_PRESENT(vertAlign)) then $
    vertAlign = *self.vertAlign
  
  if (ARG_PRESENT(fontName) || ARG_PRESENT(fontSize) || $
      ARG_PRESENT(fontStyle)) then $
    self._oitFont->GetProperty, FONT_NAME=fontName, FONT_SIZE=fontSize, $
      FONT_STYLE=fontStyle

  if (ARG_PRESENT(faceSmile)) then $
    faceSmile = *self._faceSmile

  if (ARG_PRESENT(faceEyes)) then $
    faceEyes = *self._faceEyes

  if (ARG_PRESENT(faceShow)) then $
    faceShow = self._faceShow

  ; get superclass properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisPlot::GetProperty, _EXTRA=_extra

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubbleplot::SetProperty
;
; PURPOSE:
;      This procedure method sets the value
;      of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLBubbleplot::]SetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLBubbleplot::Init followed by the word "Set"
;      can be set using IDLBubbleplot::SetProperty.
;-
pro IDLBubbleplot::SetProperty, $
    COLOR=colorIn, $
    RGB_TABLE=rgbTableIn, $
    LINECOLOR=lineColorIn, $
    LINESTYLE=linestyleIn, $
    MAGNITUDE=magIn, $
    EXPONENT=expIn, $
    SIZING=sizeIn, $
    SHADED=shadedIn, $
    FILLED=filledIn, $
    LINETHICK=thickIn, $
    BORDER=borderIn, $
    TRANSPARENCY=transIn, $
    ANTIALIAS=antialiasIn, $
    MAX_VALUE=maxValueIn, $
    XLOG=xLogIn, $
    YLOG=yLogIn, $
    LABELS=labelsIn, $
    LABEL_POSITION=labelPosIn, $
    LABEL_ALIGNMENT=alignIn, $
    LABEL_BASELINE=baselineIn, $
    LABEL_FONT_COLOR=fontColorIn, $
    LABEL_FONT_NAME=fontNameIn, $
    LABEL_FONT_SIZE=fontSizeIn, $
    LABEL_FONT_STYLE=fontStyleIn, $
    LABEL_UPDIR=updirIn, $
    LABEL_VERTICAL_ALIGNMENT=vertAlignIn, $
    FACE_SMILE=faceSmileIn, $
    FACE_EYES=faceEyesIn, $
    FACE_SHOW=faceShowIn, $
    _EXTRA=_extra

  compile_opt idl2, hidden

  update = 0b
  updateColor = 0b
  updateData = 0b
  updateLabels = 0b
  
  if (N_ELEMENTS(colorIn) ne 0) then begin
    if ((SIZE(colorIn, /N_DIMENSIONS) eq 2) || $
        (N_ELEMENTS(colorIn) eq 3)) then begin
      if (N_ELEMENTS(colorIn) mod 3 ne 0) then begin
        message, 'Invalid COLOR specification'
      endif
      color = REFORM(colorIn, 3, N_ELEMENTS(colorIn)/3)
      if (~ARRAY_EQUAL(color, *self.color)) then begin
        updateColor = 1b
        *self.color = color
        oColorTable = self->GetParameter('PALETTE')
        if (OBJ_VALID(oColorTable)) then $
          !NULL = oColorTable->SetData(TRANSPOSE(*self.color))
      endif
    endif
    color = colorIn[*]
    if (~ARRAY_EQUAL(color, *self.color)) then begin
      updateColor = 1b
      *self.color = color
    endif
  endif
  
  if (N_ELEMENTS(rgbTableIn) ne 0) then begin
    if (SIZE(rgbTableIn, /N_DIMENSIONS) eq 2) then begin
      dims = SIZE(rgbTableIn, /DIMENSIONS)
      wh = WHERE(dims eq 3, cnt)
      if (cnt eq 0) then begin
        message, 'Invalid RGB_TABLE'
        return
      endif
      rgbTable = rgbTableIn
      if (wh[0] ne 0) then $
        rgbTable = TRANSPOSE(rgbTable)
    endif else if (N_ELEMENTS(rgbTableIn) eq 1) then begin
      rr = Colortable(rgbTableIn)
      rgbTable = TRANSPOSE(rr)
    endif else begin
      message, 'Invalid RGB_TABLE'
      return
    endelse
    *self.rgbTable = rgbTable
    oColorTable = self->GetParameter('PALETTE')
    if (OBJ_VALID(oColorTable)) then $
      !NULL = oColorTable->SetData(TRANSPOSE(rgbTable))
    updateColor = 1b
  endif
  
  if (N_ELEMENTS(lineColorIn) ne 0) then begin
    if (N_ELEMENTS(lineColorIn) mod 3 ne 0) then begin
      message, 'Invalid LINE_COLOR specification'
    endif
    color = REFORM(lineColorIn, 3, N_ELEMENTS(lineColorIn)/3)
    if ((N_ELEMENTS(*self.linecolor) eq 0) || $
        ~ARRAY_EQUAL(color, *self.linecolor)) then $
      update = 1b
    *self.linecolor = color
  endif
  
  if (N_ELEMENTS(magIn) ne 0) then begin
    if ((N_ELEMENTS(*self.magnitude) eq 0) || $
        ~ARRAY_EQUAL(magIn, [*self.magnitude])) then begin
      updateData = 1b
      updateLabels = 1b
      *self.magnitude = magIn
    endif
  endif
  
  if (N_ELEMENTS(shadedIn) gt 0) then begin
    oldShaded = self.shaded
    self.shaded = KEYWORD_SET(shadedIn)
    if (oldShaded ne self.shaded) then begin
      updateColor = 1b
    endif
  endif

  if (N_ELEMENTS(filledIn) gt 0) then begin
    oldFilled = self.filled
    self.filled = KEYWORD_SET(filledIn)
    if (oldFilled ne self.filled) then $
      update = 1b
  endif

  if (N_ELEMENTS(borderIn) gt 0) then begin
    oldBorder = self.border
    self.border = KEYWORD_SET(borderIn)
    if (oldBorder ne self.border) then $
      update = 1b
  endif

  if (N_ELEMENTS(expIn) gt 0) then begin
    oldExp = self.exponent
    self.exponent = DOUBLE(expIn[0]) > 0.0001
    if (oldExp ne self.exponent) then $
      update = 1b
  endif

  if (N_ELEMENTS(xLogIn) ne 0) then begin
    self.xlog = 1b
    updateData = 1b
  endif
  
  if (N_ELEMENTS(yLogIn) ne 0) then begin
    self.ylog = 1b
    updateData = 1b
  endif
  
  if (N_ELEMENTS(linestyleIn) gt 0) then begin
    oldLinestyle = self.linestyle
    self.linestyle = 0 > linestyleIn[0] < 6
    if (oldLinestyle ne self.linestyle) then begin
      for i=1,N_ELEMENTS(*self._oBubbles)-1,3 do begin
        (*self._oBubbles)[i]->SetProperty, LINESTYLE=self.linestyle
      endfor
      update = 1b
    endif
  endif
  
  if (N_ELEMENTS(thickIn) gt 0) then begin
    oldThick = self.thick
    self.thick = 0 > thickIn[0] < 6
    if (oldThick ne self.thick) then begin
      for i=1,N_ELEMENTS(*self._oBubbles)-1,3 do begin
        (*self._oBubbles)[i]->SetProperty, THICK=self.thick
      endfor
      update = 1b
    endif
  endif
  
  if (N_ELEMENTS(sizeIn) gt 0) then begin
    oldSize = self.sizing
    self.sizing = 0.00001 > DOUBLE(sizeIn[0])
    if (oldSize ne self.sizing) then $
      update = 1b
  endif

  if (N_ELEMENTS(transIn) gt 0) then begin
    oldTrans = self.transparency
    self.transparency = 0.0 > transIn[0] < 100.
    if (oldTrans ne self.transparency) then begin
      for i=0,N_ELEMENTS(*self._oBubbles)-1 do begin
        (*self._oBubbles)[i]->SetProperty, $
          ALPHA_CHANNEL=1.0d - (self.transparency/100.)
      endfor
      update = 1b
    endif
  endif

  if (N_ELEMENTS(antialiasIn) gt 0) then begin
    for i=0,N_ELEMENTS(*self._oBubbles)-1 do begin
      if ((i mod 3) eq 2) then continue ; No antialias on the labels
      (*self._oBubbles)[i]->SetProperty, ANTIALIAS=KEYWORD_SET(antialiasIn)
    endfor
  endif

  if (N_ELEMENTS(maxValueIn) gt 0) then begin
    oldMax = self.maxValue
    self.maxValue = DOUBLE(maxValueIn[0])
    if (oldMax ne self.maxValue) then $
      updateData = 1b
  endif

  ; Label properties
  
  if (N_ELEMENTS(labelsIn) gt 0) then begin
    oldLabels = *self.labels
    *self.labels = STRING(labelsIn[*])
    if ((N_ELEMENTS(oldLabels) eq 0) || $
        ~ARRAY_EQUAL(oldLabels, [*self.labels])) then begin
      updateLabels = 1b
    endif
  endif

  if (N_ELEMENTS(labelPosIn) gt 0) then begin
    oldLabelPos = *self.labelPos
    ; Validate labels
    for i=0,N_ELEMENTS(labelPosIn)-1 do begin
      switch (STRUPCASE(labelPosIn[i])) of
        'T' :
        'TOP' : begin
          labelPosIn[i] = 'TOP'
          break
        end
        'B' :
        'BOTTOM' : begin
          labelPosIn[i] = 'BOTTOM'
          break
        end
        'L' :
        'LEFT' : begin
          labelPosIn[i] = 'LEFT'
          break
        end
        'R' :
        'RIGHT' : begin
          labelPosIn[i] = 'RIGHT'
          break
        end
        '' :
        'C' :
        'CENTER' : begin
          labelPosIn[i] = 'CENTER'
          break
        end
        else : message, 'Invalid label position: '+labelPosIn[i]
      endswitch
    endfor
    *self.labelPos = labelPosIn
    if (~ARRAY_EQUAL(oldLabelPos, [*self.labelPos])) then begin
      updateLabels = 1b
    endif
  endif

  ; Font properties
  updateFont = 0b
  
  if (N_ELEMENTS(fontNameIn) ne 0) then begin
    self.fontName = STRING(fontNameIn[0])
    updateFont = 1b
  endif

  if (N_ELEMENTS(fontSizeIn) ne 0) then begin
    self.fontSize = STRING(fontSizeIn[0])
    updateFont = 1b
  endif

  if (N_ELEMENTS(fontStyleIn) ne 0) then begin
    self.fontStyle = STRING(fontStyleIn[0])
    updateFont = 1b
  endif

  if (updateFont) then begin
    self._oitFont->SetProperty, FONT_NAME=self.fontName, $
      FONT_SIZE=self.fontSize, FONT_STYLE=self.fontStyle
    self._oFont = self._oitFont->GetFont()
    updateLabels = 1b
  endif

  if (N_ELEMENTS(alignIn) ne 0) then begin
    oldAlign = *self.alignment
    *self.alignment = DOUBLE(alignIn[*]) > 0.0d < 1.0d
    if (~ARRAY_EQUAL(oldAlign, [*self.alignment])) then begin
      updateLabels = 1b 
    endif
  endif
  
  if (N_ELEMENTS(baselineIn) ne 0) then begin
    oldBaseline = self.baseline
    self.baseline = baselineIn
    if (~ARRAY_EQUAL(oldBaseline, self.baseline)) then begin
      updateLabels = 1b
    endif
  endif

  if (N_ELEMENTS(updirIn) ne 0) then begin
    oldUpdir = self.updir
    self.updir = updirIn
    if (~ARRAY_EQUAL(oldUpdir, self.updir)) then begin
      updateLabels = 1b
    endif
  endif

  if (N_ELEMENTS(fontColorIn) ne 0) then begin
    oldFontColor = self.fontColor
    self.fontColor = fontColorIn[0:2]
    if (~ARRAY_EQUAL(oldFontColor, self.fontColor)) then begin
      updateLabels = 1b
    endif
  endif

  if (N_ELEMENTS(vertAlignIn) ne 0) then begin
    oldVertAlign = *self.vertAlign
    *self.vertAlign = DOUBLE(vertAlignIn[*]) > 0.0d < 1.0d
    if (~ARRAY_EQUAL(oldVertAlign, [*self.vertAlign])) then begin
      updateLabels = 1b 
    endif
  endif
  
  if (N_ELEMENTS(faceShowIn) gt 0) then begin
    oldShow = self._faceShow
    self._faceShow = KEYWORD_SET(faceShowIn)
    if (oldShow ne self._faceShow) then $
      update = 1b
  endif

  if (N_ELEMENTS(faceSmileIn) ne 0) then begin
    oldSmile = *self._faceSmile
    *self._faceSmile = DOUBLE(faceSmileIn[*]) > 0.0d < 1.0d
    if (~ARRAY_EQUAL(oldSmile, [*self._faceSmile])) then begin
      update = 1b
    endif
  endif
  
  if (N_ELEMENTS(faceEyesIn) ne 0) then begin
    oldEyes = *self._faceEyes
    *self._faceEyes = DOUBLE(faceEyesIn[*]) > 0.1d < 1.0d
    if (~ARRAY_EQUAL(oldEyes, [*self._faceEyes])) then begin
      update = 1b
    endif
  endif
  
  if (N_ELEMENTS(_extra) gt 0) then begin
    self->IDLitVisPlot::SetProperty, _EXTRA=_extra
  endif

  ; The order of these matter
  if (updateData) then begin
    self->_UpdateData
  endif
  if (updateColor) then begin
    self->_UpdateColor
    update = 1b ; also update the bubbles
  endif
  if (update) then begin
    self->_UpdateBubbles
  endif
  if (updateLabels) then begin
    self->_UpdateLabels
  endif
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubbleplot::GetData
;
; PURPOSE:
;      This procedure method retrieves the
;      data values of the plot.
;
; CALLING SEQUENCE:
;      Obj->[IDLBubbleplot::]GetData, arg1 [, arg2]
;
; ARGUMENTS:
;      Up to two parameters may be supplied to retrieve the data. These
;      should be named variables which will receive the requested data.
;      The data arguments are retrieved based on the documented calling
;      sequence and the number of arguments supplied by the caller:
;      One parameter:
;        obj->GetData, Values
;      Two parameters:
;        obj->GetData, Locations, Values
;
; KEYWORD PARAMETERS:
;      The mean, outlier, or suspected outlier values can be retrieved by 
;      supplying the MEANS, OUTLIER, or SUSPECTED_OUTLIER keywords.
;
;-
;----------------------------------------------------------------------------
pro IDLBubbleplot::GetData, arg1, arg2, $
                            _REF_EXTRA=_extra
  compile_opt idl2, hidden

  case n_params() of
    0:
    1: begin
        arg1 = *self.data
      end
    2: begin
        arg1 = *self.user_x_data
        arg2 = *self.user_data
      end
    else: MESSAGE, 'Incorrect number of arguments.'
  endcase
  
  ; get superclass properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->GetProperty, _EXTRA=_extra

end    


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubbleplot::_SetData
;
; PURPOSE:
;      This procedure method sets the data values of the plot.
;
; CALLING SEQUENCE:
;      Obj->[IDLBubbleplot::]_SetData, arg1 [, arg2]
;
; ARGUMENTS:
;      Up to two parameters may be supplied to set the data.
;      One parameter:
;        obj->_SetData, Values
;      Two parameters:
;        obj->_SetData, Locations, Values
;
; KEYWORD PARAMETERS:
;      The mean, outlier, or suspected outlier values can be set by 
;      supplying the MEANS, OUTLIER, or SUSPECTED_OUTLIER keywords.
;
;-
;----------------------------------------------------------------------------
pro IDLBubbleplot::_SetData, arg1, arg2, $
                   _EXTRA=_extra
  compile_opt idl2, hidden
  
  RESOLVE_ROUTINE, 'bubbleplot', /NO_RECOMPILE, /IS_FUNCTION

  case (N_Params()) of
    0: if (N_ELEMENTS(_extra) ne 0) then $
      !NULL = iBubblePlot_GetParmSet(oParmSet, _EXTRA=_extra)
    1: !NULL = iBubblePlot_GetParmSet(oParmSet, arg1, _EXTRA=_extra)
    2: !NULL = iBubblePlot_GetParmSet(oParmSet, arg1, arg2, _EXTRA=_extra)
  endcase
  if (ISA(oParmSet)) then begin
    oDataY = oParmSet->GetByName('Y')
    if (OBJ_VALID(oDataY)) then begin
      self->SetParameter, 'Y', oDataY
      oDataY->SetProperty, /AUTO_DELETE
    endif
    oDataX = oParmSet->GetByName('X')
    if (OBJ_VALID(oDataX)) then begin
       self->SetParameter, 'X', oDataX
      oDataX->SetProperty, /AUTO_DELETE
    endif
  
    ;; Notify of changed data
    self->OnDataChangeUpdate, oParmSet, '<PARAMETER SET>'
  
    ;; Clean up parameterset
    oParmSet->Remove, /ALL
    OBJ_DESTROY, oParmSet
  endif
  
  ; Send a notification message to update UI
  self->DoOnNotify, self->GetFullIdentifier(),"ADDITEMS", ''
  self->OnDataComplete, self

  oTool = self->GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->RefreshCurrentWindow
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLBubblePlot::GetXYZRange
;
; PURPOSE:
;   This function method overrides the _IDLitVisualization::GetXYZRange
;   function, taking into the tick labels.
;
function IDLBubblePlot::GetXYZRange, outxRange, outyRange, outzRange, $
                        _EXTRA=_extra
  compile_opt idl2, hidden

  if (N_ELEMENTS(*self.x_data) eq 0) then $
    return, 0

  value = 0.035 ; Best guess some nice value
  factor = value*self._szfactor*self.sizing
  outxRange = [min(*self.x_data,max=mx),mx]
  xRange = outxRange[1] - outxRange[0]
  if (xRange eq 0.0) then $
    xRange = 1.0/value/self._szfactor
  outxRange += [-xRange,xRange]*factor
  outyRange = [min(*self.data,max=mx),mx]
  yRange = outyRange[1] - outyRange[0]
  if (yRange eq 0.0) then $
    yRange = 1.0/value/self._szfactor
  outyRange += [-yRange,yRange]*factor
  outzRange = [0.0,0.0]
  return, 1

end


;----------------------------------------------------------------------------
function IDLBubblePlot::GetDataString, xyz
  compile_opt idl2, hidden

  xy = xyz[0:1]

  ; If our plot is selected, then lock the reported coordinates to the
  ; nearest data point.
  isSelected = self.IsSelected()
  if (isSelected) then begin
    xy = self->GetValueAtLocation(xy[0], xy[1])
    if (~ISA(xy)) then return, ''
  endif

  xy = STRCOMPRESS(STRING(xy, FORMAT='(G11.4)'))
  return, STRING(xy, FORMAT='("X: ",A,"  Y: ",A)')

end


;----------------------------------------------------------------------------
function IDLBubblePlot::GetValueAtLocation, arg1, arg2, $
                        DATA=data, DEVICE=device, NORMAL=normal, $
                        INTERPOLATE=interpolate, _DATASPACE=_ds
  compile_opt idl2, hidden
  
  if (N_PARAMS() lt 1) then MESSAGE, 'Incorrect number of arguments'
  hasY = N_ELEMENTS(arg2) gt 0
  xloc = arg1

  if (KEYWORD_SET(_ds)) then begin
    if (self.xLog) then xloc = alog10(xloc)
  endif
  
  if((keyword_set(device)) || (keyword_set(normal))) then begin
    converted = iconvertcoord(xloc, hasY ? arg2 : 0, $
                              DEVICE=device, NORMAL=normal, /TO_DATA)
    xloc = converted[0]
  endif

  xdata = *self.user_x_data; self.xLog ? 10^(*self.x_data) : *self.x_data
  ydata = *self.user_data; self.yLog ? 10^(*self.data) : *self.data

  if (~KEYWORD_SET(interpolate)) then begin
    minn = MIN(ABS(xdata - xloc), mnloc)
    xloc = xdata[mnloc]
    yloc = ydata[mnloc]
  endif else begin ; interpolate
    ; find 2 nearest points
    minn = MIN(ABS(xdata - xloc), loc1)
    datalength = n_elements(xdata)
  
    if(loc1 eq datalength-1) then begin
      loc2 = loc1-1
    endif else if (loc1 eq 0) then begin
      loc2 = loc1+1
    endif else begin
      if (xloc lt xdata[loc1]) then loc2 = loc1-1 else loc2 = loc1+1
    endelse
    yloc = (((ydata[loc2]-ydata[loc1])/(xdata[loc2]-xdata[loc1])) * $
          (xloc-xdata[loc2])) + ydata[loc2]
  endelse

  value = [xloc, yloc]
  return, value
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubblePlot::_UpdateSelectionVisual
;
; PURPOSE:
;      This procedure method updates the selection visual based
;      on the data.
;
; CALLING SEQUENCE:
;      Obj->[IDLBubblePlot::]_UpdateSelectionVisual
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
pro IDLBubblePlot::_UpdateSelectionVisual
  compile_opt idl2, hidden

  ; A line connecting the centers of the bubbles
  self->GetProperty, HIDE=hide
  s = SORT(*self.x_data)
  if ((N_ELEMENTS(s) eq 1) && (s[0] eq 0)) then return 
  self._oPlotSelectionVisual->SetProperty, $
    DATAX=(*self.x_data)[s], DATAY=(*self.data)[s], HIDE=hide
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubblePlot::_UpdateData
;
;-
;----------------------------------------------------------------------------
pro IDLBubblePlot::_UpdateData, sMap
  compile_opt idl2, hidden

  if (N_ELEMENTS(*self.user_data) eq 0) then return
  
  ; Copy from user data
  *self.data = *self.user_data
  *self.x_data = *self.user_x_data

  if (self.xLog && (N_ELEMENTS(*self.x_data) gt 0)) then begin
     *self.x_data = alog10(*self.x_data)
  endif
  if (self.yLog && (N_ELEMENTS(*self.data) gt 0)) then begin
     *self.data = alog10(*self.data)
  endif

  ; Ensure that the x_data matches the data
  if ((N_ELEMENTS(*self.x_data) lt N_ELEMENTS(*self.data)) || $
      (MAX(FINITE(*self.x_data)) eq 0)) then begin
    dims = SIZE(*self.data, /DIMENSIONS)
    if (dims[0] gt 0) then begin
      *self.x_data = indgen(dims[0])
    endif
  endif
  
  ; Check for a projected dataspace
  if (~N_ELEMENTS(sMap)) then $
    sMap = self->GetProjection()
  hasMap = N_TAGS(sMap) gt 0
  ; If we have data values out of the normal lonlat range, then
  ; assume these are not coordinates in degrees. Do not reproject
  if (hasMap) then begin
    minn = MIN([[*self.x_data],[*self.data]], DIMENSION=1, MAX=maxx)
    if ((minn[0] lt -360) || (maxx[0] gt 720) || $
        (minn[1] lt -90) || (maxx[1] gt 90)) then hasMap = 0
  endif
  ; Map transform the polyline data if necessary.
  if (hasMap) then begin
    data = MAP_PROJ_FORWARD(*self.x_data,*self.data, MAP=sMap)
    if (N_ELEMENTS(data) eq 2) then begin
      *self.x_data = data[0]
      *self.data = data[1]
    endif else begin
      *self.x_data = data[0,*]
      *self.data = data[1,*]
    endelse
  endif

  ; Calculate _diameter from magnitude values
  if (N_ELEMENTS(*self.magnitude) eq 0) then $
    *self.magnitude = REPLICATE(1, N_ELEMENTS(*self.data))
  if ((N_ELEMENTS(*self.magnitude) ne 0) && $
      (MAX(*self.magnitude) ne 0)) then begin
    mx = self.maxValue gt 0 ? self.maxValue : MAX(*self.magnitude)
    *self._diameter = ABS(DOUBLE(*self.magnitude)/mx)
  endif else begin
    *self._diameter = REPLICATE(1, N_ELEMENTS(*self.data))
  endelse
  
  ; Force all items to have the same number of elements
  nBubbles = N_ELEMENTS(*self.x_data) < N_ELEMENTS(*self.data) $
    < N_ELEMENTS(*self._diameter)
  if (nBubbles eq 0) then return
  *self.data = (*self.data)[0:nBubbles-1]
  *self.x_data = (*self.x_data)[0:nBubbles-1]
  *self._diameter = (*self._diameter)[0:nBubbles-1]
  
  ; Sort data to put smallest bubbles in front
  *self._sort = REVERSE(SORT(*self._diameter))
  
  ; Set up base bubble objects
  dummyData = TRANSPOSE([[REPLICATE((*self.data)[0],3)], $
                         [REPLICATE((*self.x_data)[0],3)]])
  nObjs = N_ELEMENTS(*self._oBubbles)/3
  ; Get current antialias setting
  if (nObjs ne 0) then begin
    (*self._oBubbles)[0]->GetProperty, ANTIALIAS=antialias
  endif else begin
    antialias = 1
  endelse
  ; Get current transparency
  trans = 1.0d - (self.transparency/100.)
  if (nObjs gt nBubbles) then begin
    for i=0,(nObjs-nBubbles)*3-1 do begin
      self->IDLgrModel::Remove, (*self._oBubbles)[i]
      OBJ_DESTROY, (*self._oBubbles)[i]
    endfor
    *self._oBubbles = (*self._oBubbles)[(nObjs-nBubbles)*3:*]
  endif
  if (nBubbles gt nObjs) then begin
    for i=0,(nBubbles-nObjs)-1 do begin
      ; Fill object
      c = OBJ_NEW('IDLgrPolygon', DATA=dummyData, SHADING=1, $
                  ANTIALIAS=antialias, ALPHA_CHANNEL=trans)
      self->IDLgrModel::Add, c
      ; Border line object
      l = OBJ_NEW('IDLgrPolyline', DATA=dummyData, LINESTYLE=self.linestyle, $
                  THICK=self.thick, ANTIALIAS=antialias, ALPHA_CHANNEL=trans)
      self->IDLgrModel::Add, l
      ; Label object
      t = OBJ_NEW('IDLgrText', HIDE=1, ALIGNMENT=0.5, VERTICAL_ALIGNMENT=0.5, $
                  RECOMPUTE_DIMENSIONS=2, ALPHA_CHANNEL=trans)
      self->IDLgrModel::Add, t
      *self._oBubbles = [*self._oBubbles,c,l,t]
    endfor
  endif

  ; If the data has changed, update the bubbles and labels
  self->_UpdateColor
  self->_UpdateBubbles
  self->_UpdateLabels
  self->_UpdateSelectionVisual
  
end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubbleplot::_UpdateColor
;
;-
pro IDLBubblePlot::_UpdateColor
  compile_opt idl2, hidden

  if ((SIZE(*self.color, /N_DIMENSIONS) eq 2) || $
      (N_ELEMENTS(*self.color) eq 3)) then begin
    colorTable = *self.color
  endif else begin
    colorTable = (*self.rgbTable)[*,*self.color]
  endelse

  ; Define vertColors array
  vertColors = [!NULL]
  cntr = [-0.3,0.3] ; location of bright spot
  nColors = N_ELEMENTS(colorTable[0,*])
  nBubbles = N_ELEMENTS(*self.data)
  n = N_ELEMENTS(*self._cirx)
  for i=0,nBubbles-1 do begin
    color = colorTable[*,i mod nColors]
    ; Shading goes from a bright spot of color+141 to a dark edge of color*0.6
    bright = BYTE(FIX(color)+141 < 255)
    dark = BYTE(0.6 * color)
    dst = SQRT(((*self._cirx)-cntr[0])^2+((*self._ciry)-cntr[1])^2)
    mn = MIN(dst)
    diff = MAX(dst)-mn
    vertColorsTmp = BYTARR(3,n+1)
    for j=0,n-1 do begin
      vertColorsTmp[*,j] = color - ((dst[j]-mn)/diff * (color-dark))
    endfor
    vertColorsTmp[*,n] = bright
    vertColors = [[vertColors],[vertColorsTmp]]
  endfor
  *self._vertColors = vertColors

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubblePlot::_UpdateBubbles
;
; CALLING SEQUENCE:
;      Obj->[IDLBubblePlot::]_UpdateBubbles
;
; INPUTS:
;      DataspaceX/Yrange: Optional args giving the dataspace ranges.
;
; KEYWORD PARAMETERS:
;      There are no keywords for this method.
;-
;----------------------------------------------------------------------------
pro IDLBubblePlot::_UpdateBubbles, DSxRange, DSyRange, DSzRange
  compile_opt idl2, hidden

  if ((SIZE(*self.color, /N_DIMENSIONS) eq 2) || $
      (N_ELEMENTS(*self.color) eq 3)) then begin
    colorTable = *self.color
  endif else begin
    colorTable = (*self.rgbTable)[*,*self.color]
  endelse

  nColors = N_ELEMENTS(colorTable[0,*])
  nLineColors = N_ELEMENTS((*self.linecolor)[0,*])
  nSmiles = N_ELEMENTS(*self._faceSmile)
  nEyes = N_ELEMENTS(*self._faceEyes)
  ; For each bubble recalculate the x,y data to account for changes in the axes
  for i=0,N_ELEMENTS(*self.data)-1 do begin
    ind = (*self._sort)[i]
    self->_CalculateBubble, [(*self.x_data)[ind], $
      (*self.data)[ind]], $
      (*self._diameter)[ind], SCALE=self._symsize, $
      SMILE=(*self._faceSmile)[ind mod nSmiles], $
      EYES=(*self._faceEyes)[ind mod nEyes], $  
      LINEDATA=lineData, FILLDATA=fillData
    if (N_ELEMENTS(lineData) eq 0) then continue
    
    ; Apply new data to the bubble and line objects
    if (self.shaded) then begin
      (*self._oBubbles)[i*3]->SetProperty, DATA=fillData, $
        POLYGONS=*self._conn, HIDE=0, $
        VERT_COLORS=(*self._vertColors)[*,ind*43:(ind+1)*43-1]
    endif else if (self.filled) then begin
      (*self._oBubbles)[i*3]->SetProperty, DATA=lineData, $
        POLYGONS=*self._lineConn, VERT_COLORS=0, HIDE=0, $
        COLOR=colorTable[*,ind mod nColors]
    endif else begin
      (*self._oBubbles)[i*3]->SetProperty, DATA=lineData, HIDE=1, POLYGONS=0
    endelse
    (*self._oBubbles)[i*3+1]->SetProperty, DATA=lineData, HIDE=~self.border, $
      COLOR=(*self.linecolor)[*,ind mod nLineColors], THICK=self.thick, $
      POLYLINES=(self._faceShow ? *self._faceConn : *self._lineConn)
  endfor

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubbleplot::_CalculateBubble
;
;-
pro IDLBubbleplot::_CalculateBubble, offsetIn, radiusIn, $
                                     SCALE=scaleIn, $
                                     SMILE=smileIn, EYES=eyesIn, $
                                     LINEDATA=lineData, FILLDATA=fillData
  compile_opt idl2, hidden
  
  scale = N_ELEMENTS(scaleIn) eq 3 ? scaleIn : [1,1,1]
  if (TOTAL(scale) eq 0.0) then return
  offset = N_ELEMENTS(offsetIn) eq 0 ? [0, 0] : offsetIn
  radius = N_ELEMENTS(radiusIn) eq 0 ? 1 : radiusIn[0]
  smile = (N_ELEMENTS(smileIn) eq 0 ? 1 : smileIn[0]) - 0.5
  eye = N_ELEMENTS(eyesIn) eq 0 ? 1 : eyesIn[0]

  radius ^= self.exponent
  n = N_ELEMENTS(*self._cirx)
  cirx = (*self._cirx)*radius*SQRT(self.sizing)*self._szfactor*scale[0] + $
    offset[0]
  ciry = (*self._ciry)*radius*SQRT(self.sizing)*self._szfactor*scale[1] + $
    offset[1]
  lineData = TRANSPOSE([[cirx],[ciry]])
  cntr = [-0.3,0.3]*radius*SQRT(self.sizing)*self._szfactor*scale + offset
  fillData = [[lineData],[cntr]]

  if (self._faceShow) then begin
    ; mouth
    n = 11 ; must match the number in *self._faceConn init
    ramp = findgen(n)/(n-1)
    xrange = max(cirx,MIN=mn)-mn
    mouthx = ramp*xrange*0.5 + mn + xrange*0.25
    sn = 1.0 - sin(ramp*!pi)
    yrange = max(ciry,MIN=mn)-mn
    mouthy = mn + yrange*(0.3 - 0.1*smile) + smile*sn*yrange*0.25
    ; eyes
    leftx = (*self._cirx)[0:*:3]*radius/5*eye*SQRT(self.sizing)* $
      self._szfactor*scale[0] + offset[0] - xrange*0.18
    lefty = (*self._ciry)[0:*:3]*radius/5*eye*SQRT(self.sizing)* $
      self._szfactor*scale[1] + offset[1] + yrange*0.15
    rightx = (*self._cirx)[0:*:3]*radius/5*eye*SQRT(self.sizing)* $
      self._szfactor*scale[0] + offset[0] + xrange*0.18
    righty = (*self._ciry)[0:*:3]*radius/5*eye*SQRT(self.sizing)* $
      self._szfactor*scale[1] + offset[1] + yrange*0.15
    xy = TRANSPOSE([[mouthx,leftx,rightx],[mouthy,lefty,righty]])
    lineData = [[lineData],[xy]]
  endif

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLBubbleplot::_UpdateLabels
;
;-
pro IDLBubbleplot::_UpdateLabels
  compile_opt idl2, hidden

  ; Set up base label objects
  nLabels = N_ELEMENTS(*self.labels) < N_ELEMENTS(*self.data)

  nLabelPos = N_ELEMENTS(*self.labelPos)
  n = N_ELEMENTS(*self._cirx)
  top = 0
  right = n/4
  bottom = n/2
  left = 3*n/4
  for i=0,N_ELEMENTS(*self.data)-1 do begin
    ind = (*self._sort)[i]
    if (ind ge nLabels) then begin
      (*self._oBubbles)[i*3+2]->SetProperty, HIDE=1
      continue
    endif
    (*self._oBubbles)[i*3]->GetProperty, DATA=data
    if (N_ELEMENTS(data) lt 10) then continue ; only dummy data at this time
    case (STRUPCASE((*self.labelPos)[ind mod nLabelPos])) of
      'TOP' : loc = data[*,top]
      'BOTTOM' : loc = data[*,bottom]
      'RIGHT' : loc = data[*,right]
      'LEFT' : loc = data[*,left]
       else : loc = [(*self.x_data)[ind],(*self.data)[ind]]
    endcase
    (*self._oBubbles)[i*3+2]->SetProperty, LOCATION=loc, $
      ALIGNMENT=(*self.alignment)[ind mod N_ELEMENTS(*self.alignment)], $
      VERTICAL_ALIGNMENT=(*self.vertAlign)[ind mod N_ELEMENTS(*self.vertAlign)], $
      COLOR=self.fontColor, $
      UPDIR=self.updir, BASELINE=self.baseline, FONT=self._oFont, $
      STRINGS=(*self.labels)[ind], HIDE=0
  endfor
  
end


;-----------------------------------------------------------------------------
; Override IDLgrModel::Draw so we can
; automatically adjust for changes in aspect ratio.
;
pro IDLBubblePlot::Draw, oDest, oLayer
  compile_opt idl2, hidden
  
  catch, iErr
  if (iErr ne 0) then begin
    ; Quietly return from errors so we don't crash IDL in the draw loop.
    catch, /cancel
    return
  endif
  
  ; Don't do extra work if we are in the lighting or selection pass.
  oDest->GetProperty, IS_BANDING=isBanding, $
    IS_LIGHTING=isLighting, IS_SELECTING=isSelecting
    
  if (~isLighting && ~isSelecting && ~isBanding) then begin
    oWorld = Obj_Valid(oLayer) ? oLayer->GetWorld() : Obj_New()
    if (Obj_Valid(self.Parent) && Obj_Valid(oWorld)) then begin
      matrix = self.Parent->GetCTM(TOP=oWorld)
      xpixel = Sqrt(Total(matrix[0,0:2]^2))
      ypixel = Sqrt(Total(matrix[1,0:2]^2))
      zpixel = Sqrt(Total(matrix[2,0:2]^2))
      if (xpixel eq 0) then xpixel = 1
      if (ypixel eq 0) then ypixel = 1
      if (zpixel eq 0) then zpixel = xpixel < ypixel
      oDest->GetProperty, DIMENSIONS=dim, RESOLUTION=res
      ; The factor out front was empirically determined to
      ; preserve approx the same symbol sizes as before IDL64.
      factor = 0.27d/(MIN(res)*MIN(dim))
      symSize = factor*SQRT(self.sizing)*self._szfactor / $
        [xpixel,ypixel,zpixel]
      if (MAX(ABS(symSize-self._symsize)) gt 0.00001) then begin
        self._symsize = symSize
        self->_UpdateBubbles
        self->_UpdateLabels
      endif
    endif
  endif
  
  self->IDLgrModel::Draw, oDest, oLayer
  
  ; Mac sometimes throws floating underflows...
  void = CHECK_MATH()
end


;----------------------------------------------------------------------------
;+
; IDLBubbleplot__Define
;
; PURPOSE:
;      Defines the object structure for an IDLBubbleplot object.
;-
;----------------------------------------------------------------------------
pro IDLBubbleplot__Define
  compile_opt idl2, hidden

  struct = {IDLBubbleplot, $
            inherits IDLitVisPlot, $
            data: PTR_NEW(), $
            x_data: PTR_NEW(), $
            user_data: PTR_NEW(), $
            user_x_data: PTR_NEW(), $
            _sort: PTR_NEW(), $
            color: PTR_NEW(), $
            rgbTable: PTR_NEW(), $
            linecolor: PTR_NEW(), $
            linestyle: 0, $
            labels: PTR_NEW(), $
            labelPos: PTR_NEW(), $
            magnitude: PTR_NEW(), $
            exponent: 0.0d, $
            sizing: 0.0d, $
            shaded: 0b, $
            filled: 0b, $
            border: 0b, $
            thick: 0d, $
            transparency: 0d, $
            maxValue: 0d, $
            xLog: 0b, $
            yLog: 0b, $
            _oBubbles: PTR_NEW(), $
            _diameter: PTR_NEW(), $
            _cirx: PTR_NEW(), $
            _ciry: PTR_NEW(), $
            _conn: PTR_NEW(), $
            _lineConn: PTR_NEW(), $
            _vertColors: PTR_NEW(), $
            _szfactor: 0.0d, $
            _symsize: [0d,0,0], $
            alignment: PTR_NEW(), $
            vertAlign: PTR_NEW(), $
            baseline: [0.0, 0, 0], $
            updir: [0.0, 0, 0], $
            fontColor: [0b, 0b, 0b], $
            fontName: '', $
            fontSize: 0.0, $
            fontStyle: '', $
            _oFont: OBJ_NEW(), $
            _oitFont: OBJ_NEW(), $
            _faceShow: 0b, $
            _faceConn: PTR_NEW(), $
            _faceSmile: PTR_NEW(), $
            _faceEyes: PTR_NEW() $
           }
             
end
