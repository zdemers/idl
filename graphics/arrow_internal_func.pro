; $Id: //depot/InDevelopment/scrums/IDL_Kraken/idl/idldir/lib/graphics/arrow.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;

;+
; :Description:
;    Internal routine to work around the limitations of resolve_all so a
;    resolve_all on the arrow procedure will not include new graphics 
;
;-
function arrow_internal_func, arg1, y, z, $
  DATA=data, $
  DEVICE=device, $
  NORMAL=normal, $
  POSITION=position, $
  RELATIVE=relative, $
  DEBUG=debug, $
  TARGET=ID, $
  NAME=name, $
  _REF_EXTRA=_extra
  
  compile_opt idl2, hidden
  
  nparams = n_params()

  ; Special case: In IDL 8.1 we allowed just a single argument,
  ; of the form [[x0,y0], [x1,y1]]. It's not documented, but
  ; we'll keep it working.
  if (nparams eq 1) then begin
    x = REFORM(arg1[0,*])
    y = REFORM(arg1[1,*])
    nparams = 2
  endif else if (nparams ge 2) then begin
    x = arg1
  endif

  if (nparams lt 2) then $
    MESSAGE, 'Incorrect number of arguments.'

  if (isa(X, 'STRING')) then begin
    MESSAGE, 'Style argument must be passed in after data.'
  endif
  if (isa(Y, 'STRING'))  then begin
    if (nparams gt 2) then $
      MESSAGE, 'Style argument must be passed in after data.'
    style = Y
    nparams--  
  endif
  if (isa(Z, 'STRING')) then begin
    if (nparams gt 3) then $
      MESSAGE, 'Style argument must be passed in after data.'
    style = Z
    nparams--
  endif
  if (isa(styleIn, 'STRING')) then begin
    style = styleIn
    nparams--
  endif
  
  if (n_elements(style)) then begin
    style_convert, style, COLOR=color, LINESTYLE=linestyle, THICK=thick
  endif

  nx = N_ELEMENTS(x)

  sizeX = SIZE(x, /DIM)
  sizeY = SIZE(y, /DIM)
  sizeZ = SIZE(z, /DIM)
  if (sizeX[0] ne 2) then $
    MESSAGE, 'X must be a two-element vector or a [2,n] array.'
  if (sizeY[0] ne 2) then $
    MESSAGE, 'Y must be a two-element vector or a [2,n] array.'
  if (nparams eq 3 && sizeZ[0] ne 2) then $
    MESSAGE, 'Z must be a two-element vector or a [2,n] array.'

  if (KEYWORD_SET(data) && ~ISA(toVisLayer)) then toVisLayer = 1b else toVisLayer=0b

  ; Check for unknown or illegal properties.
  if (N_ELEMENTS(_extra) gt 0) then $
    Graphic, _EXTRA=_extra, ERROR_CLASS='Arrow', /VERIFY_KEYWORDS

  ;; Set up parameters
  if (KEYWORD_SET(ID) || KEYWORD_SET(toolIDin)) then begin
    fullID = (iGetID(ID, TOOL=toolIDin))[0]
  endif
  if (N_ELEMENTS(fullID) eq 0) then $
    fullID = iGetCurrent()

  ;; Error checking
  if (fullID[0] eq '') then begin
    message, 'Graphics window does not exist.'
  endif

  ;; Get the system object
  oSystem = _IDLitSys_GetSystem(/NO_CREATE)
  if (~OBJ_VALID(oSystem)) then MESSAGE, 'Invalid object'

  ;; Get the object from ID
  oObj = oSystem->GetByIdentifier(fullID)
  if (~OBJ_VALID(oObj)) then MESSAGE, 'Invalid object'
  
  ;; Get the tool
  oTool = oObj->GetTool()
  if (~OBJ_VALID(oTool)) then MESSAGE, 'Invalid object'
  
  ;; Convert the points
  if (nparams eq 2) then begin
    points = iConvertCoord(x, y, TO_ANNOTATION_DATA=(~toVisLayer), $
      TO_DATA=toVisLayer, TARGET_IDENTIFIER=fullID, $
      TOOL=toolIDin, DATA=data, DEVICE=device, NORMAL=normal, RELATIVE=relative, $
      _EXTRA=_extra)
  endif else begin
    points = iConvertCoord(x, y, z, TO_ANNOTATION_DATA=(~toVisLayer), $
      TO_DATA=toVisLayer, TARGET_IDENTIFIER=fullID, $
      TOOL=toolIDin, DATA=data, DEVICE=device, NORMAL=normal, RELATIVE=relative, $
      _EXTRA=_extra)
  endelse

  ;; If annotation is going into the annotation layer then ensure the Z
  ;; values are as needed
  if (~toVisLayer && (nparams ne 3)) then begin
    points[2,*] = 0.99d
    zValue=0.99d
  endif
  
  npoints = (SIZE(points, /DIM))[1]

  ;; Get Manipulator
  oManip=oTool->GetByIdentifier(oTool->FindIdentifiers('*manipulators*line'))
  if (~OBJ_VALID(oManip)) then MESSAGE, 'Invalid object'

  ;; Temporarily change manipulator name
  oManip->GetProperty, NAME=oldName
  oManip->SetProperty, NAME='Arrow'
  
  ;; Get Annotation
  oDesc = oTool->GetAnnotation('Arrow')
  oArrow = oDesc->GetObjectInstance(_NO_VERTEX_VISUAL=(npoints gt 2))
  
  ;; Add annotation to proper layer in the window
  oWin = oTool->GetCurrentWindow()
  if (toVisLayer) then begin
    ;; Add to data space
    if (OBJ_HASMETHOD(oObj, 'GetDataSpace')) then begin
      oDS = oObj->GetDataSpace()
    endif else begin
      ;; The view does not have a getdataspace method
      if (OBJ_ISA(oObj, 'IDLitgrView')) then begin
        dsID = (oObj->FindIdentifiers('*DATA SPACE*'))[0]
      endif else begin
        ;; Fall back to finding first data space in the window
        dsID = oWin->FindIdentifiers('*Data Space')
      endelse
      oDS = oSystem->GetByIdentifier(dsID)
    endelse
    oDS->Add, oArrow, /NO_UPDATE
  endif else begin
    oWin->Add, oArrow, LAYER='ANNOTATION'
  endelse
  
  ; Proper name
  if (N_ELEMENTS(name) eq 1) then begin
    oArrow->GetProperty, PARENT=oParent
    oObjs = oParent->Get(/ALL, COUNT=cnt)
    sNames = []
    foreach oObj, oObjs do begin
      oObj->GetProperty, IDENTIFIER=objName
      sNames = [sNames, STRUPCASE(objName)]
    endforeach
    newName = IDLitGetUniqueName(sNames, name)
    oArrow->SetProperty, IDENTIFIER=newName, NAME=name
  endif
  
  ;; Set data on annotation
  oArrow->SetProperty, DATA=points, ZVALUE=zValue, _EXTRA=_extra
  oArrow->SetAxesRequest, 0, /ALWAYS
  
  oTool->RefreshCurrentWindow

  ;; Put old name back  
  oManip->SetProperty, NAME=oldName
  
  ;; Retrieve ID of new line
  if (Arg_Present(idOut)) then $
    idOut = oArrow->GetFullIdentifier()
 
  Graphic__define
  oGraphic = OBJ_NEW('Arrow', oArrow)

  if (ISA(position)) then begin
    oGraphic->_SetProperty, POSITION=position, DEVICE=device
  endif

  return, oGraphic
end