;+
; :Description:
;    Create IDL Map graphic.
;
; :Params:
;    Projection
;
; :Keywords:
;    _REF_EXTRA
;
; :Returns:
;    Object Reference
;-
function MAP, projection, $
  DEBUG=debug, $
  FILL_COLOR=fillColor, $
  LIMIT=limit, $
  TEST=test, $
  UVALUE=uvalue, $
  XRANGE=xrange, $
  YRANGE=yrange, $
  _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error
@map_proj_init_commonblock

  if (ISA(projection,'STRING') && N_ELEMENTS(projection) eq 1) then begin
    if (projection ne '') then begin
      ; Verify that this is a valid projection. Also pass in all map
      ; projection keywords to make sure they are valid for this projection.
      mapStruct = MAP_PROJ_INIT(projection, /GCTP, $
        _EXTRA=[c_keywordNames, 'ELLIPSOID', 'LIMIT', 'RADIANS'])
      projection = mapStruct.up_name
    endif
  endif else begin
    if ~KEYWORD_SET(test) then $
      MESSAGE, 'Projection must be specified as a scalar string.'
    projection = 'Mollweide'
  endelse
  
  if (KEYWORD_SET(test)) then begin
    if (~ISA(fillColor)) then fillColor = "Powder Blue"    
  endif

  name = 'Map'
  Graphic, name, MAP_PROJECTION=projection, LIMIT=limit, $
    ERROR_CLASS='MapProjection', $
    FILL_COLOR=fillColor, $
    _EXTRA=ex, GRAPHIC=graphic, LAYOUT=[1,1,1], ASPECT_RATIO=1

  ; We usually get a MapGrid object back, since that is the selected item.
  ; So instead, find our VisMapProjection object and wrap it instead.
  if (~ISA(graphic, 'MapProjection')) then begin
    graphic = graphic.MapProjection
  endif
  
  ; Map projection limit - if the user sets the limit, then force the
  ; dataspace UV range to match the new limit. This ensures that if you
  ; overplot new data on top of the map, that it doesn't affect the range.
  if (ISA(limit)) then begin
    sMap = graphic.GetMapStructure()
    if (N_TAGS(sMap) gt 0) then begin
      if (~ISA(xrange)) then $
        xrange = sMap.uv_box[[0,2]]
      if (~ISA(yrange)) then $
        yrange = sMap.uv_box[[1,3]]
    endif
  endif

  ; Since a map is created without data, the xrange and yrange are never set
  ; in idlitsystem__define::CreateVisualization()
  if (ISA(xrange) || ISA(yrange)) then begin
    graphic->SetProperty, XRANGE=xrange, YRANGE=yrange
  endif

  if (N_ELEMENTS(uvalue) gt 0) then $
    graphic.SetProperty, UVALUE=uvalue

  if (KEYWORD_SET(test)) then begin
    mc = MAPCONTINENTS(FILL_COLOR='light green')
    t = TEXT(0, 90, '$\Frosty$', /DATA, CLIP=0, $
      ALIGNMENT=0.5, VERTICAL_ALIGNMENT=0.5, $
      FONT_SIZE=0.5, RECOMPUTE_DIMENSIONS=0, /UNDOC)
  endif

  return, graphic
end
