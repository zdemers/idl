; $Id: //depot/InDevelopment/scrums/ENVI_Yukon/idl/idldir/lib/butterworth.pro#3 $
;
; Copyright (c) 2005-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.       
;-------------------------------------------------------------------------------

;+
;  Helper function to format colortable list output. Returns n spaces
;-------------------------------------------------------------------------------
function IDL_CTSpaces,n
  compile_opt idl2,hidden
  
  ret=''
  for i=0,n-1 do begin
    ret+=' '
  endfor
  return,ret
end

;+
;  Down samples a colortable on even intervals
;-------------------------------------------------------------------------------
function IDL_CTDownsample, table, ncolors
  compile_opt idl2,hidden
  
  s=size(table,/dimensions)
  
  if ncolors eq 1 then begin
    return,table[s[0]/2,*]
  endif
  
  return,table[indgen(ncolors)*(s[0]-1)/(ncolors-1),*]
end

;+
;  Finds a colortable if it exists.
;-------------------------------------------------------------------------------
function IDL_CTFind, colorsIn, GET_NAMES=names, $ 
                             NCOLORS=ncolors,     $
                             SILENT=silent
  compile_opt idl2,hidden
  
  if n_elements(colorsIn) ne 0 then colors=colorsIn
  retval=!NULL
  
  ; Read in colors1.tbl instead of going through loadct (avoid xserver)
  get_lun, lun
  filename = filepath('colors1.tbl', subdir=['resource', 'colors'])
  openr,lun, filename, /block
  ntables = 0b
  readu, lun, ntables
  names = bytarr(32, ntables)
  point_lun, lun, ntables * 768L + 1  ;Read table names
  readu, lun, names
  names = strtrim(names, 2)
  
  ; Determine if we should load a colortable or print a colortable
  if n_elements(colors) eq 0 then begin
    if ~keyword_set(silent) then begin
      print,'Available Colortables:'
      for i=0, n_elements(names)-1 do begin
        if i MOD 4 eq 3 || i eq n_elements(names)-1 then begin
          f="(i2,': ',a,a)"
        endif else begin
          f="(i2,': ',a,a,$)"
        endelse
        ; NOTE: current max name length is 21 characters
        print,i,names[i],IDL_CTSpaces(23-strlen(names[i])),format=f
      endfor
    endif
  endif else begin
    if size(colors,/type) eq 7 then begin
      w=where(strupcase(colors) eq strupcase(names),c)
      if c eq 0 then begin
        free_lun,lun
        message,'Unknown color table name',/NONAME
      endif
      colors=w[0]
    endif
    
    if colors le n_elements(names)-1 && colors ge 0 then begin
      
      ;-- LOADCT replacement begins --
      if keyword_set(silent) eq 0 then $
        message,'COLORTABLE: Loading table ' + names[colors],/INFO,/NONAME
      aa=assoc(lun, bytarr(256),1)  ;Read 256 long ints
      table = [[aa[colors*3]],[aa[colors*3+1]],[aa[colors*3+2]]]
      ;-- Ends --
      
      ;loadct,colors,RGB_TABLE=table
      retval=IDL_CTDownsample(table,ncolors)
    endif else begin
      free_lun,lun
      message,'Table number must be from 0 to ' + strtrim(n_elements(names)-1,2),/NONAME
    endelse
    
  endelse
  
  free_lun,lun
  
  return,retval
end

;+
;  Returns an approximate "exponential" interpolation between two points.
;-------------------------------------------------------------------------------
function IDL_CTStretch,v,n,s
  compile_opt idl2,hidden
  
  if s gt 100 || s lt -100 then begin
    message,'Stretch value must be between -100 and 100',/NONAME
  end 
  
  ret=indgen(n)*0
  p=round(abs(s)/100.*n)
  
  if s lt 0 then begin
    ret[n-p:n-1]=v[1]
    if p ne n then begin
      ret[0:n-p-1]=interpol([v[0],v[1]],n-p)
    endif
  endif else begin
    ret[0:p-1]=v[0]
    if p ne n then begin
      ret[p:n-1]=interpol([v[0],v[1]],n-p)
    endif
  endelse
  
  return,ret
end

;+
;  Generates a ncolors element array with samples between color1 and color2
;-------------------------------------------------------------------------------
function IDL_CTColorramp,color1,color2,ncolors,STRETCH=stretch
  compile_opt idl2,hidden
  
  s=(n_elements(stretch) eq 0) ? 0 : stretch
  
  color_ramp=indgen(ncolors,3)
  color_ramp[*,0]=fix(IDL_CTStretch([long(color1[0]),long(color2[0])],ncolors,s))
  color_ramp[*,1]=fix(IDL_CTStretch([long(color1[1]),long(color2[1])],ncolors,s))
  color_ramp[*,2]=fix(IDL_CTStretch([long(color1[2]),long(color2[2])],ncolors,s))
  
  return,color_ramp
end


;+
;  Generates a colortable based on the specified parameters
;-------------------------------------------------------------------------------
function IDL_CTGenerate, colors, BACKGROUND=background, $
                                         STRETCH=stretch,       $ 
                                         INDICES=indices,       $
                                         NCOLORS=ncolors
  compile_opt idl2,hidden
  
  numcolors=n_elements(colors)
  color_size=size(colors)
  
  ; Set default background or convert background into expected format
  if n_elements(background) eq 0 then begin 
    background=[0,0,0]
  endif else begin
    if (size(background,/TYPE) eq 7 && n_elements(background) eq 1) || n_elements(background) eq 3 then begin
      style_convert,temporary(background),COLOR=background
    endif else begin
      message,'BACKGROUND must be a RGB triplet or string',/NONAME
    endelse
  endelse
  
  ; Transform input RBG color array into expected format
  if color_size[-2] ne 7 then begin
    !null=where(size(colors,/dimensions) eq 3,c)
    if c eq 0 then message, 'Invalid dimensions of COLORS',/NONAME
    if color_size[-1] MOD 3 ne 0 then message, 'Invalid size of COLORS',/NONAME
    if (~isa(colors,/number)) then message,'Invalid type of COLORS',/NONAME
    
    if n_elements(color_size) eq 4 then begin
      colors=transpose(colors)
      color_size=size(colors)
    endif
    
    if color_size[1] eq 3 then begin
      colors=transpose(colors)
      numcolors=color_size[2]
    endif else begin
      numcolors=color_size[1]
    endelse
    
    for i=0, n_elements(colors[*,0])-1 do begin
      !NULL=where(colors[i,*] lt 0,c1)
      !NULL=where(colors[i,*] gt 255,c2)
      if c1 gt 0 || c2 gt 0 then begin
        message,'Illegal color: [' + strtrim(colors[i,0],2) + ',' + $
                                     strtrim(colors[i,1],2) + ',' + $
                                     strtrim(colors[i,2],2) + ']',/NONAME
      end
    endfor
  endif
  
  ; Generate indices, convienent to return single color table if user requested it
  if n_elements(indices) eq 0 then begin
    if numcolors eq 1 then begin
      if color_size[-2] eq 7 then begin
        style_convert,colors,COLOR=index_color
        if n_elements(index_color) eq 0 then return,!NULL
      endif else begin
        index_color=colors
      endelse

      return,IDL_CTColorramp(index_color,index_color,ncolors)
    endif else begin
      indices=indgen(numcolors)*(ncolors-1)/(numcolors-1)
    endelse
  endif else if n_elements(indices) ne numcolors then begin
    message,'INDICES length must equal number of COLORS',/NONAME
  endif
  
  ; Set default stretch
  if n_elements(stretch) eq 0 then stretch=indgen(numcolors-1)*0
  
  if n_elements(stretch) ne numcolors-1 then begin
    message,'STRETCH length must be one less than the number of COLORS',/NONAME
  endif
  
  ; Generate colortable
  color_table=IDL_CTColorramp(background,background,ncolors)
  for i=0, n_elements(indices)-2 do begin
    if color_size[0] eq 1 then begin
      style_convert,colors[i],COLOR=index_color1
      style_convert,colors[i+1],COLOR=index_color2
      if n_elements(index_color1) eq 0 || n_elements(index_color2) eq 0 then return,!NULL
    endif else begin
      index_color1=colors[i,*]
      index_color2=colors[i+1,*]
    endelse
    color_table[indices[i]:indices[i+1],*]=IDL_CTColorramp(index_color1,index_color2,indices[i+1]-indices[i]+1,STRETCH=stretch[i])
  endfor

  return,color_table
end

;+
; :Description:
;    Generates or retrieves an n_element colortable from color1.tbl
;
; :Params:
;    colors
;       index, name, or seed for a colortable
;
; :Keywords:
;    BACKGROUND
;       Default color which should be used for the background
;
;    STRETCH
;       Ramp factor in scaling the colortable
;
;    GET_NAMES
;       Named variable which contains the names of all the colortables
;
;    INDICES
;       Specifies the indicies of the colors in the colortable
;
;    NCOLORS
;       Number of colors the colortable should have, default is 256
;
;    NULL
;       If set returns !NULL instead of -1
;    
;    REVERSE
;       If set reverses the colortable
;
;-------------------------------------------------------------------------------
function Colortable, colors, BACKGROUND=background, $
                             GET_NAMES=get_names,   $
                             INDICES=indices,       $
                             NCOLORS=ncolors,       $
                             REVERSE=reverse,       $
                             STRETCH=stretch,       $
                             TRANSPOSE=transpose,   $
                             VERBOSE=verbose
  compile_opt idl2,hidden
  
  ; Catch errors
  ON_ERROR, 2
  CATCH, iErr
  if (iErr ne 0) then begin
    CATCH, /CANCEL
    MESSAGE, !ERROR_STATE.msg
  endif
  
  if n_elements(ncolors) eq 0 then ncolors=256
  if ~(ncolors ge 1 && ncolors le 256) then begin
    message,'NCOLORS must be between 1 and 256',/NONAME
  endif
  
  if size(colors,/N_DIMENSIONS) eq 0 then begin
    ;Find and parse colortable based on input parameters
    if arg_present(get_names) && ~keyword_set(verbose) then silent=1
    if isa(colors) && ~keyword_set(verbose) then silent=1 
    ct=IDL_CTFind(colors,NCOLORS=ncolors,GET_NAMES=get_names,SILENT=silent)
  endif else begin
    ; Generate a colortable based on input parameters
    ct=IDL_CTGenerate(colors,INDICES=indices,NCOLORS=ncolors,BACKGROUND=background,STRETCH=stretch)
  end
  
  ct=keyword_set(reverse) ? reverse(ct,1) : ct
  ct=keyword_set(transpose) ? transpose(ct) : ct
  
  return, ct
end 