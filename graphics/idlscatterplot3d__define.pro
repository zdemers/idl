; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlscatterplot3d__define.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLScatterplot3d
;
; PURPOSE:
;    The IDLScatterplot3d class is the component wrapper for IDLgrPlot
;
; CATEGORY:
;    Components
;
; MODIFICATION HISTORY:
;     Written by:   AGEH, 12/2012
;-

;----------------------------------------------------------------------------
; Lifecycle Methods
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAMES:
;   IDLScatterplot3d::Init
;
; PURPOSE:
;   Initialize this component
;
;   NOTE: Init methods are special lifecycle methods, and as such
;   cannot be called outside the context of object creation.  This
;   means that in most cases, you cannot call the Init method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Init method
;   from within the Init method of the subclass.
;
; CALLING SEQUENCE:
;
;   Obj = OBJ_NEW('IDLScatterplot3d', [X, Y, Z])
;
; INPUTS:
;   X: Vector of X coordinates
;   Y: Vector of Y coordinates
;   Z: Vector of Z coordinates
;
; OUTPUTS:
;   This function method returns 1 on success, or 0 on failure.
;
;-
function IDLScatterplot3d::Init, _REF_EXTRA=_extra
  compile_opt idl2, hidden

  ; Initialize superclass
  if (~self->IDLitVisPlot3d::Init(/REGISTER_PROPERTIES, NAME='IDLScatterplot3d', $
                                ICON='plot', TYPE='IDLPLOT', $
                                DESCRIPTION='An IDLScatterplot Visualization', $
                                _EXTRA=_extra)) then return, 0

  self.magnitude = PTR_NEW(/ALLOCATE_HEAP)
  self._userMagnitude = PTR_NEW(/ALLOCATE_HEAP)
  
  self->IDLitVisPlot3d::SetProperty, RGB_TABLE=0, LINESTYLE=6
  
  ; Set any properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLScatterplot3d::SetProperty,  _EXTRA=_extra

  RETURN, 1 ; Success

end


;----------------------------------------------------------------------------
;+
; METHODNAME:
;   IDLScatterplot3d::Cleanup
;
; PURPOSE:
;   This procedure method performs all cleanup on the object.
;
;   NOTE: Cleanup methods are special lifecycle methods, and as such
;   cannot be called outside the context of object destruction.  This
;   means that in most cases, you cannot call the Cleanup method
;   directly.  There is one exception to this rule: If you write
;   your own subclass of this class, you can call the Cleanup method
;   from within the Cleanup method of the subclass.
;
; CALLING SEQUENCE:
;   OBJ_DESTROY, Obj
;     or
;   Obj->[IDLScatterplot3d::]Cleanup
;
;-
pro IDLScatterplot3d::Cleanup, _EXTRA=_extra
  compile_opt idl2, hidden

  PTR_FREE, [self.magnitude, self._userMagnitude]
  
  ; Cleanup superclass
  self->IDLitVisPlot3d::Cleanup

end


;----------------------------------------------------------------------------
; Property Interface
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLScatterplot3d::GetProperty
;
; PURPOSE:
;      This procedure method retrieves the
;      value of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLScatterplot3d::]GetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLScatterplot3d::Init followed by the word "Get"
;      can be retrieved using IDLScatterplot3d::GetProperty.
;
;-
pro IDLScatterplot3d::GetProperty, $
    MAGNITUDE=magnitude, $
    _REF_EXTRA=_extra

  compile_opt idl2, hidden

  if (ARG_PRESENT(magnitude)) then $
    magnitude = *self.magnitude

  ; get superclass properties
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitVisPlot3d::GetProperty, _EXTRA=_extra

end

;----------------------------------------------------------------------------
;+
; METHODNAME:
;      IDLScatterplot3d::SetProperty
;
; PURPOSE:
;      This procedure method sets the value
;      of a property or group of properties.
;
; CALLING SEQUENCE:
;      Obj->[IDLScatterplot3d::]SetProperty
;
; INPUTS:
;      There are no inputs for this method.
;
; KEYWORD PARAMETERS:
;      Any keyword to IDLScatterplot3d::Init followed by the word "Set"
;      can be set using IDLScatterplot3d::SetProperty.
;-
pro IDLScatterplot3d::SetProperty, $
    MAGNITUDE=magnitude, $
    RGB_TABLE=rgbTableIn, $
    _EXTRA=_extra

  compile_opt idl2, hidden

  if (N_ELEMENTS(rgbTableIn) gt 0) then begin
    rgbTable = rgbTableIn
    self->IDLitVisPlot3d::GetProperty, RGB_TABLE=rgbTableOld
    nColOld = N_ELEMENTS(rgbTableOld[0,*])
    self->IDLitVisPlot3d::SetProperty, RGB_TABLE=rgbTable
    self->IDLitVisPlot3d::GetProperty, RGB_TABLE=rgbTableNew
    nColNew = N_ELEMENTS(rgbTableNew[0,*])
    if ((nColOld ne nColNew) && (N_ELEMENTS(magnitude) eq 0) && $
        (N_ELEMENTS(*self._userMagnitude) ne 0)) then begin
      magnitude = *self._userMagnitude
    endif
  endif
  
  if (N_ELEMENTS(magnitude) gt 0) then begin
    self->IDLitVisPlot3d::GetData, y
    self->IDLitVisPlot3d::GetProperty, RGB_TABLE=rgbTable
    nCol = N_ELEMENTS(rgbTable[0,*])
    if (ARRAY_EQUAL(rgbTable, [-1,-1,-1])) then begin
      self->IDLitVisPlot3d::SetProperty, RGB_TABLE=0
      nCol = 256
    endif
    *self._userMagnitude = magnitude
    if (SIZE(magnitude, /TNAME) ne 'BYTE') then begin
      magnitude = BYTSCL(magnitude, TOP=nCol)
    endif
    *self.magnitude = magnitude[*]
    self->IDLitVisPlot3d::SetProperty, VERT_COLORS=magnitude
  endif

  if (N_ELEMENTS(_extra) gt 0) then begin
    self->IDLitVisPlot3d::SetProperty, _EXTRA=_extra
    update = 1b
  endif

end


;----------------------------------------------------------------------------
;+
; IDLScatterplot3d__Define
;
; PURPOSE:
;      Defines the object structure for an IDLScatterplot3d object.
;-
;----------------------------------------------------------------------------
pro IDLScatterplot3d__Define
  compile_opt idl2, hidden

  struct = {IDLScatterplot3d, $
            inherits IDLitVisPlot3d, $
            _userMagnitude: PTR_NEW(), $
            magnitude: PTR_NEW() $
           }
             
end
