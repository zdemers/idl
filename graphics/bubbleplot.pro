; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/bubbleplot.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create IDL Bubbleplot graphic.
;
; :Params:
;    arg1 : optional generic argument
;    arg2 : optional generic argument
;
; :Keywords:
;    _REF_EXTRA
;
;-

;-----------------------------------------------------------------------
; Helper routine to construct the parameter set.
; If no parameters are supplied then oParmSet will be undefined.
;
function iBubbleplot_GetParmSet, oParmSet, parm1, parm2, $
                                 TEST=test, MAGNITUDE=magnitude, _EXTRA=_EXTRA
  compile_opt idl2, hidden
  
  nParams = N_Params()
  if (Keyword_Set(test)) then begin
    n = 9
    parm1 = randomn(seed,n)
    parm2 = randomn(seed,n)
    min = ABS(MIN([parm1,parm2]))
    parm1 += min
    parm2 += min
    nParams = 3
    if (N_ELEMENTS(magnitude) eq 0) then $
      magnitude = randomu(seed,n)
  endif
  
  if (nParams le 1) then return, ''
  
  oParmSet = OBJ_NEW('IDLitParameterSet', NAME='Plot parameters', $
                     ICON='plot', DESCRIPTION='Plot parameters')
  oParmSet->SetAutoDeleteMode, 1b
  
  ; Check for undefined variables.
  if (N_ELEMENTS(parm1) eq 0) then $
    MESSAGE, 'First argument is an undefined variable.'
    
  case (TOTAL([ISA(parm1),ISA(parm2)])) of
    2 : begin
      if (N_ELEMENTS(parm1) eq N_ELEMENTS(parm2)) then begin
        ; eliminate leading dimensions of 1
        parm1 = reform(parm1)
        parm2 = reform(parm2)
        oDataX = obj_new('IDLitDataIDLVector', parm1, NAME='X')
        oParmSet->add, oDataX, PARAMETER_NAME='X'
        oDataY = obj_new('IDLitDataIDLVector', parm2, NAME='Y')
        oParmSet->add, oDataY, PARAMETER_NAME='Y'
      endif else begin
        MESSAGE, 'X and Y mst have the same number of elements'
      endelse
    end
    else : message, 'Both X and Y are required inputs'
  endcase
      
  ; Set the appropriate visualization type.
  visType = 'PLOT'
  oParmSet->SetProperty, TYPE=visType
  
  return, visType
  
end

;-------------------------------------------------------------------------
; Needed because Graphic calls 'i'+graphicname
pro iBubbleplot, parm1, parm2, $
    DEBUG=debug, $
    IDENTIFIER=identifier, $
    MAGNITUDE=magnitude, $
    _REF_EXTRA=_extra
    
  compile_opt hidden, idl2
  
  ; Note: The error handler will clean up the oParmSet container.
  @idlit_itoolerror.pro
  
  visType = iBubbleplot_GetParmSet(oParmSet, parm1, parm2, $
                                   MAGNITUDE=magnitude, _EXTRA=_extra)
  
  identifier = IDLitSys_CreateTool("Plot Tool", $
                                   INITIAL_DATA=oParmSet, $
                                   WINDOW_TITLE='IDL Scatter Plot', $
                                   VISUALIZATION_TYPE='Bubbleplot', $
                                   MAGNITUDE=magnitude, _EXTRA=_extra)
    
end

;-------------------------------------------------------------------------
function bubbleplot, arg1, arg2, arg3, $
                      LAYOUT=layoutIn, $
                      _REF_EXTRA=ex
  compile_opt idl2, hidden
  ON_ERROR, 2
  
  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams lt 1 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'
  
  switch (nparams) of
    2: if (~ISA(arg2, /NUMBER)) then MESSAGE, 'Input must be an array.'
    1: if (~ISA(arg1, /NUMBER)) then MESSAGE, 'Input must be an array.'
    else :
  endswitch
  
  layout = N_ELEMENTS(layoutIn) eq 3 ? layoutIn : [1,1,1]
  ; defaults
  antialias = 1
  xstyle = 0
  ystyle = 0
  
  name = 'Bubbleplot'
  case nparams of
    0: Graphic, name, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      /AUTO_CROSSHAIR, ANTIALIAS=antialias
    1: Graphic, name, arg1, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      /AUTO_CROSSHAIR, ANTIALIAS=antialias
    2: Graphic, name, arg1, arg2, $
      LAYOUT=layout, _EXTRA=ex, GRAPHIC=graphic, XSTYLE=xstyle, YSTYLE=ystyle, $
      /AUTO_CROSSHAIR, ANTIALIAS=antialias
  endcase

  oTool = graphic->GetTool()
  oTool->RefreshCurrentWindow
  
  return, graphic
  
end
