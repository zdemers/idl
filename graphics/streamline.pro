;+
; :Description:
;    Create IDL Vector graphic.
;
; :Params:
;    arg1 : optional generic argument
;    arg2 : optional generic argument
;
; :Keywords:
;    _REF_EXTRA
;
;-
function streamline, arg1, arg2, arg3, arg4, _REF_EXTRA=ex

  compile_opt idl2, hidden
  @graphic_error
  
  nparams = n_params()
  case (nparams) of
    1: begin
      return, call_function('streamline_internal_func',arg1, _EXTRA=ex)
    end
    2: begin
      return, call_function('streamline_internal_func',arg1, arg2, _EXTRA=ex)
    end
    3: begin
      return, call_function('streamline_internal_func',arg1, arg2, arg3, _EXTRA=ex)
    end
    4: begin
      return, call_function('streamline_internal_func',arg1, arg2, arg3, arg4, _EXTRA=ex)
    end
    else: begin
      Message,'Incorrect number of arguments.'
    end
  endcase

  return, call_function('streamline_internal_func',arg1, arg2, arg3, arg4, _EXTRA=ex)
end


;--------------------------------------------------------------------------
; This is the old STREAMLINE procedure. We need to define its call here,
; and route the call to our internal .pro routine. Otherwise IDL will never
; find the old procedure name.
;
pro streamline,inverts,conn,normals,outverts,outconn, _REF_EXTRA=ex
  compile_opt hidden
  on_error, 2
  streamline_internal,inverts,conn,normals,outverts,outconn, _EXTRA=ex
end

