; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/annotatecolorbar__define.pro#1 $
;
; Copyright (c) 2011-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;----------------------------------------------------------------------------
; Purpose:
;   Create a Colorbar annotation.
;

;---------------------------------------------------------------------------
function AnnotateColorbar::Init, strType, _EXTRA=_extra

    compile_opt idl2, hidden

    ; Init our superclass
    status = self->IDLitManipAnnotation::Init(NAME='Colorbar', $
        KEYBOARD_EVENTS=0, $
        /TRANSIENT_DEFAULT, _EXTRA=_extra)

    if (status eq 0)then return, 0

    return, 1
end


;---------------------------------------------------------------------------
pro AnnotateColorbar::Cleanup
  compile_opt idl2, hidden
  Obj_Destroy, self.oColorbar
  self->IDLitManipAnnotation::Cleanup
end


;---------------------------------------------------------------------------
function AnnotateColorbar::DoAction, oTool

  compile_opt idl2, hidden

  catch, iErr
  if (iErr ne 0) then begin
    catch, /CANCEL
    Message, /RESET
    return, 0
  endif
  Colorbar = Colorbar()
  return, Isa(Colorbar)
end


;---------------------------------------------------------------------------
function AnnotateColorbar::QueryAvailability, oTool, selTypes

  compile_opt idl2, hidden

  if (~Isa(oTool)) then return, 0

  ; Trick the Colorbar class into creating itself without creating a Colorbar.
  if (~Isa(self.oColorbar)) then $
    self.oColorbar = Obj_New('Colorbar', self)

  ; See if we have any valid Colorbar targets.
  oTargets = self.oColorbar->_FindTargets(oTool)
  
  return, ISA(oTargets)
end


;---------------------------------------------------------------------------
pro AnnotateColorbar__Define

    compile_opt idl2, hidden

    ; Just define this bad boy.
    void = {AnnotateColorbar, $
            inherits IDLitManipAnnotation, $
            oColorbar: Obj_New() $
           }

end
