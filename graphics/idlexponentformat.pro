; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlexponentformat.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Returns number in exponential notation
;
; :Params:
;    axis - not used
;    index - not used
;    number - number to be converted
;-
;-------------------------------------------------------------------------------
function IDLExponentFormat, axis, index, number
  compile_opt idl2,hidden
  
  resolve_routine,'idlscientificformat',/IS_FUNCTION
  return,IDLScientificFormat(axis,index,number,/exponent)
end