;+
; :Description:
;    Create IDL Image graphic.
;
; :Params:
;    arg1 : optional generic argument
;    arg2 : optional generic argument
;
; :Keywords:
;    _REF_EXTRA
;
; :Returns:
;    Object Reference
;-
function image, arg1, arg2, arg3, $
  IRREGULAR=irregular, $
  ASPECT_RATIO=aspectIn, DEBUG=debug, _REF_EXTRA=ex

  compile_opt idl2, hidden
@graphic_error

  nparams = n_params()
  hasTestKW = ISA(ex) && MAX(ex eq 'TEST') eq 1
  if (nparams eq 0 && ~hasTestKW) then $
    MESSAGE, 'Incorrect number of arguments.'

  if (nparams eq 2) then $
    MESSAGE, 'Incorrect number of arguments.'
    
  if (nparams ge 1 && ~ISA(arg1, /ARRAY) && ~ISA(arg1, 'string')) then $
    MESSAGE, 'Input must be an array.'
  if (nparams ge 2 && ~ISA(arg2, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'
  if (nparams ge 3 && ~ISA(arg3, /ARRAY)) then $
    MESSAGE, 'Input must be an array.'
    

  RESOLVE_ROUTINE, 'GRAPHIC', /NO_RECOMPILE
   
  RESOLVE_ROUTINE, 'iImage', /NO_RECOMPILE
  
  aspect = N_ELEMENTS(aspectIn) eq 1 ? aspectIn : 1    
  name = 'Image'
  case nparams of
    0: Graphic, name, _EXTRA=ex, GRAPHIC=graphic, $
      ASPECT_RATIO=aspect
    1: Graphic, name, arg1, _EXTRA=ex, GRAPHIC=graphic, $
      ASPECT_RATIO=aspect
    2: Graphic, name, arg1, arg2, _EXTRA=ex, GRAPHIC=graphic, $
      ASPECT_RATIO=aspect
    3: begin
      if (~ISA(irregular)) then begin
        irregular = Graphic_IsIrregular(arg1, arg2, arg3, $
          _EXTRA='GRID_UNITS', SPHERE=sphere)
      endif
      if (KEYWORD_SET(irregular)) then begin
        Graphic_GridData, arg1, arg2, arg3, zOut, xOut, yOut, SPHERE=sphere
        Graphic, name, zOut, xOut, yOut, _EXTRA=ex, GRAPHIC=graphic, $
          ASPECT_RATIO=aspect
      endif else begin
        if (arg2[0] gt arg2[-1]) || (arg3[0] gt arg3[-1]) then begin
          conv1 = (arg2[0] gt arg2[-1]) ? rotate(arg1,5) : arg1
          conv1 = (arg3[0] gt arg3[-1]) ? rotate(conv1,7) : conv1
          conv2 = (arg2[0] gt arg2[-1]) ? reverse(arg2) : arg2    
          conv3 = (arg3[0] gt arg3[-1]) ? reverse(arg3) : arg3
          Graphic, name, conv1, conv2, conv3, _EXTRA=ex, GRAPHIC=graphic, $
            ASPECT_RATIO=aspect          
        endif else begin
          Graphic, name, arg1, arg2, arg3, _EXTRA=ex, GRAPHIC=graphic, $
            ASPECT_RATIO=aspect
        endelse
      endelse
      end
  endcase

  return, graphic
end
