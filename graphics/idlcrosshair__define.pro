; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlcrosshair__define.pro#1 $
;
; Copyright (c) 2011-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; CLASS_NAME:
;    IDLCrosshair
;
; PURPOSE:
;    The IDLCrosshair class implements a crosshair object
;    for the Graphics system.
;
;-


;----------------------------------------------------------------------------
function IDLCrosshair::Init, _REF_EXTRA=_extra

  compile_opt idl2, hidden

  ; Initialize superclass
  if (~self->IDLitManipulatorVisual::Init(NAME="Crosshair", $
    DESCRIPTION="Crosshair", $
    MANIPULATOR_TARGET=0, $
    _EXTRA=_extra)) then $
    return, 0

  self._oLine = obj_new("IDLgrPolyline", /REGISTER_PROPERTIES, /PRIVATE, $
    LINESTYLE=1, ALPHA=0.5)
  self->Add, self._oLine, /NO_UPDATE, /NO_NOTIFY

  ; Register all properties.
  self->IDLCrosshair::_RegisterProperties

  self._snap = 1b

  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLCrosshair::SetProperty, _EXTRA=_extra

  return, 1 ; Success
end


;----------------------------------------------------------------------------
pro IDLCrosshair::Cleanup
  compile_opt idl2, hidden

  ; Cleanup superclass
  self->IDLitManipulatorVisual::Cleanup
end


;----------------------------------------------------------------------------
; IDLCrosshair::_RegisterProperties
;
; Purpose:
;   Internal routine that will register all properties supported by
;   this object.
;
; Keywords:
;   UPDATE_FROM_VERSION: Set this keyword to a scalar representing the
;     component version from which this object is being updated.  Only
;     properties that need to be registered to update from this version
;     will be registered.  By default, all properties associated with
;     this class are registered.
;
pro IDLCrosshair::_RegisterProperties, $
  UPDATE_FROM_VERSION=updateFromVersion

  compile_opt idl2, hidden

  registerAll = ~KEYWORD_SET(updateFromVersion)

  if (registerAll) then begin
      self->Aggregate, self._oLine
      
      self->SetPropertyAttribute,['SHADING'], /HIDE

      self->RegisterProperty, 'TRANSPARENCY', /INTEGER, $
          NAME='Transparency', $
          DESCRIPTION='Transparency of line', $
          VALID_RANGE=[0,100,5]

      ; Use TRANSPARENCY property instead.
      self->SetPropertyAttribute, 'ALPHA_CHANNEL', /HIDE

      self->RegisterProperty, 'INTERPOLATE', /BOOLEAN, $
          NAME='Interpolate', $
          DESCRIPTION='Interpolate'
      self->RegisterProperty, 'SNAP', /BOOLEAN, $
          NAME='Snap', $
          DESCRIPTION='Snap'
  endif
end


;----------------------------------------------------------------------------
pro IDLCrosshair::GetProperty, $
  INTERPOLATE=interpolate, $
  LOCATION=location, $
  SNAP=snap, $
  STYLE=style, $
  TRANSPARENCY=transparency, $
  _REF_EXTRA=_extra

    compile_opt idl2, hidden

  if (ARG_PRESENT(interpolate)) then $
      interpolate = self._interpolate

  if (ARG_PRESENT(location)) then $
      location = self._location

  if (ARG_PRESENT(snap)) then $
      snap = self._snap

  if (ARG_PRESENT(style)) then $
      style = self._style

  if ARG_PRESENT(transparency) then begin
      self._oLine->GetProperty, ALPHA_CHANNEL=alpha
      transparency = 0 > ROUND(100 - alpha*100) < 100
  endif

  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitManipulatorVisual::GetProperty, _EXTRA=_extra
end


;----------------------------------------------------------------------------
pro IDLCrosshair::SetProperty, $
  INTERPOLATE=interpolate, $
  LOCATION=location, $
  SNAP=snap, $
  STYLE=styleIn, $
  TRANSPARENCY=transparency, $
  _REF_EXTRA=_extra

  compile_opt idl2, hidden

  if (ISA(interpolate)) then $
    self._interpolate = KEYWORD_SET(interpolate)

  if (ISA(location)) then begin
    self._location[0:N_ELEMENTS(location)-1] = location
    ; Change from "None" to "Manual"
    if (self._style eq 0b) then self._style = 1b
    self->UpdateLocation
  endif

  if (ISA(snap)) then $
    self._snap = KEYWORD_SET(snap)

  if (ISA(styleIn)) then begin
    if ISA(styleIn, 'STRING') then begin
      case STRMID(STRUPCASE(styleIn),0,4) of
      'NONE': style = 0
      'MANU': style = 1
      'AUTO': style = 2
      endcase
    endif else style = styleIn
    oldstyle = self._style
    if (style ne oldstyle) then begin
      self._style = style
      ; If "None" then hide the crosshair.
      if (self._style eq 0) then begin
        self->IDLitManipulatorVisual::GetProperty, HIDE=hide
        if (~hide) then begin
          self->IDLitManipulatorVisual::SetProperty, /HIDE
          oTool = self->GetTool()
          if (OBJ_VALID(oTool)) then $
            oTool->RefreshCurrentWindow
        endif
      endif
    endif
  endif

  if (N_ELEMENTS(transparency)) then begin
      self._oLine->SetProperty, $
          ALPHA_CHANNEL=0 > ((100.-transparency)/100) < 1
  endif

  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitManipulatorVisual::SetProperty, _EXTRA=_extra

end


;----------------------------------------------------------------------------
pro IDLCrosshair::UpdateLocation

  compile_opt idl2, hidden

  xloc = self._location[0]
  yloc = self._location[1]
  
  oDS = self->GetDataspace(/UNNORM)
  if (~OBJ_VALID(oDS)) then return

  oDS->GetProperty, X_MINIMUM=xmin, X_MAXIMUM=xmax, $
    Y_MINIMUM=ymin, Y_MAXIMUM=ymax, Z_MINIMUM=zmin, Z_MAXIMUM=zmax, $
    XLOG=xLog, YLOG=yLog, ZLOG=zlog

  if (xLog) then begin
    xloc = ALOG10(xloc)
    xmin = ALOG10(xmin)
    xmax = ALOG10(xmax)
  endif

  if (yLog) then begin
    yloc = ALOG10(yloc)
    ymin = ALOG10(ymin)
    ymax = ALOG10(ymax)
  endif

  xloc = (xmin le xmax) ? xmin > xloc < xmax : xmax > xloc < xmin
  yloc = (ymin le ymax) ? ymin > yloc < ymax : ymax > yloc < ymin

  if (oDS->Is3D()) then begin    ; 3D dataspace

    zloc = self._location[2]
    if (zLog) then begin
      zloc = ALOG10(zloc)
      zmin = ALOG10(zmin)
      zmax = ALOG10(zmax)
    endif
    zloc = (zmin le zmax) ? zmin > zloc < zmax : zmax > zloc < zmin

    data = [ $
      [xmin,yloc,zloc], [xmax,yloc,zloc], $
      [xloc,ymin,zloc], [xloc,ymax,zloc], $
      [xloc,yloc,zmin], [xloc,yloc,zmax] ]
    polylines = [2,0,1,2,2,3,2,4,5]

  endif else begin    ; 2D dataspace

    data = [ $
      [xmin,yloc], [xmax,yloc], $
      [xloc,ymin], [xloc,ymax] ]
    polylines = [2,0,1,2,2,3]

  endelse

  self._oLine->SetProperty, $
    DATA=data, POLYLINES=polylines
  void=check_math()
  self->IDLitManipulatorVisual::SetProperty, HIDE=0

  oTool = self->GetTool()
  if (OBJ_VALID(oTool)) then $
    oTool->RefreshCurrentWindow
end


;----------------------------------------------------------------------------
pro IDLCrosshair__Define

  compile_opt idl2, hidden

  struct = { IDLCrosshair,           $
      inherits IDLitManipulatorVisual, $
      _oLine: OBJ_NEW(), $
      _interpolate: 0b, $
      _location: [0d, 0d, 0d], $
      _snap: 0b, $
      _style: 0b $
  }
end
