; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/idlwritesvg__define.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.

;----------------------------------------------------------------------------
; Purpose:
;   This file implements the IDLWriteSVG class.
;

;---------------------------------------------------------------------------
; Lifecycle Routines
;---------------------------------------------------------------------------
; Purpose:
;   The constructor of the object.
;
; Arguments:
;   None.
;
; Keywords:
;   All superclass keywords.
;
function IDLWriteSVG::Init, $
  _EXTRA=_extra
  
  
  compile_opt idl2, hidden
  
  ; Init superclass
  ; The only properties that can be set at INIT time can be set
  ; in the superclass Init method.
  if (~self->IDLitWriter::Init('svg', $
    TYPES="IDLDEST", $
    NAME='Scalable Vector Graphics', $
    DESCRIPTION="Scalable Vector Graphics", $
    ICON='demo', $
    _EXTRA=_extra)) then $
    return, 0

  ; Default is "Vector" format
  self._graphicsFormat = 1  
  
  
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLWriteSVG::SetProperty, _EXTRA=_extra
    
  return, 1
end


;---------------------------------------------------------------------------
; Purpose:
; The destructor for the class.
;
; Arguments:
;   None.
;
; Keywords:
;   None.
;
pro IDLWriteSVG::Cleanup
  compile_opt idl2, hidden
  
  OBJ_DESTROY, self._ids
  self->IDLitWriter::Cleanup
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::GetProperty, $
  RESOLUTION=resolution, $
  _REF_EXTRA=_extra
  
  compile_opt idl2, hidden

  if (ARG_PRESENT(resolution) eq 1) then $
    resolution = self._resolution
    
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitWriter::GetProperty, _EXTRA=_extra
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetProperty, $
  RESOLUTION=resolution, $
  _EXTRA=_extra
  
  compile_opt idl2, hidden
  
  if (N_ELEMENTS(resolution) eq 1) then $
    self._resolution = resolution
    
  if (N_ELEMENTS(_extra) gt 0) then $
    self->IDLitWriter::SetProperty, _EXTRA=_extra
end


;---------------------------------------------------------------------------
function IDLWriteSVG::_GetID, arg, index
  compile_opt idl2, hidden
  
  if (ISA(arg, 'IDLitComponent')) then begin
    arg->IDLitComponent::GetProperty, NAME=name
    if (name eq '') then name = OBJ_CLASS(arg)
  endif else if (ISA(arg, 'OBJREF')) then begin
    name = OBJ_CLASS(arg)
  endif else if (ISA(arg, 'STRING')) then begin
    name = arg
  endif
  
  id = IDL_VALIDNAME(name, /CONVERT_SPACES)
  
  if (ISA(index)) then $
    id += '_' + STRTRIM(index, 2)
    
  if (self._ids.HasKey(id)) then begin
    count = self._ids[id] + 1
    self._ids[id] = count
    id += '_' + STRTRIM(count, 2)
  endif else begin
    self._ids[id] = 0L
  endelse
  
  return, id
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::_ConvertCoords, oVis, points
  compile_opt idl2, hidden
  
  oVis->VisToWindow, 0d, 0d, x0, y0
  oVis->VisToWindow, 1d, 0d, x1, void
  oVis->VisToWindow, 0d, 1d, void, y1
  x0 = x0[0]
  x1 = x1[0]
  y0 = y0[0]
  y1 = y1[0]
  dx = x1 - x0
  dy = y1 - y0
  points[0,*] = points[0,*]*dx + x0
  ; SVG has 0,0 in the upper left corner, so flip the Y coords.
  points[1,*] = self._dims[1] - (points[1,*]*dy + y0)
  points[WHERE(~FINITE(points), /NULL)] = 0
end


;---------------------------------------------------------------------------
function IDLWriteSVG::_FillStyle, colorIn, transparency
  compile_opt idl2, hidden
  
  color = STRING(colorIn, FORMAT='("fill:rgb(",I0,",",I0,",",I0,");")')
  opacity = 0 > ((100.-transparency)/100) < 1
  opacity = (opacity eq 1) ? '' : $
    'fill-opacity:'+STRING(opacity, FORMAT='(F4.2)')+';'
  style = color + opacity
  
  return, style
end


;---------------------------------------------------------------------------
function IDLWriteSVG::_StrokeStyle, $
  colorIn, transparency, linestyleIn, thickIn, $
  NO_LINECAP=noLinecap
  
  compile_opt idl2, hidden
  
  if (linestyleIn eq 6) then return, ''
  
  color = STRING(colorIn, FORMAT='("stroke:rgb(",I0,",",I0,",",I0,");")')
  
  opacity = 0 > ((100.-transparency)/100) < 1
  opacity = (opacity eq 1) ? '' : $
    'stroke-opacity:'+STRING(opacity, FORMAT='(F4.2)')+';'
  
  ; Note: Need to provide the dasharray even for solid lines, in case
  ; my parent element has a different linestyle.
  case (linestyleIn) of
    1: linestyle = 'stroke-dasharray:2,6;'
    2: linestyle = 'stroke-dasharray:6,6;'
    3: linestyle = 'stroke-dasharray:6,3,1,3;'
    4: linestyle = 'stroke-dasharray:8,3,1,3,1,3,1,3;'
    5: linestyle = 'stroke-dasharray:8,4;'
    6: linestyle = 'stroke-dasharray:0;'
    else: linestyle = 'stroke-dasharray:2000,1;'
  endcase
  
  thick = (thickIn eq LONG(thickIn)) ? LONG(thickIn) : thickIn
  
  style = color + opacity + linestyle + $
    'stroke-width:' + STRTRIM(thick,2) + ';'
    
  ; We want to use square linecaps to match how IDL draws thick lines.
  ; SVG is somewhat annoying in that it applies the linecaps to the dashes,
  ; so that the gaps between dashes gets smaller as the line gets thicker.
  ; An easy (but not quite correct) workaround is to only apply the square
  ; linecaps to solid lines.
  if (linestyleIn lt 1 && ~KEYWORD_SET(noLinecap)) then $
    style = 'stroke-linecap:square;' + style
  
  return, style
end


;---------------------------------------------------------------------------
function IDLWriteSVG::_TextStyle, colorIn, transparency, $
  horzAlign, vertAlign, fontname, fontsize, fontstyle
  
  compile_opt idl2, hidden
  
  fillstyle = self->_FillStyle(colorIn, transparency)
  
  align = ''
  if (horzAlign ge 0.25 && horzAlign lt 0.75) then begin
    align = 'text-anchor:middle;'
  endif else if (horzAlign ge 0.75) then begin
    align = 'text-anchor:end;'
  endif
  
  if (vertAlign ge 0.25 && vertAlign lt 0.75) then begin
    align += 'alignment-baseline:central;'
  endif else if (vertAlign ge 0.75) then begin
    align += 'alignment-baseline:hanging;'
  endif
  
  font = 'font-family:' + fontName + ';' + $
  'font-size:' + STRTRIM(FIX(fontSize),2) + 'pt;'
  case(fontStyle) of
    1: font += 'font-weight:bold;'
    2: font += 'font-style:italic;'
    3: font += 'font-style:italic;font-weight:bold;'
    else:
  endcase
  
  style = font + fillstyle + align
  
  return, style
  
  ; Dead code: If we need to compute a general transform from the text
  ; location, baseline & updir.
  transform = FLTARR(3,3)
  transform = [$
    [1, 0, -location[0]], $
    [0, 1, -location[1]], $
    [0, 0, 1]]
  transform #= [$
    [baseline[0:1], 0], $
    [updir[0:1], 0], $
    [0, 0, 1]]
  transform #= [$
    [1, 0, location[0]], $
    [0, 1, location[1]], $
    [0, 0, 1]]
  transform = TRANSPOSE(transform[*,0:1])
  transform = STRTRIM(STRING(transform, FORMAT='(F16.3)'), 2)
  transform = ' transform="matrix(' + STRJOIN(transform,',') + ')"'
  
end


;---------------------------------------------------------------------------
; Greek letters
;
function IDLWriteSVG::_TextParse, strIn

  compile_opt idl2, hidden
  
  str = Tex2IDL(strIn)
  
  symbolChars = TEX2IDL_UNICODETABLE()
  symChars = FIX(REFORM(symbolChars[0,*]))
  
  ; Replace reserved HTML characters with their entities.
  str = STRJOIN(STRTOK(str, '&', /EXTRACT, /PRESERVE), '&amp;')
  str = STRJOIN(STRTOK(str, '<', /EXTRACT, /PRESERVE), '&lt;')
  str = STRJOIN(STRTOK(str, '>', /EXTRACT, /PRESERVE), '&gt;')
  
  ; Replace all !Z unicode characters right away, since they are
  ; messy to parse.
  while (1) do begin
    pos = STREGEX(str, '!Z\([0-9A-F,]*\)', LENGTH=len, /FOLD_CASE)
    if (pos eq -1) then break
    str1 = STRMID(str, 0, pos)
    str2 = STRMID(str, pos + len)
    unicode = STRTOK(STRMID(str, pos+3, len-4), ',', /EXTRACT)
    unicode = STRJOIN('&#x' + unicode + ';')
    str = str1 + unicode + str2
  endwhile
  
  c = BYTE(str)
  result = ''
  bang = BYTE('!')
  nm1 = N_ELEMENTS(c) - 1
  withinTspan = 0
  prevCommand = ''
  prevFont = ''
  prevStyle = ''
  font = ''
  offset = [0.0, 0]
  withinMajor = 0
  
  for i=0,nm1 do begin
    char = ''
    command = ''
    if (c[i] eq bang && i lt nm1) then begin
      ; Swallow the next character
      i++
      command = STRUPCASE(STRING(c[i]))
      ; Handle the !11-!20 commands that are 2 characters
      if (command eq '1' && i lt nm1) then begin
        nextchar = c[i+1]
        if (nextchar ge BYTE('0') && nextchar le BYTE('9')) then begin
          command += STRING(nextchar)
          i++
        endif
      endif
      if (command eq '2' && i lt nm1 && c[i+1] eq '0') then begin
        command += '0'
        i++
      endif
    endif
    
    revert = 0b
    
    case (command) of
      '3': font = 'font-family:sans-serif;'
      '4': font = 'font-family:sans-serif;font-weight:bold;'
      '5': font = 'font-family:sans-serif;font-style:italic;'
      '6': font = 'font-family:sans-serif;font-weight:bold;font-style:italic;'
      '7': font = 'font-family:serif;'
      '8': font = 'font-family:serif;font-style:italic;'
      '9': font = 'symbol'
      'M': font = 'symbol'
      '10': font = '' ; don't switch to DejaVu, assume built-in Unicode support
      '11': font = 'font-family:monospace;'
      'G': font = 'font-family:monospace;'
      '12': font = 'font-family:monospace;font-style:italic;'
      'W': font = 'font-family:monospace;font-style:italic;'
      '13': font = 'font-family:monospace;font-weight:bold;'
      '14': font = 'font-family:monospace;font-weight:bold;font-style:italic;'
      '15': font = 'font-family:serif;font-weight:bold;'
      '16': font = 'font-family:serif;font-weight:bold;font-style:italic;'
      '17': font = 'font-family:sans-serif;'
      '18': font = 'font-family:sans-serif;'
      '19': font = 'font-family:sans-serif;'
      '20': font = 'font-family:sans-serif;'
      'X': font = ''
      'D': begin
        withinMajor = -1
        offset = [-0.5, 62]  ; em, %
      end
      'U': begin
        withinMajor = 1
        offset = [0.81, 62]
      end
      'I': begin
        if (withinMajor) then begin
          offset = (withinMajor eq -1) ? [-1.35, 27] : [1.85, 27]
        endif else begin
          offset = [-0.5, 44]
        endelse
      end
      'E': begin
        if (withinMajor) then begin
          offset = (withinMajor eq -1) ? [0., 27] : [3.4, 27]
        endif else begin
          offset = [1.14, 44]
        endelse
      end
      'N': begin
        withinMajor = 0
        offset = [0, 0]
      end
      'L': offset = [-0.85, 62]
      'A': offset = [0.8, 0]
      'B': offset = [-0.8, 0]
      'C':  ;TODO handle newline
      'R':  ;TODO handle restore
      'S':  ;TODO handle save
      else: begin
        char = STRING(c[i])
        ; Revert back from math font to original font
        if (prevCommand eq 'M') then font = prevFont
      end
    endcase
    
    stroffset = ''
    if (offset[0] ne 0) then begin
      stroffset = 'baseline-shift:' + $
        STRING(offset[0], FORMAT='(F5.2)') + 'em;'
    endif
    if (offset[1] ne 0) then begin
      stroffset += 'font-size:' + STRING(offset[1], FORMAT='(I2)') + '%;'
    endif
    
    if (char ne '') then begin
      isMathChar = font eq 'symbol' || prevCommand eq 'M'
      if (isMathChar) then begin
        fontname = 'font-family:serif;'
        ch = (BYTE(char))[0]
        ; IDL Symbol Font quirk: Lowercase Greek letters are italic
        if (ch ge 97 && ch le 122) then fontname += 'font-style:italic;'
        match = WHERE(symChars eq ch, /NULL)
        if (ISA(match)) then begin
          char = symbolChars[1, match[0]]
          if (STRMID(char,0,1) ne '&') then char = '&#x' + char + ';'
        endif
      endif else begin
        fontname = font
      endelse
      
      style = fontname + stroffset
      
      ; If possible, insert the new character into the same tspan as the
      ; previous character.
      if (prevStyle eq style && prevStyle ne '') then begin
        result = STRMID(result, 0, STRLEN(result)-8)
      endif else begin
        prevStyle = style
        if (style ne '') then begin
          result += '<tspan style="' + style + '">'
        endif
      endelse
      
      result += char
      if (style ne '') then $
        result += '</tspan>'
    endif
    
    prevCommand = command
    if (command ne 'M') then prevFont = font
  endfor
  
  ; Replace every 2 spaces with a space + non-breaking space
  result = STRTOK(result, ' {2}', /EXTRACT, /PRESERVE, /REGEX)
  result = STRJOIN(result, ' &#160;')
  
  return, result
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::_CollapsePolygons, points, conn

  compile_opt idl2, hidden
  
  connIndex = 0L
  accumIndex = -1
  ;  useless = 0L
  ;  collapsed = 0L
  ;  ntotal = 0L
  prevPoly = !NULL
  
  while connIndex lt N_ELEMENTS(conn) do begin
    npoly = conn[connIndex]
    if (npoly eq -1) then break
    if (npoly eq 0) then begin
      connIndex++
      accumIndex = -1
      prevPoly = !NULL
      continue
    endif
    conn1 = conn[connIndex + 1:connIndex + npoly]
    cIndex = connIndex
    connIndex += npoly + 1
    ppoints = points[*,conn1]
    minn = MIN(ppoints, DIM=2, MAX=maxx)
    ;    ntotal += npoly
    
    ; Polygon is off the page?
    if (maxx[0] lt 0 || maxx[1] lt 0 || $
      minn[0] gt self._dims[0] || minn[1] gt self._dims[1]) then begin
      conn[cIndex:cIndex + npoly] = 0
      accumIndex = -1
      prevPoly = !NULL
      continue
    endif
    
    ; Ignore polygons that have 4 or more vertices
    if (npoly ne 3) then begin
      accumIndex = -1
      prevPoly = !NULL
      continue
    endif
    
    ; Polygon is just a useless straight line?
    if (ARRAY_EQUAL(ppoints[*,0], ppoints[*,2])) then begin
      conn[cIndex:cIndex + npoly] = 0
      accumIndex = -1
      prevPoly = !NULL
      ;      useless++
      continue
    endif
    
    ; Isocontour produces a triangular mesh that marches from left-to-right.
    ; Imagine a set of vertices in 2d labelled like:
    ;     a  d  f  h
    ;  b  c  e  g  i
    ; The triangles from Isocontour look like:
    ;  3,a,b,c, 3,a,c,d, 3,d,c,e, 3,d,e,f, 3,f,e,g, 3,f,g,h, 3,h,g,i
    ; We want to collapse these down to:
    ;  9,a,b,c,e,g,i,h,f,d
    ;
    ; In the SVG file this will reduce the output from 21 #'s down to 9 #'s.
    ; As the row gets longer the savings will be greater.
    ;
    ; accumIndex: The index within the connectivity array where the # of
    ; vertices will be accumulated.
    ; sameFirst: finds a,b,c followed by a,c,d
    ; sameSecond: finds a,c,d followed by d,c,e
    ;
    sameFirst = ISA(prevPoly) && $
      ARRAY_EQUAL(prevPoly[*,0], ppoints[*,0]) && $
      ARRAY_EQUAL(prevPoly[*,2], ppoints[*,1])
    sameSecond = ISA(prevPoly) && $
      ARRAY_EQUAL(prevPoly[*,2], ppoints[*,0]) && $
      ARRAY_EQUAL(prevPoly[*,1], ppoints[*,1])
      
    ; If both are 0 (or both are 1) then the triangles are unrelated.
    if (sameFirst eq sameSecond) then begin
      accumIndex = -1
      prevPoly = ppoints
      continue
    endif
    
    if (accumIndex ne -1) then begin
      ; Sanity check: If the sameFirst/sameSecond don't swap values,
      ; then we must have hit some strange triangle. Stop accumulating.
      if (wasFirst eq sameFirst || wasSecond eq sameSecond) then begin
        accumIndex = -1
        prevPoly = ppoints
        continue
      endif
    endif
    
    wasFirst = sameFirst
    wasSecond = sameSecond
    
    ; Keep the last vertex of the triangle.
    newPointToInsert = conn[cIndex + 3]
    ; Get rid of the other 2 vertices.
    conn[cIndex:cIndex + 3] = 0
    
    if (accumIndex eq -1) then begin
      nCurrentLine = 3
      accumIndex = cIndex - 4
      isFirst = sameFirst
    endif
    nCurrentLine++
    conn[accumIndex] = nCurrentLine
    ; Move half the indices to the end, and insert the new point.
    ; The starting index depends upon whether we are starting with
    ; "abc" or "acd" as our first triangle.
    if (isFirst) then begin
      indx1 = accumIndex + (nCurrentLine + 4)/2
      indx2 = indx1 + (nCurrentLine - 5)/2
    endif else begin
      ; Move half the indices to the end, and insert the new point.
      indx1 = accumIndex + (nCurrentLine + 3)/2
      indx2 = indx1 + (nCurrentLine - 4)/2
    endelse
    lastPoints = conn[indx1:indx2]
    conn[indx1] = newPointToInsert
    conn[indx1 + 1] = lastPoints
    
    prevPoly = ppoints
    ;    collapsed++
  endwhile
  
  ; Debugging
  ;  print, useless, collapsed, ntotal
  
end


;---------------------------------------------------------------------------
function IDLWriteSVG::_CreateMarker, oSymbol, symSize, color, $
  ORIENT_AUTO=orientAuto

  compile_opt idl2, hidden
  
  oSymbol->GetProperty, DATA=symbol
  if (~ISA(symbol, /NUMBER) || symbol eq 0) then return, ''
  
  oSymbol->GetProperty, ALPHA=symAlpha, COLOR=symColor, $
    FILL_COLOR=symFillColor, FILLED=symFilled, THICK=symThick, $
    VERTICES=vertices, CONNECTIVITY=connectivity

  markerID = self->_GetID(oSymbol)
  symTransparency = 100.0 - symAlpha * 100.0
  
  if (N_ELEMENTS(symColor) le 1) then symColor = color
  if (N_ELEMENTS(symFillColor) le 1) then symFillColor = color

  strokeStyle = self->_StrokeStyle(symColor, symTransparency, 0, symThick, $
    /NO_LINECAP)
  fillStyle = symFilled ? self->_FillStyle(symFillColor, symTransparency) : $
    'fill:none;'
  style = strokeStyle + fillStyle
  
  
  ; SVG y-coordinate is flipped
  vertices[1,*] = -vertices[1,*]
  
  vertices = vertices*5*symSize[0]
  
  mx = MAX(vertices, DIM=2, MIN=mn)
  width = (mx[0] - mn[0]) > 2
  height = (mx[1] - mn[1]) > 2
  ; Shift the reference point up so that the symbol is centered
  ; and vertices don't get clipped.
  refX = (width gt 2) ? -mn[0] : 1
  refY = (height gt 2) ? -mn[1] : 1
  vertices = 0.8*vertices[0:1,*]
  vertices[0,*] += refX
  vertices[1,*] += refY
  orient = KEYWORD_SET(orientAuto) ? ' orient="auto"' : ''

  PRINTF, self._lun, '<defs>
  PRINTF, self._lun, '<marker id="' + markerID + '"' + $
    ' markerUnits="userSpaceOnUse"' + orient + $
    ' markerWidth="' + STRTRIM(STRING(width, FORMAT='(F16.1)'),2) + '"' + $
    ' markerHeight="' + STRTRIM(STRING(height, FORMAT='(F16.1)'),2) + '"' + $
    ' refX="' + STRTRIM(STRING(refX, FORMAT='(F16.1)'), 2) + '"' + $
    ' refY="' + STRTRIM(STRING(refY, FORMAT='(F16.1)'), 2) + '">'
    
  index = 0L
  nconn = N_ELEMENTS(connectivity)
  while (index lt nconn) do begin
    polypoints = connectivity[index]
    if (polypoints eq 0) then begin
      index++
      continue
    endif
    if (polypoints eq -1) then $
      break
      
    conn1 = connectivity[index+1:index+polypoints]
    index += polypoints + 1
    points1 = vertices[*, conn1]
    s = STRCOMPRESS(STRING(points1, $
      FORMAT='(F16.3,",",F16.3)'), /REMOVE_ALL)
    PRINTF, self._lun, '<polygon style="' + style + '"'
    PRINTF, self._lun, '  points="' + STRJOIN(s, ' ') + '" />'
  endwhile
  
  PRINTF, self._lun, '</marker>
  PRINTF, self._lun, '</defs>
  
  return, markerID
end


;---------------------------------------------------------------------------
function IDLWriteSVG::_GetTransform, oVis, TRANSFORM=origTransform
  compile_opt idl2, hidden

  transform = ''
  oVis->GetProperty, TRANSFORM=origTransform, CENTER_OF_ROTATION=centerRotation
  unitVector = origTransform ## [1,0,0,0]
  angle = ATAN(-unitVector[1], unitVector[0])*!RADEG
  if (ABS(angle) ge 1e-2) then begin
    centerRotation = centerRotation[0:1]
    self->_ConvertCoords, oVis, centerRotation
    oVis->Rotate, [0, 0, 1], angle
    rotation = STRTRIM(STRING([angle,centerRotation], FORMAT='(F16.3)'),2)
    transform = ' transform="rotate(' + STRJOIN(rotation,',') + ')"'
  endif
  return, transform
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetAxes, oAxes

  compile_opt idl2, hidden
  
  axes = oAxes->Get(/ALL, ISA='IDLitVisAxis');oDataspace->GetAxes(COUNT=nAxes)
  outputGroup = 0
  foreach ax, axes do begin
    ax->GetProperty, HIDE=ahide
    if ~ahide then outputGroup++
  endforeach
  if (outputGroup ge 2) then begin
    id = self->_GetID(oAxes)
    PRINTF, self._lun, '<g id="' + id + '">'
  endif
  foreach ax, axes do begin
    self->SetAxis, ax
  endforeach
  if (outputGroup ge 2) then $
    PRINTF, self._lun, '</g>'
    
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetAxis, oAxis

  compile_opt idl2, hidden
  
  oAxis->GetProperty, HIDE=hide, $
    AXIS_TITLE=axisTitle, $
    COLOR=color, $
    DIRECTION=direction, $
    FONT_NAME=fontName, $
    FONT_SIZE=fontSize, $
    FONT_STYLE=fontStyle, $
    GRIDSTYLE=gridstyle, $
    LOCATION=location, $
    MAJOR=major, $
    RANGE=range, $
    SHOWTEXT=showText, $
    SUBGRIDSTYLE=subgridstyle, $
    SUBTICKLEN=subticklen, $
    TEXT_COLOR=textColor, $
    TEXT_ORIENTATION=textOrientation, $
    TEXTPOS=textpos, $
    THICK=thick, $
    TICKLEN=ticklen, $
    TICKDIR=tickdir, $
    TICKNAME=tickName, $
    TICKVALUES=tickValues, $
    TRANSPARENCY=transparency, $
    XCOORD_CONV=xcoordconv,YCOORD_CONV=ycoordconv
    
  ; TODO: Take into account TRANSFORM matrix?
  
  if (hide) then return
  
  if (direction eq 2) then return
  
  oDataSpace = oAxis->GetDataSpace(/UNNORMALIZED)
  if (~OBJ_VALID(oDataSpace)) then return
  oDataSpace->GetProperty, XRANGE=xrange, YRANGE=yrange, $
    XLOG=xLog, YLOG=yLog, ZLOG=zLog, $
    XREVERSE=xreverse, YREVERSE=yreverse, ZREVERSE=zreverse
    
  if (xrange[0] eq xrange[1]) then xrange = [-1,1]
  if (yrange[0] eq yrange[1]) then yrange = [-1,1]
  
  oAxis._oAxis->GetProperty, MINOR=minor
  
  
  oAxis->VisToWindow, 0, 0, x0, y0
  oAxis->VisToWindow, 1, 0, x1, void
  oAxis->VisToWindow, 0, 1, void, y1
  x0 = x0[0]
  x1 = x1[0]
  y0 = y0[0]
  y1 = y1[0]
  dx = x1 - x0
  dy = y1 - y0
  
  if (tickdir eq 2) then tickdir = 0
  if (tickdir) then ticklen = -ticklen
  
  nticks = N_ELEMENTS(tickValues)
  
  if (direction eq 0) then begin
    if (~ARRAY_EQUAL(xcoordconv, [0, 1])) then begin
      range = xcoordconv[0] + xcoordconv[1]*range
      tickValues = xcoordconv[0] + xcoordconv[1]*tickValues
    endif
    if (~ARRAY_EQUAL(ycoordconv, [0, 1])) then begin
      location[1] = ycoordconv[0] + ycoordconv[1]*location[1]
      yrange = ycoordconv[0] + ycoordconv[1]*yrange
      ;      ticklen *= ycoordconv[1]
    endif
    xrangePixels = range*dx + x0
    xloctitle = MEAN(xrangePixels)
    ; SVG has 0,0 in the upper left corner, so flip the Y coords.
    y = self._dims[1] - (location[1]*dy + y0)
    points = [[xrangePixels[0], y], [xrangePixels[1], y]]
    y = REPLICATE(location[1], nticks)
    oAxis->VisToWindow, tickValues, y, xloc, yloc
    dy = yrange[1] - yrange[0]
    if (yreverse) then dy = -dy
    y = REPLICATE(location[1] + ticklen*dy, nticks)
    oAxis->VisToWindow, tickValues, y, xend, yend
  endif else begin
    if (~ARRAY_EQUAL(ycoordconv, [0, 1])) then begin
      range = ycoordconv[0] + ycoordconv[1]*range
      tickValues = ycoordconv[0] + ycoordconv[1]*tickValues
    endif
    if (~ARRAY_EQUAL(xcoordconv, [0, 1])) then begin
      location[0] = xcoordconv[0] + xcoordconv[1]*location[0]
      xrange = xcoordconv[0] + xcoordconv[1]*xrange
      ;      ticklen *= xcoordconv[1]
    endif
    x = location[0]*dx + x0
    ; SVG has 0,0 in the upper left corner, so flip the Y coords.
    yrangePixels = self._dims[1] - (range*dy + y0)
    yloctitle = MEAN(yrangePixels)
    points = [[x, yrangePixels[0]], [x, yrangePixels[1]]]
    x = REPLICATE(location[0], nticks)
    oAxis->VisToWindow, x, tickValues, xloc, yloc
    dx = xrange[1] - xrange[0]
    if (xreverse) then dx = -dx
    x = REPLICATE(location[0] + ticklen*dx, nticks)
    oAxis->VisToWindow, x, tickValues, xend, yend
  endelse
  
  
  ; SVG has 0,0 in the upper left corner, so flip the Y coords.
  yloc = self._dims[1] - yloc
  yend = self._dims[1] - yend
  
  xsubend = xloc + subticklen*(xend - xloc)
  ysubend = yloc + subticklen*(yend - yloc)
  
  
  ; Draw the axis baseline
  id = self->_GetID(oAxis)
  PRINTF, self._lun, '<g id="' + id + '">'
  style = self->_StrokeStyle(color, transparency, 0, thick)
  PRINTF, self._lun, '<polyline style="' + style + '"'
  s = STRCOMPRESS(STRING(points, FORMAT='(F16.1,",",F16.1)'), /REMOVE_ALL)
  PRINTF, self._lun, '  points="' + STRJOIN(s, ' ') + '" />'
  
  ; Major and minor ticks
  style = self->_StrokeStyle(color, transparency, gridstyle, thick)
  substyle = self->_StrokeStyle(color, transparency, subgridstyle, thick)
  for i=-1,nticks-1 do begin
    if (i ge 0) then begin
      PRINTF, self._lun, '<polyline style="' + style + '"'
      points = [[xloc[i], yloc[i]], [xend[i], yend[i]]]
      s = STRCOMPRESS(STRING(points, FORMAT='(F16.1,",",F16.1)'), /REMOVE_ALL)
      PRINTF, self._lun, '  points="' + STRJOIN(s, ' ') + '" />'
    endif
    
    path = ''
    for j=0,minor-1 do begin
      points = [[xloc[i>0], yloc[i>0]], [xsubend[i>0], ysubend[i>0]]]
      if (direction eq 0) then begin
        dx = (xloc[1] - xloc[0])/(minor + 1)
        if (i eq -1) then dx = -dx
        points[0,*] += (j + 1)*dx
        if (points[0,0] ge MAX(xrangePixels) || $
          points[0,0] le MIN(xrangePixels)) then break
      endif else begin
        dy = (yloc[1] - yloc[0])/(minor + 1)
        if (i eq -1) then dy = -dy
        points[1,*] += (j + 1)*dy
        if (points[1,0] le MIN(yrangePixels) || $
          points[1,0] ge MAX(yrangePixels)) then break
      endelse
      s = STRCOMPRESS(STRING(points, FORMAT='(F16.1,",",F16.1)'), /REMOVE_ALL)
      path += ' M ' + s[0] + ' L ' + s[1]
    endfor
    if (path ne '') then begin
      PRINTF, self._lun, '<path style="' + substyle + '"' + $
        ' d="' + path + '" />'
    endif
  endfor
  
  
  if (showText) then begin
    if (direction eq 0) then begin
      horzAlign = 0.5
      vertAlign = 0
      dx = ''
      dy = textpos ? ' dy="-0.3em"' : ' dy="1.1em"'
    endif else begin
      horzAlign = textpos ? 0 : 1
      vertAlign = 0.5
      dx = textpos ? ' dx="0.3em"' : ' dx="-0.3em"'
      dy = ''
    endelse
    style = self->_TextStyle(textColor, transparency, $
      horzAlign, vertAlign, fontname, fontsize, fontstyle)
    for i=0,nticks-1 do begin
      if (direction eq 0) then begin
        x = xloc[i]
        y = (textpos eq tickdir) ? yloc[i] : yend[i]
      endif else begin
        x = (textpos eq tickdir) ? xloc[i] : xend[i]
        y = yloc[i]
      endelse
      x = STRTRIM(STRING(x, FORMAT='(F16.1)'), 2)
      y = STRTRIM(STRING(y, FORMAT='(F16.1)'), 2)
      PRINTF, self._lun, '<text' + $
        ' style="' + style + '"' + $
        ' x="' + x + '"' + dx + $
        ' y="' + y + '"' + dy + $
        '>' + self->_TextParse(tickName[i]) + '</text>'
    endfor
    
    if  (ISA(axisTitle) && axisTitle ne '') then begin
      substrings = STRTOK(axisTitle, '!C', /REGEX, /FOLD_CASE, $
        /EXTRACT, /PRESERVE_NULL)
      n = N_ELEMENTS(substrings)
      offset = (direction eq 0) ? $
        (textpos ? -1.5 - (n-1) : 2.25) : (textpos ? 0 : -(n-1))
      foreach str, substrings do begin
        if (direction eq 0) then begin
          x = xloctitle
          y = (textpos eq tickdir) ? yloc[0] : yend[0]
          dx = ''
          dy = ' dy="' + STRTRIM(STRING(offset, FORMAT='(f16.2)'),2) + 'em"'
          rotate = ''
          x = STRTRIM(STRING(x, FORMAT='(F16.1)'), 2)
          y = STRTRIM(STRING(y, FORMAT='(F16.1)'), 2)
        endif else begin
          horzAlign = 0.5
          style = self->_TextStyle(textColor, transparency, $
            horzAlign, 0, fontname, fontsize, fontstyle)
          x = (textpos eq tickdir) ? xloc[0] : xend[0]
          y = yloctitle
          x = STRTRIM(STRING(x, FORMAT='(F16.1)'), 2)
          y = STRTRIM(STRING(y, FORMAT='(F16.1)'), 2)
          dx = ''
          ; For the Y-axis, since the tick labels are horizontal, make sure
          ; to bump out the axis title by the length of the tick labels.
          delta = 1 + 0.5*MAX(STRLEN(tickName))
          if (~textpos) then delta = -delta
          dy = delta + offset
          dy = ' dy="' + STRTRIM(STRING(dy, FORMAT='(F16.1)'), 2) + 'em"'
          rotate = ' transform="rotate(-90,' + x + ',' + y + ')"'
        endelse
        PRINTF, self._lun, '<text' + $
          ' style="' + style + '"' + $
          ' x="' + x + '"' + $
          ' y="' + y + '"' + $
          rotate + $
          '><tspan' + dx + dy + '>'
        PRINTF, self._lun, self->_TextParse(str)
        PRINTF, self._lun, '</tspan></text>'
        offset++
      endforeach
    endif
    
  endif
  
  PRINTF, self._lun, '</g>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetArrow, oVis
  compile_opt idl2, hidden
  
  id = self->_GetID(oVis)
  PRINTF, self._lun, '<g id="' + id + '">'
  
  self->SetPolygon, oVis
  self->SetPolyline, oVis
  
  PRINTF, self._lun, '</g>'
  
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetBubbleplot, oVis, clipID

  compile_opt idl2, hidden
  
  oVis->GetProperty, CLIP=clip, FILLED=filled, SHADED=shaded
  id = ' id="' + self->_GetID(oVis) + '"'
  PRINTF, self._lun, '<g' + id + (clip && ISA(clipID) ? clipID : '') + '>'
  
  if (filled && shaded) then begin
    oVis->SetProperty, SHADED=0
  endif

  oFill = oVis->Get(/ALL, ISA='IDLgrPolygon', COUNT=c)
  oLine = oVis->Get(/ALL, ISA='IDLgrPolyline')
  oText = oVis->Get(/ALL, ISA='IDLgrText')
  for i=0,c-1 do begin
    self->SetPolygon, oFill[i], VIS=oVis
    self->SetPolyline, oLine[i], VIS=oVis
    self->SetText, oText[i], VIS=oVis
  endfor

  if (filled && shaded) then begin
    oVis->SetProperty, SHADED=1
  endif
  
  PRINTF, self._lun, '</g>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetColorbar, oVis
  compile_opt idl2, hidden
  
  id = self->_GetID(oVis)
  PRINTF, self._lun, '<g id="' + id + '">'
  
  self->SetPolygon, oVis
  self->SetPolyline, oVis
  
  oAxis = (oVis->Get(/ALL, ISA='IDLitVisAxis', COUNT=c))[0]
  if OBJ_VALID(oAxis) then $
    self->SetAxis, oAxis
    
  PRINTF, self._lun, '</g>'
  
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetContour, oVis, clipID
  compile_opt idl2, hidden
  
  oVis->GetData, contour_data, xparm, yparm
  
  if (N_ELEMENTS(contour_data) le 1) then return
  
  oDataSpace = oVis->GetDataSpace(/UNNORMALIZED)
  if (~OBJ_VALID(oDataSpace)) then return
  oDataSpace->GetProperty, XRANGE=xrange, YRANGE=yrange, $
    XLOG=xLog, YLOG=yLog, ZLOG=zLog, $
    XREVERSE=xreverse, YREVERSE=yreverse, ZREVERSE=zreverse
    
  oVis->GetProperty, $
    CLIP=clip, $
    FILL=fill, $
    FONT_NAME=fontName, $
    FONT_SIZE=fontSize, $
    FONT_STYLE=fontStyle, $
    GRID_UNITS=gridUnits, $
    LABEL_COLOR=labelColor, $
    PLANAR=planar, $
    RGB_TABLE=rgbTable, $
    TRANSPARENCY=transparency
    
  oVis._oContour->GetProperty, $
    C_COLOR=c_color, $
    C_LABEL_SHOW=c_label_show, $
    C_LINESTYLE=c_linestyle, $
    C_THICK=c_thick, $
    C_VALUE=c_valueIn, $
    N_LEVELS=n_levelsIn
    
  if (ISA(c_valueIn, /ARRAY)) then begin
    n_levels = N_ELEMENTS(c_valueIn)
    c_value = c_valueIn
  endif else begin
    n_levels = n_levelsIn
  endelse
  
  ; if c_color is a vector, then it is index to color palette.
  isIndex = SIZE(c_color, /n_dim) eq 1 && N_ELEMENTS(c_color) eq n_levels
  
  
  geomx = temporary(xparm)
  geomy = temporary(yparm)
  
  if (gridunits eq 2) then begin
    sMap = oVis->GetProjection()
    if (N_TAGS(sMap) eq 0) then sMap = !NULL
  endif
  
  ; get the points for the contour lines and connectivity array.
  if (fill) then begin
    ISOCONTOUR, contour_data, points, connectivity, /DOUBLE, /FILL, $
      C_VALUE=c_value, $
      N_LEVELS=n_levels, $
      GEOMX=geomx, GEOMY=geomy, $
      LEVEL_VALUES=level_values, $
      MAP_STRUCTURE=sMap, $
      OUTCONN_INDICES=outconn_indices
  endif else begin
    ISOCONTOUR, contour_data, points, connectivity, /DOUBLE, $
      C_VALUE=c_value, $
      N_LEVELS=n_levels, $
      GEOMX=geomx, GEOMY=geomy, $
      LEVEL_VALUES=level_values, $
      MAP_STRUCTURE=sMap, $
      OUTCONN_INDICES=outconn_indices
  endelse
  
  points_dim = size(points, /dimensions)
  num_points = points_dim[1]
  
  ; If we are in planar mode, set the Z values to 0.
  planar = 1
  points = points[0:1,*]
  ;  if (planar) then points[2,*] = 0
  
  self->_ConvertCoords, oVis, points
  points = ROUND(points*10)/10d
  
  id = self->_GetID(oVis)
  PRINTF, self._lun, '<g id="' + id + '"' + $
    (clip && ISA(clipID) ? clipID : '') + '>'
    
  ;  polyIndex = [ ]
  oWin = (oVis->GetTool())->GetCurrentWindow()
  
  for i=0,N_ELEMENTS(outconn_indices)/2-1 do begin
  
    if (outconn_indices[2*i] eq 0 && outconn_indices[2*i+1] eq 0) then $
      continue
    conn = connectivity[outconn_indices[2*i]:outconn_indices[2*i+1]]
    
    if (fill) then begin
      self->_CollapsePolygons, points, conn
    endif
    
    color = isIndex ? rgbTable[*, c_color[i]] : c_color[*,i]
    
    if (fill) then begin
      style = self->_FillStyle(color, transparency)
    endif else begin
      strokestyle = self->_StrokeStyle(color, transparency, $
        c_linestyle[[i]], c_thick[[i]])
      style = 'fill:none;' + strokestyle
    endelse
    
    PRINTF, self._lun, '<g id="' + id + '_line' + STRTRIM(i,2) + '">'
    
    path = ''
    PRINTF, self._lun, '<path style="' + style + '"' + ' d="'
    
    n = 10
    connIndex = 0L
    while (connIndex lt N_ELEMENTS(conn)) do begin
      npoly = conn[connIndex]
      ;      polyIndex = [polyIndex, i]
      if (npoly eq 0) then begin
        connIndex++
        continue
      endif
      if (npoly eq -1) then break
      conn1 = conn[connIndex + 1:connIndex + npoly]
      connIndex += npoly + 1
      ppoints = points[*,conn1]
      
      if (planar) then begin
        s = STRCOMPRESS(STRING(ppoints, $
          FORMAT='(F16.1,",",F16.1)'), /REMOVE_ALL)
      endif else begin
        s = STRCOMPRESS(STRING(ppoints, $
          FORMAT='(F16.1,",",F16.1,",",F16.1)'), /REMOVE_ALL)
      endelse
      
      PRINTF, self._lun, ' M ' + s[0] + ' L ' + $
        STRJOIN(s[1:(n-1) < (npoly-1)],' ')
      for j=n,npoly-1,n do $
        PRINTF, self._lun, STRJOIN(s[j:(j+n-1) < (npoly-1)], ' ')
    endwhile
    
    PRINTF, self._lun, '" />'
    
    ; Labels on line contours
    if (~fill) then begin
      oVis._oContour->GetLabelInfo, oWin, i, $
        LABEL_OFFSETS=loff, LABEL_POLYLINES=lpoly, LABEL_OBJECTS=lobj
      if (ISA(lobj[0])) then begin
        n = N_ELEMENTS(lobj)
        style = self->_TextStyle(color, transparency, $
          0.5, 0, fontname, fontsize, fontstyle)
        PRINTF, self._lun, '<g>'
        for j=0,n-1 do begin
          lobj[j]->GetProperty, LOCATION=loc, STRING=str, $
            BASELINE=baseline;, UPDIR=updir
          self->_ConvertCoords, oVis, loc
          loc = STRTRIM(STRING(loc, FORMAT='(F16.1)'), 2)
          
          ; Unlike the OpenGL pipeline, the text in the SVG won't go through
          ; the parent model's transform matrix. So if the axes were reversed,
          ; we need to "undo" the baseline & updir changes.
          if (xreverse) then begin
            baseline[0] = -baseline[0]
;            updir[0] = -updir[0]
          endif
          if (yreverse) then begin
            baseline[1] = -baseline[1]
;            updir[1] = -updir[1]
          endif
          angle = ATAN(-baseline[1], baseline[0])*!RADEG
          if (ABS(angle) gt 0.01) then begin
            angle = STRTRIM(STRING(angle, FORMAT='(F16.1)'), 2)
            rotate = ' transform="rotate(' + angle + ',' + loc[0] + ',' + loc[1] + ')"'
          endif else rotate = ''
          PRINTF, self._lun, '<text style="' + style + '"' + $
            ' x="' + loc[0] + '"' + $
            ' y="' + loc[1] + '"' + rotate + $
            '>' + str + '</text>'
        endfor
        PRINTF, self._lun, '</g>'
      endif
    endif
    
    PRINTF, self._lun, '</g>'
  endfor
  
  PRINTF, self._lun, '</g>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetImage, oVis, svgFilename, clipID
  compile_opt idl2, hidden
  
  oVis->GetProperty, $
    CLIP=clip, $
    RGB_TABLE=rgb, $
    POSITION=position, $
    PIXEL_XSIZE=pixelXSize, $
    PIXEL_YSIZE=pixelYSize, $
    XORIGIN=xOrigin, $
    YORIGIN=yOrigin
    
  oImage = (oVis->Get(/ALL, ISA='IDLgrImage'))[0]
  if (~OBJ_VALID(oImage)) then return
  
  oImage->GetProperty, DATA=imageData, $
    LOCATION=location, $
    DIMENSIONS=dimensions, $
    SUB_RECT=subRect
  xOrigin = location[0]
  yOrigin = location[1]
  xFinal = xOrigin + dimensions[0]
  yFinal = yOrigin + dimensions[1]
  dims = SIZE(imageData, /DIM)
  isTrue = N_ELEMENTS(dims) eq 3
  ;  width = dims[isTrue ? 1 : 0]
  ;  height = dims[isTrue ? 2 : 1]
  ;  xFinal = xOrigin + pixelXSize*width
  ;  yFinal = yOrigin + pixelYSize*height
  
  oVis->VisToWindow, [xOrigin, xOrigin, xFinal], [yOrigin, yFinal, yOrigin], x, y
  width = LONG(x[2] - x[0])
  height = LONG(y[1] - y[0])
  xStart = LONG(x[0])
  yStart = self._dims[1] - LONG(y[0]) - height
  
  filename = self->_GetID(FILE_BASENAME(svgFilename, '.SVG', /FOLD_CASE)) + '.png'
  filepath = FILE_DIRNAME(svgFilename, /MARK) + filename
  if (isTrue) then begin
    imageData = imageData[*, subRect[0]:subRect[0]+subRect[2]-1, subRect[1]:subRect[1]+subRect[3]-1]
    WRITE_PNG, filepath, imageData
  endif else begin
    imageData = imageData[subRect[0]:subRect[0]+subRect[2]-1, subRect[1]:subRect[1]+subRect[3]-1]
    rgb = TRANSPOSE(rgb)
    WRITE_PNG, filepath, imageData, rgb[*,0], rgb[*,1], rgb[*,2]
  endelse
  
  id = self->_GetID(oVis)
  PRINTF, self._lun, '<image id="' + id + '"' + $
    (clip && ISA(clipID) ? clipID : '') + $
    ' xlink:href="' + filename + '"' + $
    ' width="' + STRTRIM(width,2) + '"' + $
    ' height="' + STRTRIM(height,2) + '"' + $
    ' x="' + STRTRIM(xStart,2) + '" y="' + STRTRIM(yStart,2) + '"' + '/>'
    
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetLegend, oVis

  compile_opt idl2, hidden
  
  oVis->GetProperty, CLIP=clip, HIDE=hide
  if (hide) then return
  
  id = ' id="' + self->_GetID(oVis) + '"'
  PRINTF, self._lun, '<g' + id + (clip && ISA(clipID) ? clipID : '') + '>'
  
  oPoly = oVis->Get(/ALL, ISA='IDLitVisPolygon')
  foreach obj, oPoly do begin
    if (OBJ_VALID(obj)) then $
      self->SetVisPolygon, obj
  endforeach
  
  oItems = oVis->Get(/ALL, ISA='IDLitVisLegendItem')
  foreach oItem, oItems do begin
    if (~OBJ_VALID(oItem)) then continue
    
    oText = oItem->Get(/ALL, ISA='IDLgrText', COUNT=c)
    for i=0,c-1 do $
      self->SetText, oText, VIS=oItem
      
    self->SetPolyline, oItem
    
    oArrow = (oItem->Get(/ALL, ISA='IDLitVisArrow'))[0]
    if (OBJ_VALID(oArrow)) then begin
      self->SetArrow, oArrow
    endif
    
  endforeach
  
  
  PRINTF, self._lun, '</g>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetMapGrid, oVis, clipID

  compile_opt idl2, hidden
  
  oVis->GetProperty, CLIP=clip, HIDE=hide
  clip = 0
  if (hide) then return
  
  id = self->_GetID(oVis)
  PRINTF, self._lun, '<g id="' + id + '"' + $
    (clip && ISA(clipID) ? clipID : '') + '>'
    
  oContainer = oVis->Get(/ALL, ISA='IDLitVisMapGridContainer', COUNT=c)
  for i=0,c-1 do begin
    oContainer[i]->IDLgrComponent::GetProperty, HIDE=hide
    id = self->_GetID(oContainer[i])
    PRINTF, self._lun, '<g id="' + id + '">'
    oLines = oContainer[i]->Get(/ALL, ISA='IDLitVisMapGridline', COUNT=c1)
    foreach oLine, oLines do begin
      oLine->GetProperty, HIDE=hide, LABEL_OBJECTS=lobj
      if (hide) then continue
      self->SetPolyline, oLine
      if (OBJ_ISA(lobj, 'IDLgrText') && OBJ_VALID(oLine._oText)) then begin
        self->SetText, oLine._oText, VIS=oLine
      endif
    endforeach
    PRINTF, self._lun, '</g>'
  endfor
  
  ; Get the horizon
  oPoly = oVis->Get(/ALL, ISA='IDLitVisPolyline', COUNT=c)
  for i=0,c-1 do begin
    self->SetPolyline, oPoly[i]
  endfor
  
  oBox = (oVis->Get(/ALL, ISA='IDLitvisMapBoxAxes'))[0]
  if (OBJ_VALID(oBox)) then begin
    self->SetPolyline, oBox
  endif
  
  PRINTF, self._lun, '</g>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetPolyline, oLinesIn, clipID, VIS=oVisIn

  compile_opt idl2, hidden
  
  if (ISA(oLinesIn[0], 'IDLitVisualization')) then begin
    oVis = oLinesIn[0]
    oLines = oVis->Get(/ALL, ISA=['IDLgrPlot', 'IDLgrPolyline'])
  endif else begin
    oLines = oLinesIn
    oVis = oVisIn
  endelse

  oVis->GetProperty, CLIP=clip, SYM_SIZE=symSize

  foreach oLine, oLines do begin
  
    if (~OBJ_VALID(oLine)) then continue

    if  (ISA(oLine, 'IDLgrPlot')) then begin
      oLine->GetProperty, DATA=points, POLAR=polar, HIDE=hide
    endif else begin
      oLine->GetProperty, DATA=points, POLYLINES=conn, HIDE=hide, $
        LABEL_OBJECTS=labelObjects, LABEL_POLYLINES=labelPolylines, $
        LABEL_OFFSETS=labelOffsets
    endelse
    
    if (hide || N_ELEMENTS(points) le 1) then continue
    
    num_points = N_ELEMENTS(points[0,*])
    if (num_points eq 0) then return
    points = points[0:1,*]
    
    if KEYWORD_SET(polar) then begin
      angle = points[1,*]
      points[1,*] = points[0,*]*SIN(angle)
      points[0,*] = points[0,*]*COS(angle)
    endif
    
    if (N_ELEMENTS(conn) le 1) then begin
      conn = [num_points, LINDGEN(num_points)]
    endif
    nconn = N_ELEMENTS(conn)
    
    dims = SIZE(points, /DIM)
    
    oLine->GetProperty, ALPHA=alpha, COLOR=color, $
      THICK=thick, LINESTYLE=linestyle, SYMBOL=oSymbol, $
      PALETTE=oPalette, VERT_COLORS=vertColors
    transparency = 100.0 - alpha * 100.0
    
    if (N_ELEMENTS(vertColors) gt 1) then begin
      ; Convert indexed color into 3xN vertex colors.
      if (SIZE(vertColors, /N_DIM) eq 1) then begin
        if (ISA(oPalette, 'IDLgrPalette')) then begin
          oPalette->GetProperty, RED=r, GREEN=g, BLUE=b
        endif else begin
          r = BINDGEN(256)
          g = r
          b = r
        endelse
        n = N_ELEMENTS(r)
        vc = vertColors mod n
        vertColors = TRANSPOSE([[r[vc]], [g[vc]], [b[vc]]])
      endif
    endif
    
    markerID = ''
    if (ISA(oSymbol, 'IDLgrSymbol')) then begin
      markerID = self->_CreateMarker(oSymbol, symSize, color)
    endif
    
    labelID = ''
    if (ISA(labelObjects) && ISA(labelObjects[0], 'IDLgrSymbol')) then begin
      labelID = self->_CreateMarker(labelObjects[0], 2*symSize, color, /ORIENT_AUTO)
    endif

    self->_ConvertCoords, oVis, points
    
    id = ' id="' + self->_GetID(oVis) + '"'
    
    PRINTF, self._lun, '<g' + id + (clip && ISA(clipID) ? clipID : '') + '>'
    
    index = 0L
    idpoly = 0L
    while (index lt nconn) do begin
      polypoints =conn[index]
      if (polypoints eq 0) then begin
        index++
        continue
      endif
      if (polypoints eq -1) then $
        break
        
      connSegment = conn[index+1:index+polypoints]
      pointSegment = points[*, connSegment]
      index += polypoints + 1
      idpoly++
      
      if (SIZE(vertColors, /N_DIM) eq 2) then begin
        nc = (SIZE(vertColors, /DIM))[1]
        vertColorSegment = vertColors[*, connSegment mod nc]
        vertColorIndex = vertColorSegment[0,*]*65536L + $
          vertColorSegment[1,*]*256L + vertColorSegment[2,*]
        uniqColorIndices = UNIQ(vertColorIndex)
      endif else begin
        ; All points in this line segment have the same color.
        vertColorSegment = [[color], [color]]
        uniqColorIndices = [0, polypoints - 1]
      endelse
      
      ; Start with 1 because the color of a line segment is the color of
      ; the second vertex.
      for i=1,N_ELEMENTS(uniqColorIndices)-1 do begin
        i1 = uniqColorIndices[i-1]
        i2 = uniqColorIndices[i]
        color1 = vertColorSegment[*,i1 + 1]
        points1 = pointSegment[*,i1:i2]
        s = STRCOMPRESS(STRING(points1, $
          FORMAT='(F16.1,",",F16.1)'), /REMOVE_ALL)
        style = self->_StrokeStyle(color1, transparency, linestyle, thick, $
          NO_LINECAP=(N_ELEMENTS(uniqColorIndices) gt 2))
        style = 'fill:none;' + style
        if (markerID ne '') then $
          style += 'marker:url(#' + markerID + ');'
        PRINTF, self._lun, '<polyline style="' + style + '"'
        PRINTF, self._lun, '  points="' + STRJOIN(s, ' ') + '" />'
      endfor
      
      ; Draw label symbols on the polylines.
      if (labelID ne '') then begin
        labelOffset1 = labelOffsets[WHERE(labelPolylines eq idpoly, /NULL)]
        ; Make the little line segments transparent, so they don't show
        ; up again. The label symbols should hopefully still be drawn.
        style = self->_StrokeStyle(color, 100, linestyle, thick, $
          /NO_LINECAP)
        style += 'marker-start:url(#' + labelID + ');'
        if (ISA(labelOffset1)) then begin
          labelOffset1 = 0 > labelOffset1*(polypoints-1) < (polypoints-2)
          foreach offset, labelOffset1 do begin
            s = STRCOMPRESS(STRING(pointSegment[*, offset:offset+1], $
              FORMAT='(F16.1,",",F16.1)'), /REMOVE_ALL)
            PRINTF, self._lun, '<polyline style="' + style + '"'
            PRINTF, self._lun, '  points="' + STRJOIN(s, ' ') + '" />'
          endforeach
        endif
      endif
    endwhile
    
    PRINTF, self._lun, '</g>'
    
  endforeach
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetPlot, oVis, clipID

  compile_opt idl2, hidden
  
  oVis->GetProperty, CLIP=clip    
  id = ' id="' + self->_GetID(oVis) + '"'
  
  PRINTF, self._lun, '<g' + id + (clip && ISA(clipID) ? clipID : '') + '>'
  
  ; Filled plot background or barplot bars
  oFill = oVis->Get(/ALL, ISA='IDLitVisPolygon', COUNT=c)
  for i=0,c-1 do self->SetPolygon, oFill[i]
  
  ; Barplot outline and error bars
  oLines = oVis->Get(/ALL, ISA='IDLitVisPolyline', COUNT=c)
  for i=0,c-1 do self->SetPolyline, oLines[i]
    
  ; Plot line
  if (ISA(oVis, 'IDLitVisPlot')) then begin
    self->SetPolyline, oVis
  endif
  
  PRINTF, self._lun, '</g>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetPolygon, oPolygonsIn, VIS=oVisIn

  compile_opt idl2, hidden
  
  if (ISA(oPolygonsIn, 'IDLitVisualization')) then begin
    oVis = oPolygonsIn[0]
    oPolygons = oVis->Get(/ALL, ISA='IDLgrPolygon')
  endif else begin
    oPolygons = oPolygonsIn
    oVis = oVisIn
  endelse
  
  foreach oPoly, oPolygons do begin
    if (~OBJ_VALID(oPoly)) then continue

    oPoly->GetProperty, ALPHA=alpha, COLOR=fillColor, HIDE=hide, $
      DATA=data, POLYGONS=connectivity, $
      SHADING=shading, $
      TEXTURE_MAP=oTextureMap, $
      PALETTE=oPalette, VERT_COLORS=vertColors
      
    if (hide) then return
    
    if (N_ELEMENTS(data) le 3 || SIZE(data, /N_DIMENSIONS) ne 2) then $
      return
      
    transparency = 100.0 - alpha * 100.0
    
    if (N_ELEMENTS(vertColors) gt 1) then begin
      ; Convert indexed color into 3xN vertex colors.
      if (SIZE(vertColors, /N_DIM) eq 1) then begin
        if (ISA(oPalette, 'IDLgrPalette')) then begin
          oPalette->GetProperty, RED=r, GREEN=g, BLUE=b
        endif else begin
          r = BINDGEN(256)
          g = r
          b = r
        endelse
        n = N_ELEMENTS(r)
        vc = vertColors mod n
        vertColors = TRANSPOSE([[r[vc]], [g[vc]], [b[vc]]])
      endif
    endif
    
    ; TODO: Handle texture maps. For now just pick the color in the middle.
    if (OBJ_VALID(oTextureMap)) then begin
      oTextureMap->GetProperty, DATA=texture
      dims = SIZE(texture, /DIM)
      if (dims[0] ge 3) then begin
        colorTmp = texture[0:2,dims[1]/2, dims[2]/2]
        ; If the image is white, just use the fill color instead.
        ; Otherwise use the color in the middle.
        if (~ARRAY_EQUAL(colorTmp, 255b)) then $
          fillColor = colorTmp
      endif
    endif
  
    ; Get rid of Z since SVG is 2D.
    data = data[0:1,*]
    dims = SIZE(data, /DIM)
    self->_ConvertCoords, oVis, data
    
    num_points = (SIZE(data, /DIM))[1]
    if (~ISA(connectivity) || N_ELEMENTS(connectivity) le 1) then begin
      connectivity = [num_points, LINDGEN(num_points)]
    endif
    
    
    PRINTF, self._lun, '<g>'
    
    index = 0L
    idpoly = 0L
    nconn = N_ELEMENTS(connectivity)
    while (index lt nconn) do begin
      polypoints =connectivity[index]
      if (polypoints eq 0) then begin
        index++
        continue
      endif
      if (polypoints eq -1) then $
        break
        
      conn1 = connectivity[index+1:index+polypoints]
      index += polypoints + 1
      idpoly++
      points1 = data[*, conn1]
      ; Round off to the nearest 10.
      points1 = LONG64(points1*10)/10.0
      mn = MIN(points1, MAX=mx, DIMENSION=2)
      ; If we have colinear points on the polygon, don't output it.
      if (mn[0] eq mx[0] || mn[1] eq mx[1]) then continue
      
      if (SIZE(vertColors, /N_DIM) eq 2) then begin
        color1 = vertColors[*, conn1[0]]
        ; If we have 4 or more points, arbitrarily pick the 3rd point as the
        ; next "color" point. This will only work correctly for rectangles.
        ; If we only have a triangle, just use 1 color since SVG doesn't
        ; support arbitrary Gouraud shading.
        color2 = (shading && N_ELEMENTS(conn1) ge 4) ? $
          vertColors[*, conn1[2]] : color1
      endif else begin
        color1 = fillColor
        color2 = color1
      endelse
      
      ; See if we need to do gradient shading, say for a barplot.
      if (~ARRAY_EQUAL(color1, color2)) then begin
        color1 = STRING(color1, FORMAT='("stop-color:rgb(",I0,",",I0,",",I0,");")')
        color2 = STRING(color2, FORMAT='("stop-color:rgb(",I0,",",I0,",",I0,");")')
        id = self->_GetID('gradient')
        PRINTF, self._lun, '<defs>'
        dx = ABS(points1[0,2] - points1[0,0])
        dy = ABS(points1[1,2] - points1[1,0])
        if (dx ge dy) then begin
          x2 = '"100%"'
          y2 = '"0%"'
        endif else begin
          x2 = '"0%"'
          y2 = '"100%"'
          ; Reverse the Y colors since SVG has 0 at the top.
          tmp = color1
          color1 = color2
          color2 = tmp
        endelse
        PRINTF, self._lun, '<linearGradient id="' + id + $
          '" x1="0%" y1="0%" x2=' + x2 + ' y2=' + y2 + '>'
        opacity = 0 > ((100.-transparency)/100) < 1
        opacity = (opacity eq 1) ? '' : $
          'stop-opacity:'+STRING(opacity, FORMAT='(F4.2)')+';'
        PRINTF, self._lun, '<stop offset="0%" style="' + color1 + opacity + '" />'
        PRINTF, self._lun, '<stop offset="100%" style="' + color2 + opacity + '" />'
        PRINTF, self._lun, '</linearGradient>
        PRINTF, self._lun, '</defs>
        style = 'fill:url(#' + id + ');'
      endif else begin
        style = self->_FillStyle(color1, transparency)
;          + self->_StrokeStyle(color1, transparency, 0, 0.25, /NO_LINECAP)
      endelse
      
      PRINTF, self._lun, '<polygon style="' + style + '"'
      
      s = STRCOMPRESS(STRING(points1, $
        FORMAT='(F16.1,",",F16.1)'), /REMOVE_ALL)
        
      PRINTF, self._lun, '  points="' + STRJOIN(s, ' ') + '" />'
    endwhile
    
    PRINTF, self._lun, '</g>'

  endforeach
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetVisPolygon, oVis, clipID

  compile_opt idl2, hidden
  
  oVis->GetProperty, CLIP=clip, HIDE=hide, $
    FILL_BACKGROUND=fillBackground, LINESTYLE=linestyle
  if (hide) then return
  if (~fillBackground && linestyle eq 6) then return
  
  oVis->GetProperty, TESSELLATE=tessellate
  resetTessellate = KEYWORD_SET(tessellate)
  if (resetTessellate) then $
    oVis->SetProperty, TESSELLATE=0
    
  id = ' id="' + self->_GetID(oVis) + '"'
  PRINTF, self._lun, '<g' + id + (clip && ISA(clipID) ? clipID : '') + '>'
  
  self->SetPolygon, oVis
  
  if (linestyle ne 6) then begin
    self->SetPolyline, oVis
  endif
  
  if (resetTessellate) then begin
    oVis->SetProperty, /TESSELLATE
  endif
  
  PRINTF, self._lun, '</g>'
  
  
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetSymbol, oVis
  compile_opt idl2, hidden
  
  id = self->_GetID(oVis)
  PRINTF, self._lun, '<g id="' + id + '">'
  
  self->SetPolygon, oVis
  self->SetPolyline, oVis
  
  PRINTF, self._lun, '</g>'
  
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetText, oTextIn, clipID, VIS=oVisIn

  compile_opt idl2, hidden
  
  if (ISA(oTextIn, 'IDLitVisualization')) then begin
    oVis = oTextIn
    oText = (oVis->Get(/ALL, ISA='IDLgrText'))[0]
  endif else begin
    oText = oTextIn
    oVis = oVisIn
  endelse

  oVis->GetProperty, HIDE=hide, CLIP=clip
  if (hide) then return

  oText->GetProperty, ALPHA=alpha, BASELINE=baseline, $;UPDIR=updir, $
    COLOR=color, FONT=oFont, HIDE=hide, $
    ALIGNMENT=horzAlign, VERTICAL_ALIGN=vertAlign, $
    LOCATIONS=points, STRING=string
  if (hide) then return
  if (N_ELEMENTS(points) le 1) then return

  oDataSpace = oVis->GetDataSpace(/UNNORMALIZED)
  if (~OBJ_VALID(oDataSpace)) then return
  oDataSpace->GetProperty, $
    XLOG=xLog, YLOG=yLog, ZLOG=zLog, $
    XREVERSE=xreverse, YREVERSE=yreverse, ZREVERSE=zreverse

  ; Unlike the OpenGL pipeline, the text in the SVG won't go through
  ; the parent model's transform matrix. So if the axes were reversed,
  ; we need to "undo" the baseline & updir changes.
  if (xreverse) then begin
    baseline[0] = -baseline[0]
;    updir[0] = -updir[0]
  endif
  if (yreverse) then begin
    baseline[1] = -baseline[1]
;    updir[1] = -updir[1]
  endif
  angle = ATAN(-baseline[1], baseline[0])*!RADEG

  ; Hack for map gridlines, so they don't get cut through the middle.
  if (ISA(oVis, 'IDLitVisMapGridline') && vertAlign eq 0.5) then begin
    oVis->GetProperty, LABEL_POSITION=labelPos
    if (labelPos gt 0 && labelPos lt 1) then begin
      vertAlign = 0
    endif
  endif
  transparency = 100.0 - alpha * 100.0
  if (ISA(oFont, 'IDLgrFont')) then begin
    oFont->GetProperty, NAME=name, SIZE=fontSize
    split = STRTOK(name, '*', /EXTRACT)
    fontName = split[0]
    fontStyle = 0
    if (STRPOS(name, 'Bold') gt 0) then fontStyle = 1
    if (STRPOS(name, 'Italic') gt 0) then fontStyle or= 2
  endif else begin
    fontName = 'Helvetica'
    fontSize = 12
    fontStyle = ''
  endelse
  
  
  ;  idVis = oVis->GetFullIdentifier()
  ;  isAnnot = STRPOS(idVis, 'ANNOTATION LAYER') ge 0
  
  self->_ConvertCoords, oVis, points
  
  points = STRTRIM(LONG(points), 2)
  
  style = self->_TextStyle(color, transparency, $
    horzAlign, 0, fontname, fontsize, fontstyle)
    
  id = ' id="' + self->_GetID(oVis) + '"'
  if (clip && ISA(clipID)) then id += clipID
  PRINTF, self._lun, '<g' + id + '>'
  
  for i=0,N_ELEMENTS(string)-1 do begin
    substrings = STRTOK(string[i], '!C', /REGEX, /FOLD_CASE, $
      /EXTRACT, /PRESERVE_NULL)
    n = N_ELEMENTS(substrings)
    offset = -(n-1)
    rotate = ''
    if (ABS(angle) gt 0.01) then begin
      angle = STRTRIM(STRING(angle, FORMAT='(F16.1)'), 2)
      rotate = ' transform="rotate(' + angle + ',' + points[0,i] + $
        ',' + points[1,i] + ')"'
    endif
    foreach str, substrings do begin
      dy = ''
      if (vertAlign ne 0 || offset ne 0) then begin
        ; Handle vertical alignments using "dy" so it gets inherited by tspan's
        dy = ' dy="' + STRTRIM(STRING(vertAlign*0.8 + offset, FORMAT='(F16.2)'), 2) + 'em"'
        vertAlign = 0
      endif
      PRINTF, self._lun, '<text' + $
        ' x="' + points[0,i] + '"' + $
        ' y="' + points[1,i] + '"' + dy + $
        rotate + $
        ' style="' + style + '">'
      PRINTF, self._lun, self->_TextParse(str)
      PRINTF, self._lun, '</text>'
      offset++
    endforeach
  endfor
  
  PRINTF, self._lun, '</g>'
  
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::SetVector, oVis, clipID

  compile_opt idl2, hidden
  
  oVis->GetProperty, CLIP=clip, HIDE=hide
  if (hide) then return
  
  id = ' id="' + self->_GetID(oVis) + '"'
  PRINTF, self._lun, '<g' + id + (clip && ISA(clipID) ? clipID : '') + '>'
  
  self->SetPolygon, oVis
  
  self->SetPolyline, oVis
  
  PRINTF, self._lun, '</g>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::HandleBitmapFormat, oWindow, svgFilename

  compile_opt idl2, hidden
  
  oTool = oWindow->GetTool()
  oWriteFile = oTool->GetService("WRITE_FILE")
  
  resolution = self._resolution
  if (resolution le 0) then resolution = 600
  image = oWriteFile.GetImage(oWindow, RESOLUTION=resolution)

  imageFile = self->_GetID(FILE_BASENAME(svgFilename, '.SVG', /FOLD_CASE)) + '.png'
  filepath = FILE_DIRNAME(svgFilename, /MARK) + imageFile
  WRITE_PNG, filepath, image
  
  PRINTF, self._lun, '<image xlink:href="' + filepath + '"' + $
    ' width="' + STRTRIM(self._dims[0],2) + '"' + $
    ' height="' + STRTRIM(self._dims[1],2) + '"/>'
end


;---------------------------------------------------------------------------
pro IDLWriteSVG::HandleVectorFormat, oWindow, svgFilename

  compile_opt idl2, hidden

  ; Get all the data space
  oView = oWindow->GetCurrentView()
  if (~OBJ_VALID(oView)) then return
  oLayers =  oView->Get(/ALL, ISA='IDLitgrLayer')
  oDataspaces = []
  foreach oLayer, oLayers do begin
    oWorld =  oLayer->GetWorld()
    oNormDS = oWorld->GetDataSpaces()
    foreach oNDS, oNormDS do begin
      if (~OBJ_VALID(oNDS)) then continue
      oDataspaces = [oDataspaces, oNDS->GetDataSpace(/UNNORMALIZED)]
    endforeach
  endforeach
  
  foreach oDataspace, oDataspaces  do begin
    if (~ISA(oDataspace)) then continue
    
    ; attach other visulaizations, ie: polyline, arrow, etc
    oAllVis1 = oDataspace->GetVisualizations(/ALL, COUNT=nVis)
    
    oAllVis = [ ]
    for i=0,nVis-1 do begin
      oVis = oAllVis1[i]
      oVis->IDLgrComponent::GetProperty, HIDE=hide
      if (hide) then continue
      if (OBJ_CLASS(oVis) eq 'IDLITVISUALIZATION') then begin
        oSubVis = oVis->Get(/ALL, COUNT=c)
        if (c gt 0) then begin
          oVis = oSubVis
        endif
      endif
      oAllVis = [oAllVis, oVis]
    endfor
    if (~ISA(oAllVis)) then continue
    
    oDSNorm = oDataspace->GetDataSpace()
    transform = self->_GetTransform(oDSNorm, TRANSFORM=origTransform)
    
    id = self->_GetID(oDataspace)
    PRINTF, self._lun, '<g id="' + id + '"' + transform + '>'
    
    clipID = ''
    if (oDataspace->_GetXYZAxisRange(xrange, yrange, zrange)) then begin
      c = [[xrange[0], yrange[0]], $
        [xrange[1], yrange[1]]]
      self->_ConvertCoords, oDataspace, c
      rect = [MIN(c[0,*]), MIN(c[1,*]), ABS(c[0,1] - c[0,0]), ABS(c[1,0] - c[1,1])]
      rect = STRTRIM(STRING(rect, FORMAT='(F16.1)'),2)
      clipID = id + 'clip'
      PRINTF, self._lun, '  <clipPath id="' + clipID + '">'
      PRINTF, self._lun, '    <rect x="' + rect[0] + '" y="' + rect[1] + $
        '" width="' + rect[2] + '" height="' + rect[3] + '"/>'
      PRINTF, self._lun, '  </clipPath>'
      clipID = ' clip-path="url(#' + clipID + ')"'
    endif
    
    foreach oVis, oAllVis do begin
      oVis->IDLgrComponent::GetProperty, HIDE=hide
      if (hide) then continue
      
      PRINTF, self._lun, ''
      
      visTransform = self->_GetTransform(oVis, TRANSFORM=origVisTransform)
      if (visTransform ne '') then begin
        PRINTF, self._lun, '<g' + visTransform + '>'
      endif
      
      case (1) of
        ISA(oVis, 'IDLitVisArrow'): self->SetArrow, oVis
        ISA(oVis, 'IDLBubblePlot'): self->SetBubbleplot, oVis, clipID
        ISA(oVis, 'IDLitVisColorbar'): self->SetColorbar, oVis
        ISA(oVis, 'IDLitVisContour'): self->SetContour, oVis, clipID
        ISA(oVis, 'IDLitVisDataAxes'): self->SetAxes, oVis
        ISA(oVis, 'IDLitVisImage'): self->SetImage, oVis, svgFilename, clipID
        ISA(oVis, 'IDLitVisLegend'): self->SetLegend, oVis
        ISA(oVis, 'IDLitVisMapGrid'): self->SetMapGrid, oVis, clipID
        ISA(oVis, 'IDLitVisPlot'): self->SetPlot, oVis, clipID
        ISA(oVis, 'IDLitVisPolyline'): self->SetPolyline, oVis, clipID
        ISA(oVis, 'IDLitVisPolygon'): self->SetVisPolygon, oVis, clipID
        ISA(oVis, 'IDLitVisStreamline'): self->SetPolyline, oVis, clipID
        ISA(oVis, 'IDLitVisSymbol'): self->SetArrow, oVis
        ISA(oVis, 'IDLitVisText'): self->SetText, oVis, clipID
        ISA(oVis, 'IDLitVisVector'): self->SetVector, oVis, clipID
        else:
      endcase
      
      if (visTransform ne '') then begin
        PRINTF, self._lun, '</g>'
        oVis->SetProperty, TRANSFORM=origVisTransform
      endif
    endforeach
    
    PRINTF, self._lun, '</g>'
    
    if (transform ne '') then $
      oDSNorm->SetProperty, TRANSFORM=origTransform
      
  endforeach
end


;---------------------------------------------------------------------------
function IDLWriteSVG::SetData, oWindow

  compile_opt idl2, hidden
  
  Defsysv, '!iTools_Debug', EXISTS=hasDebug
  if (~hasDebug || ~!iTools_Debug) then begin
    on_error, 2
    catch, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      if (self._lun ne 0) then begin
        FREE_LUN, self._lun
      endif
      MESSAGE, !ERROR_STATE.MSG, /NONAME
    endif
  endif
  
  svgFilename = self->GetFilename()
  if (svgFilename eq '') then $
    return, 0 ; failure

  if (~ISA(self._ids)) then self._ids = HASH()
  self._ids.Remove, /ALL
  

  oWindow->GetProperty, DIMENSIONS=dimensions
  self._dims = dimensions
  
  oTool = oWindow->GetTool()
  oTool->GetProperty, NAME=name
  if (name eq '') then name = 'Graphic'
  name = 'IDL ' + name
  version = !version.release
  version = STRJOIN(STRTOK(version, '<', /EXTRACT), '[')
  version = STRJOIN(STRTOK(version, '>', /EXTRACT), '[')
  desc = 'IDL Graphic, created by IDL ' + version + ', ' + SYSTIME()
  
  header = ['<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>', $
    '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"', $
    '  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"', $
    '>', $
    '<svg xmlns="http://www.w3.org/2000/svg" version="1.1"', $
    '  xmlns:xlink="http://www.w3.org/1999/xlink"', $
    '  width="' + STRTRIM(self._dims[0],2) + 'px" height="' + $
    STRTRIM(self._dims[1],2) + 'px" >', $
    '  <title>' + name + '</title>', $
    '  <desc>' + desc + '</desc>', $
    '']
  ;    '  <defs>', $
  ;    '    <style type="text/css"><![CDATA[', $
  ;    '    path {', $
  ;    '      color-interpolation: linearRGB;', $
  ;    '    }', $
  ;    '    ]]></style>', $
  ;    '  </defs>']
  
  OPENW, lun, svgFilename, /GET_LUN
  self._lun = lun
  PRINTF, self._lun, header, FORMAT='(A)'

  if (self._graphicsFormat eq 0) then begin
    self->HandleBitmapFormat, oWindow, svgFilename
  endif else begin
    self->HandleVectorFormat, oWindow, svgFilename
  endelse
  
  PRINTF, self._lun, '</svg>'
  FREE_LUN, lun
  
  void = CHECK_MATH()  ; swallow underflow errors
  return, 1
  
end


;---------------------------------------------------------------------------
; Definition
;---------------------------------------------------------------------------
; Purpose:
;   Class definition.
;
pro IDLWriteSVG__Define

  compile_opt idl2, hidden
  
  void = {IDLWriteSVG, $
    inherits IDLitWriter, $
    _lun: 0L, $
    _ids: OBJ_NEW(), $
    _dims: [0L, 0L], $
    _resolution: 0d $
  }
end
