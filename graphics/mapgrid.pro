; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/graphics/mapgrid.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Create IDL Map graphic.
;
; :Params:
;    Projection
;
; :Keywords:
;    _REF_EXTRA
;
; :Returns:
;    Object Reference
;-
function MAPGRID, projection, $
  DEBUG=debug, $
  FILL_COLOR=fillColor, $
  TRANSPARENCY=transparency, $
  _REF_EXTRA=_extra

  compile_opt idl2, hidden
@graphic_error

  !NULL = iGetCurrent(TOOL=oTool)
  if (~OBJ_VALID(oTool)) then begin
    if (ISA(projection)) then begin
      m = MAP(projection, FILL_COLOR=fillColor, _EXTRA=_extra)
      return, m.mapgrid
    endif else begin
      MESSAGE, 'A map projection must exist or be specified as a scalar string.'
    endelse
  endif

  ; Check for unknown or illegal properties.
  if (N_ELEMENTS(_extra) gt 0) then $
    Graphic, _EXTRA=_extra, ERROR_CLASS='Mapgrid', /VERIFY_KEYWORDS

  oTool->DisableUpdates, PREVIOUSLY_DISABLED=previouslyDisabled

  oCreate = oTool->GetService("CREATE_VISUALIZATION")
  oVisDesc = oTool->GetVisualization('Map Grid')
  ; Call _Create so we don't have to worry about type matching.
  oVisCmd = oCreate->_Create(oVisDesc, ID_VISUALIZATION=idVis)
  oGrid = oTool->GetByIdentifier(idVis)
  oGrid->OnProjectionChange
  oGrid->SetProperty, _EXTRA=_extra

  ; Set this after we've set all our other properties.
  if (ISA(fillColor)) then begin
    oGrid->SetProperty, FILL_COLOR=fillColor
  endif

  ; Set this after we've set FILL_COLOR
  if (ISA(transparency)) then begin
    oGrid->SetProperty, TRANSPARENCY=transparency
  endif

  if (~previouslyDisabled) then begin
    oTool->EnableUpdates
    oTool->RefreshCurrentWindow
  endif
  
  return, OBJ_NEW('MapGrid', oGrid)
  
end
