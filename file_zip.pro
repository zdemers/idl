; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/file_zip.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+


;---------------------------------------------------------------------------
function FileZip_LocalHeader, data, unixtime, filename, currentSize, $
  compressedHeaderTrailer, localHeader

  compile_opt idl2, hidden
  
  big_endian = (BYTE(1s, 0, 2))[0] eq 0b
  
  ; Calculate length of header and trailer
  ; Trailer length is always 8
  hdr_len = 10
  bitflag = data[3]
  
  ; check each bit of flag
  if ((bitflag and 4) ne 0) then begin ; fextra
    fextra_len = UINT(data[10:11], 0)
    SWAP_ENDIAN_INPLACE, fextra_len, /SWAP_IF_BIG_ENDIAN
    hdr_len += 2 + fextra_len
  endif
  
  if ((bitflag and 8) ne 0) then begin ; fname
    while(data[hdr_len] ne 0b) do hdr_len++
  endif
  
  if ((bitflag and 16) ne 0) then begin ; fcomment
    while(data[hdr_len] ne 0b) do hdr_len++
  endif
  
  if ((bitflag and 2) ne 0) then begin ; fhcrc
    hdr_len += 2
  endif
  hdrTrlLen = [hdr_len, 8]

  ; File modification time, stored in standard MS-DOS format:
  ;   Bits 00-04: seconds divided by 2
  ;   Bits 05-10: minute
  ;   Bits 11-15: hour
  ; File modification date, stored in standard MS-DOS format:
  ;   Bits 00-04: day
  ;   Bits 05-08: month
  ;   Bits 09-15: years from 1980
  ;
  ; Preserve the original file's modification time (not our temp .gz time)
  t = BIN_DATE(SYSTIME(0, unixtime))
  mtime = BYTE(UINT(t[5]/2 + t[4]*32 + t[3]*2048),0,2)
  mdate = BYTE(UINT(t[2] + t[1]*32 + ((t[0] - 1980) > 0)*512),0,2)
  
  compressionMethod = BYTE(UINT(data[2]),0,2)
  compressedSize = BYTE(ULONG(N_ELEMENTS(data) - hdr_len - 8),0,4)
  fname_len = BYTE(UINT(STRLEN(filename)),0,2)
  localHeaderOffset = BYTE(ULONG(currentSize), 0, 4)
  
  if (big_endian) then begin
    mtime = REVERSE(mtime)
    mdate = REVERSE(mdate)
    compressionMethod = REVERSE(compressionMethod)
    compressedSize = REVERSE(compressedSize)
    fname_len = REVERSE(fname_len)
    localHeaderOffset = REVERSE(localHeaderOffset)
  endif
  

  compressedHeaderTrailer = { $
    signature: [80b,75b,3b,4b], $
    version: BYTARR(2), $
    bitflag: BYTARR(2), $
    compressionMethod: compressionMethod, $
    mtime: mtime, $
    mdate: mdate, $
    crc32: data[-8:-5], $
    compressedSize: compressedSize, $
    uncompressedSize: data[-4:-1], $
    fname_len: fname_len, $
    extra_len: BYTARR(2), $
    filename: BYTE(filename), $
    localHeaderOffset: localHeaderOffset $
  }
  
  
  localHeader = [compressedHeaderTrailer.signature, $
    compressedHeaderTrailer.version, $
    compressedHeaderTrailer.bitflag, $
    compressedHeaderTrailer.compressionMethod, $
    compressedHeaderTrailer.mtime, $
    compressedHeaderTrailer.mdate, $
    compressedHeaderTrailer.crc32, $
    compressedHeaderTrailer.compressedSize, $
    compressedHeaderTrailer.uncompressedSize, $
    compressedHeaderTrailer.fname_len, $
    compressedHeaderTrailer.extra_len, $
    compressedHeaderTrailer.filename $
    ]

  
  RETURN, hdrTrlLen
end


;---------------------------------------------------------------------------
function FileZip_CentralHeader, compressedHeaderTrailer
  
  compile_opt idl2, hidden
  
  ; Setup central directory header
  ; extra length and comment length are hardcoded to be 0.  what should they be?
  zipCentralDirHdr = { $
    signature:[80b,75b,1b,2b], $  ; PK
    version_made_by:BYTARR(2), $
    version_needed:BYTARR(2), $
    bitflag:BYTARR(2), $
    compressionMethod: compressedHeaderTrailer.compressionMethod, $
    mtime:compressedHeaderTrailer.mtime, $
    mdate:compressedHeaderTrailer.mdate, $
    crc32:compressedHeaderTrailer.crc32, $
    compressedSize:compressedHeaderTrailer.compressedSize, $
    uncompressedSize:compressedHeaderTrailer.uncompressedSize, $
    fname_len:compressedHeaderTrailer.fname_len, $
    extra_len:BYTARR(2), $
    comment_len:BYTARR(2), $
    disk_num:BYTARR(2), $
    int_attr:BYTARR(2), $
    ext_attr:BYTARR(4), $
    rel_offset:compressedHeaderTrailer.localHeaderOffset, $
    filename: compressedHeaderTrailer.filename $
  }
  
  centralDirHdr = BYTE([zipCentralDirHdr.signature, $
    zipCentralDirHdr.VERSION_MADE_BY, $
    zipCentralDirHdr.VERSION_NEEDED, $
    zipCentralDirHdr.bitflag, $
    zipCentralDirHdr.compressionMethod, $
    zipCentralDirHdr.MTIME, $
    zipCentralDirHdr.MDATE, $
    zipCentralDirHdr.CRC32, $
    zipCentralDirHdr.compressedSize, $
    zipCentralDirHdr.uncompressedSize, $
    zipCentralDirHdr.FNAME_LEN, $
    zipCentralDirHdr.EXTRA_LEN, $
    zipCentralDirHdr.COMMENT_LEN, $
    zipCentralDirHdr.DISK_NUM, $
    zipCentralDirHdr.INT_ATTR, $
    zipCentralDirHdr.EXT_ATTR, $
    zipCentralDirHdr.REL_OFFSET, $
    zipCentralDirHdr.filename $
    ])
    
  RETURN, centralDirHdr
end


;---------------------------------------------------------------------------
function FileZip_EndCentralHeader, centralDirSize, $
  centralDir_offset, num_centralDir
  
  compile_opt idl2, hidden
  
  big_endian = (BYTE(1s, 0, 2))[0] eq 0b
  
  ; Setup end central directory header
  ; comment length is hardcoded to be 0.  what should it be?
  endCentralDir={$
    signature: [80b,75b,5b,6b], $
    disk_num:BYTARR(2), $
    central_dir_disk:BYTARR(2), $
    num_central_dir_in_disk:BYTE(UINT(num_centralDir), 0,2), $
    central_dir_total:BYTE(UINT(num_centralDir), 0,2), $
    central_dir_size:BYTE(ULONG(centralDirSize),0,4), $
    central_dir_start:BYTE(ULONG(centralDir_offset),0,4), $
    comment_len:BYTARR(2) $
  }
  
  
  if (big_endian) then begin
    endCentralDir.num_central_dir_in_disk = REVERSE(endCentralDir.num_central_dir_in_disk)
    endCentralDir.central_dir_total = REVERSE(endCentralDir.central_dir_total)
    endCentralDir.central_dir_size =  REVERSE(endCentralDir.central_dir_size)
    endCentralDir.central_dir_start = REVERSE(endCentralDir.central_dir_start)
  endif
  
  supp_endCentralDir=BYTE([endCentralDir.signature, $
    endCentralDir.DISK_NUM, $
    endCentralDir.CENTRAL_DIR_DISK, $
    endCentralDir.NUM_CENTRAL_DIR_IN_DISK, $
    endCentralDir.CENTRAL_DIR_TOTAL, $
    endCentralDir.CENTRAL_DIR_SIZE, $
    endCentralDir.CENTRAL_DIR_START, $
    endCentralDir.comment_len $
    ])
    
  RETURN, supp_endCentralDir
end


;---------------------------------------------------------------------------
pro FILE_ZIP, filesIn, outfileIn, $
  DEBUG=debug, $
  FILES=files, $
  LIST=listIn, $
  VERBOSE=verboseIn

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then $
    ON_ERROR, 2

  if (N_PARAMS() eq 0) then $
    MESSAGE, 'Incorrect number of arguments.'
  
  sep = PATH_SEP()
  files = ''
  fileList = LIST()
  filenames = LIST()
  doList = KEYWORD_SET(listIn)
  verbose = KEYWORD_SET(verboseIn)

  for i=0, N_ELEMENTS(filesIn)-1 do begin
    if (~FILE_TEST(filesIn[i], /READ)) then $
      MESSAGE, 'Unable to open file: ' + filesIn[i]
    if (FILE_TEST(filesIn[i], /DIRECTORY)) then begin
      dir = filesIn[i]
      c = STRMID(dir, 0, /REVERSE_OFFSET)
      if (c ne '/' && c ne '\') then dir += sep
      parentdir = FILE_DIRNAME(dir)
      parentdir = (parentdir ne '.') ? parentdir + sep : ''
      dirlen = STRLEN(parentdir)
      fileList.Add, dir
      if (~doList) then $
        filenames.Add, FILE_BASENAME(dir) + sep
      subfiles = FILE_SEARCH(dir, '*', COUNT=nf, /MARK_DIR, /MATCH_INITIAL_DOT)
      for j=0,nf-1 do begin
        subfile = subfiles[j]
        c = STRMID(subfile, 0, /REVERSE_OFFSET)
        isDir = (c eq '/' || c eq '\')
        if (~isDir && ~FILE_TEST(subfile, /REGULAR)) then continue
        if (~STRCMP(subfile, dir, dirlen)) then continue
        fileList.Add, subfile
        if (~doList) then begin
          name = STRMID(subfile, dirlen)
          filenames.Add, name
        endif
      endfor
    endif else begin
      if (FILE_TEST(filesIn[i], /REGULAR)) then begin
        fileList.Add, filesIn[i]
        if (~doList) then $
          filenames.Add, FILE_BASENAME(filesIn[i])
      endif
    endelse
  endfor
  
  files = fileList.ToArray(/NO_COPY)
  fileList = 0

  if (doList) then begin
    if (verbose) then begin
      foreach f, files do MESSAGE, /INFO, /NONAME, f
    endif
    return
  endif
  
  filenames = filenames.ToArray(/NO_COPY)
  nfiles = N_ELEMENTS(files)

  if (N_PARAMS() eq 1) then begin
    if (nfiles eq 0) then $
      MESSAGE, 'Incorrect number of arguments.'
    fname = FILE_BASENAME(filesIn[0])
    dotPos = STRPOS(fname, '.', /REVERSE_SEARCH)
    if (dotPos gt 0) then fname = STRMID(fname, 0, dotPos)
    outfile = FILE_DIRNAME(filesIn[0], /MARK) + fname + '.zip'
  endif else begin
    if (N_ELEMENTS(outfileIn) gt 1) then $
      MESSAGE, 'Output file must be a scalar string.'
    outfile = outfileIn[0]
  endelse

  dir = FILE_DIRNAME(outfile)
  if (~FILE_TEST(dir, /WRITE)) then begin
    MESSAGE, 'Unable to write to the file: ' + outfile
  endif
  
  if (verbose) then tic
  
  sep = PATH_SEP()
  
  compressedHeaderTrailers = LIST()
  
  OPENW, zip_lun, outfile, /GET_LUN

  
  total_size = 0LL
  currentSize = 0LL

  for i=0, nfiles-1 do begin

    fi = FILE_INFO(files[i])
    
    if (fi.directory) then begin
      gzipSize = 0
      ;           ID1,  ID2, CM, FLG,   MTIME, XFL, OS,  CRC32, uncompress size
      gzipData = [31b, 139b, 0b, 0b, BYTARR(4), 0b, 0b, BYTARR(4), BYTARR(4)]
    endif else begin
      total_size += fi.size
  
      file_arr = []

      if (fi.size gt 0) then begin
        lun = 0
        CATCH, iErr
        if (iErr ne 0) then begin
          CATCH, /CANCEL
          if (lun ne 0) then FREE_LUN, lun
          goto, handleError
        endif
  
        OPENR, lun, files[i], /GET_LUN        
        file_arr = BYTARR(fi.size, /NOZERO)
        READU, lun, file_arr
        FREE_LUN, lun
        lun = 0
      endif
      
      gzipData = ZLIB_COMPRESS(file_arr, /GZIP_HEADER)
      gzipSize = N_ELEMENTS(gzipData)
    endelse

    hdrTrlLen = FileZip_LocalHeader(gzipData, fi.mtime, filenames[i], $
      currentSize, compressedHeaderTrailer, localHeader)
      
    compressedHeaderTrailers.ADD,compressedHeaderTrailer
    
    WRITEU, zip_lun, localHeader
    currentSize += N_ELEMENTS(localHeader)
    ; Write the compressed data contents (minus the header/trailer)
    i1 = hdrTrlLen[0]
    i2 = gzipSize - 1 - hdrTrlLen[1]
    if (i2 ge i1) then begin
      WRITEU, zip_lun, gzipData[i1:i2]
      currentSize += i2 - i1 + 1
      if (verbose) then begin
        spaceSavings = 100 - 100.0*gzipSize/(fi.size > 1)
        spaceSavings = 0 > ROUND(spaceSavings*10)/10. < 100
        spaceSavings = STRTRIM(STRING(spaceSavings, FORMAT='(F5.1)'),2)
        MESSAGE, /INFO, /NONAME, files[i] + '  ' + spaceSavings + '%'
      endif
    endif

  endfor
  
  if (~KEYWORD_SET(debug)) then begin
    CATCH, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      goto, handleError
    endif
  endif

  
  combined_centralDir_size = 0LL
  for i=0, nfiles-1 do begin
    centralDir = FileZip_CentralHeader(compressedHeaderTrailers[i])
    combined_centralDir_size += N_ELEMENTS(centralDir)
    WRITEU, zip_lun, centralDir
  endfor
  
  
  endCentralDir = FileZip_EndCentralHeader(combined_centralDir_size, $
    currentSize, nfiles)

  WRITEU, zip_lun, endCentralDir
  FREE_LUN, zip_lun, /FORCE
  zip_lun = 0
  
  if (verbose) then begin
    siz = (FILE_INFO(outfile)).size
    spaceSavings = 100 - 100.0*siz/(total_size > 1)
    spaceSavings = 0 > ROUND(spaceSavings*10)/10. < 100
    spaceSavings = STRTRIM(STRING(spaceSavings, FORMAT='(F5.1)'),2)
    MESSAGE, /INFO, /NONAME, 'Total ' + STRTRIM(nfiles,2) + $
      ' files, ' + STRTRIM(siz,2) + ' bytes, ' + $
      spaceSavings + '% space savings'
    toc
  endif

  if (~ISA(files)) then files = ''

  return

handleError:
  if (zip_lun ne 0) then FREE_LUN, zip_lun
  FILE_DELETE, outfile, /QUIET
  files = ''
  MESSAGE, /REISSUE_LAST

end

