; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/deriv.pro#1 $
;
; Copyright (c) 1984-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;

;+
; NAME:
;	DERIV
;
; PURPOSE:
;	Perform numerical differentiation using 3-point, Lagrangian
;	interpolation.
;
; CATEGORY:
;	Numerical analysis.
;
; CALLING SEQUENCE:
;	Dy = Deriv(Y)	 	;Dy(i)/di, point spacing = 1.
;	Dy = Deriv(X, Y)	;Dy/Dx, unequal point spacing.
;
; INPUTS:
;	Y:  Variable to be differentiated.
;	X:  Variable to differentiate with respect to.  If omitted, unit
;	    spacing for Y (i.e., X(i) = i) is assumed.
;
; OPTIONAL INPUT PARAMETERS:
;	As above.
;
; OUTPUTS:
;	Returns the derivative.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	See Hildebrand, Introduction to Numerical Analysis, Mc Graw
;	Hill, 1956.  Page 82.
;
; Three-point Lagrangian interpolation polynomial for [x0,x1,x2], [y0, y1, y2]
;   f = (x-x1)(x-x2)/(x01*x02)*y0 +
;       (x-x0)(x-x2)/(x10*x12)*y1 +
;       (x-x0)(x-x1)/(x02*x12)*y2
; Where: x01 = x0 - x1, x02 = x0 - x2, x12 = x1-x2, etc.
;
; df/dx = y' = y0*(2x-x1-x2)/(x01*x02) +
;              y1*(2x-x0-x2)/(x10*x12) +
;              y2*(2x-x0-x1)/(x02*x12)
;
; Evaluate at central point x=x1 (flip sign of last term so we can use x01):
;   y' = y0*x12/(x01*x02) + y1*(1/x12 - 1/x01) - y2*x01/(x02*x12)
;
; At x=x0 (for the first point):
;   y' = y0*(x01+x02)/(x01*x02) - y1*x02/(x01*x12) + y2*x01/(x02*x12)
;
; At x=x2 (for the last point):
;   y' = -y0*x12/(x01*x02) + y1*x02/(x01*x12) - y2*(x02+x12)/(x02*x12)
;
;
; MODIFICATION HISTORY:
;	Written, DMS, Aug, 1984.
;	Corrected formula for points with unequal spacing.  DMS, Nov, 1999.
;-
;
Function Deriv, X, Y
  on_error,2              ;Return to caller if an error occurs
  n = n_elements(x)
  if n lt 3 then message, 'Parameters must have at least 3 points'

  ; Equally spaced point case
  if (n_params(0) eq 1) then begin
    d = (shift(x,-1) - shift(x,1))/2.
    d[0] = (-3.0*x[0] + 4.0*x[1] - x[2])/2.
    d[n-1] = (3.*x[n-1] - 4.*x[n-2] + x[n-3])/2.
    return, d
  endif

  if n ne n_elements(y) then message,'Vectors must have same size'

  ; Compute x01 = x0 - x1, x02 = x0 - x2, x12 = x1-x2, etc.
  ; If not floating type, ensure floating...
  t = size(x, /type)
  x0 = (t eq 5 || t eq 9) ? shift(x, 1) : shift(float(x), 1)
  x2 = (t eq 5 || t eq 9) ? shift(x, -1) : shift(float(x), -1)
  x01 = x0 - x
  x02 = x0 - x2
  x12 = x - x2

  ; Middle points (flip sign of the last term so we can reuse x01)
  ;   df/dx = y0*x12/(x01*x02) + y1*(1/x12 - 1/x01) - y2*x01/(x02*x12)
  d = shift(y,1) * (x12 / (x01*x02)) + $
    y * (1./x12 - 1./x01) - $
    shift(y,-1) * (x01 / (x02*x12))

  ; Formulae for the first and last points:
  d[0] = y[0] * (x01[1]+x02[1])/(x01[1]*x02[1]) - $
    y[1] * x02[1]/(x01[1]*x12[1]) + $
    y[2] * x01[1]/(x02[1]*x12[1])
  d[-1] = -y[-3] * x12[-2]/(x01[-2]*x02[-2]) + $
    y[-2] * x02[-2]/(x01[-2]*x12[-2]) - $
    y[-1] * (x02[-2]+x12[-2]) / (x02[-2]*x12[-2])

  return, d
end
