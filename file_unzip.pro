; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/file_unzip.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+


;---------------------------------------------------------------------------
function FileUnzip_GetLocalHeader, zip_lun, centralHeader

  compile_opt idl2, hidden

  POINT_LUN, zip_lun, centralHeader.offset
  data = BYTARR(30)
  READU, zip_lun, data
  signature = [80b, 75b, 3b, 4b]
  if (~ARRAY_EQUAL(data[0:3], signature)) then return, !NULL

  bitflag = (UINT(data[6:7],0))[0]
  mtime = (UINT(data[10:11],0))[0]
  mdate = (UINT(data[12:13],0))[0]
  crc32 = (ULONG(data[14:17],0))[0]
  compressedSize = (ULONG(data[18:21],0))[0]
  uncompressedSize = (ULONG(data[22:25],0))[0]
  fname_len = (UINT(data[26:27],0))[0]
  extra_len = (UINT(data[28:29],0))[0]
  SWAP_ENDIAN_INPLACE, bitflag, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, mtime, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, mdate, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, crc32, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, compressedSize, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, uncompressedSize, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, fname_len, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, extra_len, /SWAP_IF_BIG_ENDIAN

  filename = BYTARR(fname_len)
  READU, zip_lun, filename
  filename = STRING(filename)
  
  ; Skip useless Mac-specific files.
  ff = STRUPCASE(filename)
  if (STRPOS(ff, '__MACOSX') ge 0 || STRPOS(ff, '.DS_STORE') ge 0) then $
    return, !NULL

  if (extra_len gt 0) then begin
    extradata = BYTARR(extra_len)
    READU, zip_lun, extradata
  endif

  ; Redundancy. Pick whichever size is larger.
  nbytes = centralHeader.compressedSize > compressedSize
  if (nbytes gt 0) then begin
    data = BYTARR(nbytes, /NOZERO)
    READU, zip_lun, data
    pdata = PTR_NEW(data, /NO_COPY)
  endif else begin
    pdata = PTR_NEW(/ALLOCATE_HEAP)
  endelse

  ; Get the sizes from the central 
  entry = {bitflag: bitflag, $
    mtime: mtime, $
    mdate: mdate, $
    uncompressedSize: centralHeader.uncompressedSize > uncompressedSize, $
    filename: filename, $
    pdata: pdata $
  }

  ; File modification time, stored in standard MS-DOS format:
  ;   Bits 00-04: seconds divided by 2
  ;   Bits 05-10: minute
  ;   Bits 11-15: hour
  ; File modification date, stored in standard MS-DOS format:
  ;   Bits 00-04: day
  ;   Bits 05-08: month
  ;   Bits 09-15: years from 1980
  ;
  ; Preserve the original file's modification time (not our temp .gz time)
;  t = BIN_DATE(SYSTIME(0, unixtime))
;  mtime = BYTE(UINT(t[5]/2 + t[4]*32 + t[3]*2048),0,2)
;  mdate = BYTE(UINT(t[2] + t[1]*32 + ((t[0] - 1980) > 0)*512),0,2)
 
  return, entry 
end


;---------------------------------------------------------------------------
function FileUnzip_GetCentralHeaders, data, entries
  
  compile_opt idl2, hidden
  
  result = LIST()
  signature = [80b, 75b, 1b, 2b]
  offset = 0LL
  for i=0,entries-1 do begin
    if (~ARRAY_EQUAL(data[offset:offset+3], signature)) then return, !NULL
    bitflag = (UINT(data[offset+8:offset+9],0))[0]
    mtime = (UINT(data[offset+12:offset+13],0))[0]
    mdate = (UINT(data[offset+14:offset+15],0))[0]
    crc32 = (ULONG(data[offset+16:offset+19],0))[0]
    compressedSize = (ULONG(data[offset+20:offset+23],0))[0]
    uncompressedSize = (ULONG(data[offset+24:offset+27],0))[0]
    if (compressedSize eq 'ffffffff'x || uncompressedSize eq 'ffffffff'x) then $
      MESSAGE, 'File appears to be in ZIP64 format. Unable to decompress.'
    fname_len = (UINT(data[offset+28:offset+29],0))[0]
    extra_len = (UINT(data[offset+30:offset+31],0))[0]
    comment_len = (UINT(data[offset+32:offset+33],0))[0]
    file_offset = (ULONG(data[offset+42:offset+45],0))[0]
    SWAP_ENDIAN_INPLACE, bitflag, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, mtime, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, mdate, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, crc32, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, compressedSize, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, uncompressedSize, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, fname_len, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, extra_len, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, comment_len, /SWAP_IF_BIG_ENDIAN
    SWAP_ENDIAN_INPLACE, file_offset, /SWAP_IF_BIG_ENDIAN
    filename = STRING(data[46:46+fname_len-1])
    entry = {bitflag: bitflag, $
      mtime: mtime, $
      mdate: mdate, $
      crc32: crc32, $
      compressedSize: compressedSize, $
      uncompressedSize: uncompressedSize, $
      offset: file_offset, $
      filename: filename}
    result.Add, entry
    offset += 46 + fname_len + extra_len + comment_len
  endfor

  result = result.ToArray(DIMENSION=1, /NO_COPY)
  return, result
end


;---------------------------------------------------------------------------
function FileUnzip_GetEndCentralHeader, file, zip_lun
  
  compile_opt idl2, hidden

  info = FSTAT(zip_lun)
  if (info.size lt 22) then $
    return, !NULL

  pointlun = (info.size - 10024) > 0

  while (1) do begin
    n = 10024 < info.size
    x = BYTARR(n, /NOZERO)
    POINT_LUN, zip_lun, pointlun
    READU, zip_lun, x
    ; Use String matching to find our signature.
    ; This is about 20 times faster than using a Where.
    signature = STRING([80b,75b,5b,6b])
    ; Replace nulls characters, so it doesn't truncate the string.
    y = x
    y >= 1b
    findSignature = STRPOS(STRING(y), signature, /REVERSE_SEARCH)
    if (findSignature ge 0 && (n - findSignature) ge 22) then break
    ; If we reach the beginning of the file, there was no match.
    if (pointlun eq 0) then return, !NULL
    ; Make sure we back up less than 1024-22, in case the signature
    ; was right on the boundary of our previous read. We need to read
    ; all 22 bytes of the signature at the same time.
    pointlun = (pointlun - 10000) > 0
  endwhile

  endCentralRecord = x[findSignature:findSignature+21]
  
  diskEntries = (UINT(endCentralRecord[8:9], 0))[0]
  totalEntries = (UINT(endCentralRecord[10:11], 0))[0]
  centralDirSize = (ULONG(endCentralRecord[12:15], 0))[0]
  centralDirOffset = (ULONG(endCentralRecord[16:19], 0))[0]
  SWAP_ENDIAN_INPLACE, diskEntries, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, totalEntries, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, centralDirSize, /SWAP_IF_BIG_ENDIAN
  SWAP_ENDIAN_INPLACE, centralDirOffset, /SWAP_IF_BIG_ENDIAN
  ; Don't care whether the user has filled in disk or total entries.
  entries = (totalEntries eq 0) ? diskEntries : totalEntries

  if (entries lt 0 || centralDirOffset lt 0 || centralDirOffset ge info.size || $
    centralDirSize lt 0 || centralDirSize gt info.size) then begin
      return, !NULL
  endif

  endCentralStruct = { $
    entries: entries, $
    centralDirSize: centralDirSize, $
    centralDirOffset: centralDirOffset}
  return, endCentralStruct

end


;---------------------------------------------------------------------------
pro FILE_UNZIP, fileIn, outdirIn, $
  DEBUG=debug, $
  FILES=files, $
  LIST=listIn, $
  VERBOSE=verboseIn

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then $
    ON_ERROR, 2

  if (N_PARAMS() eq 0) then $
    MESSAGE, 'Incorrect number of arguments.'

  if (N_ELEMENTS(fileIn) gt 1) then $
    MESSAGE, 'Input file must be a scalar string.'
  
  file = fileIn[0]
  files = ''
  doList = KEYWORD_SET(listIn)
  verbose = KEYWORD_SET(verboseIn)

  sep = PATH_SEP()
  
  if (~doList) then begin
    if (N_PARAMS() eq 1) then begin
      outdir = FILE_DIRNAME(file)
    endif else begin
      if (N_ELEMENTS(outdirIn) gt 1) then $
        MESSAGE, 'Output directory must be a scalar string.'
      outdir = outdirIn[0]
    endelse
  
    c = STRMID(outdir, 0, /REVERSE)
    ; "Normalize" the name to the current OS.
    outdir = STRJOIN(STRTOK(outdir, '\/', /EXTRACT, /PRESERVE_NULL), sep)
    ; If outdir is just a blank, then don't append a slash, because that
    ; would put the unzipped files at the root of the file system.
    if (c ne '') then outdir += sep
  endif
  
  if (verbose) then begin
    tic
  endif


  OPENR, zip_lun, file, /GET_LUN

  if (~KEYWORD_SET(debug)) then begin
    CATCH, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      if (zip_lun ne 0) then FREE_LUN, zip_lun
      MESSAGE, !ERROR_STATE.msg
    endif
  endif

  endCentralStruct = FileUnzip_GetEndCentralHeader(file, zip_lun)

  notAZip = 'File does not appear to be in the ZIP format: ' + file
  if (N_TAGS(endCentralStruct) eq 0) then $
    MESSAGE, notAZip

  centralHeaders = !null

  if (endCentralStruct.entries gt 0) then begin
    POINT_LUN, zip_lun, endCentralStruct.centralDirOffset
    centralDirData = BYTARR(endCentralStruct.centralDirSize, /NOZERO)
    READU, zip_lun, centralDirData
  
    centralHeaders = FileUnzip_GetCentralHeaders(centralDirData, endCentralStruct.entries)
  
    if (~ISA(centralHeaders)) then MESSAGE, notAZip
  endif

  ;TODO: What if the directory already exists?!
  if (~doList && ~FILE_TEST(outdir)) then begin
    FILE_MKDIR, outdir
  endif

  files = LIST()
  totalBytes = 0ull

  foreach centralHeader, centralHeaders do begin
    lun = 0

    iErr = 0
    if (~KEYWORD_SET(debug)) then CATCH, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      MESSAGE, /RESET
      if (verbose) then begin
        MESSAGE, /INFO, /NONAME, 'Error uncompressing ' + localHeader.filename + ', skipping...'
      endif
      if (lun ne 0) then FREE_LUN, lun
      continue
    endif

    localHeader = FileUnzip_GetLocalHeader(zip_lun, centralHeader)
    if (N_TAGS(localHeader) eq 0) then continue
    if (localHeader.filename eq '') then continue

    nbytes = N_ELEMENTS(*localHeader.pData)
    if (verbose) then begin
      bytes = ''
      c = STRMID(localHeader.filename, 0, /REVERSE_OFFSET)
      if (c ne '/' && c ne '\') then $
        bytes = ', ' + STRTRIM(nbytes,2) + ' bytes'
      MESSAGE, /INFO, /NONAME, localHeader.filename + bytes
      totalBytes += nbytes
    endif

    ; "Normalize" the name to the current OS.
    fname = STRJOIN(STRTOK(localHeader.filename, '\/', /EXTRACT, /PRESERVE_NULL), sep)
    if (~doList) then $
      fname = outdir + fname

    if (~doList) then begin
      if (localHeader.uncompressedSize gt 0) then begin
        uncompressedData = ZLIB_UNCOMPRESS(*localHeader.pData, /NO_HEADER)
        FILE_MKDIR, FILE_DIRNAME(fname)
        OPENW, lun, fname, /GET_LUN
        WRITEU, lun, uncompressedData
        FREE_LUN, lun
      endif else begin
        ; If no data then just create an empty file or directory.
        c = STRMID(fname, 0, /REVERSE_OFFSET)
        if (c eq '/' || c eq '\') then begin
          if (~FILE_TEST(fname)) then begin
            FILE_MKDIR, fname
          endif
        endif else begin
          OPENW, lun, fname, /GET_LUN
          FREE_LUN, lun
        endelse  
      endelse
    endif

    files.Add, fname

  endforeach
  
  files = files.ToArray(/NO_COPY)
  CATCH, /CANCEL
  FREE_LUN, zip_lun
  zip_lun = 0
  
  
  if (verbose) then begin
    MESSAGE, /INFO, /NONAME, 'Total ' + STRTRIM(N_ELEMENTS(files),2) + $
      ' files, ' + STRTRIM(totalBytes,2) + ' bytes'
    toc
  endif

end

