; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/file_untar.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+

; % Unpacked 833 files, 19883957 bytes
; % Time elapsed: 1.9977422 seconds.

;---------------------------------------------------------------------------
pro FileUntar_DecodeHeader, header, filename, nbytes

  compile_opt idl2, hidden

  ustar = STRLOWCASE(STRING(header[257:261])) eq 'ustar'
  if (~ustar) then $
    MESSAGE, /NONAME, 'TAR file is not in USTAR format.'

  filename = STRTRIM(STRING(header[0:99]),2)
  filepath = STRTRIM(STRING(header[345:*]),2)
  if (filepath ne '') then $
    filename = filepath + '/' + filename

  nbytes = 0ull
  READS, STRING(header[124:134]), nbytes, FORMAT='(o)'
  
  typeflag = STRING(header[156])

  case (typeflag) of
    '5': begin
      c = STRMID(filename, 0, /REVERSE_OFFSET)
      if (c ne '/' && c ne '\') then filename += '/'
      end
    '0': ; regular file
    '': ; regular file
    'x': filename = '@PaxHeader'
    'L': ; special GNU @LongLink
    else: MESSAGE, /NONAME, 'Skipping header of type: ' + typeflag
  endcase

end


;---------------------------------------------------------------------------
function FileUntar_GetFileInfo, tar_lun, pointlun, filename, nbytes, $
  STREAM=stream

  compile_opt idl2, hidden
  
  if (tar_lun ne 0) then begin
    header = BYTARR(512, /NOZERO)
    READU, tar_lun, header
  endif else begin
    header = stream[pointlun:pointlun+511]
  endelse
  pointlun += 512

  ; Empty block?
  if (ARRAY_EQUAL(header, 0b) || ARRAY_EQUAL(header, 32b)) then begin
    return, -1
  endif

  FileUntar_DecodeHeader, header, filename, nbytes
  if (filename eq '') then return, 0

  ; Special GNU tar trick to handle long filenames.
  if (STRPOS(filename, '@LongLink') ge 0) then begin
    if (nbytes eq 0) then return, 0
    ; Read the long filename (in 512-byte blocks)
    nbytes = 512*((nbytes + 511ull)/512)
    if (tar_lun ne 0) then begin
      filename = BYTARR(nbytes)
      READU, tar_lun, filename
    endif else begin
      filename = stream[pointlun:pointlun+nbytes-1]
    endelse

    pointlun += nbytes
    filename = STRING(filename)

    ; Now read the normal header block but ignore the filename.
    if (tar_lun ne 0) then begin
      READU, tar_lun, header
    endif else begin
      header = stream[pointlun:pointlun+511]
    endelse
    pointlun += 512
      
    FileUntar_DecodeHeader, header, !NULL, nbytes

  endif else if (STRPOS(filename, '@PaxHeader') ge 0) then begin
    ; POSIX extended header (the @PaxHeader is our own IDL internal flag)
    if (nbytes eq 0) then return, 0
    ; Read the PaxHeader (in 512-byte blocks)
    nbytes = 512*((nbytes + 511ull)/512)
    if (tar_lun ne 0) then begin
      paxheader = BYTARR(nbytes)
      READU, tar_lun, paxheader
    endif else begin
      paxheader = stream[pointlun:pointlun+511]
    endelse
    pointlun += nbytes
    paxheader = STRING(paxheader)
    keywords = STRTOK(paxheader, STRING(10b), /EXTRACT)
    nbytes = !NULL
    filename = !NULL
    foreach kw, keywords do begin
      kwStart = STRPOS(kw, ' ') + 1
      kwEnd = STRPOS(kw, '=')
      if (kwStart le 0 || kwEnd le kwStart) then continue
      keyword = STRMID(kw, kwStart, kwEnd-kwStart)
      value = STRMID(kw, kwEnd+1)
      case keyword of
        'path': filename = value
        'size': nbytes = ULONG64(value)
        else: ; ignore other keywords
      endcase
    endforeach
    ; Now read the normal header block but ignore either the filename or size.
    if (tar_lun ne 0) then begin
      READU, tar_lun, header
    endif else begin
      header = stream[pointlun:pointlun+511]
    endelse
    pointlun += 512
    if (~ISA(nbytes) || ~ISA(filename)) then begin 
      FileUntar_DecodeHeader, header, filenameTmp, nbytesTmp
      if (~ISA(nbytes)) then nbytes = nbytesTmp
      if (~ISA(filename)) then filename = filenameTmp
    endif
  endif

  return, 1
end


;---------------------------------------------------------------------------
pro FILE_UNTAR, stream, outdirIn, $
  DEBUG=debug, $
  FILES=files, $
  LIST=listIn, $
  VERBOSE=verboseIn

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then $
    ON_ERROR, 2

  if (N_PARAMS() eq 0) then $
    MESSAGE, 'Incorrect number of arguments.'

  isBuffer = ISA(stream, 'BYTE')
  isFile = ISA(stream, 'STRING') && N_ELEMENTS(stream) eq 1
  if (~isBuffer && ~isFile) then $
    MESSAGE, 'Input file must be a byte array or scalar string.'
  
  if (isFile) then begin
    file = stream[0]
  endif

  files = ''
  doList = KEYWORD_SET(listIn)
  verbose = KEYWORD_SET(verboseIn)

  sep = PATH_SEP()

  if (~doList) then begin  
    if (N_PARAMS() eq 1) then begin
      if (isBuffer) then $
        MESSAGE, 'Output directory must be specified with buffer data.'
      outdir = FILE_DIRNAME(file)
    endif else begin
      if (N_ELEMENTS(outdirIn) gt 1) then $
        MESSAGE, 'Output directory must be a scalar string.'
      outdir = outdirIn[0]
    endelse
  
    c = STRMID(outdir, 0, /REVERSE)
    ; "Normalize" the name to the current OS.
    outdir = STRJOIN(STRTOK(outdir, '\/', /EXTRACT, /PRESERVE_NULL), sep)
    ; If outdir is just a blank, then don't append a slash, because that
    ; would put the untarred files at the root of the file system.
    if (c ne '') then outdir += sep
  endif
  
  if (verbose) then begin
    tic
  endif

  tar_lun = 0

  if (~KEYWORD_SET(debug)) then begin
    CATCH, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      if (tar_lun ne 0) then FREE_LUN, tar_lun
      if (ISA(streamOrig)) then begin
        stream = TEMPORARY(streamOrig)
      endif
      MESSAGE, !ERROR_STATE.msg
    endif
  endif

  if (isFile) then begin
    fi = STRLOWCASE(file)
    isGZIP = STRMID(fi,2,/REVERSE) eq '.gz' || STRMID(fi,3,/REVERSE) eq '.tgz'
    if (~isGZIP) then begin
      OPENR, tar_lun, file, /GET_LUN
      x = BYTARR(2)
      READU, tar_lun, x
      FREE_LUN, tar_lun
      isGZIP = x[0] eq 31b && x[1] eq 139b
      tar_lun = 0
    endif

    tmp = ''
    if (isGZIP) then begin
      tmp = FILEPATH(FILE_BASENAME(outdir), /TMP) + '.tar'
      FILE_GUNZIP, file, tmp, VERBOSE=verbose
      file = tmp
    endif
    
    OPENR, tar_lun, file, /GET_LUN, DELETE=(tmp ne '')

  endif else begin

    isGZIP = stream[0] eq 31b && stream[1] eq 139b
    if (isGZIP) then begin
      streamOrig = TEMPORARY(stream)
      stream = ZLIB_UNCOMPRESS(streamOrig, /GZIP_HEADER)
    endif

  endelse


  ;TODO: What if the directory already exists?!
  if (~doList && ~FILE_TEST(outdir)) then begin
    FILE_MKDIR, outdir
  endif

  files = LIST()
  empty = 0
  totalBytes = 0ull
  pointlun = 0ull
  data = BYTARR(512, /NOZERO)

  while (1) do begin

    if (isFile) then begin
      if (EOF(tar_lun)) then break
    endif else begin
      if (pointlun ge N_ELEMENTS(stream)) then break
    endelse

    lun = 0
    filename = ''
    nbytes = 0ull

    if (~KEYWORD_SET(debug)) then begin
      CATCH, iErr
      if (iErr ne 0) then begin
        CATCH, /CANCEL
        MESSAGE, /RESET
        MESSAGE, /INFO, msg
        if (lun ne 0) then FREE_LUN, lun
        if (nbytes ne 0) then begin
          ; Advance to the next block.
          pointlun += 512*((nbytes+511ull)/512)
          if (isFile) then begin
            POINT_LUN, tar_lun, pointlun
          endif
        endif
        continue
      endif
    endif

    msg = 'Error reading ' + filename + ', skipping...'
    status = FileUntar_GetFileInfo(tar_lun, pointlun, filename, nbytes, $
      STREAM=stream)

    ; Status == -1 indicates an empty block
    if (status eq -1) then begin
      empty++
      ; Two empty 512-byte blocks indicates the end of the TAR file.
      if (empty eq 2) then break
      continue
    endif
    
    ; Reset the empty flag if we found a header
    empty = 0

    ; Status == 0 indicates skip the file    
    if (status eq 0 || filename eq '') then continue

    if (verbose) then begin
      bytes = ''
      c = STRMID(filename, 0, /REVERSE_OFFSET)
      if (c ne '/' && c ne '\') then $
        bytes = ', ' + STRTRIM(nbytes,2) + ' bytes'
      MESSAGE, /INFO, /NONAME, filename + bytes
      totalBytes += nbytes
    endif

    ; "Normalize" the name to the current OS.
    fname = STRJOIN(STRTOK(filename, '\/', /EXTRACT, /PRESERVE_NULL), sep)

    if (~doList) then begin
      fname = outdir + fname
      c = STRMID(fname, 0, /REVERSE_OFFSET)
      if (c eq '/' || c eq '\') then begin
        if (~FILE_TEST(fname)) then begin
          msg = 'Error writing directory ' + fname + ', skipping...'
          FILE_MKDIR, fname
        endif
      endif else begin
        msg = 'Error writing file ' + fname + ', skipping...'
        OPENW, lun, fname, /GET_LUN
        if (nbytes gt 0) then begin
          if (isFile) then begin
            COPY_LUN, tar_lun, lun, nbytes
          endif else begin
            WRITEU, lun, stream[pointlun:pointlun+nbytes-1]
          endelse
        endif
        FREE_LUN, lun
      endelse
    endif

    ; Advance to the next block.
    pointlun += 512*((nbytes + 511ull)/512)
    if (isFile) then begin
      POINT_LUN, tar_lun, pointlun
    endif

    files.Add, fname

  endwhile
  
  CATCH, /CANCEL
  
  if (isFile) then begin
    FREE_LUN, tar_lun
  endif
  tar_lun = 0

  files = files.ToArray(/NO_COPY)

  if (ISA(streamOrig)) then begin
    stream = TEMPORARY(streamOrig)
  endif
  
  if (verbose) then begin
    MESSAGE, /INFO, /NONAME, 'Total ' + STRTRIM(N_ELEMENTS(files),2) + $
      ' files, ' + STRTRIM(totalBytes,2) + ' bytes'
    toc
  endif

end

