; $Id:
;
; Copyright (c) 2005-2013, Exelis Visual Information Solutions, Inc.  All rights reserved.
;    Unauthorized reproduction prohibited.
;
;----------------------------------------------------------------------------
; Class Name:
;   IDLhpShaderSharpen
;
; Purpose:
;   This contains the GLSL implementation of IDLdfSharpen. It will
;   be used instead of the C/C++ implementation in IDLdfSharpen
;   if GPU HW support is available.
;

;----------------------------------------------------------------------------
; Lifecycle Methods
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; IDLhpShaderSharpen::Init
;
function IDLhpShaderSharpen::Init, _EXTRA=_extra
    compile_opt idl2, hidden
    
    if (~self->IDLgrShader::Init(_EXTRA=_extra, OUTPUT_DATA_TYPE=1)) then $
       return, 0

    self.sharpFactor = 0
    self.kernelSize = 0
    self.loadedKernelSize = 0
    self->LoadShader

    return, 1
end

;----------------------------------------------------------------------------
; IDLhpShaderSharpen::Cleanup
;
pro IDLhpShaderSharpen::Cleanup
    compile_opt idl2, hidden

    self->IDLgrShader::Cleanup

end

;----------------------------------------------------------------------------
; Get/SetProperty
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; IDLhpShaderSharpen::SetProperty
;
pro IDLhpShaderSharpen::SetProperty, SHARP_FACTOR=sharpFactor, $
                                        _EXTRA=_extra
    compile_opt idl2, hidden

    if (N_Elements(sharpFactor) ne 0) then begin
        self.sharpFactor = sharpFactor
        self->UpdateKernel
    endif

    self->IDLgrShader::SetProperty, _EXTRA=_extra
end

;----------------------------------------------------------------------------
; IDLhpShaderSharpen::GetProperty
;
pro IDLhpShaderSharpen::GetProperty, SHARP_FACTOR=sharpFactor, $
                                        _REF_EXTRA=_extra
    compile_opt idl2, hidden

    if (arg_present(sharpFactor)) then begin 
        sharpFactor = self.sharpFactor
    endif

    self->IDLgrShader::GetProperty, _EXTRA=_extra
end

;----------------------------------------------------------------------
;IDLhpShaderSharpen::UpdateKernel
;
;Purpose:
; Private method to define a convolution kernel for sharpening based on
; the established sharpening factor
;
;
pro IDLhpShaderSharpen::UpdateKernel
    compile_opt idl2, hidden

    if (self.sharpFactor) eq 0 then begin
        ;; Setting kernelSize 0 will disable the shader
        self.kernelSize = 0
    endif else if (self.sharpFactor gt 0) then begin
        ;; Sharpen
        ;; Create some scaled kernel that sorta corresponds to a 0-90 range
        kernel = fltarr(3,3)-1
        kernel[1,1] = 30*(1-self.sharpFactor/145)        

        self.kernelSize = 3
        self->IDLgrShader::SetUniformVariable, 'kernel', kernel
        self->IDLgrShader::SetUniformVariable, 'scale', 1.0 / total(kernel)
    endif else if (self.sharpFactor lt 0) then begin
        ;; Smooth 
        sf = abs(self.sharpFactor)
        if (sf le 3) then begin
            kernel = fltarr(3,3) + 1.0/9.0
            kernelsize = 3
        endif else begin
            ;; There appears to be a bug with setting uniform
            ;; variable arrays on ATI cards. This will need looked
            ;; at but in the meantime we can get away with passing
            ;; a single kernel value as the 5x5 is only used when
            ;; smoothing, and the smooth kernel uses 1/25 in every element.
            kernel = 1.0/25.0 ;; fltarr(25) + 1.0/25.0
            kernelsize = 5
        endelse
        self.kernelSize = kernelSize
        self->IDLgrShader::SetUniformVariable, 'kernel', kernel
        self->IDLgrSHader::SetUniformVariable, 'scale', 1.0
    endif

    if (self.kernelSize ne self.loadedKernelSize) then begin
        ;; The kernel size changed, change the shader
        self->LoadShader
    endif
end

;----------------------------------------------------------------------------
; IDLcfShaderSharpen::LoadShader
;
; Purpose:
;   This procedure loads the shader source approprate for the current
;   kernel size.
;
pro IDLhpShaderSharpen::LoadShader
    compile_opt idl2, hidden

    vertexProgram = [ $
    'void main() {', $
      'gl_TexCoord[0] = gl_MultiTexCoord0;', $
      'gl_Position = ftransform();', $
    '}']

    case self.kernelSize of          
       3: begin
          fragmentProgram = $
       [ $
       'uniform sampler2D _IDL_ImageTexture;', $
       'uniform vec2 _IDL_ImageStep;', $
       'uniform mat3 kernel;', $
       'uniform float scale;', $
       'void main(void) {', $
       '  vec4 c;', $ ;; Centre texel
       '  vec4 sum = vec4(0.0);', $
       '  vec2 ox = vec2(_IDL_ImageStep.x, 0.0);', $
       '  vec2 oy = vec2(0.0, _IDL_ImageStep.y);', $
       '  vec2 tc = gl_TexCoord[0].st - oy;', $
       '  sum += kernel[0][0] * texture2D(_IDL_ImageTexture, tc - ox);', $
       '  sum += kernel[0][1] * texture2D(_IDL_ImageTexture, tc     );', $
       '  sum += kernel[0][2] * texture2D(_IDL_ImageTexture, tc + ox);', $
       '  tc = gl_TexCoord[0].st;', $
       '  sum += kernel[1][0] * texture2D(_IDL_ImageTexture, tc - ox);', $
       '  c = texture2D(_IDL_ImageTexture, tc);', $
       '  sum += kernel[1][1] * c;', $
       '  sum += kernel[1][2] * texture2D(_IDL_ImageTexture, tc + ox);', $
       '  tc = gl_TexCoord[0].st + oy;', $
       '  sum += kernel[2][0] * texture2D(_IDL_ImageTexture, tc - ox);', $
       '  sum += kernel[2][1] * texture2D(_IDL_ImageTexture, tc     );', $
       '  sum += kernel[2][2] * texture2D(_IDL_ImageTexture, tc + ox);', $
       '  sum *= scale;', $
       '  sum.a = c.a;', $ ;; Use alpha from centre texel
       '  gl_FragColor = sum;', $
       '}' ]    
          self->SetProperty, $
             VERTEX_PROGRAM_STRING=STRJOIN(vertexProgram, STRING(10B)), $
             FRAGMENT_PROGRAM_STRING=STRJOIN(fragmentProgram, STRING(10B))
       end
       5: begin
           ;; There appears to be a bug with setting uniform
           ;; variable arrays on ATI cards. This will need looked
           ;; at but in the meantime we can get away with passing
           ;; a single kernel value as the 5x5 is only used when
           ;; smoothing, and the smooth kernel uses 1/25 in every element.
           fragmentProgram = $
       [ $
       'uniform sampler2D _IDL_ImageTexture;', $
       'uniform vec2 _IDL_ImageStep;', $
       'uniform float kernel;', $
       'uniform float scale;', $
       'void main(void) {', $
       '  vec4 c;', $
       '  vec4 sum = vec4(0.0);', $
       '  vec2 ox = vec2(_IDL_ImageStep.x, 0.0);', $
       '  vec2 oy = vec2(0.0, _IDL_ImageStep.y);', $
       '  vec2 tc = gl_TexCoord[0].st - oy * 2.0;', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox * 2.0);', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc           );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox * 2.0);', $
       '  tc = gl_TexCoord[0].st - oy;', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox * 2.0);', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc           );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox * 2.0);', $
       '  tc = gl_TexCoord[0].st;', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox * 2.0);', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox      );', $
       '  c = texture2D(_IDL_ImageTexture, tc);', $
       '  sum += kernel * c;', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox * 2.0);', $
       '  tc = gl_TexCoord[0].st + oy;', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox * 2.0);', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc           );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox * 2.0);', $
       '  tc = gl_TexCoord[0].st + oy * 2.0;', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox * 2.0);', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc - ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc           );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox      );', $
       '  sum += kernel * texture2D(_IDL_ImageTexture, tc + ox * 2.0);', $
       '  sum *= scale;', $
       '  sum.a = c.a;', $ ;; Use alpha from centre texel
       '  gl_FragColor = sum;', $
       '}' ]
          self->SetProperty, $
             VERTEX_PROGRAM_STRING=STRJOIN(vertexProgram, STRING(10B)), $
             FRAGMENT_PROGRAM_STRING=STRJOIN(fragmentProgram, STRING(10B))
       end
       else: begin
          ;; TODO: This isn't ideal.
          ;; We're setting a shader that simply reads the texture,
          ;; it would be more efficient to disable the shader
          ;; altogether. Unfortunately we can't do this in IDL yet.
          fragmentProgram = $
          [ $
          'uniform sampler2D _IDL_ImageTexture;', $
          'void main(void) {', $
          '  gl_FragColor = texture2D(_IDL_ImageTexture, gl_TexCoord[0].xy);', $
          '}' ]    
          self->SetProperty, $
             VERTEX_PROGRAM_STRING=STRJOIN(vertexProgram, STRING(10B)), $
             FRAGMENT_PROGRAM_STRING=STRJOIN(fragmentProgram, STRING(10B))
       end
    endcase
    self.loadedKernelSize = self.kernelSize
end

;----------------------------------------------------------------------------
; IDLhpShaderSharpen::Filter
;
; Purpose:
;
;   Currently this is not used, if no HW support then zoom uses the
;   existing raster pipeline processing mechanism rather than the
;   Shader SW fallback. 
;
; Arguments:
;   Image: image data to be filtered
;
function IDLhpShaderSharpen::Filter, Image

    return, Image

end

;----------------------------------------------------------------------------
; Object Definition
;----------------------------------------------------------------------------
;----------------------------------------------------------------------------
; IDLhpShaderSharpen__Define
;
pro IDLhpShaderSharpen__Define
    compile_opt idl2, hidden

    void = { IDLhpShaderSharpen, $
             inherits IDLgrShader, $
             sharpFactor: 0e, $
             kernelSize: 0b, $
             loadedKernelSize: 0b $
           }
end
