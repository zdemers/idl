pro amber_polynorm, file, ord, calfile=calfile, file_s=file_s

if not keyword_set(file_s) then file_s = 'norm_'+file
arr = 1
r = [0, 255, 255, 0, 0, 100, 100, 100, 0, 50,0]
g = [0, 255, 0, 255, 0,100, 0, 100, 100, 100,50]
b = [0, 255, 0, 0, 255,100, 100, 0, 100, 0, 100]
tvlct, r, g, b, 0

READ_OIDATA, file, oiarray,oitarget,oiwavelength,oivis, oivis2,oit3, /inventory
if keyword_set(calfile) then READ_OIDATA, calfile, c_oiarray,c_oitarget,c_oiwavelength,c_oivis, c_oivis2, c_oit3, /inventory
wave = *oiwavelength.eff_wave
rms = make_array(6)
while arr lt 7 do begin
	if arr eq 1 then begin
		flux = *oivis(0).visamp
		fsig = *oivis(0).visamperr
		if keyword_set(calfile) then begin
			c_flux = *c_oivis(0).visamp
			flux = flux/c_flux
		endif
	endif
	if arr eq 2 then begin
		flux = *oivis(1).visamp
		fsig = *oivis(1).visamperr
		if keyword_set(calfile) then begin
			c_flux = *c_oivis(1).visamp
			flux = flux/c_flux
		endif
	endif
	if arr eq 3 then begin
		flux = *oivis(2).visamp
		fsig = *oivis(2).visamperr
		if keyword_set(calfile) then begin
			c_flux = *c_oivis(2).visamp
			flux = flux/c_flux
		endif
	endif
	if arr eq 4 then begin
		flux = *oivis(0).visphi
		fsig = *oivis(0).visphierr
		if keyword_set(calfile) then begin
			c_flux = *c_oivis(0).visphi
			flux = flux - c_flux
		endif
	endif
	if arr eq 5 then begin
		flux = *oivis(1).visphi
		fsig = *oivis(1).visphierr
		if keyword_set(calfile) then begin
			c_flux = *c_oivis(1).visphi
			flux = flux - c_flux
		endif
	endif
	if arr eq 6 then begin
		flux = *oivis(2).visphi
		fsig = *oivis(2).visphierr
		if keyword_set(calfile) then begin
			c_flux = *c_oivis(2).visphi
			flux = flux - c_flux
		endif
	endif
	rc = 0.
	acc = 'n'
	while acc ne 'y' do begin
		if rc eq 0 then exclusion = [2.1625, 2.17,2.182,2.186]
		if rc gt 0 then begin
			cursor, ex0, t1fs, /down; select exclusion region
			cursor, ex1, t1fe, /down
			exclusion = [exclusion, ex0, ex1]
;			print, exclusion
		endif
		window, 0, xsize=1000, ysize=400
		plot, wave, flux, color=0, background=255, position = [.09, .52, .95, .91], yrange = [min(flux), max(flux)], xtickname = replicate(' ', 10)
		nex = n_elements(exclusion)
		include = where(wave le exclusion[0] or wave ge exclusion[1])
		in_wave = wave[include]
		in_flux = flux[include]
		in_fsig = fsig[include]
		for i = 2., nex - 1., 2 do begin
			include = where(in_wave le exclusion[i] or in_wave ge exclusion[i+1])
			in_wave = in_wave[include]
			in_flux = in_flux[include]
			in_fsig = in_fsig[include]
		endfor
		p_circle, form=4, fill=1
		oplot, in_wave, in_flux, psym=8, color=4
		fit = poly_fit(in_wave, in_flux, ord, measure_errors=in_fsig)
		crv = fit[0]
		for i = 1., n_elements(fit) - 1. do crv += fit[i] * wave^i
		oplot, wave, crv, color=2

		if arr le 3 then fcorr = flux / crv
		if arr gt 3 then fcorr = flux - crv
		plot, wave, fcorr, color=0, background=255, position = [.09, .09, .95, .48], yrange = [min(fcorr), max(fcorr)], /noerase
		print, "Accept? (y/n): "
		acc = get_kbrd(1)
		rc += 1
	endwhile
	crv = fit[0]
	for i = 1., n_elements(fit) - 1. do crv += fit[i] * in_wave^i
	if arr le 3 then in_flux = in_flux / crv
	if arr gt 3 then in_flux = in_flux - crv	
	ncon = n_elements(in_wave)
	rms[arr-1] = sqrt(1./ncon * total(in_flux^2))
	if arr gt 3 then rms[arr-1] = sqrt(1./ncon * total((in_flux - crv)^2))
	if arr eq 1 then *oivis(0).visamp =  fcorr
	if arr eq 2 then *oivis(1).visamp =  fcorr
	if arr eq 3 then *oivis(2).visamp =  fcorr
	if arr eq 4 then *oivis(0).visphi =  fcorr
	if arr eq 5 then *oivis(1).visphi =  fcorr
	if arr eq 6 then *oivis(2).visphi =  fcorr		
	arr += 1
endwhile
print, "RMS: "
print, rms
write_oidata, file_s, oiarray, oitarget, oiwavelength, oivis, oivis2, oit3
get_lun, lun
openw, lun, file_s + '_rms.txt'
for i = 0., n_elements(rms) - 1. do printf, lun, rms[i]
close, lun
free_lun, lun
end
