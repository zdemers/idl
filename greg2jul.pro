; $Id: //depot/InDevelopment/scrums/IDL_Kraken/idl/idldir/lib/julday.pro#1 $
;
; Copyright (c) 2012-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
;	  GREG2JUL
;
; PURPOSE:
;	  Calculate the Julian Day Number for a given month, day, and year,
;	  using the proleptic Gregorian calendar.
;	  See also JUL2GREG, the inverse of this function.
;
;   CT, June 2012.
;-
;
function greg2jul, month, day, year, hour, minute, second

  compile_opt idl2

  ON_ERROR, 2		; Return to caller if errors
  CATCH, err
  if (err ne 0) then begin
    CATCH, /CANCEL
    MESSAGE, !ERROR_STATE.msg
  endif

  case N_PARAMS() of
    0: result = JULDAY(/PROLEPTIC)
    1: result = JULDAY(month, /PROLEPTIC)
    2: result = JULDAY(month, day, /PROLEPTIC)
    3: result = JULDAY(month, day, year, /PROLEPTIC)
    4: result = JULDAY(month, day, year, hour, /PROLEPTIC)
    5: result = JULDAY(month, day, year, hour, minute, /PROLEPTIC)
    6: result = JULDAY(month, day, year, hour, minute, second, /PROLEPTIC)
  endcase
  return, result
end

