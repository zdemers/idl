; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/grib_list.pro#1 $
;
; Copyright (c) 2010, Research Systems, Inc.  All rights reserved.
;       Unauthorized reproduction prohibited.
;+
; :Description:
;    Lists the keys and if the DUMP keyword is set, type/value,
;    for the keys in a GRIB file.
;
; :Params:
;    filename
;       Input GRIB file
;    
;    record (optional)
;       The record to list
;
; :Keywords:
;    ALL
;       List all information 
;       
;    OUT
;       Variable to capture the string output
;       
;    FILTER
;       Strings to filter on * eq wild cards
; -

; +
; Add spaces in front and behind val
; ------------------------------------------------------------------------------
function _IDL_GRIB_format, val, pre, post
  compile_opt idl2,hidden
  on_error, 2
  
  if pre eq 0 then begin 
    strpre=''
  endif else begin
    strpre=string((bytarr(pre)+32b))
  endelse
  if post eq 0 then begin
    strpost=''
  endif else begin
    strpost=string((bytarr(post)+32b))
  endelse
  if strlen(val) eq 0 then begin
    val=''
  endif

  return,strpre+val+strpost
end

; +
; Make the print output pretty
; ------------------------------------------------------------------------------
pro _IDL_GRIB_print, output, DUMP=dump
  compile_opt idl2,hidden
  on_error,2
  
  spaces=5
  col0_max = max(strlen(output[0,*]))
  col1_max = max(strlen(output[1,*]))
  col2_max = max(strlen(output[2,*]))
  
  
  for i=0, n_elements(output[0,*])-1 do begin
    print,_IDL_GRIB_format(output[0,i],col0_max-strlen(output[0,i]),spaces), $
          _IDL_GRIB_format(output[1,i],0,col1_max-strlen(output[1,i])+spaces), $
          _IDL_GRIB_format(output[2,i],0,col2_max-strlen(output[2,i]))
  endfor
end

; +
; Extract the type/value from the key value
; ------------------------------------------------------------------------------
function _IDL_GRIB_help, IDL_GRIB_msg
  compile_opt idl2,hidden
  on_error, 2

  help,IDL_GRIB_msg, OUTPUT=out
  out = strtrim(strsplit(strmid(out,strlen('IDL_GRIB_MSG')),'=',/extract),2)
  
  if strpos(out[1],'Array') ge 0 then begin
    return,out[0]+strupcase(out[1])
  endif
  
  return, out[1]
end

;-------------------------------------------------------------------------------
function _grib_list, filename, record, ALL=all
  compile_opt idl2,hidden
  on_error, 2
  
  ; Sanity
  if (filename eq '') then message,'GRIB_LIST: Invalid input parameters.',/NONAME
  if isa(record) then if record le 0 then message,'GRIB_LIST: Invalid input parameters.',/NONAME
  
  f = grib_open(filename)
  h = grib_new_from_file(f)
  result = list()
  record_start=1
  kiter=!null
  
  ; If key fails, ignore and keep going
  catch, ierr
  if (ierr ne 0) then begin
    if strcmp(!error_state.msg,'GRIB Error: Invalid type',/fold_case) then begin
      result.add,tmp
      message,/reset
    endif else begin
      catch,/cancel
      message, !error_state.msg
    endelse
  endif
  
  
  if ~isa(record) && ~keyword_set(all) then begin
    while(h ne !null) do begin
      if result.IsEmpty() then begin
        tmp=[strtrim(record_start,2),'','']
      endif else begin
        tmp=[strtrim(fix((result[-1])[0])+1,2),'','']
      endelse
      
      kiter = grib_keys_iterator_new(h,/read_only)
      while grib_keys_iterator_next(kiter) do begin
        key = grib_keys_iterator_get_name(kiter)
        ; Key off shortName for a more human readable format
        ; strcmp(key, 'parameterName', /fold_case)
        if strcmp(key, 'shortName', /fold_case) then begin
          if (grib_get_size(h, key) gt 1) then begin
            tmp[1] = grib_get_array(h, key)
          endif else begin
            tmp[1] = grib_get(h,key)
          endelse
        endif else if strcmp(key, 'values', /fold_case) then begin
          if (grib_get_size(h, key) gt 1) then begin
            tmp[2] = _IDL_GRIB_help(grib_get_array(h, key))
          endif else begin
            tmp[2] = _IDL_GRIB_help(grib_get(h, key))
          endelse
        endif
      endwhile
      
      result.add,tmp
      grib_keys_iterator_delete, kiter
      grib_release, h
      h = grib_new_from_file(f)
    endwhile
  endif else if isa(record) && ~keyword_set(all) then begin
    while (record_start ne record) && (h ne !null) do begin
      grib_release, h
      h = grib_new_from_file(f)
      record_start++
    endwhile
    
    if (h ne !null) then begin
      if kiter eq !null then begin
        kiter = grib_keys_iterator_new(h,/all)
      endif
      while grib_keys_iterator_next(kiter) do begin
        key = grib_keys_iterator_get_name(kiter)
        tmp = [strtrim(record_start,2),strtrim(key,2),'']
        if (grib_get_size(h, key) gt 1) then begin
          tmp[2] = _IDL_GRIB_help(grib_get_array(h, key))
        endif else begin
          tmp[2] = _IDL_GRIB_help(grib_get(h, key))
        endelse
        result.add,tmp
      endwhile
      
      
      grib_keys_iterator_delete, kiter
      kiter=!null
      grib_release, h
      h = grib_new_from_file(f)
    endif
  endif else if keyword_set(all) then begin
    while(h ne !null) do begin
      ; Add key value pair information
      if kiter eq !null then begin
        kiter = grib_keys_iterator_new(h,/all)
      endif
      while grib_keys_iterator_next(kiter) do begin
        key = grib_keys_iterator_get_name(kiter)
        tmp = [strtrim(record_start,2),strtrim(key,2),'']
        if (grib_get_size(h, key) gt 1) then begin
          tmp[2] = _IDL_GRIB_help(grib_get_array(h, key))
        endif else begin
          tmp[2] = _IDL_GRIB_help(grib_get(h, key))
        endelse
        result.add,tmp
      endwhile
      
      grib_keys_iterator_delete, kiter
      kiter=!null
      grib_release, h
      h = grib_new_from_file(f)
      record_start++
    endwhile
  endif
  
  grib_close, f
  if result.Count() eq 0 then begin
    return, ['','','']
  endif
  return, transpose(result.toArray())
end

pro grib_list, filename, record, ALL=all, OUTPUT=out, FILTER=filter
  compile_opt idl2
  on_error,2
   
  out=_grib_list(filename,record,ALL=all)
  
  if isa(filter) then begin
    filterarr = strsplit(filter,'*',/extract)
    pos=strpos(out[1,*],filterarr[0])
    w=where(pos ge 0)
    
    for i=1, n_elements(filterarr)-1 do begin
      tmppos=strpos(out[1,*],filterarr[i])
      tmpw=where(tmppos ge pos)
      pos=tmppos

      ww=[]
      for i=0, n_elements(tmpw)-1 do begin
        forw=where(tmpw[i] eq w)
        if forw ge 0 then begin
          ww=[ww,w[forw]]
        endif
      endfor
      w=ww
    endfor
    
    if ~isa(w) || w[0] eq -1 then begin
      out=[]
    endif else begin
      out=out[*, w]
    endelse
  endif
  if ~arg_present(out) && isa(out) then begin
    _IDL_GRIB_print,out
  endif
end