pro amber_telluric_calibrate, file, wrange=wrange

read_amber, file, oiarray, oitarget,oiwavelength,oivis, oivis2,oit3, spec, /inventory

fxbopen, unit,file,7,header

tlines = [2.16347, 2.16869]

r = [0, 255, 255, 0, 0, 100, 100, 100, 0, 50,0]
g = [0, 255, 0, 255, 0,100, 0, 100, 100, 100,50]
b = [0, 255, 0, 0, 255,100, 100, 0, 100, 0, 100]
tvlct, r, g, b, 0

window, 0, xsize=1000, ysize=600
wave = *spec(0).wavelength/1.e-6

if not keyword_set(wrange) then wrange = [2.155, 2.185]
tot_flux = *spec(0).spectrum+*spec(1).spectrum+*spec(2).spectrum
cont = where(wave le 2.16 or wave ge 2.18)
cont_flux = mean(tot_flux[cont])
plot, wave, tot_flux/cont_flux, xrange = wrange, ytitle = "I/I!dC!n", color=0, background=255, yrange = [min(tot_flux/cont_flux), max(tot_flux/cont_flux)], /xstyle, position = [.09, .09, .95, .48]
for i = 0., n_elements(tlines) - 1. do oplot, [tlines[i], tlines[i]], [0,2], color=4

cursor, t1ws, t1fs, /down ; select telluric region
cursor, t1we, t1fe, /down
where_t1 = where(wave ge t1ws and wave le t1we)
print, where_t1
oplot, wave[where_t1], tot_flux[where_t1]/cont_flux, color=4, psym=2
t1fit = gaussfit(wave[where_t1], tot_flux[where_t1]/cont_flux, t1coeff) ;fit telluric

oplot, wave[where_t1], t1fit, color=2

cursor, t2ws, t2fs, /down ; select telluric region
cursor, t2we, t2fe, /down
where_t2 = where(wave ge t2ws and wave le t2we)
oplot, wave[where_t2], tot_flux[where_t2]/cont_flux, color=4, psym=2
t2fit = gaussfit(wave[where_t2], tot_flux[where_t2]/cont_flux, t2coeff) ;fit telluric

oplot, wave[where_t2], t2fit, color=2

t1min = where(t1fit eq min(t1fit))
t2min = where(t2fit eq min(t2fit))
t1wave = wave[where_t1[t1min]]
t2wave = wave[where_t2[t2min]]
t1diff = tlines[0] - t1wave
t2diff = tlines[1] - t2wave
wcorr = mean([t1diff, t2diff]) ;wavelength correction
wave = wave + wcorr
plot, wave, tot_flux/cont_flux, xrange = wrange, ytitle = "I/I!dC!n", color=0, background=255, yrange = [min(tot_flux/cont_flux), max(tot_flux/cont_flux)], /xstyle, position = [.09, .52, .95, .91], /noerase, xtickname = replicate(' ', 10)
for i = 0., n_elements(tlines) - 1. do oplot, [tlines[i], tlines[i]], [0,2], color=4

;write data
*oiwavelength.eff_wave = wave
interf_file = 'oi_'+file
spec_file = 'int_' + file
write_oidata, interf_file, oiarray, oitarget, oiwavelength, oivis, oivis2, oit3
spec_array = dblarr(5, n_elements(tot_flux))
spec_array[0,*] = wave
spec_array[1,*] = *spec(0).spectrum
spec_array[2,*] = *spec(1).spectrum
spec_array[3,*] = *spec(2).spectrum
spec_array[4,*] = tot_flux
mwrfits, spec_array, spec_file, header

end
