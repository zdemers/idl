; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/cdf_exists.pro#1 $
;
;
; Copyright (c) 1992-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+
; NAME:
;	CDF_EXISTS
;
; PURPOSE:
;	Test for the existence of the CDF library
;
; CATEGORY:
;	File Formats
;
; CALLING SEQUENCE:
;	Result = CDF_EXISTS()
;
; INPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; OUTPUTS:
;	Returns TRUE (1) if the CDF data format library is
;	supported. Returns FALSE(0) if it is not.
;
; EXAMPLE:
;	IF cdf_exists() EQ 0 THEN Fail,"CDF not supported on this machine"
;
; MODIFICATION HISTORY
;	Written by:	Joshua Goldstein,  12/8/92
;	Rewritten: CT, VIS, Jan 2012
;
;-
FUNCTION cdf_exists

  has_cdf = MAX(ROUTINE_INFO(/SYSTEM, /FUNCTIONS) eq 'CDF_INQUIRE')
	return, has_cdf

END
