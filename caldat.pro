; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/caldat.pro#1 $
;
; Copyright (c) 1992-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;

;+
; NAME:
;	CALDAT
;
; PURPOSE:
;	Return the calendar date and time given julian date.
;	This is the inverse of the function JULDAY.
; CATEGORY:
;	Misc.
;
; CALLING SEQUENCE:
;	CALDAT, Julian, Month, Day, Year, Hour, Minute, Second
;	See also: julday, the inverse of this function.
;
; INPUTS:
;	JULIAN contains the Julian Day Number (which begins at noon) of the
;	specified calendar date.  It should be a long integer.
; OUTPUTS:
;	(Trailing parameters may be omitted if not required.)
;	MONTH:	Number of the desired month (1 = January, ..., 12 = December).
;
;	DAY:	Number of day of the month.
;
;	YEAR:	Number of the desired year.
;
;	HOUR:	Hour of the day
;	Minute: Minute of the day
;	Second: Second (and fractions) of the day.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	Accuracy using IEEE double precision numbers is approximately
;	1/10000th of a second.
;
; MODIFICATION HISTORY:
;	Translated from "Numerical Recipies in C", by William H. Press,
;	Brian P. Flannery, Saul A. Teukolsky, and William T. Vetterling.
;	Cambridge University Press, 1988 (second printing).
;
;	DMS, July 1992.
;	DMS, April 1996, Added HOUR, MINUTE and SECOND keyword
;	AB, 7 December 1997, Generalized to handle array input.
;	AB, 3 January 2000, Make seconds output as DOUBLE in array output.
; CT, Nov 2006: For Hour/Min/Sec, tweak the input to make sure hours
;       and minutes are correct. Restrict hours to 0-23 & min to 0-59.
; CT, June 2012: Add undocumented PROLEPTIC_GREGORIAN, used by JUL2GREG.
;     Also rewrote the algorithm using integer arithmetic, for speed.
;-
;
pro CALDAT, julian, month, day, year, hour, minute, second, $
  PROLEPTIC_GREGORIAN=prolepticGregorian

COMPILE_OPT idl2

	ON_ERROR, 2		; Return to caller if errors

	nParam = N_PARAMS()
	IF (nParam LT 1) THEN MESSAGE,'Incorrect number of arguments.', $
	 NONAME=KEYWORD_SET(prolepticGregorian)

	min_julian = -31776
	max_julian = 1827933925
	minn = MIN(julian, MAX=maxx)
	IF (minn LT min_julian) OR (maxx GT max_julian) THEN MESSAGE, $
		'Value of Julian date is out of allowed range.', $
   NONAME=KEYWORD_SET(prolepticGregorian)

	igreg = 2299161L    ;Beginning of Gregorian calendar
	type = SIZE(julian, /TYPE)
	isFloat = type eq 4 || type eq 5 || type eq 6 || type eq 9
	julLong = isFloat ? FLOOR(julian + 0.5d) : julian
	minJul = MIN(julLong)
  
  jShift = julLong + 32082  ; shift back to 4800 BC
  
  if (minjul ge igreg || KEYWORD_SET(prolepticgregorian)) then begin

    jShift -= 38
    g400 = jShift/146097
    deltaG = jShift mod 146097
    c100 = ((deltaG/36524 + 1)*3)/4
    deltaC = deltaG - c100*36524
    year = g400*400 + c100*100

  endif else begin

    n = N_ELEMENTS(jShift)
    deltaC = jShift
    year = n gt 1 ? LONARR(n) : 0L

    gregChange = WHERE(julLong ge igreg, ngreg)

    if (ngreg gt 0) then begin
      js = jShift[gregChange] - 38
      g400 = js/146097
      deltaG = js mod 146097
      c100 = ((deltaG/36524 + 1)*3)/4
      deltaC[gregChange] = deltaG - c100*36524
      year[gregChange] = g400*400 + c100*100
    endif
    
  endelse

  b4 = deltaC/1461
  deltaB = TEMPORARY(deltaC) mod 1461
  a = (deltaB/365 + 1)*3/4
  deltaA = TEMPORARY(deltaB) - 365*a
    
  year += b4*4 + a
  month = (5*deltaA + 308)/153
  day = TEMPORARY(deltaA) - ((month + 2)*153)/5 + 123

  year = TEMPORARY(year) - 4800 + month/12
  isBC = (year le 0)
  if ~ARRAY_EQUAL(isBC, 0) then year -= isBC

  month = TEMPORARY(month) mod 12 + 1


; see if we need to do hours, minutes, seconds
	IF (nParam GT 4) THEN BEGIN
		fraction = julian + 0.5d - julLong
    eps = 1d-12 > 1d-12*ABS(Temporary(julLong))
		hour = 0 > Floor(fraction * 24d + eps) < 23
		fraction -= hour/24d
		minute = 0 > Floor(fraction*1440d + eps) < 59
		second = 0 > (TEMPORARY(fraction) - minute/1440d)*86400d
	ENDIF

; if julian is an array, reform all output to correct dimensions
	IF (SIZE(julian,/N_DIMENSION) GT 0) THEN BEGIN
		dimensions = SIZE(julian,/DIMENSION)
		month = REFORM(month,dimensions)
		day = REFORM(day,dimensions)
		year = REFORM(year,dimensions)
		IF (nParam GT 4) THEN BEGIN
			hour = REFORM(hour,dimensions)
			minute = REFORM(minute,dimensions)
			second = REFORM(second,dimensions)
		ENDIF
	ENDIF

END
