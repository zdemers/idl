; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/read_video.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
; READ_VIDEO
;
;-

;----------------------------------------------------------------------------
;+
; Helper object
;
; PURPOSE:
;      Creates an object for a video read handle object
;-
;----------------------------------------------------------------------------
function readvideohandle::Init, file, _EXTRA=_extra
  compile_opt idl2, hidden
  on_error, 2
  
  quiet = !quiet
  catch, err
  if (err ne 0) then begin
    !quiet = quiet
    catch, /CANCEL
    return, 0
  endif
  
  !quiet = 1
  if (SIZE(file, /TNAME) eq 'STRING') then begin
    self.oVid = OBJ_NEW('IDLffVideoRead', file[0])
    self.streams = PTR_NEW(self.oVid->GetStreams())
    self.nStreams = N_ELEMENTS(*self.streams)
  endif
  !quiet = quiet
  
  if (~OBJ_VALID(self.oVid)) then return, 0
  return, 1
  
end

;-----------------------------------------------------------------------------
; Clean up
;
pro readvideohandle::Cleanup, _EXTRA=_extra
  compile_opt idl2, hidden
  on_error, 2
  
  OBJ_DESTROY, self.oVid
  PTR_FREE, self.streams
  
end

;----------------------------------------------------------------------------
; Return nStreams
;
function readvideohandle::NStreams
  compile_opt idl2, hidden
  on_error, 2

  return, self.nStreams
end

;----------------------------------------------------------------------------
; Return streams
;
function readvideohandle::Streams
  compile_opt idl2, hidden
  on_error, 2
  
  return, *self.streams
end

;----------------------------------------------------------------------------
; Do the work of reading the data
;
function readvideohandle::Read, streamIndex, $
                      TIME_START=timeStartIn, TIME_END=timeEndIn, $
                      FRAME_START=frameStartIn, FRAME_END=frameEndIn, $
                      ALL=all, SUB_RECT=subrect, NODATA=nodataIn, $
                      TIME_OUT=timeOut, FRAME_OUT=frameOut

  compile_opt idl2, hidden
  on_error, 2
  
  if (N_ELEMENTS((*self.streams)) eq 0) then $
    return, !NULL    
  
  hasTime = KEYWORD_SET(timeStartIn) || KEYWORD_SET(timeEndIn)
  hasFrame = KEYWORD_SET(frameStartIn) || KEYWORD_SET(frameEndIn)
  
  nodata = KEYWORD_SET(nodataIn)
  
  if (hasTime) then begin
    startTime = KEYWORD_SET(timeStartIn) ? timeStartIn[0] > 0d : 0d
    endTime = KEYWORD_SET(timeEndIn) ? $
      timeEndIn[0] < (*self.streams)[streamIndex].length : startTime
  endif
  
  rate = (*self.streams)[streamIndex].rate
  if (~FINITE(rate)) then rate = 0.0
  if ((rate eq 0.0) && ((*self.streams)[streamIndex].length ne 0.0)) then begin
    rate = (*self.streams)[streamIndex].count / $
      (*self.streams)[streamIndex].length  
  endif
  if (rate eq 0.0) then rate = 1.0
  
  if (hasFrame) then begin
    ; Convert to time
    frameStart = KEYWORD_SET(frameStartIn) ? LONG(frameStartIn[0]) : 0l
    frameEnd = KEYWORD_SET(frameEndIn) ? LONG(frameEndIn[0]) : frameStart
    startTime = (frameStart) / rate > 0d
    endTime = (frameEnd) / rate < $
      (*self.streams)[streamIndex].length
  endif
  
  if (N_ELEMENTS(startTime) eq 0) then begin
    startTime = 0.0
  endif
  
  if (N_ELEMENTS(endTime) eq 0) then begin
    ; Only ask for one frame/packet
    endTime = startTime
  endif
  
  if (KEYWORD_SET(all)) then begin
    startTime = 0.0
    endTime = (*self.streams)[streamIndex].length
  endif
  
  catch, err
  if (err ne 0) then begin
    catch, /cancel
    message, 'Unable to read data from the file'
    return, !NULL
  endif
  
  type = (*self.streams)[streamIndex].type

  typeVideo = 1
  typeAudio = 2
  typeData = 3
  
  ; Create output variable
  if (~nodata) then begin
    case type of
      typeVideo : begin
        n = ROUND(rate * (endTime-startTime)) + 1 > 1
        if (N_ELEMENTS(subrect) ne 0) then begin
          width = subrect[2]-subrect[0]+1
          height = subrect[3]-subrect[1]+1
        endif else begin
          width = (*self.streams)[streamIndex].width
          height = (*self.streams)[streamIndex].height
        endelse
        out = BYTARR(3, width, height, n)
      end
      typeAudio : begin
        out = list()
      end
      typeData : begin
        out = list()
      end
    endcase
  endif
  
  ; Move pointer to a little bit before desired time for reference framing
  self.oVid->Seek, startTime-1
  
  done = 0b
  ctr = 0l
  timeOut = [!NULL]
  repeat begin
    data = self.oVid->GetNext(ONLY_STREAM=streamIndex, TIME=timeOuttmp)
    if (N_ELEMENTS(data) eq 1) then break
    ; Have we found the starting place?
    if (timeOuttmp ge startTime) then begin
      if (~nodata) then begin
        case type of
          typeVideo : begin
            if (N_ELEMENTS(subrect) ne 0) then begin
              out[*,*,*,ctr++] = $
                data[*,subrect[0]:subrect[2],subrect[1]:subrect[3]]
            endif else begin
              out[*,*,*,ctr++] = data
            endelse
          end
          typeAudio : begin
            out.add, data
            ctr++
          end
          typeData : begin
            out.add, data
            ctr++
          end
        endcase
      endif
      timeOut = [timeOut, timeOuttmp]
    endif
    ; Are we done yet?
    if (timeOuttmp ge endTime) then $
      done = 1b
  endrep until done
  
  if (N_ELEMENTS(timeOut) eq 1) then begin
    timeOut = timeOut[0]
  endif
  
  if (ARG_PRESENT(frameOut) && (type eq typeVideo)) then begin
    frameOut = LONG(timeOut * rate)
  endif
  
  if (ctr eq 0) then $
    return, !NULL
    
  ; Post process the data
  if (~nodata) then begin
    case type of
      typeVideo : outData = reform(out[*,*,*,0:ctr-1])
      typeAudio : outData = out[0:ctr-1].toArray(DIMENSION=2)
      typeData : outData = out[0:ctr-1].toArray()
    endcase
  endif
  
  return, outData
  
end

;-----------------------------------------------------------------------------
; Object definition
;
pro readvideohandle__define
  compile_opt idl2, hidden
  
  struct = {readvideohandle, $
    oVid: OBJ_NEW(), $
    streams: PTR_NEW(), $
    nStreams: 0l, $
    streamIndex: 0l $
  }
  
end

;----------------------------------------------------------------------------
;+
; Primary routine
; 
; NAME:
;      Read_Video
;-
;----------------------------------------------------------------------------
function read_video, fileIn, streamIn, $
                     ALL=allIn, TIME_START=timeStartIn, TIME_END=timeEndIn, $
                     FRAME_START=frameStartIn, FRAME_END=frameEndIn, $
                     TIME_OUT=timeOut, FRAME_OUT=frameOut, SUB_RECT=subRectIn, $
                     HANDLE=handleIn, CLOSE=closeIn, NODATA=nodataIn

  compile_opt idl2, hidden
  on_error, 2

  ; First things first, check for valid handle
  if (ISA(handleIn, 'readvideohandle')) then begin
    hasHandle = 1b
    oVid = handleIn
  endif else begin
    hasHandle = 0b
    oVid = OBJ_NEW('readvideohandle', fileIn)
    if (~OBJ_VALID(oVid)) then begin
      return, !NULL
    endif
  endelse
  
  typeVideo = 1
  
  streamstructs = oVid->Streams()
  if (N_ELEMENTS(streamIn) ne 0) then begin
    streamIndex = FIX(streamIn[0])
  endif else begin
    ; Find first video stream
    streamIndex = (WHERE(streamstructs.type eq typeVideo))[0]
  endelse
  if ((streamIndex lt 0) || (streamIndex ge oVid->NStreams())) then begin
    message, 'No matching stream found'
    return, !NULL
  endif
  
  hasTime = KEYWORD_SET(timeStartIn) || KEYWORD_SET(timeEndIn)
  hasFrame = KEYWORD_SET(frameStartIn) || KEYWORD_SET(frameEndIn)
  
  if (hasTime && hasFrame) then begin
    message, 'Time and Frame cannot be set simultaneously'
    return, !NULL
  endif
  
  type = streamstructs[streamIndex].type
  
  if (hasFrame && (type ne typeVideo)) then begin
    message, 'Frames can only be specified for VIDEO'
    return, !NULL
  endif

  if (N_ELEMENTS(subrectIn) ne 0) then begin
    msg = ''
    if (type ne typeVideo) then $
      msg = 'SUBRECT can only be specified for reading video'
    if (N_ELEMENTS(subrectIn) ne 4) then $
      msg = 'SUBRECT must have 4 elements: [X0, Y0, X1, Y1]'
    wh = WHERE(subrectIn lt 0, cnt)
    if (cnt ne 0) then $
      msg = 'SUBRECT must be all positive values'
    subrect = ULONG(subrectIn)
    if (subrect[0] gt subrect[2]) then $ ; swap places
      subrect[2] xor= (subrect[0] xor= (subrect[2] xor= subrect[0]))
    if (subrect[1] gt subrect[3]) then $ ; swap places
      subrect[3] xor= (subrect[1] xor= (subrect[3] xor= subrect[1]))
    if (subrect[2] ge streamstructs[streamIndex].width) then $
      msg = 'Invalid SUBRECT range'
    if (subrect[3] ge streamstructs[streamIndex].height) then $
      msg = 'Invalid SUBRECT range'
    if (msg ne '') then begin
      message, msg
      return, !NULL
    endif
  endif
  
  ; Temporary catch block around read call
  catch, err
  if (err ne 0) then begin
    catch, /cancel
    message, 'Unable to read data'
    return, !NULL
  endif
  
  data = oVid->Read(streamIndex, TIME_START=timeStartIn, TIME_END=timeEndIn, $
                    FRAME_START=frameStartIn, FRAME_END=frameEndIn, $
                    ALL=allIn, SUB_RECT=subrect, NODATA=nodataIn, $
                    TIME_OUT=timeOut, FRAME_OUT=frameOut)
                    
  catch, /cancel
  if ((N_ELEMENTS(data) eq 0) && ~KEYWORD_SET(nodataIn)) then begin
    message, 'Unable to read data'
    return, !NULL
  endif

  ; Close video object if not needed anymore                     
  if (KEYWORD_SET(closeIn)) then begin
    OBJ_DESTROY, oVid
  endif
  if (~ARG_PRESENT(handleIn) && (hasHandle eq 0)) then begin
    OBJ_DESTROY, oVid
  endif
  
  ; Return handle if requested
  if ((hasHandle eq 0) && ARG_PRESENT(handleIn) && OBJ_VALID(oVid)) then begin
    handleIn = oVid
  endif
  
  return, data
  
end
