; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/query_video.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; NAME:
; QUERY_VIDEO
;
;-

function query_video, file, info, $
                      METADATA=metadata, STREAMS=streams, $
                      CODECS_READ=codRead, CODECS_WRITE=codWrite, $
                      FORMATS_READ=formRead, FORMATS_WRITE=formWrite, $
                      VIDEO=video, AUDIO=audio

  compile_opt idl2, hidden
  on_error, 2

  if (ARG_PRESENT(codWrite) || ARG_PRESENT(formWrite)) then begin
    oVid = OBJ_NEW('IDLffVideoWrite')
    codWrite = oVid->GetCodecs(VIDEO=KEYWORD_SET(video), $
      AUDIO=KEYWORD_SET(audio))
    formWrite = oVid->GetFormats()
    OBJ_DESTROY, oVid
  endif

  ; Yes this might cause the ff object to be created twice, but the user might
  ; not supply a file or might supply a non-video file so just get this info
  ; now. No one should be asking for the codecs or formats more than once so 
  ; this should happen at most only once per session.
  if (ARG_PRESENT(codRead) || ARG_PRESENT(formRead)) then begin
    ; suppress bad codec messages
    quiet = !quiet
    !quiet = 1
    oVid = OBJ_NEW('IDLffVideoRead')
    codRead = oVid->GetCodecs(VIDEO=KEYWORD_SET(video), $
      AUDIO=KEYWORD_SET(audio))
    formRead = oVid->GetFormats()
    OBJ_DESTROY, oVid
    !quiet = quiet
  endif

  ; Do we have at least a single readable file?
  if ((N_ELEMENTS(file) ne 1) || (SIZE(file, /TNAME) ne 'STRING')) then $
    return, 0
  if (~FILE_TEST(file, /READ)) then $
    return, 0

  catch, err
  if (err ne 0) then begin
    catch, /cancel
    if (OBJ_VALID(oVid)) then OBJ_DESTROY, oVid
    return, 0
  endif
  
  ; suppress bad codec messages
  quiet = !quiet
  !quiet = 1
  oVid = OBJ_NEW('IDLffVideoRead', file)
  !quiet = quiet
  if (~OBJ_VALID(oVid)) then $
    return, 0

  if (ARG_PRESENT(metadata)) then begin
    metadata = !NULL
    keys = oVid->GetMetadata(/KEYS)
    n = N_ELEMENTS(keys)
    if (n ne 0) then begin
      metadata = STRARR(2,n)
      for i=0,n-1 do begin
        value = oVid->GetMetadata(keys[i])
        metadata[*,i] = [keys[i], value]
      endfor
    endif
  endif

  streams = oVid->GetStreams()
  if (N_ELEMENTS(streams) eq 0) then $
    return, 0
  ; Ensure file has at least one valid video stream that can be read
  firstStream = -1
  for i=0,N_ELEMENTS(streams)-1 do begin
    if ((streams[i].codec ne 'Unknown') && (streams[i].type eq 1)) then begin
      firstStream = i
      break
    endif
  endfor
  if (firstStream lt 0) then $
    return, 0

  if (ARG_PRESENT(info)) then begin
    info = {DIMENSIONS:[streams[firstStream].width, $
                        streams[firstStream].height], $
            TYPE:'VIDEO', $
            NUM_FRAMES:streams[firstStream].count, $
            LENGTH:streams[firstStream].length, $
            CODEC:streams[firstStream].codec, $
            RATE:streams[firstStream].rate}
  endif
  
  OBJ_DESTROY, oVid
  
  return, 1
  
end
