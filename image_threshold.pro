; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/image_threshold.pro#1 $
; Copyright (c) 2010-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;+
; :Description:
;    Generates and applies a threshold for an image
;
; :Params:
;    data - A single image or stack of images to be processed
;
; :Keywords:
;    THRESHOLD
;       Captures the value of the threshold.
;    
;    BYIMAGE
;       If a stack is passed in, the stack can either be processed by image or by stack.
;    
;    INTERLEAVE
;       The dimensions of the index in a stack of images.  The default is interleave=0
;    
;    INVERT
;       Applies an inverse to image before processing.  Useful for dark images or when the algorithm should pull out bright spots.
;    
;    MASK_INVERT
;       Normally on return any values with 0 represent values below the threshold and 1 represents values above the threshold.  Set this keyword to flip this.
;    
;    HISTMIN
;       The minimum value to consider in the image.
;    
;    HISTMAX
;       The maximum value to consider in the image.
;    
;    ISODATA
;       Applies the isodata algorithm (default).
;
; :Notes:
;   Ported to IDL from ImageJ plugin
;-

; Based on the Kittler-Illingworth algorithm [Kittler, J & Illingworth, J (1986), 
; "Minimum error thresholding", Pattern Recognition 19: 41-47] 
;-------------------------------------------------------------------------------
function IDLthreshold_mean, data
  compile_opt idl2,hidden

  threshold = -1
  tot = total(data,/double)
  sum = total(dindgen(n_elements(data))*data,/double)

  return,floor(sum/tot)
end

; Helper function2 for MINERROR
;-------------------------------------------------------------------------------
function _IDLthreshold_A, y, j
  compile_opt idl2,hidden
  
  return,total(y[0:j],/double)
end

function _IDLthreshold_B, y, j
  compile_opt idl2,hidden
  
  return,total(dindgen(j+1)*y[0:j],/double)
end

function _IDLthreshold_C, y, j
  compile_opt idl2,hidden
  
  return,total(dindgen(j+1)*dindgen(j+1)*y[0:j],/double)
end

; Based on the Kittler-Illingworth algorithm [Kittler, J & Illingworth, J (1986), 
; "Minimum error thresholding", Pattern Recognition 19: 41-47] 
;-------------------------------------------------------------------------------
function IDLthreshold_minerror, data
  compile_opt idl2,hidden

  threshold = IDLthreshold_mean(data)
  Tprev = -2
  
  while threshold ne Tprev do begin
    mu = _IDLthreshold_B(data,threshold) / _IDLthreshold_A(data,threshold)
    nu = (_IDLthreshold_B(data,n_elements(data)-1) - _IDLthreshold_B(data,threshold)) / (_IDLthreshold_A(data,n_elements(data)-1) - _IDLthreshold_A(data,threshold))
    p = _IDLthreshold_A(data,threshold) / _IDLthreshold_A(data,n_elements(data)-1)
    q = (_IDLthreshold_A(data,n_elements(data)-1) - _IDLthreshold_A(data,threshold)) / _IDLthreshold_A(data,n_elements(data)-1)
    sigma2 = _IDLthreshold_C(data,threshold) / _IDLthreshold_A(data,threshold) - (mu*mu)
    tau2 = (_IDLthreshold_C(data,n_elements(data)-1) - _IDLthreshold_C(data,threshold)) / (_IDLthreshold_A(data,n_elements(data)-1) - _IDLthreshold_A(data,threshold)) - (nu*nu)
    
    w0 = 1.0/sigma2 - 1.0/tau2
    w1 = mu/sigma2 - nu/tau2
    w2 = (mu*mu)/sigma2 - (nu*nu)/tau2 + alog10((sigma2*(q*q)) / (tau2*(p*p)))
    
    sqterm = w1*w1 - w0*w2
    if sqterm lt 0 then begin
      message,/noname, "IMAGE_THRESHOLD: MINERROR: not converging."
      return,threshold
    endif
    
    Tprev = threshold
    temp = (w1+sqrt(sqterm))/w0
    
    if ~finite(temp) then begin
      message,/noname,"IMAGE_THRESHOLD: MINERROR: not converging."
      threshold=Tprev
    endif else begin
      threshold = floor(temp)
    endelse
    
  end

  return,threshold
end

; Based on the Kapur-Sahoo-Wong algorithm [Kapur, JN; Sahoo, PK & Wong, ACK 
; (1985), "A New Method for Gray-Level Picture Thresholding Using the Entropy 
; of the Histogram", Graphical Models and Image Processing 29(3): 273-285] 
;-------------------------------------------------------------------------------
function IDLthreshold_maxentropy, data
  compile_opt idl2,hidden
  
  total = total(data,/double)
  norm_histo = data/total
  threshold = -1
  
  P1=dblarr(n_elements(data))
  P2=dblarr(n_elements(data))
  P1[0]=norm_histo[0]
  P2[0]=1.0-P1[0]
  for i=1, n_elements(data)-1 do begin
    P1[i] = P1[i-1] + norm_histo[i]
    P2[i] = 1.0 - P1[i]
  endfor
  
  w=where(~(abs(P1) lt (machar(/double)).eps))
  first_bin = w[0]
  
  last_bin = n_elements(data) - 1
  w=where(~(abs(P2) lt (machar(/double)).eps), count)
  last_bin = (count gt 0) ? w[-1] : last_bin
  
  max_ent = (machar(/double)).xmin
  for it = first_bin, last_bin do begin
    ent_back = 0.0
    for ih = 0, it do begin
      if data[ih] ne 0 then begin
        ent_back -= (norm_histo[ih] / P1[it]) * alog(norm_histo[ih] / P1[it])
      endif
    endfor 
    
    ent_obj = 0.0
    for ih=it+1, n_elements(data)-1 do begin
      if data[ih] ne 0 then begin
        ent_obj -= (norm_histo[ih] / P2[it]) * alog(norm_histo[ih] / P2[it])
      endif
    endfor
    
    tot_ent = ent_back + ent_obj
    
    if max_ent lt tot_ent then begin
      max_ent = tot_ent
      threshold = it
    endif
  endfor

  return,threshold
end

; Based on the Tsai algorithm [Tsai, W (1985), "Moment-preserving thresholding: 
; a new approach", Computer Vision, Graphics, and Image Processing 29: 377-393] 
;-------------------------------------------------------------------------------
function IDLthreshold_moments, data
  compile_opt idl2,hidden
  
  total = total(data,/double)
  histo = data/total
  l = n_elements(data)
  
  m1 = total(dindgen(l) * histo,/double)
  m2 = total(dindgen(l) * dindgen(l) * histo,/double)
  m3 = total(dindgen(l) * dindgen(l) * dindgen(l) * histo,/double)
  
  cd = 1.0 * m2 - m1 * m1
  c0 = ( -m2 * m2 + m1 * m3 ) / cd
  c1 = ( 1.0 * (-m3) + m2 * m1 ) / cd
  z0 = 0.5 * ( -c1 - sqrt ( c1 * c1 - 4.0 * c0 ) )
  z1 = 0.5 * ( -c1 + sqrt ( c1 * c1 - 4.0 * c0 ) )
  p0 = ( z1 - m1 ) / ( z1 - z0 ) 
  
  sum = total(histo,/cumulative,/double)
  threshold = where(sum gt p0)

  return,threshold[0]
end

; Based on the Otsu algorithm [Otsu, N (1979), "A threshold selection method 
; from gray-level histograms", IEEE Trans. Sys., Man., Cyber. 9: 62-66, 
; doi:10.1109/TSMC.1979.4310076] 
;-------------------------------------------------------------------------------
function IDLthreshold_otsu, data
  compile_opt idl2,hidden
  
  N = total(data,/double)
  S = total(dindgen(N)*data,/double)
  
  N1 = total(data,/cumulative,/double) + data[0]
  Sk = total(dindgen(N)*data,/cumulative,/double)
  
  denom = double(N1) * (N-N1)
  w = where(denom ne 0)
  num = (N1[w]/N) * S[w] - Sk[w]
  BCV = dindgen(N)*0
  BCV[w] = (num*num)/denom[w]
  !null = max(BCV,threshold)

  return,threshold
end

; Based on the isodata algorithm [T.W. Ridler, S. Calvard, Picture thresholding 
; using an iterative selection method, IEEE Trans. System, Man and Cybernetics, 
; SMC-8 (1978) 630-632.] 
;-------------------------------------------------------------------------------
function IDLthreshold_isodata, data
  compile_opt idl2,hidden
  
  w = where(data gt 0, c)
  if c gt 0 then threshold=w[0]+1
  
  while 1 eq 1 do begin
    l=total(data[0:threshold-1]*lindgen(threshold))
    tl=total(data[0:threshold-1])
    u=total(data[threshold+1:*]*(lindgen(threshold)+threshold+1))
    tu=total(data[threshold+1:*])
    
    if tl gt 0 && tu gt 0 then begin
      l /= tl
      u /= tu
      if threshold eq round((l+u)/2d) then break
    endif 
    
    threshold++
    if threshold gt (n_elements(data)-2) then begin
      return, -1
    end
  end
  
  return,threshold
end

;-------------------------------------------------------------------------------
function IDLthreshold_applyThreshold,data,thresh, MASK_INVERT=mask_invert
  compile_opt idl2,hidden
  
  if keyword_set(mask_invert) then begin
    return, data le thresh
  endif else begin
    return, data gt thresh
  endelse
end

;-------------------------------------------------------------------------------
function IDLthreshold_applyhistogram, data, HISTMAX=histmax, $
                                            HISTMIN=histmin, $
                                            NOBLACK=noblack, $
                                            NOWHITE=nowhite
  compile_opt idl2,hidden
  
  h=histogram(data)
  
  if isa(noblack) then h[0]=0
  if isa(nowhite) then h[-1]=0
  if isa(histmax) then h[histmax:*]=0
  if isa(histmin) then h[0:histmin]=0
  
  return,h
end

;-------------------------------------------------------------------------------
function IDLthreshold_validateInput, ISODATA=isodata,       $
                                     OTSU=otsu,             $
                                     Moments=moments,       $
                                     MAXENTROPY=maxentropy, $
                                     MINERROR=minerror,     $
                                     MEAN=mean
  compile_opt idl2, hidden
  
  a = []
  
  ; Algorithm list, if it exists, add it to the list
  if keyword_set(isodata) then a=[a,'isodata']
  if keyword_set(otsu) then a=[a,'otsu']
  if keyword_set(moments) then a=[a,'moments']
  if keyword_set(maxentropy) then a=[a,'maxentropy']
  if keyword_set(minerror) then a=[a,'minerror']
  if keyword_set(mean) then a=[a,'mean']
  
  ; Default case
  if a eq !null then a='isodata'
  return, a
end

;-------------------------------------------------------------------------------
function IDLthreshold_internal, data, THRESHOLD=threshold,     $
                                      INVERT=invert,           $
                                      BYIMAGE=byimage,         $
                                      MASK_INVERT=mask_invert, $
                                      HISTMAX=histmax,         $
                                      HISTMIN=histmin,         $
                                      NOBLACK=noblack,         $
                                      NOWHITE=nowhite,         $
                                      ISODATA=isodata,         $
                                      OTSU=otsu,               $
                                      MOMENTS=moments,         $
                                      MAXENTROPY=maxentropy,   $
                                      MINERROR=minerror,       $
                                      MEAN=mean
  compile_opt idl2, hidden
  
  ; Verify input and set the algorithm
  algorithm = IDLthreshold_validateInput(ISODATA=isodata,       $
                                         OTSU=otsu,             $
                                         MOMENTS=moments,       $
                                         MAXENTROPY=maxentropy, $
                                         MINERROR=minerror,     $
                                         MEAN=mean)
  if n_elements(algorithm) gt 1 then message,'IMAGE_THRESHOLD: Keywords ' + strjoin(algorithm,', ') + ' are mutually exclusive.',/NONAME
  
  ; Initialize variables
  threshold=[]
  result=[]
  l=n_elements(data[0,0,*])
  h=list()
  
  ; Histogram the image, either by stack or by image
  for i=0,l-1 do begin
    if keyword_set(invert) then begin
      if keyword_set(byimage) then begin
        if keyword_set(noblack) then begin
          h.add,IDLthreshold_applyhistogram(255 - data[*,*,i],HISTMAX=histmax,HISTMIN=histmin,/NOWHITE)
        endif else if keyword_set(nowhite) then begin
          h.add,IDLthreshold_applyhistogram(255 - data[*,*,i],HISTMAX=histmax,HISTMIN=histmin,/NOBLACK)
        endif else begin
          h.add,IDLthreshold_applyhistogram(255 - data[*,*,i],HISTMAX=histmax,HISTMIN=histmin)
        endelse
      endif else begin
        if keyword_set(noblack) then begin
          h.add,IDLthreshold_applyhistogram(255 - data,HISTMAX=histmax,HISTMIN=histmin,/NOWHITE)
        endif else if keyword_set(nowhite) then begin
          h.add,IDLthreshold_applyhistogram(255 - data,HISTMAX=histmax,HISTMIN=histmin,/NOBLACK)
        endif else begin
          h.add,IDLthreshold_applyhistogram(255 - data,HISTMAX=histmax,HISTMIN=histmin)
        endelse
      endelse
    endif else begin
      if keyword_set(byimage) then begin
        if keyword_set(noblack) then begin
          h.add,IDLthreshold_applyhistogram(data[*,*,i],HISTMAX=histmax,HISTMIN=histmin,/NOBLACK)
        endif else if keyword_set(nowhite) then begin
          h.add,IDLthreshold_applyhistogram(data[*,*,i],HISTMAX=histmax,HISTMIN=histmin,/NOWHITE)
        endif else begin
          h.add,IDLthreshold_applyhistogram(data[*,*,i],HISTMAX=histmax,HISTMIN=histmin)
        endelse
      endif else begin
        if keyword_set(noblack) then begin
          h.add,IDLthreshold_applyhistogram(data,HISTMAX=histmax,HISTMIN=histmin,/NOBLACK)
        endif else if keyword_set(nowhite) then begin
          h.add,IDLthreshold_applyhistogram(data,HISTMAX=histmax,HISTMIN=histmin,/NOWHITE)
        endif else begin
          h.add,IDLthreshold_applyhistogram(data,HISTMAX=histmax,HISTMIN=histmin)
        endelse
      endelse
    endelse
  endfor
    
  ; Generate the threshold and generate a mask
  for i=0, l-1 do begin
    case algorithm of
      'isodata': begin
        threshold = [threshold,IDLthreshold_isodata(h[i])]
      end
      'otsu': begin
        threshold = [threshold,IDLthreshold_otsu(h[i])]
      end
      'moments': begin
        threshold = [threshold,IDLthreshold_moments(h[i])]
      end
      'maxentropy': begin
        threshold = [threshold,IDLthreshold_maxentropy(h[i])]
      end
      'minerror': begin
        threshold = [threshold,IDLthreshold_minerror(h[i])]
      end
      'mean': begin
        threshold = [threshold,IDLthreshold_mean(h[i])]
      end
    endcase
    result=[[[result]],[[IDLthreshold_applyThreshold(data[*,*,i],threshold[i],MASK_INVERT=mask_invert)]]]
  endfor

  return, result
end

;-------------------------------------------------------------------------------
function image_threshold, data, INTERLEAVE=interleave, _REF_EXTRA=_extra
  compile_opt idl2, hidden
  
  if ~isa(data) then message,'Incorrect number of arguments.'
  if ~isa(data,/NUMBER) then message,'Incorrect input format.'
  if size(data,/N_DIMENSIONS) gt 3 then message,'Incorrect dimensions of data.'
  if ~keyword_set(interleave) then interleave=0
  if size(data,/N_DIMENSIONS) le 2 then interleave=2
  
  case (interleave) of
    0: begin
      if ~isa(data,'byte') then begin
        return, transpose(IDLthreshold_internal(transpose(bytscl(data),[1,2,0]),_EXTRA=_extra),[2,0,1])
      endif else begin
        return, transpose(IDLthreshold_internal(transpose(data,[1,2,0]),_EXTRA=_extra),[2,0,1])
      endelse
    end
    1: begin
      if ~isa(data,'byte') then begin
        return, transpose(IDLthreshold_internal(transpose(bytscl(data),[0,2,1]),_EXTRA=_extra),[0,2,1])
      endif else begin
        return, transpose(IDLthreshold_internal(transpose(data,[0,2,1]),_EXTRA=_extra),[0,2,1])
      endelse
    end
    2: begin
      if ~isa(data,'byte') then begin
        return, IDLthreshold_internal(bytscl(data),_EXTRA=_extra)
      endif else begin
        return, IDLthreshold_internal(data,_EXTRA=_extra)
      endelse
    end
    else: begin
      message,'Invalid value for interleave.'
    end
  endcase

end