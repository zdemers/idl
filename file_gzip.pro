; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/file_gzip.pro#1 $
;
; Copyright (c) 2013-2013, Exelis Visual Information Solutions, Inc. All
;       rights reserved. Unauthorized reproduction is prohibited.
;
;+

;---------------------------------------------------------------------------
pro FILE_GZIP_COMPRESS, fileIn, fileOut, $
  BUFFER=buffer, $
  CLOSE=doClose, $
  COUNT=count, $
  DEBUG=debug, $
  DELETE=delete, $
  NBYTES=fileNbytes, $
  OFFSET=fileOffset, $
  UNCOMPRESS=uncompress, $
  VERBOSE=verbose
  
  compile_opt idl2, hidden

  common file_gzip_common, hashFileIn, hashFileOut

  if (~KEYWORD_SET(debug)) then ON_ERROR, 2

  lunIn = !null
  lunOut = !null
  
  keepOpen = ISA(doClose) && ~KEYWORD_SET(doClose)

  if (ISA(fileIn, 'STRING') && ISA(hashFileIn, 'HASH')) then begin
    if (hashFileIn.HasKey(fileIn)) then begin
      lunIn = hashFileIn[fileIn]
    endif
  endif

  if (ISA(fileOut, 'STRING') && ISA(hashFileOut, 'HASH')) then begin
    if (hashFileOut.HasKey(fileOut)) then begin
      lunOut = hashFileOut[fileOut]
    endif
  endif

  if (KEYWORD_SET(doClose)) then begin
    count = 0L
    buffer = 0b

    if (ISA(lunIn)) then begin
      hashFileIn.Remove, fileIn
      FREE_LUN, lunIn, /FORCE
    endif
    if (ISA(hashFileIn, 'HASH') && hashFileIn.IsEmpty()) then $
      OBJ_DESTROY, hashFileIn

    if (ISA(lunOut)) then begin
      hashFileOut.Remove, fileOut
      FREE_LUN, lunOut, /FORCE
    endif
    if (ISA(hashFileOut, 'HASH') && hashFileOut.IsEmpty()) then $
      OBJ_DESTROY, hashFileOut

    return
  endif
  
  if (~ISA(lunIn) && ~FILE_TEST(fileIn, /REGULAR, /READ)) then $
      MESSAGE, /NONAME, 'Unable to read file: "' + fileIn + '"'
    
  if (~KEYWORD_SET(debug)) then begin
    CATCH, iErr
    if (iErr ne 0) then begin
      CATCH, /CANCEL
      if (ISA(lunIn)) then FREE_LUN, lunIn, /FORCE
      if (ISA(lunOut)) then FREE_LUN, lunOut, /FORCE
      MESSAGE, /NONAME, 'Unable to write file: "' + fileOut + '"'
    endif
  endif


  if (ISA(buffer)) then begin

    ; Need to determine the original number of uncompressed bytes.
    if (KEYWORD_SET(uncompress) && $
      (~ISA(fileOffset) && ~ISA(fileNbytes))) then begin
      OPENR, lunInTmp, fileIn, /GET_LUN, /SWAP_IF_BIG_ENDIAN
      infoIn = FSTAT(lunInTmp)
      POINT_LUN, lunInTmp, infoIn.size - 4
      fileNbytes = 0ul
      READU, lunInTmp, fileNbytes
      FREE_LUN, lunInTmp, /FORCE
    endif

    if (~ISA(lunIn)) then begin
      OPENR, lunIn, fileIn, /GET_LUN, COMPRESS=KEYWORD_SET(uncompress)
    endif

    if (ISA(fileOffset)) then begin
      POINT_LUN, lunIn, fileOffset
    endif

    ; Need to determine the number of bytes to compress.
    if (~KEYWORD_SET(uncompress) && ~ISA(fileNbytes)) then begin
      infoIn = FSTAT(lunIn)
      fileNbytes = infoIn.size
      if (ISA(fileOffset)) then $
        fileNbytes = (fileNbytes - fileOffset) > 0
    endif

    buffer = []
    count = 0L
    if (fileNbytes gt 0) then begin
      buffer = BYTARR(fileNbytes, /NOZERO)
      POINT_LUN, -lunIn, currentLun
      CATCH, iErr
      if (iErr ne 0) then begin
        CATCH, /CANCEL
      endif else begin
        READU, lunIn, buffer
      endelse
      POINT_LUN, -lunIn, newLun
      count = newLun - currentLun
      if (count lt N_ELEMENTS(buffer)) then $
        buffer = (count gt 0) ? buffer[0:count-1] : 0b
    endif

    if (keepOpen) then begin
      if (~ISA(hashFileIn)) then hashFileIn = HASH()
      hashFileIn[fileIn] = lunIn
    endif else begin
      FREE_LUN, lunIn, /FORCE
    endelse
    lunIn = 0

    if (~KEYWORD_SET(uncompress)) then begin
      buffer = ZLIB_COMPRESS(buffer, /GZIP_HEADER)
    endif

    infoOut = {size: N_ELEMENTS(buffer)}

  endif else begin

    if (~ISA(lunIn)) then begin
      OPENR, lunIn, fileIn, /GET_LUN, COMPRESS=KEYWORD_SET(uncompress)
    endif

    if (~ISA(lunOut)) then begin
      OPENW, lunOut, fileOut, /GET_LUN, COMPRESS=~KEYWORD_SET(uncompress)
    endif

    if (ISA(fileOffset)) then begin
      POINT_LUN, lunIn, fileOffset
    endif

    if (ISA(fileNbytes)) then begin
      POINT_LUN, -lunOut, currentLun
      CATCH, iErr
      if (iErr ne 0) then begin
        CATCH, /CANCEL
      endif else begin
        COPY_LUN, lunIn, lunOut, fileNbytes
      endelse
      POINT_LUN, -lunOut, newLun
      count = newLun - currentLun
    endif else begin
      COPY_LUN, lunIn, lunOut, /EOF, TRANSFER_COUNT=count
    endelse

    if (keepOpen) then begin
      if (~ISA(hashFileIn)) then hashFileIn = HASH()
      hashFileIn[fileIn] = lunIn
      if (~ISA(hashFileOut)) then hashFileOut = HASH()
      hashFileOut[fileOut] = lunOut
    endif else begin
      FREE_LUN, lunIn, /FORCE
      FREE_LUN, lunOut, /FORCE
    endelse
    lunIn = 0
    lunOut = 0
    
  endelse


  if (KEYWORD_SET(delete) && ~keepOpen) then $
    FILE_DELETE, fileIn
    
  if (KEYWORD_SET(verbose)) then begin
    infoIn = FILE_INFO(fileIn)
    infoOut = FILE_INFO(fileOut)
    if (KEYWORD_SET(uncompress)) then begin
      countStr = '  ' + STRTRIM(count,2) + ' bytes'
      MESSAGE, /INFO, /NONAME, 'Uncompress ' + fileIn + countStr
    endif else begin
      spaceSavings = 100 - 100.0*infoOut.size/(infoIn.size > 1)
      spaceSavings = 0 > ROUND(spaceSavings*10)/10. < 100
      spaceSavings = STRTRIM(STRING(spaceSavings, FORMAT='(F5.1)'),2)
      MESSAGE, /INFO, /NONAME, 'Compress ' + fileIn + '  ' + spaceSavings + '%'      
    endelse
  endif
end


;---------------------------------------------------------------------------
pro FILE_GZIP, filesIn, filesOut, $
  BUFFER=buffer, $
  CLOSE=doClose, $
  COUNT=count, $
  DEBUG=debug, $
  DELETE=delete, $
  NBYTES=fileNbytes, $
  OFFSET=fileOffset, $
  UNCOMPRESS=uncompress, $
  VERBOSE=verbose

  compile_opt idl2, hidden
  
  if (~KEYWORD_SET(debug)) then $
    ON_ERROR, 2

  if (N_PARAMS() eq 0) then $
    MESSAGE, 'Incorrect number of arguments.'

  hasBuffer = ARG_PRESENT(buffer)
  if (N_PARAMS() eq 2 && hasBuffer) then $
    MESSAGE, 'BUFFER keyword can not be used with FileOut argument.'

  if (ISA(buffer) && ~hasBuffer) then $
    MESSAGE, 'BUFFER must be set to a named variable, not an expression.'

  nIn = N_ELEMENTS(filesIn)
  nOut = N_ELEMENTS(filesOut)
  if (N_PARAMS() eq 2 && nIn ne nOut) then $
    MESSAGE, 'Input and output file names must have the same number of elements.'

  if (nIn gt 1) then begin
    if (hasBuffer) then $
      MESSAGE, 'Multiple files can not be used with BUFFER.'
    if (ISA(fileNbytes) || ISA(fileOffset)) then $
      MESSAGE, 'Multiple files can not be used with NBYTES or OFFSET.'
  endif

  if (hasBuffer && ISA(doClose) && ~KEYWORD_SET(uncompress)) then $
    MESSAGE, 'BUFFER keyword can not be used with CLOSE.'

  count = 0L
  if (hasBuffer) then begin
    buffer = 0b
  endif

  foreach file, filesIn, i do begin
    if (nOut eq 0) then begin
      if (KEYWORD_SET(uncompress)) then begin
        fileOut = FILE_DIRNAME(file, /MARK_DIRECTORY) + FILE_BASENAME(file, '.gz')
      endif else begin
        fileOut = file + '.gz'
      endelse      
    endif else begin
      fileOut = filesOut[i]
    endelse
    FILE_GZIP_COMPRESS, file, fileOut, $
      CLOSE=doClose, $
      COUNT=count1, $
      NBYTES=fileNbytes, $
      OFFSET=fileOffset, $
      BUFFER=buffer, DEBUG=debug, DELETE=delete, $
      UNCOMPRESS=uncompress, VERBOSE=verbose
    count = (i gt 0) ? [count, count1] : count1
  endforeach

end

