; $Id: //depot/Release/ENVI51_IDL83/idl/idldir/lib/derivsig.pro#1 $
;
;+
; NAME:
;	DERIVSIG
;
; PURPOSE:
;	This function computes the standard deviation of a derivative
;       as found by the DERIV function, using the input variables of
;	DERIV and the standard deviations of those input variables.
;
; CATEGORY:
;	Numerical analysis.
;
; CALLING SEQUENCE:
;	sigDy = Derivsig(sigy)		;sigma(Dy(i)/di), point spacing = 1.
;	sigDy = Derivsig(X,Y,sigx,sigy) ;sigma(Dy/Dx), unequal point spacing.
;
; INPUTS:
;	Y:	The variable to be differentiated. Omit if X is omitted.
;	X:	The Variable to differentiate with respect to. If omitted,
;		unit spacing is assumed for Y, i.e. X(i) = i.
;       sigy:	The standard deviation of Y. (Vector if used alone in
;		call, vector or constant if used with other parameters)
;       sigx:	The standard deviation of X (either vector or constant).
;		Use "0.0" if the abscissa is exact; omit if X is omitted.
;
; OPTIONAL INPUT PARAMETERS:
;	As above.
;
; OUTPUTS:
;	This function returns the standard deviation of the derivative.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	See Bevington, "Data Analysis and Reduction for the Physical
;           Sciences," McGraw-Hill (1969), Chap 4.
;
; MODIFICATION HISTORY:
;       Written by Richard Bonomo at the University of Wisconsin - Madison
;       department of Electrical and Computer Engineering, July, 1991.
;	"DERIV" written by DMS, Aug, 1984.
;	CT, Aug 2013: Use correct formula (from R.M.Churchill) for unequal X spacing.
;-
;

Function Derivsig, X, Y, sigx, sigy

  on_error,2              ;Return to caller if an error occurs
  prms = n_params(0)
  n = n_elements(x)
  if (n lt 3) && (prms gt 1) then $
    message, 'X must have at least 3 points'
  if (n lt 3) && (prms eq 1) then message, $
    'sigy must be a vector of at least 3 points if used alone'

  if ((prms ne 1) && (prms ne 4)) then message,$
    'function DERIVSIG must be called with either 1 or 4 parameters'

  if prms eq 1 then begin ; unit spacing assumed
    sigy = x
    if n_elements(sigy) eq 1 then $
      sigy = fltarr(n) + sigy
    ; Points in the middle
    sigd = sqrt(0.25*(shift(sigy,-1)*shift(sigy,-1) + $
      shift(sigy,1)*shift(sigy,1)))
    ; Special case for first point
    sigd[0] = sqrt(0.25*(sigy[0]^2*9.0 + sigy[1]^2*16.0 + sigy[2]^2))
    ; Special case for last point
    sigd[-1] = sqrt(0.25*(sigy[-1]^2*9.0 + sigy[-2]^2*16.0 + sigy[-3]^2))
    return, sigd
  endif

  if n ne n_elements(y) then $
    message,'Vectors must have same size'
  if n_elements(sigy) eq 1 then $
    sigy = fltarr(n) + sigy
  nix = n_elements(sigx)
  if (nix eq 1) && (sigx[0] ne 0.0) then $
    sigx = fltarr(n) + sigx
  nix = n_elements(sigx)
  if (nix ne n) && (nix ne 1) then message,$
    'sigx vector must have the same length as X, or be a scalar'

  ; From Deriv,
  ; Three-point Lagrangian interpolation polynomial for [x0,x1,x2], [y0, y1, y2]
  ;
  ; At the central points:
  ;   y' = y0*x12/(x01*x02) + y1*(1/x12 - 1/x01) - y2*x01/(x02*x12)
  ;
  ; Where: x01 = x0 - x1, x02 = x0 - x2, x12 = x1-x2, etc.
  ; 
  ; The error estimate (if just Y has errors) is given by:
  ;   sigma = sigy0^2(dy'/dy0)^2 + sigy1^2(dy'/dy1)^2 + sigy2^2(dy'/dy2)^2
  ;
  ;   dy'/dy0 = x12/(x01*x02)
  ;   dy'/dy1 = (1/x12 - 1/x01)
  ;   dy'/dy2 = x01/(x02*x12)
  ;
  t = SIZE(x, /TYPE)
  x1 = (t eq 5 || t eq 9) ? x : float(x)
  x0 = shift(x1, 1)
  x2 = shift(x1, -1)
  x01 = x0 - x1
  x02 = x0 - x2
  x12 = x1 - x2
  sigy1sqr = sigy^2.0
  sigy0sqr = shift(sigy1sqr,1)
  sigy2sqr = shift(sigy1sqr,-1)
  sigd = sigy0sqr*(x12/(x01*x02))^2 + $
         sigy1sqr*(1/x12 - 1/x01)^2 + $
         sigy2sqr*(x01/(x02*x12))^2

  ; If X also has errors, then:
  ;   sigma += sigx0^2*(dy'/dx0)^2 + sigx1^2*(dy'/dx1)^2 + sigx2^2*(dy'/dx2)^2
  ;
  if (nix ne 1) then begin
    t = SIZE(y, /TYPE)
    y1 = (t eq 5 || t eq 9) ? y : float(y)
    y0 = shift(y1,1)
    y2 = shift(y1,-1)
    t = SIZE(sigx, /TYPE)
    sigx1sqr = sigx^2.0
    sigx0sqr = shift(sigx1sqr,1)
    sigx2sqr = shift(sigx1sqr,-1)
    dyp_dx0 = y0*(1/x02^2 - 1/x01^2) + y1/x01^2 + y2/x02^2
    dyp_dx1 = y0/x01^2 - y1*(1/x01^2 + 1/x12^2) - y2/x12^2
    dyp_dx2 = -y0/x02^2 + y1/x12^2 + y2*(1/x12^2 - 1/x02^2)
    sigd += sigx0sqr*dyp_dx0^2 + sigx1sqr*dyp_dx1^2 + sigx2sqr*dyp_dx2^2
  endif

  sigd = sqrt(sigd)

  ; Special case for first point
  ;   y' = y0*(x01+x02)/(x01*x02) - y1*x02/(x01*x12) + y2*x01/(x02*x12)
  ;   sigma = sigy0^2(dy'/dy0)^2 + sigy1^2(dy'/dy1)^2 + sigy2^2(dy'/dy2)^2
  ;   dy'/dy0 = (x01+x02)/(x01*x02)
  ;   dy'/dy1 = -x02/(x01*x12)
  ;   dy'/dy2 = x01/(x02*x12)
  ;
  xx = TEMPORARY(x1)
  x0 = xx[0]
  x1 = xx[1]
  x2 = xx[2]
  x01 = x0 - x1
  x02 = x0 - x2
  x12 = x1 - x2
  dyp_dy0 = (x01+x02)/(x01*x02)
  dyp_dy1 = -x02/(x01*x12)
  dyp_dy2 = x01/(x02*x12)
  sigd[0] = sigy1sqr[0]*dyp_dy0^2 + $
    sigy1sqr[1]*dyp_dy1^2 + sigy1sqr[2]*dyp_dy2^2

  if (nix ne 1) then begin
    yy = TEMPORARY(y1)
    y0 = yy[0]
    y1 = yy[1]
    y2 = yy[2]
    dyp_dx0 = -y0*(1/x01^2 + 1/x02^2) + y1/x01^2 + y2/x02^2
    dyp_dx1 = y0/x01^2 + y1*(1/x12^2 - 1/x01^2) - y2/x12^2
    dyp_dx2 = y0/x02^2 - y1/x12^2 + y2*(1/x12^2 - 1/x02^2)
    sigd[0] += sigx1sqr[0]*dyp_dx0^2 + sigx1sqr[1]*dyp_dx1^2 + sigx1sqr[2]*dyp_dx2^2
  endif
  sigd[0] = sqrt(sigd[0])

  ; Special case for last point
  ;   y' = -y0*x12/(x01*x02) + y1*x02/(x01*x12) - y2*(x02+x12)/(x02*x12)
  ;   sigma = sigy0^2(dy'/dy0)^2 + sigy1^2(dy'/dy1)^2 + sigy2^2(dy'/dy2)^2
  ;   dy'/dy0 = -x12/(x01*x02)
  ;   dy'/dy1 = x02/(x01*x12)
  ;   dy'/dy2 = -(x02+x12)/(x02*x12)
  ;
  x0 = xx[-3]
  x1 = xx[-2]
  x2 = xx[-1]
  x01 = x0 - x1
  x02 = x0 - x2
  x12 = x1 - x2
  dyp_dy0 = -x12/(x01*x02)
  dyp_dy1 = x02/(x01*x12)
  dyp_dy2 = -(x02+x12)/(x02*x12)
  sigd[-1] = sigy1sqr[-3]*dyp_dy0^2 + $
    sigy1sqr[-2]*dyp_dy1^2 + sigy1sqr[-1]*dyp_dy2^2

  if (nix ne 1) then begin
    y0 = yy[-3]
    y1 = yy[-2]
    y2 = yy[-1]
    dyp_dx0 = y0*(1/x01^2 - 1/x02^2) - y1/x01^2 + y2/x02^2
    dyp_dx1 = -y0/x01^2 + y1*(1/x01^2 - 1/x12^2) + y2/x12^2
    dyp_dx2 = y0/x02^2 + y1/x12^2 - y2*(1/x02^2 + 1/x12^2)
    sigd[-1] += sigx1sqr[-3]*dyp_dx0^2 + sigx1sqr[-2]*dyp_dx1^2 + sigx1sqr[-1]*dyp_dx2^2
  endif
  sigd[-1] = sqrt(sigd[-1])

  return, sigd
end
